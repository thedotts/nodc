﻿using System.IO;
using System.Text.RegularExpressions;

namespace NODC_OMS.Helper
{
    public class FileHelper
    {
        public static string sanitiseFilename(string name)
        {
            string invalidChars = Regex.Escape(new string(Path.GetInvalidFileNameChars()));
            string invalidReStr = string.Format(@"[{0}]+", invalidChars);
            name = Regex.Replace(name, invalidReStr, "_");
            name = name.Replace('&', '_');
            name = name.Replace(' ', '_');
            return name;
        }
    }
}