﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI.WebControls;

namespace NODC_OMS.Helper
{
    public class EmailHelper
    {
        //
        // Helper: Send Email

        public static bool SendEmail(string subject, string body, ListDictionary replacements, string recipient)
        {
            try
            {
                MailDefinition md = new MailDefinition();
                md.From = ConfigurationManager.AppSettings["SystemEmailAddress"].ToString();
                md.IsBodyHtml = true;
                md.Subject = subject;

                //recipient = "chanern@thedottsolutions.com";

                MailMessage mm = md.CreateMailMessage(recipient, replacements, body, new System.Web.UI.Control());
                mm.From = new MailAddress(ConfigurationManager.AppSettings["SystemEmailAddress"].ToString(), ConfigurationManager.AppSettings["SiteName"].ToString());

                SmtpClient smtp = new SmtpClient();

                //SmtpClient client = new SmtpClient();
                //client.UseDefaultCredentials = false;
                //client.Credentials = new System.Net.NetworkCredential("donotreply@mss.org.sg", "Buru35081");
                //client.Port = 587; // You can use Port 25 if 587 is blocked
                //client.Host = "smtp.office365.com";
                //client.DeliveryMethod = SmtpDeliveryMethod.Network;
                //client.EnableSsl = true;

                smtp.Send(mm);
                //client.Send(mm);

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static bool SendEmail(string subject, string body, ListDictionary replacements, string recipient, string cc, string bcc, byte[] attachment, string fileName)
        {
            try
            {
                MailDefinition md = new MailDefinition();
                md.From = ConfigurationManager.AppSettings["SystemEmailAddress"].ToString();
                md.IsBodyHtml = true;
                md.Subject = subject;

                MailMessage mm = md.CreateMailMessage(recipient, replacements, body, new System.Web.UI.Control());
                mm.From = new MailAddress(ConfigurationManager.AppSettings["SystemEmailAddress"].ToString(), ConfigurationManager.AppSettings["SiteName"].ToString());
                if (!string.IsNullOrEmpty(cc))
                {
                    mm.CC.Add(cc);
                }
                if (!string.IsNullOrEmpty(bcc))
                {
                    mm.Bcc.Add(bcc);
                }
                MemoryStream stream = new MemoryStream(attachment);
                Attachment attc = new Attachment(stream, fileName);
                mm.Attachments.Add(attc);

                SmtpClient smtp = new SmtpClient();

                //SmtpClient client = new SmtpClient();
                //client.UseDefaultCredentials = false;
                //client.Credentials = new System.Net.NetworkCredential("donotreply@mss.org.sg", "Buru35081");
                //client.Port = 587; // You can use Port 25 if 587 is blocked
                //client.Host = "smtp.office365.com";
                //client.DeliveryMethod = SmtpDeliveryMethod.Network;
                //client.EnableSsl = true;

                smtp.Send(mm);
                //client.Send(mm);

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}