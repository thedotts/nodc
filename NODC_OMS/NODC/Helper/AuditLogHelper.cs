﻿using DataAccess.POCO;
using NODC_OMS.Models;

namespace NODC_OMS.Helper
{
    public class AuditLogHelper
    {
        public static bool WriteAuditLog(int userTriggeringId, string userRole, string tableAffected, string description)
        {
            AuditLog auditLogs = new AuditLog();
            auditLogs.IPAddress = GetIPAddress();
            auditLogs.UserTriggeringId = userTriggeringId;
            auditLogs.UserRole = userRole;
            auditLogs.TableAffected = tableAffected;
            auditLogs.Description = description;

            AuditLogRepository _auditLogsModel = new AuditLogRepository();
            bool result = _auditLogsModel.Add(auditLogs);

            return result;
        }

        public static string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
    }
}