﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace NODC_OMS.Helper
{
    public class FormValidationHelper
    {
        //Email Format
        public static bool EmailFormat(string email)
        {
            return Regex.IsMatch(email,
                @"^([a-zA-Z0-9])(([a-zA-Z0-9])*([\._\+-])*([a-zA-Z0-9]))*@(([a-zA-Z0-9\-])+(\.))+([a-zA-Z]{2,4})+$");
        }

        //String Length
        public static bool StringLength(string keyword, int length)
        {
            if (keyword.Length <= length)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Amount Format: Format
        public static bool AmountFormat(string amount)
        {
            try
            {
                decimal val = Convert.ToDecimal(amount);

                return true;
            }
            catch
            {
                return false;
            }
        }

        //Amount Format: Range
        public static bool AmountFormat(string amount, int max)
        {
            if (!amount.StartsWith("-"))
            {
                string[] amt = amount.Split('.');

                if (amt[0].Length <= max)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        //Amount Format2: Range
        public static bool AmountFormat2(string amount, int max)
        {
            string[] amt = amount.Split('.');

            if (amt[0].Length <= max)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //AmountFormatter
        public static string AmountFormatter(decimal amount, int dp)
        {
            switch (dp)
            {
                case 0:
                    return amount.ToString("#,##0");
                case 1:
                    return amount.ToString("#,##0.0");
                case 2:
                    return amount.ToString("#,##0.00");
                case 3:
                    return amount.ToString("#,##0.000");
                case 4:
                    return amount.ToString("#,##0.0000");
                default:
                    return amount.ToString("#,##0.00");
            }
        }

        //Integer Format: Format
        public static bool IntegerFormat(string integer)
        {
            try
            {
                int i = Convert.ToInt32(integer);

                return true;
            }
            catch
            {
                return false;
            }
        }

        //Integer Format: Range
        public static bool IntegerFormat(string integer, int max)
        {
            if (!integer.StartsWith("-"))
            {
                if (integer.Length <= max)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        //Integer Formatter
        public static string IntegerFormatter(int integer)
        {
            return integer.ToString("#,##0");
        }
    }
}