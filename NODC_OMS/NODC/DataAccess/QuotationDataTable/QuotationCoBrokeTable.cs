﻿using DataAccess.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DataAccess
{
    public class QuotationCoBrokeTable
    {
        public int RowId { get; set; }

        public SelectList UserID { get; set; }

        public SelectList PercentageOfProfit { get; set; }

    }
}