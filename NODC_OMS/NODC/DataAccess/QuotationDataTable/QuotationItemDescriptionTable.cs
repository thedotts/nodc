﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DataAccess
{
    public class QuotationItemDescriptionTable
    {

        public int RowId { get; set; }

        public string ItemName { get; set; }

        public string Description { get; set; }

        public string Qty { get; set; }

        public string Unit { get; set; }

        public string Rate { get; set; }

        public string Amount { get; set; }

        public string Unit_Cost { get; set; }

        public string Est_Cost { get; set; }

        public string AdjCost { get; set; }

        public string Est_Profit { get; set; }

        public string Remarks { get; set; }

        public string Mgr_Comments { get; set; }

        public string FromDB { get; set; }

    }

    public class ItemDescriptionBigTable
    {
        public int TableID { get; set; }

        public string CategoryName { get; set; }

        public bool IsBasic { get; set; }

        public List<ItemDescriptionRow> ItemDescriptionRows { get; set; }

    }

    public class ItemDescriptionRow
    {
        public int ID { get; set; }

        public int RowId { get; set; }

        public string Description { get; set; }

        public string Qty { get; set; }

        public string Unit { get; set; }

        public string Rate { get; set; }

        public string Amount { get; set; }

        public string Unit_Cost { get; set; }

        public string Est_Cost { get; set; }

        public string AdjCost { get; set; }

        public string Est_Profit { get; set; }

        public string Remarks { get; set; }

        public string Mgr_Comments { get; set; }

    }
}