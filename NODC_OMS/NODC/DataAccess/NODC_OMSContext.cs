﻿using DataAccess.POCO;
using System.Data.Entity;

namespace DataAccess
{
    public class NODC_OMSContext : DbContext
    {
        public NODC_OMSContext()
            : base("NODC_OMS")
        {
            this.Database.Connection.StateChange += Connection_StateChange;
        }

        void Connection_StateChange(object sender, System.Data.StateChangeEventArgs e)
        {
            if (e.CurrentState == System.Data.ConnectionState.Open)
            {
                var connection = (System.Data.Common.DbConnection)sender;
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "SET ANSI_NULLS OFF";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public DbSet<AuditLog> AuditLogs { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<CustomerReminder> CustomerReminders { get; set; }

        public DbSet<Invoice> Invoices { get; set; }

        public DbSet<InvoiceItemDescription> InvoiceItemDescriptions { get; set; }

        public DbSet<EarlyPayout> EarlyPayouts { get; set; }

        public DbSet<Payout> Payouts { get; set; }

        public DbSet<PayoutCalculation> PayoutCalculations { get; set; }

        public DbSet<Project> Projects { get; set; }

        public DbSet<ProjectProjectExpenses> ProjectProjectExpenses { get; set; }

        public DbSet<ProjectEstimator> ProjectEstimators { get; set; }

        public DbSet<Quotation> Quotations { get; set; }

        public DbSet<QuotationCoBroke> QuotationCoBrokes { get; set; }

        public DbSet<QuotationItemDescription> QuotationItemDescriptions { get; set; }

        public DbSet<SystemSetting> SystemSettings { get; set; }

        public DbSet<User> Users { get; set; }
    }
}