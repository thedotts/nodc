﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess
{
    public class TaskListLoadAllRecord
    {
        public int ID { get; set; }

        public string ItemID { get; set; }

        public string Name { get; set; }

        public string Date { get; set; }

        public string Amount { get; set; }

        public string Status { get; set; }

        public string Type { get; set; }

    }
}