﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.POCO
{
    public class User
    {
        [Key]
        
        public int ID { get; set; }

        [Display(Name = "User ID")]
        public string UserID { get; set; }

        [Display(Name = "Login ID *")]
        public string LoginID { get; set; }

        [Display(Name = "Password *")]
        public string Password { get; set; }

        [Display(Name = "Name *")]
        public string Name { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Email *")]
        public string Email { get; set; }

        [Display(Name = "Tel *")]
        public string Tel { get; set; }

        [Display(Name = "Mobile")]
        public string Mobile { get; set; }

        [Display(Name = "Bank Name")]
        public string BankName { get; set; }

        [Display(Name = "Bank Account No")]
        public string BankAccNo { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

        [Display(Name = "Monthly Reminder?")]
        public string Reminder { get; set; }

        [Display(Name = "Is Agency Leader?")]
        public string IsAgencyLeader { get; set; }

        [Display(Name = "Upline *")]
        public int UplineId { get; set; }

        [Display(Name = "Role")]
        public string Role { get; set; }

        [Display(Name = "Position")]
        public string Position { get; set; }

        [Display(Name = "Preset Value *")]
        public decimal PresetValue { get; set; }

        [Display(Name = "Total Completed Profit")]
        public decimal TotalCompletedProfit { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        public string IsDeleted { get; set; }

        public string ResetPasswordToken { get; set; }

        [ForeignKey("UplineId")]
        public virtual User Upline { get; set; }
    }
}