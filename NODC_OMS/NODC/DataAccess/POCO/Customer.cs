﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.POCO
{
    public class Customer
    {
        [Key]
        public int ID { get; set; }

        [Display(Name = "Customer ID")]
        public string CustomerID { get; set; }

        [Display(Name = "Login ID *")]
        public string LoginID { get; set; }

        [Display(Name = "Password *")]
        public string Password { get; set; }

        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Display(Name = "Agency *")]
        public string Agency { get; set; }

        [Display(Name = "Contact Person *")]
        public string ContactPerson { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Tel *")]
        public string Tel { get; set; }

        [Display(Name = "Fax")]
        public string Fax { get; set; }

        [Display(Name = "Bank Name")]
        public string BankName { get; set; }

        [Display(Name = "Bank Account No")]
        public string BankAccNo { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

        public string UplineUserType { get; set; }

        [Display(Name = "Upline *")]
        public int UplineId { get; set; }

        [Display(Name = "Position")]
        public string Position { get; set; }

        [Display(Name = "Assign to")]
        public int AssignToId { get; set; }

        public decimal TotalCompletedProfit { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        public string IsDeleted { get; set; }

        public string ResetPasswordToken { get; set; }

        [ForeignKey("UplineId")]
        public virtual User Upline { get; set; }

        [ForeignKey("UplineId")]
        public virtual Customer Customers { get; set; }

        [ForeignKey("AssignToId")]
        public virtual User AssignTo { get; set; }

        public virtual IList<CustomerReminder> CustomerReminders { get; set; }
    }
}