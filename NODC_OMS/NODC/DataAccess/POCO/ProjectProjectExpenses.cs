﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.POCO
{
    public class ProjectProjectExpenses
    {
        [Key]
        public int ID { get; set; }

        public int ProjectId { get; set; }

        public string Description { get; set; }

        public string SupplierName { get; set; }

        public string InvoiceNo { get; set; }

        public DateTime Date { get; set; }

        public string GST { get; set; }

        public decimal Cost { get; set; }

        public string Status { get; set; }
    }
}