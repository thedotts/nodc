﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DataAccess.POCO
{
    public class SystemSetting
    {
        [Key]
        public int ID { get; set; }

        public string Code { get; set; }

        public string Value { get; set; }

        public string DataType { get; set; }

        public string IsRequired { get; set; }

        public DateTime UpdatedOn { get; set; }
    }
}