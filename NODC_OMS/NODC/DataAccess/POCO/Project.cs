﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.POCO
{
    public class Project
    {
        [Key]
        public int ID { get; set; }

        [Display(Name = "Project ID")]
        public string ProjectID { get; set; }

        [Display(Name = "Company")]
        public string CompanyName { get; set; }

        [Display(Name = "Quotation Reference *")]
        public int QuotationReferenceId { get; set; }

        [Display(Name = "Customer *")]
        public int CustomerId { get; set; }

        [Display(Name = "Prepared by")]
        public int PreparedById { get; set; }

        [Display(Name = "Date *")]
        public DateTime Date { get; set; }

        [Display(Name = "Coordinator *")]
        public int CoordinatorId { get; set; }

        [Display(Name = "Project Title *")]
        public string ProjectTitle { get; set; }

        [Display(Name = "Description")]
        public string ProjectDescription { get; set; }

        [Display(Name = "Type")]
        public string ProjectType { get; set; }

        [Display(Name = "Photos")]
        public string PhotoUploads { get; set; }

        [Display(Name = "Floor Plan")]
        public string FloorPlanUploads { get; set; }

        [Display(Name = "3D Drawings")]
        public string ThreeDDrawingUploads { get; set; }

        [Display(Name = "As-built Drawings")]
        public string AsBuildDrawingUploads { get; set; }

        [Display(Name = "Submission Stage")]
        public string SubmissionStageUploads { get; set; }

        [Display(Name = "Others")]
        public string OtherUploads { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        public DateTime? CompletedDate { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        public string IsDeleted { get; set; }

        public virtual IList<ProjectProjectExpenses> ProjectedExpenses { get; set; }

        [ForeignKey("QuotationReferenceId")]
        public virtual Quotation Quotation { get; set; }

        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("PreparedById")]
        public virtual User PreparedBy { get; set; }

        [ForeignKey("CoordinatorId")]
        public virtual User Coordinator { get; set; }

        [ForeignKey("CoordinatorId")]
        public virtual Customer Coordinator_Customer { get; set; }
    }
}