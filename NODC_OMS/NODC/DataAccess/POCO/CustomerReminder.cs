﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.POCO
{
    public class CustomerReminder
    {
        [Key]
        public int ID { get; set; }

        public int CustomerId { get; set; }

        public string Description { get; set; }

        public DateTime Date { get; set; }

        public string Repeat { get; set; }

        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
    }
}