﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.POCO
{
    public class ProjectEstimator
    {
        [Key]
        public int ID { get; set; }

        public int ProjectId { get; set; }

        public string ProjectExpensesDescription { get; set; }

        public string ProjectExpensesCategoryName { get; set; }

        public decimal ProjectExpensesAmount { get; set; }

        public decimal ProjectExpensesProjectedCost { get; set; }

        public decimal ProjectExpensesAdjCost { get; set; }

        public decimal ProjectExpensesProfit { get; set; }

        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }
    }
}