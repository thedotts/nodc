﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DataAccess.POCO
{
    public class PayoutCalculation
    {
        [Key]
        public int ID { get; set; }

        public int PayoutId { get; set; }

        //public int Tier { get; set; }

        public int UserId { get; set; }

        public string UserType { get; set; }

        public decimal Revenue { get; set; }

        public decimal PayoutPercentage { get; set; }

        public decimal Commission { get; set; }

        [ForeignKey("PayoutId")]
        public virtual Payout Payout { get; set; }

        [ForeignKey("UserId")]
        public virtual User Salesperson { get; set; }

        [ForeignKey("UserId")]
        public virtual Customer Customer { get; set; }
    }
}