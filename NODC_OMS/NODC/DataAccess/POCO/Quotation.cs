﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.POCO
{
    public class Quotation
    {
        [Key]
        public int ID { get; set; }

        [Display(Name = "Quotation ID")]
        public string QuotationID { get; set; }

        [Display(Name = "Company *")]
        public string CompanyName { get; set; }

        public string HasGST { get; set; }

        [Display(Name = "Customer *")]
        public int? CustomerId { get; set; }

        [Display(Name = "Contact Person *")]
        public string ContactPerson { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Tel")]
        public string Tel { get; set; }

        [Display(Name = "Fax")]
        public string Fax { get; set; }

        [Display(Name = "Prepared By")]
        public int PreparedById { get; set; }

        public string SalesPersonType { get; set; }

        [Display(Name = "Sales Person *")]
        public int? SalesPersonId { get; set; }

        [Display(Name = "Date *")]
        public DateTime? QuotationDate { get; set; }

        [Display(Name = "Sales Documents")]
        public string SalesDocumentUploads { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

        [Display(Name = "Payment Terms")]
        public string PaymentTerms { get; set; }

        public string Status { get; set; }

        public int ReviseQuotationTimes { get; set; }

        public int VariationOrderTimes { get; set; }

        public int PreviousQuotationId { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        public string IsDeleted { get; set; }

        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("PreparedById")]
        public virtual User PreparedBy { get; set; }

        [ForeignKey("SalesPersonId")]
        public virtual User SalesPerson { get; set; }

        public virtual IList<QuotationCoBroke> CoBrokes { get; set; }

        public virtual IList<QuotationItemDescription> ItemDescriptions { get; set; }
    }
}