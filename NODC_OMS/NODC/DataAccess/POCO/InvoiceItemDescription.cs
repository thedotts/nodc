﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.POCO
{
    public class InvoiceItemDescription
    {
        [Key]
        public int ID { get; set; }

        public int InvoiceId { get; set; }

        public string Description { get; set; }

        public string PercentageOfQuotation { get; set; }

        public decimal Amount { get; set; }

        [ForeignKey("InvoiceId")]
        public virtual Invoice Invoice { get; set; }
    }
}