﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.POCO
{
    public class QuotationCoBroke
    {
        [Key]
        public int ID { get; set; }

        public int QuotationId { get; set; }

        public string QuotationCoBrokeUserType { get; set; }

        public int QuotationCoBrokeUserId { get; set; }

        public int QuotationCoBrokePercentOfProfit { get; set; }

        [ForeignKey("QuotationId")]
        public virtual Quotation Quotation { get; set; }

        [ForeignKey("QuotationCoBrokeUserId")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("QuotationCoBrokeUserId")]
        public virtual User User { get; set; }
    }
}