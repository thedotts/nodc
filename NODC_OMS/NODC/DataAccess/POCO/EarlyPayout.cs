﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DataAccess.POCO
{
    public class EarlyPayout
    {
        [Key]
        public int ID { get; set; }

        [Display(Name = "Early Payout ID")]
        public string EarlyPayoutID { get; set; }

        [Display(Name = "Project *")]
        [Required(ErrorMessage = "Project is required!")]
        public int ProjectId { get; set; }

        [Display(Name = "Pay To *")]
        //[Required(ErrorMessage = "Pay To is required!")]
        public int PayToId { get; set; }

        public string UserType { get; set; }

        [Display(Name = "Payout Date *")]
        [Required(ErrorMessage = "Payout Date is required!")]
        public DateTime PayoutDate { get; set; }

        [Display(Name = "Amount *")]
        [Required(ErrorMessage = "Amount is required!")]
        public decimal Amount { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        public string IsDeleted { get; set; }

        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }

        [ForeignKey("PayToId")]
        public virtual User User { get; set; }

        [ForeignKey("PayToId")]
        public virtual Customer Customer { get; set; }
    }
}