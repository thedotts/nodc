﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.POCO
{
    public class Invoice
    {
        [Key]
        public int ID { get; set; }

        [Display(Name = "Invoice ID")]
        public string InvoiceID { get; set; }

        [Display(Name = "Company")]
        public string Company { get; set; }

        [Display(Name = "Quotation *")]
        [Required(ErrorMessage = "Quotation is required!")]
        public int QuotationId { get; set; }

        [Display(Name = "Project")]
        public int ProjectId { get; set; }

        [Display(Name = "Customer *")]
        [Required(ErrorMessage = "Customer is required!")]
        public int CustomerId { get; set; }

        [Display(Name = "Contact Person *")]
        [Required(ErrorMessage = "Contact Person is required!")]
        public string ContactPerson { get; set; }

        [Display(Name = "Prepared By")]
        public int PreparedById { get; set; }

        [Display(Name = "Date *")]
        [Required(ErrorMessage = "Date is required!")]
        public DateTime Date { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

        [Display(Name = "Notes")]
        public string Notes { get; set; }

        [Display(Name = "GST *")]
        [Required(ErrorMessage = "GST is required!")]
        public string GST { get; set; }

        [Display(Name = "Payment Made")]
        public decimal? PaymentMade { get; set; }

        [Display(Name = "Subtotal")]
        public decimal SubtotalAmount { get; set; }

        [Display(Name = "GST ")]
        public decimal GSTAmount { get; set; }

        [Display(Name = "Grand Total")]
        public decimal GrandTotalAmount { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        public string IsDeleted { get; set; }

        public virtual IList<InvoiceItemDescription> ItemDescriptions { get; set; }

        [ForeignKey("QuotationId")]
        public virtual Quotation Quotation { get; set; }

        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("PreparedById")]
        public virtual User PreparedBy { get; set; }
    }
}