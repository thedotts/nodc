﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.POCO
{
    public class QuotationItemDescription
    {
        [Key]
        public int ID { get; set; }

        public int QuotationId { get; set; }

        public int RowId { get; set; }

        public string CategoryName { get; set; }

        public string QuotationItem_Description { get; set; }

        public decimal QuotationItemQty { get; set; }

        public string QuotationItemUnit { get; set; }

        public decimal QuotationItemRate { get; set; }

        public decimal QuotationItemAmount { get; set; }

        public decimal QuotationItemUnitCost { get; set; }

        public decimal QuotationItemEstCost { get; set; }

        public decimal QuotationItemEstProfit { get; set; }

        public string QuotationItemRemarks { get; set; }

        public string QuotationItemMgrComments { get; set; }

        [ForeignKey("QuotationId")]
        public virtual Quotation Quotation { get; set; }
    }
}