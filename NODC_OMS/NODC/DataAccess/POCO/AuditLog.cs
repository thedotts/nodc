﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.POCO
{
    public class AuditLog
    {
        [Key]
        public int ID { get; set; }

        public string IPAddress { get; set; }

        public DateTime Timestamp { get; set; }

        public int UserTriggeringId { get; set; }

        public string UserRole { get; set; }

        public string TableAffected { get; set; }

        public string Description { get; set; }

        [ForeignKey("UserTriggeringId")]
        public virtual User User { get; set; }

        [ForeignKey("UserTriggeringId")]
        public virtual Customer Customer { get; set; }
    }
}