﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DataAccess.POCO
{
    public class Payout
    {
        [Key]
        public int ID { get; set; }

        [Display(Name = "Payout ID")]
        public string PayoutID { get; set; }

        [Display(Name = "Pay To")]
        public int PayToId { get; set; }

        public string UserType { get; set; }

        [Display(Name = "Period")]
        public DateTime Period { get; set; }

        [Display(Name = "Comm Amount (SGD)")]
        public decimal TotalCommissionAmount { get; set; }

        [Display(Name = "Retained Amount")]
        public decimal TotalRetainedAmount { get; set; }

        [Display(Name = "Total Early Payout Amount")]
        public decimal TotalEarlyPayoutAmount { get; set; }

        [Display(Name = "Payout Amount")]
        public decimal TotalPayoutAmount { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        public string IsDeleted { get; set; }

        public virtual List<PayoutCalculation> PayoutCalculations { get; set; }

        [ForeignKey("PayToId")]
        public virtual User Salesperson { get; set; }

        [ForeignKey("PayToId")]
        public virtual Customer Customer { get; set; }
    }
}