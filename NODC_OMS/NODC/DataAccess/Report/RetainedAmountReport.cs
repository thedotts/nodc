﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess.Report
{
    public class RetainedAmountReport
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string RetainAmount { get; set; }
    }
}