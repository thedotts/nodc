﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess.Report
{
    public class PayoutReport
    {
        public string Name { get; set; }

        public string NRIC { get; set; }

        public string BankNumber { get; set; }

        public string BankName { get; set; }

        public string CommissionAmount { get; set; }
    }
}