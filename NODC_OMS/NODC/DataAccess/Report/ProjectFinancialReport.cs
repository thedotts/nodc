﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess.Report
{
    public class ProjectFinancialReport
    {
        public string InvoiceID { get; set; }

        public string Description { get; set; }

        public string Percentage { get; set; }

        public string Status { get; set; }

        public string Amount { get; set; }

        public string BillDate { get; set; }

        public class ExpenseReport
        {
            public string Description { get; set; }

            public string SupplierName { get; set; }

            public string InvoiceNo { get; set; }

            public DateTime Date { get; set; }

            public string GST { get; set; }

            public string Cost { get; set; }

            public string Status { get; set; }
        }
    }
}