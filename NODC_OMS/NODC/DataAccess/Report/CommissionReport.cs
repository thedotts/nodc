﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess.Report
{
    public class CommissionReport
    {
        public string Name { get; set; }

        public string NRIC { get; set; }

        public string CommissionValue { get; set; }
    }
}