﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess.Report
{
    public class ExpenseReport
    {
        public int ObjectID { get; set; }

        public string ProjectName { get; set; }

        public string ProjectID { get; set; }

        public string SupplierName { get; set; }

        public string Description { get; set; }

        public string Cost { get; set; }

        public string GST { get; set; }

        public string Status { get; set; }

        public DateTime Date { get; set; }
    }
}