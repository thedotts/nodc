﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess
{
    public class PayoutSummary
    {
        public bool Result { get; set; }

        public string ErrorMessage { get; set; }

        public string PayoutSummaryView { get; set; }
    }
}