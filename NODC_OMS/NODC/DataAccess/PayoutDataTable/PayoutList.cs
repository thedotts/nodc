﻿using DataAccess.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess.PayoutDataTable
{
    public class PayoutList
    {
        public string UserID { get; set; }

        public string Name { get; set; }

        public string UserType { get; set; }

        public Payout Payout { get; set; }

        public List<PayoutCalculationList> PayoutCalculations { get; set; }
    }

    public class PayoutCalculationList
    {
        public string UserID { get; set; }

        public string Name { get; set; }

        public string UserType { get; set; }

        public PayoutCalculation PayoutCalculation { get; set; }
    }
}