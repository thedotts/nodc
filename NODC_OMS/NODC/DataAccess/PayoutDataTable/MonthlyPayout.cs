﻿using DataAccess.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess
{
    //public class MonthlyPayout
    //{
    //    public int UserId { get; set; }

    //    public string UserID { get; set; }

    //    public string UserType { get; set; }

    //    public string UserRole { get; set; }

    //    public string UserPosition { get; set; }

    //    public int UserUplineId { get; set; }

    //    public bool HasPayout { get; set; }

    //    public Payout Payout { get; set; }
    //}

    public class MonthlyPayout
    {
        public int UserId { get; set; }

        public string UserType { get; set; }

        public decimal TotalCommission { get; set; }
    }
}