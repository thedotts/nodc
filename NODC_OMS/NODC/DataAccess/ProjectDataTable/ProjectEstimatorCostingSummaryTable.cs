﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess
{
    public class ProjectEstimatorCostingSummaryTable
    {
        public int RowId { get; set; }

        public string CategoryName { get; set; }

        public string Amount { get; set; }

        public string ProjectedCost { get; set; }

        public string AdjCost { get; set; }

        public string AdjProfit { get; set; }
    }
}