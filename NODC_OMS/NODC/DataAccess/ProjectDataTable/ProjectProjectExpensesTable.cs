﻿using DataAccess.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DataAccess
{
    public class ProjectProjectExpensesTable
    {
        public int RowId { get; set; }

        public string Description { get; set; }

        public string SupplierName { get; set; }

        public string InvoiceNo { get; set; }

        public string Date { get; set; }

        public SelectList GST { get; set; }

        public string CostBeforeGST { get; set; }

        public SelectList Status { get; set; }
    }
}