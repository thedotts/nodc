﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess
{
    public class ProjectEstimatorTable
    {

        //Costing Summary Part
        public string QuotationCostingSummaryView { get; set; }

        //Item Description Part

        public string QuotationItemDescriptionView { get; set; }

        public bool Result { get; set; }

        public string ErrorMessage { get; set; }
    }
}