﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess
{
    public class ProjectTitle
    {
        public bool Result { get; set; }

        public string ErrorMessage { get; set; }

        public string Options { get; set; }
    }
}