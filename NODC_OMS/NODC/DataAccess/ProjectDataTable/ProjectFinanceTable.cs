﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess
{
    public class ProjectFinanceTable
    {
        public string projectStatus { get; set; }

        public string TotalRevenue { get; set; }

        public string TotalExpenses { get; set; }

        public string TotalProfit { get; set; }

        public string highlight { get; set; }

        public string ProjectExpensesView { get; set; }

        public bool Result { get; set; }

        public string ErrorMessage { get; set; }
    }
}