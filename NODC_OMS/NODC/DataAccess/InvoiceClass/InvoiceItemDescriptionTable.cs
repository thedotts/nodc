﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DataAccess
{
    public class InvoiceItemDescriptionTable
    {
        public int RowId { get; set; }

        public string Description { get; set; }

        public string PercentageOfQuotation { get; set; }

        public SelectList PercentageOfQuotationDDL { get; set; }

        public string Amount { get; set; }
    }
}