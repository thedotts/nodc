﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess
{
    public class InvoiceQuotation
    {
        public bool Result { get; set; }

        public string ErrorMessage { get; set; }

        public InvoiceQuotationDetail QuotationDetail { get; set; }
    }

    public class InvoiceQuotationDetail
    {
        public string Company { get; set; }

        public int CustomerId { get; set; }

        public string GST { get; set; }

        public string TotalQuotationAmount { get; set; }
    }

    public class InvoiceCustomer
    {
        public bool Result { get; set; }

        public string ErrorMessage { get; set; }

        public InvoiceCustomerDetail CustomerDetail { get; set; }
    }

    public class InvoiceCustomerDetail
    {
        public string ContactPerson { get; set; }

        public string Address { get; set; }

        public string Tel { get; set; }

        public string Fax { get; set; }
    }
}