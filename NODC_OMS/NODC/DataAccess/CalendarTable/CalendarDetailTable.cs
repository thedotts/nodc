﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DataAccess
{
    public class CalendarDetailTable
    {

        public string summary { get; set; }

        public DateTime mainDate { get; set; }

        public string start { get; set; }

        public string end { get; set; }

        public string durationHour { get; set; }

        public List<CalendarDetailSmallTable> CalendarDetailSmallTable { get; set; }

    }

    public class CalendarDetailSmallTable
    {
        public string summary { get; set; }

        public string start { get; set; }

        public string end { get; set; }

        public string durationHour { get; set; }

    }

}