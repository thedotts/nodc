﻿namespace System.Web.Mvc
{
    public class Dropdown
    {
        public string name { get; set; }

        public string val { get; set; }

        public string optGroup { get; set; }
    }
}