﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DataAccess
{
    public class CustomerReminderTable
    {
        public int RowId { get; set; }

        public string Description { get; set; }

        public string Date { get; set; }

        public SelectList RepeatDDL { get; set; }

    }
}