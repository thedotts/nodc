﻿using DataAccess.POCO;
using PagedList;
using System.Collections.Generic;

namespace NODC_OMS.Models
{
    public interface IAuditLogRepository
    {
        IList<AuditLog> GetAll(string tableName, string startDate, string endDate);

        IList<AuditLog> GetAll(int userid);

        IPagedList<AuditLog> GetPaged(string tableName, string startDate, string endDate, int page, int size);

        IList<AuditLog> GetUniqueTable();

        bool Add(AuditLog data);
    }
}