﻿using DataAccess;
using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public class QuotationCoBrokeRepository : IQuotationCoBrokeRepository
    {
        private NODC_OMSContext db;

        public QuotationCoBrokeRepository()
        {
            db = new NODC_OMSContext();
        }

        public IQueryable<QuotationCoBroke> Select()
        {
            var result = from u in db.QuotationCoBrokes select u;

            return result;
        }

        public IQueryable<QuotationCoBroke> SelectAll()
        {
            var result = from u in db.QuotationCoBrokes select u;

            return result;
        }

        public IList<QuotationCoBroke> GetAll()
        {
            try
            {
                var records = Select();

                return records.OrderBy(e => e.QuotationCoBrokeUserId).ToList();
            }
            catch
            {
                throw;
            }
        }

        public bool Add(QuotationCoBroke data)
        {
            try
            {
                db.QuotationCoBrokes.Add(data);
                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                QuotationCoBroke quotationCoBroke = db.QuotationCoBrokes.Find(id);

                db.QuotationCoBrokes.Remove(quotationCoBroke);

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public List<QuotationCoBroke> GetAllRelatedQuotationId(int id)
        {
            try
            {
                var records = Select();

                return records.OrderBy(e => e.QuotationId).Where(e => e.QuotationId == id).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<QuotationCoBroke> GetPersonalAll(int id)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.QuotationId == id).OrderBy(e => e.QuotationId).ToList();
            }
            catch
            {
                throw;
            }
        }
    }
}