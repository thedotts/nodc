﻿using DataAccess;
using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public class QuotationRepository : IQuotationRepository
    {
        private NODC_OMSContext db;

        public QuotationRepository()
        {
            db = new NODC_OMSContext();
        }

        public IQueryable<Quotation> Select()
        {
            var result = from u in db.Quotations select u;

            return result.Where(e => e.IsDeleted == "N");
        }

        public IQueryable<Quotation> SelectAll()
        {
            var result = from u in db.Quotations select u;

            return result;
        }

        public IList<Quotation> GetAll()
        {
            try
            {
                var records = Select();

                return records.OrderBy(e => e.QuotationID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Quotation> GetAllQuotations(string startDate, string endDate)
        {
            try
            {
                var records = SelectAll();

                if (!string.IsNullOrEmpty(startDate))
                {
                    DateTime start = Convert.ToDateTime(startDate + " 00:00:00");

                    records = records.Where(e => e.CreatedOn >= start);
                }

                if (!string.IsNullOrEmpty(endDate))
                {
                    DateTime end = Convert.ToDateTime(endDate + " 23:59:59");

                    records = records.Where(e => e.CreatedOn <= end);
                }

                return records.OrderBy(e => e.QuotationID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Quotation> GetUserQuotations(int id)
        {
            try
            {
                var records = Select();

                records = records.Where(e => e.PreparedById == id);

                return records.ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Quotation> GetAllLatestQuotationID()
        {
            try
            {
                var records = SelectAll();

                return records.GroupBy(e => e.PreviousQuotationId).Select(eGroup => eGroup.OrderByDescending(e => e.CreatedOn).FirstOrDefault()).OrderByDescending(e => e.CreatedOn).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Quotation> getAllSameCustomerId(int id)
        {
            try
            {
                var records = Select();
                return records.GroupBy(e => e.PreviousQuotationId).Select(eGroup => eGroup.OrderByDescending(e => e.CreatedOn).FirstOrDefault()).OrderByDescending(e => e.CreatedOn).Where(e => e.CustomerId == id).ToList();
                //return records.Where(e => e.CustomerId == id).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Quotation> getAllRelatedIdGetAllRelatedQuotationId(int id)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.PreviousQuotationId == id).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Quotation> GetAllTaskList(int id)
        {

            try
            {
                var records = Select();

                records = records.Where(e => e.SalesPersonId == id);

                return records.GroupBy(e => e.PreviousQuotationId).Select(eGroup => eGroup.OrderByDescending(e => e.CreatedOn).FirstOrDefault()).OrderByDescending(e => e.CreatedOn).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Quotation> GetAllTaskListSuperAdmin()
        {

            try
            {
                var records = Select();

                return records.GroupBy(e => e.PreviousQuotationId).Select(eGroup => eGroup.OrderByDescending(e => e.CreatedOn).FirstOrDefault()).OrderByDescending(e => e.CreatedOn).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Quotation> GetConfirmedQuotations()
        {
            try
            {
                var records = Select();

                return records.Where(e => e.Status == "Confirmed").GroupBy(e => e.PreviousQuotationId).Select(eGroup => eGroup.OrderByDescending(e => e.CreatedOn).FirstOrDefault()).OrderBy(e => e.QuotationID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<Quotation> GetPaged(string keyword, string status, List<int> QuotationId, List<int> salesId, int customerId, int page, int size)
        {
            try
            {
                var records = Select();

                if (!string.IsNullOrEmpty(keyword))
                {
                    records = records.Where(e => e.QuotationID.Contains(keyword) || e.Customer.CompanyName.Contains(keyword) || e.Customer.ContactPerson.Contains(keyword));
                }

                if (!string.IsNullOrEmpty(status))
                {
                    records = records.Where(e => e.Status.Contains(status));
                }

                if (QuotationId.Count > 0)
                {
                    records = records.Where(e => QuotationId.Contains((int)e.ID));
                }

                if(salesId.Count > 0)
                {
                    records = records.Where(e => salesId.Contains((int)e.SalesPersonId));
                }

                if(customerId > 0)
                {
                    records = records.Where(e => e.SalesPersonId == customerId);
                }

                return records.GroupBy(e => e.PreviousQuotationId).Select(eGroup => eGroup.OrderByDescending(e => e.CreatedOn).FirstOrDefault()).OrderByDescending(e => e.CreatedOn).ToPagedList(page, size);
                //return records.OrderByDescending(e => e.QuotationID).ThenBy(e => e.ContactPerson).ThenBy(e => e.CompanyName).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<Quotation> GetPagedCustomer(string keyword, string status, int customerId, int page, int size)
        {
            try
            {
                var records = Select();

                if (!string.IsNullOrEmpty(keyword))
                {
                    records = records.Where(e => e.QuotationID.Contains(keyword) || e.ContactPerson.Contains(keyword) || e.CompanyName.Contains(keyword));
                }

                records = records.Where(e => e.Status.Contains("Confirmed") || e.Status.Contains("Pending Customer Confirmation"));

                if (customerId > 0)
                {
                    records = records.Where(e => e.CustomerId == customerId);
                }

                return records.GroupBy(e => e.PreviousQuotationId).Select(eGroup => eGroup.OrderByDescending(e => e.CreatedOn).FirstOrDefault()).OrderByDescending(e => e.CreatedOn).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<Quotation> GetPreviousQuotationPaged(int id, int page, int size)
        {
            try
            {
                var records = Select();

                return records.OrderByDescending(e => e.QuotationID).ThenBy(e => e.ContactPerson).ThenBy(e => e.CompanyName).Where(e => e.PreviousQuotationId == id).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<Quotation> GetQuotationsDataPaged(string startDate, string endDate, int page, int size)
        {
            try
            {
                var records = Select();

                if (!string.IsNullOrEmpty(startDate))
                {
                    DateTime start = Convert.ToDateTime(startDate + " 00:00:00");

                    records = records.Where(e => e.CreatedOn >= start);
                }

                if (!string.IsNullOrEmpty(endDate))
                {
                    DateTime end = Convert.ToDateTime(endDate + " 23:59:59");

                    records = records.Where(e => e.CreatedOn <= end);
                }

                return records.OrderBy(e => e.QuotationID).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public Quotation GetSingle(int id)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.ID == id).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public IList<Quotation> GetLast()
        {
            try
            {
                var records = SelectAll();

                return records.Where(e => e.ID == e.PreviousQuotationId && e.Status != "Draft").OrderByDescending(e => e.ID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public bool Add(Quotation data)
        {
            try
            {
                data.CreatedOn = DateTime.Now;
                data.UpdatedOn = DateTime.Now;
                data.IsDeleted = "N";

                db.Quotations.Add(data);

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateData(int id, Quotation updateData)
        {
            try
            {
                Quotation data = db.Quotations.Find(id);
                data.QuotationID = updateData.QuotationID;
                data.CompanyName = updateData.CompanyName;
                data.HasGST = updateData.HasGST;
                data.CustomerId = updateData.CustomerId;
                data.ContactPerson = updateData.ContactPerson;
                data.Address = updateData.Address;
                data.Tel = updateData.Tel;
                data.Fax = updateData.Fax;
                data.PreparedById = updateData.PreparedById;
                data.SalesPersonType = updateData.SalesPersonType;
                data.SalesPersonId = updateData.SalesPersonId;
                data.QuotationDate = updateData.QuotationDate;
                data.SalesDocumentUploads = updateData.SalesDocumentUploads;
                data.Remarks = updateData.Remarks;
                data.PaymentTerms = updateData.PaymentTerms;
                data.Status = updateData.Status;
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool UpdatePreviousQuotationID(int id)
        {
            try
            {
                Quotation data = db.Quotations.Find(id);

                data.PreviousQuotationId = id;
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateQuotationApprovalStatus(int id, string status, string SalesDocumentUpload)
        {
            try
            {
                Quotation data = db.Quotations.Find(id);

                data.Status = status;
                data.SalesDocumentUploads = SalesDocumentUpload;
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                Quotation quotation = db.Quotations.Find(id);

                quotation.IsDeleted = "Y";
                quotation.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}