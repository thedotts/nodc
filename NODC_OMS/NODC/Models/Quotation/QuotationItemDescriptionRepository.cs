﻿using DataAccess;
using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public class QuotationItemDescriptionRepository : IQuotationItemDescriptionRepository
    {
        private NODC_OMSContext db;

        public QuotationItemDescriptionRepository()
        {
            db = new NODC_OMSContext();
        }

        public IQueryable<QuotationItemDescription> Select()
        {
            var result = from u in db.QuotationItemDescriptions select u;

            return result;
        }

        public IQueryable<QuotationItemDescription> SelectAll()
        {
            var result = from u in db.QuotationItemDescriptions select u;

            return result;
        }


        public IList<QuotationItemDescription> GetAll()
        {
            try
            {
                var records = Select();

                return records.OrderBy(e => e.QuotationId).ToList();
            }
            catch
            {
                throw;
            }
        }

        public bool Add(QuotationItemDescription data)
        {
            try
            {
                db.QuotationItemDescriptions.Add(data);
                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateData(int id, QuotationItemDescription updateData)
        {
            try
            {
                QuotationItemDescription data = db.QuotationItemDescriptions.Find(id);

                data.QuotationItemRemarks = updateData.QuotationItemRemarks;
                data.QuotationItemMgrComments = updateData.QuotationItemMgrComments;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public List<QuotationItemDescription> getAllRelatedIdGetAllRelatedQuotationId(int id)
        {
            try
            {
                var records = Select();

                return records.OrderBy(e => e.QuotationId == id).Where(e => e.QuotationId == id).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<QuotationItemDescription> GetPersonalAll(int id)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.QuotationId == id).OrderBy(e => e.QuotationId).ToList();
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                QuotationItemDescription quotationItemDescription = db.QuotationItemDescriptions.Find(id);

                db.QuotationItemDescriptions.Remove(quotationItemDescription);

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}