﻿using DataAccess.POCO;
using PagedList;
using System.Collections.Generic;

namespace NODC_OMS.Models
{
    public interface IQuotationCoBrokeRepository
    {
        IList<QuotationCoBroke> GetAll();

        bool Add(QuotationCoBroke data);

        List<QuotationCoBroke> GetAllRelatedQuotationId(int id);

        IList<QuotationCoBroke> GetPersonalAll(int id);

        bool Delete(int id);
    }
}