﻿using DataAccess.POCO;
using PagedList;
using System.Collections.Generic;

namespace NODC_OMS.Models
{
    public interface IQuotationItemDescriptionRepository
    {
        IList<QuotationItemDescription> GetAll();

        bool Add(QuotationItemDescription data);

        bool UpdateData(int id, QuotationItemDescription updateData);

        List<QuotationItemDescription> getAllRelatedIdGetAllRelatedQuotationId(int id);

        IList<QuotationItemDescription> GetPersonalAll(int id);

        bool Delete(int id);
    }
}