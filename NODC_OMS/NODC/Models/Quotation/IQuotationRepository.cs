﻿using DataAccess.POCO;
using PagedList;
using System.Collections.Generic;

namespace NODC_OMS.Models
{
    public interface IQuotationRepository
    {
        IList<Quotation> GetAll();

        List<Quotation> GetUserQuotations(int id);

        IList<Quotation> GetAllQuotations(string startDate, string endDate);

        IList<Quotation> GetAllLatestQuotationID();

        List<Quotation> getAllSameCustomerId(int id);

        IList<Quotation> GetAllTaskList(int id);

        IList<Quotation> GetAllTaskListSuperAdmin();

        IList<Quotation> GetConfirmedQuotations();

        List<Quotation> getAllRelatedIdGetAllRelatedQuotationId(int id);

        IPagedList<Quotation> GetPaged(string keyword, string status, List<int> QuotationId, List<int> salesId, int customerId, int page, int size);

        IPagedList<Quotation> GetPagedCustomer(string keyword, string status, int customerId, int page, int size);

        IPagedList<Quotation> GetPreviousQuotationPaged(int id, int page, int size);

        //Export Data Part Use
        IPagedList<Quotation> GetQuotationsDataPaged(string startDate, string endDate, int page, int size);

        Quotation GetSingle(int id);

        IList<Quotation> GetLast();

        bool Add(Quotation data);

        bool UpdateData(int id, Quotation updateData);

        bool UpdatePreviousQuotationID(int id);

        bool UpdateQuotationApprovalStatus(int id, string status, string SalesDocumentUpload);

        bool Delete(int id);
    }
}