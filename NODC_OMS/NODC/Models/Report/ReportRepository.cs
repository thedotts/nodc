﻿using DataAccess.Report;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models.Report
{
    public class ReportRepository : IReportRepository
    {

        public IPagedList<ExpenseReport> GetExpenseReportPaged(List<ExpenseReport> list, int page, int size)
        {
            try
            {
                return list.OrderByDescending(e => e.Date).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<CommissionReport> GetCommissionReportPaged(List<CommissionReport> list, int page, int size)
        {
            try
            {
                return list.OrderByDescending(e => e.Name).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<PayoutReport> GetPayoutReportPaged(List<PayoutReport> list, int page, int size)
        {
            try
            {
                return list.OrderByDescending(e => e.Name).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<ProjectFinancialReport> GetProjectFinancialReportPaged(List<ProjectFinancialReport> list, int page, int size)
        {
            try
            {
                return list.OrderByDescending(e => e.InvoiceID).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<ProjectFinancialReport.ExpenseReport> GetProjectExpenseReportPaged(List<ProjectFinancialReport.ExpenseReport> list, int page, int size)
        {
            try
            {
                return list.OrderByDescending(e => e.Date).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<RetainedAmountReport> GetRetainedAmountReportPaged(List<RetainedAmountReport> list, int page, int size)
        {
            try
            {
                return list.OrderByDescending(e => e.Name).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public static bool Contains(string source, string toCheck, StringComparison comp)
        {
            return source != null && toCheck != null && source.IndexOf(toCheck, comp) >= 0;
        }

    }
}