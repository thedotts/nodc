﻿using DataAccess.Report;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models.Report
{
    public interface IReportRepository
    {
        IPagedList<ExpenseReport> GetExpenseReportPaged(List<ExpenseReport> list, int page, int size);

        IPagedList<CommissionReport> GetCommissionReportPaged(List<CommissionReport> list, int page, int size);

        IPagedList<PayoutReport> GetPayoutReportPaged(List<PayoutReport> list, int page, int size);

        IPagedList<ProjectFinancialReport> GetProjectFinancialReportPaged(List<ProjectFinancialReport> list, int page, int size);

        IPagedList<ProjectFinancialReport.ExpenseReport> GetProjectExpenseReportPaged(List<ProjectFinancialReport.ExpenseReport> list, int page, int size);

        IPagedList<RetainedAmountReport> GetRetainedAmountReportPaged(List<RetainedAmountReport> list, int page, int size);
    }
}