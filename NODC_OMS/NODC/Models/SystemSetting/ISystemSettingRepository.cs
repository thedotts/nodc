﻿using DataAccess.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public interface ISystemSettingRepository
    {
        IList<SystemSetting> GetAll();

        SystemSetting GetSingle(int id);

        SystemSetting GetSingle(string code);

        string GetCodeValue(string code);

        bool Update(int id, string value);
    }
}