﻿using DataAccess;
using DataAccess.POCO;
using NODC_OMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public class SystemSettingRepository : ISystemSettingRepository
    {
        private NODC_OMSContext db;

        public SystemSettingRepository()
        {
            db = new NODC_OMSContext();
        }

        public IQueryable<SystemSetting> Select()
        {
            var result = from f in db.SystemSettings select f;

            return result;
        }

        public IList<SystemSetting> GetAll()
        {
            try
            {
                var records = Select();

                return records.ToList();
            }
            catch
            {
                throw;
            }
        }

        public SystemSetting GetSingle(int id)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.ID == id).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public SystemSetting GetSingle(string code)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.Code == code).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public string GetCodeValue(string code)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.Code == code).FirstOrDefault().Value;
            }
            catch
            {
                throw;
            }
        }

        public bool Update(int id, string value)
        {
            try
            {
                SystemSetting setting = db.SystemSettings.Find(id);

                setting.Value = value;
                setting.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}