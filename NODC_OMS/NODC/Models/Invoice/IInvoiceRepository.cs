﻿using DataAccess.POCO;
using PagedList;
using System.Collections.Generic;

namespace NODC_OMS.Models
{
    public interface IInvoiceRepository
    {
        IList<Invoice> GetAll(List<int> uplineId);

        //For Project Get Payment Summary Use
        IList<Invoice> getAllProjectPaymentSummary(int quotationID);

        List<Invoice> GetAllProjectRelatedInvoiceID(int quotationId);

        //For Export Data Download Use
        IList<Invoice> GetAllInvoices(string startDate, string endDate);

        IPagedList<Invoice> GetPaged(string keyword, string status, List<int> uplineId, int customerId, int page, int size);

        IPagedList<Invoice> GetInvoicesDataPaged(string startDate, string endDate, int page, int size);

        Invoice GetSingle(int id);

        Invoice GetLast();

        bool Add(Invoice data);

        bool Update(int id, Invoice updateData);

        bool Delete(int id);
    }
}