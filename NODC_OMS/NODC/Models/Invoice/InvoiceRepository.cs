﻿using DataAccess;
using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public class InvoiceRepository : IInvoiceRepository
    {
        private NODC_OMSContext db;

        public InvoiceRepository()
        {
            db = new NODC_OMSContext();
        }

        public IQueryable<Invoice> Select()
        {
            var result = from u in db.Invoices select u;

            return result.Where(e => e.IsDeleted == "N");
        }

        public IQueryable<Invoice> SelectAll()
        {
            var result = from u in db.Invoices select u;

            return result;
        }

        public IList<Invoice> GetAll(List<int> uplineId)
        {
            try
            {
                var records = Select();

                if (uplineId.Count > 0)
                {
                    records = records.Where(e => uplineId.Contains(e.PreparedById));
                }

                return records.OrderByDescending(e => e.Date).ToList();
            }
            catch
            {
                throw;
            }
        }

        //This is for Project Get Payment Summary Use
        public IList<Invoice> getAllProjectPaymentSummary(int quotationID)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.QuotationId == quotationID).OrderBy(e => e.InvoiceID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Invoice> GetAllProjectRelatedInvoiceID(int quotationId)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.QuotationId == quotationId).OrderBy(e => e.InvoiceID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Invoice> GetAllInvoices(string startDate, string endDate)
        {
            try
            {
                var records = SelectAll();

                if (!string.IsNullOrEmpty(startDate))
                {
                    DateTime start = Convert.ToDateTime(startDate + " 00:00:00");

                    records = records.Where(e => e.CreatedOn >= start);
                }

                if (!string.IsNullOrEmpty(endDate))
                {
                    DateTime end = Convert.ToDateTime(endDate + " 23:59:59");

                    records = records.Where(e => e.CreatedOn <= end);
                }

                return records.OrderBy(e => e.InvoiceID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<Invoice> GetPaged(string keyword, string status, List<int> uplineId, int customerId, int page, int size)
        {
            try
            {
                var records = Select();

                if (!string.IsNullOrEmpty(keyword))
                {
                    records = records.Where(e => e.InvoiceID.Contains(keyword) || e.ContactPerson.Contains(keyword));
                }

                if (status != "All")
                {
                    records = records.Where(e => e.Status.Contains(status));
                }

                if (uplineId.Count > 0)
                {
                    records = records.Where(e => uplineId.Contains(e.PreparedById));
                }

                if (customerId > 0)
                {
                    records = records.Where(e => e.CustomerId == customerId);
                }

                return records.OrderByDescending(e => e.Date).ThenBy(e => e.InvoiceID).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<Invoice> GetInvoicesDataPaged(string startDate, string endDate, int page, int size)
        {
            try
            {
                var records = Select();

                if (!string.IsNullOrEmpty(startDate))
                {
                    DateTime start = Convert.ToDateTime(startDate + " 00:00:00");

                    records = records.Where(e => e.CreatedOn >= start);
                }

                if (!string.IsNullOrEmpty(endDate))
                {
                    DateTime end = Convert.ToDateTime(endDate + " 23:59:59");

                    records = records.Where(e => e.CreatedOn <= end);
                }

                return records.OrderBy(e => e.InvoiceID).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public Invoice GetSingle(int id)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.ID == id).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Invoice GetLast()
        {
            try
            {
                var records = SelectAll();

                DateTime today = DateTime.Now;

                return records.Where(e => e.CreatedOn.Year == today.Year).OrderByDescending(e => e.ID).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public bool Add(Invoice data)
        {
            try
            {
                data.CreatedOn = DateTime.Now;
                data.UpdatedOn = DateTime.Now;
                data.IsDeleted = "N";

                db.Invoices.Add(data);

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Update(int id, Invoice updateData)
        {
            try
            {
                Invoice data = db.Invoices.Find(id);
                data.Company = updateData.Company;
                data.QuotationId = updateData.QuotationId;
                data.ProjectId = updateData.ProjectId;
                data.CustomerId = updateData.CustomerId;
                data.ContactPerson = updateData.ContactPerson;
                data.Date = updateData.Date;
                data.Remarks = updateData.Remarks;
                data.Notes = updateData.Notes;
                data.GST = updateData.GST;
                data.PaymentMade = updateData.PaymentMade;
                data.SubtotalAmount = updateData.SubtotalAmount;
                data.GSTAmount = updateData.GSTAmount;
                data.GrandTotalAmount = updateData.GrandTotalAmount;
                data.Status = updateData.Status;
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                Invoice invoice = db.Invoices.Find(id);

                invoice.IsDeleted = "Y";
                invoice.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}