﻿using DataAccess;
using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public class InvoiceItemDescriptionRepository : IInvoiceItemDescriptionRepository
    {

        private NODC_OMSContext db;

        public InvoiceItemDescriptionRepository()
        {
            db = new NODC_OMSContext();
        }

        public IQueryable<InvoiceItemDescription> Select()
        {
            var result = from u in db.InvoiceItemDescriptions select u;

            return result;
        }

        public IList<InvoiceItemDescription> GetAll(int invoiceId = 0)
        {
            try
            {
                var records = Select();

                if (invoiceId > 0)
                {
                    records = records.Where(e => e.InvoiceId == invoiceId);
                }

                return records.OrderBy(e => e.InvoiceId).ToList();
            }
            catch
            {
                throw;
            }
        }

        public InvoiceItemDescription GetSingle(int id)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.ID == id).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public bool Add(InvoiceItemDescription data)
        {
            try
            {
                db.InvoiceItemDescriptions.Add(data);

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Update(int id, InvoiceItemDescription updateData)
        {
            try
            {
                InvoiceItemDescription data = db.InvoiceItemDescriptions.Find(id);

                data.Description = updateData.Description;
                data.PercentageOfQuotation = updateData.PercentageOfQuotation;
                data.Amount = updateData.Amount;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                InvoiceItemDescription data = db.InvoiceItemDescriptions.Find(id);

                db.InvoiceItemDescriptions.Remove(data);

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}