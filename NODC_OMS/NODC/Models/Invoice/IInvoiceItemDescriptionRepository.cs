﻿using DataAccess.POCO;
using PagedList;
using System.Collections.Generic;

namespace NODC_OMS.Models
{
    public interface IInvoiceItemDescriptionRepository
    {
        IList<InvoiceItemDescription> GetAll(int invoiceId = 0);

        InvoiceItemDescription GetSingle(int id);

        bool Add(InvoiceItemDescription data);

        bool Update(int id, InvoiceItemDescription updateData);

        bool Delete(int id);
    }
}