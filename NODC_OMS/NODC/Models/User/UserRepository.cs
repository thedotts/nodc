﻿using DataAccess;
using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public class UserRepository : IUserRepository
    {
        private NODC_OMSContext db;

        public UserRepository()
        {
            db = new NODC_OMSContext();
        }

        public IQueryable<User> Select()
        {
            var result = from u in db.Users select u;

            return result.Where(e => e.IsDeleted == "N");
        }

        public IQueryable<User> SelectAll()
        {
            var result = from u in db.Users select u;

            return result;
        }

        public IList<User> GetAll(string role = null, string status = null)
        {
            try
            {
                var records = Select();

                if (!string.IsNullOrEmpty(role))
                {
                    records = records.Where(e => e.Role == role);
                }

                if (!string.IsNullOrEmpty(status))
                {
                    records = records.Where(e => e.Status == status);
                }

                return records.OrderBy(e => e.UserID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<User> GetAll(List<string> roles, string status = null)
        {
            try
            {
                var records = Select();

                if (roles.Count > 0)
                {
                    records = records.Where(e => roles.Contains(e.Role));
                }

                if (!string.IsNullOrEmpty(status))
                {
                    records = records.Where(e => e.Status == status);
                }

                return records.OrderBy(e => e.UserID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<User> GetAllSameUplineID(int id)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.UplineId == id && e.ID != id).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<User> GetDownlineUsers(int id, List<string> salesPositions)
        {
            try
            {
                var records = Select();

                if (salesPositions.Count > 0)
                {
                    records = records.Where(e => salesPositions.Contains(e.Position));
                }

                return records.Where(e => e.UplineId == id && e.ID != id).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<User> ListOwnAndDownline(int id)
        {
            try
            {
                var records = Select();

                if (id > 0)
                {
                    records = records.Where(e => e.ID == id || e.UplineId == id);
                }

                return records.ToList();

            }
            catch
            {
                throw;
            }
        }

        public IPagedList<User> GetPaged(string keyword, string status, int page, int size)
        {
            try
            {
                var records = Select();

                if (!string.IsNullOrEmpty(keyword))
                {
                    records = records.Where(e => e.UserID.Contains(keyword) || e.Name.Contains(keyword));
                }

                if (!string.IsNullOrEmpty(status))
                {
                    records = records.Where(e => e.Status.Contains(status));
                }

                return records.OrderByDescending(e => e.UserID).ThenBy(e => e.Name).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<User> GetUsersDataPaged(string startDate, string endDate, int page, int size)
        {
            try
            {
                var records = Select();

                if (!string.IsNullOrEmpty(startDate))
                {
                    DateTime start = Convert.ToDateTime(startDate + " 00:00:00");

                    records = records.Where(e => e.CreatedOn >= start);
                }

                if (!string.IsNullOrEmpty(endDate))
                {
                    DateTime end = Convert.ToDateTime(endDate + " 23:59:59");

                    records = records.Where(e => e.CreatedOn <= end);
                }

                return records.OrderBy(e => e.UserID).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public IList<User> GetAllUsers(string startDate, string endDate)
        {
            try
            {
                var records = SelectAll();

                if (!string.IsNullOrEmpty(startDate))
                {
                    DateTime start = Convert.ToDateTime(startDate + " 00:00:00");

                    records = records.Where(e => e.CreatedOn >= start);
                }

                if (!string.IsNullOrEmpty(endDate))
                {
                    DateTime end = Convert.ToDateTime(endDate + " 23:59:59");

                    records = records.Where(e => e.CreatedOn <= end);
                }

                return records.OrderBy(e => e.UserID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public User GetSingle(int id)
        {
            try
            {
                var records = SelectAll();

                return records.Where(e => e.ID == id).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public User GetLast()
        {
            try
            {
                var records = Select();

                return records.OrderByDescending(e => e.ID).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public User GetUniqueEmail(string email)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.Email == email && e.Status == "Active").FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public User GetPersonalUniqueEmail(int id,string email)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.Email == email && e.Status == "Active" && e.ID != id).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public bool Add(User data)
        {
            try
            {
                data.CreatedOn = DateTime.Now;
                data.UpdatedOn = DateTime.Now;
                data.IsDeleted = "N";

                db.Users.Add(data);

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Update(int id, User updateData)
        {
            try
            {
                User data = db.Users.Find(id);
                data.Password = updateData.Password;
                data.Status = updateData.Status;
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateData(int id, User updateData)
        {
            try
            {
                User data = db.Users.Find(id);

                data.LoginID = updateData.LoginID;
                data.Password = updateData.Password;
                data.Name = updateData.Name;
                data.Address = updateData.Address;
                data.Email = updateData.Email;
                data.Tel = updateData.Tel;
                data.Mobile = updateData.Mobile;
                data.BankName = updateData.BankName;
                data.BankAccNo = updateData.BankAccNo;
                data.Remarks = updateData.Remarks;
                data.Reminder = updateData.Reminder;
                data.IsAgencyLeader = updateData.IsAgencyLeader;
                data.UplineId = updateData.UplineId;
                data.Role = updateData.Role;
                data.Position = updateData.Position;
                data.PresetValue = updateData.PresetValue;
                data.TotalCompletedProfit = updateData.TotalCompletedProfit;
                data.Status = updateData.Status;
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool PromoteUser(int id, decimal totalCompletedProfit, string promotion)
        {
            try
            {
                User data = db.Users.Find(id);

                data.Position = promotion;
                data.TotalCompletedProfit = totalCompletedProfit;
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateAccountsUplineID(int id)
        {
            try
            {
                User data = db.Users.Find(id);
                data.UplineId = id;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                User user = db.Users.Find(id);

                user.IsDeleted = "Y";
                user.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public User FindLoginID(string loginID)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.LoginID == loginID).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public User FindPersonalLoginID(int id, string loginID)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.LoginID == loginID && e.ID != id).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public bool AssignResetPassword(int id, string token)
        {
            try
            {
                User data = db.Users.Find(id);
                data.ResetPasswordToken = token;
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}