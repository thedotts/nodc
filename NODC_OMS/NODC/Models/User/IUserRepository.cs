﻿using DataAccess.POCO;
using PagedList;
using System.Collections.Generic;

namespace NODC_OMS.Models
{
    public interface IUserRepository
    {

        IList<User> GetAll(string role = null, string status = null);

        IList<User> GetAll(List<string> roles, string status = null);

        List<User> GetAllSameUplineID(int id);

        List<User> GetDownlineUsers(int id, List<string> salesPositions);

        IList<User> ListOwnAndDownline(int id);

        IPagedList<User> GetPaged(string keyword, string status, int page, int size);

        IPagedList<User> GetUsersDataPaged(string startDate, string endDate, int page, int size);

        IList<User> GetAllUsers(string startDate, string endDate);

        User GetSingle(int id);

        User GetLast();

        User GetUniqueEmail(string email);

        User GetPersonalUniqueEmail(int id, string email);

        bool Add(User data);

        bool Update(int id, User updateData);

        bool UpdateData(int id, User updateData);

        bool PromoteUser(int id, decimal totalCompletedProfit, string promotion);

        bool UpdateAccountsUplineID(int id);

        bool Delete(int id);

        User FindLoginID(string loginID);

        User FindPersonalLoginID(int id, string loginID);

        bool AssignResetPassword(int id, string token);

    }
}