﻿using DataAccess;
using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public class PayoutRepository : IPayoutRepository
    {
        private NODC_OMSContext db;

        public PayoutRepository()
        {
            db = new NODC_OMSContext();
        }

        public IQueryable<Payout> Select()
        {
            var result = from p in db.Payouts where p.IsDeleted == "N" select p;

            return result;
        }

        public IQueryable<Payout> SelectAll()
        {
            var result = from p in db.Payouts select p;

            return result;
        }

        public IList<Payout> GetAll(int payToId, string userType)
        {
            try
            {
                var records = Select();

                if (payToId > 0)
                {
                    records = records.Where(e => e.PayToId == payToId);
                }

                if (!string.IsNullOrEmpty(userType))
                {
                    records = records.Where(e => e.UserType == userType);
                }

                return records.OrderBy(e => e.PayoutID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Payout> GetAllPayouts(string startDate, string endDate)
        {
            try
            {
                var records = SelectAll();

                if (!string.IsNullOrEmpty(startDate))
                {
                    DateTime start = Convert.ToDateTime(startDate + " 00:00:00");

                    records = records.Where(e => e.CreatedOn >= start);
                }

                if (!string.IsNullOrEmpty(endDate))
                {
                    DateTime end = Convert.ToDateTime(endDate + " 23:59:59");

                    records = records.Where(e => e.CreatedOn <= end);
                }

                return records.OrderBy(e => e.PayoutID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public Payout GetSingle(int id)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.ID == id).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Payout GetLast()
        {
            try
            {
                var records = Select();

                DateTime today = DateTime.Now;

                return records.Where(e => e.CreatedOn.Year == today.Year).OrderByDescending(e => e.ID).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<Payout> GetPaged(int payToId, string userType, int page, int pageSize)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.PayToId == payToId && e.UserType == userType).OrderBy(e => e.PayoutID).ToPagedList(page, pageSize);
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<Payout> GetPayoutsDataPaged(string startDate, string endDate, int page, int size)
        {
            try
            {
                var records = Select();

                if (!string.IsNullOrEmpty(startDate))
                {
                    DateTime start = Convert.ToDateTime(startDate + " 00:00:00");

                    records = records.Where(e => e.CreatedOn >= start);
                }

                if (!string.IsNullOrEmpty(endDate))
                {
                    DateTime end = Convert.ToDateTime(endDate + " 23:59:59");

                    records = records.Where(e => e.CreatedOn <= end);
                }

                return records.OrderBy(e => e.PayoutID).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public bool Add(Payout addData)
        {
            try
            {
                addData.CreatedOn = DateTime.Now;
                addData.UpdatedOn = DateTime.Now;
                addData.IsDeleted = "N";

                db.Payouts.Add(addData);

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Update(int id, Payout updateData)
        {
            try
            {
                Payout data = db.Payouts.Find(id);
                data.PayToId = updateData.PayToId;
                data.UserType = updateData.UserType;
                data.Period = updateData.Period;
                data.TotalCommissionAmount = updateData.TotalCommissionAmount;
                data.TotalRetainedAmount = updateData.TotalRetainedAmount;
                data.TotalEarlyPayoutAmount = updateData.TotalEarlyPayoutAmount;
                data.TotalPayoutAmount = updateData.TotalPayoutAmount;
                data.Status = updateData.Status;
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                Payout data = db.Payouts.Find(id);
                data.IsDeleted = "Y";
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}