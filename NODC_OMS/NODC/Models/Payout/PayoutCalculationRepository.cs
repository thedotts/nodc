﻿using DataAccess;
using DataAccess.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public class PayoutCalculationRepository : IPayoutCalculationRepository
    {
        private NODC_OMSContext db;

        public PayoutCalculationRepository()
        {
            db = new NODC_OMSContext();
        }

        public IQueryable<PayoutCalculation> Select()
        {
            var result = from p in db.PayoutCalculations select p;

            return result;
        }

        public IList<PayoutCalculation> GetAll(int payoutId = 0)
        {
            try
            {
                var records = Select();

                if (payoutId > 0)
                {
                    records = records.Where(e => e.PayoutId == payoutId);
                }

                return records.OrderBy(e => e.PayoutId).ToList();
            }
            catch
            {
                throw;
            }
        }

        public PayoutCalculation GetSingle(int id)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.ID == id).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public bool Add(PayoutCalculation addData)
        {
            try
            {
                db.PayoutCalculations.Add(addData);

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                PayoutCalculation data = db.PayoutCalculations.Find(id);

                db.PayoutCalculations.Remove(data);

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}