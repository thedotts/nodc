﻿using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public interface IPayoutRepository
    {
        IList<Payout> GetAll(int payToId, string userType);

        IList<Payout> GetAllPayouts(string startDate, string endDate);

        Payout GetSingle(int id);

        Payout GetLast();

        IPagedList<Payout> GetPaged(int payToId, string userType, int page, int pageSize);

        //For Export Data Module Use
        IPagedList<Payout> GetPayoutsDataPaged(string startDate, string endDate, int page, int size);

        bool Add(Payout addData);

        bool Update(int id, Payout updateData);

        bool Delete(int id);
    }
}