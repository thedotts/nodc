﻿using DataAccess;
using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public class EarlyPayoutRepository : IEarlyPayoutRepository
    {
        private NODC_OMSContext db;

        public EarlyPayoutRepository()
        {
            db = new NODC_OMSContext();
        }

        public IQueryable<EarlyPayout> Select()
        {
            var result = from u in db.EarlyPayouts select u;

            return result.Where(e => e.IsDeleted == "N");
        }

        public IQueryable<EarlyPayout> SelectAll()
        {
            var result = from u in db.EarlyPayouts select u;

            return result;
        }

        public IList<EarlyPayout> GetAll()
        {
            try
            {
                var records = Select();

                return records.OrderByDescending(e => e.PayoutDate).ThenBy(e => e.EarlyPayoutID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<EarlyPayout> GetAllEarlyPayouts(string startDate, string endDate)
        {
            try
            {
                var records = SelectAll();

                if (!string.IsNullOrEmpty(startDate))
                {
                    DateTime start = Convert.ToDateTime(startDate + " 00:00:00");

                    records = records.Where(e => e.CreatedOn >= start);
                }

                if (!string.IsNullOrEmpty(endDate))
                {
                    DateTime end = Convert.ToDateTime(endDate + " 23:59:59");

                    records = records.Where(e => e.CreatedOn <= end);
                }

                return records.OrderBy(e => e.EarlyPayoutID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<EarlyPayout> GetRelatedEarlyPayouts(int payToId, string userType, DateTime? date = null, string status = null)
        {
            try
            {
                var records = Select();

                if (date != null)
                {
                    DateTime start = new DateTime(Convert.ToDateTime(date).Year, Convert.ToDateTime(date).Month, 1, 0, 0, 0, 0).AddMonths(-1);
                    DateTime end = new DateTime(Convert.ToDateTime(date).Year, Convert.ToDateTime(date).Month, 1, 23, 59, 59, 999).AddDays(-1);

                    records = records.Where(e => e.PayoutDate >= start && e.PayoutDate <= end);
                }

                if (!string.IsNullOrEmpty(status))
                {
                    records = records.Where(e => e.Status == status);
                }

                return records.Where(e => e.PayToId == payToId && e.UserType == userType).ToList();
            }
            catch
            {
                throw;
            }
        }

        public EarlyPayout GetSingle(int id)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.ID == id).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public EarlyPayout GetLast()
        {
            try
            {
                var records = Select();

                DateTime today = DateTime.Now;

                return records.Where(e => e.CreatedOn.Year == today.Year).OrderByDescending(e => e.ID).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<EarlyPayout> GetPaged(string keyword, string status, string period, int page, int size)
        {
            try
            {
                var records = Select();

                if (!string.IsNullOrEmpty(keyword))
                {
                    records = records.Where(e => e.EarlyPayoutID.Contains(keyword) || e.User.UserID.Contains(keyword) || e.User.Name.Contains(keyword) || e.Customer.CustomerID.Contains(keyword) || e.Customer.CompanyName.Contains(keyword));
                }

                if (status != "All")
                {
                    records = records.Where(e => e.Status.Contains(status));
                }

                if (!string.IsNullOrEmpty(period))
                {
                    DateTime today = DateTime.Now;
                    DateTime start = DateTime.Now;
                    DateTime end = DateTime.Now;

                    if (period == "Current Month")
                    {
                        start = new DateTime(today.Year, today.Month, 1, 0, 0, 0, 0);
                        end = new DateTime(today.Year, today.Month + 1, 1, 23, 59, 59, 999).AddDays(-1);
                    }
                    else if (period == "Last Month")
                    {
                        start = new DateTime(today.Year, today.Month, 1, 0, 0, 0, 0).AddMonths(-1);
                        end = new DateTime(today.Year, today.Month, 1, 23, 59, 59, 999).AddDays(-1);
                    }
                    else if (period == "Last Quarter")
                    {

                    }

                    records = records.Where(e => e.PayoutDate >= start && e.PayoutDate <= end);
                }

                return records.OrderByDescending(e => e.PayoutDate).ThenBy(e => e.EarlyPayoutID).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<EarlyPayout> GetEarlyPayoutsDataPaged(string startDate, string endDate, int page, int size)
        {
            try
            {
                var records = Select();

                if (!string.IsNullOrEmpty(startDate))
                {
                    DateTime start = Convert.ToDateTime(startDate + " 00:00:00");

                    records = records.Where(e => e.CreatedOn >= start);
                }

                if (!string.IsNullOrEmpty(endDate))
                {
                    DateTime end = Convert.ToDateTime(endDate + " 23:59:59");

                    records = records.Where(e => e.CreatedOn <= end);
                }

                return records.OrderBy(e => e.EarlyPayoutID).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public bool Add(EarlyPayout addData)
        {
            try
            {
                addData.CreatedOn = DateTime.Now;
                addData.UpdatedOn = DateTime.Now;
                addData.IsDeleted = "N";

                db.EarlyPayouts.Add(addData);

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Update(int id, EarlyPayout updateData)
        {
            try
            {
                EarlyPayout data = db.EarlyPayouts.Find(id);
                data.ProjectId = updateData.ProjectId;
                data.PayToId = updateData.PayToId;
                data.UserType = updateData.UserType;
                data.PayoutDate = updateData.PayoutDate;
                data.Amount = updateData.Amount;
                data.Status = updateData.Status;
                data.Remarks = updateData.Remarks;
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool CompletePayout(int id)
        {
            try
            {
                EarlyPayout data = db.EarlyPayouts.Find(id);
                data.Status = "Completed";
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                EarlyPayout data = db.EarlyPayouts.Find(id);
                data.IsDeleted = "Y";
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}