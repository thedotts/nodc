﻿using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public interface IEarlyPayoutRepository
    {
        IList<EarlyPayout> GetAll();

        IList<EarlyPayout> GetAllEarlyPayouts(string startDate, string endDate);

        IList<EarlyPayout> GetRelatedEarlyPayouts(int payToId, string userType, DateTime? date = null, string status = null);

        EarlyPayout GetSingle(int id);

        EarlyPayout GetLast();

        IPagedList<EarlyPayout> GetPaged(string keyword, string status, string period, int page, int size);

        //For Export Data Module Use
        IPagedList<EarlyPayout> GetEarlyPayoutsDataPaged(string startDate, string endDate, int page, int size);

        bool Add(EarlyPayout addData);

        bool Update(int id, EarlyPayout updateData);

        bool CompletePayout(int id);

        bool Delete(int id);
    }
}