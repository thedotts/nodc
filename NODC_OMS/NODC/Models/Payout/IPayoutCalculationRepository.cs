﻿using DataAccess.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public interface IPayoutCalculationRepository
    {
        IList<PayoutCalculation> GetAll(int payoutId = 0);

        PayoutCalculation GetSingle(int id);

        bool Add(PayoutCalculation addData);

        bool Delete(int id);
    }
}