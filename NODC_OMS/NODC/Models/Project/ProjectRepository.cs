﻿using DataAccess;
using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public class ProjectRepository : IProjectRepository
    {
        private NODC_OMSContext db;

        public ProjectRepository()
        {
            db = new NODC_OMSContext();
        }

        public IQueryable<Project> Select()
        {
            var result = from u in db.Projects select u;

            return result.Where(e => e.IsDeleted == "N");
        }

        public IQueryable<Project> SelectAll()
        {
            var result = from u in db.Projects select u;

            return result;
        }

        public IList<Project> GetAll()
        {
            try
            {
                var records = Select();

                return records.OrderBy(e => e.ProjectID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Project> GetSalesReminder(int coordinatorId, string status, DateTime? projectDate)
        {
            try
            {
                var records = Select();

                if (coordinatorId > 0)
                {
                    records = records.Where(e => e.CoordinatorId == coordinatorId);
                }

                if (!string.IsNullOrEmpty(status))
                {
                    records = records.Where(e => e.Status == status);
                }

                if (projectDate != null)
                {
                    int year = projectDate.Value.Year;
                    int month = projectDate.Value.Month;

                    DateTime start = new DateTime(1, month, year);
                    DateTime end = start.AddMonths(1).AddMilliseconds(-1);

                    records = records.Where(e => e.Date >= start && e.Date <= end);
                }

                return records.OrderBy(e => e.ProjectTitle).ThenBy(e => e.ProjectID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Project> getOwnAndDownline(List<int> Coordinatorid)
        {
            try
            {
                var records = Select();

                if (Coordinatorid.Count > 0)
                {
                    records = records.Where(e => Coordinatorid.Contains((int)e.CoordinatorId));
                }

                return records.OrderBy(e => e.ProjectID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Project> GetAllProjects(string startDate, string endDate)
        {
            try
            {
                var records = SelectAll();

                if (!string.IsNullOrEmpty(startDate))
                {
                    DateTime start = Convert.ToDateTime(startDate + " 00:00:00");

                    records = records.Where(e => e.CreatedOn >= start);
                }

                if (!string.IsNullOrEmpty(endDate))
                {
                    DateTime end = Convert.ToDateTime(endDate + " 23:59:59");

                    records = records.Where(e => e.CreatedOn <= end);
                }

                return records.OrderBy(e => e.ProjectID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Project> GetCompletedProjects()
        {
            try
            {
                var records = Select();

                return records.Where(e => e.Status == "Completed").OrderBy(e => e.ProjectID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Project> GetAllQuotationRelatedProjectItem(int qid)
        {
            try
            {
                var records = SelectAll();

                return records.Where(e => e.QuotationReferenceId == qid).OrderBy(e => e.ProjectID).ToList();
            }
            catch
            {
                throw;
            }
        }

        //For Create New User get Total Completed Profit Used.
        public IList<Project> GetAllCompletedProject(int coordinatorId)
        {
            try
            {
                var records = Select();

                DateTime now = DateTime.Now;
                DateTime thisMonth = new DateTime(now.Year, now.Month, 1);
                DateTime lastMonth = thisMonth.AddMonths(-1);

                return records.Where(e => e.Status == "Completed").Where(e => e.CoordinatorId == coordinatorId).Where(e => lastMonth <= e.CompletedDate).Where(e => e.CompletedDate < thisMonth).OrderBy(e => e.ProjectID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Project> GetAllProjectRelatedQuotationID(int quotationId)
        {
            try
            {
                var records = SelectAll();

                return records.Where(e => e.QuotationReferenceId == quotationId).OrderBy(e => e.ProjectID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Project> GetAllRelatedCustomer(int id, DateTime? date = null)
        {
            try
            {
                var records = Select();

                if (date != null)
                {
                    DateTime start = new DateTime(Convert.ToDateTime(date).Year, Convert.ToDateTime(date).Month, 1, 0, 0, 0, 0).AddMonths(-1);
                    DateTime end = new DateTime(Convert.ToDateTime(date).Year, Convert.ToDateTime(date).Month, 1, 23, 59, 59, 999).AddDays(-1);

                    records = records.Where(e => e.Date >= start && e.Date <= end);
                }

                return records.Where(e => e.CustomerId == id).OrderBy(e => e.ProjectID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Project> GetAllRelatedSalesperson(int id, DateTime date)
        {
            try
            {
                var records = Select();

                DateTime start = new DateTime(date.Year, date.Month, 1, 0, 0, 0, 0).AddMonths(-1);
                DateTime end = new DateTime(date.Year, date.Month, 1, 23, 59, 59, 999).AddDays(-1);

                return records.Where(e => e.CoordinatorId == id && (e.Date >= start && e.Date <= end)).OrderBy(e => e.ProjectID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<Project> GetPaged(string keyword, string status, int CustomerId, List<int> salesId, int page, int size)
        {
            try
            {
                var records = Select();

                if (!string.IsNullOrEmpty(keyword))
                {
                    records = records.Where(e => e.ProjectID.Contains(keyword) || e.Customer.ContactPerson.Contains(keyword));
                }

                if (!string.IsNullOrEmpty(status))
                {
                    records = records.Where(e => e.Status.Contains(status));
                }

                if(CustomerId > 0)
                {
                    records = records.Where(e => e.CustomerId == CustomerId);
                }

                if (salesId.Count > 0)
                {
                    records = records.Where(e => salesId.Contains((int)e.CoordinatorId));
                }

                return records.OrderByDescending(e => e.ProjectID).ThenBy(e => e.Customer.ContactPerson).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<Project> GetProjectsDataPaged(string startDate, string endDate, int page, int size)
        {
            try
            {
                var records = Select();

                if (!string.IsNullOrEmpty(startDate))
                {
                    DateTime start = Convert.ToDateTime(startDate + " 00:00:00");

                    records = records.Where(e => e.CreatedOn >= start);
                }

                if (!string.IsNullOrEmpty(endDate))
                {
                    DateTime end = Convert.ToDateTime(endDate + " 23:59:59");

                    records = records.Where(e => e.CreatedOn <= end);
                }

                return records.OrderBy(e => e.ProjectID).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public Project GetSingle(int id)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.ID == id).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Project GetAllSingle(int id)
        {
            try
            {
                var records = SelectAll();

                return records.Where(e => e.ID == id).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Project GetLast()
        {
            try
            {
                var records = Select();

                return records.OrderByDescending(e => e.ID).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public bool Add(Project data)
        {
            try
            {
                data.CreatedOn = DateTime.Now;
                data.UpdatedOn = DateTime.Now;
                data.IsDeleted = "N";

                db.Projects.Add(data);

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Update(int id, Project updateData)
        {
            try
            {
                Project data = db.Projects.Find(id);
                data.QuotationReferenceId = updateData.QuotationReferenceId;
                data.CustomerId = updateData.CustomerId;
                data.Date = updateData.Date;
                data.CoordinatorId = updateData.CoordinatorId;
                data.ProjectTitle = updateData.ProjectTitle;
                data.ProjectDescription = updateData.ProjectDescription;
                data.ProjectType = updateData.ProjectType;
                data.PhotoUploads = updateData.PhotoUploads;
                data.FloorPlanUploads = updateData.FloorPlanUploads;
                data.ThreeDDrawingUploads = updateData.ThreeDDrawingUploads;
                data.AsBuildDrawingUploads = updateData.AsBuildDrawingUploads;
                data.SubmissionStageUploads = updateData.SubmissionStageUploads;
                data.OtherUploads = updateData.OtherUploads;
                data.Remarks = updateData.Remarks;
                data.Status = updateData.Status;
                data.CompletedDate = updateData.CompletedDate;
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                Project project = db.Projects.Find(id);

                project.IsDeleted = "Y";
                project.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Complete(int id)
        {
            try
            {
                Project project = db.Projects.Find(id);

                project.Status = "Completed";
                project.CompletedDate = DateTime.Now;
                project.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public IList<Project> GetCompletedProjects(DateTime date)
        {
            try
            {
                var records = Select();

                DateTime lastMonth = date.AddMonths(-1);
                DateTime start = new DateTime(lastMonth.Year, lastMonth.Month, 1, 0, 0, 0, 0);
                DateTime end = start.AddMonths(1).AddMilliseconds(-1);

                records = records.Where(e => e.Status == "Completed" && (e.CompletedDate >= start && e.CompletedDate <= end));

                return records.OrderByDescending(e => e.ProjectID).ToList();
            }
            catch
            {
                throw;
            }
        }
    }
}