﻿using DataAccess;
using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public class ProjectProjectExpensesRepository : IProjectProjectExpensesRepository
    {
        private NODC_OMSContext db;

        public ProjectProjectExpensesRepository()
        {
            db = new NODC_OMSContext();
        }

        public IQueryable<ProjectProjectExpenses> Select()
        {
            var result = from u in db.ProjectProjectExpenses select u;

            return result;
        }

        public IQueryable<ProjectProjectExpenses> SelectAll()
        {
            var result = from u in db.ProjectProjectExpenses select u;

            return result;
        }

        public IList<ProjectProjectExpenses> GetAll()
        {
            try
            {
                var records = Select();

                return records.OrderBy(e => e.ProjectId).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<ProjectProjectExpenses> GetPersonalAll(int id)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.ProjectId == id).OrderBy(e => e.ProjectId).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<ProjectProjectExpenses> GetPersonalAllReport(int id, DateTime DateRangeFrom, DateTime DateRangeTo, string GST)
        {
            try
            {
                var records = Select();

                if (GST == "Yes")
                {
                    records = records.Where(e => e.GST == "Yes");
                }
                else if(GST == "No")
                {
                    records = records.Where(e => e.GST == "No");
                }

                return records.Where(e => e.ProjectId == id && e.Date >= DateRangeFrom && e.Date <= DateRangeTo).OrderBy(e => e.ProjectId).ToList();
            }
            catch
            {
                throw;
            }
        }

        public bool Add(ProjectProjectExpenses data)
        {
            try
            {
                db.ProjectProjectExpenses.Add(data);
                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                ProjectProjectExpenses projectExpenses = db.ProjectProjectExpenses.Find(id);

                db.ProjectProjectExpenses.Remove(projectExpenses);

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}