﻿using DataAccess.POCO;
using PagedList;
using System.Collections.Generic;

namespace NODC_OMS.Models
{
    public interface IProjectEstimatorRepository
    {
        IList<ProjectEstimator> GetAll();

        IList<ProjectEstimator> GetAllSameProjectID(int id);

        ProjectEstimator GetExistingProjectExpensesItem(int projectid, string categoryName, string description);

        ProjectEstimator GetSingle(int id);

        bool Add(ProjectEstimator data);

        bool Update(int id, ProjectEstimator updateData);

        bool Delete(int id);
    }
}