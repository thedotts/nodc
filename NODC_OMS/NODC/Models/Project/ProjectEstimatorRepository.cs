﻿using DataAccess;
using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public class ProjectEstimatorRepository : IProjectEstimatorRepository
    {
        private NODC_OMSContext db;

        public ProjectEstimatorRepository()
        {
            db = new NODC_OMSContext();
        }

        public IQueryable<ProjectEstimator> Select()
        {
            var result = from u in db.ProjectEstimators select u;

            return result;
        }

        public IList<ProjectEstimator> GetAll()
        {
            try
            {
                var records = Select();

                return records.OrderBy(e => e.ProjectId).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<ProjectEstimator> GetAllSameProjectID(int id)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.ProjectId == id).OrderBy(e => e.ProjectId).ToList();
            }
            catch
            {
                throw;
            }
        }

        public ProjectEstimator GetExistingProjectExpensesItem(int projectid, string categoryName, string description)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.ProjectId == projectid && e.ProjectExpensesCategoryName == categoryName && e.ProjectExpensesDescription == description).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public ProjectEstimator GetSingle(int id)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.ID == id).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public bool Add(ProjectEstimator data)
        {
            try
            {
                db.ProjectEstimators.Add(data);
                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Update(int id, ProjectEstimator updateData)
        {
            try
            {
                ProjectEstimator data = db.ProjectEstimators.Find(id);

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                ProjectEstimator projectEstimator = db.ProjectEstimators.Find(id);

                db.ProjectEstimators.Remove(projectEstimator);

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}