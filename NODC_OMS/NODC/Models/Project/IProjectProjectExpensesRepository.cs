﻿using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;

namespace NODC_OMS.Models
{
    public interface IProjectProjectExpensesRepository
    {
        bool Add(ProjectProjectExpenses data);

        IList<ProjectProjectExpenses> GetAll();

        IList<ProjectProjectExpenses> GetPersonalAll(int id);

        IList<ProjectProjectExpenses> GetPersonalAllReport(int id, DateTime DateRangeFrom, DateTime DateRangeTo, string GST);

        bool Delete(int id);
    }
}