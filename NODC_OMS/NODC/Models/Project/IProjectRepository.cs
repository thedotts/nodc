﻿using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;

namespace NODC_OMS.Models
{
    public interface IProjectRepository
    {
        IList<Project> GetAll();

        IList<Project> GetSalesReminder(int coordinatorId, string status, DateTime? projectDate);

        List<Project> getOwnAndDownline(List<int> coordinatorid);

        IList<Project> GetAllProjects(string startDate, string endDate);

        IList<Project> GetCompletedProjects();

        IList<Project> GetAllQuotationRelatedProjectItem(int qid);

        IList<Project> GetAllCompletedProject(int coordinatorId);

        IList<Project> GetAllRelatedCustomer(int id, DateTime? date = null);

        IList<Project> GetAllRelatedSalesperson(int id, DateTime date);

        List<Project> GetAllProjectRelatedQuotationID(int quotationId);

        IPagedList<Project> GetPaged(string keyword, string status, int CustomerId, List<int> SalesId, int page, int size);

        IPagedList<Project> GetProjectsDataPaged(string startDate, string endDate, int page, int size);

        Project GetSingle(int id);

        Project GetAllSingle(int id);

        Project GetLast();

        bool Add(Project data);

        bool Update(int id, Project updateData);

        bool Delete(int id);

        bool Complete(int id);

        IList<Project> GetCompletedProjects(DateTime date);
    }
}