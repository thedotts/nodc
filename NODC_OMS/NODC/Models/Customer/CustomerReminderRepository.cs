﻿using DataAccess;
using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public class CustomerReminderRepository : ICustomerReminderRepository
    {
       private NODC_OMSContext db;

       public CustomerReminderRepository()
       {
            db = new NODC_OMSContext();
       }

       public IQueryable<CustomerReminder> Select()
       {
            var result = from u in db.CustomerReminders select u;

            return result;
       }

       public IList<CustomerReminder> GetAll()
       {
           try
           {
               var records = Select();

               return records.OrderBy(e => e.CustomerId).ToList();
           }
           catch
           {
               throw;
           }
       }

       public IList<CustomerReminder> GetPersonalAll(int id)
       {
           try
           {
               var records = Select();

               return records.Where(e => e.CustomerId == id).OrderBy(e => e.CustomerId).ToList();
           }
           catch
           {
               throw;
           }
       }

       public CustomerReminder GetSingle(int id)
       {
           try
           {
               var records = Select();

               return records.Where(e => e.ID == id).FirstOrDefault();
           }
           catch
           {
               throw;
           }
       }

       public bool Add(CustomerReminder data)
       {
           try
           {

               db.CustomerReminders.Add(data);
               db.SaveChanges();

               return true;
           }
           catch
           {
               throw;
           }
       }

        public bool Update(int id, CustomerReminder updateData)
        {
            try
            {
                CustomerReminder data = db.CustomerReminders.Find(id);

                data.CustomerId = updateData.CustomerId;
                data.Description = updateData.Description;
                data.Date = updateData.Date;
                data.Repeat = updateData.Repeat;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                CustomerReminder customerReminder = db.CustomerReminders.Find(id);

                db.CustomerReminders.Remove(customerReminder);

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}