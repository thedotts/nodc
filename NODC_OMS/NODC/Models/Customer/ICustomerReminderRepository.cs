﻿using DataAccess.POCO;
using PagedList;
using System.Collections.Generic;

namespace NODC_OMS.Models
{
    public interface ICustomerReminderRepository
    {
        IList<CustomerReminder> GetAll();

        IList<CustomerReminder> GetPersonalAll(int id);

        CustomerReminder GetSingle(int id);

        bool Add(CustomerReminder data);

        bool Update(int id, CustomerReminder updateData);

        bool Delete(int id);

    }
}