﻿using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;

namespace NODC_OMS.Models
{
    public interface ICustomerRepository
    {

        IList<Customer> GetAll();

        IList<Customer> GetCustomerEventsReminder(int assignId, List<string> status, DateTime? reminderDate);

        IList<Customer> GetActiveCustomers();

        IList<Customer> GetAllTaskList();

        IList<Customer> ViewDownline(int id);

        IList<Customer> GetCustomerDownlineWithUserType(int id, string uplineType);

        IList<Customer> GetCustomerDownline(int id);

        IPagedList<Customer> GetPaged(List<int> OwnAndUplineID, string keyword, string status, int page, int size);

        IPagedList<Customer> GetCustomersDataPaged(string startDate, string endDate, int page, int size);

        IList<Customer> GetAllCustomers(string startDate, string endDate);

        IList<Customer> GetAllCommissionReport(string agency, string startDate, string endDate);

        Customer GetSingle(int id);

        Customer GetSingleNoFilter(int id);

        Customer GetLast();

        Customer GetUniqueEmail(string email);

        bool Add(Customer data);

        bool Update(int id, Customer updateData);

        bool PromoteCustomerPosition(int id, decimal totalCompletedProfit, string position);

        bool UpdateData(int id, Customer updateData);

        bool Delete(int id);

        bool Active(int id);

        Customer FindLoginID(string loginID);

        Customer FindPersonalLoginID(int id, string loginID);

        bool AssignResetPassword(int id, string token);
    }
}