﻿using DataAccess;
using DataAccess.POCO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public class CustomerRepository : ICustomerRepository
    {
        private NODC_OMSContext db;

        public CustomerRepository()
        {
            db = new NODC_OMSContext();
        }

        public IQueryable<Customer> Select()
        {
            var result = from u in db.Customers select u;

            return result.Where(e => e.IsDeleted == "N");
        }

        public IQueryable<Customer> SelectAll()
        {
            var result = from u in db.Customers select u;

            return result;
        }

        public IList<Customer> GetAll()
        {
            try
            {
                var records = Select();

                return records.OrderBy(e => e.CustomerID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Customer> GetCustomerEventsReminder(int assignId, List<string> status, DateTime? reminderDate)
        {
            try
            {
                var records = Select();

                if (assignId > 0)
                {
                    records = records.Where(e => e.AssignToId == assignId);
                }

                if (status != null)
                {
                    if (status.Count > 0)
                    {
                        records = records.Where(e => status.Contains(e.Status));
                    }
                }

                if (reminderDate != null)
                {
                    DateTime date = Convert.ToDateTime(reminderDate);

                    records = records.Where(e => e.CustomerReminders.Where(f => f.Date <= date).ToList().Count > 0);
                }

                return records.OrderBy(e => e.CustomerID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Customer> GetActiveCustomers()
        {
            try
            {
                var records = Select();

                return records.Where(e => e.Status != "Suspend").OrderBy(e => e.CustomerID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Customer> GetAllTaskList()
        {
            try
            {
                var records = Select();

                records = records.Where(e => e.Status == "Lead");

                return records.OrderByDescending(e => e.CreatedOn).ThenBy(e => e.CustomerID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Customer> ViewDownline(int id)
        {
            try
            {
                var records = Select();

                records = records.Where(e => e.UplineUserType == "User" && e.UplineId == id);

                return records.ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Customer> GetCustomerDownline(int id)
        {
            try
            {
                var records = Select();

                records = records.Where(e => e.UplineId == id || e.AssignToId == id);

                return records.ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Customer> GetCustomerDownlineWithUserType(int id, string uplineType)
        {
            try
            {
                var records = Select();

                records = records.Where(e => e.UplineId == id && e.UplineUserType == uplineType);

                return records.ToList();
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<Customer> GetPaged(List<int> OwnAndUplineId, string keyword, string status, int page, int size)
        {
            try
            {
                var records = Select();

                if(OwnAndUplineId.Count > 0)
                {
                    //records = records.Where(e => e.AssignToId == id);
                    //records = records.Where(e => e.UplineId == id);
                    records = records.Where(e => OwnAndUplineId.Contains((int)e.UplineId) || OwnAndUplineId.Contains((int)e.AssignToId));
                }

                if (!string.IsNullOrEmpty(keyword))
                {
                    records = records.Where(e => e.CustomerID.Contains(keyword) || e.ContactPerson.Contains(keyword) || e.CompanyName.Contains(keyword));
                }

                if (!string.IsNullOrEmpty(status))
                {
                    records = records.Where(e => e.Status.Contains(status));
                }

                return records.OrderByDescending(e => e.CustomerID).ThenBy(e => e.ContactPerson).ThenBy(e => e.CompanyName).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public IPagedList<Customer> GetCustomersDataPaged(string startDate, string endDate, int page, int size)
        {
            try
            {
                var records = Select();

                if (!string.IsNullOrEmpty(startDate))
                {
                    DateTime start = Convert.ToDateTime(startDate + " 00:00:00");

                    records = records.Where(e => e.CreatedOn >= start);
                }

                if (!string.IsNullOrEmpty(endDate))
                {
                    DateTime end = Convert.ToDateTime(endDate + " 23:59:59");

                    records = records.Where(e => e.CreatedOn <= end);
                }

                return records.OrderBy(e => e.CustomerID).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public IList<Customer> GetAllCustomers(string startDate, string endDate)
        {
            try
            {
                var records = SelectAll();

                if (!string.IsNullOrEmpty(startDate))
                {
                    DateTime start = Convert.ToDateTime(startDate + " 00:00:00");

                    records = records.Where(e => e.CreatedOn >= start);
                }

                if (!string.IsNullOrEmpty(endDate))
                {
                    DateTime end = Convert.ToDateTime(endDate + " 23:59:59");

                    records = records.Where(e => e.CreatedOn <= end);
                }

                return records.OrderBy(e => e.CustomerID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IList<Customer> GetAllCommissionReport(string agency, string startDate, string endDate)
        {
            try
            {
                var records = SelectAll();

                if (!string.IsNullOrEmpty(startDate))
                {
                    DateTime start = Convert.ToDateTime(startDate);

                    records = records.Where(e => e.CreatedOn >= start);
                }

                if (!string.IsNullOrEmpty(endDate))
                {
                    DateTime end = Convert.ToDateTime(endDate);

                    records = records.Where(e => e.CreatedOn <= end);
                }

                return records.Where(e => e.Agency == agency).OrderBy(e => e.CustomerID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public Customer GetSingle(int id)
        {
            try
            {
               var records = Select();

               return records.Where(e => e.ID == id).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Customer GetSingleNoFilter(int id)
        {
            try
            {
                var records = SelectAll();

                return records.Where(e => e.ID == id).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Customer GetLast()
        {
            try
            {
                var records = Select();

                return records.OrderByDescending(e => e.ID).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Customer GetUniqueEmail(string email)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.Email == email && e.Status == "Active").FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public bool Add(Customer data)
        {
            try
            {
                data.CreatedOn = DateTime.Now;
                data.UpdatedOn = DateTime.Now;
                data.IsDeleted = "N";

                db.Customers.Add(data);

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Update(int id, Customer updateData)
        {
            try
            {
                Customer data = db.Customers.Find(id);
                data.Password = updateData.Password;
                data.Status = updateData.Status;
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool PromoteCustomerPosition(int id, decimal totalCompletedProfit, string position)
        {
            try
            {
                Customer data = db.Customers.Find(id);
                data.TotalCompletedProfit = totalCompletedProfit;
                data.Position = position;
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateData(int id, Customer updateData)
        {
            try
            {
                Customer data = db.Customers.Find(id);

                data.LoginID = updateData.LoginID;
                data.Password = updateData.Password;
                data.CompanyName = updateData.CompanyName;
                data.Agency = updateData.Agency;
                data.ContactPerson = updateData.ContactPerson;
                data.Address = updateData.Address;
                data.Email = updateData.Email;
                data.Tel = updateData.Tel;
                data.Fax = updateData.Fax;
                data.BankName = updateData.BankName;
                data.BankAccNo = updateData.BankAccNo;
                data.Remarks = updateData.Remarks;
                data.UplineUserType = updateData.UplineUserType;
                data.UplineId = updateData.UplineId;
                data.Position = updateData.Position;
                data.AssignToId = updateData.AssignToId;
                data.Status = updateData.Status;
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                Customer customer = db.Customers.Find(id);

                customer.IsDeleted = "Y";
                customer.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Active(int id)
        {
            try
            {
                Customer customer = db.Customers.Find(id);

                customer.Status = "Active";
                customer.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }

        public Customer FindLoginID(string LoginID)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.LoginID == LoginID).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Customer FindPersonalLoginID(int id, string loginID)
        {
            try
            {
                var records = Select();

                return records.Where(e => e.LoginID == loginID && e.ID != id).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public bool AssignResetPassword(int id, string token)
        {
            try
            {
                Customer data = db.Customers.Find(id);
                data.ResetPasswordToken = token;
                data.UpdatedOn = DateTime.Now;

                db.SaveChanges();

                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}