﻿using DataAccess;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public interface ITaskListLoadAllRepository
    {
        IPagedList<TaskListLoadAllRecord> GetPaged(List<TaskListLoadAllRecord> list, int page, int size);
    }
}