﻿using DataAccess;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NODC_OMS.Models
{
    public class TaskListLoadAllRepository : ITaskListLoadAllRepository
    {
        public IPagedList<TaskListLoadAllRecord> GetPaged(List<TaskListLoadAllRecord> list, int page, int size)
        {
            try
            {

                return list.OrderBy(e => e.ItemID).ThenBy(e => e.Date).ToPagedList(page, size);
            }
            catch
            {
                throw;
            }
        }

        public static bool Contains(string source, string toCheck, StringComparison comp)
        {
            return source != null && toCheck != null && source.IndexOf(toCheck, comp) >= 0;
        }
    }
}