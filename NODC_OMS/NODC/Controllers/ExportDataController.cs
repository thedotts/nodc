﻿using DataAccess;
using DataAccess.POCO;
using Newtonsoft.Json;
using NODC_OMS.Models;
using OfficeOpenXml;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NODC_OMS.Controllers
{

    [HandleError]
    [RedirecttingActionAdmin]
    public class ExportDataController : ControllerBase
    {
        private IUserRepository _usersModel;
        private ICustomerRepository _customersModel;
        private ICustomerReminderRepository _customerRemindersModel;
        private IQuotationRepository _quotationsModel;
        private IQuotationCoBrokeRepository _quotationCoBrokesModel;
        private IQuotationItemDescriptionRepository _quotationItemDescriptionsModel;
        private ISystemSettingRepository _settingsModel;
        private IProjectRepository _projectsModel;
        private IProjectProjectExpensesRepository _projectExpensesModel;
        private IInvoiceRepository _invoicesModel;
        private IInvoiceItemDescriptionRepository _invoiceItemDescriptionsModel;
        private IProjectEstimatorRepository _projectEstimatorsModel;
        private IPayoutRepository _payoutsModel;
        private IEarlyPayoutRepository _earlyPayoutsModel;
        private IPayoutCalculationRepository _payoutSummariesModel;

        // GET: Project
        public ExportDataController()
            : this(new UserRepository(), new CustomerRepository(), new CustomerReminderRepository(), new QuotationRepository(), new QuotationCoBrokeRepository(), new QuotationItemDescriptionRepository(), new SystemSettingRepository(), new ProjectRepository(), new ProjectProjectExpensesRepository(), new InvoiceRepository(), new InvoiceItemDescriptionRepository(), new ProjectEstimatorRepository(), new PayoutRepository(), new EarlyPayoutRepository(), new PayoutCalculationRepository())
        {

        }

        public ExportDataController(IUserRepository usersModel, ICustomerRepository customersModel, ICustomerReminderRepository customerRemindersModel, IQuotationRepository quotationsModel, IQuotationCoBrokeRepository quotationCoBrokesModel, IQuotationItemDescriptionRepository quotationItemDescriptionsModel, ISystemSettingRepository settingsModel, IProjectRepository projectsModel, IProjectProjectExpensesRepository projectExpensesModel, IInvoiceRepository invoicesModel, IInvoiceItemDescriptionRepository invoiceItemDescriptionsModel, IProjectEstimatorRepository projectEstimatorsModel, IPayoutRepository payoutsModel, IEarlyPayoutRepository earlyPayoutsModel, IPayoutCalculationRepository payoutSummariesModel)
        {
            _usersModel = usersModel;
            _settingsModel = settingsModel;
            _customersModel = customersModel;
            _customerRemindersModel = customerRemindersModel;
            _quotationsModel = quotationsModel;
            _quotationCoBrokesModel = quotationCoBrokesModel;
            _quotationItemDescriptionsModel = quotationItemDescriptionsModel;
            _projectsModel = projectsModel;
            _projectExpensesModel = projectExpensesModel;
            _invoicesModel = invoicesModel;
            _invoiceItemDescriptionsModel = invoiceItemDescriptionsModel;
            _projectEstimatorsModel = projectEstimatorsModel;
            _payoutsModel = payoutsModel;
            _earlyPayoutsModel = earlyPayoutsModel;
            _payoutSummariesModel = payoutSummariesModel;
        }

        // GET: ExportData
        public ActionResult Index()
        {
            if (Session["TableName"] != null)
            {
                Session.Remove("TableName");
            }

            if (Session["FromDate"] != null)
            {
                Session.Remove("FromDate");
            }

            if (Session["ToDate"] != null)
            {
                Session.Remove("ToDate");
            }

            return RedirectToAction("Options");
        }

        //GET: Options
        public ActionResult Options()
        {
            ViewData["TableName"] = "";

            if (Session["TableName"] != null)
            {
                ViewData["TableName"] = Session["TableName"].ToString();
            }

            ViewData["FromDate"] = DateTime.Now.ToString("dd/MM/yyyy");

            if (Session["FromDate"] != null)
            {
                ViewData["FromDate"] = Session["FromDate"];
            }

            ViewData["ToDate"] = DateTime.Now.ToString("dd/MM/yyyy");

            if (Session["ToDate"] != null)
            {
                ViewData["ToDate"] = Session["ToDate"];
            }

            Dropdown[] tableNameDDL = TableNameDDL();
            ViewData["TableNameDropdown"] = new SelectList(tableNameDDL, "val", "name", ViewData["TableName"].ToString());

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Options
        [HttpPost]
        public ActionResult Options(FormCollection form)
        {
            if (string.IsNullOrEmpty(form["TableName"]))
            {
                ModelState.AddModelError("TableName", "Table Name is required!");
            }

            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;

            if (!string.IsNullOrEmpty(form["FromDate"]))
            {
                try
                {
                    fromDate = Convert.ToDateTime(form["FromDate"].ToString());
                }
                catch
                {
                    ModelState.AddModelError("FromDate", "From Date is not valid!");
                }
            }

            if (!string.IsNullOrEmpty(form["ToDate"]))
            {
                try
                {
                    toDate = Convert.ToDateTime(form["ToDate"].ToString());
                }
                catch
                {
                    ModelState.AddModelError("ToDate", "To Date is not valid!");
                }
            }

            if (fromDate > toDate)
            {
                ModelState.AddModelError("ToDate", "To Date is earlier than From Date!");
            }

            if (ModelState.IsValid)
            {
                Session.Add("TableName", form["TableName"].ToString());
                Session.Add("FromDate", form["FromDate"].ToString());
                Session.Add("ToDate", form["ToDate"].ToString());

                if (Session["PageSize"] != null)
                {
                    Session.Remove("PageSize");
                }

                switch (form["TableName"].ToString())
                {
                    case "Customers": return RedirectToAction("Customers");
                    case "Users": return RedirectToAction("Users");
                    case "Quotations": return RedirectToAction("Quotations");
                    case "Projects": return RedirectToAction("Projects");
                    case "ProjectEstimators": return RedirectToAction("ProjectEstimators");
                    case "Invoices": return RedirectToAction("Invoices");
                    case "Payouts": return RedirectToAction("Payouts");
                    case "EarlyPayouts": return RedirectToAction("EarlyPayouts");

                    default: Session.Add("Result", "danger|Table Name not found!"); break;
                }
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            ViewData["TableName"] = form["TableName"].ToString();
            Session["TableName"] = form["TableName"].ToString();

            ViewData["FromDate"] = form["FromDate"].ToString();
            Session["FromDate"] = form["FromDate"].ToString();

            ViewData["ToDate"] = form["ToDate"].ToString();
            Session["ToDate"] = form["ToDate"].ToString();

            Dropdown[] tableNameDDL = TableNameDDL();
            ViewData["TableNameDropdown"] = new SelectList(tableNameDDL, "val", "name", ViewData["TableName"].ToString());

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Users
        public ActionResult Users(int page = 1)
        {
            int pageSize = 20;
            if (Session["PageSize"] != null)
            {
                pageSize = Convert.ToInt32(Session["PageSize"]);
            }
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            string from = "";
            if (Session["FromDate"] != null)
            {
                from = Session["FromDate"].ToString();
            }
            else
            {
                Session["FromDate"] = "";
            }

            string to = "";
            if (Session["ToDate"] != null)
            {
                to = Session["ToDate"].ToString();
            }
            else
            {
                Session["ToDate"] = "";
            }

            IPagedList<User> Users = _usersModel.GetUsersDataPaged(from, to, page, pageSize);
            ViewData["Users"] = Users;

            Dropdown[] pageSizeDDL = PageSizeDDL();
            ViewData["PageSizeDropdown"] = new SelectList(pageSizeDDL, "val", "name", pageSize.ToString());

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Customers
        public ActionResult Customers(int page = 1)
        {
            int pageSize = 20;
            if (Session["PageSize"] != null)
            {
                pageSize = Convert.ToInt32(Session["PageSize"]);
            }
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            string from = "";
            if (Session["FromDate"] != null)
            {
                from = Session["FromDate"].ToString();
            }
            else
            {
                Session["FromDate"] = "";
            }

            string to = "";
            if (Session["ToDate"] != null)
            {
                to = Session["ToDate"].ToString();
            }
            else
            {
                Session["ToDate"] = "";
            }

            IPagedList<Customer> Customers = _customersModel.GetCustomersDataPaged(from, to, page, pageSize);
            ViewData["Customers"] = Customers;

            Dropdown[] pageSizeDDL = PageSizeDDL();
            ViewData["PageSizeDropdown"] = new SelectList(pageSizeDDL, "val", "name", pageSize.ToString());

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Quotations
        public ActionResult Quotations(int page = 1)
        {
            int pageSize = 20;
            if (Session["PageSize"] != null)
            {
                pageSize = Convert.ToInt32(Session["PageSize"]);
            }
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            string from = "";
            if (Session["FromDate"] != null)
            {
                from = Session["FromDate"].ToString();
            }
            else
            {
                Session["FromDate"] = "";
            }

            string to = "";
            if (Session["ToDate"] != null)
            {
                to = Session["ToDate"].ToString();
            }
            else
            {
                Session["ToDate"] = "";
            }
            
            IPagedList<Quotation> Quotations = _quotationsModel.GetQuotationsDataPaged(from, to, page, pageSize);
            ViewData["Quotations"] = Quotations;

            Dropdown[] pageSizeDDL = PageSizeDDL();
            ViewData["PageSizeDropdown"] = new SelectList(pageSizeDDL, "val", "name", pageSize.ToString());

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Projects
        public ActionResult Projects(int page = 1)
        {
            int pageSize = 20;
            if (Session["PageSize"] != null)
            {
                pageSize = Convert.ToInt32(Session["PageSize"]);
            }
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            string from = "";
            if (Session["FromDate"] != null)
            {
                from = Session["FromDate"].ToString();
            }
            else
            {
                Session["FromDate"] = "";
            }

            string to = "";
            if (Session["ToDate"] != null)
            {
                to = Session["ToDate"].ToString();
            }
            else
            {
                Session["ToDate"] = "";
            }

            IPagedList<Project> Projects = _projectsModel.GetProjectsDataPaged(from, to, page, pageSize);
            ViewData["Projects"] = Projects;

            Dropdown[] pageSizeDDL = PageSizeDDL();
            ViewData["PageSizeDropdown"] = new SelectList(pageSizeDDL, "val", "name", pageSize.ToString());

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Project Estimators
        public ActionResult ProjectEstimators(int page = 1)
        {
            int pageSize = 20;
            if (Session["PageSize"] != null)
            {
                pageSize = Convert.ToInt32(Session["PageSize"]);
            }
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            string from = "";
            if (Session["FromDate"] != null)
            {
                from = Session["FromDate"].ToString();
            }
            else
            {
                Session["FromDate"] = "";
            }

            string to = "";
            if (Session["ToDate"] != null)
            {
                to = Session["ToDate"].ToString();
            }
            else
            {
                Session["ToDate"] = "";
            }

            IPagedList<Project> projects = _projectsModel.GetProjectsDataPaged(from, to, page, pageSize);
            ViewData["ProjectEstimators"] = projects;

            Dropdown[] pageSizeDDL = PageSizeDDL();
            ViewData["PageSizeDropdown"] = new SelectList(pageSizeDDL, "val", "name", pageSize.ToString());

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Invoices
        public ActionResult Invoices(int page = 1)
        {
            int pageSize = 20;
            if (Session["PageSize"] != null)
            {
                pageSize = Convert.ToInt32(Session["PageSize"]);
            }
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            string from = "";
            if (Session["FromDate"] != null)
            {
                from = Session["FromDate"].ToString();
            }
            else
            {
                Session["FromDate"] = "";
            }

            string to = "";
            if (Session["ToDate"] != null)
            {
                to = Session["ToDate"].ToString();
            }
            else
            {
                Session["ToDate"] = "";
            }

            IPagedList<Invoice> Invoices = _invoicesModel.GetInvoicesDataPaged(from, to, page, pageSize);
            ViewData["Invoices"] = Invoices;

            IList<QuotationItemDescription> quotationItemDescription = _quotationItemDescriptionsModel.GetAll();
            ViewData["QuotationItemDescriptions"] = quotationItemDescription;

            Dropdown[] pageSizeDDL = PageSizeDDL();
            ViewData["PageSizeDropdown"] = new SelectList(pageSizeDDL, "val", "name", pageSize.ToString());

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Payouts
        public ActionResult Payouts(int page = 1)
        {
            int pageSize = 20;
            if (Session["PageSize"] != null)
            {
                pageSize = Convert.ToInt32(Session["PageSize"]);
            }
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            string from = "";
            if (Session["FromDate"] != null)
            {
                from = Session["FromDate"].ToString();
            }
            else
            {
                Session["FromDate"] = "";
            }

            string to = "";
            if (Session["ToDate"] != null)
            {
                to = Session["ToDate"].ToString();
            }
            else
            {
                Session["ToDate"] = "";
            }

            IPagedList<Payout> Payouts = _payoutsModel.GetPayoutsDataPaged(from, to, page, pageSize);
            ViewData["Payouts"] = Payouts;

            Dropdown[] pageSizeDDL = PageSizeDDL();
            ViewData["PageSizeDropdown"] = new SelectList(pageSizeDDL, "val", "name", pageSize.ToString());

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: EarlyPayouts
        public ActionResult EarlyPayouts(int page = 1)
        {
            int pageSize = 20;
            if (Session["PageSize"] != null)
            {
                pageSize = Convert.ToInt32(Session["PageSize"]);
            }
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            string from = "";
            if (Session["FromDate"] != null)
            {
                from = Session["FromDate"].ToString();
            }
            else
            {
                Session["FromDate"] = "";
            }

            string to = "";
            if (Session["ToDate"] != null)
            {
                to = Session["ToDate"].ToString();
            }
            else
            {
                Session["ToDate"] = "";
            }

            IPagedList<EarlyPayout> EarlyPayouts = _earlyPayoutsModel.GetEarlyPayoutsDataPaged(from, to, page, pageSize);
            ViewData["EarlyPayouts"] = EarlyPayouts;

            Dropdown[] pageSizeDDL = PageSizeDDL();
            ViewData["PageSizeDropdown"] = new SelectList(pageSizeDDL, "val", "name", pageSize.ToString());

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: ExportUsersExcel
        public void ExportUsersExcel(string fromDate, string toDate)
        {
            IList<User> users = _usersModel.GetAllUsers(fromDate, toDate);

            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create Worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Users");

                //set first row name
                ws.Cells[1, 1].Value = "User ID";
                ws.Cells[1, 2].Value = "Login ID";
                ws.Cells[1, 3].Value = "Name";
                ws.Cells[1, 4].Value = "Address";
                ws.Cells[1, 5].Value = "Email";
                ws.Cells[1, 6].Value = "Tel";
                ws.Cells[1, 7].Value = "Mobile";
                ws.Cells[1, 8].Value = "Bank Name";
                ws.Cells[1, 9].Value = "Bank Account No";
                ws.Cells[1, 10].Value = "Remarks";
                ws.Cells[1, 11].Value = "Reminder";
                ws.Cells[1, 12].Value = "Is Agency Leader";
                ws.Cells[1, 13].Value = "Upline";
                ws.Cells[1, 14].Value = "Role";
                ws.Cells[1, 15].Value = "Position";
                ws.Cells[1, 16].Value = "Preset Value";
                ws.Cells[1, 17].Value = "Total Completed Profit";
                ws.Cells[1, 18].Value = "Status";
                ws.Cells[1, 19].Value = "Created On";
                ws.Cells[1, 20].Value = "Updated On";
                ws.Cells[1, 21].Value = "Is Deleted";

                int userRow = 2;

                foreach (User user in users)
                {
                    //Dump Data
                    ws.Cells[userRow, 1].Value = user.UserID;
                    ws.Cells[userRow, 2].Value = user.LoginID;
                    ws.Cells[userRow, 3].Value = user.Name;

                    if(string.IsNullOrEmpty(user.Address))
                    {
                        ws.Cells[userRow, 4].Value = "-";
                    }
                    else
                    {
                        ws.Cells[userRow, 4].Value = user.Address;
                    }

                    if(string.IsNullOrEmpty(user.Email))
                    {
                        ws.Cells[userRow, 5].Value = "-";
                    }
                    else
                    {
                        ws.Cells[userRow, 5].Value = user.Email;
                    }

                    ws.Cells[userRow, 6].Value = user.Tel;

                    if(string.IsNullOrEmpty(user.Mobile))
                    {
                        ws.Cells[userRow, 7].Value = "-";
                    }
                    else
                    {
                        ws.Cells[userRow, 7].Value = user.Mobile;
                    }

                    if (string.IsNullOrEmpty(user.BankName))
                    {
                        ws.Cells[userRow, 8].Value = "-";
                    }
                    else
                    {
                        ws.Cells[userRow, 8].Value = user.BankName;
                    }

                    if (string.IsNullOrEmpty(user.BankAccNo))
                    {
                        ws.Cells[userRow, 9].Value = "-";
                    }
                    else
                    {
                        ws.Cells[userRow, 9].Value = user.BankAccNo;
                    }

                    if (string.IsNullOrEmpty(user.Remarks))
                    {
                        ws.Cells[userRow, 10].Value = "-";
                    }
                    else
                    {
                        ws.Cells[userRow, 10].Value = user.Remarks;
                    }
                    
                    ws.Cells[userRow, 11].Value = user.Reminder;
                    ws.Cells[userRow, 12].Value = user.IsAgencyLeader;

                    if(user.Upline == null)
                    {
                        ws.Cells[userRow, 13].Value = "-";
                    }
                    else
                    {
                        //if no upline id.
                        if(user.ID == user.Upline.ID)
                        {
                            ws.Cells[userRow, 13].Value = "-";
                        }
                        else
                        {
                            ws.Cells[userRow, 13].Value = user.Upline.UserID + " - " + user.Upline.Name;
                        }
                        
                    }
                    
                    ws.Cells[userRow, 14].Value = user.Role;

                    if(string.IsNullOrEmpty(user.Position))
                    {
                        ws.Cells[userRow, 15].Value = "-";
                    }
                    else
                    {
                        ws.Cells[userRow, 15].Value = user.Position;
                    }
                   
                    ws.Cells[userRow, 16].Value = user.PresetValue.ToString("#,##0.00");
                    ws.Cells[userRow, 17].Value = user.TotalCompletedProfit.ToString("#,##0.00");
                    ws.Cells[userRow, 18].Value = user.Status;
                    ws.Cells[userRow, 19].Value = user.CreatedOn.ToString("dd/MM/yyyy HH:mm:ss");
                    ws.Cells[userRow, 20].Value = user.UpdatedOn.ToString("dd/MM/yyyy HH:mm:ss");
                    ws.Cells[userRow, 21].Value = user.IsDeleted;
                    userRow++;
                }

                ws.Cells[ws.Dimension.Address].AutoFitColumns();

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=users-" + DateTime.Now.ToString("yyyyMMddHHmm") + ".xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }

        //GET: ExportCustomersExcel
        public void ExportCustomersExcel(string fromDate, string toDate)
        {
            IList<Customer> customers = _customersModel.GetAllCustomers(fromDate, toDate);

            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create Worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Customers");

                //Create Worksheet 2
                ExcelWorksheet ws2 = pck.Workbook.Worksheets.Add("Customer Reminders");

                //set first row name
                ws.Cells[1, 1].Value = "Customer ID";
                ws.Cells[1, 2].Value = "Login ID";
                ws.Cells[1, 3].Value = "Company Name";
                ws.Cells[1, 4].Value = "Agency";
                ws.Cells[1, 5].Value = "Contact Person";
                ws.Cells[1, 6].Value = "Address";
                ws.Cells[1, 7].Value = "Email";
                ws.Cells[1, 8].Value = "Tel";
                ws.Cells[1, 9].Value = "Fax";
                ws.Cells[1, 10].Value = "Bank Name";
                ws.Cells[1, 11].Value = "Bank Acc No";
                ws.Cells[1, 12].Value = "Remarks";
                ws.Cells[1, 13].Value = "Upline";
                ws.Cells[1, 14].Value = "Position";
                ws.Cells[1, 15].Value = "Assign To";
                ws.Cells[1, 16].Value = "Total Completed Profit";
                ws.Cells[1, 17].Value = "Status";
                ws.Cells[1, 18].Value = "Created On";
                ws.Cells[1, 19].Value = "Updated On";
                ws.Cells[1, 20].Value = "Is Deleted";

                //set first row name (customer Reminder)
                ws2.Cells[1, 1].Value = "Customer ID";
                ws2.Cells[1, 2].Value = "Description";
                ws2.Cells[1, 3].Value = "Date";
                ws2.Cells[1, 4].Value = "Repeat";

                int customerRow = 2;
                int customerReminderRow = 2;

                foreach (Customer customer in customers)
                {
                    //Dump Data
                    ws.Cells[customerRow, 1].Value = customer.CustomerID;
                    ws.Cells[customerRow, 2].Value = customer.LoginID;

                    if(string.IsNullOrEmpty(customer.CompanyName))
                    {
                        ws.Cells[customerRow, 3].Value = "-";
                    }
                    else
                    {
                        ws.Cells[customerRow, 3].Value = customer.CompanyName;
                    }

                    ws.Cells[customerRow, 4].Value = customer.Agency;
                    ws.Cells[customerRow, 5].Value = customer.ContactPerson;

                    if (string.IsNullOrEmpty(customer.Address))
                    {
                        ws.Cells[customerRow, 6].Value = "-";
                    }
                    else
                    {
                        ws.Cells[customerRow, 6].Value = customer.Address;
                    }

                    if (string.IsNullOrEmpty(customer.Email))
                    {
                        ws.Cells[customerRow, 7].Value = "-";
                    }
                    else
                    {
                        ws.Cells[customerRow, 7].Value = customer.Email;
                    }

                    ws.Cells[customerRow, 8].Value = customer.Tel;

                    if (string.IsNullOrEmpty(customer.Fax))
                    {
                        ws.Cells[customerRow, 9].Value = "-";
                    }
                    else
                    {
                        ws.Cells[customerRow, 9].Value = customer.Fax;
                    }

                    if (string.IsNullOrEmpty(customer.BankName))
                    {
                        ws.Cells[customerRow, 10].Value = "-";
                    }
                    else
                    {
                        ws.Cells[customerRow, 10].Value = customer.BankName;
                    }

                    if (string.IsNullOrEmpty(customer.BankAccNo))
                    {
                        ws.Cells[customerRow, 11].Value = customer.BankAccNo;
                    }
                    else
                    {
                        ws.Cells[customerRow, 11].Value = customer.BankAccNo;
                    }

                    if (string.IsNullOrEmpty(customer.Remarks))
                    {
                        ws.Cells[customerRow, 12].Value = customer.Remarks;
                    }
                    else
                    {
                        ws.Cells[customerRow, 12].Value = customer.Remarks;
                    }

                    if(customer.UplineUserType == "C")
                    {
                        Customer getCustomerUpline = _customersModel.GetSingle(customer.UplineId);

                        ws.Cells[customerRow, 13].Value = getCustomerUpline.CustomerID + " - " + getCustomerUpline.ContactPerson;
                    }
                    else
                    {
                        User getUserUpline = _usersModel.GetSingle(customer.UplineId);

                        ws.Cells[customerRow, 13].Value = getUserUpline.UserID + " - " + getUserUpline.Name;
                    }

                    ws.Cells[customerRow, 14].Value = customer.Position;

                    User getAssignTo = _usersModel.GetSingle(customer.AssignToId);

                    ws.Cells[customerRow, 15].Value = getAssignTo.UserID + " - " + getAssignTo.Name;

                    ws.Cells[customerRow, 16].Value = customer.TotalCompletedProfit.ToString("#,##0.00");
                    ws.Cells[customerRow, 17].Value = customer.Status;
                    ws.Cells[customerRow, 18].Value = customer.CreatedOn.ToString("dd/MM/yyyy HH:mm:ss");
                    ws.Cells[customerRow, 19].Value = customer.UpdatedOn.ToString("dd/MM/yyyy HH:mm:ss");
                    ws.Cells[customerRow, 20].Value = customer.IsDeleted;
                    customerRow++;

                    IList<CustomerReminder> getCustomerReminder = _customerRemindersModel.GetPersonalAll(customer.ID);

                    foreach(CustomerReminder _getCustomerReminder in getCustomerReminder)
                    {
                        ws2.Cells[customerReminderRow, 1].Value = customer.CustomerID;
                        ws2.Cells[customerReminderRow, 2].Value = _getCustomerReminder.Description;
                        ws2.Cells[customerReminderRow, 3].Value = _getCustomerReminder.Date.ToString("dd/MM/yyyy");
                        ws2.Cells[customerReminderRow, 4].Value = _getCustomerReminder.Repeat;
                        customerReminderRow++;
                    }
                }

                ws.Cells[ws.Dimension.Address].AutoFitColumns();
                ws2.Cells[ws2.Dimension.Address].AutoFitColumns();

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=customers-" + DateTime.Now.ToString("yyyyMMddHHmm") + ".xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }

        //GET: ExportQuotationsExcel
        public void ExportQuotationsExcel(string fromDate, string toDate)
        {
            IList<Quotation> quotation = _quotationsModel.GetAllQuotations(fromDate, toDate);

            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create Worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Quotations");

                //Create Worksheet 2
                ExcelWorksheet ws2 = pck.Workbook.Worksheets.Add("Quotation Co-Broke");

                //Create Worksheet 3
                ExcelWorksheet ws3 = pck.Workbook.Worksheets.Add("Quotation Item Descriptions");

                //set first row name
                ws.Cells[1, 1].Value = "Quotation ID";
                ws.Cells[1, 2].Value = "Company Name";
                ws.Cells[1, 3].Value = "Customer";
                ws.Cells[1, 4].Value = "Contact Person";
                ws.Cells[1, 5].Value = "Address";
                ws.Cells[1, 6].Value = "Tel";
                ws.Cells[1, 7].Value = "Fax";
                ws.Cells[1, 8].Value = "Prepared By";
                ws.Cells[1, 9].Value = "Sales Person";
                ws.Cells[1, 10].Value = "Quotation Date";
                ws.Cells[1, 11].Value = "Sales Document Uploads";
                ws.Cells[1, 12].Value = "Remarks";
                ws.Cells[1, 13].Value = "Payment Terms";
                ws.Cells[1, 14].Value = "Status";
                ws.Cells[1, 15].Value = "Created On";
                ws.Cells[1, 16].Value = "Updated On";
                ws.Cells[1, 17].Value = "Is Deleted";

                //set first row name (quotation Co-Broke)
                ws2.Cells[1, 1].Value = "Quotation ID";
                ws2.Cells[1, 2].Value = "User";
                ws2.Cells[1, 3].Value = "% of Profit";

                //set first row name (quotation Item Description)
                ws3.Cells[1, 1].Value = "Quotation ID";
                ws3.Cells[1, 2].Value = "Category Name";
                ws3.Cells[1, 3].Value = "Description";
                ws3.Cells[1, 4].Value = "Qty";
                ws3.Cells[1, 5].Value = "Unit";
                ws3.Cells[1, 6].Value = "Rate";
                ws3.Cells[1, 7].Value = "Amount";
                ws3.Cells[1, 8].Value = "Est Cost";
                ws3.Cells[1, 9].Value = "Est Profit";
                ws3.Cells[1, 10].Value = "Remarks";
                ws3.Cells[1, 11].Value = "Mgr Comments";


                int quotationRow = 2;
                int quotationCoBrokeRow = 2;
                int quotationItemDescriptionRow = 2;

                foreach (Quotation _quotation in quotation)
                {
                    //Dump Data
                    ws.Cells[quotationRow, 1].Value = _quotation.QuotationID;

                    if (string.IsNullOrEmpty(_quotation.CompanyName))
                    {
                        ws.Cells[quotationRow, 2].Value = "-";
                    }
                    else
                    {
                        ws.Cells[quotationRow, 2].Value = _quotation.CompanyName;
                    }

                    if (_quotation.Customer != null)
                    {
                        if(!string.IsNullOrEmpty(_quotation.Customer.CompanyName))
                        {
                            ws.Cells[quotationRow, 3].Value = _quotation.Customer.CustomerID + " - " + _quotation.Customer.CompanyName;
                        }
                        else
                        {
                            ws.Cells[quotationRow, 3].Value = _quotation.Customer.CustomerID + " - " + _quotation.Customer.ContactPerson;
                        }

                        ws.Cells[quotationRow, 4].Value = _quotation.ContactPerson;

                        if (!string.IsNullOrEmpty(_quotation.Customer.Address))
                        {
                            ws.Cells[quotationRow, 5].Value = _quotation.Customer.Address;
                        }
                        else
                        {
                            ws.Cells[quotationRow, 5].Value = "-";
                        }

                        ws.Cells[quotationRow, 6].Value = _quotation.Customer.Tel;

                        if (!string.IsNullOrEmpty(_quotation.Customer.Fax))
                        {
                            ws.Cells[quotationRow, 7].Value = _quotation.Customer.Fax;
                        }
                        else
                        {
                            ws.Cells[quotationRow, 7].Value = "-";
                        }
                    }
                    else
                    {
                        ws.Cells[quotationRow, 3].Value = "-";
                        ws.Cells[quotationRow, 4].Value = "-";
                        ws.Cells[quotationRow, 5].Value = "-";
                        ws.Cells[quotationRow, 6].Value = "-";
                        ws.Cells[quotationRow, 7].Value = "-";
                    }

                    if (_quotation.PreparedBy != null)
                    {
                        ws.Cells[quotationRow, 8].Value = _quotation.PreparedBy.Name;
                    }
                    else
                    {
                        ws.Cells[quotationRow, 8].Value = "-";
                    }

                    if (_quotation.SalesPerson != null)
                    {
                        ws.Cells[quotationRow, 9].Value = _quotation.SalesPerson.UserID + "- " + _quotation.SalesPerson.Name;
                    }
                    else
                    {
                        ws.Cells[quotationRow, 9].Value = "-";
                    }

                    ws.Cells[quotationRow, 10].Value = Convert.ToDateTime(_quotation.QuotationDate).ToString("dd/MM/yyyy");

                    if (!string.IsNullOrEmpty(_quotation.SalesDocumentUploads))
                    {
                        ws.Cells[quotationRow, 11].Value = _quotation.SalesDocumentUploads;
                    }
                    else
                    {
                        ws.Cells[quotationRow, 11].Value = "-";
                    }

                    if (!string.IsNullOrEmpty(_quotation.Remarks))
                    {
                        ws.Cells[quotationRow, 12].Value = _quotation.Remarks;
                    }
                    else
                    {
                        ws.Cells[quotationRow, 12].Value = "-";
                    }

                    if (!string.IsNullOrEmpty(_quotation.PaymentTerms))
                    {
                        ws.Cells[quotationRow, 13].Value = _quotation.PaymentTerms;
                    }
                    else
                    {
                        ws.Cells[quotationRow, 13].Value = "-";
                    }

                    ws.Cells[quotationRow, 14].Value = _quotation.Status;
                    ws.Cells[quotationRow, 15].Value = _quotation.CreatedOn.ToString("dd/MM/yyyy HH:mm:ss");
                    ws.Cells[quotationRow, 16].Value = _quotation.UpdatedOn.ToString("dd/MM/yyyy HH:mm:ss");
                    ws.Cells[quotationRow, 17].Value = _quotation.IsDeleted;
                    quotationRow++;

                    IList<QuotationCoBroke> getCoBroke = _quotationCoBrokesModel.GetPersonalAll(_quotation.ID);

                    foreach (QuotationCoBroke _getCoBroke in getCoBroke)
                    {
                        ws2.Cells[quotationCoBrokeRow, 1].Value = _quotation.QuotationID;

                        if (_getCoBroke.User != null)
                        {
                            ws2.Cells[quotationCoBrokeRow, 2].Value = _getCoBroke.User.UserID + " - " + _getCoBroke.User.Name;
                        }
                        else
                        {
                            ws2.Cells[quotationCoBrokeRow, 2].Value = "-";
                        }

                        ws2.Cells[quotationCoBrokeRow, 3].Value = _getCoBroke.QuotationCoBrokePercentOfProfit + "%";
                        quotationCoBrokeRow++;
                    }

                    IList<QuotationItemDescription> getQuotationItemDescriptions = _quotationItemDescriptionsModel.GetPersonalAll(_quotation.ID);

                    foreach (QuotationItemDescription _getQuotationItemDescriptions in getQuotationItemDescriptions)
                    {
                        ws3.Cells[quotationItemDescriptionRow, 1].Value = _quotation.QuotationID;

                        if (!string.IsNullOrEmpty(_getQuotationItemDescriptions.CategoryName))
                        {
                            ws3.Cells[quotationItemDescriptionRow, 2].Value = _getQuotationItemDescriptions.CategoryName;
                        }
                        else
                        {
                            ws3.Cells[quotationItemDescriptionRow, 2].Value = "-";
                        }

                        if (!string.IsNullOrEmpty(_getQuotationItemDescriptions.QuotationItem_Description))
                        {
                            ws3.Cells[quotationItemDescriptionRow, 3].Value = _getQuotationItemDescriptions.QuotationItem_Description;
                        }
                        else
                        {
                            ws3.Cells[quotationItemDescriptionRow, 3].Value = "-";
                        }

                        ws3.Cells[quotationItemDescriptionRow, 4].Value = _getQuotationItemDescriptions.QuotationItemQty.ToString("#,##0.0");

                        if (!string.IsNullOrEmpty(_getQuotationItemDescriptions.QuotationItemUnit))
                        {
                            ws3.Cells[quotationItemDescriptionRow, 5].Value = _getQuotationItemDescriptions.QuotationItemUnit;
                        }
                        else
                        {
                            ws3.Cells[quotationItemDescriptionRow, 5].Value = "-";
                        }
                        
                        ws3.Cells[quotationItemDescriptionRow, 6].Value = _getQuotationItemDescriptions.QuotationItemRate.ToString("#,##0.00");
                        ws3.Cells[quotationItemDescriptionRow, 7].Value = _getQuotationItemDescriptions.QuotationItemAmount.ToString("#,##0.00");
                        ws3.Cells[quotationItemDescriptionRow, 8].Value = _getQuotationItemDescriptions.QuotationItemEstCost.ToString("#,##0.00");
                        ws3.Cells[quotationItemDescriptionRow, 9].Value = _getQuotationItemDescriptions.QuotationItemEstProfit.ToString("#,##0.00");

                        if (!string.IsNullOrEmpty(_getQuotationItemDescriptions.QuotationItemRemarks))
                        {
                            ws3.Cells[quotationItemDescriptionRow, 10].Value = _getQuotationItemDescriptions.QuotationItemRemarks;
                        }
                        else
                        {
                            ws3.Cells[quotationItemDescriptionRow, 10].Value = "-";
                        }

                        if (!string.IsNullOrEmpty(_getQuotationItemDescriptions.QuotationItemMgrComments))
                        {
                            ws3.Cells[quotationItemDescriptionRow, 11].Value = _getQuotationItemDescriptions.QuotationItemMgrComments;
                        }
                        else
                        {
                            ws3.Cells[quotationItemDescriptionRow, 11].Value = "-";
                        }
                        quotationItemDescriptionRow++;
                    }
                }

                ws.Cells[ws.Dimension.Address].AutoFitColumns();
                ws2.Cells[ws2.Dimension.Address].AutoFitColumns();
                ws3.Cells[ws3.Dimension.Address].AutoFitColumns();

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=quotations-" + DateTime.Now.ToString("yyyyMMddHHmm") + ".xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }

        //GET: ExportProjectsExcel
        public void ExportProjectsExcel(string fromDate, string toDate)
        {

            IList<Project> projects = _projectsModel.GetAllProjects(fromDate, toDate);

            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create Worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Projects");

                //Create Worksheet 2
                ExcelWorksheet ws2 = pck.Workbook.Worksheets.Add("Project Expenses");

                //set first row name
                ws.Cells[1, 1].Value = "Project ID";
                ws.Cells[1, 2].Value = "Company Name";
                ws.Cells[1, 3].Value = "Quotation Reference";
                ws.Cells[1, 4].Value = "Customer";
                ws.Cells[1, 5].Value = "Contact Person";
                ws.Cells[1, 6].Value = "Address";
                ws.Cells[1, 7].Value = "Tel";
                ws.Cells[1, 8].Value = "Fax";
                ws.Cells[1, 9].Value = "Prepared By";
                ws.Cells[1, 10].Value = "Date";
                ws.Cells[1, 11].Value = "Co-ordinator";
                ws.Cells[1, 12].Value = "Co-ordinator Email";
                ws.Cells[1, 13].Value = "Co-ordinator Tel";
                ws.Cells[1, 14].Value = "Project Title";
                ws.Cells[1, 15].Value = "Description";
                ws.Cells[1, 16].Value = "Type";
                ws.Cells[1, 17].Value = "Photos";
                ws.Cells[1, 18].Value = "Floor Plan";
                ws.Cells[1, 19].Value = "3D Drawings";
                ws.Cells[1, 20].Value = "As-built Drawings";
                ws.Cells[1, 21].Value = "Submission Stage";
                ws.Cells[1, 22].Value = "Others";
                ws.Cells[1, 23].Value = "Remarks";
                ws.Cells[1, 24].Value = "Status";
                ws.Cells[1, 25].Value = "Created On";
                ws.Cells[1, 26].Value = "Updated On";
                ws.Cells[1, 27].Value = "Is Deleted";

                //set first row name (project expenses)
                ws2.Cells[1, 1].Value = "Project ID";
                ws2.Cells[1, 2].Value = "Description";
                ws2.Cells[1, 3].Value = "Supplier Name";
                ws2.Cells[1, 4].Value = "Invoice No";
                ws2.Cells[1, 5].Value = "Date";
                ws2.Cells[1, 6].Value = "GST";
                ws2.Cells[1, 7].Value = "Cost (before GST)";
                ws2.Cells[1, 8].Value = "Status";

                int projectRow = 2;
                int projectExpensesRow = 2;

                foreach (Project _projects in projects)
                {
                    //Dump Data
                    ws.Cells[projectRow, 1].Value = _projects.ProjectID;
                    ws.Cells[projectRow, 2].Value = _projects.CompanyName;

                    if (_projects.Quotation != null)
                    {
                        ws.Cells[projectRow, 3].Value = _projects.Quotation.QuotationID;
                    }
                    else
                    {
                        ws.Cells[projectRow, 3].Value = "-";
                    }

                    if (_projects.Customer != null)
                    {
                        if (!string.IsNullOrEmpty(_projects.Customer.CompanyName))
                        {
                            ws.Cells[projectRow, 4].Value = _projects.Customer.CompanyName + " - " + _projects.Customer.CustomerID; 
                        }
                        else
                        {
                            ws.Cells[projectRow, 4].Value = _projects.Customer.ContactPerson + " - " + _projects.Customer.CustomerID;
                        }

                        ws.Cells[projectRow, 5].Value = _projects.Customer.ContactPerson;

                        if (!string.IsNullOrEmpty(_projects.Customer.Address))
                        {
                            ws.Cells[projectRow, 6].Value = _projects.Customer.Address;
                        }
                        else
                        {
                            ws.Cells[projectRow, 6].Value = "-";
                        }

                        ws.Cells[projectRow, 7].Value = _projects.Customer.Tel;

                        if (!string.IsNullOrEmpty(_projects.Customer.Fax))
                        {
                            ws.Cells[projectRow, 8].Value = _projects.Customer.Fax;
                        }
                        else
                        {
                            ws.Cells[projectRow, 8].Value = "-";
                        }
                    }
                    else
                    {
                        ws.Cells[projectRow, 4].Value = "-";
                        ws.Cells[projectRow, 5].Value = "-";
                        ws.Cells[projectRow, 6].Value = "-";
                        ws.Cells[projectRow, 7].Value = "-";
                        ws.Cells[projectRow, 8].Value = "-";
                    }

                    if (_projects.PreparedBy != null)
                    {
                        ws.Cells[projectRow, 9].Value = _projects.PreparedBy.Name;
                    }
                    else
                    {
                        ws.Cells[projectRow, 9].Value = "-";
                    }

                    ws.Cells[projectRow, 10].Value = _projects.Date.ToString("dd/MM/yyyy");

                    if (_projects.Coordinator != null)
                    {
                        ws.Cells[projectRow, 11].Value = _projects.Coordinator.Name;

                        if (!string.IsNullOrEmpty(_projects.Coordinator.Email))
                        {
                            ws.Cells[projectRow, 12].Value = _projects.Coordinator.Email;
                        }
                        else
                        {
                            ws.Cells[projectRow, 12].Value = "-";
                        }
                        
                        ws.Cells[projectRow, 13].Value = _projects.Coordinator.Tel;
                    }
                    else
                    {
                        ws.Cells[projectRow, 11].Value = "-";
                        ws.Cells[projectRow, 12].Value = "-";
                        ws.Cells[projectRow, 13].Value = "-";
                    }

                    ws.Cells[projectRow, 14].Value = _projects.ProjectTitle;

                    if (!string.IsNullOrEmpty(_projects.ProjectDescription))
                    {
                        ws.Cells[projectRow, 15].Value = _projects.ProjectDescription;
                    }
                    else
                    {
                        ws.Cells[projectRow, 15].Value = "-";
                    }

                    ws.Cells[projectRow, 16].Value = _projects.ProjectType;

                    if (!string.IsNullOrEmpty(_projects.PhotoUploads))
                    {
                        ws.Cells[projectRow, 17].Value = _projects.PhotoUploads;
                    }
                    else
                    {
                        ws.Cells[projectRow, 17].Value = "-";
                    }

                    if (!string.IsNullOrEmpty(_projects.FloorPlanUploads))
                    {
                        ws.Cells[projectRow, 18].Value = _projects.FloorPlanUploads;
                    }
                    else
                    {
                        ws.Cells[projectRow, 18].Value = "-";
                    }

                    if (!string.IsNullOrEmpty(_projects.ThreeDDrawingUploads))
                    {
                        ws.Cells[projectRow, 19].Value = _projects.ThreeDDrawingUploads;
                    }
                    else
                    {
                        ws.Cells[projectRow, 19].Value = "-";
                    }

                    if (!string.IsNullOrEmpty(_projects.AsBuildDrawingUploads))
                    {
                        ws.Cells[projectRow, 20].Value = _projects.AsBuildDrawingUploads;
                    }
                    else
                    {
                        ws.Cells[projectRow, 20].Value = "-";
                    }

                    if (!string.IsNullOrEmpty(_projects.SubmissionStageUploads))
                    {
                        ws.Cells[projectRow, 21].Value = _projects.SubmissionStageUploads;
                    }
                    else
                    {
                        ws.Cells[projectRow, 21].Value = "-";
                    }

                    if (!string.IsNullOrEmpty(_projects.OtherUploads))
                    {
                        ws.Cells[projectRow, 22].Value = _projects.OtherUploads;
                    }
                    else
                    {
                        ws.Cells[projectRow, 22].Value = "-";
                    }

                    if (!string.IsNullOrEmpty(_projects.Remarks))
                    {
                        ws.Cells[projectRow, 23].Value = _projects.Remarks;
                    }
                    else
                    {
                        ws.Cells[projectRow, 23].Value = "-";
                    }

                    ws.Cells[projectRow, 24].Value = _projects.Status;
                    ws.Cells[projectRow, 25].Value = _projects.CreatedOn.ToString("dd/MM/yyyy HH:mm:ss");
                    ws.Cells[projectRow, 26].Value = _projects.UpdatedOn.ToString("dd/MM/yyyy HH:mm:ss");
                    ws.Cells[projectRow, 27].Value = _projects.IsDeleted;
                    projectRow++;

                    IList<ProjectProjectExpenses> getProjectExpenses = _projectExpensesModel.GetPersonalAll(_projects.ID);

                    foreach (ProjectProjectExpenses _getProjectExpenses in getProjectExpenses)
                    {
                        ws2.Cells[projectExpensesRow, 1].Value = _projects.ProjectID;
                        ws2.Cells[projectExpensesRow, 2].Value = _getProjectExpenses.Description;
                        ws2.Cells[projectExpensesRow, 3].Value = _getProjectExpenses.SupplierName;
                        ws2.Cells[projectExpensesRow, 4].Value = _getProjectExpenses.InvoiceNo;
                        ws2.Cells[projectExpensesRow, 5].Value = _getProjectExpenses.Date.ToString("dd/MM/yyyy");
                        ws2.Cells[projectExpensesRow, 6].Value = _getProjectExpenses.GST;
                        ws2.Cells[projectExpensesRow, 7].Value = _getProjectExpenses.Cost.ToString("#,##0.00");
                        ws2.Cells[projectExpensesRow, 8].Value = _getProjectExpenses.Status;
                        projectExpensesRow++;
                    }
                }

                ws.Cells[ws.Dimension.Address].AutoFitColumns();
                ws2.Cells[ws2.Dimension.Address].AutoFitColumns();

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=projects-" + DateTime.Now.ToString("yyyyMMddHHmm") + ".xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }

        //GET: ExportInvoicesExcel
        public void ExportInvoicesExcel(string fromDate, string toDate)
        {

            IList<Invoice> invoices = _invoicesModel.GetAllInvoices(fromDate, toDate);

            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create Worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Invoices");

                //Create Worksheet 2
                ExcelWorksheet ws2 = pck.Workbook.Worksheets.Add("Invoice Item Descriptions");

                //set first row name
                ws.Cells[1, 1].Value = "Invoice ID";
                ws.Cells[1, 2].Value = "Quotation";
                ws.Cells[1, 3].Value = "Company";
                ws.Cells[1, 4].Value = "Customer";
                ws.Cells[1, 5].Value = "Contact Person";
                ws.Cells[1, 6].Value = "Address";
                ws.Cells[1, 7].Value = "Tel";
                ws.Cells[1, 8].Value = "Fax";
                ws.Cells[1, 9].Value = "Prepared By";
                ws.Cells[1, 10].Value = "Date";
                ws.Cells[1, 11].Value = "Remarks";
                ws.Cells[1, 12].Value = "Note";
                ws.Cells[1, 13].Value = "GST";
                ws.Cells[1, 14].Value = "Total Quotation Amount";
                ws.Cells[1, 15].Value = "Payment Made";
                ws.Cells[1, 16].Value = "Status";
                ws.Cells[1, 17].Value = "Created On";
                ws.Cells[1, 18].Value = "Updated On";
                ws.Cells[1, 19].Value = "Is Deleted";

                //set first row name (invoice item descriptions)
                ws2.Cells[1, 1].Value = "Invoice ID";
                ws2.Cells[1, 2].Value = "Description";
                ws2.Cells[1, 3].Value = "% of Quotation";
                ws2.Cells[1, 4].Value = "Amount";

                int invoiceRow = 2;
                int invoiceItemDescriptionsRow = 2;

                foreach (Invoice _invoices in invoices)
                {
                    //Dump Data
                    ws.Cells[invoiceRow, 1].Value = _invoices.InvoiceID;

                    if (_invoices.Quotation != null)
                    {
                        if (!string.IsNullOrEmpty(_invoices.Quotation.CompanyName))
                        {
                            ws.Cells[invoiceRow, 2].Value = _invoices.Quotation.QuotationID + " - " + _invoices.Quotation.CompanyName;
                        }
                        else
                        {
                            ws.Cells[invoiceRow, 2].Value = _invoices.Quotation.QuotationID + " - " + _invoices.Quotation.ContactPerson;
                        }
                    }
                    else
                    {
                        ws.Cells[invoiceRow, 2].Value = "-";
                    }

                    if (!string.IsNullOrEmpty(_invoices.Company))
                    {
                        ws.Cells[invoiceRow, 3].Value = _invoices.Company;
                    }
                    else
                    {
                        ws.Cells[invoiceRow, 3].Value = "-";
                    }

                    if (_invoices.Customer != null)
                    {
                        if(!string.IsNullOrEmpty(_invoices.Customer.CompanyName))
                        {
                            ws.Cells[invoiceRow, 4].Value = _invoices.Customer.CustomerID + " - " + _invoices.Customer.CompanyName;
                        }
                        else
                        {
                            ws.Cells[invoiceRow, 4].Value = _invoices.Customer.CustomerID + " - " + _invoices.Customer.ContactPerson;
                        }

                        ws.Cells[invoiceRow, 5].Value = _invoices.ContactPerson;

                        if (!string.IsNullOrEmpty(_invoices.Customer.Address))
                        {
                            ws.Cells[invoiceRow, 6].Value = _invoices.Customer.Address;
                        }
                        else
                        {
                            ws.Cells[invoiceRow, 6].Value = "-";
                        }

                        ws.Cells[invoiceRow, 7].Value = _invoices.Customer.Tel;

                        if (!string.IsNullOrEmpty(_invoices.Customer.Fax))
                        {
                            ws.Cells[invoiceRow, 8].Value = _invoices.Customer.Fax;
                        }
                        else
                        {
                            ws.Cells[invoiceRow, 8].Value = "-";
                        }
                    }
                    else
                    {
                        ws.Cells[invoiceRow, 4].Value = "-";
                        ws.Cells[invoiceRow, 5].Value = "-";
                        ws.Cells[invoiceRow, 6].Value = "-";
                        ws.Cells[invoiceRow, 7].Value = "-";
                        ws.Cells[invoiceRow, 8].Value = "-";
                    }

                    if (_invoices.PreparedBy != null)
                    {
                        ws.Cells[invoiceRow, 9].Value = _invoices.PreparedBy.Name;
                    }
                    else
                    {
                        ws.Cells[invoiceRow, 9].Value = "-";
                    }

                    ws.Cells[invoiceRow, 10].Value = _invoices.Date.ToString("dd/MM/yyyy");

                    if (!string.IsNullOrEmpty(_invoices.Remarks))
                    {
                        ws.Cells[invoiceRow, 11].Value = _invoices.Remarks;
                    }
                    else
                    {
                        ws.Cells[invoiceRow, 11].Value = "-";
                    }

                    if (!string.IsNullOrEmpty(_invoices.Notes))
                    {
                        ws.Cells[invoiceRow, 12].Value = _invoices.Notes;
                    }
                    else
                    {
                        ws.Cells[invoiceRow, 12].Value = "-";
                    }

                    ws.Cells[invoiceRow, 13].Value = _invoices.GST;

                    IList<QuotationItemDescription> quotationItemDescription = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(_invoices.QuotationId);

                    decimal TotalQuotationAmount = 0;

                    foreach(QuotationItemDescription _quotationItemDescription in quotationItemDescription)
                    {
                        TotalQuotationAmount += _quotationItemDescription.QuotationItemAmount;
                    }

                    ws.Cells[invoiceRow, 14].Value = TotalQuotationAmount;

                    if (_invoices.PaymentMade != null)
                    {
                        ws.Cells[invoiceRow, 15].Value = _invoices.PaymentMade;
                    }
                    else
                    {
                        ws.Cells[invoiceRow, 15].Value = "-";
                    }

                    ws.Cells[invoiceRow, 16].Value = _invoices.Status;
                    ws.Cells[invoiceRow, 17].Value = _invoices.CreatedOn.ToString("dd/MM/yyyy HH:mm:ss");
                    ws.Cells[invoiceRow, 18].Value = _invoices.UpdatedOn.ToString("dd/MM/yyyy HH:mm:ss");
                    ws.Cells[invoiceRow, 19].Value = _invoices.IsDeleted;
                    invoiceRow++;

                    IList<InvoiceItemDescription> getInvoiceItemDescriptions = _invoiceItemDescriptionsModel.GetAll().Where(e => e.InvoiceId == _invoices.ID).ToList();

                    foreach (InvoiceItemDescription _getInvoiceItemDescriptions in getInvoiceItemDescriptions)
                    {
                        ws2.Cells[invoiceItemDescriptionsRow, 1].Value = _invoices.InvoiceID;
                        ws2.Cells[invoiceItemDescriptionsRow, 2].Value = _getInvoiceItemDescriptions.Description;

                        if (!string.IsNullOrEmpty(_getInvoiceItemDescriptions.PercentageOfQuotation))
                        {
                            ws2.Cells[invoiceItemDescriptionsRow, 3].Value = _getInvoiceItemDescriptions.PercentageOfQuotation;
                        }
                        else
                        {
                            ws2.Cells[invoiceItemDescriptionsRow, 3].Value = "-";
                        }

                        ws2.Cells[invoiceItemDescriptionsRow, 4].Value = _getInvoiceItemDescriptions.Amount.ToString("#,##0.00");
                        invoiceItemDescriptionsRow++;
                    }

                    ws2.Cells[invoiceItemDescriptionsRow, 3].Value = "SubTotal";
                    ws2.Cells[invoiceItemDescriptionsRow, 4].Value = _invoices.SubtotalAmount.ToString("#,##0.00");
                    invoiceItemDescriptionsRow++;
                    if (_invoices.GST == "Yes")
                    {
                        ws2.Cells[invoiceItemDescriptionsRow, 3].Value = "GST (7%)";
                    }
                    else
                    {
                        ws2.Cells[invoiceItemDescriptionsRow, 3].Value = "GST (0%)";
                    }
                    ws2.Cells[invoiceItemDescriptionsRow, 4].Value = _invoices.GSTAmount.ToString("#,##0.00");
                    invoiceItemDescriptionsRow++;
                    ws2.Cells[invoiceItemDescriptionsRow, 3].Value = "Grant Total";
                    ws2.Cells[invoiceItemDescriptionsRow, 4].Value = _invoices.GrandTotalAmount.ToString("#,##0.00");
                    invoiceItemDescriptionsRow++;
                    invoiceItemDescriptionsRow++;
                }

                ws.Cells[ws.Dimension.Address].AutoFitColumns();
                ws2.Cells[ws2.Dimension.Address].AutoFitColumns();

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=invoices-" + DateTime.Now.ToString("yyyyMMddHHmm") + ".xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }

        //GET: ExportPayoutsExcel
        public void ExportPayoutsExcel(string fromDate, string toDate)
        {
            IList<Payout> payouts = _payoutsModel.GetAllPayouts(fromDate, toDate);

            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create Worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Payouts");
                ExcelWorksheet ws2 = pck.Workbook.Worksheets.Add("Payout Summaries");

                //set first row name
                ws.Cells[1, 1].Value = "Payout ID";
                ws.Cells[1, 2].Value = "Pay To";
                ws.Cells[1, 3].Value = "User Type";
                ws.Cells[1, 4].Value = "Period";
                ws.Cells[1, 5].Value = "Total Commission Amount";
                ws.Cells[1, 6].Value = "Total Retained Amount";
                ws.Cells[1, 7].Value = "Total Early Payout Amount";
                ws.Cells[1, 8].Value = "Total Payout Amount";
                ws.Cells[1, 9].Value = "Status";
                ws.Cells[1, 10].Value = "Created On";
                ws.Cells[1, 11].Value = "Updated On";
                ws.Cells[1, 12].Value = "Is Deleted";

                //set first row name (Payout Summaries)
                ws2.Cells[1, 1].Value = "Payout ID";
                ws2.Cells[1, 2].Value = "Tier";
                ws2.Cells[1, 3].Value = "User";
                ws2.Cells[1, 4].Value = "User Type";
                ws2.Cells[1, 5].Value = "Revenue";
                ws2.Cells[1, 6].Value = "Payout Percentage";
                ws2.Cells[1, 7].Value = "Commission";

                int payoutRow = 2;
                int payoutSummariesRow = 2;

                foreach (Payout _payouts in payouts)
                {
                    //Dump Data
                    ws.Cells[payoutRow, 1].Value = _payouts.PayoutID;

                    if (_payouts.UserType == "Customer")
                    {
                        if(!string.IsNullOrEmpty(_payouts.Customer.CompanyName))
                        {
                            ws.Cells[payoutRow, 2].Value = _payouts.Customer.CustomerID + " - " + _payouts.Customer.CompanyName;
                        }
                        else
                        {
                            ws.Cells[payoutRow, 2].Value = _payouts.Customer.CustomerID + " - " + _payouts.Customer.ContactPerson;
                        }

                    }
                    else
                    {
                        //User
                        ws.Cells[payoutRow, 2].Value = _payouts.Salesperson.UserID + " - " + _payouts.Salesperson.Name;
                    }

                    ws.Cells[payoutRow, 3].Value = _payouts.UserType;
                    ws.Cells[payoutRow, 4].Value = _payouts.Period.ToString("dd/MM/yyyy");
                    ws.Cells[payoutRow, 5].Value = _payouts.TotalCommissionAmount.ToString("#,##0.00");
                    ws.Cells[payoutRow, 6].Value = _payouts.TotalRetainedAmount.ToString("#,##0.00");
                    ws.Cells[payoutRow, 7].Value = _payouts.TotalEarlyPayoutAmount.ToString("#,##0.00");
                    ws.Cells[payoutRow, 8].Value = _payouts.TotalPayoutAmount.ToString("#,##0.00");
                    ws.Cells[payoutRow, 9].Value = _payouts.Status;
                    ws.Cells[payoutRow, 10].Value = _payouts.CreatedOn.ToString("dd/MM/yyyy HH:mm:ss");
                    ws.Cells[payoutRow, 11].Value = _payouts.UpdatedOn.ToString("dd/MM/yyyy HH:mm:ss");
                    ws.Cells[payoutRow, 12].Value = _payouts.IsDeleted;
                    payoutRow++;

                    IList<PayoutCalculation> payoutSummaries = _payoutSummariesModel.GetAll().Where(e => e.PayoutId == _payouts.ID).ToList();

                    foreach (PayoutCalculation _payoutSummaries in payoutSummaries)
                    {
                        ws2.Cells[payoutSummariesRow, 1].Value = _payouts.PayoutID;
                        //ws2.Cells[payoutSummariesRow, 2].Value = _payoutSummaries.Tier;

                        if (_payoutSummaries.UserId != null)
                        {
                             if (_payoutSummaries.UserType == "Customer")
                             {
                                 if (!string.IsNullOrEmpty(_payoutSummaries.Customer.CompanyName))
                                 {
                                     ws2.Cells[payoutSummariesRow, 3].Value = _payoutSummaries.Customer.CustomerID + " - " + _payoutSummaries.Customer.CompanyName;
                                 }
                                 else
                                 {
                                     ws2.Cells[payoutSummariesRow, 3].Value = _payoutSummaries.Customer.CustomerID + " - " + _payoutSummaries.Customer.ContactPerson;
                                 }
                             }
                             else
                             {
                                 //User
                                 ws2.Cells[payoutSummariesRow, 3].Value = _payoutSummaries.Salesperson.UserID + " - " + _payoutSummaries.Salesperson.Name;
                             }
                        }
                        else
                        {
                             ws2.Cells[payoutSummariesRow, 3].Value  = "-";
                        }

                        if (!string.IsNullOrEmpty(_payoutSummaries.UserType))
                        {
                            ws2.Cells[payoutSummariesRow, 4].Value = _payoutSummaries.UserType;
                        }
                        else
                        {
                            ws2.Cells[payoutSummariesRow, 4].Value = "-";
                        }

                        ws2.Cells[payoutSummariesRow, 5].Value = _payoutSummaries.Revenue.ToString("#,##0.00");
                        ws2.Cells[payoutSummariesRow, 6].Value = _payoutSummaries.PayoutPercentage;
                        ws2.Cells[payoutSummariesRow, 7].Value = _payoutSummaries.Commission.ToString("#,##0.00");
                        payoutSummariesRow++;
                    }
                }

                ws.Cells[ws.Dimension.Address].AutoFitColumns();
                ws2.Cells[ws2.Dimension.Address].AutoFitColumns();

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=payouts-" + DateTime.Now.ToString("yyyyMMddHHmm") + ".xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }

        //GET: ExportEarlyPayoutsExcel
        public void ExportEarlyPayoutsExcel(string fromDate, string toDate)
        {
            IList<EarlyPayout> earlyPayouts = _earlyPayoutsModel.GetAllEarlyPayouts(fromDate, toDate);

            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create Worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Early Payouts");

                //set first row name
                ws.Cells[1, 1].Value = "Early Payout ID";
                ws.Cells[1, 2].Value = "Project ID";
                ws.Cells[1, 3].Value = "Pay To";
                ws.Cells[1, 4].Value = "User Type";
                ws.Cells[1, 5].Value = "Payout Date";
                ws.Cells[1, 6].Value = "Amount";
                ws.Cells[1, 7].Value = "Status";
                ws.Cells[1, 8].Value = "Remarks";
                ws.Cells[1, 9].Value = "Created On";
                ws.Cells[1, 10].Value = "Updated On";
                ws.Cells[1, 11].Value = "Is Deleted";

                int earlyPayoutRow = 2;

                foreach (EarlyPayout _earlyPayouts in earlyPayouts)
                {
                    //Dump Data
                    ws.Cells[earlyPayoutRow, 1].Value = _earlyPayouts.EarlyPayoutID;

                    if (_earlyPayouts.Project != null)
                    {
                        if (!string.IsNullOrEmpty(_earlyPayouts.Project.Customer.CompanyName))
                        {
                            ws.Cells[earlyPayoutRow, 2].Value = _earlyPayouts.Project.ProjectID + " - " + _earlyPayouts.Project.Customer.CompanyName;
                        }
                        else
                        {
                            ws.Cells[earlyPayoutRow, 2].Value = _earlyPayouts.Project.ProjectID + " - " + _earlyPayouts.Project.Customer.ContactPerson;
                        }
                    }
                    else
                    {
                        ws.Cells[earlyPayoutRow, 2].Value = "-";
                    }


                    if (_earlyPayouts.UserType == "Customer")
                    {
                        if (!string.IsNullOrEmpty(_earlyPayouts.Customer.CompanyName))
                        {
                            ws.Cells[earlyPayoutRow, 3].Value = _earlyPayouts.Customer.CustomerID + " - " + _earlyPayouts.Customer.CompanyName;
                        }
                        else
                        {
                            ws.Cells[earlyPayoutRow, 3].Value = _earlyPayouts.Customer.CustomerID + " - " + _earlyPayouts.Customer.ContactPerson;
                        }

                    }
                    else
                    {
                        //User
                        ws.Cells[earlyPayoutRow, 3].Value = _earlyPayouts.User.UserID + " - " + _earlyPayouts.User.Name;
                    }

                    ws.Cells[earlyPayoutRow, 4].Value = _earlyPayouts.UserType;
                    ws.Cells[earlyPayoutRow, 5].Value = _earlyPayouts.PayoutDate.ToString("dd/MM/yyyy");
                    ws.Cells[earlyPayoutRow, 6].Value = _earlyPayouts.Amount.ToString("#,##0.00");
                    ws.Cells[earlyPayoutRow, 7].Value = _earlyPayouts.Status;

                    if (!string.IsNullOrEmpty(_earlyPayouts.Remarks))
                    {
                        ws.Cells[earlyPayoutRow, 8].Value = _earlyPayouts.Remarks;
                    }
                    else
                    {
                        ws.Cells[earlyPayoutRow, 8].Value = "-";
                    }

                    ws.Cells[earlyPayoutRow, 9].Value = _earlyPayouts.CreatedOn.ToString("dd/MM/yyyy HH:mm:ss");
                    ws.Cells[earlyPayoutRow, 10].Value = _earlyPayouts.UpdatedOn.ToString("dd/MM/yyyy HH:mm:ss");
                    ws.Cells[earlyPayoutRow, 11].Value = _earlyPayouts.IsDeleted;
                    earlyPayoutRow++;
                }

                ws.Cells[ws.Dimension.Address].AutoFitColumns();

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=early-payouts-" + DateTime.Now.ToString("yyyyMMddHHmm") + ".xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }

        //GET: GetCustomerReminderInfo
        public ActionResult GetCustomerReminderInfo(int id)
        {
            if (id > 0)
            {
                IList<CustomerReminder> customerReminder = _customerRemindersModel.GetPersonalAll(id);
                ViewData["CustomerReminder"] = customerReminder;
            }
            else
            {
                Session.Add("Result", "danger|Customer record not found!");
                return RedirectToAction("Customers");
            }

            return View();
        }

        //GET: GetQuotationCoBrokeInfo
        public ActionResult GetQuotationCoBrokeInfo(int id)
        {
            if (id > 0)
            {
                IList<QuotationCoBroke> quotationCoBrokes = _quotationCoBrokesModel.GetPersonalAll(id);
                ViewData["QuotationCoBrokes"] = quotationCoBrokes;
            }
            else
            {
                Session.Add("Result", "danger|Quotation Item Descriptions record not found!");
                return RedirectToAction("Quotations");
            }

            return View();
        }

        //GET: GetQuotationItemDescriptionInfo
        public ActionResult GetQuotationItemDescriptionInfo(int id)
        {
            if (id > 0)
            {
                //Get Quotation Item Description

                //New Get Quotation Item Description
                List<QuotationItemDescription> quotationItemDescription = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(id);
                List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();

                //Loop Item Description Items
                IList<SystemSetting> listItem = _settingsModel.GetAll();
                SystemSetting CategoryList = listItem.Where(e => e.Code == "QUOTATION_CATEGORIES").FirstOrDefault();

                string[] categoryList = CategoryList.Value.Split('|');

                //Start
                int countItemDescription = 1;
                int resetNumber = 1;
                foreach (QuotationItemDescription _quotationItemDescription in quotationItemDescription)//14 items
                {
                    string ItemName = _quotationItemDescription.CategoryName;
                    int ItemQty = (int)_quotationItemDescription.QuotationItemQty;
                    decimal ItemRate = (decimal)_quotationItemDescription.QuotationItemRate;
                    decimal ItemEstCost = (decimal)_quotationItemDescription.QuotationItemEstCost;
                    bool IsBasic = false; //default set to false means it is not from db item.

                    List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                    if (ItemQty == null)
                    {
                        ItemQty = 0;
                    }

                    if (ItemRate == null)
                    {
                        ItemRate = 0;
                    }

                    if (ItemEstCost == null)
                    {
                        ItemEstCost = 0;
                    }


                    foreach (string _categoryList in categoryList)
                    {
                        if (_categoryList == _quotationItemDescription.CategoryName)
                        {
                            IsBasic = true;
                        }
                    }

                    if (quotationItemDescriptionBigTable.Count == 0)
                    {
                        resetNumber = 1;
                        ItemDescriptionRows.Add(new ItemDescriptionRow()
                        {
                            RowId = resetNumber,
                            Description = _quotationItemDescription.QuotationItem_Description,
                            Qty = Convert.ToString(_quotationItemDescription.QuotationItemQty),
                            Unit = _quotationItemDescription.QuotationItemUnit,
                            Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                            Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                            Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                            AdjCost = null,
                            Est_Profit = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstProfit).ToString("#,##0.00"),
                            Remarks = _quotationItemDescription.QuotationItemRemarks,
                            Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                        });

                        quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                        {
                            TableID = countItemDescription,
                            CategoryName = _quotationItemDescription.CategoryName,
                            IsBasic = IsBasic,
                            ItemDescriptionRows = ItemDescriptionRows
                        });
                        countItemDescription++;
                    }
                    else
                    {
                        if (quotationItemDescriptionBigTable.Where(e => e.CategoryName == _quotationItemDescription.CategoryName).FirstOrDefault() != null)
                        {
                            foreach (ItemDescriptionBigTable _loopBigTable in quotationItemDescriptionBigTable)
                            {
                                if (_loopBigTable.CategoryName == _quotationItemDescription.CategoryName)
                                {
                                    _loopBigTable.ItemDescriptionRows.Add(new ItemDescriptionRow()
                                    {
                                        RowId = resetNumber,
                                        Description = _quotationItemDescription.QuotationItem_Description,
                                        Qty = Convert.ToString(_quotationItemDescription.QuotationItemQty),
                                        Unit = _quotationItemDescription.QuotationItemUnit,
                                        Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                                        Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                                        Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                                        AdjCost = null,
                                        Est_Profit = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstProfit).ToString("#,##0.00"),
                                        Remarks = _quotationItemDescription.QuotationItemRemarks,
                                        Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                                    });
                                    break;
                                }
                            }
                        }
                        else
                        {
                            resetNumber = 1;
                            ItemDescriptionRows.Add(new ItemDescriptionRow()
                            {
                                RowId = resetNumber,
                                Description = _quotationItemDescription.QuotationItem_Description,
                                Qty = Convert.ToString(_quotationItemDescription.QuotationItemQty),
                                Unit = _quotationItemDescription.QuotationItemUnit,
                                Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                                Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                                Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                                AdjCost = null,
                                Est_Profit = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstProfit).ToString("#,##0.00"),
                                Remarks = _quotationItemDescription.QuotationItemRemarks,
                                Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                            });

                            quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                            {
                                TableID = countItemDescription,
                                CategoryName = _quotationItemDescription.CategoryName,
                                IsBasic = IsBasic,
                                ItemDescriptionRows = ItemDescriptionRows
                            });
                            countItemDescription++;
                        }
                    }
                    resetNumber++;
                    //End
                }

                ViewData["QuotationItemDescription"] = quotationItemDescriptionBigTable;
            }
            else
            {
                Session.Add("Result", "danger|Quotation Item Descriptions record not found!");
                return RedirectToAction("Quotations");
            }

            return View();
        }

        //GET: GetProjectExpensesInfo
        public ActionResult GetProjectExpensesInfo(int id)
        {
            if (id > 0)
            {
                IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetPersonalAll(id);
                ViewData["ProjectExpenses"] = projectExpenses;
            }
            else
            {
                Session.Add("Result", "danger|Project Expenses record not found!");
                return RedirectToAction("Projects");
            }

            return View();
        }

        //GET: GetProjectEstimatorsInfo
        public ActionResult GetProjectEstimatorsInfo(int id)
        {
            //Get Quotation Item Description

            Project project = _projectsModel.GetSingle(id);


            if (project != null)
            {

                Quotation quotation = _quotationsModel.GetSingle(project.QuotationReferenceId);

                //New Get Quotation Item Description
                List<QuotationItemDescription> quotationItemDescription = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(id);
                List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();

                IList<ProjectEstimator> projectEstimator = _projectEstimatorsModel.GetAllSameProjectID(project.ID);

                int countItemDescription = 1;
                int resetNumber = 1;

                //Test 
                foreach (QuotationItemDescription _quotationItemDescription in quotationItemDescription)
                {
                    ProjectEstimator getExistingItem = _projectEstimatorsModel.GetExistingProjectExpensesItem(project.ID, _quotationItemDescription.CategoryName, _quotationItemDescription.QuotationItem_Description);

                    List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                    string AdjCost = null;
                    decimal Profit = (decimal)_quotationItemDescription.QuotationItemAmount;

                    if (getExistingItem != null)
                    {
                        AdjCost = getExistingItem.ProjectExpensesAdjCost.ToString("#,##0.00");
                        Profit = (decimal)_quotationItemDescription.QuotationItemAmount - getExistingItem.ProjectExpensesAdjCost;
                    }

                    if (quotationItemDescriptionBigTable.Count == 0)
                    {

                        ItemDescriptionRows.Add(new ItemDescriptionRow()
                        {
                            RowId = resetNumber,
                            Description = _quotationItemDescription.QuotationItem_Description,
                            Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                            Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                            AdjCost = AdjCost,
                            Est_Profit = Profit.ToString("#,##0.00")
                        });

                        quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                        {
                            TableID = countItemDescription,
                            CategoryName = _quotationItemDescription.CategoryName,
                            ItemDescriptionRows = ItemDescriptionRows
                        });
                        countItemDescription++;
                    }
                    else
                    {
                        if (quotationItemDescriptionBigTable.Where(e => e.CategoryName == _quotationItemDescription.CategoryName).FirstOrDefault() != null)
                        {
                            foreach (ItemDescriptionBigTable _loopBigTable in quotationItemDescriptionBigTable)
                            {
                                if (_loopBigTable.CategoryName == _quotationItemDescription.CategoryName)
                                {
                                    _loopBigTable.ItemDescriptionRows.Add(new ItemDescriptionRow()
                                    {
                                        RowId = resetNumber,
                                        Description = _quotationItemDescription.QuotationItem_Description,
                                        Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                                        Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                                        AdjCost = AdjCost,
                                        Est_Profit = Profit.ToString("#,##0.00")
                                    });
                                    break;
                                }
                            }
                        }
                        else
                        {
                            resetNumber = 1;
                            ItemDescriptionRows.Add(new ItemDescriptionRow()
                            {
                                RowId = resetNumber,
                                Description = _quotationItemDescription.QuotationItem_Description,
                                Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                                Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                                AdjCost = AdjCost,
                                Est_Profit = Profit.ToString("#,##0.00")
                            });

                            quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                            {
                                TableID = countItemDescription,
                                CategoryName = _quotationItemDescription.CategoryName,
                                ItemDescriptionRows = ItemDescriptionRows
                            });
                            countItemDescription++;
                        }
                    }
                    resetNumber++;
                }

                ViewData["QuotationItemDescription"] = quotationItemDescriptionBigTable;
            
            }
            else
            {
                Session.Add("Result", "danger|Project Estimators Item Descriptions record not found!");
                return RedirectToAction("ProjectEstimators");
            }

            return View();
        }

        //GET: GetInvoiceItemDescriptionsInfo
        public ActionResult GetInvoiceItemDescriptionsInfo(int id)
        {
            if (id > 0)
            {
                Invoice invoice = _invoicesModel.GetSingle(id);
                ViewData["Invoice"] = invoice;
                IList<InvoiceItemDescription> invoiceItemDescriptions = _invoiceItemDescriptionsModel.GetAll().Where(e => e.InvoiceId == id).ToList();
                ViewData["InvoiceItemDescriptions"] = invoiceItemDescriptions;
            }
            else
            {
                Session.Add("Result", "danger|Invoice Item Descriptions record not found!");
                return RedirectToAction("Invoices");
            }

            return View();
        }

        //GET: GetPayoutSummaryInfo
        public ActionResult GetPayoutSummaryInfo(int id)
        {
            if (id > 0)
            {
                IList<PayoutCalculation> payoutSummary = _payoutSummariesModel.GetAll().Where(e => e.PayoutId == id).ToList();
                ViewData["PayoutSummary"] = payoutSummary;
            }
            else
            {
                Session.Add("Result", "danger|Payout Summary record not found!");
                return RedirectToAction("Payouts");
            }

            return View();
        }

        //Table Name Dropdown
        public Dropdown[] TableNameDDL()
        {
            List<string> tableNames = new List<string>();

            tableNames.Add("Customers");
            tableNames.Add("Users");
            tableNames.Add("Quotations");
            tableNames.Add("Projects");
            tableNames.Add("ProjectEstimators");
            tableNames.Add("Invoices");
            tableNames.Add("Payouts");
            tableNames.Add("EarlyPayouts");

            tableNames = tableNames.OrderBy(e => e).ToList();

            Dropdown[] ddl = new Dropdown[tableNames.Count];

            int count = 0;
            foreach (string name in tableNames)
            {
                ddl[count] = new Dropdown { name = name, val = name };
                count++;
            }

            return ddl;
        }

        //PageSize Dropdown
        public Dropdown[] PageSizeDDL()
        {
            Dropdown[] ddl = new Dropdown[5];
            ddl[0] = new Dropdown { name = "20", val = "20" };
            ddl[1] = new Dropdown { name = "40", val = "40" };
            ddl[2] = new Dropdown { name = "60", val = "60" };
            ddl[3] = new Dropdown { name = "80", val = "80" };
            ddl[4] = new Dropdown { name = "100", val = "100" };
            return ddl;
        }
    }
}