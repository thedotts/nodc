﻿using DataAccess;
using DataAccess.POCO;
using NODC_OMS.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NODC_OMS.Controllers
{
    public class ControllerBase : Controller
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            string logPath = Server.MapPath(ConfigurationManager.AppSettings["ErrorLogPath"].ToString() + "Error_" + DateTime.Now.ToString("yyyyMMdd") + ".log");

            WriteErrorLog(logPath, filterContext.Exception.ToString());

            if (filterContext.HttpContext.IsCustomErrorEnabled)
            {
                filterContext.ExceptionHandled = true;
                this.View("Error").ExecuteResult(this.ControllerContext);
            }
        }

        static void WriteErrorLog(string logFile, string text)
        {
            StringBuilder message = new StringBuilder();
            message.AppendLine(DateTime.Now.ToString());
            message.AppendLine(text);
            message.AppendLine("=========================================");

            System.IO.File.AppendAllText(logFile, message.ToString());
        }
    }

    public class RedirectingActionAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (HttpContext.Current.Session["LoginID"] == null)
            {
                HttpContext.Current.Session.Add("Result", "danger|Please login to continue!");

                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Access",
                    action = "Index"
                }));
            }
            else
            {
                if (HttpContext.Current.Session["UserGroup"].ToString() == "Customer")
                {
                    HttpContext.Current.Session.Add("Result", "danger|Warning! The module you are accessing is restricted to Admin only!");

                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "Customers",
                        action = "Index"
                    }));
                }
            }
        }
    }

    public class RedirecttingActionAdminAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (HttpContext.Current.Session["UserName"] == null)
            {
                HttpContext.Current.Session.Add("Result", "danger|Please login to continue!");

                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Access",
                    action = "Index"
                }));
            }
            else
            {
                if (HttpContext.Current.Session["UserGroup"].ToString() != "Super Admin")
                {
                    HttpContext.Current.Session.RemoveAll();
                    HttpContext.Current.Session.Add("Result", "danger|You have no access to this module!");

                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "Access",
                        action = "Index"
                    }));
                }
            }
        }
    }

    public class RedirectingActionCustomerAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (HttpContext.Current.Session["LoginID"] == null)
            {
                HttpContext.Current.Session.Add("Result", "danger|Please login to continue!");

                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Home",
                    action = "Index"
                }));
            }
            else
            {
                if (HttpContext.Current.Session["UserGroup"].ToString() != "Customer")
                {
                    HttpContext.Current.Session.RemoveAll();
                    HttpContext.Current.Session.Add("Result", "danger|Please login in Admin Portal!");

                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "Access",
                        action = "Index"
                    }));
                }
            }
        }
    }
}