﻿using DataAccess;
using DataAccess.POCO;
using Newtonsoft.Json;
using NODC_OMS.Controllers;
using NODC_OMS.Helper;
using NODC_OMS.Models;
using PagedList;
using Rotativa;
using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NODC_OMS.Controllers
{
    [HandleError]
    [RedirectingAction]
    public class QuotationController : ControllerBase
    {
        private IUserRepository _usersModel;
        private IQuotationRepository _quotationsModel;
        private IQuotationCoBrokeRepository _quotationCoBrokesModel;
        private IQuotationItemDescriptionRepository _quotationItemDescriptionsModel;
        private IProjectRepository _projectsModel;
        private ISystemSettingRepository _settingsModel;
        private ICustomerRepository _customersModel;
        private IInvoiceRepository _invoicesModel;

        // GET: Quotation
        public QuotationController()
            : this(new UserRepository(), new QuotationRepository(), new QuotationCoBrokeRepository(), new QuotationItemDescriptionRepository(),new ProjectRepository(), new SystemSettingRepository(), new CustomerRepository(), new InvoiceRepository())
        {

        }

        public QuotationController(IUserRepository usersModel, IQuotationRepository quotationsModel,IQuotationCoBrokeRepository quotationCoBrokesModel, IQuotationItemDescriptionRepository quotationItemDescriptionsModel,IProjectRepository projectsModel, ISystemSettingRepository settingsModel, ICustomerRepository customersModel, IInvoiceRepository invoicesModel)
        {
            _usersModel = usersModel;
            _quotationsModel = quotationsModel;
            _quotationCoBrokesModel = quotationCoBrokesModel;
            _quotationItemDescriptionsModel = quotationItemDescriptionsModel;
            _projectsModel = projectsModel;
            _settingsModel = settingsModel;
            _customersModel = customersModel;
            _invoicesModel = invoicesModel;
        }

        // GET: Quotation
        public ActionResult Index()
        {
            if (Session["SearchKeyword"] != null)
            {
                Session.Remove("SearchKeyword");
            }

            if (Session["SearchStatus"] != null)
            {
                Session.Remove("SearchStatus");
            }

            return RedirectToAction("Listing");
        }

        //GET: Listing
        public ActionResult Listing(int page = 1)
        {

            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);

            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = "";
            ViewData["SearchStatus"] = "";
            ViewData["GetStatusValue"] = "All Quotations";

            if (Session["SearchKeyword"] != null)
            {
                ViewData["SearchKeyword"] = Session["SearchKeyword"];
            }

            if (Session["SearchStatus"] != null)
            {

                if (Session["SearchStatus"].ToString() == "Confirmed")
                {
                    ViewData["SearchStatus"] = "Confirmed";
                    ViewData["GetStatusValue"] = "Confirmed Only";
                }
                else if (Session["SearchStatus"].ToString() == "Rejected")
                {
                    ViewData["SearchStatus"] = "Rejected";
                    ViewData["GetStatusValue"] = "Rejected Only";
                }
                else if (Session["SearchStatus"].ToString() == "Pending")
                {
                    ViewData["SearchStatus"] = "Pending";
                    ViewData["GetStatusValue"] = "Pending Only";
                }
                else
                {
                    ViewData["SearchStatus"] = Session["SearchStatus"];
                }
            }

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", null, null, null);

            //Super Admin, able to see all.
            //Sales, only can see his own and his downline item.
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            User getUserRole = _usersModel.GetSingle(loginMainID);

            List<int> SalesId = new List<int>();
            int CustomerId = 0;
            string SalesPersonType = null;

            if(getUserRole.Role == "Sales")
            {
                //SalesPersonType = "User";

                SalesId.Add(loginMainID);

                //sales and customer, filter

                //first, get all upline id is same with login main ID.
                IList<User> getSameUplineID = _usersModel.GetAll(); //6

                foreach (User _getSameUplineID in getSameUplineID)
                {
                    if(_getSameUplineID.ID == loginMainID)//if match login ID, insert
                    {
                        SalesId.Add(loginMainID);
                    }
                    else
                    {
                        string Result = "No";
                        int CheckID = _getSameUplineID.ID; //2 // for keep check upline id use
                        int TheDownlineID = _getSameUplineID.ID; //2 //hold the current user id

                        do
                        {
                            User GetUpline = _usersModel.GetSingle(CheckID);//2, 2

                            if (GetUpline.ID == GetUpline.UplineId)
                            {
                                Result = "Yes";
                            }
                            else
                            {
                                if (GetUpline.UplineId == loginMainID)
                                {
                                    SalesId.Add(TheDownlineID);
                                    Result = "Yes";
                                }
                                else
                                {
                                    CheckID = GetUpline.UplineId;
                                }
                            }
                        }
                        while(Result != "Yes");
                    }
                }

            }
            else if(getUserRole.Role == "Customer")
            {
                //Customer Part
                //SalesPersonType = "Customer";
                CustomerId = loginMainID;
            }

            //If is super admin, able to do delete function

            ViewData["IsSuperAdmin"] = "No";

            if(getUserRole.Role == "Super Admin")
            {
                ViewData["IsSuperAdmin"] = "Yes";
            }

            //first get all latest Quotation ID and pass into the getPaged Item
            List<int> LatestQuotationId = new List<int>();

            IPagedList<Quotation> quotations = null;

            if (getUserRole.Role == "Super Admin")
            {
                //LatestQuotationId = null;
                //SalesId = null;
                quotations = _quotationsModel.GetPaged(ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), LatestQuotationId, SalesId, 0, page, pageSize);
            }
            else
            {
                IList<Quotation> getLatestQuotationID = _quotationsModel.GetAllLatestQuotationID(); //6

                foreach (Quotation _getLatestQuotationID in getLatestQuotationID)
                {

                    LatestQuotationId.Add(_getLatestQuotationID.ID);
                }

                quotations = _quotationsModel.GetPaged(ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), LatestQuotationId, SalesId, CustomerId, page, pageSize);
            }

            List<QuotationItemDescription> newTableItemDescriptions = new List<QuotationItemDescription>();

            foreach (Quotation _quotations in quotations)
            {
                int mainID = _quotations.ID;

                List<QuotationItemDescription> quotationsAmount = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(mainID);

                QuotationItemDescription itemRecord = new QuotationItemDescription();

                foreach (QuotationItemDescription _quotationsAmount in quotationsAmount)
                {
                    itemRecord.ID = _quotationsAmount.ID;
                    itemRecord.QuotationId = _quotationsAmount.QuotationId;
                    itemRecord.QuotationItemAmount += _quotationsAmount.QuotationItemAmount;
                }

                newTableItemDescriptions.Add(itemRecord);
            }

            ViewData["Quotation"] = quotations;
            ViewData["GetListingAmount"] = newTableItemDescriptions;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Listing
        [HttpPost]
        public ActionResult Listing(FormCollection form)
        {
            int page = 1;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = form["SearchKeyword"].Trim();
            Session.Add("SearchKeyword", form["SearchKeyword"].Trim());

            ViewData["GetStatusValue"] = "All Quotations";


            if (form["SearchStatusValue"].Trim() == "Confirmed")
            {
                ViewData["GetStatusValue"] = "Confirmed Only";
            }

            if (form["SearchStatusValue"].Trim() == "Rejected")
            {
                ViewData["GetStatusValue"] = "Rejected Only";
            }

            if (form["SearchStatusValue"].Trim() == "Pending")
            {
                ViewData["GetStatusValue"] = "Pending Only";
            }

            string item = "";

            if (form["SearchStatusValue"].Trim() != "All")
            {
                item = form["SearchStatusValue"].Trim();
            }

            ViewData["SearchStatus"] = item;
            Session.Add("SearchStatus", item);

            int loginMainID = Convert.ToInt32(Session["MainID"]);

            User getUserRole = _usersModel.GetSingle(loginMainID);

            List<int> SalesId = new List<int>();
            int CustomerId = 0;
            string SalesPersonType = null;

            if (getUserRole.Role == "Sales")
            {
                //SalesPersonType = "User";

                SalesId.Add(loginMainID);

                //sales and customer, filter

                //first, get all upline id is same with login main ID.
                string Result = "No";
                int CheckID = loginMainID;
                do
                {
                    List<User> getSameUplineID = _usersModel.GetAllSameUplineID(CheckID); //6

                    if (getSameUplineID.Count > 0)
                    {
                        foreach (User _getSameUplineID in getSameUplineID)
                        {
                            SalesId.Add(_getSameUplineID.ID);
                        }
                        CheckID = getSameUplineID[0].ID;
                    }
                    else
                    {
                        Result = "Yes";
                    }
                }
                while (Result != "Yes");

            }
            //{
            //    SalesPersonType = "User";

            //    SalesId.Add(loginMainID);

            //    //sales and customer, filter

            //    //first, get all upline id is same with login main ID.

            //    List<User> getSameUplineID = _usersModel.GetAllSameUplineID(loginMainID); //6

            //    foreach (User _getSameUplineID in getSameUplineID)
            //    {

            //        SalesId.Add(_getSameUplineID.ID);
            //    }
            //}
            else if (getUserRole.Role == "Customer")
            {
                //Customer Part
                SalesPersonType = "Customer";
                CustomerId = loginMainID;
            }

            //If is super admin, able to do delete function

            ViewData["IsSuperAdmin"] = "No";
            if (getUserRole.Role == "Super Admin")
            {
                ViewData["IsSuperAdmin"] = "Yes";
            }

            ////first get all latest Quotation ID and pass into the getPaged Item
            //List<int> LatestQuotationId = new List<int>();
            //IList<Quotation> getLatestQuotationID = _quotationsModel.GetAllLatestQuotationID(); //6

            //foreach (Quotation _getLatestQuotationID in getLatestQuotationID)
            //{

            //    LatestQuotationId.Add(_getLatestQuotationID.ID);
            //}

            //IPagedList<Quotation> quotations = _quotationsModel.GetPaged(ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), LatestQuotationId, SalesId, CustomerId, page, pageSize);


            //List<QuotationItemDescription> newTableItemDescriptions = new List<QuotationItemDescription>();

            //foreach (Quotation _quotations in quotations)
            //{
            //    int mainID = _quotations.ID;

            //    List<QuotationItemDescription> quotationsAmount = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(mainID);

            //    QuotationItemDescription itemRecord = new QuotationItemDescription();

            //    foreach (QuotationItemDescription _quotationsAmount in quotationsAmount)
            //    {
            //        itemRecord.ID = _quotationsAmount.ID;
            //        itemRecord.QuotationId = _quotationsAmount.QuotationId;
            //        itemRecord.QuotationItemAmount += _quotationsAmount.QuotationItemAmount;
            //    }

            //    newTableItemDescriptions.Add(itemRecord);
            //}

            //first get all latest Quotation ID and pass into the getPaged Item
            List<int> LatestQuotationId = new List<int>();

            IPagedList<Quotation> quotations = null;

            if (getUserRole.Role == "Super Admin") //if super admin, dont need filter anything.
            {
                //LatestQuotationId = null;
                //SalesId = null;
                quotations = _quotationsModel.GetPaged(ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), LatestQuotationId, SalesId, 0, page, pageSize);
            }
            else
            {
                IList<Quotation> getLatestQuotationID = _quotationsModel.GetAllLatestQuotationID(); //6

                foreach (Quotation _getLatestQuotationID in getLatestQuotationID)
                {

                    LatestQuotationId.Add(_getLatestQuotationID.ID);
                }

                quotations = _quotationsModel.GetPaged(ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), LatestQuotationId, SalesId, CustomerId, page, pageSize);
            }

            List<QuotationItemDescription> newTableItemDescriptions = new List<QuotationItemDescription>();

            foreach (Quotation _quotations in quotations)
            {
                int mainID = _quotations.ID;

                List<QuotationItemDescription> quotationsAmount = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(mainID);

                QuotationItemDescription itemRecord = new QuotationItemDescription();

                foreach (QuotationItemDescription _quotationsAmount in quotationsAmount)
                {
                    itemRecord.ID = _quotationsAmount.ID;
                    itemRecord.QuotationId = _quotationsAmount.QuotationId;
                    itemRecord.QuotationItemAmount += _quotationsAmount.QuotationItemAmount;
                }

                newTableItemDescriptions.Add(itemRecord);
            }

            ViewData["Quotation"] = quotations;
            ViewData["GetListingAmount"] = newTableItemDescriptions;

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", ViewData["SearchStatus"].ToString());

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Previous Quotation View
        public ActionResult PreviousQuotation(int id,int page = 1)
        {
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            Quotation getMainQuotation = _quotationsModel.GetSingle(id);

            if(getMainQuotation == null)
            {
                //if totally ID not match
                Session.Add("Result", "danger|Quotation record not found!");
                return RedirectToAction("Index","Quotation");
            }
            else
            {
                IPagedList<Quotation> countQuotations = _quotationsModel.GetPreviousQuotationPaged(id, page, pageSize);
                
                //if there is only 1 record, return back to main page.
                if(countQuotations.Count <= 1)
                {
                    Session.Add("Result", "danger|Previous Quotation record not found!");
                    return RedirectToAction("Index", "Quotation");
                }
            }

            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            IPagedList<Quotation> quotations = _quotationsModel.GetPreviousQuotationPaged(id, page, pageSize);

            List<QuotationItemDescription> newTableItemDescriptions = new List<QuotationItemDescription>();

            foreach (Quotation _quotations in quotations)
            {
                int mainID = _quotations.ID;

                List<QuotationItemDescription> quotationsAmount = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(mainID);

                QuotationItemDescription itemRecord = new QuotationItemDescription();

                foreach (QuotationItemDescription _quotationsAmount in quotationsAmount)
                {
                    itemRecord.ID = _quotationsAmount.ID;
                    itemRecord.QuotationId = _quotationsAmount.QuotationId;
                    itemRecord.QuotationItemAmount += _quotationsAmount.QuotationItemAmount;
                }

                newTableItemDescriptions.Add(itemRecord);
            }

            ViewData["PreviousQuotation"] = quotations;
            ViewData["GetListingAmount"] = newTableItemDescriptions;
            ViewData["TitleOfThePreviousQuotation"] = getMainQuotation.CompanyName;
            ViewData["ID"] = id;
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Create
        public ActionResult Create()
        {
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            List<QuotationCoBrokeTable> quotationCoBrokeTable = new List<QuotationCoBrokeTable>();

            Dropdown[] userDDL = UserDDL();
            Dropdown[] ownAndDownlineDDL = OwnAndDownlineDDL();

            quotationCoBrokeTable.Add(new QuotationCoBrokeTable()
            {
                RowId = 1,
                UserID = new SelectList(userDDL, "val", "name", loginMainID),
                PercentageOfProfit = new SelectList(PercentageOfProfitDDL(), "val", "name", "100")
            });

            int ItemCount = quotationCoBrokeTable.Count;

            ViewData["QuotationCoBrokeTable"] = quotationCoBrokeTable;
            ViewData["CoBrokeItemCount"] = ItemCount;

            string PaymentTerms = _settingsModel.GetCodeValue("PAYMENT_TERMS");

            ViewData["PaymentTerms"] = PaymentTerms;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            Dropdown[] companyDDL = CompanyDDL();
            ViewData["CompanyDropdown"] = new SelectList(companyDDL, "val", "name", null);

            Dropdown[] customerDDL = CustomerDDL();
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", null);

            int MainID = Convert.ToInt32(Session["MainID"]);

            User getPreparedUser = _usersModel.GetSingle(MainID);

            ViewData["PreparedBy"] = getPreparedUser.Name;

            if (getPreparedUser.Role != "Super Admin")
            {
                ViewData["UserDropdown"] = new SelectList(ownAndDownlineDDL, "val", "name", loginMainID);
            }
            else
            {
                ViewData["UserDropdown"] = new SelectList(userDDL, "val", "name", loginMainID);
            }

            ViewData["SalesDocumentFiles"] = null;

            ViewData["QuotationDate"] = DateTime.Now.ToString("dd/MM/yyyy");

            //Loop Item Description Items
            IList<SystemSetting> listItem = _settingsModel.GetAll();
            SystemSetting CategoryList = listItem.Where(e => e.Code == "QUOTATION_CATEGORIES").FirstOrDefault();
            List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();
            int count = 1;
            if(!string.IsNullOrEmpty(CategoryList.Value))
            {
                string[] categoryList = CategoryList.Value.Split('|');
                
                foreach (string _categoryList in categoryList)
                {
                    List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                    ItemDescriptionRows.Add(new ItemDescriptionRow()
                    {
                        RowId = 1,
                        Description = null,
                        Qty = "1.0",
                        Unit = "lot",
                        Rate = null,
                        Amount = null,
                        Unit_Cost = null,
                        Est_Cost = null,
                        AdjCost = null,
                        Est_Profit = null,
                        Remarks = null,
                        Mgr_Comments = null
                    });

                    quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                    {
                        TableID = count,
                        CategoryName = _categoryList,
                        IsBasic = true,
                        ItemDescriptionRows = ItemDescriptionRows

                    });
                    count++;
                }
            }
            else
            {
                count = 2;
            }

            ViewData["TotalCategory"] = count - 1;
            ViewData["QuotationItemDescription"] = quotationItemDescriptionBigTable;
            ViewData["CheckQuotationItemLength"] = "No";

            if(quotationItemDescriptionBigTable.Count > 0)
            {
                ViewData["CheckQuotationItemLength"] = "Yes";
            }

            ViewData["PostBack"] = "No";

            List<Quotation> getUserQuotations = _quotationsModel.GetUserQuotations(loginMainID);
            List<string> descriptionsArray = new List<string>();

            foreach (Quotation _getUserQuotations in getUserQuotations)
            {
                foreach (QuotationItemDescription _getQuotationItemDescriptions in _getUserQuotations.ItemDescriptions)
                {
                    if (!descriptionsArray.Contains(_getQuotationItemDescriptions.QuotationItem_Description))
                    {
                        descriptionsArray.Add(_getQuotationItemDescriptions.QuotationItem_Description);
                    }
                }
            }
            descriptionsArray = descriptionsArray.OrderBy(e => e).ToList();
            ViewData["AutoCompleteList"] = JsonConvert.SerializeObject(descriptionsArray);
            return View();
        }

        //POST: Create
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(Quotation quotation, FormCollection form)
        {

            int MainID = Convert.ToInt32(Session["MainID"]);

            ViewData["QuotationDate"] = "";
            ViewData["PaymentTerms"] = form["PaymentTerms"];

            //Loop Item Description Items
            IList<SystemSetting> listItem = _settingsModel.GetAll();
            SystemSetting CategoryList = listItem.Where(e => e.Code == "QUOTATION_CATEGORIES").FirstOrDefault();

            string[] categoryList = CategoryList.Value.Split('|');
            int count = 0;

            foreach (string _categoryList in categoryList)
            {
                count++;
            }

            ViewData["TotalCategory"] = count;

            //Validation Part
            if(quotation.CustomerId == null)
            {
                ModelState.AddModelError("quotation.CustomerId", "Customer is required!");
            }

            if(string.IsNullOrEmpty(quotation.ContactPerson))
            {
                ModelState.AddModelError("quotation.ContactPerson", "Contact Person is required!");
            }

            if(string.IsNullOrEmpty(form["QuotationDate"]))
            {
                ModelState.AddModelError("QuotationDate", "Date is required!");
            }
            else
            {
                quotation.QuotationDate = Convert.ToDateTime(form["QuotationDate"].ToString());
                ViewData["QuotationDate"] = form["QuotationDate"].ToString();
            }

            if (string.IsNullOrEmpty(form["SalesPersonID"]))
            {
                ModelState.AddModelError("SalesPersonID", "Sales Person is required!");
            }
            else
            {
                string SalesPersonCheck = Convert.ToString(form["SalesPersonID"]);

                quotation.SalesPersonId = Convert.ToInt32(SalesPersonCheck);
            }

            string[] CompanyGSTCheck = null;
            

            //Split Company Name and GST check
            if (form["Company"] != "")
            {
                CompanyGSTCheck = form["Company"].Split('-');

                if (string.IsNullOrEmpty(CompanyGSTCheck[0]))
                {
                    ModelState.AddModelError("Company", "Company Name not found!");
                }

                if (string.IsNullOrEmpty(CompanyGSTCheck[1]))
                {
                    ModelState.AddModelError("Company", "Company GST not found!");
                }
            }
            else
            {
                ModelState.AddModelError("Company", "Company is required!");
            }

            //Quotation CoBroke Validation Part
            List<string> coBrokeKeys = form.AllKeys.Where(e => e.Contains("QuotationCoBrokePercentageOfProfit_")).ToList();

            List<QuotationCoBrokeTable> quotationCoBrokeTable = new List<QuotationCoBrokeTable>();

            int TotalPercentage = 0;
            int ItemCount = 0;

            foreach (string _coBrokeKeys in coBrokeKeys)
            {
                string rowId = _coBrokeKeys.Split('_')[1];

                string UserID = form["QuotationCoBrokeUser_" + rowId];
                string PercentageOfProfit = form["QuotationCoBrokePercentageOfProfit_" + rowId];

                if (string.IsNullOrEmpty(UserID))
                {
                    ModelState.AddModelError("QuotationCoBrokeUser_" + rowId, "User is required!");
                }

                if (string.IsNullOrEmpty(PercentageOfProfit))
                {
                    ModelState.AddModelError("QuotationCoBrokePercentageOfProfit_" + rowId, "Profit is required!");
                }

                TotalPercentage += Convert.ToInt32(PercentageOfProfit);

                if(coBrokeKeys.Count <= 3)
                {
                    quotationCoBrokeTable.Add(new QuotationCoBrokeTable()
                    {
                        RowId = Convert.ToInt32(rowId),
                        UserID = new SelectList(UserDDL(), "val", "name", UserID),
                        PercentageOfProfit = new SelectList(PercentageOfProfitDDL(), "val", "name", PercentageOfProfit)
                    });

                    ItemCount++;
                }
                else
                {
                    //if more than 3 items, error message prompt
                    ModelState.AddModelError("TotalOfCoBrokeItem", "Maximum 3 Co-broke!");
                }

            }

            ViewData["CoBrokeItemCount"] = ItemCount;

            //Calculate Total of Percentage is equal to 100%
            if (TotalPercentage != 100)
            {
                ModelState.AddModelError("TotalOfPercentage", "Total of Profit must equal to 100%!");
            }

            //Item Description Validatin Part
            List<string> ItemDescriptionKeys = form.AllKeys.Where(e => e.Contains("BigTableItemName-")).ToList();
            

            //For PostBack Use
            //List<QuotationItemDescriptionTable> quotationItemDescriptionTable = new List<QuotationItemDescriptionTable>();
            List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();

            foreach(string _ItemDescriptionKeys in ItemDescriptionKeys)
            {
                string rowId = _ItemDescriptionKeys.Split('-')[1];
                string ItemName = form["BigTableItemName-" + rowId];
                string IsBasic = form["BigTableItemNameFromDB-" + rowId];

                //Check From here
                List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                List<string> ItemDescriptionInsideKeys = form.AllKeys.Where(e => e.Contains("QuotationItemDescriptionDescription_" + rowId + "_")).ToList();
                //Foreach Item
                foreach(string _ItemDescriptionInsideKeys in ItemDescriptionInsideKeys)
                {
                    string insideRowId = _ItemDescriptionInsideKeys.Split('_')[1];
                    string insideRowId2 = _ItemDescriptionInsideKeys.Split('_')[2];

                    //if same, then start validation. (Same Table Item)
                    if (string.IsNullOrEmpty(ItemName))
                    {
                        ModelState.AddModelError("BigTableItemName-" + insideRowId, "Item Name cannot be empty!");
                    }
                    else
                    {
                        if (quotationItemDescriptionBigTable.Where(e => e.CategoryName == ItemName).FirstOrDefault() != null)
                        {
                            ModelState.AddModelError("BigTableItemName-" + insideRowId, "Item Name cannot be same!");
                        }
                    }

                    if(insideRowId == rowId)
                    {
                        string Description = form["QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2];
                        string Qty = form["QuotationItemDescriptionQty_" + insideRowId + "_" + insideRowId2];
                        string Unit = form["QuotationItemDescriptionUnit_" + insideRowId + "_" + insideRowId2];
                        string Rate = form["QuotationItemDescriptionRate_" + insideRowId + "_" + insideRowId2];
                        string Amount = form["QuotationItemDescriptionAmount2_" + insideRowId + "_" + insideRowId2];
                        string Unit_Cost = form["QuotationItemDescriptionUnitCost_" + insideRowId + "_" + insideRowId2];
                        string Est_Cost = form["QuotationItemDescriptionEstCost2_" + insideRowId + "_" + insideRowId2];
                        string Est_Profit = form["QuotationItemDescriptionProfit2_" + insideRowId + "_" + insideRowId2];
                        string Remarks = form["QuotationItemDescriptionRemarks_" + insideRowId + "_" + insideRowId2];
                        string Mgr_Comments = form["QuotationItemDescriptionMgrComments_" + insideRowId + "_" + insideRowId2];
                        string ItemNameFromDB = form["BigTableItemNameFromDB-" + insideRowId];

                        if (string.IsNullOrEmpty(Description))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2, "Item Description cannot be empty!");
                        }
                        else
                        {
                            if (ItemDescriptionRows.Where(e => e.Description == Description).FirstOrDefault() != null)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2, "Item Description cannot be same!");
                            }
                        }

                        if (string.IsNullOrEmpty(Qty))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionQty_" + insideRowId + "_" + insideRowId2, "Quantity cannot be empty!");
                        }
                        else
                        {
                            //Check is digit or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Qty);

                            if (!checkFormat)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionQty_" + insideRowId + "_" + insideRowId2, "Invalid Quantity Format!");
                            }
                        }

                        if (string.IsNullOrEmpty(Unit))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionUnit_" + insideRowId + "_" + insideRowId2, "Unit cannot be empty!");
                        }

                        if (string.IsNullOrEmpty(Rate))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionRate_" + insideRowId + "_" + insideRowId2, "Rate cannot be empty!");
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Rate);

                            if(!checkFormat)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionRate_" + insideRowId + "_" + insideRowId2, "Invalid Rate Format!");
                            }
                        }

                        if (string.IsNullOrEmpty(Unit_Cost))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionUnitCost_" + insideRowId + "_" + insideRowId2, "Unit Cost cannot be empty!");
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Unit_Cost);

                            if (!checkFormat)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionUnitCost_" + insideRowId + "_" + insideRowId2, "Invalid Unit Cost Format!");
                            }
                        }

                        if (string.IsNullOrEmpty(Est_Cost))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionEstCost2_" + insideRowId + "_" + insideRowId2, "Est Cost cannot be empty!");
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Est_Cost);

                            if (!checkFormat)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionEstCost_" + insideRowId + "_" + insideRowId2, "Invalid Est Cost Format!");
                            }
                        }

                        if(string.IsNullOrEmpty(Remarks))
                        {
                            Remarks = null;
                        }

                        if(string.IsNullOrEmpty(Mgr_Comments))
                        {
                            Mgr_Comments = null;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(form["SalesPersonID"]))
                            {
                                string SalesPersonCheck = Convert.ToString(form["SalesPersonID"]);

                                quotation.SalesPersonId = Convert.ToInt32(SalesPersonCheck);

                                //Check is nearest Agency enter or not

                                string Result = "";

                                int findAgencyID = (int)quotation.SalesPersonId;
                                do
                                {
                                    User searchAgencyLeader = _usersModel.GetSingle(findAgencyID);

                                    if (searchAgencyLeader != null)
                                    {
                                        if (searchAgencyLeader.IsAgencyLeader != "Yes")
                                        {
                                            //if not agency leader, find their upline and search again.
                                            Result = "No";
                                            findAgencyID = searchAgencyLeader.UplineId;
                                        }
                                        else
                                        {
                                            //if get agency leader, compare the login user id is same with this agency leader id or not.
                                            Result = "Yes";
                                            if (MainID != searchAgencyLeader.ID)
                                            {
                                                //if not same, cannot enter.
                                                ModelState.AddModelError("QuotationItemDescriptionMgrComments_" + insideRowId + "_" + insideRowId2, "This field is only for nearest agency leader to fill in!");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ModelState.AddModelError("QuotationItemDescriptionMgrComments_" + insideRowId + "_" + insideRowId2, "Nearest Agency Not Found!");
                                    }

                                } while (Result != "Yes");
                            }
                            else
                            {
                                ModelState.AddModelError("QuotationItemDescriptionMgrComments_" + insideRowId + "_" + insideRowId2, "Sales Person not found!");
                            }
                            
                        }

                        if(string.IsNullOrEmpty(Amount))
                        {
                            Amount = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Amount);

                            if (checkFormat)
                            {
                                Amount = Convert.ToDecimal(Amount).ToString("#,##0.00");
                            }
                        }

                        if (string.IsNullOrEmpty(Est_Profit))
                        {
                            Est_Profit = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Est_Profit);

                            if (checkFormat)
                            {
                                Est_Profit = Convert.ToDecimal(Est_Profit).ToString("#,##0.00");
                            }
                        }

                        ItemDescriptionRows.Add(new ItemDescriptionRow()
                        {
                            RowId = Convert.ToInt32(insideRowId2),
                            Description = Description,
                            Qty = Qty,
                            Unit = Unit,
                            Rate = Rate,
                            Amount = Amount,
                            Unit_Cost = Unit_Cost,
                            Est_Cost = Est_Cost,
                            AdjCost = null,
                            Est_Profit = Est_Profit,
                            Remarks = Remarks,
                            Mgr_Comments = Mgr_Comments
                        });

                    }
                }
                bool ConvertIsBasic = true;

                if (IsBasic == "True")
                {
                    ConvertIsBasic = Convert.ToBoolean("True");
                }
                else
                {
                    ConvertIsBasic = Convert.ToBoolean("False");
                }

                quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                {
                    TableID = Convert.ToInt32(rowId),
                    CategoryName = ItemName,
                    IsBasic = Convert.ToBoolean(ConvertIsBasic),
                    ItemDescriptionRows = ItemDescriptionRows

                });
            }

            ViewData["CheckQuotationItemLength"] = "No";

            if (quotationItemDescriptionBigTable.Count > 0)
            {
                ViewData["CheckQuotationItemLength"] = "Yes";
            }

            if(ModelState.IsValid)
            {
                //For store Cobroke item
                QuotationCoBroke quotationCoBrokRecord = new QuotationCoBroke();
                
                //For store Item Description item
                QuotationItemDescription quotationItemDescriptionRecord = new QuotationItemDescription();

                //Assign Quotation ID
                string prefix = _settingsModel.GetCodeValue("PREFIX_QUOTATION_ID");

                IList<Quotation> lastRecord = _quotationsModel.GetLast();

                //Convert year to last two digit
                var date = DateTime.Now;
                int last_two_digit = date.Year % 100;

                if (lastRecord.Count > 0)
                {
                    quotation.QuotationID = prefix + last_two_digit + (lastRecord.Count + 1).ToString().PadLeft(5, '0');
                }
                else
                {
                    quotation.QuotationID = prefix + last_two_digit + "00001";
                }

                Customer getCustomerInfo = _customersModel.GetSingle((int)quotation.CustomerId);

                if(getCustomerInfo != null)
                {
                    quotation.Address = getCustomerInfo.Address;
                    quotation.Tel = getCustomerInfo.Tel;
                    quotation.Fax = getCustomerInfo.Fax;
                }
                else
                {
                    quotation.Address = null;
                    quotation.Tel = null;
                    quotation.Fax = null;
                }

                quotation.PreparedById = MainID;
                quotation.CompanyName = CompanyGSTCheck[0];
                quotation.HasGST = CompanyGSTCheck[1];

                if(!string.IsNullOrEmpty(quotation.SalesDocumentUploads))
                {
                    string[] SalesDocuments = quotation.SalesDocumentUploads.Split(',');

                    foreach (string file in SalesDocuments)
                    {
                        string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                        string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["QuotationSalesDocumentsFolder"].ToString()), file);

                        System.IO.File.Move(sourceFile, destinationFile);
                    }
                }

                //check prepared by user is agency leader or not
                //if yes, skip looping
                //else run looping

                User checkPreparedByUser = _usersModel.GetSingle(MainID);

                string AssignQuotationStatus = null;

                if (checkPreparedByUser.IsAgencyLeader == "Yes")
                {
                    User getSalesPersonName = _usersModel.GetSingle(Convert.ToInt32(quotation.SalesPersonId));

                    AssignQuotationStatus = "Pending Customer Confirmation ( " + getSalesPersonName.Name + " )";
                }
                else
                {
                    //Update Status
                    string Result = "";
                    int getAgencyLeader = 0;
                    string AgencyLeaderName = "";
                    int findAgencyID = (int)quotation.SalesPersonId;
                    do
                    {
                        User searchAgencyLeader = _usersModel.GetSingle(findAgencyID);

                        if (searchAgencyLeader != null)
                        {
                            if (searchAgencyLeader.IsAgencyLeader != "Yes")
                            {
                                //if not agency leader, find their upline and search again.
                                Result = "No";
                                findAgencyID = searchAgencyLeader.UplineId;
                            }
                            else
                            {
                                Result = "Yes";
                                getAgencyLeader = searchAgencyLeader.ID;
                                AgencyLeaderName = searchAgencyLeader.Name;

                            }
                        }
                        else
                        {
                            //if agency leader not found, send to super admin
                            AgencyLeaderName = "Super Admin";
                        }

                    } while (Result != "Yes");
                    //End

                    AssignQuotationStatus = "Pending Manager Approval ( " + AgencyLeaderName + " )";
                }

                quotation.Status = AssignQuotationStatus;
                quotation.ReviseQuotationTimes = 0;
                quotation.VariationOrderTimes = 0;
                quotation.PaymentTerms = form["PaymentTerms"];

                bool result = _quotationsModel.Add(quotation);

                if(result)
                {
                    bool update_quotation_previous_id = _quotationsModel.UpdatePreviousQuotationID(quotation.ID);

                    if (update_quotation_previous_id)
                    {

                        //Log File
                        string userRole = Session["UserGroup"].ToString();

                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }

                        string tableAffected = "Quotations";
                        string description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Quotation [" + quotation.QuotationID + "]";

                        bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                        //Store CoBroke Item

                        bool hasQuotationCoBroke = false;

                        foreach (QuotationCoBrokeTable _quotationCoBrokeTable in quotationCoBrokeTable)
                        {

                            quotationCoBrokRecord.QuotationId = quotation.ID;

                            //determine cobroke user type
                            string UserTypeCheck = Convert.ToString(_quotationCoBrokeTable.UserID.SelectedValue);

                            quotationCoBrokRecord.QuotationCoBrokeUserId = Convert.ToInt32(UserTypeCheck);
                            quotationCoBrokRecord.QuotationCoBrokePercentOfProfit = Convert.ToInt32(_quotationCoBrokeTable.PercentageOfProfit.SelectedValue);

                            bool add_quotation_cobroke = _quotationCoBrokesModel.Add(quotationCoBrokRecord);

                            if (add_quotation_cobroke)
                            {
                                if (!hasQuotationCoBroke)
                                {
                                    hasQuotationCoBroke = true;
                                }
                            }
                        }

                        if (hasQuotationCoBroke)
                        {
                            userRole = Session["UserGroup"].ToString();
                            if (userRole == "Sales")
                            {
                                userRole += " " + Session["Position"].ToString();
                            }
                            tableAffected = "QuotationCoBrokes";
                            description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Quotation CoBrokes [" + quotation.QuotationID + "]";

                            bool cobroke_create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                        }

                        bool hasItemDescription = false;

                        //Loop BigTable
                        foreach (ItemDescriptionBigTable _quotationItemDescriptionBigTable in quotationItemDescriptionBigTable)
                        {
                            foreach (ItemDescriptionRow _quotationItemDesriptionRow in _quotationItemDescriptionBigTable.ItemDescriptionRows)
                            {
                                quotationItemDescriptionRecord.QuotationId = quotation.ID;
                                quotationItemDescriptionRecord.CategoryName = _quotationItemDescriptionBigTable.CategoryName;
                                quotationItemDescriptionRecord.QuotationItem_Description = _quotationItemDesriptionRow.Description;
                                quotationItemDescriptionRecord.RowId = _quotationItemDescriptionBigTable.TableID;

                                if (_quotationItemDesriptionRow.Qty == "")
                                {
                                    _quotationItemDesriptionRow.Qty = "0";
                                    quotationItemDescriptionRecord.QuotationItemQty = 0;
                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemQty = Convert.ToDecimal(_quotationItemDesriptionRow.Qty);
                                }

                                if(_quotationItemDesriptionRow.Unit_Cost == "")
                                {
                                    _quotationItemDesriptionRow.Unit_Cost = "0";
                                    quotationItemDescriptionRecord.QuotationItemUnitCost = 0;
                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemUnitCost = Convert.ToDecimal(_quotationItemDesriptionRow.Unit_Cost);
                                }

                                if (string.IsNullOrEmpty(_quotationItemDesriptionRow.Unit))
                                {
                                    quotationItemDescriptionRecord.QuotationItemUnit = null;
                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemUnit = _quotationItemDesriptionRow.Unit;
                                }
                                
                                quotationItemDescriptionRecord.QuotationItemRate = Convert.ToDecimal(_quotationItemDesriptionRow.Rate);

                                decimal Amount = Convert.ToDecimal(_quotationItemDesriptionRow.Rate) * Convert.ToDecimal(_quotationItemDesriptionRow.Qty);
                                decimal EstCost = Convert.ToDecimal(_quotationItemDesriptionRow.Qty) * Convert.ToDecimal(_quotationItemDesriptionRow.Unit_Cost);
                                
                                quotationItemDescriptionRecord.QuotationItemAmount = Amount;
                                quotationItemDescriptionRecord.QuotationItemEstCost = EstCost;

                                decimal EstProfit = Amount - Convert.ToDecimal(_quotationItemDesriptionRow.Est_Cost);
                                quotationItemDescriptionRecord.QuotationItemEstProfit = EstProfit;

                                if (string.IsNullOrEmpty(_quotationItemDesriptionRow.Remarks))
                                {
                                    quotationItemDescriptionRecord.QuotationItemRemarks = null;
                                }
                                else
                                {
                                    //string newRemarks = _quotationItemDesriptionRow.Remarks.Replace("\r\n", "|");
                                    quotationItemDescriptionRecord.QuotationItemRemarks = _quotationItemDesriptionRow.Remarks;
                                }

                                if(string.IsNullOrEmpty(_quotationItemDesriptionRow.Mgr_Comments))
                                {
                                    quotationItemDescriptionRecord.QuotationItemMgrComments = null;
                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemMgrComments = _quotationItemDesriptionRow.Mgr_Comments;
                                }
                                
                                bool add_quotation_item_description = _quotationItemDescriptionsModel.Add(quotationItemDescriptionRecord);

                                if (add_quotation_item_description)
                                {
                                    if (!hasItemDescription)
                                    {
                                        hasItemDescription = true;
                                    }
                                }
                            }
                        }

                        if (hasItemDescription)
                        {
                            userRole = Session["UserGroup"].ToString();
                            if (userRole == "Sales")
                            {
                                userRole += " " + Session["Position"].ToString();
                            }
                            tableAffected = "QuotationItemDescriptions";
                            description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Quotation Item Descriptions [" + quotation.QuotationID + "]";

                            bool itemDescription_create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                        }

                        //Search nearest Agency and email to them.

                        //dont need to send email to prepared by pending manager approval 
                        //else will do looping to send email to sales person agency leader.
                        if (checkPreparedByUser.IsAgencyLeader != "Yes")
                        {
                            string Result2 = "";
                            int getAgencyLeader2 = 0;
                            int findAgencyID2 = (int)quotation.SalesPersonId;
                            do
                            {
                                User searchAgencyLeader = _usersModel.GetSingle(findAgencyID2);

                                if (searchAgencyLeader != null)
                                {
                                    if (searchAgencyLeader.IsAgencyLeader != "Yes")
                                    {
                                        //if not agency leader, find their upline and search again.
                                        Result2 = "No";
                                        findAgencyID2 = searchAgencyLeader.UplineId;
                                    }
                                    else
                                    {
                                        Result2 = "Yes";
                                        getAgencyLeader2 = searchAgencyLeader.ID;

                                        //Start To Email to the Agency Leader
                                        string subject = "Pending [" + quotation.QuotationID + "] Quotation Approval";
                                        string body = System.IO.File.ReadAllText(Server.MapPath("~/Templates/PendingQuotationNotification.html"));
                                        string recipient = searchAgencyLeader.Email;
                                        string name = searchAgencyLeader.Name;
                                        string quotationID = Convert.ToString(quotation.QuotationID);
                                        string status = quotation.Status;

                                        ListDictionary replacements = new ListDictionary();

                                        replacements.Add("<%Name%>", name);
                                        replacements.Add("<%QuotationID%>", quotationID);
                                        replacements.Add("<%Status%>", status);
                                        replacements.Add("<%Url%>", Url.Action("Edit", "Quotation", new { @id = quotation.ID }, "http"));

                                        bool sent = EmailHelper.SendEmail(subject, body, replacements, recipient);
                                    }
                                }
                                else
                                {
                                    //if agency leader not found, send to super admin

                                    IList<User> getSuperAdminInfo = _usersModel.GetAll().Where(e => e.Role == "Super Admin").ToList();

                                    foreach (User _getSuperAdminInfo in getSuperAdminInfo)
                                    {
                                        //Start To Email to all super admin
                                        string subject = "Pending [" + quotation.QuotationID + "] Quotation Approval";
                                        string body = System.IO.File.ReadAllText(Server.MapPath("~/Templates/PendingQuotationNotification.html"));
                                        string recipient = _getSuperAdminInfo.Email;
                                        string name = _getSuperAdminInfo.Name;
                                        string quotationID = Convert.ToString(quotation.QuotationID);
                                        string status = quotation.Status;

                                        ListDictionary replacements = new ListDictionary();

                                        replacements.Add("<%Name%>", name);
                                        replacements.Add("<%QuotationID%>", quotationID);
                                        replacements.Add("<%Status%>", status);
                                        replacements.Add("<%Url%>", Url.Action("Edit", "Quotation", new { @id = quotation.ID }, "http"));

                                        bool sent = EmailHelper.SendEmail(subject, body, replacements, recipient);
                                    }

                                    Session.Add("Result", "success|" + quotation.QuotationID + " has been successfully created but no agency leader found! The email will send to super admin");
                                    return RedirectToAction("Index");
                                }
                            } while (Result2 != "Yes");
                        }

                        Session.Add("Result", "success|" + quotation.QuotationID + " has been successfully created!");
                        return RedirectToAction("Index");
                        
                    }
                    else
                    {
                        Session.Add("Result", "danger|An error occured while save quotation previous quotation records!");
                        return View();
                    }
                }
                else
                {
                    Session.Add("Result", "danger|An error occured while save quotation records!");
                    return View();
                }
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            ViewData["QuotationCoBrokeTable"] = quotationCoBrokeTable;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            Dropdown[] companyDDL = CompanyDDL();
            ViewData["CompanyDropdown"] = new SelectList(companyDDL, "val", "name", form["Company"]);

            Dropdown[] customerDDL = CustomerDDL();
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", null);

            Dropdown[] userDDL = UserDDL();
            Dropdown[] ownAndDownlineDDL = OwnAndDownlineDDL();

            User getPreparedUser = _usersModel.GetSingle(MainID);

            if (getPreparedUser.Role != "Super Admin")
            {
                ViewData["UserDropdown"] = new SelectList(ownAndDownlineDDL, "val", "name", MainID);
            }
            else
            {
                ViewData["UserDropdown"] = new SelectList(userDDL, "val", "name", MainID);
            }

            ViewData["UserDropdown"] = new SelectList(ownAndDownlineDDL, "val", "name", form["SalesPersonID"]);

            ViewData["SalesDocumentFiles"] = quotation.SalesDocumentUploads;

            //User getPreparedUser = _usersModel.GetSingle(MainID);

            ViewData["PreparedBy"] = getPreparedUser.Name;

            ViewData["QuotationItemDescription"] = quotationItemDescriptionBigTable;

            ViewData["PostBack"] = "Yes";

            List<Quotation> getUserQuotations = _quotationsModel.GetUserQuotations(MainID);
            List<string> descriptionsArray = new List<string>();

            foreach (Quotation _getUserQuotations in getUserQuotations)
            {
                foreach (QuotationItemDescription _getQuotationItemDescriptions in _getUserQuotations.ItemDescriptions)
                {
                    if (!descriptionsArray.Contains(_getQuotationItemDescriptions.QuotationItem_Description))
                    {
                        descriptionsArray.Add(_getQuotationItemDescriptions.QuotationItem_Description);
                    }
                }
            }
            descriptionsArray = descriptionsArray.OrderBy(e => e).ToList();
            ViewData["AutoCompleteList"] = JsonConvert.SerializeObject(descriptionsArray);

            return View();
        }

        //POST: Save As Draft
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateDraft(Quotation quotation, FormCollection form)
        {

            int MainID = Convert.ToInt32(Session["MainID"]);

            //Loop Item Description Items
            IList<SystemSetting> listItem = _settingsModel.GetAll();
            SystemSetting CategoryList = listItem.Where(e => e.Code == "QUOTATION_CATEGORIES").FirstOrDefault();

            string[] categoryList = CategoryList.Value.Split('|');
            int count = 0;

            foreach (string _categoryList in categoryList)
            {
                count++;
            }

            ViewData["TotalCategory"] = count;

            //Split Company Name and GST check
            if(form["Company"] != "")
            {
                string[] CompanyGSTCheck = form["Company"].Split('-');

                quotation.CompanyName = CompanyGSTCheck[0];
                quotation.HasGST = CompanyGSTCheck[1];
            }

            if (!string.IsNullOrEmpty(form["QuotationDate"]))
            {
                quotation.QuotationDate = Convert.ToDateTime(form["QuotationDate"].ToString());
            }

            //Quotation CoBroke Validation Part
            List<string> coBrokeKeys = form.AllKeys.Where(e => e.Contains("QuotationCoBrokePercentageOfProfit_")).ToList();

            List<QuotationCoBrokeTable> quotationCoBrokeTable = new List<QuotationCoBrokeTable>();

            int TotalPercentage = 0;
            int ItemCount = 0;

            foreach (string _coBrokeKeys in coBrokeKeys)
            {
                string rowId = _coBrokeKeys.Split('_')[1];

                string UserID = form["QuotationCoBrokeUser_" + rowId];
                string PercentageOfProfit = form["QuotationCoBrokePercentageOfProfit_" + rowId];

                TotalPercentage += Convert.ToInt32(PercentageOfProfit);

                if (coBrokeKeys.Count <= 3)
                {
                    quotationCoBrokeTable.Add(new QuotationCoBrokeTable()
                    {
                        RowId = Convert.ToInt32(rowId),
                        UserID = new SelectList(UserDDL(), "val", "name", UserID),
                        PercentageOfProfit = new SelectList(PercentageOfProfitDDL(), "val", "name", PercentageOfProfit)
                    });

                    ItemCount++;
                }
            }

            ViewData["CoBrokeItemCount"] = ItemCount;

            //Item Description Validatin Part
            List<string> ItemDescriptionKeys = form.AllKeys.Where(e => e.Contains("BigTableItemName-")).ToList();

            //For PostBack Use
            List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();

            foreach (string _ItemDescriptionKeys in ItemDescriptionKeys)
            {
                string rowId = _ItemDescriptionKeys.Split('-')[1];
                string ItemName = form["BigTableItemName-" + rowId];
                string IsBasic = form["BigTableItemNameFromDB-" + rowId];

                //Check From here
                List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                List<string> ItemDescriptionInsideKeys = form.AllKeys.Where(e => e.Contains("QuotationItemDescriptionDescription_" + rowId + "_")).ToList();

                //Foreach Item
                foreach (string _ItemDescriptionInsideKeys in ItemDescriptionInsideKeys)
                {
                    string insideRowId = _ItemDescriptionInsideKeys.Split('_')[1];
                    string insideRowId2 = _ItemDescriptionInsideKeys.Split('_')[2];

                    //if same, then start validation. (Same Table Item)
                    if (insideRowId == rowId)
                    {
                        string Description = form["QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2];
                        string Qty = form["QuotationItemDescriptionQty_" + insideRowId + "_" + insideRowId2];
                        string Unit = form["QuotationItemDescriptionUnit_" + insideRowId + "_" + insideRowId2];
                        string Rate = form["QuotationItemDescriptionRate_" + insideRowId + "_" + insideRowId2];
                        string Amount = form["QuotationItemDescriptionAmount2_" + insideRowId + "_" + insideRowId2];
                        string Unit_Cost = form["QuotationItemDescriptionUnitCost_" + insideRowId + "_" + insideRowId2];
                        string Est_Cost = form["QuotationItemDescriptionEstCost2_" + insideRowId + "_" + insideRowId2];
                        string Est_Profit = form["QuotationItemDescriptionProfit2_" + insideRowId + "_" + insideRowId2];
                        string Remarks = form["QuotationItemDescriptionRemarks_" + insideRowId + "_" + insideRowId2];
                        string Mgr_Comments = form["QuotationItemDescriptionMgrComments_" + insideRowId + "_" + insideRowId2];
                        string ItemNameFromDB = form["BigTableItemNameFromDB-" + insideRowId];

                        if (!string.IsNullOrEmpty(Amount))
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Amount);

                            if (checkFormat)
                            {
                                Amount = Convert.ToDecimal(Amount).ToString("#,##0.00");
                            }
                        }

                        if (!string.IsNullOrEmpty(Qty))
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Qty);

                            if (checkFormat)
                            {
                                Qty = Convert.ToDecimal(Qty).ToString("#,##0.0");
                            }
                        }

                        if (!string.IsNullOrEmpty(Unit_Cost))
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Unit_Cost);

                            if (checkFormat)
                            {
                                Unit_Cost = Convert.ToDecimal(Unit_Cost).ToString("#,##0.0");
                            }
                        }


                        if (!string.IsNullOrEmpty(Est_Profit))
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Est_Profit);

                            if (checkFormat)
                            {
                                Est_Profit = Convert.ToDecimal(Est_Profit).ToString("#,##0.00");
                            }
                        }

                        ItemDescriptionRows.Add(new ItemDescriptionRow()
                        {
                            RowId = Convert.ToInt32(insideRowId2),
                            Description = Description,
                            Qty = Qty,
                            Unit = Unit,
                            Rate = Rate,
                            Amount = Amount,
                            Unit_Cost = Unit_Cost,
                            Est_Cost = Est_Cost,
                            AdjCost = null,
                            Est_Profit = Est_Profit,
                            Remarks = Remarks,
                            Mgr_Comments = Mgr_Comments
                        });
                        //count++;
                    }
                }
                bool ConvertIsBasic = true;

                if (IsBasic == "True")
                {
                    ConvertIsBasic = Convert.ToBoolean("True");
                }
                else
                {
                    ConvertIsBasic = Convert.ToBoolean("False");
                }

                quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                {
                    TableID = Convert.ToInt32(rowId),
                    CategoryName = ItemName,
                    IsBasic = Convert.ToBoolean(ConvertIsBasic),
                    ItemDescriptionRows = ItemDescriptionRows

                });
            }

            ViewData["CheckQuotationItemLength"] = "No";

            if (quotationItemDescriptionBigTable.Count > 0)
            {
                ViewData["CheckQuotationItemLength"] = "Yes";
            }

            if (ModelState.IsValid)
            {
                //For store Cobroke item
                QuotationCoBroke quotationCoBrokRecord = new QuotationCoBroke();

                //For store Item Description item
                QuotationItemDescription quotationItemDescriptionRecord = new QuotationItemDescription();

                //Assign Quotation ID
                quotation.QuotationID = null;

                int CustomerID = 0;

                if(quotation.CustomerId != null)
                {
                    CustomerID = (int)quotation.CustomerId;
                }

                Customer getCustomerInfo = _customersModel.GetSingle(CustomerID);

                if (getCustomerInfo != null)
                {
                    quotation.Address = getCustomerInfo.Address;
                    quotation.Tel = getCustomerInfo.Tel;
                    quotation.Fax = getCustomerInfo.Fax;
                }
                else
                {
                    quotation.Address = null;
                    quotation.Tel = null;
                    quotation.Fax = null;
                }

                quotation.PreparedById = MainID;

                if(form["SalesPersonID"] != "")
                {
                    string SalesPersonCheck = form["SalesPersonID"].ToString();

                    quotation.SalesPersonId = Convert.ToInt32(SalesPersonCheck);
                }

                if (!string.IsNullOrEmpty(quotation.SalesDocumentUploads))
                {
                    string[] SalesDocuments = quotation.SalesDocumentUploads.Split(',');

                    foreach (string file in SalesDocuments)
                    {
                        string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                        string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["QuotationSalesDocumentsFolder"].ToString()), file);

                        System.IO.File.Move(sourceFile, destinationFile);
                    }
                }

                quotation.Status = "Draft";
                quotation.ReviseQuotationTimes = 0;
                quotation.VariationOrderTimes = 0;
                quotation.PaymentTerms = form["PaymentTerms"];

                bool result = _quotationsModel.Add(quotation);

                if (result)
                {
                    bool update_quotation_previous_id = _quotationsModel.UpdatePreviousQuotationID(quotation.ID);

                    if (update_quotation_previous_id)
                    {

                        string userRole = Session["UserGroup"].ToString();
                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }

                        string tableAffected = "Quotations";
                        string description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Quotations Draft";

                        bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                        //Store CoBroke Item

                        bool hasCoBroke = false;

                        foreach (QuotationCoBrokeTable _quotationCoBrokeTable in quotationCoBrokeTable)
                        {
                            quotationCoBrokRecord.QuotationId = quotation.ID;

                            //determine cobroke user type
                            quotationCoBrokRecord.QuotationCoBrokeUserId = 0;

                            if (_quotationCoBrokeTable.UserID.SelectedValue != "")
                            {
                                string UserTypeCheck = Convert.ToString(_quotationCoBrokeTable.UserID.SelectedValue);

                                quotationCoBrokRecord.QuotationCoBrokeUserId = Convert.ToInt32(UserTypeCheck);
                            }

                            quotationCoBrokRecord.QuotationCoBrokePercentOfProfit = Convert.ToInt32(_quotationCoBrokeTable.PercentageOfProfit.SelectedValue);

                            bool add_quotation_cobroke = _quotationCoBrokesModel.Add(quotationCoBrokRecord);

                            if (add_quotation_cobroke)
                            {
                                if (!hasCoBroke)
                                {
                                    hasCoBroke = true;
                                }
                            }
                        }

                        if (hasCoBroke)
                        {
                            userRole = Session["UserGroup"].ToString();
                            if (userRole == "Sales")
                            {
                                userRole += " " + Session["Position"].ToString();
                            }
                            tableAffected = "QuotationCoBrokes";
                            description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Quotation CoBrokes [ Draft:" + quotation.ID + " ]";

                            bool coBroke_create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                        }

                        bool hasItemDescription = false;

                        //Loop BigTable
                        foreach (ItemDescriptionBigTable _quotationItemDescriptionBigTable in quotationItemDescriptionBigTable)
                        {
                            foreach (ItemDescriptionRow _quotationItemDesriptionRow in _quotationItemDescriptionBigTable.ItemDescriptionRows)
                            {
                                quotationItemDescriptionRecord.QuotationId = quotation.ID;
                                quotationItemDescriptionRecord.CategoryName = _quotationItemDescriptionBigTable.CategoryName;
                                quotationItemDescriptionRecord.QuotationItem_Description = _quotationItemDesriptionRow.Description;
                                quotationItemDescriptionRecord.RowId = _quotationItemDescriptionBigTable.TableID;

                                if (_quotationItemDesriptionRow.Qty == "")
                                {
                                    quotationItemDescriptionRecord.QuotationItemQty = Convert.ToDecimal("0.00");
                                    _quotationItemDesriptionRow.Qty = "0";
                                }
                                else
                                {
                                    decimal CheckQty;

                                    if (Decimal.TryParse(_quotationItemDesriptionRow.Qty, out CheckQty))
                                    {
                                        quotationItemDescriptionRecord.QuotationItemQty = Convert.ToDecimal(_quotationItemDesriptionRow.Qty);
                                    }
                                    else
                                    {
                                        quotationItemDescriptionRecord.QuotationItemQty = 0;
                                        _quotationItemDesriptionRow.Qty = "0";
                                    }
                                }

                                if (_quotationItemDesriptionRow.Unit_Cost == "")
                                {
                                    quotationItemDescriptionRecord.QuotationItemUnitCost = Convert.ToDecimal("0.00");
                                    _quotationItemDesriptionRow.Unit_Cost = "0";
                                }
                                else
                                {
                                    decimal CheckUnitCost;

                                    if (Decimal.TryParse(_quotationItemDesriptionRow.Unit_Cost, out CheckUnitCost))
                                    {
                                        quotationItemDescriptionRecord.QuotationItemUnitCost = Convert.ToDecimal(_quotationItemDesriptionRow.Unit_Cost);
                                    }
                                    else
                                    {
                                        quotationItemDescriptionRecord.QuotationItemUnitCost = 0;
                                        _quotationItemDesriptionRow.Unit_Cost = "0";
                                    }
                                }

                                if (string.IsNullOrEmpty(_quotationItemDesriptionRow.Unit))
                                {
                                    quotationItemDescriptionRecord.QuotationItemUnit = null;
                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemUnit = _quotationItemDesriptionRow.Unit;
                                }

                                if(_quotationItemDesriptionRow.Rate == "")
                                {
                                    quotationItemDescriptionRecord.QuotationItemRate = Convert.ToDecimal("0.00");
                                    _quotationItemDesriptionRow.Rate = "0.00";
                                }
                                else
                                {

                                    decimal CheckRate;

                                    if (Decimal.TryParse(_quotationItemDesriptionRow.Rate, out CheckRate))
                                    {
                                        quotationItemDescriptionRecord.QuotationItemRate = Convert.ToDecimal(_quotationItemDesriptionRow.Rate);
                                    }
                                    else
                                    {
                                        quotationItemDescriptionRecord.QuotationItemRate = 0;
                                        _quotationItemDesriptionRow.Rate = "0.00";
                                    }
                                }
                                
                                decimal Amount = Convert.ToDecimal(_quotationItemDesriptionRow.Rate) * Convert.ToDecimal(_quotationItemDesriptionRow.Qty);
                                quotationItemDescriptionRecord.QuotationItemAmount = Amount;

                                decimal EstCost = Convert.ToDecimal(_quotationItemDesriptionRow.Qty) * Convert.ToDecimal(_quotationItemDesriptionRow.Unit_Cost);
                                quotationItemDescriptionRecord.QuotationItemEstCost = EstCost;

                                if(string.IsNullOrEmpty(_quotationItemDesriptionRow.Est_Cost))
                                {
                                    _quotationItemDesriptionRow.Est_Cost = "0";
                                }

                                decimal EstProfit = Amount - Convert.ToDecimal(_quotationItemDesriptionRow.Est_Cost);
                                quotationItemDescriptionRecord.QuotationItemEstProfit = EstProfit;

                                if (string.IsNullOrEmpty(_quotationItemDesriptionRow.Remarks))
                                {
                                    quotationItemDescriptionRecord.QuotationItemRemarks = null;
                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemRemarks = _quotationItemDesriptionRow.Remarks;
                                }

                                if (string.IsNullOrEmpty(_quotationItemDesriptionRow.Mgr_Comments))
                                {
                                    quotationItemDescriptionRecord.QuotationItemMgrComments = null;
                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemMgrComments = _quotationItemDesriptionRow.Mgr_Comments;
                                }
                                
                                bool add_quotation_item_description = _quotationItemDescriptionsModel.Add(quotationItemDescriptionRecord);

                                if (add_quotation_item_description)
                                {
                                    if (!hasItemDescription)
                                    {
                                        hasItemDescription = true;
                                    }
                                }
                            }
                        }

                        if (hasItemDescription)
                        {
                            userRole = Session["UserGroup"].ToString();
                            if (userRole == "Sales")
                            {
                                userRole += " " + Session["Position"].ToString();
                            }
                            tableAffected = "QuotationItemDescriptions";
                            description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Quotation Item Descriptions [ Draft:" + quotation.ID + " ]";

                            bool itemDescription_create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                        }

                        Session.Add("Result", "success|Has been successfully save as draft!");
                        return RedirectToAction("Index");

                    }
                    else
                    {
                        Session.Add("Result", "danger|An error occured while save quotation previous quotation draft records!");
                        return View();
                    }
                }
                else
                {
                    Session.Add("Result", "danger|An error occured while save quotation draft records!");
                    return View();
                }
            }

            ViewData["QuotationCoBrokeTable"] = quotationCoBrokeTable;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            Dropdown[] companyDDL = CompanyDDL();
            ViewData["CompanyDropdown"] = new SelectList(companyDDL, "val", "name", null);

            Dropdown[] customerDDL = CustomerDDL();
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", null);

            Dropdown[] userDDL = UserDDL();

            Dropdown[] ownAndDownlineDDL = OwnAndDownlineDDL();

            User getPreparedUser = _usersModel.GetSingle(MainID);

            if (getPreparedUser.Role != "Super Admin")
            {
                ViewData["UserDropdown"] = new SelectList(ownAndDownlineDDL, "val", "name", quotation.SalesPersonId);
            }
            else
            {
                ViewData["UserDropdown"] = new SelectList(userDDL, "val", "name", quotation.SalesPersonId);
            }

            //ViewData["UserDropdown"] = new SelectList(ownAndDownlineDDL, "val", "name", null);

            ViewData["SalesDocumentFiles"] = quotation.SalesDocumentUploads;

            ViewData["PreparedBy"] = getPreparedUser.Name;

            ViewData["QuotationDate"] = quotation.QuotationDate;

            ViewData["QuotationItemDescription"] = quotationItemDescriptionBigTable;

            List<Quotation> getUserQuotations = _quotationsModel.GetUserQuotations(MainID);
            List<string> descriptionsArray = new List<string>();

            foreach (Quotation _getUserQuotations in getUserQuotations)
            {
                foreach (QuotationItemDescription _getQuotationItemDescriptions in _getUserQuotations.ItemDescriptions)
                {
                    if (!descriptionsArray.Contains(_getQuotationItemDescriptions.QuotationItem_Description))
                    {
                        descriptionsArray.Add(_getQuotationItemDescriptions.QuotationItem_Description);
                    }
                }
            }
            descriptionsArray = descriptionsArray.OrderBy(e => e).ToList();
            ViewData["AutoCompleteList"] = JsonConvert.SerializeObject(descriptionsArray);

            return View();
        }

        //GET: Edit
        public ActionResult Edit(int id)
        {
            int MainID = Convert.ToInt32(Session["MainID"]);

            Quotation checkQuoatation = _quotationsModel.GetSingle(id);

            User checkUser = _usersModel.GetSingle(MainID);//5, sales

            //Get Quotation 
            Quotation quotation = _quotationsModel.GetSingle(id);
            ViewData["Quotation"] = quotation;

            if (checkUser.Role != "Super Admin")// then is sales person come in
            { //1,5

                //check can go in update page or not
                //the one who create 
                if (!(checkQuoatation.PreparedById == MainID || checkQuoatation.SalesPersonId == MainID))
                {

                        if (quotation.Status.Contains("Pending Manager Approval"))
                        {

                            //For loop to get the agency leader id to compare with login id.
                            string Result = "";
                            int getAgencyLeader = 0;

                            int? SalesPersonID = quotation.SalesPersonId;

                            if (SalesPersonID != null)
                            {
                                int findAgencyID = (int)quotation.SalesPersonId;

                                do
                                {
                                    User searchAgencyLeader = _usersModel.GetSingle(findAgencyID);

                                    if (searchAgencyLeader != null)
                                    {
                                        if (searchAgencyLeader.IsAgencyLeader != "Yes")
                                        {
                                            //if not agency leader, find their upline and search again.
                                            Result = "No";
                                            findAgencyID = searchAgencyLeader.UplineId;
                                        }
                                        else
                                        {
                                            Result = "Yes";
                                            getAgencyLeader = searchAgencyLeader.ID;

                                            if (MainID == getAgencyLeader)
                                            {
                                                //show approve reject button if match
                                                Session.Add("CheckQuotationApproval", MainID);//main ID
                                                return RedirectToAction("UpdateApplication", new { @id = id });
                                            }
                                            else
                                            {
                                                return RedirectToAction("View", new { @id = id });
                                            }

                                        }
                                    }
                                    else
                                    {
                                        //if agency leader not found, check is super admin or not.
                                        User getSuperAdmin = _usersModel.GetSingle((int)quotation.SalesPersonId);

                                        Result = "Yes";

                                        if (getSuperAdmin.Role == "Super Admin")
                                        {
                                            //show approve reject button if match
                                            Session.Add("CheckQuotationApproval", MainID);//main ID
                                            return RedirectToAction("UpdateApplication", new { @id = id });
                                        }
                                        else
                                        {
                                            return RedirectToAction("View", new { @id = id });
                                        }
                                        
                                    }
                                } while (Result != "Yes");
                            }
                        }
                        else if (quotation.Status.Contains("Pending Sales Revision") || quotation.Status == "Cancelled by Customer")
                        {
                            if (quotation.SalesPersonId == MainID)
                            {
                                return RedirectToAction("ResubmmitApplication", new { @id = id });
                            }
                        }
            

                        if (quotation.SalesPersonId == MainID && quotation.Status.Contains("Pending Customer Confirmation"))
                        {
                            //if match sales person and also status is pending customer confirmation.
                            Session.Add("CheckQuotationApproval", MainID);//main ID
                            return RedirectToAction("UpdateApplication", new { @id = id });
                            //ViewData["PendingCustomerConfirmation"] = "Yes";
                        }
                    
                    //Session.Add("Result", "danger|You are not allow to edit this quotation!");
                    //return RedirectToAction("Index");
                }
                else
                {
                    if (quotation.Status.Contains("Pending Manager Approval"))
                    {

                        //For loop to get the agency leader id to compare with login id.
                        string Result = "";
                        int getAgencyLeader = 0;

                        int? SalesPersonID = quotation.SalesPersonId;

                        if (SalesPersonID != null)
                        {
                            int findAgencyID = (int)quotation.SalesPersonId;

                            do
                            {
                                User searchAgencyLeader = _usersModel.GetSingle(findAgencyID);

                                if (searchAgencyLeader != null)
                                {
                                    if (searchAgencyLeader.IsAgencyLeader != "Yes")
                                    {
                                        //if not agency leader, find their upline and search again.
                                        Result = "No";
                                        findAgencyID = searchAgencyLeader.UplineId;
                                    }
                                    else
                                    {
                                        Result = "Yes";
                                        getAgencyLeader = searchAgencyLeader.ID;

                                        if (MainID == getAgencyLeader)
                                        {
                                            //show approve reject button if match
                                            Session.Add("CheckQuotationApproval", MainID);//main ID
                                            return RedirectToAction("UpdateApplication", new { @id = id });
                                        }
                                        else
                                        {
                                            return RedirectToAction("View", new { @id = id });
                                        }

                                    }
                                }
                                else
                                {
                                    //if agency leader not found, check is super admin or not.
                                    User getSuperAdmin = _usersModel.GetSingle((int)quotation.SalesPersonId);

                                    Result = "Yes";

                                    if (getSuperAdmin.Role == "Super Admin")
                                    {
                                        //show approve reject button if match
                                        Session.Add("CheckQuotationApproval", MainID);//main ID
                                        return RedirectToAction("UpdateApplication", new { @id = id });
                                    }
                                    else
                                    {
                                        return RedirectToAction("View", new { @id = id });
                                    }

                                }
                            } while (Result != "Yes");
                        }
                    }
                    else if (quotation.Status.Contains("Pending Sales Revision") || quotation.Status == "Cancelled by Customer")
                    {
                        if (quotation.SalesPersonId == MainID)
                        {
                            return RedirectToAction("ResubmmitApplication", new { @id = id });
                        }
                    }


                    if (quotation.SalesPersonId == MainID && quotation.Status.Contains("Pending Customer Confirmation"))
                    {
                        //if match sales person and also status is pending customer confirmation.
                        Session.Add("CheckQuotationApproval", MainID);//main ID
                        return RedirectToAction("UpdateApplication", new { @id = id });
                        //ViewData["PendingCustomerConfirmation"] = "Yes";
                    }
                }
            }
            else
            {
                if (quotation.Status.Contains("Pending Manager Approval"))
                {

                    //For loop to get the agency leader id to compare with login id.
                    string Result = "";
                    int getAgencyLeader = 0;

                    int? SalesPersonID = quotation.SalesPersonId;

                    if (SalesPersonID != null)
                    {
                        int findAgencyID = (int)quotation.SalesPersonId;

                        do
                        {
                            User searchAgencyLeader = _usersModel.GetSingle(findAgencyID);

                            if (searchAgencyLeader != null)
                            {
                                if (searchAgencyLeader.IsAgencyLeader != "Yes")
                                {
                                    //if not agency leader, find their upline and search again.
                                    Result = "No";
                                    findAgencyID = searchAgencyLeader.UplineId;
                                }
                                else
                                {
                                    Result = "Yes";
                                    getAgencyLeader = searchAgencyLeader.ID;

                                    if (MainID == getAgencyLeader)
                                    {
                                        //show approve reject button if match
                                        Session.Add("CheckQuotationApproval", MainID);//main ID
                                        return RedirectToAction("UpdateApplication", new { @id = id });
                                    }
                                    else
                                    {
                                        //return RedirectToAction("View", new { @id = id });
                                    }

                                }
                            }
                            else
                            {
                                //if agency leader not found, check is super admin or not.
                                User getSuperAdmin = _usersModel.GetSingle((int)quotation.SalesPersonId);

                                Result = "Yes";

                                if (getSuperAdmin.Role == "Super Admin")
                                {
                                    //show approve reject button if match
                                    Session.Add("CheckQuotationApproval", MainID);//main ID
                                    return RedirectToAction("UpdateApplication", new { @id = id });
                                }
                                else
                                {
                                    //return RedirectToAction("View", new { @id = id });
                                }

                            }
                        } while (Result != "Yes");
                    }
                }
                else if (quotation.Status.Contains("Pending Sales Revision") || quotation.Status == "Cancelled by Customer")
                {
                    if (quotation.SalesPersonId == MainID)
                    {
                        return RedirectToAction("ResubmmitApplication", new { @id = id });
                    }
                }


                if (quotation.SalesPersonId == MainID && quotation.Status.Contains("Pending Customer Confirmation"))
                {
                    //if match sales person and also status is pending customer confirmation.
                    Session.Add("CheckQuotationApproval", MainID);//main ID
                    return RedirectToAction("UpdateApplication", new { @id = id });
                    //ViewData["PendingCustomerConfirmation"] = "Yes";
                }
            }

            //This 2 TempData is from Reject Status PostBack.
            //if(TempData["Remarks"] != null)
            //{
            //    ModelState.AddModelError("quotation.Remarks", "Remarks is required when reject quotation");
            //    Session.Add("Result", "danger|There is something wrong in the form!");
            //}

            //if (TempData["SalesDocumentUpload"] != null)
            //{
            //    ModelState.AddModelError("quotation.SalesDocumentUploads", "Sales Document is needed to upload!");
            //    Session.Add("Result", "danger|There is something wrong in the form!");
            //}

            //here is to check whether the login user is able to approve or not
            //if yes, redirect to updateApplication page
            //if no, continue this page.

            //if(quotation.Status == "Pending Sales Revision")
            //{
            //    ViewData["CheckDraftButton"] = quotation.Status;
            //}

            //ViewData["CheckDraftButton"] = quotation.Status;

            //This is for Pending Customer Confirmation Used
            

            //This is to disable all field if is pending manager approval
            //ViewData["DisableForPendingManagerApproval"] = null;

            //Get Quotation CoBroke Item
            List<QuotationCoBroke> quotationCoBroke = _quotationCoBrokesModel.GetAllRelatedQuotationId(id);
            List<QuotationCoBrokeTable> quotationCoBrokeTable = new List<QuotationCoBrokeTable>();

            ViewData["QuotationCoBrokeTable"] = quotationCoBrokeTable;

            int count = 1;
            foreach (QuotationCoBroke _quotationCoBroke in quotationCoBroke)
            {
                quotationCoBrokeTable.Add(new QuotationCoBrokeTable()
                {
                    RowId = Convert.ToInt32(count),
                    UserID = new SelectList(UserDDL(), "val", "name", _quotationCoBroke.QuotationCoBrokeUserId),
                    PercentageOfProfit = new SelectList(PercentageOfProfitDDL(), "val", "name", _quotationCoBroke.QuotationCoBrokePercentOfProfit)
                });
                count++;
            }

            //New Get Quotation Item Description
            List<QuotationItemDescription> quotationItemDescription = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(id);
            List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();

            //Loop Item Description Items
            IList<SystemSetting> listItem = _settingsModel.GetAll();
            SystemSetting CategoryList = listItem.Where(e => e.Code == "QUOTATION_CATEGORIES").FirstOrDefault();

            string[] categoryList = CategoryList.Value.Split('|');

            //Start
            //int countItemDescription = 1;
            int resetNumber = 1;
            foreach(QuotationItemDescription _quotationItemDescription in quotationItemDescription)//14 items
            {
                string ItemName = _quotationItemDescription.CategoryName;
                decimal ItemQty = _quotationItemDescription.QuotationItemQty;
                decimal ItemRate = _quotationItemDescription.QuotationItemRate;
                decimal ItemUnitCost = _quotationItemDescription.QuotationItemUnitCost;
                decimal ItemEstCost = _quotationItemDescription.QuotationItemEstCost;
                bool IsBasic = false; //default set to false means it is not from db item.

                List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                if(ItemQty == null)
                {
                    ItemQty = 0;
                }

                if(ItemRate == null)
                {
                    ItemRate = 0;
                }

                if(ItemUnitCost == null)
                {
                    ItemUnitCost = 0;
                }

                if(ItemEstCost == null)
                {
                    ItemEstCost = 0;
                }

                foreach (string _categoryList in categoryList)
                {
                   if(_categoryList == _quotationItemDescription.CategoryName)
                   {
                       IsBasic = true;
                   }
                }

                if (quotationItemDescriptionBigTable.Count == 0)
                {
                    resetNumber = 1;
                    ItemDescriptionRows.Add(new ItemDescriptionRow()
                    {
                        RowId = resetNumber,
                        Description = _quotationItemDescription.QuotationItem_Description,
                        Qty = Convert.ToDecimal(_quotationItemDescription.QuotationItemQty).ToString("#,##0.0"),
                        Unit = _quotationItemDescription.QuotationItemUnit,
                        Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                        Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                        Unit_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemUnitCost).ToString("#,##0.00"),
                        Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                        AdjCost = null,
                        Est_Profit = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstProfit).ToString("#,##0.00"),
                        Remarks = _quotationItemDescription.QuotationItemRemarks,
                        Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                    });

                    quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                    {
                        TableID = _quotationItemDescription.RowId,
                        CategoryName = _quotationItemDescription.CategoryName,
                        IsBasic = IsBasic,
                        ItemDescriptionRows = ItemDescriptionRows
                    });
                    //countItemDescription++;
                }
                else
                {
                    if (quotationItemDescriptionBigTable.Where(e => e.TableID == _quotationItemDescription.RowId).FirstOrDefault() != null)
                    {
                        foreach (ItemDescriptionBigTable _loopBigTable in quotationItemDescriptionBigTable)
                        {
                            if(_loopBigTable.TableID == _quotationItemDescription.RowId)
                            {
                                _loopBigTable.ItemDescriptionRows.Add(new ItemDescriptionRow()
                                {
                                    RowId = resetNumber,
                                    Description = _quotationItemDescription.QuotationItem_Description,
                                    Qty = Convert.ToDecimal(_quotationItemDescription.QuotationItemQty).ToString("#,##0.0"),
                                    Unit = _quotationItemDescription.QuotationItemUnit,
                                    Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                                    Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                                    Unit_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemUnitCost).ToString("#,##0.00"),
                                    Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                                    AdjCost = null,
                                    Est_Profit = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstProfit).ToString("#,##0.00"),
                                    Remarks = _quotationItemDescription.QuotationItemRemarks,
                                    Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                                });
                                break;
                            }
                        }
                    }
                    else
                    {
                        resetNumber = 1;
                        ItemDescriptionRows.Add(new ItemDescriptionRow()
                        {
                            RowId = resetNumber,
                            Description = _quotationItemDescription.QuotationItem_Description,
                            Qty = Convert.ToDecimal(_quotationItemDescription.QuotationItemQty).ToString("#,##0.0"),
                            Unit = _quotationItemDescription.QuotationItemUnit,
                            Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                            Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                            Unit_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemUnitCost).ToString("#,##0.00"),
                            Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                            AdjCost = null,
                            Est_Profit = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstProfit).ToString("#,##0.00"),
                            Remarks = _quotationItemDescription.QuotationItemRemarks,
                            Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                        });

                        quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                        {
                            TableID = _quotationItemDescription.RowId,
                            CategoryName = _quotationItemDescription.CategoryName,
                            IsBasic = IsBasic,
                            ItemDescriptionRows = ItemDescriptionRows
                        });
                        //countItemDescription++;
                    }
                }
                resetNumber++;
                //End
            }

            //ViewData["CheckDraftButton"] = quotation.Status;

            ViewData["QuotationItemDescription"] = quotationItemDescriptionBigTable;

            ViewData["CheckQuotationItemLength"] = "No";

            if (quotationItemDescriptionBigTable.Count > 0)
            {
                ViewData["CheckQuotationItemLength"] = "Yes";
            }

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            ViewData["SalesDocumentFiles"] = quotation.SalesDocumentUploads;

            ViewData["PreparedBy"] = quotation.PreparedBy.Name;

            ViewData["QuotationDate"] = Convert.ToDateTime(quotation.QuotationDate).ToString("dd/MM/yyyy");

            //get company value and assign to dropdown.
            string CompanyValue = quotation.CompanyName + "-" + quotation.HasGST;

            Dropdown[] companyDDL = CompanyDDL();
            ViewData["CompanyDropdown"] = new SelectList(companyDDL, "val", "name", CompanyValue);

            int AssignSalesPersonID = 0;

            if (quotation.CustomerId != 0 && quotation.CustomerId != null)
            {
                AssignSalesPersonID = (int)quotation.CustomerId;
            }

            Dropdown[] customerDDL = CustomerDDL(null, AssignSalesPersonID);
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", null);

            Dropdown[] userDDL = UserDDL();

            Dropdown[] ownAndDownlineDDL = OwnAndDownlineDDL();

            User getPreparedUser = _usersModel.GetSingle(MainID);

            if (getPreparedUser.Role != "Super Admin")
            {
                ViewData["UserDropdown"] = new SelectList(ownAndDownlineDDL, "val", "name", quotation.SalesPersonId);
            }
            else
            {
                ViewData["UserDropdown"] = new SelectList(userDDL, "val", "name", quotation.SalesPersonId);
            }

            Dropdown[] percentageDDL = PercentageOfProfitDDL();
            ViewData["PercentageOfProfitDropdown"] = new SelectList(percentageDDL, "val", "name", null);

            //ViewData["IsNearestAgency"] = "No";

            //if (quotation.Status.Contains("Pending Manager Approval"))
            //{

            //    //For loop to get the agency leader id to compare with login id.
            //    string Result = "";
            //    int getAgencyLeader = 0;

            //    int? SalesPersonID = quotation.SalesPersonId;

            //    if (SalesPersonID != null)
            //    {
            //        int findAgencyID = (int)quotation.SalesPersonId;

            //        do
            //        {
            //            User searchAgencyLeader = _usersModel.GetSingle(findAgencyID);

            //            if (searchAgencyLeader != null)
            //            {
            //                if (searchAgencyLeader.IsAgencyLeader != "Yes")
            //                {
            //                    //if not agency leader, find their upline and search again.
            //                    Result = "No";
            //                    findAgencyID = searchAgencyLeader.UplineId;
            //                }
            //                else
            //                {
            //                    Result = "Yes";
            //                    getAgencyLeader = searchAgencyLeader.ID;

            //                    if (MainID == getAgencyLeader)
            //                    {
            //                        //show approve reject button if match
            //                        //ViewData["IsNearestAgency"] = "Yes";
            //                    }

            //                }
            //            }
            //            else
            //            {
            //                //if agency leader not found, check is super admin or not.
            //                User getSuperAdmin = _usersModel.GetSingle((int)quotation.SalesPersonId);

            //                if (getSuperAdmin.Role == "Super Admin")
            //                {
            //                    //show approve reject button if match
            //                    //ViewData["IsNearestAgency"] = "Yes";
            //                }
            //                Result = "Yes";
            //            }
            //        } while (Result != "Yes");
            //    }
            //}
            //else if()

            List<Quotation> getUserQuotations = _quotationsModel.GetUserQuotations(MainID);
            List<string> descriptionsArray = new List<string>();

            foreach (Quotation _getUserQuotations in getUserQuotations)
            {
                foreach (QuotationItemDescription _getQuotationItemDescriptions in _getUserQuotations.ItemDescriptions)
                {
                    if (!descriptionsArray.Contains(_getQuotationItemDescriptions.QuotationItem_Description))
                    {
                        descriptionsArray.Add(_getQuotationItemDescriptions.QuotationItem_Description);
                    }
                }
            }
            descriptionsArray = descriptionsArray.OrderBy(e => e).ToList();
            ViewData["AutoCompleteList"] = JsonConvert.SerializeObject(descriptionsArray);
            return View();
        }

        //POST: Edit
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int id, Quotation quotation, FormCollection form)
        {

            //Get OldData
            Quotation oldData = _quotationsModel.GetSingle(id);

            quotation.QuotationID = oldData.QuotationID;
            quotation.CompanyName = oldData.CompanyName;
            quotation.PreparedById = oldData.PreparedById;
            quotation.Status = oldData.Status;
            quotation.Tel = quotation.Tel;

            string checkStatus = oldData.Status;

            int AssignSalesPersonId = 0;

            int MainID = Convert.ToInt32(Session["MainID"]);

            //ViewData["CheckDraftButton"] = oldData.Status;

            //ViewData["IsNearestAgency"] = "No";


            //This is for Pending Customer Confirmation Used
            //ViewData["PendingCustomerConfirmation"] = "No";

            //if (oldData.SalesPersonId == MainID)
            //{
            //    ViewData["PendingCustomerConfirmation"] = "Yes";
            //}

            //Loop Item Description Items
            IList<SystemSetting> listItem = _settingsModel.GetAll();
            SystemSetting CategoryList = listItem.Where(e => e.Code == "QUOTATION_CATEGORIES").FirstOrDefault();

            string[] categoryList = CategoryList.Value.Split('|');
            int count = 0;

            foreach (string _categoryList in categoryList)
            {
                count++;
            }

            ViewData["TotalCategory"] = count;

            //Validation Part
            if (quotation.CustomerId == null)
            {
                ModelState.AddModelError("quotation.CustomerId", "Customer is required!");
            }

            if (string.IsNullOrEmpty(quotation.ContactPerson))
            {
                ModelState.AddModelError("quotation.ContactPerson", "Contact Person is required!");
            }

            if (quotation.QuotationDate == null)
            {
                ModelState.AddModelError("quotation.QuotationDate", "Date is required!");
            }

            if (string.IsNullOrEmpty(form["SalesPersonID"]))
            {
                ModelState.AddModelError("SalesPersonID", "Sales Person is required!");
            }
            else
            {
                string SalesPersonCheck = Convert.ToString(form["SalesPersonID"]);

                quotation.SalesPersonId = Convert.ToInt32(SalesPersonCheck);
                AssignSalesPersonId = Convert.ToInt32(SalesPersonCheck);
            }

            if (oldData.Status.Contains("Pending Sales Revision") || oldData.Status == "Cancelled by Customer")
            {
                if(string.IsNullOrEmpty(quotation.Remarks))
                {
                    ModelState.AddModelError("quotation.Remarks", "Remarks is required!");
                }

                if (string.IsNullOrEmpty(quotation.SalesDocumentUploads))
                {
                    ModelState.AddModelError("quotation.SalesDocumentUploads", "Sales Document Upload is required!");
                }
            }

            string[] CompanyGSTCheck = null;

            //Split Company Name and GST check
            if (form["Company"] != "")
            {
                CompanyGSTCheck = form["Company"].Split('-');

                if (string.IsNullOrEmpty(CompanyGSTCheck[0]))
                {
                    ModelState.AddModelError("Company", "Company Name not found!");
                }

                if (string.IsNullOrEmpty(CompanyGSTCheck[1]))
                {
                    ModelState.AddModelError("Company", "Company GST not found!");
                }
            }
            else
            {
                ModelState.AddModelError("Company", "Company is required!");
            }

            //Quotation CoBroke Validation Part
            List<string> coBrokeKeys = form.AllKeys.Where(e => e.Contains("QuotationCoBrokePercentageOfProfit_")).ToList();

            List<QuotationCoBrokeTable> quotationCoBrokeTable = new List<QuotationCoBrokeTable>();

            int TotalPercentage = 0;
            int ItemCount = 0;

            foreach (string _coBrokeKeys in coBrokeKeys)
            {
                string rowId = _coBrokeKeys.Split('_')[1];

                string UserID = form["QuotationCoBrokeUser_" + rowId];
                string PercentageOfProfit = form["QuotationCoBrokePercentageOfProfit_" + rowId];

                if (string.IsNullOrEmpty(UserID))
                {
                    ModelState.AddModelError("QuotationCoBrokeUser_" + rowId, "User is required!");
                }

                if (string.IsNullOrEmpty(PercentageOfProfit))
                {
                    ModelState.AddModelError("QuotationCoBrokePercentageOfProfit_" + rowId, "Profit is required!");
                }

                TotalPercentage += Convert.ToInt32(PercentageOfProfit);

                if (coBrokeKeys.Count <= 3)
                {
                    quotationCoBrokeTable.Add(new QuotationCoBrokeTable()
                    {
                        RowId = Convert.ToInt32(rowId),
                        UserID = new SelectList(UserDDL(), "val", "name", UserID),
                        PercentageOfProfit = new SelectList(PercentageOfProfitDDL(), "val", "name", PercentageOfProfit)
                    });

                    ItemCount++;
                }
                else
                {
                    //if more than 3 items, error message prompt
                    ModelState.AddModelError("TotalOfCoBrokeItem", "Maximum 3 Co-broke!");
                }

            }

            ViewData["CoBrokeItemCount"] = ItemCount;

            //Calculate Total of Percentage is equal to 100%
            if (TotalPercentage != 100)
            {
                ModelState.AddModelError("TotalOfPercentage", "Total of Profit must equal to 100%!");
            }

            //Item Description Validatin Part
            List<string> ItemDescriptionKeys = form.AllKeys.Where(e => e.Contains("BigTableItemName-")).ToList();

            //For PostBack Use
            //List<QuotationItemDescriptionTable> quotationItemDescriptionTable = new List<QuotationItemDescriptionTable>();
            List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();

            foreach (string _ItemDescriptionKeys in ItemDescriptionKeys)
            {
                string rowId = _ItemDescriptionKeys.Split('-')[1];
                string ItemName = form["BigTableItemName-" + rowId];
                string IsBasic = form["BigTableItemNameFromDB-" + rowId];

                //Check From here
                List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                List<string> ItemDescriptionInsideKeys = form.AllKeys.Where(e => e.Contains("QuotationItemDescriptionDescription_" + rowId + "_")).ToList();

                //Foreach Item
                foreach (string _ItemDescriptionInsideKeys in ItemDescriptionInsideKeys)
                {
                    string insideRowId = _ItemDescriptionInsideKeys.Split('_')[1];
                    string insideRowId2 = _ItemDescriptionInsideKeys.Split('_')[2];

                    if (insideRowId == rowId)
                    {
                        string Description = form["QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2];
                        string Qty = form["QuotationItemDescriptionQty_" + insideRowId + "_" + insideRowId2];
                        string Unit = form["QuotationItemDescriptionUnit_" + insideRowId + "_" + insideRowId2];
                        string Rate = form["QuotationItemDescriptionRate_" + insideRowId + "_" + insideRowId2];
                        string Amount = form["QuotationItemDescriptionAmount2_" + insideRowId + "_" + insideRowId2];
                        string Unit_Cost = form["QuotationItemDescriptionUnitCost_" + insideRowId + "_" + insideRowId2];
                        string Est_Cost = form["QuotationItemDescriptionEstCost2_" + insideRowId + "_" + insideRowId2];
                        string Est_Profit = form["QuotationItemDescriptionProfit2_" + insideRowId + "_" + insideRowId2];
                        string Remarks = form["QuotationItemDescriptionRemarks_" + insideRowId + "_" + insideRowId2];
                        string Mgr_Comments = form["QuotationItemDescriptionMgrComments_" + insideRowId + "_" + insideRowId2];

                        //if same, then start validation. (Same Table Item)
                        if (string.IsNullOrEmpty(ItemName))
                        {
                            ModelState.AddModelError("BigTableItemName-" + insideRowId, "Item Name cannot be empty!");
                        }
                        else
                        {
                            if (quotationItemDescriptionBigTable.Where(e => e.CategoryName == ItemName).FirstOrDefault() != null)
                            {
                                ModelState.AddModelError("BigTableItemName" + insideRowId, "Item Name cannot be same!");
                            }
                        }

                        if (string.IsNullOrEmpty(Description))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2, "Item Description cannot be empty!");
                        }
                        else
                        {
                            if (ItemDescriptionRows.Where(e => e.Description == Description).FirstOrDefault() != null)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2, "Item Description cannot be same!");
                            }
                        }

                        if (string.IsNullOrEmpty(Qty))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionQty_" + insideRowId + "_" + insideRowId2, "Quantity cannot be empty!");
                        }
                        else
                        {
                            //Check is digit or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Qty);

                            if (!checkFormat)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionQty_" + insideRowId + "_" + insideRowId2, "Invalid Quantity Format!");
                            }
                        }

                        if (string.IsNullOrEmpty(Unit_Cost))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionUnitCost_" + insideRowId + "_" + insideRowId2, "Unit Cost cannot be empty!");
                        }
                        else
                        {
                            //Check is digit or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Unit_Cost);

                            if (!checkFormat)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionUnitCost_" + insideRowId + "_" + insideRowId2, "Invalid Unit Cost Format!");
                            }
                        }

                        if (string.IsNullOrEmpty(Unit))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionUnit_" + insideRowId + "_" + insideRowId2, "Unit cannot be empty!");
                        }

                        if (string.IsNullOrEmpty(Rate))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionRate_" + insideRowId + "_" + insideRowId2, "Rate cannot be empty!");
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Rate);

                            if (!checkFormat)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionRate_" + insideRowId + "_" + insideRowId2, "Invalid Rate Format!");
                            }
                        }

                        if (string.IsNullOrEmpty(Est_Cost))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionEstCost_" + insideRowId + "_" + insideRowId2, "Est Cost cannot be empty!");
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Est_Cost);

                            if (!checkFormat)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionEstCost_" + insideRowId + "_" + insideRowId2, "Invalid Est Cost Format!");
                            }
                        }

                        if (string.IsNullOrEmpty(Remarks))
                        {
                            Remarks = null;
                        }

                        if (string.IsNullOrEmpty(Mgr_Comments))
                        {
                            Mgr_Comments = null;
                        }
                        else
                        {

                            string SalesPersonCheck = Convert.ToString(form["SalesPersonID"]);

                            quotation.SalesPersonId = Convert.ToInt32(SalesPersonCheck);

                            //Check is nearest Agency enter or not

                            string Result = "";

                            int findAgencyID = (int)quotation.SalesPersonId;
                            do
                            {
                                User searchAgencyLeader = _usersModel.GetSingle(findAgencyID);

                                if (searchAgencyLeader != null)
                                {
                                    if (searchAgencyLeader.IsAgencyLeader != "Yes")
                                    {
                                        //if not agency leader, find their upline and search again.
                                        Result = "No";
                                        findAgencyID = searchAgencyLeader.UplineId;
                                    }
                                    else
                                    {
                                        //if get agency leader, compare the login user id is same with this agency leader id or not.
                                        Result = "Yes";
                                        if (MainID != searchAgencyLeader.ID)
                                        {
                                            //if not same, cannot enter.
                                            ModelState.AddModelError("QuotationItemDescriptionMgrComments_" + insideRowId + "_" + insideRowId2, "This field is only for nearest agency leader to fill in!");
                                        }
                                    }
                                }
                                else
                                {
                                    ModelState.AddModelError("QuotationItemDescriptionMgrComments_" + insideRowId + "_" + insideRowId2, "Nearest Agency Not Found!");
                                }

                            } while (Result != "Yes");
                        }

                        if (string.IsNullOrEmpty(Amount))
                        {
                            Amount = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Amount);

                            if (checkFormat)
                            {
                                Amount = Convert.ToDecimal(Amount).ToString("#,##0.00");
                            }
                        }

                        if (string.IsNullOrEmpty(Est_Profit))
                        {
                            Est_Profit = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Est_Profit);

                            if (checkFormat)
                            {
                                Est_Profit = Convert.ToDecimal(Est_Profit).ToString("#,##0.00");
                            }
                        }

                        ItemDescriptionRows.Add(new ItemDescriptionRow()
                        {
                            RowId = Convert.ToInt32(insideRowId2),
                            Description = Description,
                            Qty = Qty,
                            Unit = Unit,
                            Rate = Rate,
                            Amount = Amount,
                            Unit_Cost = Unit_Cost,
                            Est_Cost = Est_Cost,
                            AdjCost = null,
                            Est_Profit = Est_Profit,
                            Remarks = Remarks,
                            Mgr_Comments = Mgr_Comments
                        });

                    }
                }

                bool ConvertIsBasic = true;

                if (IsBasic == "True")
                {
                    ConvertIsBasic = Convert.ToBoolean("True");
                }
                else
                {
                    ConvertIsBasic = Convert.ToBoolean("False");
                }

                quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                {
                    TableID = Convert.ToInt32(rowId),
                    CategoryName = ItemName,
                    IsBasic = Convert.ToBoolean(ConvertIsBasic),
                    ItemDescriptionRows = ItemDescriptionRows

                });
            }

            ViewData["CheckQuotationItemLength"] = "No";

            if (quotationItemDescriptionBigTable.Count > 0)
            {
                ViewData["CheckQuotationItemLength"] = "Yes";
            }

            if (ModelState.IsValid)
            {

                //For store Cobroke item
                QuotationCoBroke quotationCoBrokRecord = new QuotationCoBroke();

                //For store Item Description item
                QuotationItemDescription quotationItemDescriptionRecord = new QuotationItemDescription();

                bool oldDataIsDraft = false;

                if (checkStatus == "Draft")
                {
                    //Assign Quotation ID
                    string prefix = _settingsModel.GetCodeValue("PREFIX_QUOTATION_ID");

                    IList<Quotation> lastRecord = _quotationsModel.GetLast();

                    //Convert year to last two digit
                    var date = DateTime.Now;
                    int last_two_digit = date.Year % 100;

                    if (lastRecord.Count > 0)
                    {
                        quotation.QuotationID = prefix + last_two_digit + (lastRecord.Count + 1).ToString().PadLeft(5, '0');
                    }
                    else
                    {
                        quotation.QuotationID = prefix + last_two_digit + "00001";
                    }

                    quotation.ReviseQuotationTimes = 0;
                    quotation.VariationOrderTimes = 0;
                    oldDataIsDraft = true;
                }
                else if (checkStatus.Contains("Pending Customer Confirmation"))
                {
                    //Check Revised Quotation and Variation
                    quotation.ReviseQuotationTimes = oldData.ReviseQuotationTimes + 1;
                    quotation.VariationOrderTimes = oldData.VariationOrderTimes;
                    quotation.PreviousQuotationId = oldData.PreviousQuotationId;

                    if(oldData.VariationOrderTimes == 0)
                    {
                        if(quotation.ReviseQuotationTimes == 0)
                        {
                            quotation.QuotationID = oldData.QuotationID + "-R" + quotation.ReviseQuotationTimes;
                        }
                        else
                        {
                            string[] getQuotationID = oldData.QuotationID.Split('-');

                            //Sample: Q1700001-R2-V1
                            quotation.QuotationID = getQuotationID[0] + "-R" + quotation.ReviseQuotationTimes;
                        }
                        
                    }
                    else
                    {
                        //got Variation Order Before
                        string[] getQuotationID = oldData.QuotationID.Split('-');

                        //Sample: Q1700001-R2-V1
                        quotation.QuotationID = getQuotationID[0] + "-R" + quotation.ReviseQuotationTimes + "-V" + oldData.VariationOrderTimes;
                    }
                }
                else if (checkStatus == "Confirmed")
                {
                    //Check Variation Quotation and Revised
                    quotation.VariationOrderTimes = oldData.VariationOrderTimes + 1;
                    quotation.ReviseQuotationTimes = oldData.ReviseQuotationTimes;
                    quotation.PreviousQuotationId = oldData.PreviousQuotationId;

                    if (oldData.ReviseQuotationTimes == 0)
                    {
                        if (quotation.VariationOrderTimes == 0)
                        {
                            quotation.QuotationID = oldData.QuotationID + "-V" + quotation.VariationOrderTimes;
                        }
                        else
                        {
                            string[] getQuotationID = oldData.QuotationID.Split('-');

                            //Sample: Q1700001-R2-V1
                            quotation.QuotationID = getQuotationID[0] + "-V" + quotation.VariationOrderTimes;
                        }
                    }
                    else
                    {
                        //got Revised Order Before
                        string[] getQuotationID = oldData.QuotationID.Split('-');

                        //Sample: Q1700001-R1-V2
                        quotation.QuotationID = getQuotationID[0] + "-R" + oldData.ReviseQuotationTimes + "-V" + quotation.VariationOrderTimes;
                    }
                }
                else if (checkStatus.Contains("Pending Sales Revision") || checkStatus == "Cancelled by Customer")
                {
                    //This is for resubmit

                    //Check Variation Quotation and Revised
                    quotation.VariationOrderTimes = oldData.VariationOrderTimes + 1;
                    quotation.ReviseQuotationTimes = oldData.ReviseQuotationTimes;
                    quotation.PreviousQuotationId = oldData.PreviousQuotationId;

                    if (oldData.ReviseQuotationTimes == 0)
                    {
                        if (quotation.VariationOrderTimes == 0)
                        {
                            quotation.QuotationID = oldData.QuotationID + "-V" + quotation.VariationOrderTimes;
                        }
                        else
                        {
                            string[] getQuotationID = oldData.QuotationID.Split('-');

                            //Sample: Q1700001-R2-V1
                            quotation.QuotationID = getQuotationID[0] + "-V" + quotation.VariationOrderTimes;
                        }
                    }
                    else
                    {
                        //got Revised Order Before
                        string[] getQuotationID = oldData.QuotationID.Split('-');

                        //Sample: Q1700001-R1-V2
                        quotation.QuotationID = getQuotationID[0] + "-R" + oldData.ReviseQuotationTimes + "-V" + quotation.VariationOrderTimes;
                    }
                }
                else
                {
                    quotation.QuotationID = oldData.QuotationID;
                    oldDataIsDraft = true;
                }

                Customer getCustomerInfo = _customersModel.GetSingle((int)quotation.CustomerId);

                if (getCustomerInfo != null)
                {
                    quotation.Address = getCustomerInfo.Address;
                    quotation.Tel = getCustomerInfo.Tel;
                    quotation.Fax = getCustomerInfo.Fax;
                }
                else
                {
                    quotation.Address = null;
                    quotation.Tel = null;
                    quotation.Fax = null;
                }

                quotation.PreparedById = MainID;

                string SalesPersonCheck = Convert.ToString(form["SalesPersonID"]);
                
                quotation.SalesPersonId = Convert.ToInt32(SalesPersonCheck);
                quotation.CompanyName = CompanyGSTCheck[0];
                quotation.HasGST = CompanyGSTCheck[1];

                AssignSalesPersonId = Convert.ToInt32(SalesPersonCheck);

                if (!string.IsNullOrEmpty(quotation.SalesDocumentUploads))
                {

                    if(oldData.SalesDocumentUploads != quotation.SalesDocumentUploads)
                    {
                        string[] SalesDocuments = quotation.SalesDocumentUploads.Split(',');

                        foreach (string file in SalesDocuments)
                        {
                            string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                            string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["QuotationSalesDocumentsFolder"].ToString()), file);

                            System.IO.File.Move(sourceFile, destinationFile);
                        }
                    }

                }

                //check here

                User CheckUserAgency = _usersModel.GetSingle(oldData.PreparedById);

                string PreparedByIsAgencyLeader = "No";

                if (CheckUserAgency.IsAgencyLeader == "Yes")
                {
                    PreparedByIsAgencyLeader = "Yes";
                }

                //Update Status
                string Result = "";
                int getAgencyLeader = 0;
                string AgencyLeaderName = "";
                int findAgencyID = (int)quotation.SalesPersonId;
                do
                {
                    User searchAgencyLeader = _usersModel.GetSingle(findAgencyID);

                    if (searchAgencyLeader != null)
                    {
                        if (searchAgencyLeader.IsAgencyLeader != "Yes")
                        {
                            //if not agency leader, find their upline and search again.
                            Result = "No";
                            findAgencyID = searchAgencyLeader.UplineId;
                        }
                        else
                        {
                            Result = "Yes";
                            getAgencyLeader = searchAgencyLeader.ID;
                            AgencyLeaderName = searchAgencyLeader.Name;

                        }
                    }
                    else
                    {
                        //if agency leader not found, send to super admin
                        AgencyLeaderName = "Super Admin";
                    }

                } while (Result != "Yes");

                string UpdatedSalesPerson = "No";

                if (checkStatus == "Draft")
                {
                    //if yes, skip manager approval
                    //assign sales person to pending customer confirmation
                    if (PreparedByIsAgencyLeader == "Yes")
                    {
                        User getSalesPersonName = _usersModel.GetSingle(Convert.ToInt32(quotation.SalesPersonId));
                        quotation.Status = "Pending Customer Confirmation ( " + getSalesPersonName.Name + " )";
                        UpdatedSalesPerson = "Yes";
                    }
                    else
                    {
                        //here is pending manager approval because prepared by user is not agency leader.
                        quotation.Status = "Pending Manager Approval ( " + AgencyLeaderName + " )";
                        UpdatedSalesPerson = "Yes";
                    }

                }
                else if (checkStatus.Contains("Pending Sales Revision") || checkStatus == "Cancelled by Customer")
                {
                    if (PreparedByIsAgencyLeader == "Yes")
                    {
                        User getSalesPersonName = _usersModel.GetSingle(Convert.ToInt32(quotation.SalesPersonId));
                        quotation.Status = "Pending Customer Confirmation ( " + getSalesPersonName.Name + " )";
                        UpdatedSalesPerson = "Yes";
                    }
                    else
                    {
                        quotation.Status = "Pending Manager Approval ( " + AgencyLeaderName + " )";
                        UpdatedSalesPerson = "Yes";
                    }

                }
                else if (checkStatus.Contains("Pending Manager Approval"))
                {

                    //check if old data quotation status is not same. then update new quotation status
                    if (checkStatus != "Pending Manager Approval ( " + AgencyLeaderName + " )")
                    {
                        quotation.Status = "Pending Manager Approval ( " + AgencyLeaderName + " )";
                        UpdatedSalesPerson = "Yes";
                    }
                    else
                    {
                        quotation.Status = checkStatus;
                    }
                }
                else if (checkStatus.Contains("Pending Customer Confirmation"))
                {
                    //purpose: if pending customer confirmation.
                    //super admin edit sales person id, if sales person id not same with previous one.
                    //update quotation pending customer confirmation sales person name status.

                    int SalesPersonID = Convert.ToInt32(oldData.SalesPersonId);
                    //get oldData salesperson id
                    User getSalesPerson = _usersModel.GetSingle(SalesPersonID);

                    if (checkStatus != "Pending Customer Confirmation ( " + getSalesPerson.Name + " )")
                    {
                        quotation.Status = "Pending Customer Confirmation ( " + getSalesPerson.Name + " )";
                    }
                    else
                    {
                        quotation.Status = checkStatus;
                    }
                }
                else
                {
                    quotation.Status = checkStatus;
                }

                //quotation.ReviseQuotationTimes = 0;
                //quotation.VariationOrderTimes = 0;

                bool result = false;

                if (oldDataIsDraft == false)
                {
                    result = _quotationsModel.Add(quotation);

                    //Update Linked project quotation id to latest quotation id.
                    if(result)
                    {
                        List<Project> project = _projectsModel.GetAllProjectRelatedQuotationID(oldData.ID);

                        foreach(Project _project in project)
                        {
                            _project.QuotationReferenceId = quotation.ID;

                            bool update_project_quotation_id = _projectsModel.Update(_project.ID, _project);

                            if (update_project_quotation_id)
                            {
                                string userRole = Session["UserGroup"].ToString();
                                if (userRole == "Sales")
                                {
                                    userRole += " " + Session["Position"].ToString();
                                }
                                string tableAffected = "Projects";
                                string description = userRole + " [" + Session["LoginID"].ToString() + "] Updated Projects [" + _project.ProjectID + "]";

                                bool project_create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                            }
                        }

                        List<Invoice> invoices = _invoicesModel.GetAllProjectRelatedInvoiceID(oldData.ID);

                        foreach (Invoice invoice in invoices)
                        {
                            invoice.QuotationId = quotation.ID;

                            bool update_invoice_quotation_id = _invoicesModel.Update(invoice.ID, invoice);


                            if (update_invoice_quotation_id)
                            {
                                string userRole = Session["UserGroup"].ToString();
                                if (userRole == "Sales")
                                {
                                    userRole += " " + Session["Position"].ToString();
                                }
                                string tableAffected = "Invoices";
                                string description = userRole + " [" + Session["LoginID"].ToString() + "] Updated Invoices [" + invoice.InvoiceID + "]";

                                bool invoice_update_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                            }
                        }
                    }
                }
                else
                {
                    //This is Draft, so is update to new application.
                    result = _quotationsModel.UpdateData(oldData.ID, quotation);
                }
                
                if (result)
                {

                    bool update_quotation_previous_id = false;

                    if(oldDataIsDraft == false)
                    {
                        update_quotation_previous_id = _quotationsModel.UpdatePreviousQuotationID(quotation.PreviousQuotationId);
                    }
                    else
                    {
                        update_quotation_previous_id = _quotationsModel.UpdatePreviousQuotationID(oldData.ID);
                    }

                    if (update_quotation_previous_id)
                    {
                        string userRole = Session["UserGroup"].ToString();
                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }

                        string tableAffected = "Quotations";
                        string description = "";

                        if (checkStatus.Contains("Pending Sales Revision") && checkStatus == "Cancelled by Customer" && checkStatus != "Confirmed" && !checkStatus.Contains("Pending Customer Confirmation"))
                        {
                            description = userRole + " [" + Session["LoginID"].ToString() + "] Resubmitted Quotation [" + quotation.QuotationID + "]";
                        }
                        else
                        {
                            description = userRole + " [" + Session["LoginID"].ToString() + "] Updated Quotation [" + quotation.QuotationID + "]";
                        }

                        bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                        if (!checkStatus.Contains("Pending Sales Revision") && checkStatus != "Cancelled by Customer" && checkStatus != "Confirmed" && !checkStatus.Contains("Pending Customer Confirmation"))
                        {
                            //Remove All QuotationCoBroke Item First
                            //Get Database Quotation CoBroke Items based on quotation ID
                            IList<QuotationCoBroke> getQuotationCoBroke = _quotationCoBrokesModel.GetPersonalAll(id);

                            foreach (QuotationCoBroke _getQuotationCoBroke in getQuotationCoBroke)
                            {
                                int quotationCoBrokeID = _getQuotationCoBroke.ID;
                                bool delete_quotation_coBroke = _quotationCoBrokesModel.Delete(quotationCoBrokeID);
                            }
                        }

                        bool hasCoBroke = false;

                        //Store CoBroke Item
                        foreach (QuotationCoBrokeTable _quotationCoBrokeTable in quotationCoBrokeTable)
                        {
                            if(oldDataIsDraft == false)
                            {
                                quotationCoBrokRecord.QuotationId = quotation.ID;
                            }
                            else
                            {
                                quotationCoBrokRecord.QuotationId = oldData.ID;
                            }
                            

                            //determine cobroke user type
                            string UserTypeCheck = Convert.ToString(_quotationCoBrokeTable.UserID.SelectedValue);

                            quotationCoBrokRecord.QuotationCoBrokeUserId = Convert.ToInt32(UserTypeCheck);
                            quotationCoBrokRecord.QuotationCoBrokePercentOfProfit = Convert.ToInt32(_quotationCoBrokeTable.PercentageOfProfit.SelectedValue);

                            bool add_quotation_cobroke = _quotationCoBrokesModel.Add(quotationCoBrokRecord);

                            if (add_quotation_cobroke)
                            {
                                if (!hasCoBroke)
                                {
                                    hasCoBroke = true;
                                }
                            }
                        }

                        if (hasCoBroke)
                        {
                            userRole = Session["UserGroup"].ToString();
                            if (userRole == "Sales")
                            {
                                userRole += " " + Session["Position"].ToString();
                            }
                            tableAffected = "QuotationCoBrokes";
                            description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Quotation CoBrokes [" + quotation.QuotationID + "]";

                            bool itemDescription_create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                        }

                        if (!checkStatus.Contains("Pending Sales Revision") && checkStatus != "Cancelled by Customer" && checkStatus != "Confirmed" && !checkStatus.Contains("Pending Customer Confirmation"))
                        {
                            //Remove All QuotationItemDescription Item
                            //Get Database Quotation Items Description based on quotation ID
                            IList<QuotationItemDescription> getQuotationItemDescription = _quotationItemDescriptionsModel.GetPersonalAll(id);

                            foreach (QuotationItemDescription _getQuotationItemDescription in getQuotationItemDescription)
                            {
                                int quotationItemDescriptionID = _getQuotationItemDescription.ID;
                                bool delete_quotation_itemDescription = _quotationItemDescriptionsModel.Delete(quotationItemDescriptionID);
                            }
                        }

                        bool hasItemDescription = false;
                        

                        //Loop BigTable
                        foreach (ItemDescriptionBigTable _quotationItemDescriptionBigTable in quotationItemDescriptionBigTable)
                        {
                            foreach (ItemDescriptionRow _quotationItemDesriptionRow in _quotationItemDescriptionBigTable.ItemDescriptionRows)
                            {

                                if (oldDataIsDraft == false)
                                {
                                    quotationItemDescriptionRecord.QuotationId = quotation.ID;
                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationId = oldData.ID;
                                }

                                quotationItemDescriptionRecord.CategoryName = _quotationItemDescriptionBigTable.CategoryName;
                                quotationItemDescriptionRecord.QuotationItem_Description = _quotationItemDesriptionRow.Description;
                                quotationItemDescriptionRecord.RowId = _quotationItemDescriptionBigTable.TableID;

                                if (_quotationItemDesriptionRow.Qty == "")
                                {
                                    _quotationItemDesriptionRow.Qty = "0";
                                    quotationItemDescriptionRecord.QuotationItemQty = 0;

                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemQty = Convert.ToDecimal(_quotationItemDesriptionRow.Qty);
                                }

                                if (_quotationItemDesriptionRow.Unit_Cost == "")
                                {
                                    _quotationItemDesriptionRow.Unit_Cost = "0";
                                    quotationItemDescriptionRecord.QuotationItemUnitCost = 0;

                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemUnitCost = Convert.ToDecimal(_quotationItemDesriptionRow.Unit_Cost);
                                }

                                if (string.IsNullOrEmpty(_quotationItemDesriptionRow.Unit))
                                {
                                    quotationItemDescriptionRecord.QuotationItemUnit = null;
                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemUnit = _quotationItemDesriptionRow.Unit;
                                }

                                quotationItemDescriptionRecord.QuotationItemRate = Convert.ToDecimal(_quotationItemDesriptionRow.Rate);

                                decimal Amount = Convert.ToDecimal(_quotationItemDesriptionRow.Rate) * Convert.ToDecimal(_quotationItemDesriptionRow.Qty);
                                quotationItemDescriptionRecord.QuotationItemAmount = Amount;
                                
                                decimal EstCost = Convert.ToDecimal(_quotationItemDesriptionRow.Qty) * Convert.ToDecimal(_quotationItemDesriptionRow.Unit_Cost);
                                quotationItemDescriptionRecord.QuotationItemEstCost = EstCost;

                                decimal EstProfit = Amount - EstCost;
                                quotationItemDescriptionRecord.QuotationItemEstProfit = EstProfit;

                                if (string.IsNullOrEmpty(_quotationItemDesriptionRow.Remarks))
                                {
                                    quotationItemDescriptionRecord.QuotationItemRemarks = null;
                                }
                                else
                                {
                                    //string newRemark = _quotationItemDesriptionRow.Remarks.Replace("\r\n", "|");
                                    quotationItemDescriptionRecord.QuotationItemRemarks = _quotationItemDesriptionRow.Remarks;
                                }

                                if (string.IsNullOrEmpty(_quotationItemDesriptionRow.Mgr_Comments))
                                {
                                    quotationItemDescriptionRecord.QuotationItemMgrComments = null;
                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemMgrComments = _quotationItemDesriptionRow.Mgr_Comments;
                                }
                                

                                bool add_quotation_item_description = _quotationItemDescriptionsModel.Add(quotationItemDescriptionRecord);

                                if (add_quotation_item_description)
                                {
                                    if (!hasItemDescription)
                                    {
                                        hasItemDescription = true;
                                    }
                                }
                            }
                        }

                        if (hasItemDescription)
                        {
                            userRole = Session["UserGroup"].ToString();
                            if (userRole == "Sales")
                            {
                                userRole += " " + Session["Position"].ToString();
                            }
                            tableAffected = "QuotationItemDescriptions";
                            description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Quotation Item Descriptions [" + quotation.QuotationID + "]";

                            bool itemDescription_create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                        }

                        //Check Status to verify want to send email or not
                        if (quotation.Status.Contains("Pending Manager Approval"))
                        {
                            if(UpdatedSalesPerson == "Yes")
                            {
                                //Search nearest Agency and email to them.
                                string Result2 = "";
                                int getAgencyLeader2 = 0;
                                int findAgencyID2 = (int)quotation.SalesPersonId;
                                do
                                {
                                    User searchAgencyLeader = _usersModel.GetSingle(findAgencyID2);

                                    if (searchAgencyLeader != null)
                                    {
                                        if (searchAgencyLeader.IsAgencyLeader != "Yes")
                                        {
                                            //if not agency leader, find their upline and search again.
                                            Result2 = "No";
                                            findAgencyID2 = searchAgencyLeader.UplineId;
                                        }
                                        else
                                        {
                                            Result2 = "Yes";
                                            getAgencyLeader2 = searchAgencyLeader.ID;
                                            ViewData["IsNearestAgency"] = "Yes";
                                            //Start To Email to the Agency Leader
                                            string subject = "Pending [" + quotation.QuotationID + "] Quotation Approval";
                                            string body = System.IO.File.ReadAllText(Server.MapPath("~/Templates/PendingQuotationNotification.html"));
                                            string recipient = searchAgencyLeader.Email;
                                            string name = searchAgencyLeader.Name;
                                            string quotationID = Convert.ToString(quotation.QuotationID);
                                            string status = quotation.Status;
                                            ListDictionary replacements = new ListDictionary();

                                            replacements.Add("<%Name%>", name);
                                            replacements.Add("<%QuotationID%>", quotationID);
                                            replacements.Add("<%Status%>", status);

                                            if (oldDataIsDraft == false)
                                            {
                                                replacements.Add("<%Url%>", Url.Action("Edit", "Quotation", new { @id = quotation.ID }, "http"));
                                            }
                                            else
                                            {
                                                replacements.Add("<%Url%>", Url.Action("Edit", "Quotation", new { @id = oldData.ID }, "http"));
                                            }

                                            bool sent = EmailHelper.SendEmail(subject, body, replacements, recipient);
                                        }
                                    }
                                    else
                                    {
                                        //if agency leader not found, send to super admin

                                        IList<User> getSuperAdminInfo = _usersModel.GetAll().Where(e => e.Role == "Super Admin").ToList();

                                        foreach (User _getSuperAdminInfo in getSuperAdminInfo)
                                        {
                                            //Start To Email to all super admin
                                            string subject = "Pending [" + quotation.QuotationID + "] Quotation Approval";
                                            string body = System.IO.File.ReadAllText(Server.MapPath("~/Templates/PendingQuotationNotification.html"));
                                            string recipient = _getSuperAdminInfo.Email;
                                            string name = _getSuperAdminInfo.Name;
                                            string quotationID = Convert.ToString(quotation.QuotationID);
                                            string status = quotation.Status;
                                            ListDictionary replacements = new ListDictionary();

                                            replacements.Add("<%Name%>", name);
                                            replacements.Add("<%QuotationID%>", quotationID);
                                            replacements.Add("<%Status%>", status);

                                            if (oldDataIsDraft == false)
                                            {
                                                replacements.Add("<%Url%>", Url.Action("Edit", "Quotation", new { @id = quotation.ID }, "http"));
                                            }
                                            else
                                            {
                                                replacements.Add("<%Url%>", Url.Action("Edit", "Quotation", new { @id = oldData.ID }, "http"));
                                            }

                                            bool sent = EmailHelper.SendEmail(subject, body, replacements, recipient);
                                        }

                                        Session.Add("Result", "success|" + quotation.QuotationID + " has been successfully edited but no agency leader found! The email will send to super admin");
                                        return RedirectToAction("Index");
                                    }
                                } while (Result2 != "Yes");
                            }
                        }

                        if (oldDataIsDraft != false)
                        {
                            Session.Add("Result", "success|" + quotation.QuotationID + " has been successfully created!");
                        }
                        else
                        {
                            if (quotation.Status == "Confirmed")
                            {
                                Session.Add("Result", "success|" + quotation.QuotationID + " has been successfully edited!");
                            }
                            else
                            {
                                Session.Add("Result", "success|" + oldData.QuotationID + " has been successfully created!");
                            }
                            
                        }
                        
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        Session.Add("Result", "danger|An error occured while edit quotation previous quotation records!");
                        return View();
                    }
                }
                else
                {
                    Session.Add("Result", "danger|An error occured while edit quotation records!");
                    return View();
                }
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            //assign back the current id to save as draft
            quotation.ID = id;

            //Get Quotation 
            ViewData["Quotation"] = quotation;

            //ViewData["CheckDraftButton"] = oldData.Status;

            ViewData["QuotationCoBrokeTable"] = quotationCoBrokeTable;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            Dropdown[] companyDDL = CompanyDDL();
            ViewData["CompanyDropdown"] = new SelectList(companyDDL, "val", "name", form["Company"]);

            Dropdown[] customerDDL = CustomerDDL(null, AssignSalesPersonId);
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", null);

            Dropdown[] userDDL = UserDDL();

            Dropdown[] ownAndDownlineDDL = OwnAndDownlineDDL();

            User getPreparedUser = _usersModel.GetSingle(MainID);

            if (getPreparedUser.Role != "Super Admin")
            {
                ViewData["UserDropdown"] = new SelectList(ownAndDownlineDDL, "val", "name", form["SalesPersonID"]);
            }
            else
            {
                ViewData["UserDropdown"] = new SelectList(userDDL, "val", "name", form["SalesPersonID"]);
            }

            //ViewData["UserDropdown"] = new SelectList(ownAndDownlineDDL, "val", "name", form["SalesPersonID"]);

            ViewData["SalesDocumentFiles"] = quotation.SalesDocumentUploads;

            //User getPreparedUser = _usersModel.GetSingle(oldData.PreparedById);

            ViewData["PreparedBy"] = getPreparedUser.Name;

            ViewData["QuotationDate"] = DateTime.Now.ToString("dd/MM/yyyy");

            ViewData["QuotationItemDescription"] = quotationItemDescriptionBigTable;

            Dropdown[] percentageDDL = PercentageOfProfitDDL();
            ViewData["PercentageOfProfitDropdown"] = new SelectList(percentageDDL, "val", "name", null);

            List<Quotation> getUserQuotations = _quotationsModel.GetUserQuotations(MainID);
            List<string> descriptionsArray = new List<string>();

            foreach (Quotation _getUserQuotations in getUserQuotations)
            {
                foreach (QuotationItemDescription _getQuotationItemDescriptions in _getUserQuotations.ItemDescriptions)
                {
                    if (!descriptionsArray.Contains(_getQuotationItemDescriptions.QuotationItem_Description))
                    {
                        descriptionsArray.Add(_getQuotationItemDescriptions.QuotationItem_Description);
                    }
                }
            }
            descriptionsArray = descriptionsArray.OrderBy(e => e).ToList();
            ViewData["AutoCompleteList"] = JsonConvert.SerializeObject(descriptionsArray);

            return View();
        }

        //GET: UpdateApplication ( For Approval Used )
        public ActionResult UpdateApplication(int id)
        {
            int MainID = Convert.ToInt32(Session["MainID"]);

            //Session.Add("CheckQuotationApproval", MainID);//main ID

            if (Session["CheckQuotationApproval"] != null)
            { 
                int CheckValue = Convert.ToInt32(Session["CheckQuotationApproval"].ToString());

                if (CheckValue != MainID)
                {
                    //Kick out from here
                    Session.Add("Result", "danger|You are not allow to update this application!");
                    return RedirectToAction("Index", "Quotation");
                }
            }
            else
            {
                //kick out from here
                //message for kick out
                Session.Add("Result", "danger|You are not allow to update this application!");
                return RedirectToAction("Index", "Quotation");
            }

            //This 2 TempData is from Reject Status PostBack.
            if (TempData["Remarks"] != null)
            {
                ModelState.AddModelError("quotation.Remarks", "Remarks is required when reject quotation");
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            if (TempData["SalesDocumentUpload"] != null)
            {
                ModelState.AddModelError("quotation.SalesDocumentUploads", "Sales Document is needed to upload!");
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            //Get Quotation 
            Quotation quotation = _quotationsModel.GetSingle(id);
            ViewData["Quotation"] = quotation;

            //Get Quotation CoBroke Item
            List<QuotationCoBroke> quotationCoBroke = _quotationCoBrokesModel.GetAllRelatedQuotationId(id);
            List<QuotationCoBrokeTable> quotationCoBrokeTable = new List<QuotationCoBrokeTable>();

            ViewData["QuotationCoBrokeTable"] = quotationCoBrokeTable;

            int count = 1;
            foreach (QuotationCoBroke _quotationCoBroke in quotationCoBroke)
            {
                quotationCoBrokeTable.Add(new QuotationCoBrokeTable()
                {
                    RowId = Convert.ToInt32(count),
                    UserID = new SelectList(UserDDL(), "val", "name", _quotationCoBroke.QuotationCoBrokeUserId),
                    PercentageOfProfit = new SelectList(PercentageOfProfitDDL(), "val", "name", _quotationCoBroke.QuotationCoBrokePercentOfProfit)
                });
                count++;
            }

            //New Get Quotation Item Description
            List<QuotationItemDescription> quotationItemDescription = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(id);
            List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();

            //Loop Item Description Items
            IList<SystemSetting> listItem = _settingsModel.GetAll();
            SystemSetting CategoryList = listItem.Where(e => e.Code == "QUOTATION_CATEGORIES").FirstOrDefault();

            string[] categoryList = CategoryList.Value.Split('|');

            //Start
            int countItemDescription = 1;
            int resetNumber = 1;
            foreach (QuotationItemDescription _quotationItemDescription in quotationItemDescription)//14 items
            {
                string ItemName = _quotationItemDescription.CategoryName;
                decimal ItemQty = _quotationItemDescription.QuotationItemQty;
                decimal ItemRate = _quotationItemDescription.QuotationItemRate;
                decimal ItemUnitCost = _quotationItemDescription.QuotationItemUnitCost;
                decimal ItemEstCost = _quotationItemDescription.QuotationItemEstCost;
                bool IsBasic = false; //default set to false means it is not from db item.

                List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                if (ItemQty == null)
                {
                    ItemQty = 0;
                }

                if (ItemRate == null)
                {
                    ItemRate = 0;
                }

                if (ItemUnitCost == null)
                {
                    ItemUnitCost = 0;
                }

                if (ItemEstCost == null)
                {
                    ItemEstCost = 0;
                }


                foreach (string _categoryList in categoryList)
                {
                    if (_categoryList == _quotationItemDescription.CategoryName)
                    {
                        IsBasic = true;
                    }
                }

                if (quotationItemDescriptionBigTable.Count == 0)
                {
                    resetNumber = 1;
                    ItemDescriptionRows.Add(new ItemDescriptionRow()
                    {
                        ID = _quotationItemDescription.ID,
                        RowId = resetNumber,
                        Description = _quotationItemDescription.QuotationItem_Description,
                        Qty = Convert.ToDecimal(_quotationItemDescription.QuotationItemQty).ToString("#,##0.00"),
                        Unit = _quotationItemDescription.QuotationItemUnit,
                        Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                        Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                        Unit_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemUnitCost).ToString("#,##0.00"),
                        Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                        AdjCost = null,
                        Est_Profit = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstProfit).ToString("#,##0.00"),
                        Remarks = _quotationItemDescription.QuotationItemRemarks,
                        Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                    });

                    quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                    {
                        TableID = countItemDescription,
                        CategoryName = _quotationItemDescription.CategoryName,
                        IsBasic = IsBasic,
                        ItemDescriptionRows = ItemDescriptionRows
                    });
                    countItemDescription++;
                }
                else
                {
                    if (quotationItemDescriptionBigTable.Where(e => e.CategoryName == _quotationItemDescription.CategoryName).FirstOrDefault() != null)
                    {
                        foreach (ItemDescriptionBigTable _loopBigTable in quotationItemDescriptionBigTable)
                        {
                            if (_loopBigTable.CategoryName == _quotationItemDescription.CategoryName)
                            {
                                _loopBigTable.ItemDescriptionRows.Add(new ItemDescriptionRow()
                                {
                                    ID = _quotationItemDescription.ID,
                                    RowId = resetNumber,
                                    Description = _quotationItemDescription.QuotationItem_Description,
                                    Qty = Convert.ToDecimal(_quotationItemDescription.QuotationItemQty).ToString("#,##0.00"),
                                    Unit = _quotationItemDescription.QuotationItemUnit,
                                    Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                                    Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                                    Unit_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemUnitCost).ToString("#,##0.00"),
                                    Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                                    AdjCost = null,
                                    Est_Profit = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstProfit).ToString("#,##0.00"),
                                    Remarks = _quotationItemDescription.QuotationItemRemarks,
                                    Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                                });
                                break;
                            }
                        }
                    }
                    else
                    {
                        resetNumber = 1;
                        ItemDescriptionRows.Add(new ItemDescriptionRow()
                        {
                            ID = _quotationItemDescription.ID,
                            RowId = resetNumber,
                            Description = _quotationItemDescription.QuotationItem_Description,
                            Qty = Convert.ToDecimal(_quotationItemDescription.QuotationItemQty).ToString("#,##0.00"),
                            Unit = _quotationItemDescription.QuotationItemUnit,
                            Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                            Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                            Unit_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemUnitCost).ToString("#,##0.00"),
                            Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                            AdjCost = null,
                            Est_Profit = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstProfit).ToString("#,##0.00"),
                            Remarks = _quotationItemDescription.QuotationItemRemarks,
                            Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                        });

                        quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                        {
                            TableID = countItemDescription,
                            CategoryName = _quotationItemDescription.CategoryName,
                            IsBasic = IsBasic,
                            ItemDescriptionRows = ItemDescriptionRows
                        });
                        countItemDescription++;
                    }
                }
                resetNumber++;
                //End
            }

            ViewData["QuotationItemDescription"] = quotationItemDescriptionBigTable;

            if(TempData["PostBack"] != null)
            {
                ViewData["QuotationItemDescription"] = TempData["quotationItemDescriptionBigTable"];
            }

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            ViewData["SalesDocumentFiles"] = quotation.SalesDocumentUploads;

            ViewData["PreparedBy"] = quotation.PreparedBy.Name;

            ViewData["QuotationDate"] = DateTime.Now.ToString("dd/MM/yyyy");

            //get company value and assign to dropdown.
            string CompanyValue = quotation.CompanyName + "-" + quotation.HasGST;

            Dropdown[] companyDDL = CompanyDDL();
            ViewData["CompanyDropdown"] = new SelectList(companyDDL, "val", "name", CompanyValue);

            Dropdown[] customerDDL = CustomerDDL(null, (int)quotation.CustomerId);
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", null);

            Dropdown[] userDDL = UserDDL();

            Dropdown[] ownAndDownlineDDL = OwnAndDownlineDDL();

            User getPreparedUser = _usersModel.GetSingle(MainID);

            if (getPreparedUser.Role != "Super Admin")
            {
                ViewData["UserDropdown"] = new SelectList(ownAndDownlineDDL, "val", "name", quotation.SalesPersonId);
            }
            else
            {
                ViewData["UserDropdown"] = new SelectList(userDDL, "val", "name", quotation.SalesPersonId);
            }

            //ViewData["UserDropdown"] = new SelectList(ownAndDownlineDDL, "val", "name", quotation.SalesPersonId);

            Dropdown[] percentageDDL = PercentageOfProfitDDL();
            ViewData["PercentageOfProfitDropdown"] = new SelectList(percentageDDL, "val", "name", null);

            List<Quotation> getUserQuotations = _quotationsModel.GetUserQuotations(MainID);
            List<string> descriptionsArray = new List<string>();

            foreach (Quotation _getUserQuotations in getUserQuotations)
            {
                foreach (QuotationItemDescription _getQuotationItemDescriptions in _getUserQuotations.ItemDescriptions)
                {
                    if (!descriptionsArray.Contains(_getQuotationItemDescriptions.QuotationItem_Description))
                    {
                        descriptionsArray.Add(_getQuotationItemDescriptions.QuotationItem_Description);
                    }
                }
            }
            descriptionsArray = descriptionsArray.OrderBy(e => e).ToList();
            ViewData["AutoCompleteList"] = JsonConvert.SerializeObject(descriptionsArray);

            return View();
        }

        //POST: EditDraft
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditDraft(int id, Quotation quotation, FormCollection form)
        {
            Quotation oldData = _quotationsModel.GetSingle(id);

            int MainID = Convert.ToInt32(Session["MainID"]);

            List<Quotation> getUserQuotations = _quotationsModel.GetUserQuotations(MainID);
            List<string> descriptionsArray = new List<string>();

            foreach (Quotation _getUserQuotations in getUserQuotations)
            {
                foreach (QuotationItemDescription _getQuotationItemDescriptions in _getUserQuotations.ItemDescriptions)
                {
                    if (!descriptionsArray.Contains(_getQuotationItemDescriptions.QuotationItem_Description))
                    {
                        descriptionsArray.Add(_getQuotationItemDescriptions.QuotationItem_Description);
                    }
                }
            }
            descriptionsArray = descriptionsArray.OrderBy(e => e).ToList();
            ViewData["AutoCompleteList"] = JsonConvert.SerializeObject(descriptionsArray);

            //Loop Item Description Items
            IList<SystemSetting> listItem = _settingsModel.GetAll();
            SystemSetting CategoryList = listItem.Where(e => e.Code == "QUOTATION_CATEGORIES").FirstOrDefault();

            string[] categoryList = CategoryList.Value.Split('|');
            int count = 0;

            foreach (string _categoryList in categoryList)
            {
                count++;
            }

            ViewData["TotalCategory"] = count;

            //Split Company Name and GST check

            if(form["Company"] != "")
            {
                string[] CompanyGSTCheck = form["Company"].Split('-');

                quotation.CompanyName = CompanyGSTCheck[0];
                quotation.HasGST = CompanyGSTCheck[1];
            }

            if (!string.IsNullOrEmpty(form["QuotationDate"]))
            {
                quotation.QuotationDate = Convert.ToDateTime(form["QuotationDate"].ToString());
            }

            //Quotation CoBroke Validation Part
            List<string> coBrokeKeys = form.AllKeys.Where(e => e.Contains("QuotationCoBrokePercentageOfProfit_")).ToList();

            List<QuotationCoBrokeTable> quotationCoBrokeTable = new List<QuotationCoBrokeTable>();

            int TotalPercentage = 0;
            int ItemCount = 0;

            foreach (string _coBrokeKeys in coBrokeKeys)
            {
                string rowId = _coBrokeKeys.Split('_')[1];

                string UserID = form["QuotationCoBrokeUser_" + rowId];
                string PercentageOfProfit = form["QuotationCoBrokePercentageOfProfit_" + rowId];

                TotalPercentage += Convert.ToInt32(PercentageOfProfit);

                if (coBrokeKeys.Count <= 3)
                {
                    quotationCoBrokeTable.Add(new QuotationCoBrokeTable()
                    {
                        RowId = Convert.ToInt32(rowId),
                        UserID = new SelectList(UserDDL(), "val", "name", UserID),
                        PercentageOfProfit = new SelectList(PercentageOfProfitDDL(), "val", "name", PercentageOfProfit)
                    });

                    ItemCount++;
                }
            }

            ViewData["CoBrokeItemCount"] = ItemCount;

            //Item Description Validatin Part
            List<string> ItemDescriptionKeys = form.AllKeys.Where(e => e.Contains("BigTableItemName-")).ToList();
            
            //For PostBack Use
            List<QuotationItemDescriptionTable> quotationItemDescriptionTable = new List<QuotationItemDescriptionTable>();

            foreach (string _ItemDescriptionKeys in ItemDescriptionKeys)
            {
                string rowId = _ItemDescriptionKeys.Split('-')[1];

                List<string> ItemDescriptionInsideKeys = form.AllKeys.Where(e => e.Contains("QuotationItemDescriptionDescription_" + rowId + "_")).ToList();

                //Foreach Item
                foreach (string _ItemDescriptionInsideKeys in ItemDescriptionInsideKeys)
                {
                    string insideRowId = _ItemDescriptionInsideKeys.Split('_')[1];
                    string insideRowId2 = _ItemDescriptionInsideKeys.Split('_')[2];

                    if (insideRowId == rowId)
                    {
                        string ItemName = form["BigTableItemName-" + insideRowId];
                        string Description = form["QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2];
                        string Qty = form["QuotationItemDescriptionQty_" + insideRowId + "_" + insideRowId2];
                        string Unit = form["QuotationItemDescriptionUnit_" + insideRowId + "_" + insideRowId2];
                        string Rate = form["QuotationItemDescriptionRate_" + insideRowId + "_" + insideRowId2];
                        string Amount = form["QuotationItemDescriptionAmount2_" + insideRowId + "_" + insideRowId2];
                        string Unit_Cost = form["QuotationItemDescriptionUnitCost_" + insideRowId + "_" + insideRowId2];
                        string Est_Cost = form["QuotationItemDescriptionEstCost2_" + insideRowId + "_" + insideRowId2];
                        string Est_Profit = form["QuotationItemDescriptionProfit2_" + insideRowId + "_" + insideRowId2];
                        string Remarks = form["QuotationItemDescriptionRemarks_" + insideRowId + "_" + insideRowId2];
                        string Mgr_Comments = form["QuotationItemDescriptionMgrComments_" + insideRowId + "_" + insideRowId2];

                        if (string.IsNullOrEmpty(Remarks))
                        {
                            Remarks = null;
                        }

                        if (string.IsNullOrEmpty(Mgr_Comments))
                        {
                            Mgr_Comments = null;
                        }

                        if (string.IsNullOrEmpty(Amount))
                        {
                            Amount = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Amount);

                            if (checkFormat)
                            {
                                Amount = Convert.ToDecimal(Amount).ToString("#,##0.00");
                            }
                        }

                        if (string.IsNullOrEmpty(Unit_Cost))
                        {
                            Unit_Cost = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Unit_Cost);

                            if (checkFormat)
                            {
                                Unit_Cost = Convert.ToDecimal(Unit_Cost).ToString("#,##0.00");
                            }
                        }

                        if (string.IsNullOrEmpty(Est_Cost))
                        {
                            Est_Cost = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Est_Cost);

                            if (checkFormat)
                            {
                                Est_Cost = Convert.ToDecimal(Est_Cost).ToString("#,##0.00");
                            }
                        }

                        if (string.IsNullOrEmpty(Est_Profit))
                        {
                            Est_Profit = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Est_Profit);

                            if (checkFormat)
                            {
                                Est_Profit = Convert.ToDecimal(Est_Profit).ToString("#,##0.00");
                            }
                        }

                        //Add Item if match
                        quotationItemDescriptionTable.Add(new QuotationItemDescriptionTable()
                        {
                            RowId = Convert.ToInt32(insideRowId),
                            ItemName = ItemName,
                            Description = Description,
                            Qty = Qty,
                            Unit = Unit,
                            Rate = Rate,
                            Amount = Amount,
                            Unit_Cost = Unit_Cost,
                            Est_Cost = Est_Cost,
                            AdjCost = "0.00",
                            Est_Profit = Est_Profit,
                            Remarks = Remarks,
                            Mgr_Comments = Mgr_Comments
                        });
                    }
                }
            }

            ViewData["CheckQuotationItemLength"] = "No";

            if (quotationItemDescriptionTable.Count > 0)
            {
                ViewData["CheckQuotationItemLength"] = "Yes";
            }

            if (ModelState.IsValid)
            {
                //For store Cobroke item
                QuotationCoBroke quotationCoBrokRecord = new QuotationCoBroke();

                //For store Item Description item
                QuotationItemDescription quotationItemDescriptionRecord = new QuotationItemDescription();

                //Assign Quotation ID
                quotation.QuotationID = oldData.QuotationID;

                int CustomerID = 0;

                if (quotation.CustomerId != null)
                {
                    CustomerID = (int)quotation.CustomerId;
                }

                Customer getCustomerInfo = _customersModel.GetSingle(CustomerID);

                if (getCustomerInfo != null)
                {
                    quotation.Address = getCustomerInfo.Address;
                    quotation.Tel = getCustomerInfo.Tel;
                    quotation.Fax = getCustomerInfo.Fax;
                }
                else
                {
                    quotation.Address = null;
                    quotation.Tel = null;
                    quotation.Fax = null;
                }

                quotation.PreparedById = MainID;

                if(form["SalesPersonID"] != "")
                {
                    string SalesPersonCheck = form["SalesPersonID"].ToString();

                    quotation.SalesPersonId = Convert.ToInt32(SalesPersonCheck);
                }

                if (!string.IsNullOrEmpty(quotation.SalesDocumentUploads))
                {
                    string[] SalesDocuments = quotation.SalesDocumentUploads.Split(',');

                    foreach (string file in SalesDocuments)
                    {
                        string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                        string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["QuotationSalesDocumentsFolder"].ToString()), file);

                        System.IO.File.Move(sourceFile, destinationFile);
                    }
                }
                else
                {
                    quotation.SalesDocumentUploads = oldData.SalesDocumentUploads;
                }

                quotation.QuotationID = oldData.QuotationID;
                quotation.Status = oldData.Status;
                quotation.ReviseQuotationTimes = oldData.ReviseQuotationTimes;
                quotation.VariationOrderTimes = oldData.VariationOrderTimes;

                bool result = _quotationsModel.UpdateData(id, quotation);

                if (result)
                {
                    bool update_quotation_previous_id = _quotationsModel.UpdatePreviousQuotationID(oldData.PreviousQuotationId);

                    if (update_quotation_previous_id)
                    {
                        string userRole = Session["UserGroup"].ToString();
                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }

                        string tableAffected = "Quotations";
                        string description = userRole + " [" + Session["LoginID"].ToString() + "] Edited Quotation [ Draft: " + quotation.ID + " ]";

                        bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                        //Remove All QuotationCoBroke Item First
                        //Get Database Quotation CoBroke Items based on quotation ID
                        IList<QuotationCoBroke> getQuotationCoBroke =  _quotationCoBrokesModel.GetPersonalAll(id);

                        foreach (QuotationCoBroke _getQuotationCoBroke in getQuotationCoBroke)
                        {
                            int quotationCoBrokeID = _getQuotationCoBroke.ID;
                            bool delete_quotation_coBroke = _quotationCoBrokesModel.Delete(quotationCoBrokeID);
                        }

                        bool hasCoBroke = false;

                        //Store CoBroke Item
                        foreach (QuotationCoBrokeTable _quotationCoBrokeTable in quotationCoBrokeTable)
                        {

                            quotationCoBrokRecord.QuotationId = oldData.ID;

                            if (_quotationCoBrokeTable.UserID.SelectedValue != "")
                            {
                                //determine cobroke user type
                                string UserTypeCheck = Convert.ToString(_quotationCoBrokeTable.UserID.SelectedValue);

                                quotationCoBrokRecord.QuotationCoBrokeUserId = Convert.ToInt32(UserTypeCheck);
                            }

                            quotationCoBrokRecord.QuotationCoBrokePercentOfProfit = Convert.ToInt32(_quotationCoBrokeTable.PercentageOfProfit.SelectedValue);

                            bool add_quotation_cobroke = _quotationCoBrokesModel.Add(quotationCoBrokRecord);

                            if (add_quotation_cobroke)
                            {
                                if (!hasCoBroke)
                                {
                                    hasCoBroke = true;
                                }
                            }
                        }

                        if (hasCoBroke)
                        {
                            userRole = Session["UserGroup"].ToString();
                            if (userRole == "Sales")
                            {
                                userRole += " " + Session["Position"].ToString();
                            }
                            tableAffected = "QuotationCoBrokes";
                            description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Quotation CoBrokes [ Draft: " + quotation.ID + " ]";

                            bool hasCoBroke_create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                        }

                        //Remove All QuotationItemDescription Item
                        //Get Database Quotation Items Description based on quotation ID
                        IList<QuotationItemDescription> getQuotationItemDescription = _quotationItemDescriptionsModel.GetPersonalAll(id);

                        foreach (QuotationItemDescription _getQuotationItemDescription in getQuotationItemDescription)
                        {
                            int quotationItemDescriptionID = _getQuotationItemDescription.ID;
                            bool delete_quotation_itemDescription = _quotationItemDescriptionsModel.Delete(quotationItemDescriptionID);
                        }

                        bool hasItemDescription = false;

                        foreach (QuotationItemDescriptionTable _quotationItemDescriptionTable in quotationItemDescriptionTable)
                        {

                            //decimal ItemQty = 0;
                            //decimal ItemRate = 0;
                            //decimal EstCost = 0;

                            quotationItemDescriptionRecord.QuotationId = oldData.ID;
                            quotationItemDescriptionRecord.RowId = _quotationItemDescriptionTable.RowId;
                            quotationItemDescriptionRecord.CategoryName = _quotationItemDescriptionTable.ItemName;
                            quotationItemDescriptionRecord.QuotationItem_Description = _quotationItemDescriptionTable.Description;

                            if (!string.IsNullOrEmpty(_quotationItemDescriptionTable.Qty))//got digit
                            {

                                decimal CheckQty;

                                if (Decimal.TryParse(_quotationItemDescriptionTable.Qty, out CheckQty))
                                {
                                    quotationItemDescriptionRecord.QuotationItemQty = Convert.ToDecimal(_quotationItemDescriptionTable.Qty);
                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemQty = 0;
                                    _quotationItemDescriptionTable.Qty = "0.0";
                                }

                            }
                            else //else is null
                            {
                                quotationItemDescriptionRecord.QuotationItemQty = 0;
                            }

                            //quotationItemDescriptionRecord.QuotationItemQty = ItemQty;
                            quotationItemDescriptionRecord.QuotationItemUnit = _quotationItemDescriptionTable.Unit;

                            if (!string.IsNullOrEmpty(_quotationItemDescriptionTable.Rate))
                            {
                                decimal CheckRate;

                                if (Decimal.TryParse(_quotationItemDescriptionTable.Rate, out CheckRate))
                                {
                                    quotationItemDescriptionRecord.QuotationItemRate = Convert.ToDecimal(_quotationItemDescriptionTable.Rate);
                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemRate = 0;
                                    _quotationItemDescriptionTable.Rate = "0.00";
                                }
                            }
                            else
                            {
                                quotationItemDescriptionRecord.QuotationItemRate = 0;
                            }

                            decimal Amount = quotationItemDescriptionRecord.QuotationItemRate * quotationItemDescriptionRecord.QuotationItemQty;
                            quotationItemDescriptionRecord.QuotationItemAmount = Amount;

                            if (!string.IsNullOrEmpty(_quotationItemDescriptionTable.Unit_Cost))
                            {
                                decimal CheckUnitCost;

                                if (Decimal.TryParse(_quotationItemDescriptionTable.Unit_Cost, out CheckUnitCost))
                                {
                                    quotationItemDescriptionRecord.QuotationItemUnitCost = Convert.ToDecimal(_quotationItemDescriptionTable.Unit_Cost);
                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemUnitCost = 0;
                                    _quotationItemDescriptionTable.Unit_Cost = "0.00";
                                }
                            }
                            else
                            {
                                quotationItemDescriptionRecord.QuotationItemUnitCost = 0;
                            }


                            decimal EstCost = quotationItemDescriptionRecord.QuotationItemQty * quotationItemDescriptionRecord.QuotationItemUnitCost;
                            quotationItemDescriptionRecord.QuotationItemEstCost = EstCost;

                            decimal EstProfit = Amount - quotationItemDescriptionRecord.QuotationItemEstCost;
                            quotationItemDescriptionRecord.QuotationItemEstProfit = EstProfit;

                            quotationItemDescriptionRecord.QuotationItemRemarks = _quotationItemDescriptionTable.Remarks;
                            quotationItemDescriptionRecord.QuotationItemMgrComments = _quotationItemDescriptionTable.Mgr_Comments;

                            bool add_quotation_item_description = _quotationItemDescriptionsModel.Add(quotationItemDescriptionRecord);

                            if (add_quotation_item_description)
                            {
                                if (!hasItemDescription)
                                {
                                    hasItemDescription = true;
                                }
                            }
                        }

                        if (hasItemDescription)
                        {
                            userRole = Session["UserGroup"].ToString();
                            if (userRole == "Sales")
                            {
                                userRole += " " + Session["Position"].ToString();
                            }
                            tableAffected = "QuotationItemDescriptions";
                            description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Quotation Item Descriptions [ Draft: " + quotation.ID + " ]";

                            bool itemDescription_create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                        }

                        Session.Add("Result", "success|has been successfully edited draft!");
                        return RedirectToAction("Index");

                    }
                    else
                    {
                        Session.Add("Result", "danger|An error occured while edit quotation previous quotation draft records!");
                        return View();
                    }
                }
                else
                {
                    Session.Add("Result", "danger|An error occured while edit quotation draft records!");
                    return View();
                }
            }
            return RedirectToAction("Index");
        }

        //Approve Quotation
        [HttpPost]
        public ActionResult ApproveQuotation(int id, Quotation quotation, FormCollection form)
        {
            int MainID = Convert.ToInt32(Session["MainID"]);

            Quotation oldData = _quotationsModel.GetSingle(id);

            //if(quotation.Status != "Pending Manager Approval")
            //{
            //    Session.Add("Result", "danger|Quotation " + quotation.QuotationID + " is not allow to do approval action!");

            //    return RedirectToAction("Index");
            //}

            string SalesDocumentUpload = oldData.SalesDocumentUploads;

            if (oldData.Status.Contains("Pending Customer Confirmation"))
            {
                if (string.IsNullOrEmpty(quotation.SalesDocumentUploads))
                {
                    ModelState.AddModelError("quotation.SalesDocumentUploads", "Sales Document is needed to upload!");
                }
                else
                {
                    if (quotation.SalesDocumentUploads != oldData.SalesDocumentUploads)
                    {
                        string[] SalesDocuments = quotation.SalesDocumentUploads.Split(',');

                        foreach (string file in SalesDocuments)
                        {
                            string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                            string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["QuotationSalesDocumentsFolder"].ToString()), file);

                            System.IO.File.Move(sourceFile, destinationFile);
                        }
                        SalesDocumentUpload = quotation.SalesDocumentUploads;
                    }
                }
            }

            //Check the Quotation Remarks and Comments
            //Repump the Quotation Items
            //Item Description Validatin Part
            List<string> ItemDescriptionKeys = form.AllKeys.Where(e => e.Contains("BigTableItemName-")).ToList();

            //For PostBack Use
            //List<QuotationItemDescriptionTable> quotationItemDescriptionTable = new List<QuotationItemDescriptionTable>();
            List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();

            foreach (string _ItemDescriptionKeys in ItemDescriptionKeys)
            {
                string rowId = _ItemDescriptionKeys.Split('-')[1];
                string ItemName = form["BigTableItemName-" + rowId];
                string IsBasic = form["BigTableItemNameFromDB-" + rowId];

                //Check From here
                List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                List<string> ItemDescriptionInsideKeys = form.AllKeys.Where(e => e.Contains("QuotationItemDescriptionDescription_" + rowId + "_")).ToList();

                //Foreach Item
                foreach (string _ItemDescriptionInsideKeys in ItemDescriptionInsideKeys)
                {
                    string insideRowId = _ItemDescriptionInsideKeys.Split('_')[1];
                    string insideRowId2 = _ItemDescriptionInsideKeys.Split('_')[2];

                    if (insideRowId == rowId)
                    {
                        string ID = form["QuotationItemDescriptionID_" + insideRowId + "_" + insideRowId2];
                        string Description = form["QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2];
                        string Qty = form["QuotationItemDescriptionQty_" + insideRowId + "_" + insideRowId2];
                        string Unit = form["QuotationItemDescriptionUnit_" + insideRowId + "_" + insideRowId2];
                        string Rate = form["QuotationItemDescriptionRate_" + insideRowId + "_" + insideRowId2];
                        string Amount = form["QuotationItemDescriptionAmount2_" + insideRowId + "_" + insideRowId2];
                        string Unit_Cost = form["QuotationItemDescriptionUnitCost2_" + insideRowId + "_" + insideRowId2];
                        string Est_Cost = form["QuotationItemDescriptionEstCost2_" + insideRowId + "_" + insideRowId2];
                        string Est_Profit = form["QuotationItemDescriptionProfit2_" + insideRowId + "_" + insideRowId2];
                        string Remarks = form["QuotationItemDescriptionRemarks_" + insideRowId + "_" + insideRowId2];
                        string Mgr_Comments = form["QuotationItemDescriptionMgrComments_" + insideRowId + "_" + insideRowId2];

                        //if same, then start validation. (Same Table Item)
                        if (string.IsNullOrEmpty(ItemName))
                        {
                            ModelState.AddModelError("BigTableItemName-" + insideRowId, "Item Name cannot be empty!");
                        }
                        else
                        {
                            if (quotationItemDescriptionBigTable.Where(e => e.CategoryName == ItemName).FirstOrDefault() != null)
                            {
                                ModelState.AddModelError("BigTableItemName" + insideRowId, "Item Name cannot be same!");
                            }
                        }

                        if (string.IsNullOrEmpty(Description))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2, "Item Description cannot be empty!");
                        }
                        else
                        {
                            if (ItemDescriptionRows.Where(e => e.Description == Description).FirstOrDefault() != null)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2, "Item Description cannot be same!");
                            }
                        }

                        if (string.IsNullOrEmpty(Remarks))
                        {
                            Remarks = null;
                        }

                        if (string.IsNullOrEmpty(Mgr_Comments))
                        {
                            Mgr_Comments = null;
                        }

                        if (string.IsNullOrEmpty(Amount))
                        {
                            Amount = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Amount);

                            if (checkFormat)
                            {
                                Amount = Convert.ToDecimal(Amount).ToString("#,##0.00");
                            }
                        }

                        if (string.IsNullOrEmpty(Est_Cost))
                        {
                            Est_Cost = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Est_Cost);

                            if (checkFormat)
                            {
                                Est_Cost = Convert.ToDecimal(Est_Cost).ToString("#,##0.00");
                            }
                        }

                        if (string.IsNullOrEmpty(Est_Profit))
                        {
                            Est_Profit = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Est_Profit);

                            if (checkFormat)
                            {
                                Est_Profit = Convert.ToDecimal(Est_Profit).ToString("#,##0.00");
                            }
                        }

                        ItemDescriptionRows.Add(new ItemDescriptionRow()
                        {
                            ID = Convert.ToInt32(ID),
                            RowId = Convert.ToInt32(insideRowId2),
                            Description = Description,
                            Qty = Qty,
                            Unit = Unit,
                            Rate = Rate,
                            Amount = Amount,
                            Unit_Cost = Unit_Cost,
                            Est_Cost = Est_Cost,
                            AdjCost = null,
                            Est_Profit = Est_Profit,
                            Remarks = Remarks,
                            Mgr_Comments = Mgr_Comments
                        });

                    }
                }

                bool ConvertIsBasic = true;

                if (IsBasic == "True")
                {
                    ConvertIsBasic = Convert.ToBoolean("True");
                }
                else
                {
                    ConvertIsBasic = Convert.ToBoolean("False");
                }

                quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                {
                    TableID = Convert.ToInt32(rowId),
                    CategoryName = ItemName,
                    IsBasic = Convert.ToBoolean(ConvertIsBasic),
                    ItemDescriptionRows = ItemDescriptionRows

                });
            }

            TempData["quotationItemDescriptionBigTable"] = quotationItemDescriptionBigTable;

            if(ModelState.IsValid)
            {
                //loop to get nearest agency
                //For loop to get the agency leader id to compare with login id.

                if (oldData.Status.Contains("Pending Manager Approval"))
                {

                    string Result = "";
                    int getAgencyLeader = 0;
                    int findAgencyID = (int)oldData.SalesPersonId;

                    ViewData["IsNearestAgency"] = "No";

                    do
                    {
                        User searchAgencyLeader = _usersModel.GetSingle(findAgencyID);

                        if (searchAgencyLeader != null)
                        {
                            if (searchAgencyLeader.IsAgencyLeader != "Yes")
                            {
                                //if not agency leader, find their upline and search again.
                                Result = "No";
                                findAgencyID = searchAgencyLeader.UplineId;
                            }
                            else
                            {
                                Result = "Yes";
                                getAgencyLeader = searchAgencyLeader.ID;

                                if (MainID == getAgencyLeader)
                                {
                                    //show approve reject button if match
                                    ViewData["IsNearestAgency"] = "Yes";
                                }

                            }
                        }
                        else
                        {
                            //if agency leader not found, check is super admin or not.
                            User getSuperAdmin = _usersModel.GetSingle((int)oldData.SalesPersonId);

                            if (getSuperAdmin.Role == "Super Admin")
                            {
                                //show approve reject button if match
                                ViewData["IsNearestAgency"] = "Yes";
                            }
                            Result = "Yes";
                        }
                    } while (Result != "Yes");
                }
                else
                {
                    ViewData["IsNearestAgency"] = "Yes";
                }
                
                if (ViewData["IsNearestAgency"].ToString() == "Yes")
                {
                    //if yes, do status update
                    string Status = oldData.Status;

                    if (oldData.Status.Contains("Pending Manager Approval"))
                    {
                        Status = "Pending Customer Confirmation ( " + oldData.SalesPerson.Name + " )";
                    }
                    else if (oldData.Status.Contains("Pending Customer Confirmation"))
                    {
                        Status = "Confirmed";
                    }

                    foreach (ItemDescriptionBigTable _quotationItemDescriptionBigTable2 in quotationItemDescriptionBigTable)
                    {
                        int description_id = 0;

                        if (_quotationItemDescriptionBigTable2.ItemDescriptionRows[0] != null)
                        {

                            if(_quotationItemDescriptionBigTable2.ItemDescriptionRows.Count > 1)
                            {

                                for (int count = 0; count < _quotationItemDescriptionBigTable2.ItemDescriptionRows.Count;count++ )
                                {
                                    description_id = _quotationItemDescriptionBigTable2.ItemDescriptionRows[count].ID;

                                    QuotationItemDescription updateItem = new QuotationItemDescription();

                                    updateItem.QuotationItemRemarks = _quotationItemDescriptionBigTable2.ItemDescriptionRows[count].Remarks;
                                    updateItem.QuotationItemMgrComments = _quotationItemDescriptionBigTable2.ItemDescriptionRows[count].Mgr_Comments;

                                    bool update_quotation_description_item = _quotationItemDescriptionsModel.UpdateData(description_id, updateItem);

                                }

                            }
                            else
                            {
                                description_id = _quotationItemDescriptionBigTable2.ItemDescriptionRows[0].ID;

                                QuotationItemDescription updateItem = new QuotationItemDescription();

                                updateItem.QuotationItemRemarks = _quotationItemDescriptionBigTable2.ItemDescriptionRows[0].Remarks;
                                updateItem.QuotationItemMgrComments = _quotationItemDescriptionBigTable2.ItemDescriptionRows[0].Mgr_Comments;

                                bool update_quotation_description_item = _quotationItemDescriptionsModel.UpdateData(description_id, updateItem);

                            }

                           
                            string userRole = Session["UserGroup"].ToString();
                            if (userRole == "Sales")
                            {
                                userRole += " " + Session["Position"].ToString();
                            }
                            string tableAffected = "QuotationItemDescriptions";
                            string description = userRole + " [" + Session["LoginID"].ToString() + "] Updated Quotation Item Descriptions [" + description_id + "]";

                            bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                        }

                    }

                    bool update_quotation_status = _quotationsModel.UpdateQuotationApprovalStatus(oldData.ID, Status, SalesDocumentUpload);

                    if (update_quotation_status)
                    {
                        string userRole = Session["UserGroup"].ToString();
                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }

                        string tableAffected = "Quotations";
                        string description = userRole + " [" + Session["LoginID"].ToString() + "] Approved Quotation [" + oldData.QuotationID + "]";

                        bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                        Session.Add("Result", "success|" + oldData.QuotationID + " approved!");

                        Session.Remove("CheckQuotationApproval");

                        //if status is confirmed, send email with quotation attachment to all super admin.
                        if(Status == "Confirmed")
                        {
                            return RedirectToAction("SendEmailAllSuperAdmin/" + id, "SendEmailQuotation");
                        }
                    }
                    else
                    {
                        Session.Add("Result", "danger|" + oldData.QuotationID + " failed to approved due to some issue!");
                    }

                }
                else
                {
                    Session.Add("Result", "danger|" + oldData.QuotationID + " failed to approved due to you are not the nearest agency leader!");
                }

                return RedirectToAction("Index", "Quotation");
            }
            else
            {
                TempData["SalesDocumentUpload"] = ModelState;
                TempData["PostBack"] = "Yes";
                return RedirectToAction("UpdateApplication/" + id, "Quotation");
            }
        }

        //Reject Quotation
        [HttpPost]
        public ActionResult RejectQuotation(int id, FormCollection form)
        {
            int MainID = Convert.ToInt32(Session["MainID"]);

            Quotation quotation = _quotationsModel.GetSingle(id);

            //loop to get nearest agency
            //For loop to get the agency leader id to compare with login id.

            ViewData["IsNearestAgency"] = "No";

            if (quotation.Status.Contains("Pending Manager Approval"))
            {
                string Result = "";
                int getAgencyLeader = 0;
                int findAgencyID = (int)quotation.SalesPersonId;

                do
                {
                    User searchAgencyLeader = _usersModel.GetSingle(findAgencyID);

                    if (searchAgencyLeader != null)
                    {
                        if (searchAgencyLeader.IsAgencyLeader != "Yes")
                        {
                            //if not agency leader, find their upline and search again.
                            Result = "No";
                            findAgencyID = searchAgencyLeader.UplineId;
                        }
                        else
                        {
                            Result = "Yes";
                            getAgencyLeader = searchAgencyLeader.ID;

                            if (MainID == getAgencyLeader)
                            {
                                //show approve reject button if match
                                ViewData["IsNearestAgency"] = "Yes";
                            }

                        }
                    }
                    else
                    {
                        //if agency leader not found, check is super admin or not.
                        User getSuperAdmin = _usersModel.GetSingle((int)quotation.SalesPersonId);

                        if (getSuperAdmin.Role == "Super Admin")
                        {
                            //show approve reject button if match
                            ViewData["IsNearestAgency"] = "Yes";
                        }
                        Result = "Yes";
                    }
                } while (Result != "Yes");
            }

            //check remarks field is fill in or not
            if (string.IsNullOrEmpty(form["quotation.Remarks"]))
            {
                ModelState.AddModelError("quotation.Remarks", "Remarks is required when rejecting quotation!");
            }

            //Repump the Quotation Items
            //Item Description Validatin Part
            List<string> ItemDescriptionKeys = form.AllKeys.Where(e => e.Contains("BigTableItemName-")).ToList();

            //For PostBack Use
            //List<QuotationItemDescriptionTable> quotationItemDescriptionTable = new List<QuotationItemDescriptionTable>();
            List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();

            foreach (string _ItemDescriptionKeys in ItemDescriptionKeys)
            {
                string rowId = _ItemDescriptionKeys.Split('-')[1];
                string ItemName = form["BigTableItemName-" + rowId];
                string IsBasic = form["BigTableItemNameFromDB-" + rowId];

                //Check From here
                List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                List<string> ItemDescriptionInsideKeys = form.AllKeys.Where(e => e.Contains("QuotationItemDescriptionDescription_" + rowId + "_")).ToList();

                //Foreach Item
                foreach (string _ItemDescriptionInsideKeys in ItemDescriptionInsideKeys)
                {
                    string insideRowId = _ItemDescriptionInsideKeys.Split('_')[1];
                    string insideRowId2 = _ItemDescriptionInsideKeys.Split('_')[2];

                    if (insideRowId == rowId)
                    {
                        string ID = form["QuotationItemDescriptionID_" + insideRowId + "_" + insideRowId2];
                        string Description = form["QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2];
                        string Qty = form["QuotationItemDescriptionQty_" + insideRowId + "_" + insideRowId2];
                        string Unit = form["QuotationItemDescriptionUnit_" + insideRowId + "_" + insideRowId2];
                        string Rate = form["QuotationItemDescriptionRate_" + insideRowId + "_" + insideRowId2];
                        string Amount = form["QuotationItemDescriptionAmount2_" + insideRowId + "_" + insideRowId2];
                        string Unit_Cost = form["QuotationItemDescriptionUnitCost2_" + insideRowId + "_" + insideRowId2];
                        string Est_Cost = form["QuotationItemDescriptionEstCost2_" + insideRowId + "_" + insideRowId2];
                        string Est_Profit = form["QuotationItemDescriptionProfit2_" + insideRowId + "_" + insideRowId2];
                        string Remarks = form["QuotationItemDescriptionRemarks_" + insideRowId + "_" + insideRowId2];
                        string Mgr_Comments = form["QuotationItemDescriptionMgrComments_" + insideRowId + "_" + insideRowId2];

                        //if same, then start validation. (Same Table Item)
                        if (string.IsNullOrEmpty(ItemName))
                        {
                            ModelState.AddModelError("BigTableItemName-" + insideRowId, "Item Name cannot be empty!");
                        }
                        else
                        {
                            if (quotationItemDescriptionBigTable.Where(e => e.CategoryName == ItemName).FirstOrDefault() != null)
                            {
                                ModelState.AddModelError("BigTableItemName" + insideRowId, "Item Name cannot be same!");
                            }
                        }

                        if (string.IsNullOrEmpty(Description))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2, "Item Description cannot be empty!");
                        }
                        else
                        {
                            if (ItemDescriptionRows.Where(e => e.Description == Description).FirstOrDefault() != null)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2, "Item Description cannot be same!");
                            }
                        }

                        if (string.IsNullOrEmpty(Remarks))
                        {
                            Remarks = null;
                        }

                        if (string.IsNullOrEmpty(Mgr_Comments))
                        {
                            Mgr_Comments = null;
                        }

                        if (string.IsNullOrEmpty(Amount))
                        {
                            Amount = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Amount);

                            if (checkFormat)
                            {
                                Amount = Convert.ToDecimal(Amount).ToString("#,##0.00");
                            }
                        }

                        if (string.IsNullOrEmpty(Est_Cost))
                        {
                            Est_Cost = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Est_Cost);

                            if (checkFormat)
                            {
                                Est_Cost = Convert.ToDecimal(Est_Cost).ToString("#,##0.00");
                            }
                        }

                        if (string.IsNullOrEmpty(Est_Profit))
                        {
                            Est_Profit = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Est_Profit);

                            if (checkFormat)
                            {
                                Est_Profit = Convert.ToDecimal(Est_Profit).ToString("#,##0.00");
                            }
                        }

                        ItemDescriptionRows.Add(new ItemDescriptionRow()
                        {
                            ID = Convert.ToInt32(ID),
                            RowId = Convert.ToInt32(insideRowId2),
                            Description = Description,
                            Qty = Qty,
                            Unit = Unit,
                            Rate = Rate,
                            Amount = Amount,
                            Unit_Cost = Unit_Cost,
                            Est_Cost = Est_Cost,
                            AdjCost = null,
                            Est_Profit = Est_Profit,
                            Remarks = Remarks,
                            Mgr_Comments = Mgr_Comments
                        });

                    }
                }

                bool ConvertIsBasic = true;

                if (IsBasic == "True")
                {
                    ConvertIsBasic = Convert.ToBoolean("True");
                }
                else
                {
                    ConvertIsBasic = Convert.ToBoolean("False");
                }

                quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                {
                    TableID = Convert.ToInt32(rowId),
                    CategoryName = ItemName,
                    IsBasic = Convert.ToBoolean(ConvertIsBasic),
                    ItemDescriptionRows = ItemDescriptionRows

                });
            }

            TempData["quotationItemDescriptionBigTable"] = quotationItemDescriptionBigTable;

            if(ModelState.IsValid)
            {

                if (quotation.Status.Contains("Pending Manager Approval"))
                {
                    if (ViewData["IsNearestAgency"].ToString() == "Yes")
                    {
                        //if yes, do status update
                        string status = quotation.Status;

                        //Check is pending manager or customer part.
                        if (quotation.Status.Contains("Pending Manager Approval"))
                        {
                            status = "Pending Sales Revision ( " + quotation.SalesPerson.Name + " )";
                        }
                        else if (quotation.Status.Contains("Pending Customer Confirmation"))
                        {
                            status = "Cancelled by Customer";
                        }

                        quotation.Status = status;
                        quotation.Remarks = form["quotation.Remarks"].ToString();

                        foreach (ItemDescriptionBigTable _quotationItemDescriptionBigTable2 in quotationItemDescriptionBigTable)
                        {
                            int description_id = 0;

                            if (_quotationItemDescriptionBigTable2.ItemDescriptionRows[0] != null)
                            {
                                QuotationItemDescription updateItem = new QuotationItemDescription();

                                if (_quotationItemDescriptionBigTable2.ItemDescriptionRows.Count > 0)
                                {
                                    for (int count = 0; count < _quotationItemDescriptionBigTable2.ItemDescriptionRows.Count; count++)
                                    {
                                        description_id = _quotationItemDescriptionBigTable2.ItemDescriptionRows[count].ID;

                                        updateItem.QuotationItemRemarks = _quotationItemDescriptionBigTable2.ItemDescriptionRows[count].Remarks;
                                        updateItem.QuotationItemMgrComments = _quotationItemDescriptionBigTable2.ItemDescriptionRows[count].Mgr_Comments;

                                        bool update_quotation_description_item = _quotationItemDescriptionsModel.UpdateData(description_id, updateItem);
                                    }
                                }
                                else
                                {
                                    description_id = _quotationItemDescriptionBigTable2.ItemDescriptionRows[0].ID;

                                    updateItem.QuotationItemRemarks = _quotationItemDescriptionBigTable2.ItemDescriptionRows[0].Remarks;
                                    updateItem.QuotationItemMgrComments = _quotationItemDescriptionBigTable2.ItemDescriptionRows[0].Mgr_Comments;

                                    bool update_quotation_description_item = _quotationItemDescriptionsModel.UpdateData(description_id, updateItem);
                                }
                                
                                string userRole = Session["UserGroup"].ToString();
                                if (userRole == "Sales")
                                {
                                    userRole += " " + Session["Position"].ToString();
                                }
                                string tableAffected = "QuotationItemDescriptions";
                                string description = userRole + " [" + Session["LoginID"].ToString() + "] Updated Quotation Item Descriptions [" + description_id + "]";

                                bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                            }

                        }

                        bool update_quotation_status = _quotationsModel.UpdateData(id, quotation);

                        if (update_quotation_status)
                        {
                            string userRole = Session["UserGroup"].ToString();
                            if (userRole == "Sales")
                            {
                                userRole += " " + Session["Position"].ToString();
                            }
                            string tableAffected = "Quotations";
                            string description = userRole + " [" + Session["LoginID"].ToString() + "] Rejected Quotation [" + quotation.QuotationID + "]";

                            bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                            Session.Add("Result", "success|" + quotation.QuotationID + " rejected!");
                            Session.Remove("CheckQuotationApproval");
                        }
                        else
                        {
                            Session.Add("Result", "danger|" + quotation.QuotationID + " failed to reject due to some issue!");
                        }

                    }
                    else
                    {
                        Session.Add("Result", "danger|" + quotation.QuotationID + " failed to reject due to you are not the nearest agency leader!");
                    }
                }
                else
                {
                    //if yes, do status update
                    string status = quotation.Status;

                    //Check is pending manager or customer part.
                    if (quotation.Status.Contains("Pending Manager Approval"))
                    {
                        status = "Pending Sales Revision ( " + quotation.SalesPerson.Name + " )";
                    }
                    else if (quotation.Status.Contains("Pending Customer Confirmation"))
                    {
                        status = "Cancelled by Customer";
                    }

                    quotation.Status = status;
                    quotation.Remarks = form["quotation.Remarks"].ToString();

                    //Update Quotation Item first

                    foreach(ItemDescriptionBigTable _quotationItemDescriptionBigTable2 in quotationItemDescriptionBigTable)
                    {
                        int description_id = 0;

                        if(_quotationItemDescriptionBigTable2.ItemDescriptionRows[0] != null)
                        {

                            QuotationItemDescription updateItem = new QuotationItemDescription();

                            if(_quotationItemDescriptionBigTable2.ItemDescriptionRows.Count > 0)
                            {
                                
                                for(int count = 0; count < _quotationItemDescriptionBigTable2.ItemDescriptionRows.Count; count++)
                                {
                                    description_id = _quotationItemDescriptionBigTable2.ItemDescriptionRows[count].ID;

                                    updateItem.QuotationItemRemarks = _quotationItemDescriptionBigTable2.ItemDescriptionRows[count].Remarks;
                                    updateItem.QuotationItemMgrComments = _quotationItemDescriptionBigTable2.ItemDescriptionRows[count].Mgr_Comments;

                                    bool update_quotation_description_item = _quotationItemDescriptionsModel.UpdateData(description_id, updateItem);
                                }
                                
                            }
                            else
                            {
                                description_id = _quotationItemDescriptionBigTable2.ItemDescriptionRows[0].ID;

                                updateItem.QuotationItemRemarks = _quotationItemDescriptionBigTable2.ItemDescriptionRows[0].Remarks;
                                updateItem.QuotationItemMgrComments = _quotationItemDescriptionBigTable2.ItemDescriptionRows[0].Mgr_Comments;

                                bool update_quotation_description_item = _quotationItemDescriptionsModel.UpdateData(description_id, updateItem);

                            }

                            string userRole = Session["UserGroup"].ToString();
                            if (userRole == "Sales")
                            {
                                userRole += " " + Session["Position"].ToString();
                            }
                            string tableAffected = "QuotationItemDescriptions";
                            string description = userRole + " [" + Session["LoginID"].ToString() + "] Updated Quotation Item Descriptions [" + description_id + "]";

                            bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                        }

                    }

                    bool update_quotation_status = _quotationsModel.UpdateData(id, quotation);

                    if (update_quotation_status)
                    {
                        string userRole = Session["UserGroup"].ToString();
                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }
                        string tableAffected = "Quotations";
                        string description = userRole + " [" + Session["LoginID"].ToString() + "] Rejected Quotation [" + quotation.QuotationID + "]";

                        bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                        Session.Add("Result", "success|" + quotation.QuotationID + " rejected!");
                        Session.Remove("CheckQuotationApproval");
                    }
                    else
                    {
                        Session.Add("Result", "danger|" + quotation.QuotationID + " failed to reject due to some issue!");
                    }
                }
            }
            else
            {
                TempData["Remarks"] = ModelState;
                TempData["PostBack"] = "Yes";
                return RedirectToAction("UpdateApplication/" + id, "Quotation");
            }

            return RedirectToAction("Index", "Quotation");
        }

        //Resubmmit Application
        public ActionResult ResubmmitApplication(int id)
        {
            int MainID = Convert.ToInt32(Session["MainID"]);

            //here is to check whether the login user is able to approve or not
            //if yes, redirect to updateApplication page
            //if no, continue this page.

            //Get Quotation 
            Quotation quotation = _quotationsModel.GetSingle(id);
            ViewData["Quotation"] = quotation;

            //This is for Pending Customer Confirmation Used
            if (!quotation.Status.Contains("Pending Sales Revision") && quotation.Status != "Cancelled by Customer")
            {
                Session.Add("Result", "danger|You are not allow to resubmmit this application!");
                return RedirectToAction("Index", "Quotation");
            }
            else
            {
                if (quotation.SalesPersonId != MainID)
                {
                    Session.Add("Result", "danger|You are not allow to resubmmit this application!");
                    return RedirectToAction("Index", "Quotation");
                }
            }

            //Get Quotation CoBroke Item
            List<QuotationCoBroke> quotationCoBroke = _quotationCoBrokesModel.GetAllRelatedQuotationId(id);
            List<QuotationCoBrokeTable> quotationCoBrokeTable = new List<QuotationCoBrokeTable>();

            ViewData["QuotationCoBrokeTable"] = quotationCoBrokeTable;

            int count = 1;
            foreach (QuotationCoBroke _quotationCoBroke in quotationCoBroke)
            {
                quotationCoBrokeTable.Add(new QuotationCoBrokeTable()
                {
                    RowId = Convert.ToInt32(count),
                    UserID = new SelectList(UserDDL(), "val", "name", _quotationCoBroke.QuotationCoBrokeUserId),
                    PercentageOfProfit = new SelectList(PercentageOfProfitDDL(), "val", "name", _quotationCoBroke.QuotationCoBrokePercentOfProfit)
                });
                count++;
            }

            //New Get Quotation Item Description
            List<QuotationItemDescription> quotationItemDescription = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(id);
            List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();

            //Loop Item Description Items
            IList<SystemSetting> listItem = _settingsModel.GetAll();
            SystemSetting CategoryList = listItem.Where(e => e.Code == "QUOTATION_CATEGORIES").FirstOrDefault();

            string[] categoryList = CategoryList.Value.Split('|');

            //Start
            int countItemDescription = 1;
            int resetNumber = 1;
            foreach (QuotationItemDescription _quotationItemDescription in quotationItemDescription)//14 items
            {
                string ItemName = _quotationItemDescription.CategoryName;
                decimal ItemQty = _quotationItemDescription.QuotationItemQty;
                decimal ItemRate = _quotationItemDescription.QuotationItemRate;
                decimal ItemUnitCost = _quotationItemDescription.QuotationItemUnitCost;
                decimal ItemEstCost = _quotationItemDescription.QuotationItemEstCost;
                bool IsBasic = false; //default set to false means it is not from db item.

                List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                if (ItemQty == null)
                {
                    ItemQty = 0;
                }

                if (ItemRate == null)
                {
                    ItemRate = 0;
                }

                if(ItemUnitCost == null)
                {
                    ItemUnitCost = 0;
                }

                if (ItemEstCost == null)
                {
                    ItemEstCost = 0;
                }


                foreach (string _categoryList in categoryList)
                {
                    if (_categoryList == _quotationItemDescription.CategoryName)
                    {
                        IsBasic = true;
                    }
                }

                if (quotationItemDescriptionBigTable.Count == 0)
                {
                    resetNumber = 1;
                    ItemDescriptionRows.Add(new ItemDescriptionRow()
                    {
                        RowId = resetNumber,
                        Description = _quotationItemDescription.QuotationItem_Description,
                        Qty = Convert.ToDecimal(_quotationItemDescription.QuotationItemQty).ToString("#,##0.0"),
                        Unit = _quotationItemDescription.QuotationItemUnit,
                        Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                        Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                        Unit_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemUnitCost).ToString("#,##0.00"),
                        Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                        AdjCost = null,
                        Est_Profit = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstProfit).ToString("#,##0.00"),
                        Remarks = _quotationItemDescription.QuotationItemRemarks,
                        Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                    });

                    quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                    {
                        TableID = countItemDescription,
                        CategoryName = _quotationItemDescription.CategoryName,
                        IsBasic = IsBasic,
                        ItemDescriptionRows = ItemDescriptionRows
                    });
                    countItemDescription++;
                }
                else
                {
                    if (quotationItemDescriptionBigTable.Where(e => e.CategoryName == _quotationItemDescription.CategoryName).FirstOrDefault() != null)
                    {
                        foreach (ItemDescriptionBigTable _loopBigTable in quotationItemDescriptionBigTable)
                        {
                            if (_loopBigTable.CategoryName == _quotationItemDescription.CategoryName)
                            {
                                _loopBigTable.ItemDescriptionRows.Add(new ItemDescriptionRow()
                                {
                                    RowId = resetNumber,
                                    Description = _quotationItemDescription.QuotationItem_Description,
                                    Qty = Convert.ToDecimal(_quotationItemDescription.QuotationItemQty).ToString("#,##0.0"),
                                    Unit = _quotationItemDescription.QuotationItemUnit,
                                    Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                                    Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                                    Unit_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemUnitCost).ToString("#,##0.00"),
                                    Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                                    AdjCost = null,
                                    Est_Profit = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstProfit).ToString("#,##0.00"),
                                    Remarks = _quotationItemDescription.QuotationItemRemarks,
                                    Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                                });
                                break;
                            }
                        }
                    }
                    else
                    {
                        resetNumber = 1;
                        ItemDescriptionRows.Add(new ItemDescriptionRow()
                        {
                            RowId = resetNumber,
                            Description = _quotationItemDescription.QuotationItem_Description,
                            Qty = Convert.ToDecimal(_quotationItemDescription.QuotationItemQty).ToString("#,##0.0"),
                            Unit = _quotationItemDescription.QuotationItemUnit,
                            Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                            Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                            Unit_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemUnitCost).ToString("#,##0.00"),
                            Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                            AdjCost = null,
                            Est_Profit = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstProfit).ToString("#,##0.00"),
                            Remarks = _quotationItemDescription.QuotationItemRemarks,
                            Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                        });

                        quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                        {
                            TableID = countItemDescription,
                            CategoryName = _quotationItemDescription.CategoryName,
                            IsBasic = IsBasic,
                            ItemDescriptionRows = ItemDescriptionRows
                        });
                        countItemDescription++;
                    }
                }
                resetNumber++;
                //End
            }

            ViewData["QuotationItemDescription"] = quotationItemDescriptionBigTable;

            ViewData["CheckQuotationItemLength"] = "No";

            if (quotationItemDescriptionBigTable.Count > 0)
            {
                ViewData["CheckQuotationItemLength"] = "Yes";
            }

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            ViewData["SalesDocumentFiles"] = quotation.SalesDocumentUploads;

            ViewData["PreparedBy"] = quotation.PreparedBy.Name;

            ViewData["QuotationDate"] = Convert.ToDateTime(quotation.QuotationDate).ToString("dd/MM/yyyy");

            //get company value and assign to dropdown.
            string CompanyValue = quotation.CompanyName + "-" + quotation.HasGST;

            Dropdown[] companyDDL = CompanyDDL();
            ViewData["CompanyDropdown"] = new SelectList(companyDDL, "val", "name", CompanyValue);

            Dropdown[] customerDDL = CustomerDDL(null, (int)quotation.CustomerId);
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", null);

            Dropdown[] userDDL = UserDDL();

            Dropdown[] ownAndDownlineDDL = OwnAndDownlineDDL();
            ViewData["UserDropdown"] = new SelectList(ownAndDownlineDDL, "val", "name", quotation.SalesPersonId);

            Dropdown[] percentageDDL = PercentageOfProfitDDL();
            ViewData["PercentageOfProfitDropdown"] = new SelectList(percentageDDL, "val", "name", null);

            List<Quotation> getUserQuotations = _quotationsModel.GetUserQuotations(MainID);
            List<string> descriptionsArray = new List<string>();

            foreach (Quotation _getUserQuotations in getUserQuotations)
            {
                foreach (QuotationItemDescription _getQuotationItemDescriptions in _getUserQuotations.ItemDescriptions)
                {
                    if (!descriptionsArray.Contains(_getQuotationItemDescriptions.QuotationItem_Description))
                    {
                        descriptionsArray.Add(_getQuotationItemDescriptions.QuotationItem_Description);
                    }
                }
            }
            descriptionsArray = descriptionsArray.OrderBy(e => e).ToList();
            ViewData["AutoCompleteList"] = JsonConvert.SerializeObject(descriptionsArray);

            return View();
        }

        //POST: Resubmmit Application
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ResubmmitApplication(int id, Quotation quotation, FormCollection form)
        {

            //Get OldData
            Quotation oldData = _quotationsModel.GetSingle(id);

            quotation.QuotationID = oldData.QuotationID;
            quotation.CompanyName = oldData.CompanyName;
            quotation.HasGST = oldData.HasGST;
            quotation.PreparedById = oldData.PreparedById;
            quotation.Status = oldData.Status;
            quotation.Tel = quotation.Tel;

            int MainID = Convert.ToInt32(Session["MainID"]);

            //Loop Item Description Items
            IList<SystemSetting> listItem = _settingsModel.GetAll();
            SystemSetting CategoryList = listItem.Where(e => e.Code == "QUOTATION_CATEGORIES").FirstOrDefault();

            string[] categoryList = CategoryList.Value.Split('|');
            int count = 0;

            foreach (string _categoryList in categoryList)
            {
                count++;
            }

            ViewData["TotalCategory"] = count;

            //Validation Part
            if (quotation.CustomerId == null)
            {
                ModelState.AddModelError("quotation.CustomerId", "Customer is required!");
            }

            if (string.IsNullOrEmpty(quotation.ContactPerson))
            {
                ModelState.AddModelError("quotation.ContactPerson", "Contact Person is required!");
            }

            if (quotation.QuotationDate == null)
            {
                ModelState.AddModelError("quotation.QuotationDate", "Date is required!");
            }

            if (string.IsNullOrEmpty(form["SalesPersonID"]))
            {
                ModelState.AddModelError("SalesPersonID", "Sales Person is required!");
            }
            else
            {
                string SalesPersonCheck = Convert.ToString(form["SalesPersonID"]);

                quotation.SalesPersonId = Convert.ToInt32(SalesPersonCheck);
            }

            if (oldData.Status.Contains("Pending Sales Revision") || oldData.Status == "Cancelled by Customer")
            {
                if (string.IsNullOrEmpty(quotation.Remarks))
                {
                    ModelState.AddModelError("quotation.Remarks", "Remarks is required!");
                }

                if (string.IsNullOrEmpty(quotation.SalesDocumentUploads))
                {
                    ModelState.AddModelError("quotation.SalesDocumentUploads", "Sales Document Upload is required!");
                }
            }

            string[] CompanyGSTCheck = null;
            //Split Company Name and GST check
            if (form["Company"] != "")
            {
                CompanyGSTCheck = form["Company"].Split('-');

                if (string.IsNullOrEmpty(CompanyGSTCheck[0]))
                {
                    ModelState.AddModelError("Company", "Company Name not found!");
                }

                if (string.IsNullOrEmpty(CompanyGSTCheck[1]))
                {
                    ModelState.AddModelError("Company", "Company GST not found!");
                }
            }
            else
            {
                ModelState.AddModelError("Company", "Company is required!");
            }

            //Quotation CoBroke Validation Part
            List<string> coBrokeKeys = form.AllKeys.Where(e => e.Contains("QuotationCoBrokePercentageOfProfit_")).ToList();

            List<QuotationCoBrokeTable> quotationCoBrokeTable = new List<QuotationCoBrokeTable>();

            int TotalPercentage = 0;
            int ItemCount = 0;

            foreach (string _coBrokeKeys in coBrokeKeys)
            {
                string rowId = _coBrokeKeys.Split('_')[1];

                string UserID = form["QuotationCoBrokeUser_" + rowId];
                string PercentageOfProfit = form["QuotationCoBrokePercentageOfProfit_" + rowId];

                if (string.IsNullOrEmpty(UserID))
                {
                    ModelState.AddModelError("QuotationCoBrokeUser_" + rowId, "User is required!");
                }

                if (string.IsNullOrEmpty(PercentageOfProfit))
                {
                    ModelState.AddModelError("QuotationCoBrokePercentageOfProfit_" + rowId, "Profit is required!");
                }

                TotalPercentage += Convert.ToInt32(PercentageOfProfit);

                if (coBrokeKeys.Count <= 3)
                {
                    quotationCoBrokeTable.Add(new QuotationCoBrokeTable()
                    {
                        RowId = Convert.ToInt32(rowId),
                        UserID = new SelectList(UserDDL(), "val", "name", UserID),
                        PercentageOfProfit = new SelectList(PercentageOfProfitDDL(), "val", "name", PercentageOfProfit)
                    });

                    ItemCount++;
                }
                else
                {
                    //if more than 3 items, error message prompt
                    ModelState.AddModelError("TotalOfCoBrokeItem", "Maximum 3 Co-broke!");
                }

            }

            ViewData["CoBrokeItemCount"] = ItemCount;

            //Calculate Total of Percentage is equal to 100%
            if (TotalPercentage != 100)
            {
                ModelState.AddModelError("TotalOfPercentage", "Total of Profit must equal to 100%!");
            }

            //Item Description Validatin Part
            List<string> ItemDescriptionKeys = form.AllKeys.Where(e => e.Contains("BigTableItemName-")).ToList();

            //For PostBack Use
            //List<QuotationItemDescriptionTable> quotationItemDescriptionTable = new List<QuotationItemDescriptionTable>();
            List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();

            foreach (string _ItemDescriptionKeys in ItemDescriptionKeys)
            {
                string rowId = _ItemDescriptionKeys.Split('-')[1];
                string ItemName = form["BigTableItemName-" + rowId];
                string IsBasic = form["BigTableItemNameFromDB-" + rowId];

                //Check From here
                List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                List<string> ItemDescriptionInsideKeys = form.AllKeys.Where(e => e.Contains("QuotationItemDescriptionDescription_" + rowId + "_")).ToList();

                //Foreach Item
                foreach (string _ItemDescriptionInsideKeys in ItemDescriptionInsideKeys)
                {
                    string insideRowId = _ItemDescriptionInsideKeys.Split('_')[1];
                    string insideRowId2 = _ItemDescriptionInsideKeys.Split('_')[2];

                    if (insideRowId == rowId)
                    {
                        string Description = form["QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2];
                        string Qty = form["QuotationItemDescriptionQty_" + insideRowId + "_" + insideRowId2];
                        string Unit = form["QuotationItemDescriptionUnit_" + insideRowId + "_" + insideRowId2];
                        string Rate = form["QuotationItemDescriptionRate_" + insideRowId + "_" + insideRowId2];
                        string Amount = form["QuotationItemDescriptionAmount2_" + insideRowId + "_" + insideRowId2];
                        string Unit_Cost = form["QuotationItemDescriptionUnitCost_" + insideRowId + "_" + insideRowId2];
                        string Est_Cost = form["QuotationItemDescriptionEstCost2_" + insideRowId + "_" + insideRowId2];
                        string Est_Profit = form["QuotationItemDescriptionProfit2_" + insideRowId + "_" + insideRowId2];
                        string Remarks = form["QuotationItemDescriptionRemarks_" + insideRowId + "_" + insideRowId2];
                        string Mgr_Comments = form["QuotationItemDescriptionMgrComments_" + insideRowId + "_" + insideRowId2];

                        //if same, then start validation. (Same Table Item)
                        if (string.IsNullOrEmpty(ItemName))
                        {
                            ModelState.AddModelError("BigTableItemName-" + insideRowId, "Item Name cannot be empty!");
                        }
                        else
                        {
                            if (quotationItemDescriptionBigTable.Where(e => e.CategoryName == ItemName).FirstOrDefault() != null)
                            {
                                ModelState.AddModelError("BigTableItemName" + insideRowId, "Item Name cannot be same!");
                            }
                        }

                        if (string.IsNullOrEmpty(Description))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2, "Item Description cannot be empty!");
                        }
                        else
                        {
                            if (ItemDescriptionRows.Where(e => e.Description == Description).FirstOrDefault() != null)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2, "Item Description cannot be same!");
                            }
                        }

                        if (string.IsNullOrEmpty(Qty))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionQty_" + insideRowId + "_" + insideRowId2, "Quantity cannot be empty!");
                        }
                        else
                        {
                            //Check is digit or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Qty);

                            if (!checkFormat)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionQty_" + insideRowId + "_" + insideRowId2, "Invalid Quantity Format!");
                            }
                        }

                        if (string.IsNullOrEmpty(Unit))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionUnit_" + insideRowId + "_" + insideRowId2, "Unit cannot be empty!");
                        }

                        if (string.IsNullOrEmpty(Rate))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionRate_" + insideRowId + "_" + insideRowId2, "Rate cannot be empty!");
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Rate);

                            if (!checkFormat)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionRate_" + insideRowId + "_" + insideRowId2, "Invalid Rate Format!");
                            }
                        }

                        if (string.IsNullOrEmpty(Unit_Cost))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionUnitCost_" + insideRowId + "_" + insideRowId2, "Unit Cost cannot be empty!");
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Unit_Cost);

                            if (!checkFormat)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionUnitCost_" + insideRowId + "_" + insideRowId2, "Invalid Unit Cost Format!");
                            }
                        }

                        if (string.IsNullOrEmpty(Remarks))
                        {
                            Remarks = null;
                        }

                        if (string.IsNullOrEmpty(Mgr_Comments))
                        {
                            Mgr_Comments = null;
                        }
                        else
                        {

                            string SalesPersonCheck = Convert.ToString(form["SalesPersonID"]);

                            quotation.SalesPersonId = Convert.ToInt32(SalesPersonCheck);

                            //Check is nearest Agency enter or not

                            string Result = "";

                            int findAgencyID = (int)quotation.SalesPersonId;
                            do
                            {
                                User searchAgencyLeader = _usersModel.GetSingle(findAgencyID);

                                if (searchAgencyLeader != null)
                                {
                                    if (searchAgencyLeader.IsAgencyLeader != "Yes")
                                    {
                                        //if not agency leader, find their upline and search again.
                                        Result = "No";
                                        findAgencyID = searchAgencyLeader.UplineId;
                                    }
                                    else
                                    {
                                        //if get agency leader, compare the login user id is same with this agency leader id or not.
                                        Result = "Yes";
                                        if (MainID != searchAgencyLeader.ID)
                                        {
                                            //if not same, cannot enter.
                                            ModelState.AddModelError("QuotationItemDescriptionMgrComments_" + insideRowId + "_" + insideRowId2, "This field is only for nearest agency leader to fill in!");
                                        }
                                    }
                                }
                                else
                                {
                                    ModelState.AddModelError("QuotationItemDescriptionMgrComments_" + insideRowId + "_" + insideRowId2, "Nearest Agency Not Found!");
                                }

                            } while (Result != "Yes");
                        }

                        if (string.IsNullOrEmpty(Amount))
                        {
                            Amount = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Amount);

                            if (checkFormat)
                            {
                                Amount = Convert.ToDecimal(Amount).ToString("#,##0.00");
                            }
                        }

                        if (string.IsNullOrEmpty(Est_Cost))
                        {
                            Est_Cost = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Est_Cost);

                            if (checkFormat)
                            {
                                Est_Cost = Convert.ToDecimal(Est_Cost).ToString("#,##0.00");
                            }
                        }

                        if (string.IsNullOrEmpty(Est_Profit))
                        {
                            Est_Profit = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Est_Profit);

                            if (checkFormat)
                            {
                                Est_Profit = Convert.ToDecimal(Est_Profit).ToString("#,##0.00");
                            }
                        }

                        ItemDescriptionRows.Add(new ItemDescriptionRow()
                        {
                            RowId = Convert.ToInt32(insideRowId2),
                            Description = Description,
                            Qty = Qty,
                            Unit = Unit,
                            Rate = Rate,
                            Amount = Amount,
                            Unit_Cost = Unit_Cost,
                            Est_Cost = Est_Cost,
                            AdjCost = null,
                            Est_Profit = Est_Profit,
                            Remarks = Remarks,
                            Mgr_Comments = Mgr_Comments
                        });

                    }
                }
                bool ConvertIsBasic = true;

                if (IsBasic == "True")
                {
                    ConvertIsBasic = Convert.ToBoolean("True");
                }
                else
                {
                    ConvertIsBasic = Convert.ToBoolean("False");
                }

                quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                {
                    TableID = Convert.ToInt32(rowId),
                    CategoryName = ItemName,
                    IsBasic = Convert.ToBoolean(ConvertIsBasic),
                    ItemDescriptionRows = ItemDescriptionRows

                });
            }

            ViewData["CheckQuotationItemLength"] = "No";

            if (quotationItemDescriptionBigTable.Count > 0)
            {
                ViewData["CheckQuotationItemLength"] = "Yes";
            }

            if (ModelState.IsValid)
            {
                //For store Cobroke item
                QuotationCoBroke quotationCoBrokRecord = new QuotationCoBroke();

                //For store Item Description item
                QuotationItemDescription quotationItemDescriptionRecord = new QuotationItemDescription();

                bool oldDataIsDraft = false;

                if (oldData.Status == "Draft")
                {
                    //Assign Quotation ID
                    string prefix = _settingsModel.GetCodeValue("PREFIX_QUOTATION_ID");

                    IList<Quotation> lastRecord = _quotationsModel.GetLast();

                    //Convert year to last two digit
                    var date = DateTime.Now;
                    int last_two_digit = date.Year % 100;

                    if (lastRecord.Count > 0)
                    {
                        quotation.QuotationID = prefix + last_two_digit + (lastRecord.Count + 1).ToString().PadLeft(5, '0');
                    }
                    else
                    {
                        quotation.QuotationID = prefix + last_two_digit + "00001";
                    }

                    quotation.ReviseQuotationTimes = 0;
                    quotation.VariationOrderTimes = 0;
                    oldDataIsDraft = true;
                }
                else if (oldData.Status.Contains("Pending Customer Confirmation"))
                {
                    //Check Revised Quotation and Variation
                    quotation.ReviseQuotationTimes = oldData.ReviseQuotationTimes + 1;
                    quotation.VariationOrderTimes = oldData.VariationOrderTimes;
                    quotation.PreviousQuotationId = oldData.PreviousQuotationId;

                    if (oldData.VariationOrderTimes == 0)
                    {
                        if (quotation.ReviseQuotationTimes == 0)
                        {
                            quotation.QuotationID = oldData.QuotationID + "-R" + quotation.ReviseQuotationTimes;
                        }
                        else
                        {
                            string[] getQuotationID = oldData.QuotationID.Split('-');

                            //Sample: Q1700001-R2-V1
                            quotation.QuotationID = getQuotationID[0] + "-R" + quotation.ReviseQuotationTimes;
                        }

                    }
                    else
                    {
                        //got Variation Order Before
                        string[] getQuotationID = oldData.QuotationID.Split('-');

                        //Sample: Q1700001-R2-V1
                        quotation.QuotationID = getQuotationID[0] + "-R" + quotation.ReviseQuotationTimes + "-V" + oldData.VariationOrderTimes;
                    }
                }
                else if (oldData.Status == "Confirmed")
                {
                    //Check Variation Quotation and Revised
                    quotation.VariationOrderTimes = oldData.VariationOrderTimes + 1;
                    quotation.ReviseQuotationTimes = oldData.ReviseQuotationTimes;
                    quotation.PreviousQuotationId = oldData.PreviousQuotationId;

                    if (oldData.ReviseQuotationTimes == 0)
                    {
                        if (quotation.VariationOrderTimes == 0)
                        {
                            quotation.QuotationID = oldData.QuotationID + "-V" + quotation.VariationOrderTimes;
                        }
                        else
                        {
                            string[] getQuotationID = oldData.QuotationID.Split('-');

                            //Sample: Q1700001-R2-V1
                            quotation.QuotationID = getQuotationID[0] + "-V" + quotation.VariationOrderTimes;
                        }
                    }
                    else
                    {
                        //got Revised Order Before
                        string[] getQuotationID = oldData.QuotationID.Split('-');

                        //Sample: Q1700001-R1-V2
                        quotation.QuotationID = getQuotationID[0] + "-R" + oldData.ReviseQuotationTimes + "-V" + quotation.VariationOrderTimes;
                    }
                }
                else if (oldData.Status.Contains("Pending Sales Revision") || oldData.Status == "Cancelled by Customer")
                {
                    //This is for resubmit

                    //Check Variation Quotation and Revised
                    quotation.VariationOrderTimes = oldData.VariationOrderTimes + 1;
                    quotation.ReviseQuotationTimes = oldData.ReviseQuotationTimes;
                    quotation.PreviousQuotationId = oldData.PreviousQuotationId;

                    if (oldData.ReviseQuotationTimes == 0)
                    {
                        if (quotation.VariationOrderTimes == 0)
                        {
                            quotation.QuotationID = oldData.QuotationID + "-V" + quotation.VariationOrderTimes;
                        }
                        else
                        {
                            string[] getQuotationID = oldData.QuotationID.Split('-');

                            //Sample: Q1700001-R2-V1
                            quotation.QuotationID = getQuotationID[0] + "-V" + quotation.VariationOrderTimes;
                        }
                    }
                    else
                    {
                        //got Revised Order Before
                        string[] getQuotationID = oldData.QuotationID.Split('-');

                        //Sample: Q1700001-R1-V2
                        quotation.QuotationID = getQuotationID[0] + "-R" + oldData.ReviseQuotationTimes + "-V" + quotation.VariationOrderTimes;
                    }
                }
                else
                {
                    quotation.QuotationID = oldData.QuotationID;
                    oldDataIsDraft = true;
                }

                Customer getCustomerInfo = _customersModel.GetSingle((int)quotation.CustomerId);

                if (getCustomerInfo != null)
                {
                    quotation.Address = getCustomerInfo.Address;
                    quotation.Tel = getCustomerInfo.Tel;
                    quotation.Fax = getCustomerInfo.Fax;
                }
                else
                {
                    quotation.Address = null;
                    quotation.Tel = null;
                    quotation.Fax = null;
                }

                quotation.PreparedById = MainID;

                string SalesPersonCheck = Convert.ToString(form["SalesPersonID"]);

                quotation.SalesPersonId = Convert.ToInt32(SalesPersonCheck);
                quotation.CompanyName = CompanyGSTCheck[0];
                quotation.HasGST = CompanyGSTCheck[1];

                if (!string.IsNullOrEmpty(quotation.SalesDocumentUploads))
                {

                    if (oldData.SalesDocumentUploads != quotation.SalesDocumentUploads)
                    {
                        string[] SalesDocuments = quotation.SalesDocumentUploads.Split(',');

                        foreach (string file in SalesDocuments)
                        {
                            string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                            string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["QuotationSalesDocumentsFolder"].ToString()), file);

                            System.IO.File.Move(sourceFile, destinationFile);
                        }
                    }

                }

                User CheckUserAgency = _usersModel.GetSingle(quotation.PreparedById);

                string PreparedByIsAgencyLeader = "No";

                if (CheckUserAgency.IsAgencyLeader == "Yes")
                {
                    PreparedByIsAgencyLeader = "Yes";
                }

                //Update Status
                string Result = "";
                int getAgencyLeader = 0;
                string AgencyLeaderName = "";
                int findAgencyID = (int)quotation.SalesPersonId;
                do
                {
                    User searchAgencyLeader = _usersModel.GetSingle(findAgencyID);

                    if (searchAgencyLeader != null)
                    {
                        if (searchAgencyLeader.IsAgencyLeader != "Yes")
                        {
                            //if not agency leader, find their upline and search again.
                            Result = "No";
                            findAgencyID = searchAgencyLeader.UplineId;
                        }
                        else
                        {
                            Result = "Yes";
                            getAgencyLeader = searchAgencyLeader.ID;
                            AgencyLeaderName = searchAgencyLeader.Name;

                        }
                    }
                    else
                    {
                        //if agency leader not found, send to super admin
                        AgencyLeaderName = "Super Admin";
                    }

                } while (Result != "Yes");

                //string UpdatedSalesPerson = "No";

                if (oldData.Status == "Draft")
                {
                    //if yes, skip manager approval
                    //assign sales person to pending customer confirmation
                    if (PreparedByIsAgencyLeader == "Yes")
                    {
                        User getSalesPersonName = _usersModel.GetSingle(Convert.ToInt32(quotation.SalesPersonId));
                        quotation.Status = "Pending Customer Confirmation ( " + getSalesPersonName.Name + " )";
                        //UpdatedSalesPerson = "Yes";
                    }
                    else
                    {
                        //here is pending manager approval because prepared by user is not agency leader.
                        quotation.Status = "Pending Manager Approval ( " + AgencyLeaderName + " )";
                        //UpdatedSalesPerson = "Yes";
                    }
                }
                else if (oldData.Status.Contains("Pending Sales Revision") || oldData.Status == "Cancelled by Customer")
                {

                    if (PreparedByIsAgencyLeader == "Yes")
                    {
                        User getSalesPersonName = _usersModel.GetSingle(Convert.ToInt32(quotation.SalesPersonId));
                        quotation.Status = "Pending Customer Confirmation ( " + getSalesPersonName.Name + " )";
                        //UpdatedSalesPerson = "Yes";
                    }
                    else
                    {
                        quotation.Status = "Pending Manager Approval ( " + AgencyLeaderName + " )";
                        //UpdatedSalesPerson = "Yes";
                    }
                }
                else if (oldData.Status.Contains("Pending Manager Approval"))
                {

                    //check if old data quotation status is not same. then update new quotation status
                    if (oldData.Status != "Pending Manager Approval ( " + AgencyLeaderName + " )")
                    {
                        quotation.Status = "Pending Manager Approval ( " + AgencyLeaderName + " )";
                        //UpdatedSalesPerson = "Yes";
                    }
                    else
                    {
                        quotation.Status = oldData.Status;
                    }
                }
                else if (oldData.Status.Contains("Pending Customer Confirmation"))
                {
                    //purpose: if pending customer confirmation.
                    //super admin edit sales person id, if sales person id not same with previous one.
                    //update quotation pending customer confirmation sales person name status.

                    int SalesPersonID = Convert.ToInt32(oldData.SalesPersonId);
                    //get oldData salesperson id
                    User getSalesPerson = _usersModel.GetSingle(SalesPersonID);

                    if (oldData.Status != "Pending Customer Confirmation ( " + getSalesPerson.Name + " )")
                    {
                        quotation.Status = "Pending Customer Confirmation ( " + getSalesPerson.Name + " )";
                    }
                    else
                    {
                        quotation.Status = oldData.Status;
                    }
                }
                else
                {
                    quotation.Status = oldData.Status;
                }

                //quotation.ReviseQuotationTimes = 0;
                //quotation.VariationOrderTimes = 0;

                bool result = false;

                if (oldDataIsDraft == false)
                {
                    result = _quotationsModel.Add(quotation);

                    //Update Linked project quotation id to latest quotation id.
                    if (result)
                    {
                        List<Project> project = _projectsModel.GetAllProjectRelatedQuotationID(oldData.ID);

                        foreach (Project _project in project)
                        {
                            _project.QuotationReferenceId = quotation.ID;

                            bool update_project_quotation_id = _projectsModel.Update(_project.ID, _project);


                            if (update_project_quotation_id)
                            {
                                string userRole = Session["UserGroup"].ToString();
                                if (userRole == "Sales")
                                {
                                    userRole += " " + Session["Position"].ToString();
                                }
                                string tableAffected = "Projects";
                                string description = userRole + " [" + Session["LoginID"].ToString() + "] Updated Projects [" + _project.ProjectID + "]";

                                bool project_create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                            }
                        }

                        List<Invoice> invoices = _invoicesModel.GetAllProjectRelatedInvoiceID(oldData.ID);

                        foreach (Invoice invoice in invoices)
                        {
                            invoice.QuotationId = quotation.ID;

                            bool update_invoice_quotation_id = _invoicesModel.Update(invoice.ID, invoice);


                            if (update_invoice_quotation_id)
                            {
                                string userRole = Session["UserGroup"].ToString();
                                if (userRole == "Sales")
                                {
                                    userRole += " " + Session["Position"].ToString();
                                }
                                string tableAffected = "Invoices";
                                string description = userRole + " [" + Session["LoginID"].ToString() + "] Updated Invoices [" + invoice.InvoiceID + "]";

                                bool invoice_update_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                            }
                        }
                    }
                }
                else
                {
                    //This is Draft, so is update to new application.
                    result = _quotationsModel.UpdateData(oldData.ID, quotation);
                }

                if (result)
                {

                    bool update_quotation_previous_id = false;

                    if (oldDataIsDraft == false)
                    {
                        update_quotation_previous_id = _quotationsModel.UpdatePreviousQuotationID(quotation.PreviousQuotationId);
                    }
                    else
                    {
                        update_quotation_previous_id = _quotationsModel.UpdatePreviousQuotationID(oldData.ID);
                    }

                    if (update_quotation_previous_id)
                    {
                        string userRole = Session["UserGroup"].ToString();
                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }

                        string tableAffected = "Quotations";
                        string description = "";

                        if (oldData.Status.Contains("Pending Sales Revision") && oldData.Status == "Cancelled by Customer" && oldData.Status != "Confirmed" && !oldData.Status.Contains("Pending Customer Confirmation"))
                        {
                            description = userRole + " [" + Session["LoginID"].ToString() + "] Resubmitted Quotation [" + quotation.QuotationID + "]";
                        }
                        else
                        {
                            description = userRole + " [" + Session["LoginID"].ToString() + "] Updated Quotation [" + quotation.QuotationID + "]";
                        }

                        bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                        if (!oldData.Status.Contains("Pending Sales Revision") && oldData.Status != "Cancelled by Customer" && oldData.Status != "Confirmed" && !oldData.Status.Contains("Pending Customer Confirmation"))
                        {
                            //Remove All QuotationCoBroke Item First
                            //Get Database Quotation CoBroke Items based on quotation ID
                            IList<QuotationCoBroke> getQuotationCoBroke = _quotationCoBrokesModel.GetPersonalAll(id);

                            foreach (QuotationCoBroke _getQuotationCoBroke in getQuotationCoBroke)
                            {
                                int quotationCoBrokeID = _getQuotationCoBroke.ID;
                                bool delete_quotation_coBroke = _quotationCoBrokesModel.Delete(quotationCoBrokeID);
                            }
                        }

                        bool hasCoBroke = false;

                        //Store CoBroke Item
                        foreach (QuotationCoBrokeTable _quotationCoBrokeTable in quotationCoBrokeTable)
                        {
                            if (oldDataIsDraft == false)
                            {
                                quotationCoBrokRecord.QuotationId = quotation.ID;
                            }
                            else
                            {
                                quotationCoBrokRecord.QuotationId = oldData.ID;
                            }


                            //determine cobroke user type
                            string UserTypeCheck = Convert.ToString(_quotationCoBrokeTable.UserID.SelectedValue);

                            quotationCoBrokRecord.QuotationCoBrokeUserId = Convert.ToInt32(UserTypeCheck);
                            quotationCoBrokRecord.QuotationCoBrokePercentOfProfit = Convert.ToInt32(_quotationCoBrokeTable.PercentageOfProfit.SelectedValue);

                            bool add_quotation_cobroke = _quotationCoBrokesModel.Add(quotationCoBrokRecord);

                            if (add_quotation_cobroke)
                            {
                                if (!hasCoBroke)
                                {
                                    hasCoBroke = true;
                                }
                            }
                        }

                        if (hasCoBroke)
                        {
                            userRole = Session["UserGroup"].ToString();
                            if (userRole == "Sales")
                            {
                                userRole += " " + Session["Position"].ToString();
                            }
                            tableAffected = "QuotationCoBrokes";
                            description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Quotation CoBrokes [" + quotation.QuotationID + "]";

                            bool itemDescription_create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                        }

                        if (!oldData.Status.Contains("Pending Sales Revision") && oldData.Status != "Cancelled by Customer" && oldData.Status != "Confirmed" && !oldData.Status.Contains("Pending Customer Confirmation"))
                        {
                            //Remove All QuotationItemDescription Item
                            //Get Database Quotation Items Description based on quotation ID
                            IList<QuotationItemDescription> getQuotationItemDescription = _quotationItemDescriptionsModel.GetPersonalAll(id);

                            foreach (QuotationItemDescription _getQuotationItemDescription in getQuotationItemDescription)
                            {
                                int quotationItemDescriptionID = _getQuotationItemDescription.ID;
                                bool delete_quotation_itemDescription = _quotationItemDescriptionsModel.Delete(quotationItemDescriptionID);
                            }
                        }

                        bool hasItemDescription = false;

                        //Loop BigTable
                        foreach (ItemDescriptionBigTable _quotationItemDescriptionBigTable in quotationItemDescriptionBigTable)
                        {
                            foreach (ItemDescriptionRow _quotationItemDesriptionRow in _quotationItemDescriptionBigTable.ItemDescriptionRows)
                            {

                                if (oldDataIsDraft == false)
                                {
                                    quotationItemDescriptionRecord.QuotationId = quotation.ID;
                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationId = oldData.ID;
                                }

                                quotationItemDescriptionRecord.CategoryName = _quotationItemDescriptionBigTable.CategoryName;
                                quotationItemDescriptionRecord.QuotationItem_Description = _quotationItemDesriptionRow.Description;
                                quotationItemDescriptionRecord.RowId = _quotationItemDescriptionBigTable.TableID;

                                if (_quotationItemDesriptionRow.Qty == "")
                                {
                                    _quotationItemDesriptionRow.Qty = "0";
                                    quotationItemDescriptionRecord.QuotationItemQty = 0;

                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemQty = Convert.ToDecimal(_quotationItemDesriptionRow.Qty);
                                }

                                if (string.IsNullOrEmpty(_quotationItemDesriptionRow.Unit))
                                {
                                    quotationItemDescriptionRecord.QuotationItemUnit = null;
                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemUnit = _quotationItemDesriptionRow.Unit;
                                }

                                quotationItemDescriptionRecord.QuotationItemRate = Convert.ToDecimal(_quotationItemDesriptionRow.Rate);

                                decimal Amount = Convert.ToDecimal(_quotationItemDesriptionRow.Rate) * Convert.ToDecimal(_quotationItemDesriptionRow.Qty);
                                quotationItemDescriptionRecord.QuotationItemAmount = Amount;

                                if (_quotationItemDesriptionRow.Unit_Cost == "")
                                {
                                    _quotationItemDesriptionRow.Unit_Cost = "0";
                                    quotationItemDescriptionRecord.QuotationItemUnitCost = 0;

                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemUnitCost = Convert.ToDecimal(_quotationItemDesriptionRow.Unit_Cost);
                                }

                                decimal EstCost = Convert.ToDecimal(_quotationItemDesriptionRow.Qty) * Convert.ToDecimal(_quotationItemDesriptionRow.Unit_Cost);
                                quotationItemDescriptionRecord.QuotationItemEstCost = EstCost;

                                decimal EstProfit = Amount - EstCost;
                                quotationItemDescriptionRecord.QuotationItemEstProfit = EstProfit;

                                if (string.IsNullOrEmpty(_quotationItemDesriptionRow.Remarks))
                                {
                                    quotationItemDescriptionRecord.QuotationItemRemarks = null;
                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemRemarks = _quotationItemDesriptionRow.Remarks;
                                }

                                if (string.IsNullOrEmpty(_quotationItemDesriptionRow.Mgr_Comments))
                                {
                                    quotationItemDescriptionRecord.QuotationItemMgrComments = null;
                                }
                                else
                                {
                                    quotationItemDescriptionRecord.QuotationItemMgrComments = _quotationItemDesriptionRow.Mgr_Comments;
                                }


                                bool add_quotation_item_description = _quotationItemDescriptionsModel.Add(quotationItemDescriptionRecord);

                                if (add_quotation_item_description)
                                {
                                    if (!hasItemDescription)
                                    {
                                        hasItemDescription = true;
                                    }
                                }
                            }
                        }

                        if (hasItemDescription)
                        {
                            userRole = Session["UserGroup"].ToString();
                            if (userRole == "Sales")
                            {
                                userRole += " " + Session["Position"].ToString();
                            }
                            tableAffected = "QuotationItemDescriptions";
                            description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Quotation Item Descriptions [" + quotation.QuotationID + "]";

                            bool itemDescription_create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                        }

                        //Check Status to verify want to send email or not
                        if (quotation.Status.Contains("Pending Manager Approval"))
                        {
                            //Search nearest Agency and email to them.
                            string Result2 = "";
                            int getAgencyLeader2 = 0;
                            int findAgencyID2 = (int)quotation.SalesPersonId;
                            do
                            {
                                User searchAgencyLeader = _usersModel.GetSingle(findAgencyID2);

                                if (searchAgencyLeader != null)
                                {
                                    if (searchAgencyLeader.IsAgencyLeader != "Yes")
                                    {
                                        //if not agency leader, find their upline and search again.
                                        Result2 = "No";
                                        findAgencyID2 = searchAgencyLeader.UplineId;
                                    }
                                    else
                                    {
                                        Result2 = "Yes";
                                        getAgencyLeader2 = searchAgencyLeader.ID;
                                        ViewData["IsNearestAgency"] = "Yes";
                                        //Start To Email to the Agency Leader
                                        string subject = "Pending [" + quotation.QuotationID + "] Quotation Approval";
                                        string body = System.IO.File.ReadAllText(Server.MapPath("~/Templates/PendingQuotationNotification.html"));
                                        string recipient = searchAgencyLeader.Email;
                                        string name = searchAgencyLeader.Name;
                                        string quotationID = Convert.ToString(quotation.QuotationID);
                                        string status = quotation.Status;
                                        ListDictionary replacements = new ListDictionary();

                                        replacements.Add("<%Name%>", name);
                                        replacements.Add("<%QuotationID%>", quotationID);
                                        replacements.Add("<%Status%>", status);

                                        if (oldDataIsDraft == false)
                                        {
                                            replacements.Add("<%Url%>", Url.Action("Edit", "Quotation", new { @id = quotation.ID }, "http"));
                                        }
                                        else
                                        {
                                            replacements.Add("<%Url%>", Url.Action("Edit", "Quotation", new { @id = oldData.ID }, "http"));
                                        }

                                        bool sent = EmailHelper.SendEmail(subject, body, replacements, recipient);
                                    }
                                }
                                else
                                {
                                    //if agency leader not found, send to super admin

                                    IList<User> getSuperAdminInfo = _usersModel.GetAll().Where(e => e.Role == "Super Admin").ToList();

                                    foreach (User _getSuperAdminInfo in getSuperAdminInfo)
                                    {
                                        //Start To Email to all super admin
                                        string subject = "Pending [" + quotation.QuotationID + "] Quotation Approval";
                                        string body = System.IO.File.ReadAllText(Server.MapPath("~/Templates/PendingQuotationNotification.html"));
                                        string recipient = _getSuperAdminInfo.Email;
                                        string name = _getSuperAdminInfo.Name;
                                        string quotationID = Convert.ToString(quotation.QuotationID);
                                        string status = quotation.Status;
                                        ListDictionary replacements = new ListDictionary();

                                        replacements.Add("<%Name%>", name);
                                        replacements.Add("<%QuotationID%>", quotationID);
                                        replacements.Add("<%Status%>", status);

                                        if (oldDataIsDraft == false)
                                        {
                                            replacements.Add("<%Url%>", Url.Action("Edit", "Quotation", new { @id = quotation.ID }, "http"));
                                        }
                                        else
                                        {
                                            replacements.Add("<%Url%>", Url.Action("Edit", "Quotation", new { @id = oldData.ID }, "http"));
                                        }

                                        bool sent = EmailHelper.SendEmail(subject, body, replacements, recipient);
                                    }

                                    Session.Add("Result", "success|" + quotation.QuotationID + " has been successfully edited but no agency leader found! The email will send to super admin");
                                    return RedirectToAction("Index");
                                }
                            } while (Result2 != "Yes");

                        }
                        if (oldDataIsDraft != false)
                        {
                            Session.Add("Result", "success|" + quotation.QuotationID + " has been successfully created!");
                        }
                        else
                        {
                            if (oldData.Status == "Confirmed")
                            {
                                Session.Add("Result", "success|" + quotation.QuotationID + " has been successfully edited!");
                            }
                            else
                            {
                                Session.Add("Result", "success|" + oldData.QuotationID + " has been successfully created!");
                            }

                        }

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        Session.Add("Result", "danger|An error occured while edit quotation previous quotation records!");
                        return View();
                    }
                }
                else
                {
                    Session.Add("Result", "danger|An error occured while edit quotation records!");
                    return View();
                }
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            //assign back the current id to save as draft
            quotation.ID = id;

            //Get Quotation 
            ViewData["Quotation"] = quotation;

            //ViewData["CheckDraftButton"] = oldData.Status;

            ViewData["QuotationCoBrokeTable"] = quotationCoBrokeTable;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            Dropdown[] companyDDL = CompanyDDL();
            ViewData["CompanyDropdown"] = new SelectList(companyDDL, "val", "name", form["Company"]);

            int AssignSalesPersonId = 0;

            if (quotation.CustomerId != 0 && quotation.CustomerId != null)
            {
                AssignSalesPersonId = Convert.ToInt32(quotation.CustomerId);
            }

            Dropdown[] customerDDL = CustomerDDL(null, AssignSalesPersonId);
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", null);

            Dropdown[] userDDL = UserDDL();

            Dropdown[] ownAndDownlineDDL = OwnAndDownlineDDL();
            ViewData["UserDropdown"] = new SelectList(ownAndDownlineDDL, "val", "name", form["SalesPersonID"]);

            ViewData["SalesDocumentFiles"] = quotation.SalesDocumentUploads;

            User getPreparedUser = _usersModel.GetSingle(oldData.PreparedById);

            ViewData["PreparedBy"] = getPreparedUser.Name;

            ViewData["QuotationDate"] = DateTime.Now.ToString("dd/MM/yyyy");

            ViewData["QuotationItemDescription"] = quotationItemDescriptionBigTable;

            Dropdown[] percentageDDL = PercentageOfProfitDDL();
            ViewData["PercentageOfProfitDropdown"] = new SelectList(percentageDDL, "val", "name", null);

            List<Quotation> getUserQuotations = _quotationsModel.GetUserQuotations(MainID);
            List<string> descriptionsArray = new List<string>();

            foreach (Quotation _getUserQuotations in getUserQuotations)
            {
                foreach (QuotationItemDescription _getQuotationItemDescriptions in _getUserQuotations.ItemDescriptions)
                {
                    if (!descriptionsArray.Contains(_getQuotationItemDescriptions.QuotationItem_Description))
                    {
                        descriptionsArray.Add(_getQuotationItemDescriptions.QuotationItem_Description);
                    }
                }
            }
            descriptionsArray = descriptionsArray.OrderBy(e => e).ToList();
            ViewData["AutoCompleteList"] = JsonConvert.SerializeObject(descriptionsArray);

            return View();
        }

        //Generate PDF
        public ActionResult GenerateQuotation(int id)
        {

            //Get Quotation 
            Quotation quotation = _quotationsModel.GetSingle(id);
            ViewData["Quotation"] = quotation;

            //Get system setting terms and conditions
            SystemSetting settings = _settingsModel.GetSingle(14);
            ViewData["TermsAndConditions"] = settings.Value;

            if(quotation.HasGST == "Yes")
            {
                SystemSetting setting = _settingsModel.GetSingle("GST");
                ViewData["GST"] = setting.Value;
            }

            SystemSetting getCompanyName = _settingsModel.GetSingle("COMPANY_NAME");
            ViewData["SystemSettingCompanyName"] = getCompanyName.Value;

			string[] listOfCompanies = _settingsModel.GetCodeValue("LIST_OF_COMPANY").Split(';');

			ViewData["CompanyName"] = "MY RENOBUDDY (SG) PTE LTD";
			ViewData["CompanyInfo"] = "Office / Factory: 1007 Eunos Avenue 7 #01-25 S(409578)|Email: Enquiries@myrenobuddy.com.sg|Contact / Fax: +65 6741 3503 (O) +6741 3060 (F)|Co. Reg. No: 201534632H";

			string CompanyValue = "";

			if (!string.IsNullOrEmpty(getCompanyName.Value))
			{
				CompanyValue = getCompanyName.Value.ToString();
			}

			foreach (string company in listOfCompanies)
			{
				string[] comp = company.Split('|');

				if (comp[0] == CompanyValue)
				{
					ViewData["CompanyName"] = comp[0];
					ViewData["CompanyInfo"] = comp[2].Replace("\r\n", "|");
					break;
				}
			}

			//Get Quotation CoBroke Item
			List<QuotationCoBroke> quotationCoBroke = _quotationCoBrokesModel.GetAllRelatedQuotationId(id);
            ViewData["QuotationCoBroke"] = quotationCoBroke;

            //Get Quotation Item Description

            //New Get Quotation Item Description
            List<QuotationItemDescription> quotationItemDescription = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(id);
            List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();

            //Loop Item Description Items
            IList<SystemSetting> listItem = _settingsModel.GetAll();
            SystemSetting CategoryList = listItem.Where(e => e.Code == "QUOTATION_CATEGORIES").FirstOrDefault();

            string[] categoryList = CategoryList.Value.Split('|');

            //Start
            int countItemDescription = 1;
            int resetNumber = 1;
            foreach (QuotationItemDescription _quotationItemDescription in quotationItemDescription)//14 items
            {
                string ItemName = _quotationItemDescription.CategoryName;
                decimal ItemQty = (decimal)_quotationItemDescription.QuotationItemQty;
                decimal ItemRate = (decimal)_quotationItemDescription.QuotationItemRate;
                decimal ItemUnitCost = (decimal)_quotationItemDescription.QuotationItemUnitCost;
                decimal ItemEstCost = (decimal)_quotationItemDescription.QuotationItemEstCost;
                bool IsBasic = false; //default set to false means it is not from db item.

                List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                if (ItemQty == null)
                {
                    ItemQty = 0;
                }

                if (ItemRate == null)
                {
                    ItemRate = 0;
                }

                if(ItemUnitCost == null)
                {
                    ItemUnitCost = 0;
                }

                if (ItemEstCost == null)
                {
                    ItemEstCost = 0;
                }

                foreach (string _categoryList in categoryList)
                {
                    if (_categoryList == _quotationItemDescription.CategoryName)
                    {
                        IsBasic = true;
                    }
                }

                if (quotationItemDescriptionBigTable.Count == 0)
                {
                    resetNumber = 1;
                    ItemDescriptionRows.Add(new ItemDescriptionRow()
                    {
                        RowId = resetNumber,
                        Description = _quotationItemDescription.QuotationItem_Description,
                        Qty = Convert.ToString(_quotationItemDescription.QuotationItemQty),
                        Unit = _quotationItemDescription.QuotationItemUnit,
                        Rate = Convert.ToString(_quotationItemDescription.QuotationItemRate),
                        Amount = Convert.ToString(_quotationItemDescription.QuotationItemAmount),
                        Unit_Cost = Convert.ToString(_quotationItemDescription.QuotationItemUnitCost),
                        Est_Cost = Convert.ToString(_quotationItemDescription.QuotationItemEstCost),
                        AdjCost = null,
                        Est_Profit = Convert.ToString(_quotationItemDescription.QuotationItemEstProfit),
                        Remarks = _quotationItemDescription.QuotationItemRemarks,
                        Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                    });

                    quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                    {
                        TableID = countItemDescription,
                        CategoryName = _quotationItemDescription.CategoryName,
                        IsBasic = IsBasic,
                        ItemDescriptionRows = ItemDescriptionRows
                    });
                    countItemDescription++;
                }
                else
                {
                    if (quotationItemDescriptionBigTable.Where(e => e.CategoryName == _quotationItemDescription.CategoryName).FirstOrDefault() != null)
                    {
                        foreach (ItemDescriptionBigTable _loopBigTable in quotationItemDescriptionBigTable)
                        {
                            if (_loopBigTable.CategoryName == _quotationItemDescription.CategoryName)
                            {
                                _loopBigTable.ItemDescriptionRows.Add(new ItemDescriptionRow()
                                {
                                    RowId = resetNumber,
                                    Description = _quotationItemDescription.QuotationItem_Description,
                                    Qty = Convert.ToString(_quotationItemDescription.QuotationItemQty),
                                    Unit = _quotationItemDescription.QuotationItemUnit,
                                    Rate = Convert.ToString(_quotationItemDescription.QuotationItemRate),
                                    Amount = Convert.ToString(_quotationItemDescription.QuotationItemAmount),
                                    Unit_Cost = Convert.ToString(_quotationItemDescription.QuotationItemUnitCost),
                                    Est_Cost = Convert.ToString(_quotationItemDescription.QuotationItemEstCost),
                                    AdjCost = null,
                                    Est_Profit = Convert.ToString(_quotationItemDescription.QuotationItemEstProfit),
                                    Remarks = _quotationItemDescription.QuotationItemRemarks,
                                    Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                                });
                                break;
                            }
                        }
                    }
                    else
                    {
                        resetNumber = 1;
                        ItemDescriptionRows.Add(new ItemDescriptionRow()
                        {
                            RowId = resetNumber,
                            Description = _quotationItemDescription.QuotationItem_Description,
                            Qty = Convert.ToString(_quotationItemDescription.QuotationItemQty),
                            Unit = _quotationItemDescription.QuotationItemUnit,
                            Rate = Convert.ToString(_quotationItemDescription.QuotationItemRate),
                            Amount = Convert.ToString(_quotationItemDescription.QuotationItemAmount),
                            Unit_Cost = Convert.ToString(_quotationItemDescription.QuotationItemUnitCost),
                            Est_Cost = Convert.ToString(_quotationItemDescription.QuotationItemEstCost),
                            AdjCost = null,
                            Est_Profit = Convert.ToString(_quotationItemDescription.QuotationItemEstProfit),
                            Remarks = _quotationItemDescription.QuotationItemRemarks,
                            Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                        });

                        quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                        {
                            TableID = countItemDescription,
                            CategoryName = _quotationItemDescription.CategoryName,
                            IsBasic = IsBasic,
                            ItemDescriptionRows = ItemDescriptionRows
                        });
                        countItemDescription++;
                    }
                }
                resetNumber++;
                //End
            }

            ViewData["QuotationItemDescription"] = quotationItemDescriptionBigTable;

            return new Rotativa.ViewAsPdf("GenerateQuotationFormPDF")
            {
                FileName = quotation.QuotationID + "-quotation-" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf",
                PageSize = Rotativa.Options.Size.A4
            };
        }

        //GET: View
        public ActionResult View(int id)
        {

            int MainID = Convert.ToInt32(Session["MainID"]);

            //Get Quotation 
            Quotation quotation = _quotationsModel.GetSingle(id);
            ViewData["Quotation"] = quotation;

            //Get Quotation CoBroke Item
            List<QuotationCoBroke> quotationCoBroke = _quotationCoBrokesModel.GetAllRelatedQuotationId(id);
            ViewData["QuotationCoBroke"] = quotationCoBroke;

            //Get Quotation Item Description

            //New Get Quotation Item Description
            List<QuotationItemDescription> quotationItemDescription = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(id);
            List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();

            //Loop Item Description Items
            IList<SystemSetting> listItem = _settingsModel.GetAll();
            SystemSetting CategoryList = listItem.Where(e => e.Code == "QUOTATION_CATEGORIES").FirstOrDefault();

            string[] categoryList = CategoryList.Value.Split('|');

            //Start
            int countItemDescription = 1;
            int resetNumber = 1;
            foreach (QuotationItemDescription _quotationItemDescription in quotationItemDescription)//14 items
            {
                string ItemName = _quotationItemDescription.CategoryName;
                decimal ItemQty = (decimal)_quotationItemDescription.QuotationItemQty;
                decimal ItemRate = (decimal)_quotationItemDescription.QuotationItemRate;
                decimal ItemUnitCost = (decimal)_quotationItemDescription.QuotationItemUnitCost;
                decimal ItemEstCost = (decimal)_quotationItemDescription.QuotationItemEstCost;
                bool IsBasic = false; //default set to false means it is not from db item.

                List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                if (ItemQty == null)
                {
                    ItemQty = 0;
                }

                if (ItemRate == null)
                {
                    ItemRate = 0;
                }

                if (ItemUnitCost == null)
                {
                    ItemUnitCost = 0;
                }

                foreach (string _categoryList in categoryList)
                {
                    if (_categoryList == _quotationItemDescription.CategoryName)
                    {
                        IsBasic = true;
                    }
                }

                if (quotationItemDescriptionBigTable.Count == 0)
                {
                    resetNumber = 1;
                    ItemDescriptionRows.Add(new ItemDescriptionRow()
                    {
                        RowId = resetNumber,
                        Description = _quotationItemDescription.QuotationItem_Description,
                        Qty = Convert.ToDecimal(_quotationItemDescription.QuotationItemQty).ToString("#,##0.0"),
                        Unit = _quotationItemDescription.QuotationItemUnit,
                        Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                        Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                        Unit_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemUnitCost).ToString("#,##0.00"),
                        Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                        AdjCost = null,
                        Est_Profit = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstProfit).ToString("#,##0.00"),
                        Remarks = _quotationItemDescription.QuotationItemRemarks,
                        Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                    });

                    quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                    {
                        TableID = countItemDescription,
                        CategoryName = _quotationItemDescription.CategoryName,
                        IsBasic = IsBasic,
                        ItemDescriptionRows = ItemDescriptionRows
                    });
                    countItemDescription++;
                }
                else
                {
                    if (quotationItemDescriptionBigTable.Where(e => e.CategoryName == _quotationItemDescription.CategoryName).FirstOrDefault() != null)
                    {
                        foreach (ItemDescriptionBigTable _loopBigTable in quotationItemDescriptionBigTable)
                        {
                            if (_loopBigTable.CategoryName == _quotationItemDescription.CategoryName)
                            {
                                _loopBigTable.ItemDescriptionRows.Add(new ItemDescriptionRow()
                                {
                                    RowId = resetNumber,
                                    Description = _quotationItemDescription.QuotationItem_Description,
                                    Qty = Convert.ToDecimal(_quotationItemDescription.QuotationItemQty).ToString("#,##0.0"),
                                    Unit = _quotationItemDescription.QuotationItemUnit,
                                    Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                                    Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                                    Unit_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemUnitCost).ToString("#,##0.00"),
                                    Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                                    AdjCost = null,
                                    Est_Profit = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstProfit).ToString("#,##0.00"),
                                    Remarks = _quotationItemDescription.QuotationItemRemarks,
                                    Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                                });
                                break;
                            }
                        }
                    }
                    else
                    {
                        resetNumber = 1;
                        ItemDescriptionRows.Add(new ItemDescriptionRow()
                        {
                            RowId = resetNumber,
                            Description = _quotationItemDescription.QuotationItem_Description,
                            Qty = Convert.ToDecimal(_quotationItemDescription.QuotationItemQty).ToString("#,##0.0"),
                            Unit = _quotationItemDescription.QuotationItemUnit,
                            Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                            Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                            Unit_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemUnitCost).ToString("#,##0.00"),
                            Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                            AdjCost = null,
                            Est_Profit = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstProfit).ToString("#,##0.00"),
                            Remarks = _quotationItemDescription.QuotationItemRemarks,
                            Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                        });

                        quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                        {
                            TableID = countItemDescription,
                            CategoryName = _quotationItemDescription.CategoryName,
                            IsBasic = IsBasic,
                            ItemDescriptionRows = ItemDescriptionRows
                        });
                        countItemDescription++;
                    }
                }
                resetNumber++;
                //End
            }

            ViewData["QuotationItemDescription"] = quotationItemDescriptionBigTable;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            ViewData["SalesDocumentFiles"] = quotation.SalesDocumentUploads;

            ViewData["PreparedBy"] = quotation.PreparedBy.Name;

            ViewData["QuotationDate"] = Convert.ToDateTime(quotation.QuotationDate).ToString("dd/MM/yyyy");

            string CompanyValue = quotation.CompanyName + "-" + quotation.HasGST;

            Dropdown[] companyDDL = CompanyDDL();
            ViewData["CompanyDropdown"] = new SelectList(companyDDL, "val", "name", CompanyValue);

            string fromView = "Yes";

            Dropdown[] customerDDL = CustomerDDL(fromView);
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", quotation.CustomerId);

            Dropdown[] userDDL = UserDDL();

            Dropdown[] ownAndDownlineDDL = OwnAndDownlineDDL();

            User getPreparedUser = _usersModel.GetSingle(MainID);

            if (getPreparedUser.Role != "Super Admin")
            {
                ViewData["UserDropdown"] = new SelectList(ownAndDownlineDDL, "val", "name", quotation.SalesPersonId);
            }
            else
            {
                ViewData["UserDropdown"] = new SelectList(userDDL, "val", "name", quotation.SalesPersonId);
            }

            //ViewData["UserDropdown"] = new SelectList(ownAndDownlineDDL, "val", "name", quotation.SalesPersonId);

            return View();
        }

        //GET: Delete
        public ActionResult Delete(int id)
        {
            //Only Super Admin allow to delete quotation.

            int MainID = Convert.ToInt32(Session["MainID"]);

            Quotation quotation = _quotationsModel.GetSingle(id);

            IList<Project> project = _projectsModel.GetAllProjectRelatedQuotationID(id);

            if(project.Count > 0)
            {
                Session.Add("Result", "danger|" + quotation.QuotationID + " cannot delete because its link with Project!");
                return RedirectToAction("Index");
            }

            User CheckUserRole = _usersModel.GetSingle(MainID);

            if (CheckUserRole.Role != "Super Admin")
            {
                Session.Add("Result", "danger|You are not allow to delete this quotation!");
                return RedirectToAction("Index");
            }

            //Delete all those related item
            List<Quotation> getRelatedPreviousQuotation = _quotationsModel.getAllRelatedIdGetAllRelatedQuotationId(quotation.PreviousQuotationId);

            bool result = false;
            
            foreach (Quotation _getRelatedPreviousQuotation in getRelatedPreviousQuotation)
            {
                result = _quotationsModel.Delete(_getRelatedPreviousQuotation.ID);
            }

            if (result)
            {
                //int MainID = Convert.ToInt32(Session["MainID"]);
                string userRole = "User";
                string tableAffected = "Quotations";
                string description = userRole + " [" + Session["LoginID"].ToString() + "] Delete Quotation [" + quotation.QuotationID + "]";

                bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                Session.Add("Result", "success|" + quotation.QuotationID + " successfully deleted!");
            }
            else
            {
                Session.Add("Result", "danger|Quotation record not found!");
            }

            return RedirectToAction("Index");
        }

        //Status Dropdown
        public Dropdown[] StatusDDL()
        {

            Dropdown[] ddl = new Dropdown[3];

            ddl[0] = new Dropdown { name = "Accepted", val = "Accepted" };
            ddl[1] = new Dropdown { name = "Rejected", val = "Rejected" };
            ddl[2] = new Dropdown { name = "Pending", val = "Pending" };

            return ddl;
        }

        //Percentage of Profit Dropdown
        public Dropdown[] PercentageOfProfitDDL()
        {

            Dropdown[] ddl = new Dropdown[20];

            int number = 5;

            for(int i = 0;i <= 19;i++)
            {
                ddl[i] = new Dropdown { name = number + "%", val = Convert.ToString(number) };
                number += 5;
            }

            return ddl;
        }

        //Company Dropdown
        public Dropdown[] CompanyDDL()
        {

            IList<SystemSetting> settings = _settingsModel.GetAll();

            //COMPANY GST
            SystemSetting CompanyList = settings.Where(e => e.Code == "LIST_OF_COMPANY").FirstOrDefault();
            string[] companyList = CompanyList.Value.Split(';');

            Dropdown[] ddl = new Dropdown[companyList.Length + 1];

            int count = 1;

            foreach (string _companyList in companyList)
            {
                string[] val = _companyList.Split('|');

                ddl[count] = new Dropdown { name = val[0], val = val[0] + "-" + val[1] };
                count++;
            }

            return ddl;
        }

        //Customer Dropdown
        public Dropdown[] CustomerDDL(string fromView = null, int SelectedCustomer = 0)
        {
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            //if super admin, open for all
            User user = _usersModel.GetSingle(loginMainID);

            if (user.Role == "Super Admin")
            {

                IList<Customer> getAllCustomer = _customersModel.GetAll();

                int count = 0;
                Dropdown[] ddl = new Dropdown[getAllCustomer.Count];

                foreach (Customer _getAllCustomer in getAllCustomer)
                {
                    if (!string.IsNullOrEmpty(_getAllCustomer.CompanyName))
                    {
                        ddl[count] = new Dropdown { name = _getAllCustomer.ContactPerson + " - " + _getAllCustomer.CompanyName, val = Convert.ToString(_getAllCustomer.ID) };
                    }
                    else
                    {
                        ddl[count] = new Dropdown { name = _getAllCustomer.ContactPerson, val = Convert.ToString(_getAllCustomer.ID) };
                    }

                    count++;
                }

                return ddl;
            }
            else
            {

                if(!string.IsNullOrEmpty(fromView))
                {
                    IList<Customer> getAllCustomer = _customersModel.GetAll();

                    int count = 0;
                    Dropdown[] ddl = new Dropdown[getAllCustomer.Count];

                    foreach (Customer _getAllCustomer in getAllCustomer)
                    {
                        if (!string.IsNullOrEmpty(_getAllCustomer.CompanyName))
                        {
                            ddl[count] = new Dropdown { name = _getAllCustomer.ContactPerson + " - " + _getAllCustomer.CompanyName, val = Convert.ToString(_getAllCustomer.ID) };
                        }
                        else
                        {
                            ddl[count] = new Dropdown { name = _getAllCustomer.ContactPerson, val = Convert.ToString(_getAllCustomer.ID) };
                        }

                        count++;
                    }

                    return ddl;
                }
                else
                {
                    IList<Customer> getAllCustomer = _customersModel.GetAll();
                    //IList<Customer> getAllCustomer = _customersModel.GetCustomerDownline(loginMainID);

                    Customer getSelectedCustomer = null;

                    int size = getAllCustomer.Count;

                    if (SelectedCustomer > 0)
                    {
                        size++;
                        getSelectedCustomer = _customersModel.GetSingleNoFilter(SelectedCustomer);
                    }

                    Dropdown[] ddl = new Dropdown[size + 1];
                    int count = 1;

                    if (SelectedCustomer > 0)
                    {
                        if (!string.IsNullOrEmpty(getSelectedCustomer.CompanyName))
                        {
                            ddl[count] = new Dropdown { name = getSelectedCustomer.ContactPerson + " - " + getSelectedCustomer.CompanyName, val = Convert.ToString(getSelectedCustomer.ID) };
                        }
                        else
                        {
                            ddl[count] = new Dropdown { name = getSelectedCustomer.ContactPerson, val = Convert.ToString(getSelectedCustomer.ID) };
                        }

                        //ddl[count] = new Dropdown { name = getSelectedCustomer.CustomerID + " - " + getSelectedCustomer.ContactPerson, val = Convert.ToString(getSelectedCustomer.ID) };
                        count++;

                        getAllCustomer = getAllCustomer.Where(c => c.ID != getSelectedCustomer.ID).ToList();
                    }

                    foreach (Customer _getAllCustomer in getAllCustomer)
                    {
                        if (!string.IsNullOrEmpty(_getAllCustomer.CompanyName))
                        {
                            ddl[count] = new Dropdown { name = _getAllCustomer.ContactPerson + " - " + _getAllCustomer.CompanyName, val = Convert.ToString(_getAllCustomer.ID) };
                        }
                        else
                        {
                            ddl[count] = new Dropdown { name = _getAllCustomer.ContactPerson, val = Convert.ToString(_getAllCustomer.ID) };
                        }

                        count++;
                    }

                    return ddl;
                } 
            }
        }

        //User Dropdown
        public Dropdown[] UserDDL()
        {

            IList<User> getAllUser = _usersModel.GetAll().Where(e => e.Role != "Accounts").ToList();
            //IList<Customer> getAllCustomer = _customersModel.GetAll();

            int count = 0;
            Dropdown[] ddl = new Dropdown[getAllUser.Count];

            foreach (User _getAllUser in getAllUser)
            {
                ddl[count] = new Dropdown { name = _getAllUser.UserID + " - " + _getAllUser.Name, val = Convert.ToString(_getAllUser.ID) };
                count++;
            }

            //foreach(Customer _getAllCustomer in getAllCustomer)
            //{
            //    ddl[count] = new Dropdown { name = _getAllCustomer.CustomerID + " - " + _getAllCustomer.ContactPerson, val = "C-" + Convert.ToString(_getAllCustomer.ID) };
            //    count++;
            //}

            return ddl;
        }

        //Own and downline Dropdown
        public Dropdown[] OwnAndDownlineDDL()
        {
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            IList<User> getAllUser = _usersModel.ListOwnAndDownline(loginMainID).Where(e => e.Role != "Accounts").ToList();

            int count = 0;
            Dropdown[] ddl = new Dropdown[getAllUser.Count];

            foreach (User _getAllUser in getAllUser)
            {
                ddl[count] = new Dropdown { name = _getAllUser.UserID + " - " + _getAllUser.Name, val = Convert.ToString(_getAllUser.ID) };
                count++;
            }

            return ddl;
        }

        //GET: GetCustomerInfo
        public string GetCustomerInfo(int id)
        {
            string result = "";

            Customer getCustomerInfo = _customersModel.GetSingle(id);

            string item1 = "\"Address\": \"" + getCustomerInfo.Address + "\", \"Tel\": \"" + getCustomerInfo.Tel + "\", \"Fax\": \"" + getCustomerInfo.Fax + "\", \"ContactPerson\": \"" + getCustomerInfo.ContactPerson + "\"";
            result += "[{" + item1 + "}]";

            return result;
        }

        //GET: GetQuotationCoBroke
        public ActionResult GetQuotationCoBroke(int id)
        {
            ViewData["RowId"] = id;

            Dropdown[] userDDL = UserDDL();
            ViewData["UserDropdown"] = new SelectList(userDDL, "val", "name", null);

            Dropdown[] percentageOfProfitDDL = PercentageOfProfitDDL();
            ViewData["PercentageDropdown"] = new SelectList(PercentageOfProfitDDL(), "val", "name", "5");

            return View();
        }

        //GET: GetQuotationItemDescriptionInside
        public ActionResult GetQuotationItemDescriptionInside(int id, int mainID)
        {
            ViewData["RowId"] = id;
            ViewData["MainID"] = mainID;

            return View();
        }

        //GET: GetQuotationItemDescriptionOutside
        public ActionResult GetQuotationItemDescriptionOutside(int id, int mainID)
        {
            ViewData["RowId"] = id;
            ViewData["MainID"] = mainID;

            return View();
        }

        //POST: FileUploader
        [HttpPost]
        public void FileUploader()
        {
            string filesUploaded = "";

            try
            {
                if (Request.Files.Count > 0)
                {
                    foreach (string key in Request.Files)
                    {
                        HttpPostedFileBase attachment = Request.Files[key];

                        if (!string.IsNullOrEmpty(attachment.FileName))
                        {
                            string mimeType = attachment.ContentType;
                            int fileLength = attachment.ContentLength;

                            string[] allowedTypes = ConfigurationManager.AppSettings["AllowedFileTypes"].ToString().Split(',');

                            if (allowedTypes.Contains(mimeType))
                            {
                                if (fileLength <= Convert.ToInt32(ConfigurationManager.AppSettings["MaxFileSize"]) * 1024 * 1024)
                                {
                                    string file = attachment.FileName.Substring(attachment.FileName.LastIndexOf(@"\") + 1, attachment.FileName.Length - (attachment.FileName.LastIndexOf(@"\") + 1));
                                    string fileName = Path.GetFileNameWithoutExtension(file);
                                    string newFileName = FileHelper.sanitiseFilename(fileName) + "_" + DateTime.Now.ToString("yyMMddHHmmss") + Path.GetExtension(file).ToLower();
                                    string path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), newFileName);

                                    if (!System.IO.File.Exists(path))
                                    {
                                        string oriPath = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), newFileName);

                                        attachment.SaveAs(oriPath);

                                        filesUploaded += newFileName + ",";
                                    }
                                    else
                                    {
                                        Response.Write("{\"result\":\"error\",\"msg\":\"" + newFileName + " already exists.\"}");
                                        break;
                                    }
                                }
                                else
                                {
                                    Response.Write("{\"result\":\"error\",\"msg\":\"File size exceeds 2MB.\"}");
                                    break;
                                }
                            }
                            else
                            {
                                Response.Write("{\"result\":\"error\",\"msg\":\"Invalid file type.\"}");
                                break;
                            }
                        }
                        else
                        {
                            Response.Write("{\"result\":\"error\",\"msg\":\"Please select a file to upload.\"}");
                            break;
                        }
                    }
                }
                else
                {
                    Response.Write("{\"result\":\"error\",\"msg\":\"Please select a file to upload.\"}");
                }

                if (!string.IsNullOrEmpty(filesUploaded))
                {
                    Response.Write("{\"result\":\"success\",\"msg\":\"" + filesUploaded.Substring(0, filesUploaded.Length - 1) + "\"}");
                }
            }
            catch
            {
                Response.Write("{\"result\":\"error\",\"msg\":\"An error occured while uploading file.\"}");
            }
        }

        //GET: ValidateAmount
        public string ValidateAmount(string amount)
        {
            string result = "";

            bool checkFormat = FormValidationHelper.AmountFormat(amount);

            if (checkFormat)
            {
                bool checkRange = FormValidationHelper.AmountFormat(amount, 20);

                if (checkRange)
                {
                    string newAmount = FormValidationHelper.AmountFormatter(Convert.ToDecimal(amount), 2);

                    result = "{\"result\":true,\"newAmount\":\"" + newAmount + "\"}";
                }
                else
                {
                    result = "{\"result\":false}";
                }
            }
            else
            {
                result = "{\"result\":false}";
            }

            return result;
        }

        //GET: ValidateAmountForQty
        public string ValidateAmountForQty(string amount)
        {
            string result = "";

            bool checkFormat = FormValidationHelper.AmountFormat(amount);

            if (checkFormat)
            {
                bool checkRange = FormValidationHelper.AmountFormat(amount, 20);

                if (checkRange)
                {
                    string newAmount = FormValidationHelper.AmountFormatter(Convert.ToDecimal(amount), 1);

                    result = "{\"result\":true,\"newAmount\":\"" + newAmount + "\"}";
                }
                else
                {
                    result = "{\"result\":false}";
                }
            }
            else
            {
                result = "{\"result\":false}";
            }

            return result;
        }
    }
}