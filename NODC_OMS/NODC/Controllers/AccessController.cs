﻿using NODC_OMS.Models;
using DataAccess.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using NODC_OMS.Helper;
using System.Collections.Specialized;

namespace NODC_OMS.Controllers
{
    public class AccessController : Controller
    {

        private IUserRepository _usersModel;

        public AccessController()
            : this(new UserRepository())
        {

        }

        public AccessController(IUserRepository usersModel)
        {
            _usersModel = usersModel;
        }

        // GET: Access
        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        //GET: Login
        public ActionResult Login()
        {
            //if have cookie stored in the client site.
            if (Request.Cookies["NODC_User"] != null)
            {
                var value = Request.Cookies["NODC_User"];

                User user2 = _usersModel.GetSingle(Convert.ToInt32(value.Values["UserId"]));

                if(user2 != null){//true
                    if(user2.Status == "Active"){

                        Session.Add("MainID", user2.ID);//main ID
                        Session.Add("LoginID", user2.UserID);//User ID
                        Session.Add("UserName", user2.Name);//User Name

                        string userDescription = "";

                        if(user2.Role != "Sales")
                        {
                            Session.Add("UserGroup", user2.Role); //UserGroup //role
                            Session.Add("Position", ""); //position
                            userDescription = user2.Role;
                        }
                        else
                        {
                            Session.Add("UserGroup", user2.Role); //UserGroup //role
                            Session.Add("Position", user2.Position); //position
                            userDescription = user2.Role + " - " + user2.Position;
                        }

                        int user = user2.ID;
                        string userRole = "User";
                        string tableAffected = "Login Transactions";
                        string description = userDescription + " [" + user2.LoginID + "] Logged In";
                        bool result = AuditLogHelper.WriteAuditLog(user, userRole, tableAffected, description);

                        return RedirectToAction("Index","TaskList");
                    }
                    else
                    {
                        Session.Add("Result", "danger|User suspended!");
                    }
                }
                else
                {
                    Session.Add("Result", "danger|User not found!");
                }
            }

            ViewData["LoginID"] = "";
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(FormCollection form)
        {
            if (string.IsNullOrEmpty(form["LoginID"]))
            {
                ModelState.AddModelError("LoginID", "Login ID is required!");
            }

            if (string.IsNullOrEmpty(form["Password"]))
            {
                ModelState.AddModelError("Password", "Password is required!");
            }

            if(ModelState.IsValid){

                string loginID = form["LoginID"].ToString();
                string password = form["Password"].ToString();

                User user2 = _usersModel.FindLoginID(loginID);

                if(user2 != null)
                {
                    password = EncryptionHelper.Encrypt(password);

                    if (user2.Password == password)
                    {

                        if(user2.Status == "Active")
                        {
                            Session.Add("MainID", user2.ID);//main ID
                            Session.Add("LoginID", user2.LoginID);//User ID
                            Session.Add("UserName", user2.Name);//User Name

                            string userDescription = "";

                            if (user2.Role != "Sales")
                            {
                                Session.Add("UserGroup", user2.Role); //UserGroup //role
                                Session.Add("Position", ""); //position
                                userDescription = user2.Role;
                            }
                            else
                            {
                                Session.Add("UserGroup", user2.Role); //UserGroup //role
                                Session.Add("Position", user2.Position); //position
                                userDescription = user2.Role + " - " + user2.Position;
                            }

                            int user = user2.ID;
                            string userRole = "User";
                            
                            string tableAffected = "Login Transactions";
                            string description = userDescription + " [" + user2.LoginID + "] Logged In";
                            bool result = AuditLogHelper.WriteAuditLog(user, userRole, tableAffected, description);

                            if (!string.IsNullOrEmpty(form["remember"]))
                            {
                                int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["RememberMeTimeOut"]);
                                HttpCookie cookie = new HttpCookie("NODC_User");
                                cookie.Values.Add("UserId", user2.ID.ToString());// UserId=2
                                cookie.Expires = DateTime.Now.AddDays(timeout);
                                Response.Cookies.Add(cookie);
                            }

                            return RedirectToAction("Index", "TaskList");
                        }else
                        {
                            Session.Add("Result", "danger|User suspended!");
                        }
                    }
                    else
                    {
                        Session.Add("Result", "danger|Password incorrect!");
                    }
                }
                else
                {
                    Session.Add("Result", "danger|User record not found!");
                }
            }

            ViewData["LoginID"] = form["LoginID"].ToString();
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Logout
        public ActionResult Logout(string msg = null)
        {

            if (Response.Cookies["NODC_User"] != null)
            {
                HttpCookie cookie = Response.Cookies["NODC_User"];
                cookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(cookie);
            }

            Session.RemoveAll();

            if (!string.IsNullOrEmpty(msg))
            {
                Session.Add("Result", "danger|" + msg);
            }

            return RedirectToAction("Login");
        }

        //GET: Forgot Password
        public ActionResult ForgotPassword()
        {
            ViewData["LoginID"] = "";
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        // POST: Forget Password
        [HttpPost]
        public ActionResult ForgotPassword(FormCollection form)
        {

            string loginID = form["LoginID"].Trim();

            ViewData["LoginID"] = loginID;

            if (string.IsNullOrEmpty(loginID))
            {
                ModelState.AddModelError("LoginID", "Login ID field is required.");
            }

            if (ModelState.IsValid)
            {
                User userData = _usersModel.FindLoginID(loginID);

                if (userData != null)
                {
                    if(!string.IsNullOrEmpty(userData.Email))
                    {
                        // Email
                        string key = EncryptionHelper.GenerateRandomString(8);
                        string token = EncryptionHelper.GenerateHash(key, ConfigurationManager.AppSettings["Salt"].ToString());

                        bool updated = _usersModel.AssignResetPassword(userData.ID, token);

                        if (updated)
                        {
                            int user = userData.ID;
                            string userRole = userData.Role;
                            if (userRole == "Sales")
                            {
                                userRole += " " + userData.Position.ToString();
                            }

                            string tableAffected = "Users";

                            string description = "";

                            if (userData.Role != "Sales")
                            {
                                description = userData.Role + " [" + userData.LoginID + "] Forgot Password ||";
                            }
                            else
                            {
                                description = userData.Role + " - " + userData.Position + " [" + userData.LoginID + "] Forgot Password ||";
                            }

                            string subject = "Reset Password for Website Administration System";
                            string body = System.IO.File.ReadAllText(Server.MapPath("~/Templates/ForgotPassword.html"));
                            string recipient = userData.Email;
                            string name = userData.Name;
                            ListDictionary replacements = new ListDictionary();
                            replacements.Add("<%Name%>", name);
                            replacements.Add("<%Url%>", Url.Action("ResetPassword", "Access", new { @loginID = loginID, @key = key }, "http"));

                            bool sent = EmailHelper.SendEmail(subject, body, replacements, recipient);

                            if (sent)
                            {
                                Session.Add("Result", "success|An email has been sent to you regarding instructions on how to reset your password.");

                                if (userData.Role != "Sales")
                                {
                                    description = userData.Role + " [" + userData.LoginID + "] Forgot Password || Email Sent";
                                }
                                else
                                {
                                    description = userData.Role + " - " + userData.Position + " [" + userData.LoginID + "] Forgot Password || Email Sent";
                                }

                                bool result = AuditLogHelper.WriteAuditLog(user, userRole, tableAffected, description);

                                return RedirectToAction("Login", "Access");
                            }
                            else
                            {
                                if (userData.Role != "Sales")
                                {
                                    description = userData.Role + " [" + userData.LoginID + "] Forgot Password || Email Not Sent";
                                }
                                else
                                {
                                    description = userData.Role + " - " + userData.Position + " [" + userData.LoginID + "] Forgot Password || Email Not Sent";
                                }

                                bool result = AuditLogHelper.WriteAuditLog(user, userRole, tableAffected, description);

                                Session.Add("Result", "danger|An error occurred while sending email.");
                            }
                        }
                        else
                        {
                            Session.Add("Result", "danger|An error occurred while generating email.");
                        }
                    }
                    else
                    {
                        Session.Add("Result", "danger|Email not found. Please contact system admin.");
                    }
                }
                else
                {
                    Session.Add("Result", "danger|User record not found.");
                }
            }
            else
            {
                Session.Add("Result", "danger|There are errors in the form!");
            }

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        // GET: Reset Password

        public ActionResult ResetPassword(string loginID = "", string key = "")
        {
            ViewData["LoginID"] = loginID;
            ViewData["key"] = key;

            if (String.IsNullOrEmpty(key))
            {
                Session.Add("Result", "danger|No key provided!");

                return RedirectToAction("ForgotPassword", "Access");
            }
            else
            {
                string token = EncryptionHelper.GenerateHash(key, ConfigurationManager.AppSettings["Salt"].ToString());

                User userData = _usersModel.FindLoginID(loginID);

                if (userData != null)
                {
                    if (userData.ResetPasswordToken != token)
                    {
                        Session.Add("Result", "danger|Invalid key provided!");

                        return RedirectToAction("ForgotPassword", "Access");
                    }
                }
                else
                {
                    Session.Add("Result", "danger|User record not found!");

                    return RedirectToAction("ForgotPassword", "Access");
                }
            }

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: ResetPassword

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(FormCollection form)
        {
            string loginID = form["LoginID"].Trim();
            string password = form["Password"].Trim();
            string confirmPassword = form["ConfirmPassword"].Trim();
            string tokenCheck = form["tokenCheck"].Trim();

            ViewData["LoginID"] = loginID;
            ViewData["key"] = tokenCheck;

            if (string.IsNullOrEmpty(loginID))
            {
                ModelState.AddModelError("LoginID", "Login ID is required!");
            }

            if (string.IsNullOrEmpty(password))
            {
                ModelState.AddModelError("Password", "Password is required!");
            }

            if (string.IsNullOrEmpty(confirmPassword))
            {
                ModelState.AddModelError("ConfirmPassword", "Confirm Password is required!");
            }
            else
            {
                if (!string.IsNullOrEmpty(password) && password != confirmPassword)
                {
                    ModelState.AddModelError("Password", "New Password and Confirm Password not match!");
                }
                else
                {
                    password = EncryptionHelper.Encrypt(password);
                }
            }

            if (ModelState.IsValid)
            {
                User userData = _usersModel.FindLoginID(loginID);

                string token = EncryptionHelper.GenerateHash(tokenCheck, ConfigurationManager.AppSettings["Salt"].ToString());

                if (userData != null)
                {

                    if (!string.IsNullOrEmpty(userData.ResetPasswordToken))
                    {
                        if(userData.ResetPasswordToken == token)
                        {
                            userData.Password = password;
                            userData.ResetPasswordToken = null;

                            bool updated = _usersModel.Update(userData.ID, userData);

                            if (updated)
                            {
                                int user = userData.ID;
                                string userRole = userData.Role;
                                if (userRole == "Sales")
                                {
                                    userRole += " " + userData.Position.ToString();
                                }
                                string tableAffected = "Users";

                                string description = "";

                                if (userData.Role != "Sales")
                                {
                                    description = userData.Role + " User [" + userData.LoginID + "] Reset Password";
                                }
                                else
                                {
                                    description = userData.Role + " - " + userData.Position + " User [" + userData.LoginID + "] Reset Password";
                                }

                                bool result = AuditLogHelper.WriteAuditLog(user, userRole, tableAffected, description);

                                Session.Add("Result", "success|Password updated! You may login using your new password!");

                                return RedirectToAction("Login", "Access");
                            }
                            else
                            {
                                Session.Add("Result", "danger|An error occurred while updating password!");
                            }
                        }
                        else
                        {
                            Session.Add("Result", "danger|Please enter your reset email address!");
                        }
                    }
                    else
                    {
                        Session.Add("Result", "danger|This email address never request for reset password!");
                    }
                    
                }
                else
                {
                    Session.Add("Result", "danger|User record not found!");
                }
            }
            else
            {
                Session.Add("Result", "danger|There are errors in the form!");
            }

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }
    }
}