﻿using DataAccess.POCO;
using NODC_OMS.Helper;
using NODC_OMS.Controllers;
using NODC_OMS.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NODC_OMS.Controllers
{
    [HandleError]
    [RedirecttingActionAdmin]
    public class SystemSettingController : ControllerBase
    {
        private ISystemSettingRepository _settingsModel;
        private IUserRepository _usersModel;

        // GET: Setting
        public SystemSettingController()
            : this(new SystemSettingRepository(), new UserRepository())
        {

        }

        public SystemSettingController(ISystemSettingRepository settingsModel, IUserRepository usersModel)
        {
            _settingsModel = settingsModel;
            _usersModel = usersModel;
        }


        // GET: SystemSetting
        public ActionResult Index()
        {
            return RedirectToAction("Listing");
        }


        // GET: Listing
        public ActionResult Listing()
        {

            IList<SystemSetting> setting = _settingsModel.GetAll();

            ViewData["SystemSetting"] = setting;
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: EditSystemSetting
        public ActionResult EditSystemSetting()
        {
            if (Session["UserGroup"].ToString() == "View Only")
            {
                return RedirectToAction("Listing");
            }

            IList<SystemSetting> settings = _settingsModel.GetAll();
            ViewData["SystemSettings"] = settings;

            //COMPANY GST
            SystemSetting CompanyList = settings.Where(e => e.Code == "LIST_OF_COMPANY").FirstOrDefault();
            string[] companyList = CompanyList.Value.Split(';');

            int count = 1;

            foreach (string _companyList in companyList)
            {
                string[] val = _companyList.Split('|');

                Dropdown[] gSTDDL = GSTDDL();
                ViewData["GSTDropdown_" + count] = new SelectList(gSTDDL, "val", "name",val[1]);
                count++;
            }

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: EditSystemSetting
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditSystemSetting(FormCollection form)
        {
            List<string> settingKeys = form.AllKeys.Where(e => e.Contains("SystemSettings_ID_")).ToList();

            foreach (string key in settingKeys)
            {
                string[] _id = key.Split('_');

                SystemSetting setting = _settingsModel.GetSingle(form["SystemSettings_Code_" + _id[2]].ToString());

                if (form["SystemSettings_Code_" + _id[2]].ToString() == "AGENCY_LIST")
                {
                    //This is the First Agency Item Validation
                    if (string.IsNullOrEmpty(form["SystemSettings_" + _id[2] + "_1_Code"]))
                    {
                        ModelState.AddModelError("SystemSettings_" + _id[2] + "_1_Code", "Agency is required!");
                    }

                    //This is the Second Agency Item Validation
                    if (string.IsNullOrEmpty(form["SystemSettings_" + _id[2] + "_2_Code"]))
                    {
                        ModelState.AddModelError("SystemSettings_" + _id[2] + "_2_Code", "Agency is required!");
                    }
                }
                else if (form["SystemSettings_Code_" + _id[2]].ToString() == "QUOTATION_CATEGORIES")
                {
                    //This is the Quotation Categories Item Validation
                    List<string> QuotationCategoriesKeys = form.AllKeys.Where(e => e.Contains(_id[2] + "_")).ToList();

                    if (QuotationCategoriesKeys.Count == 0)
                    {
                        //ModelState.AddModelError("QuotationMessage", "Please add AT LEAST ONE quotation category!");
                    }
                    else
                    {
                        int count = 1;

                        foreach (string qcKeys in QuotationCategoriesKeys)
                        {
                            string[] rowId = qcKeys.Split('_');

                            if (string.IsNullOrEmpty(form[qcKeys]))
                            {
                                ModelState.AddModelError("SystemSettings_" + _id[2] + "_" + count + "_Code", "Quotation Name is required!");
                            }
                            count++;
                        }
                    }
                    
                }
                else if (form["SystemSettings_Code_" + _id[2]].ToString() == "LIST_OF_COMPANY")
                {
                    //This is the List of Company Item 1 Validation
                    if (string.IsNullOrEmpty(form["SystemSettings_" + _id[2] + "_1_Code"]))
                    {
                        ModelState.AddModelError("SystemSettings_" + _id[2] + "_1_Code", "Name is required!");
                    }

                    if (string.IsNullOrEmpty(form["SystemSettings_" + _id[2] + "_1_GST"]))
                    {
                        ModelState.AddModelError("SystemSettings_" + _id[2] + "_1_GST", "GST is required!");
                    }

                    //if GST not YES and NO
                    if (form["SystemSettings_" + _id[2] + "_1_GST"] != "Yes" && form["SystemSettings_" + _id[2] + "_1_GST"] != "No")
                    {
                        ModelState.AddModelError("SystemSettings_" + _id[2] + "_1_GST", "GST either Yes or No!");
                    }

                    if (string.IsNullOrEmpty(form["SystemSettings_" + _id[2] + "_1_Info"]))
                    {
                        ModelState.AddModelError("SystemSettings_" + _id[2] + "_1_Info", "Company Info is required!");
                    }

                    //This is the List of Company Item 2 Validation
                    if (string.IsNullOrEmpty(form["SystemSettings_" + _id[2] + "_2_Code"]))
                    {
                        ModelState.AddModelError("SystemSettings_" + _id[2] + "_2_Code", "Name is required!");
                    }

                    if (string.IsNullOrEmpty(form["SystemSettings_" + _id[2] + "_2_GST"]))
                    {
                        ModelState.AddModelError("SystemSettings_" + _id[2] + "_2_GST", "GST is required!");
                    }

                    //if GST not YES and NO
                    if (form["SystemSettings_" + _id[2] + "_2_GST"] != "Yes" && form["SystemSettings_" + _id[2] + "_2_GST"] != "No")
                    {
                        ModelState.AddModelError("SystemSettings_" + _id[2] + "_2_GST", "GST either Yes or No!");
                    }

                    if (string.IsNullOrEmpty(form["SystemSettings_" + _id[2] + "_2_Info"]))
                    {
                        ModelState.AddModelError("SystemSettings_" + _id[2] + "_2_Info", "Company Info is required!");
                    }
                }
                else
                {

                    if(string.IsNullOrEmpty(form["Value_" + _id[2]]))
                    {
                        //This is to check the field is required field or not
                        if(setting.IsRequired == "Y"){
                            ModelState.AddModelError("Value_" + _id[2], "This item is required!");
                        }
                    }
                }
            }

            if (ModelState.IsValid)
            {
                bool settingLog = false;

                foreach (string key in settingKeys)
                {
                    string[] _id = key.Split('_');

                    int id = Convert.ToInt32(form[key]);
                    string value = "";

                    if (form["SystemSettings_Code_" + _id[2]].ToString() == "AGENCY_LIST")
                    {
                        value = form["SystemSettings_" + _id[2] + "_1_Code"] + "|" + form["SystemSettings_" + _id[2] + "_2_Code"];
                    }
                    else if (form["SystemSettings_Code_" + _id[2]].ToString() == "QUOTATION_CATEGORIES")
                    {
                        List<string> quotationCategoriesKeys = form.AllKeys.Where(e => e.Contains(_id[2] + "_")).ToList();

                        if(quotationCategoriesKeys.Count != 0)
                        {

                            foreach (string qcKey in quotationCategoriesKeys)
                            {
                                string[] rowId = qcKey.Split('_');
                                value += form["SystemSettings_" + _id[2] + "_" + rowId[2] + "_Code"] + "|";
                                //value = value.Substring(0, value.Length - 1);
                            }
                            value = value.Substring(0, value.Length - 1);

                        }
                        //System.Diagnostics.Debug.Write(value);
                    }
                    else if (form["SystemSettings_Code_" + _id[2]].ToString() == "LIST_OF_COMPANY")
                    {
                        List<string> listOfCompanyKeys = form.AllKeys.Where(e => e.Contains("Company_" +_id[2] + "_")).ToList();

                        foreach (string locKey in listOfCompanyKeys)
                        {
                            string[] rowId = locKey.Split('_');
                            value += form["SystemSettings_" + _id[2] + "_" + rowId[3] + "_Code"] + "|" + form["SystemSettings_" + _id[2] + "_" + rowId[3] + "_GST"] + "|" + form["SystemSettings_" + _id[2] + "_" + rowId[3] + "_Info"] + ";";
                        }
                        value = value.Substring(0, value.Length - 1);
                    }
                    else
                    {
                        value = form["Value_" + _id[2]].ToString();
                    }

                    bool result = _settingsModel.Update(id, value);

                    if (result)
                    {
                        if (!settingLog)
                        {
                            settingLog = true;
                        }
                    }
                }

                if (settingLog)
                {
                    int userTriggering = Convert.ToInt32(Session["MainID"]);

                    User user = _usersModel.GetSingle(userTriggering);

                    string tableAffected = "SystemSettings";
                    string userRole = Session["UserGroup"].ToString();
                    if (userRole == "Sales")
                    {
                        userRole += " " + Session["Position"].ToString();
                    }
                    string description = Session["UserGroup"].ToString() + " [" + user.Name + "] Updated System Settings";
                    AuditLogHelper.WriteAuditLog(userTriggering, userRole, tableAffected, description);
                }

                Session.Add("Result", "success|System settings have been successfully updated!");

                return RedirectToAction("Listing");
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            IList<SystemSetting> settings = new List<SystemSetting>();

            foreach (string key in settingKeys)
            {
                string[] _id = key.Split('_');

                SystemSetting record = _settingsModel.GetSingle(form["SystemSettings_Code_" + _id[2]].ToString());

                SystemSetting setting = new SystemSetting();
                setting.ID = record.ID;
                setting.Code = record.Code;
                setting.DataType = record.DataType;
                if (form["SystemSettings_Code_" + _id[2]].ToString() == "AGENCY_LIST")
                {
                    setting.Value = form["SystemSettings_" + _id[2] + "_1_Code"] + "|" + form["SystemSettings_" + _id[2] + "_2_Code"];
                }
                else if (form["SystemSettings_Code_" + _id[2]].ToString() == "QUOTATION_CATEGORIES")
                {
                    List<string> quotationCategoriesKeys = form.AllKeys.Where(e => e.Contains(_id[2] + "_")).ToList();

                    foreach (string qcKey in quotationCategoriesKeys)
                    {
                        string[] rowId = qcKey.Split('_');
                        setting.Value += form["SystemSettings_" + _id[2] + "_" + rowId[2] + "_Code"] + "|";
                    }

                    if (!string.IsNullOrEmpty(setting.Value))
                    {
                        setting.Value = setting.Value.Substring(0, setting.Value.Length - 1);
                    }
                }
                else if (form["SystemSettings_Code_" + _id[2]].ToString() == "LIST_OF_COMPANY")
                {
                    List<string> listOfCompanyKeys = form.AllKeys.Where(e => e.Contains("Company_" + _id[2] + "_")).ToList();
                    int count = 1;
                    foreach (string locKey in listOfCompanyKeys)
                    {
                        string[] rowId = locKey.Split('_');
                        setting.Value += form["SystemSettings_" + _id[2] + "_" + rowId[3] + "_Code"] + "|" + form["SystemSettings_" + _id[2] + "_" + rowId[3] + "_GST"] + "|" + form["SystemSettings_" + _id[2] + "_" + rowId[3] + "_Info"] + ";";
                        
                        Dropdown[] gSTDDL = GSTDDL();
                        ViewData["GSTDropdown_" + count] = new SelectList(gSTDDL, "val", "name", form["SystemSettings_" + _id[2] + "_" + rowId[3] + "_GST"]);
                        count++;
                    }
                    setting.Value = setting.Value.Substring(0, setting.Value.Length - 1);
                }
                else
                {
                    setting.Value = form["Value_" + _id[2]];
                }

                settings.Add(setting);
            }

            ViewData["SystemSettings"] = settings;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GST Dropdown
        public Dropdown[] GSTDDL()
        {

            Dropdown[] ddl = new Dropdown[2];

            ddl[0] = new Dropdown { name = "Yes", val = "Yes" };
            ddl[1] = new Dropdown { name = "No", val = "No" };

            return ddl;
        }

    }
}