﻿using DataAccess;
using DataAccess.POCO;
using Newtonsoft.Json;
using NODC_OMS.Controllers;
using NODC_OMS.Helper;
using NODC_OMS.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NODC_OMS.Controllers
{
    [HandleError]
    [RedirectingAction]
    public class InvoiceController : ControllerBase
    {
        private IInvoiceRepository _invoicesModel;
        private IInvoiceItemDescriptionRepository _invoiceItemsModel;
        private IProjectRepository _projectsModel;
        private IQuotationRepository _quotationsModel;
        private ICustomerRepository _customersModel;
        private IUserRepository _usersModel;
        private ISystemSettingRepository _settingsModel;

        // GET: Project
        public InvoiceController()
            : this(
            new InvoiceRepository(),
            new InvoiceItemDescriptionRepository(),
            new ProjectRepository(),
            new QuotationRepository(),
            new CustomerRepository(),
            new UserRepository(),
            new SystemSettingRepository())
        {
        }

        public InvoiceController(
            IInvoiceRepository invoicesModel,
            IInvoiceItemDescriptionRepository invoiceItemsModel,
            IProjectRepository projectsModel,
            IQuotationRepository quotationsModel,
            ICustomerRepository customersModel,
            IUserRepository usersModel,
            ISystemSettingRepository settingsModel)
        {
            _invoicesModel = invoicesModel;
            _invoiceItemsModel = invoiceItemsModel;
            _projectsModel = projectsModel;
            _quotationsModel = quotationsModel;
            _customersModel = customersModel;
            _usersModel = usersModel;
            _settingsModel = settingsModel;
        }

        // GET: Invoice
        public ActionResult Index()
        {
            if (Session["SearchKeyword"] != null)
            {
                Session.Remove("SearchKeyword");
            }

            if (Session["SearchStatus"] != null)
            {
                Session.Remove("SearchStatus");
            }

            return RedirectToAction("Listing");
        }

        //GET: Listing
        public ActionResult Listing(int page = 1)
        {
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = "";
            if (Session["SearchKeyword"] != null)
            {
                ViewData["SearchKeyword"] = Session["SearchKeyword"];
            }

            ViewData["SearchStatus"] = "";
            if (Session["SearchStatus"] != null)
            {
                ViewData["SearchStatus"] = Session["SearchStatus"];
            }

            ViewData["GetStatusValue"] = "All Invoices";
            if (ViewData["SearchStatus"].ToString() == "Pending")
            {
                ViewData["GetStatusValue"] = "Pending Payment Only";
            }

            List<int> salesUserId = new List<int>();

            if (Session["UserGroup"].ToString() == "Sales")
            {
                salesUserId.Add(Convert.ToInt32(Session["MainID"]));

                List<User> uplineUsers = _usersModel.GetAllSameUplineID(Convert.ToInt32(Session["MainID"]));

                foreach (User user in uplineUsers)
                {
                    salesUserId.Add(user.ID);
                }
            }

            IPagedList<Invoice> invoices = _invoicesModel.GetPaged(ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), salesUserId, 0, page, pageSize);
            ViewData["Invoice"] = invoices;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Listing
        [HttpPost]
        public ActionResult Listing(FormCollection form)
        {
            int page = 1;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = form["SearchKeyword"].Trim();
            Session.Add("SearchKeyword", form["SearchKeyword"].Trim());

            ViewData["SearchStatus"] = form["SearchStatusValue"].Trim();
            Session.Add("SearchStatus", form["SearchStatusValue"].Trim());

            ViewData["GetStatusValue"] = "All Invoices";
            if (ViewData["SearchStatus"].ToString() == "Pending")
            {
                ViewData["GetStatusValue"] = "Pending Payment Only";
            }

            List<int> salesUserId = new List<int>();

            if (Session["UserGroup"].ToString() == "Sales")
            {
                salesUserId.Add(Convert.ToInt32(Session["MainID"]));

                List<User> uplineUsers = _usersModel.GetAllSameUplineID(Convert.ToInt32(Session["MainID"]));

                foreach (User user in uplineUsers)
                {
                    salesUserId.Add(user.ID);
                }
            }

            IPagedList<Invoice> invoices = _invoicesModel.GetPaged(ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), salesUserId, 0, page, pageSize);
            ViewData["Invoice"] = invoices;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Create
        public ActionResult Create()
        {
            Dropdown[] quotationDDL = QuotationDDL();
            ViewData["QuotationDropdown"] = new SelectList(quotationDDL, "val", "name");

            Dropdown[] customerDDL = CustomerDDL();
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name");

            Dropdown[] gstDDL = GSTDDL();
            ViewData["GSTDropdown"] = new SelectList(gstDDL, "val", "name");

            List<InvoiceItemDescriptionTable> itemDescriptionsTable = new List<InvoiceItemDescriptionTable>();
            itemDescriptionsTable.Add(new InvoiceItemDescriptionTable()
            {
                RowId = 1,
                Description = null,
                PercentageOfQuotation = "5",
                PercentageOfQuotationDDL = new SelectList(PercentageOfQuotationDDL(), "val", "name", "5"),
                Amount = null
            });
            ViewData["InvoiceItemDescriptionTable"] = itemDescriptionsTable;

            ViewData["QuotationCompany"] = "";
            ViewData["CustomerAddress"] = "";
            ViewData["CustomerTel"] = "";
            ViewData["CustomerFax"] = "";
            ViewData["TotalQuotationAmount"] = "";

            ViewData["InvoiceInfoSection"] = "";
            if (Session["UserGroup"].ToString() == "Sales")
            {
                ViewData["InvoiceInfoSection"] = "style=display:none;";
            }

            ViewData["PostBack"] = "No";

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Create
        [HttpPost]
        public ActionResult Create(Invoice invoice, FormCollection form)
        {
            Quotation quotation = null;
            if (invoice.QuotationId > 0)
            {
                quotation = _quotationsModel.GetSingle(invoice.QuotationId);

                if (quotation == null)
                {
                    ModelState.AddModelError("invoice.QuotationId", "Quotation record not found!");
                }
                else
                {
                    if (quotation.Status != "Confirmed")
                    {
                        ModelState.AddModelError("invoice.QuotationId", "Quotation has not been confirmed!");
                    }
                }
            }

            Customer customer = null;
            if (invoice.CustomerId > 0)
            {
                customer = _customersModel.GetSingle(invoice.CustomerId);

                if (customer == null)
                {
                    ModelState.AddModelError("invoice.CustomerId", "Customer record not found!");
                }
                else
                {
                    if (customer.Status == "Suspended")
                    {
                        ModelState.AddModelError("invoice.CustomerId", "Customer has been suspended!");
                    }
                }
            }

            ModelState paymentMadeState = ModelState["invoice.PaymentMade"];
            if (paymentMadeState.Errors.Count > 0)
            {
                paymentMadeState.Errors.Clear();
            }

            string paymentMade = form["invoice.PaymentMade"];

            if (!string.IsNullOrEmpty(paymentMade))
            {
                bool checkFormat = FormValidationHelper.AmountFormat(paymentMade);

                if (!checkFormat)
                {
                    ModelState.AddModelError("invoice.PaymentMade", "'" + paymentMade + "' is not a valid amount!");
                }
                else
                {
                    invoice.PaymentMade = Convert.ToDecimal(paymentMade);
                }
            }
            else
            {
                invoice.PaymentMade = null;
            }

            List<InvoiceItemDescriptionTable> itemDescriptionsTable = new List<InvoiceItemDescriptionTable>();

            List<string> itemDescriptionKeys = form.AllKeys.Where(e => e.Contains("ItemDescription_Description_")).ToList();

            if (itemDescriptionKeys.Count == 0)
            {
                ModelState.AddModelError("ItemDescription", "Please add AT LEAST ONE Item!");
            }
            else
            {
                foreach (string key in itemDescriptionKeys)
                {
                    string rowId = key.Split('_')[2];

                    string description = form["ItemDescription_Description_" + rowId];

                    if (string.IsNullOrEmpty(description))
                    {
                        ModelState.AddModelError("ItemDescription_Description_" + rowId, "Description is required!");
                    }

                    string percentageOfQuotation = form["ItemDescription_PercentageOfQuotation_" + rowId];

                    string amount = form["ItemDescription_Amount_" + rowId];

                    if (string.IsNullOrEmpty(amount))
                    {
                        ModelState.AddModelError("ItemDescription_Amount_" + rowId, "Amount is required!");
                    }
                    else
                    {
                        bool checkFormat = FormValidationHelper.AmountFormat(amount);

                        if (!checkFormat)
                        {
                            ModelState.AddModelError("ItemDescription_Amount_" + rowId, "'" + amount + "' is not a valid amount!");
                        }
                    }

                    itemDescriptionsTable.Add(new InvoiceItemDescriptionTable()
                    {
                        RowId = Convert.ToInt32(rowId),
                        Description = description,
                        PercentageOfQuotation = percentageOfQuotation,
                        PercentageOfQuotationDDL = new SelectList(PercentageOfQuotationDDL(), "val", "name", percentageOfQuotation),
                        Amount = amount
                    });
                }
            }

            ModelState subtotalState = ModelState["invoice.SubtotalAmount"];
            if (subtotalState.Errors.Count > 0)
            {
                subtotalState.Errors.Clear();
            }
            invoice.SubtotalAmount = Convert.ToDecimal(form["invoice.SubtotalAmount"]);

            ModelState gstAmountState = ModelState["invoice.GSTAmount"];
            if (gstAmountState.Errors.Count > 0)
            {
                gstAmountState.Errors.Clear();
            }
            invoice.GSTAmount = Convert.ToDecimal(form["invoice.GSTAmount"]);

            ModelState grantTotalState = ModelState["invoice.GrandTotalAmount"];
            if (grantTotalState.Errors.Count > 0)
            {
                grantTotalState.Errors.Clear();
            }
            invoice.GrandTotalAmount = Convert.ToDecimal(form["invoice.GrandTotalAmount"]);

            if (ModelState["invoice.ProjectId"] != null)
            {
                ModelState projectId = ModelState["invoice.ProjectId"];
                if (projectId.Errors.Count > 0)
                {
                    projectId.Errors.Clear();
                }
            }

            if (ModelState.IsValid)
            {
                //Assign Invoice ID
                string prefix = _settingsModel.GetCodeValue("PREFIX_INVOICE_ID") + DateTime.Now.ToString("yy");

                Invoice lastRecord = _invoicesModel.GetLast();
                if (lastRecord != null)
                {
                    invoice.InvoiceID = prefix + (lastRecord.ID + 1).ToString().PadLeft(5, '0');
                }
                else
                {
                    invoice.InvoiceID = prefix + "00001";
                }

                invoice.Company = quotation.CompanyName;

                invoice.Status = "Pending Payment";

                if (invoice.PaymentMade != null)
                {
                    if (invoice.PaymentMade >= invoice.GrandTotalAmount)
                    {
                        invoice.Status = "Completed";
                    }
                }

                invoice.PreparedById = Convert.ToInt32(Session["MainID"]);

                bool result = _invoicesModel.Add(invoice);

                if (result)
                {
                    int userId = Convert.ToInt32(Session["MainID"]);
                    string userRole = Session["UserGroup"].ToString();
                    if (userRole == "Sales")
                    {
                        userRole += " " + Session["Position"].ToString();
                    }
                    string tableAffected = "Invoices";
                    string description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Invoice [" + invoice.InvoiceID + "]";

                    bool invoice_create_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);

                    bool hasItemDescription = false;

                    foreach (InvoiceItemDescriptionTable itemDescription in itemDescriptionsTable)
                    {
                        InvoiceItemDescription itemDesc = new InvoiceItemDescription();
                        itemDesc.InvoiceId = invoice.ID;
                        itemDesc.Description = itemDescription.Description;
                        itemDesc.PercentageOfQuotation = itemDescription.PercentageOfQuotation;
                        itemDesc.Amount = Convert.ToDecimal(itemDescription.Amount);

                        bool item_create_result = _invoiceItemsModel.Add(itemDesc);

                        if (item_create_result)
                        {
                            if (!hasItemDescription)
                            {
                                hasItemDescription = true;
                            }
                        }
                    }

                    if (hasItemDescription)
                    {
                        userId = Convert.ToInt32(Session["MainID"]);
                        userRole = Session["UserGroup"].ToString();
                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }
                        tableAffected = "InvoiceItemDescriptions";
                        description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Invoice Item Descriptions [" + invoice.InvoiceID + "]";

                        bool itemDescription_create_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);
                    }

                    Session.Add("Result", "success|" + invoice.InvoiceID + " has been successfully created!");

                    int page = 1;

                    if (Session["Page"] != null)
                    {
                        page = Convert.ToInt32(Session["Page"]);
                    }

                    return RedirectToAction("Listing", new { @page = page });
                }
                else
                {
                    Session.Add("Result", "danger|An error occured while saving invoice records!");
                }
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            Dropdown[] quotationDDL = QuotationDDL();
            ViewData["QuotationDropdown"] = new SelectList(quotationDDL, "val", "name", invoice.QuotationId);

            string fromView = "Edit";

            Dropdown[] projectDDL = ProjectDDL(invoice.QuotationId, fromView);
            ViewData["ProjectDropdown"] = new SelectList(projectDDL, "val", "name", invoice.ProjectId);

            if (invoice.ProjectId == 0)
            {
                if (projectDDL.Length == 0)
                {
                    ViewData["ProjectDropdown"] = null;
                }
            }

            Dropdown[] customerDDL = CustomerDDL();
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", invoice.CustomerId);

            Dropdown[] gstDDL = GSTDDL();
            ViewData["GSTDropdown"] = new SelectList(gstDDL, "val", "name", invoice.GST);

            ViewData["InvoiceItemDescriptionTable"] = itemDescriptionsTable;

            ViewData["QuotationCompany"] = "";
            ViewData["CustomerAddress"] = "";
            ViewData["CustomerTel"] = "";
            ViewData["CustomerFax"] = "";
            ViewData["TotalQuotationAmount"] = "";

            if (quotation != null)
            {
                ViewData["QuotationCompany"] = quotation.CompanyName;

                decimal totalQuotationAmount = 0;

                foreach (QuotationItemDescription itemDescription in quotation.ItemDescriptions)
                {
                    totalQuotationAmount += Convert.ToDecimal(itemDescription.QuotationItemAmount);
                }

                ViewData["TotalQuotationAmount"] = totalQuotationAmount.ToString("#,##0.00");
            }

            if (customer != null)
            {
                ViewData["CustomerAddress"] = customer.Address;
                ViewData["CustomerTel"] = customer.Tel;
                ViewData["CustomerFax"] = customer.Fax;
            }

            ViewData["Invoice"] = invoice;
            ViewData["IsPostBack"] = true;
            ViewData["InvoiceInfoSection"] = "";
            if (Session["UserGroup"].ToString() == "Sales")
            {
                ViewData["InvoiceInfoSection"] = "style=display:none;";
            }

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Edit
        public ActionResult Edit(int id)
        {
            Invoice invoice = _invoicesModel.GetSingle(id);

            if (invoice != null)
            {
                if (Session["UserGroup"].ToString() == "Sales")
                {
                    return RedirectToAction("View", new { @id = id });
                }
                else
                {
                    if (invoice.Status == "Completed")
                    {
                        return RedirectToAction("View", new { @id = id });
                    }
                }
            }
            else
            {
                Session.Add("Result", "danger|Invoice record not found!");

                int page = 1;

                if (Session["Page"] != null)
                {
                    page = Convert.ToInt32(Session["Page"]);
                }

                return RedirectToAction("Listing", new { @page = page });
            }

            Quotation quotation = _quotationsModel.GetSingle(invoice.QuotationId);
            Customer customer = _customersModel.GetSingle(invoice.CustomerId);

            Dropdown[] quotationDDL = QuotationDDL(invoice.QuotationId);
            ViewData["QuotationDropdown"] = new SelectList(quotationDDL, "val", "name", invoice.QuotationId);

            string fromView = "Edit";

            Dropdown[] projectDDL = ProjectDDL(invoice.QuotationId, fromView);
            ViewData["ProjectDropdown"] = new SelectList(projectDDL, "val", "name", invoice.ProjectId);

            if (invoice.ProjectId == 0)
            {
                if (projectDDL.Length == 0)
                {
                    ViewData["ProjectDropdown"] = null;
                }
            }


            Dropdown[] customerDDL = CustomerDDL(invoice.CustomerId);
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", invoice.CustomerId);

            Dropdown[] gstDDL = GSTDDL();
            ViewData["GSTDropdown"] = new SelectList(gstDDL, "val", "name", invoice.GST);

            List<InvoiceItemDescriptionTable> itemDescriptionsTable = new List<InvoiceItemDescriptionTable>();

            int rowId = 1;

            foreach (InvoiceItemDescription itemDescription in invoice.ItemDescriptions)
            {
                itemDescriptionsTable.Add(new InvoiceItemDescriptionTable()
                {
                    RowId = rowId,
                    Description = itemDescription.Description,
                    PercentageOfQuotation = itemDescription.PercentageOfQuotation,
                    PercentageOfQuotationDDL = new SelectList(PercentageOfQuotationDDL(), "val", "name", itemDescription.PercentageOfQuotation),
                    Amount = itemDescription.Amount.ToString("#,##0.00")
                });

                rowId++;
            }

            ViewData["InvoiceItemDescriptionTable"] = itemDescriptionsTable;

            ViewData["QuotationCompany"] = "";
            ViewData["CustomerAddress"] = "";
            ViewData["CustomerTel"] = "";
            ViewData["CustomerFax"] = "";
            ViewData["TotalQuotationAmount"] = "";

            if (quotation != null)
            {
                ViewData["QuotationCompany"] = quotation.CompanyName;

                decimal totalQuotationAmount = 0;

                foreach (QuotationItemDescription itemDescription in quotation.ItemDescriptions)
                {
                    totalQuotationAmount += Convert.ToDecimal(itemDescription.QuotationItemAmount);
                }

                ViewData["TotalQuotationAmount"] = totalQuotationAmount.ToString("#,##0.00");
            }

            if (customer != null)
            {
                ViewData["CustomerAddress"] = customer.Address;
                ViewData["CustomerTel"] = customer.Tel;
                ViewData["CustomerFax"] = customer.Fax;
            }

            ViewData["Invoice"] = invoice;
            ViewData["InvoiceInfoSection"] = "";
            if (Session["UserGroup"].ToString() == "Sales")
            {
                ViewData["InvoiceInfoSection"] = "style=display:none;";
            }

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Edit
        [HttpPost]
        public ActionResult Edit(int id, Invoice invoice, FormCollection form)
        {
            Invoice oldData = _invoicesModel.GetSingle(id);
            invoice.ID = oldData.ID;
            invoice.InvoiceID = oldData.InvoiceID;
            invoice.PreparedById = oldData.PreparedById;
            invoice.PreparedBy = oldData.PreparedBy;
            invoice.Status = oldData.Status;

            Quotation quotation = null;
            if (invoice.QuotationId > 0)
            {
                quotation = _quotationsModel.GetSingle(invoice.QuotationId);

                if (quotation == null)
                {
                    ModelState.AddModelError("invoice.QuotationId", "Quotation record not found!");
                }
                else
                {
                    if (quotation.Status != "Confirmed")
                    {
                        ModelState.AddModelError("invoice.QuotationId", "Quotation has not been confirmed!");
                    }
                }
            }

            Customer customer = null;
            if (invoice.CustomerId > 0)
            {
                customer = _customersModel.GetSingle(invoice.CustomerId);

                if (customer == null)
                {
                    ModelState.AddModelError("invoice.CustomerId", "Customer record not found!");
                }
                else
                {
                    if (customer.Status == "Suspended")
                    {
                        ModelState.AddModelError("invoice.CustomerId", "Customer has been suspended!");
                    }
                }
            }

            ModelState paymentMadeState = ModelState["invoice.PaymentMade"];
            if (paymentMadeState.Errors.Count > 0)
            {
                paymentMadeState.Errors.Clear();
            }

            string paymentMade = form["invoice.PaymentMade"];

            if (!string.IsNullOrEmpty(paymentMade))
            {
                bool checkFormat = FormValidationHelper.AmountFormat(paymentMade);

                if (!checkFormat)
                {
                    ModelState.AddModelError("invoice.PaymentMade", "'" + paymentMade + "' is not a valid amount!");
                }
                else
                {
                    invoice.PaymentMade = Convert.ToDecimal(paymentMade);
                }
            }
            else
            {
                invoice.PaymentMade = null;
            }

            List<InvoiceItemDescriptionTable> itemDescriptionsTable = new List<InvoiceItemDescriptionTable>();

            List<string> itemDescriptionKeys = form.AllKeys.Where(e => e.Contains("ItemDescription_Description_")).ToList();

            if (itemDescriptionKeys.Count == 0)
            {
                ModelState.AddModelError("ItemDescription", "Please add AT LEAST ONE Item!");
            }
            else
            {
                foreach (string key in itemDescriptionKeys)
                {
                    string rowId = key.Split('_')[2];

                    string description = form["ItemDescription_Description_" + rowId];

                    if (string.IsNullOrEmpty(description))
                    {
                        ModelState.AddModelError("ItemDescription_Description_" + rowId, "Description is required!");
                    }

                    string percentageOfQuotation = form["ItemDescription_PercentageOfQuotation_" + rowId];

                    string amount = form["ItemDescription_Amount_" + rowId];

                    if (string.IsNullOrEmpty(amount))
                    {
                        ModelState.AddModelError("ItemDescription_Amount_" + rowId, "Amount is required!");
                    }
                    else
                    {
                        bool checkFormat = FormValidationHelper.AmountFormat(amount);

                        if (!checkFormat)
                        {
                            ModelState.AddModelError("ItemDescription_Amount_" + rowId, "'" + amount + "' is not a valid amount!");
                        }
                    }

                    itemDescriptionsTable.Add(new InvoiceItemDescriptionTable()
                    {
                        RowId = Convert.ToInt32(rowId),
                        Description = description,
                        PercentageOfQuotation = percentageOfQuotation,
                        PercentageOfQuotationDDL = new SelectList(PercentageOfQuotationDDL(), "val", "name", percentageOfQuotation),
                        Amount = amount
                    });
                }
            }

            ModelState subtotalState = ModelState["invoice.SubtotalAmount"];
            if (subtotalState.Errors.Count > 0)
            {
                subtotalState.Errors.Clear();
            }
            invoice.SubtotalAmount = Convert.ToDecimal(form["invoice.SubtotalAmount"]);

            ModelState gstAmountState = ModelState["invoice.GSTAmount"];
            if (gstAmountState.Errors.Count > 0)
            {
                gstAmountState.Errors.Clear();
            }
            invoice.GSTAmount = Convert.ToDecimal(form["invoice.GSTAmount"]);

            ModelState grantTotalState = ModelState["invoice.GrandTotalAmount"];
            if (grantTotalState.Errors.Count > 0)
            {
                grantTotalState.Errors.Clear();
            }

            if(ModelState["ProjectId"] != null)
            {
                ModelState projectId = ModelState["invoice.ProjectId"];
                if (projectId.Errors.Count > 0)
                {
                    projectId.Errors.Clear();
                }
            }

            invoice.GrandTotalAmount = Convert.ToDecimal(form["invoice.GrandTotalAmount"]);

            if (ModelState.IsValid)
            {
                invoice.Company = quotation.CompanyName;

                invoice.Status = "Pending Payment";

                if (invoice.PaymentMade != null)
                {
                    if (invoice.PaymentMade >= invoice.GrandTotalAmount)
                    {
                        invoice.Status = "Completed";
                    }
                }

                bool result = _invoicesModel.Update(id, invoice);

                if (result)
                {
                    int userId = Convert.ToInt32(Session["MainID"]);
                    string userRole = Session["UserGroup"].ToString();
                    if (userRole == "Sales")
                    {
                        userRole += " " + Session["Position"].ToString();
                    }
                    string tableAffected = "Invoices";
                    string description = userRole + " [" + Session["LoginID"].ToString() + "] Updated Invoice [" + invoice.InvoiceID + "]";

                    bool invoice_update_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);

                    bool hasDeleteItemDescription = false;

                    if (oldData.ItemDescriptions.Count > 0)
                    {
                        foreach (InvoiceItemDescription itemDescription in oldData.ItemDescriptions)
                        {
                            bool itemDescription_delete_result = _invoiceItemsModel.Delete(itemDescription.ID);

                            if (itemDescription_delete_result)
                            {
                                if (!hasDeleteItemDescription)
                                {
                                    hasDeleteItemDescription = true;
                                }
                            }
                        }
                    }

                    bool hasItemDescription = false;

                    foreach (InvoiceItemDescriptionTable itemDescription in itemDescriptionsTable)
                    {
                        InvoiceItemDescription itemDesc = new InvoiceItemDescription();
                        itemDesc.InvoiceId = invoice.ID;
                        itemDesc.Description = itemDescription.Description;
                        itemDesc.PercentageOfQuotation = itemDescription.PercentageOfQuotation;
                        itemDesc.Amount = Convert.ToDecimal(itemDescription.Amount);

                        bool item_create_result = _invoiceItemsModel.Add(itemDesc);

                        if (item_create_result)
                        {
                            if (!hasItemDescription)
                            {
                                hasItemDescription = true;
                            }
                        }
                    }

                    if (hasDeleteItemDescription)
                    {
                        userId = Convert.ToInt32(Session["MainID"]);
                        userRole = Session["UserGroup"].ToString();
                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }
                        tableAffected = "InvoiceItemDescriptions";
                        description = userRole + " [" + Session["LoginID"].ToString() + "] Deleted Invoice Item Descriptions [" + invoice.InvoiceID + "]";

                        bool itemDescription_delete_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);
                    }

                    if (hasItemDescription)
                    {
                        userId = Convert.ToInt32(Session["MainID"]);
                        userRole = Session["UserGroup"].ToString();
                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }
                        tableAffected = "InvoiceItemDescriptions";
                        description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Invoice Item Descriptions [" + invoice.InvoiceID + "]";

                        bool itemDescription_create_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);
                    }

                    Session.Add("Result", "success|" + invoice.InvoiceID + " has been successfully updated!");

                    int page = 1;

                    if (Session["Page"] != null)
                    {
                        page = Convert.ToInt32(Session["Page"]);
                    }

                    return RedirectToAction("Listing", new { @page = page });
                }
                else
                {
                    Session.Add("Result", "danger|An error occured while updating invoice records!");
                }
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            Dropdown[] quotationDDL = QuotationDDL();
            ViewData["QuotationDropdown"] = new SelectList(quotationDDL, "val", "name", invoice.QuotationId);

            Dropdown[] customerDDL = CustomerDDL();
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", invoice.CustomerId);

            Dropdown[] gstDDL = GSTDDL();
            ViewData["GSTDropdown"] = new SelectList(gstDDL, "val", "name", invoice.GST);

            ViewData["InvoiceItemDescriptionTable"] = itemDescriptionsTable;

            ViewData["QuotationCompany"] = "";
            ViewData["CustomerAddress"] = "";
            ViewData["CustomerTel"] = "";
            ViewData["CustomerFax"] = "";
            ViewData["TotalQuotationAmount"] = "";

            if (quotation != null)
            {
                ViewData["QuotationCompany"] = quotation.CompanyName;

                decimal totalQuotationAmount = 0;

                foreach (QuotationItemDescription itemDescription in quotation.ItemDescriptions)
                {
                    totalQuotationAmount += Convert.ToDecimal(itemDescription.QuotationItemAmount);
                }

                ViewData["TotalQuotationAmount"] = totalQuotationAmount.ToString("#,##0.00");
            }

            if (customer != null)
            {
                ViewData["CustomerAddress"] = customer.Address;
                ViewData["CustomerTel"] = customer.Tel;
                ViewData["CustomerFax"] = customer.Fax;
            }

            ViewData["Invoice"] = invoice;
            ViewData["InvoiceInfoSection"] = "";
            if (Session["UserGroup"].ToString() == "Sales")
            {
                ViewData["InvoiceInfoSection"] = "style=display:none;";
            }

            string fromView = "Edit";
            ViewData["PostBack"] = true;

            Dropdown[] projectDDL = ProjectDDL(invoice.QuotationId, fromView);
            ViewData["ProjectDropdown"] = new SelectList(projectDDL, "val", "name", invoice.ProjectId);

            if(invoice.ProjectId == 0)
            {
                if(projectDDL.Length == 0)
                {
                    ViewData["ProjectDropdown"] = null;
                }
            }

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: MakePayment
        public ActionResult MakePayment(int id)
        {
            if (Session["UserGroup"].ToString() == "Super Admin")
            {
                Invoice invoice = _invoicesModel.GetSingle(id);

                if (invoice != null)
                {
                    invoice.PaymentMade = invoice.GrandTotalAmount;
                    invoice.Status = "Completed";

                    bool result = _invoicesModel.Update(id, invoice);

                    if (result)
                    {
                        int userId = Convert.ToInt32(Session["MainID"]);
                        string userRole = Session["UserGroup"].ToString();
                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }
                        string tableAffected = "Invoices";
                        string description = userRole + " [" + Session["LoginID"].ToString() + "] Made Payment to Invoice [" + invoice.InvoiceID + "]";

                        bool invoice_update_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);

                        Session.Add("Result", "danger|" + invoice.InvoiceID + " has been successfully paid!");
                    }
                    else
                    {
                        Session.Add("Result", "danger|An error occured while updating invoice record!");
                    }
                }
                else
                {
                    Session.Add("Result", "danger|Invoice record not found!");
                }
            }
            else
            {
                return RedirectToAction("Logout", "Access", new { @msg = "You have no access to this module!" });
            }

            int page = 1;

            if (Session["Page"] != null)
            {
                page = Convert.ToInt32(Session["Page"]);
            }

            return RedirectToAction("Listing", new { @page = page });
        }

        //GET: Delete
        public ActionResult Delete(int id)
        {
            if (Session["UserGroup"].ToString() != "Sales")
            {
                Invoice invoice = _invoicesModel.GetSingle(id);

                if (invoice != null)
                {
                    bool result = _invoicesModel.Delete(id);

                    if (result)
                    {
                        int userId = Convert.ToInt32(Session["MainID"]);
                        string userRole = Session["UserGroup"].ToString();
                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }
                        string tableAffected = "Invoices";
                        string description = userRole + " [" + Session["LoginID"].ToString() + "] Deleted Invoice [" + invoice.InvoiceID + "]";

                        bool invoice_delete_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);

                        Session.Add("Result", "danger|" + invoice.InvoiceID + " has been successfully deleted!");
                    }
                    else
                    {
                        Session.Add("Result", "danger|An error occured while deleting invoice record!");
                    }
                }
                else
                {
                    Session.Add("Result", "danger|Invoice record not found!");
                }
            }
            else
            {
                return RedirectToAction("Logout", "Access", new { @msg = "You have no access to this module!" });
            }

            int page = 1;

            if (Session["Page"] != null)
            {
                page = Convert.ToInt32(Session["Page"]);
            }

            return RedirectToAction("Listing", new { @page = page });
        }

        //GET: View
        public ActionResult View(int id)
        {
            Invoice invoice = _invoicesModel.GetSingle(id);

            if (invoice == null)
            {
                Session.Add("Result", "danger|Invoice record not found!");

                int page = 1;

                if (Session["Page"] != null)
                {
                    page = Convert.ToInt32(Session["Page"]);
                }

                return RedirectToAction("Listing", new { @page = page });
            }

            Quotation quotation = _quotationsModel.GetSingle(invoice.QuotationId);
            Customer customer = _customersModel.GetSingle(invoice.CustomerId);

            Dropdown[] quotationDDL = QuotationDDL(invoice.QuotationId);
            ViewData["QuotationDropdown"] = new SelectList(quotationDDL, "val", "name", invoice.QuotationId);

            string fromView = "Yes";

            //if no project, dont show project field
            if(invoice.ProjectId != 0)
            {
                Dropdown[] projectDDL = ProjectDDL(invoice.ProjectId, fromView);
                ViewData["ProjectDropdown"] = new SelectList(projectDDL, "val", "name", invoice.ProjectId);
            }

            Dropdown[] customerDDL = CustomerDDL(invoice.CustomerId);
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", invoice.CustomerId);

            Dropdown[] gstDDL = GSTDDL();
            ViewData["GSTDropdown"] = new SelectList(gstDDL, "val", "name", invoice.GST);

            List<InvoiceItemDescriptionTable> itemDescriptionsTable = new List<InvoiceItemDescriptionTable>();

            int rowId = 1;

            foreach (InvoiceItemDescription itemDescription in invoice.ItemDescriptions)
            {
                itemDescriptionsTable.Add(new InvoiceItemDescriptionTable()
                {
                    RowId = rowId,
                    Description = itemDescription.Description,
                    PercentageOfQuotation = itemDescription.PercentageOfQuotation,
                    PercentageOfQuotationDDL = new SelectList(PercentageOfQuotationDDL(), "val", "name", itemDescription.PercentageOfQuotation),
                    Amount = itemDescription.Amount.ToString("#,##0.00")
                });

                rowId++;
            }

            ViewData["InvoiceItemDescriptionTable"] = itemDescriptionsTable;

            ViewData["QuotationCompany"] = "";
            ViewData["CustomerAddress"] = "";
            ViewData["CustomerTel"] = "";
            ViewData["CustomerFax"] = "";
            ViewData["TotalQuotationAmount"] = "";

            if (quotation != null)
            {
                ViewData["QuotationCompany"] = quotation.CompanyName;

                decimal totalQuotationAmount = 0;

                foreach (QuotationItemDescription itemDescription in quotation.ItemDescriptions)
                {
                    totalQuotationAmount += Convert.ToDecimal(itemDescription.QuotationItemAmount);
                }

                ViewData["TotalQuotationAmount"] = totalQuotationAmount.ToString("#,##0.00");
            }

            if (customer != null)
            {
                ViewData["CustomerAddress"] = customer.Address;
                ViewData["CustomerTel"] = customer.Tel;
                ViewData["CustomerFax"] = customer.Fax;
            }

            ViewData["Invoice"] = invoice;
            ViewData["InvoiceInfoSection"] = "";
            if (Session["UserGroup"].ToString() == "Sales")
            {
                ViewData["InvoiceInfoSection"] = "style=display:none;";

            }
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: GenerateInvoice
        public ActionResult GenerateInvoice(int id)
        {
            Invoice invoice = _invoicesModel.GetSingle(id);
            ViewData["Invoice"] = invoice;

            ViewData["ProjectTitle"] = null;

            if(invoice.ProjectId != 0)
            {
                Project project = _projectsModel.GetAllSingle(invoice.ProjectId);

                if (project != null)
                {
                    ViewData["ProjectTitle"] = project.ProjectTitle;
                }
            }

            string[] listOfCompanies = _settingsModel.GetCodeValue("LIST_OF_COMPANY").Split(';');

            ViewData["CompanyName"] = "MY RENOBUDDY (SG) PTE LTD";
            ViewData["CompanyInfo"] = "Office / Factory: 1007 Eunos Avenue 7 #01-25 S(409578)|Email: Enquiries@myrenobuddy.com.sg|Contact / Fax: +65 6741 3503 (O) +6741 3060 (F)|Co. Reg. No: 201534632H";

            foreach (string company in listOfCompanies)
            {
                string[] comp = company.Split('|');

                if (comp[0] == invoice.Company)
                {
                    ViewData["CompanyName"] = comp[0];
                    ViewData["CompanyInfo"] = comp[2].Replace("\r\n", "|");
                    break;
                }
            }

            string footer = Server.MapPath("~/Views/Invoice/InvoicePDFFooter.html");
            string customSwitch = string.Format("--footer-html \"{0}\" " + "--footer-spacing \"0\" ", footer);

            //return View();
            return new Rotativa.ViewAsPdf("GenerateInvoice")
            {
                FileName = invoice.InvoiceID + "-invoice-" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf",
                PageSize = Rotativa.Options.Size.A4,
                CustomSwitches = customSwitch
            };
        }

        //GET: GetQuotationDetails
        public string GetQuotationDetails(int qid)
        {
            Quotation quotation = _quotationsModel.GetSingle(qid);

            InvoiceQuotation invoiceQuotation = new InvoiceQuotation();
            invoiceQuotation.QuotationDetail = new InvoiceQuotationDetail();

            if (quotation != null)
            {
                if (quotation.Status == "Confirmed")
                {
                    decimal totalQuotationAmount = 0;

                    foreach (QuotationItemDescription quotationItem in quotation.ItemDescriptions)
                    {
                        totalQuotationAmount += (decimal)quotationItem.QuotationItemAmount;
                    }

                    invoiceQuotation.Result = true;
                    invoiceQuotation.QuotationDetail.Company = quotation.CompanyName;
                    invoiceQuotation.QuotationDetail.CustomerId = Convert.ToInt32(quotation.CustomerId);
                    invoiceQuotation.QuotationDetail.GST = quotation.HasGST;
                    invoiceQuotation.QuotationDetail.TotalQuotationAmount = totalQuotationAmount.ToString("#,##0.00");
                }
                else
                {
                    invoiceQuotation.Result = false;
                    invoiceQuotation.ErrorMessage = "Quotation is not confirmed!";
                }
            }
            else
            {
                invoiceQuotation.Result = false;
                invoiceQuotation.ErrorMessage = "Quotation record not found!";
            }

            return JsonConvert.SerializeObject(invoiceQuotation);
        }

        //GET: GetProjectList
        public string GetProjectList(int qid)
        {
            IList <Project> project = _projectsModel.GetAllQuotationRelatedProjectItem(qid);

            ProjectTitle projectTitle = new ProjectTitle();
            
            if (project.Count > 0)
            {
                //projectTitle.ProjectTitleList = new List<ProjectTitleList>();

                projectTitle.Result = true;

                string options = String.Join("", project.Select(e => String.Format("<option value=\"{0}\">{1}</option>", e.ID, e.IsDeleted == "Y" ? e.ProjectID + " - " + e.ProjectTitle + " (Deleted)" : e.ProjectID + " - " + e.ProjectTitle)).ToList());

                projectTitle.Options = options;
                
            }
            else
            {
                projectTitle.Result = false;
                projectTitle.ErrorMessage = "Project record not found!";
            }

            return JsonConvert.SerializeObject(projectTitle);
        }

        //GET: GetCustomerDetails
        public string GetCustomerDetails(int cid)
        {
            Customer customer = _customersModel.GetSingle(cid);

            InvoiceCustomer invoiceCustomer = new InvoiceCustomer();
            invoiceCustomer.CustomerDetail = new InvoiceCustomerDetail();

            if (customer != null)
            {
                if (customer.Status != "Suspended")
                {
                    invoiceCustomer.Result = true;
                    invoiceCustomer.CustomerDetail.ContactPerson = customer.ContactPerson;
                    invoiceCustomer.CustomerDetail.Address = customer.Address;
                    invoiceCustomer.CustomerDetail.Tel = customer.Tel;
                    invoiceCustomer.CustomerDetail.Fax = customer.Fax;
                }
                else
                {
                    invoiceCustomer.Result = false;
                    invoiceCustomer.ErrorMessage = "Customer has been suspended!";
                }
            }
            else
            {
                invoiceCustomer.Result = false;
                invoiceCustomer.ErrorMessage = "Customer record not found!";
            }

            return JsonConvert.SerializeObject(invoiceCustomer);
        }

        //GET: AddItemDescription
        public ActionResult AddItemDescription(int rowId, int rowCount, int qid = 0)
        {
            Dropdown[] percentageOfQuotationDDL = PercentageOfQuotationDDL();
            ViewData["PercentageOfQuotationDropdown"] = new SelectList(percentageOfQuotationDDL, "val", "name", "5");

            ViewData["Amount"] = "";

            if (qid > 0)
            {
                Quotation quotation = _quotationsModel.GetSingle(qid);

                if (quotation != null)
                {
                    decimal totalQuotationAmount = 0;

                    foreach (QuotationItemDescription itemDescription in quotation.ItemDescriptions)
                    {
                        totalQuotationAmount += (decimal)itemDescription.QuotationItemAmount;
                    }

                    totalQuotationAmount = totalQuotationAmount * 5 / 100;

                    ViewData["Amount"] = totalQuotationAmount.ToString("#,##0.00");
                }
            }

            ViewData["RowId"] = ++rowId;
            ViewData["RowCount"] = ++rowCount;
            return View();
        }

        //GET: CalculateItemAmount
        public string CalculateItemAmount(int percent, int qid = 0)
        {
            string amount = "";

            if (qid > 0)
            {
                Quotation quotation = _quotationsModel.GetSingle(qid);

                if (quotation != null)
                {
                    decimal totalQuotationAmount = 0;

                    foreach (QuotationItemDescription itemDescription in quotation.ItemDescriptions)
                    {
                        totalQuotationAmount += (decimal)itemDescription.QuotationItemAmount;
                    }

                    totalQuotationAmount = totalQuotationAmount * percent / 100;

                    amount = totalQuotationAmount.ToString("#,##0.00");
                }
            }

            return amount;
        }

        //GET: CalculateGST
        public string CalculateGST(decimal subtotal)
        {
            string gstValue = _settingsModel.GetCodeValue("GST");

            decimal gstAmount = subtotal * Convert.ToDecimal(gstValue) / 100;

            return FormValidationHelper.AmountFormatter(gstAmount, 2);
        }

        //GET: ValidateAmount
        public string ValidateAmount(string amount)
        {
            bool result = FormValidationHelper.AmountFormat(amount);

            string res = "";

            if (result)
            {
                string newAmount = FormValidationHelper.AmountFormatter(Convert.ToDecimal(amount), 2);

                res = "{\"Result\":true,\"NewAmount\":\"" + newAmount + "\"}";
            }
            else
            {
                res = "{\"Result\":false,\"ErrorMessage\":\"'" + amount + "' is not a valid amount!\"}";
            }

            return res;
        }

        //Quotation Dropdown
        public Dropdown[] QuotationDDL(int qid = 0)
        {
            IList<Quotation> quotations = _quotationsModel.GetConfirmedQuotations();

            if (Session["UserGroup"].ToString() == "Sales")
            {
                List<int> salesUserId = new List<int>();

                salesUserId.Add(Convert.ToInt32(Session["MainID"]));

                List<User> uplineUsers = _usersModel.GetAllSameUplineID(Convert.ToInt32(Session["MainID"]));

                foreach (User user in uplineUsers)
                {
                    salesUserId.Add(user.ID);
                }

                quotations = quotations.Where(e => salesUserId.Contains(Convert.ToInt32(e.SalesPersonId))).ToList();
            }

            Quotation selectedQuotation = _quotationsModel.GetSingle(qid);

            if (qid > 0)
            {
                quotations = quotations.Where(e => e.ID != selectedQuotation.ID).ToList();
            }

            int size = quotations.Count + 1;

            if (selectedQuotation != null)
            {
                size++;
            }

            Dropdown[] ddl = new Dropdown[size];

            int count = 1;

            if (selectedQuotation != null)
            {
                if (!string.IsNullOrEmpty(selectedQuotation.Customer.CompanyName))
                {
                    ddl[count] = new Dropdown { name = selectedQuotation.QuotationID + " - " + selectedQuotation.CompanyName, val = selectedQuotation.ID.ToString() };
                }
                else
                {
                    ddl[count] = new Dropdown { name = selectedQuotation.QuotationID + " - " + selectedQuotation.ContactPerson, val = selectedQuotation.ID.ToString() };
                }

                count++;
            }

            foreach (Quotation quotation in quotations)
            {
                if (!string.IsNullOrEmpty(quotation.Customer.CompanyName))
                {
                    ddl[count] = new Dropdown { name = quotation.QuotationID + " - " + quotation.CompanyName, val = quotation.ID.ToString() };
                }
                else
                {
                    ddl[count] = new Dropdown { name = quotation.QuotationID + " - " + quotation.ContactPerson, val = quotation.ID.ToString() };
                }

                count++;
            }

            return ddl;
        }

        //Project Dropdown
        public Dropdown[] ProjectDDL(int pid = 0, string fromView = "No")
        {

            Dropdown[] ddl = new Dropdown[0];

            if(fromView == "Edit")
            {

                //pid become quotation id to loop all the project related quotation id
                IList<Project> GetAllQuotationRelatedProjectItem = _projectsModel.GetAllProjectRelatedQuotationID(pid);

                if(GetAllQuotationRelatedProjectItem.Count > 0)
                {
                    ddl = new Dropdown[GetAllQuotationRelatedProjectItem.Count + 1];

                    int count = 1;
                    foreach(Project projects in GetAllQuotationRelatedProjectItem)
                    {

                        string value = Convert.ToString(projects.ID);

                        if(projects.IsDeleted == "Y")
                        {
                            ddl[count] = new Dropdown { name = projects.ProjectID + " - " + projects.ProjectTitle + " (Deleted)", val = value };
                        }
                        else
                        {
                            ddl[count] = new Dropdown { name = projects.ProjectID + " - " + projects.ProjectTitle, val = value };
                        }

                        count++;
                    }
                }
            }
            else if(fromView == "Yes")
            {

                Project getProject = _projectsModel.GetAllSingle(pid);

                ddl = new Dropdown[1];

                if (getProject.IsDeleted == "Y")
                {
                    ddl[0] = new Dropdown { name = getProject.ProjectID + " - " + getProject.ProjectTitle + " (Deleted)", val = getProject.ProjectID };
                }
                else
                {
                    ddl[0] = new Dropdown { name = getProject.ProjectID + " - " + getProject.ProjectTitle, val = getProject.ProjectID };
                }

            }
            return ddl;
        }

        //Customer Dropdown
        public Dropdown[] CustomerDDL(int cid = 0)
        {
            Dictionary<int, string> filteredCustomers = new Dictionary<int, string>();
            int userId = Convert.ToInt32(Session["MainID"]);

            IList<Customer> customers = _customersModel.GetActiveCustomers();
            Customer selectedCustomer = _customersModel.GetSingle(cid);
            if (selectedCustomer != null)
            {
                customers = customers.Where(e => e.ID != selectedCustomer.ID).ToList();

                if (!string.IsNullOrEmpty(selectedCustomer.CompanyName))
                {
                    filteredCustomers.Add(selectedCustomer.ID, selectedCustomer.CustomerID + " - " + selectedCustomer.CompanyName);
                }
                else
                {
                    filteredCustomers.Add(selectedCustomer.ID, selectedCustomer.CustomerID + " - " + selectedCustomer.ContactPerson);
                }
            }

            foreach (Customer customer in customers)
            {
                bool flag = false;

                if (Session["UserGroup"].ToString() == "Super Admin")
                {
                    flag = true;
                }
                else
                {
                    if ((customer.UplineId == userId && customer.UplineUserType == "User") || customer.AssignToId == userId)
                    {
                        flag = true;
                    }
                }

                if (flag)
                {
                    if (!string.IsNullOrEmpty(customer.CompanyName))
                    {
                        filteredCustomers.Add(customer.ID, customer.CustomerID + " - " + customer.CompanyName);
                    }
                    else
                    {
                        filteredCustomers.Add(customer.ID, customer.CustomerID + " - " + customer.ContactPerson);
                    }
                }
            }

            Dropdown[] ddl = new Dropdown[filteredCustomers.Count + 1];

            int count = 1;

            foreach (KeyValuePair<int, string> customer in filteredCustomers)
            {
                ddl[count] = new Dropdown { name = customer.Value, val = customer.Key.ToString() };

                count++;
            }

            return ddl;
        }

        //GST Dropdown
        public Dropdown[] GSTDDL()
        {
            Dropdown[] ddl = new Dropdown[2];
            ddl[0] = new Dropdown { name = "Yes", val = "Yes" };
            ddl[1] = new Dropdown { name = "No", val = "No" };
            return ddl;
        }

        //Percentage of Quotation Dropdown
        public Dropdown[] PercentageOfQuotationDDL()
        {
            Dropdown[] ddl = new Dropdown[21];

            for (int percent = 0, count = 0; percent <= 100; percent += 5, count++)
            {
                if (percent == 0)
                {
                    ddl[count] = new Dropdown { name = "Value", val = "" };
                }
                else
                {
                    ddl[count] = new Dropdown { name = percent + "%", val = percent.ToString() };
                }
            }

            return ddl;
        }
    }
}