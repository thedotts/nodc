﻿using Google.Apis.Calendar.v3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.Threading;
using System.Threading.Tasks;
using NODC_OMS.Models;
using DataAccess.POCO;
using System.Configuration;
using System.IO;
using DataAccess;
using System.Text;

namespace NODC_OMS.Controllers
{
    [HandleError]
    [RedirecttingActionAdmin]
    public class CalendarController : ControllerBase
    {
        private IUserRepository _usersModel;

        public CalendarController()
            : this(new UserRepository())
        {

        }

        public CalendarController(IUserRepository usersModel)
        {
            _usersModel = usersModel;
        }

        static string[] Scopes = { CalendarService.Scope.CalendarReadonly };
        static string ApplicationName = "Google Calendar";

        // GET: Index
        public ActionResult Index()
        {

            if (Session["User"] != null)
            {
                Session.Remove("User");
            }

            return RedirectToAction("Options");
        }

        //GET: Options
        public ActionResult Options()
        {
            string User = "";
            if (Session["User"] != null)
            {
                User = Session["User"].ToString();
            }

            Dropdown[] userDDL = UserDDL();
            ViewData["UserDropdown"] = new SelectList(userDDL, "val", "name", User);

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            return View();
        }

        // POST: Options
        [HttpPost]
        public ActionResult Options(FormCollection form)
        {

            string username = null;

            if (string.IsNullOrEmpty(form["Username"]))
            {
                ModelState.AddModelError("Username", "Username is required!");
            }
            else
            {
                username = form["Username"].ToString();
            }

            if (ModelState.IsValid)
            {
                Session.Add("User", form["Username"].ToString());
                return RedirectToAction("ViewCalendar/" + username);
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            Dropdown[] userDDL = UserDDL();
            ViewData["UserDropdown"] = new SelectList(userDDL, "val", "name", form["Username"]);

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            return View();
        }

        // GET: ViewCalendar
        public ActionResult ViewCalendar(int id)
        {
            User user = _usersModel.GetSingle(id);

            List<CalendarDetailTable> calendarDetailTable = new List<CalendarDetailTable>();

            if (user != null)
            {

                string email = user.Email;

                UserCredential credential;

                string path = Path.Combine(Server.MapPath("~"), "client_secret.json");

                ////check path
                //string logPath = Server.MapPath(ConfigurationManager.AppSettings["ErrorLogPath"].ToString() + "GoogleCalendar_" + DateTime.Now.ToString("yyyyMMdd") + ".log");

                //StringBuilder message = new StringBuilder();
                //message.AppendLine("Start Dianose");
                //System.IO.File.AppendAllText(logPath, message.ToString());

                using (var stream =
                    new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    string credPath = Server.MapPath("~/GoogleCalendar/");
                    credPath = Path.Combine(credPath, ".credentials/Google-api-calendar-token.json");

                    //message.AppendLine("Create path");
                    //System.IO.File.AppendAllText(logPath, message.ToString());

                    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        GoogleClientSecrets.Load(stream).Secrets,
                        Scopes,
                        "user",
                        CancellationToken.None,
                        new FileDataStore(credPath, true)).Result;
                }

                //message.AppendLine("Using FileStream");
                //System.IO.File.AppendAllText(logPath, message.ToString());

                // Create Google Calendar API service.
                var service = new CalendarService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = ApplicationName,
                });

                //message.AppendLine("Create Google Calendar API Services");
                //System.IO.File.AppendAllText(logPath, message.ToString());

                // Define parameters of request.
                EventsResource.ListRequest request = service.Events.List(email);
                request.TimeMin = DateTime.Now;
                request.ShowDeleted = false;
                request.SingleEvents = true;
                request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

                //message.AppendLine("Request param");
                //System.IO.File.AppendAllText(logPath, message.ToString());

                try
                {
                    // List events.
                    Events events = request.Execute();

                    //message.AppendLine("Try before if");
                    //message.AppendLine(events.Items.Count.ToString());
                    //System.IO.File.AppendAllText(logPath, message.ToString());

                    if (events.Items != null && events.Items.Count > 0)
                    {
                        foreach (var eventItem in events.Items)
                        {
                            List<CalendarDetailSmallTable> CalendarDetailSmallTable = new List<CalendarDetailSmallTable>();

                            string mainDate = eventItem.Start.DateTime.ToString();
                            string start = eventItem.Start.DateTime.ToString();
                            string end = eventItem.End.DateTime.ToString();
                            string create = eventItem.Created.Value.ToString();
                            string status = eventItem.Status;
                            double durationHour = 0;
                            string FormattedHour = null;

                            if (String.IsNullOrEmpty(mainDate))
                            {
                                mainDate = eventItem.Start.Date;
                            }

                            if (!String.IsNullOrEmpty(start) && !String.IsNullOrEmpty(end))
                            {
                                durationHour = (Convert.ToDateTime(end) - Convert.ToDateTime(start)).TotalHours;

                                DateTime startDate = Convert.ToDateTime(start);
                                DateTime endDate = Convert.ToDateTime(end);

                                //New Test
                                TimeSpan duration = endDate.Subtract(startDate);

                                if (duration.TotalDays > 0)
                                {
                                    if (duration.Days > 0)
                                    {
                                        if(duration.Days <= 1)
                                        {
                                            FormattedHour += duration.Days + " Day ";
                                        }
                                        else
                                        {
                                            FormattedHour += duration.Days + " Days ";
                                        }
                                        
                                    }

                                    if (duration.Hours <= 1)
                                    {
                                        FormattedHour += duration.Hours + " Hour ";
                                    }
                                    else
                                    {
                                        FormattedHour += duration.Hours + " Hours ";
                                    }

                                    //if (duration.Hours > 0)
                                    //{
                                    //    FormattedHour += duration.Hours + " Hours ";
                                    //}

                                    if (duration.Minutes > 0)
                                    {
                                        if (duration.Minutes <= 1)
                                        {
                                            FormattedHour += duration.Minutes + " Minute";
                                        }
                                        else
                                        {
                                            FormattedHour += duration.Minutes + " Minutes";
                                        }
                                        
                                    }

                                    //FormattedHour = duration.Days + " Days " + duration.Hours + " Hours " + duration.Minutes + " Minutes";
                                }
                                else
                                {
                                    FormattedHour = "-";
                                }

                            }
                            else
                            {
                                FormattedHour = "-";
                            }

                            if (calendarDetailTable.Count == 0)
                            {

                                CalendarDetailSmallTable.Add(new CalendarDetailSmallTable()
                                {
                                    summary = eventItem.Summary,
                                    start = start,
                                    end = end,
                                    durationHour = FormattedHour
                                });

                                calendarDetailTable.Add(new CalendarDetailTable()
                                {
                                    summary = eventItem.Summary,
                                    mainDate = Convert.ToDateTime(mainDate),
                                    start = start,
                                    end = end,
                                    durationHour = FormattedHour,
                                    CalendarDetailSmallTable = CalendarDetailSmallTable
                                });
                            }
                            else
                            {
                                if (calendarDetailTable.Where(e => e.mainDate.Date == Convert.ToDateTime(mainDate).Date).FirstOrDefault() != null)
                                {
                                    foreach (CalendarDetailTable calendar in calendarDetailTable)
                                    {
                                        if (calendar.mainDate.Date == Convert.ToDateTime(mainDate).Date)
                                        {
                                            calendar.CalendarDetailSmallTable.Add(new CalendarDetailSmallTable()
                                            {
                                                summary = eventItem.Summary,
                                                start = start,
                                                end = end,
                                                durationHour = FormattedHour
                                            });
                                        }
                                        break;
                                    }
                                }
                                else
                                {
                                    CalendarDetailSmallTable.Add(new CalendarDetailSmallTable()
                                    {
                                        summary = eventItem.Summary,
                                        start = start,
                                        end = end,
                                        durationHour = FormattedHour
                                    });

                                    calendarDetailTable.Add(new CalendarDetailTable()
                                    {
                                        summary = eventItem.Summary,
                                        mainDate = Convert.ToDateTime(mainDate),
                                        start = start,
                                        end = end,
                                        durationHour = FormattedHour,
                                        CalendarDetailSmallTable = CalendarDetailSmallTable
                                    });
                                }
                            }
                        }
                    }
                    ViewData["CalendarList"] = calendarDetailTable;
                    ViewData["UserDetail"] = user.UserID + " - " + user.Name;
                    ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

                    return View();
                }
                catch (Exception ex)
                {
                    Session.Add("Result", "danger|User [ " + user.UserID + " - " + user.Name + " ] email address is not valid to view calendar!");
                    return RedirectToAction("Options", "Calendar");
                }

            }
            else
            {
                Session.Add("Result", "danger|User not found!");
                return RedirectToAction("Options","Calendar");
            }
        }

        public ActionResult authorize()
        {
            return View();
        }

        public Dropdown[] UserDDL()
        {

            IList<User> getAllUser = _usersModel.GetAll().Where(e => e.Email != null).ToList();

            Dropdown[] ddl = new Dropdown[getAllUser.Count + 1];
            int count = 1;

            foreach (User _getAllUser in getAllUser)
            {
                ddl[count] = new Dropdown { name = _getAllUser.UserID + " - " + _getAllUser.Name, val = _getAllUser.ID.ToString() };
                count++;
            }

            return ddl;
        }
    }
}