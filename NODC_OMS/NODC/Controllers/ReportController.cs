﻿using DataAccess.POCO;
using DataAccess.Report;
using NODC_OMS.Controllers;
using NODC_OMS.Models;
using NODC_OMS.Models.Report;
using OfficeOpenXml;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NODC_OMS.Controllers
{

    [HandleError]
    [RedirecttingActionAdmin]
    public class ReportController : ControllerBase
    {

        private IReportRepository _reportsModel;
        private IProjectRepository _projectsModel;
        private IProjectProjectExpensesRepository _projectExpensesModel;
        private ICustomerRepository _customersModel;
        private IPayoutRepository _payoutsModel;
        private IInvoiceRepository _invoicesModel;

        public ReportController()
            : this(new ReportRepository(), new ProjectRepository(), new ProjectProjectExpensesRepository(), new CustomerRepository(), new PayoutRepository(), new InvoiceRepository())
        {

        }

        public ReportController(IReportRepository reportsModel, IProjectRepository projectsModel, IProjectProjectExpensesRepository projectExpensesModel, ICustomerRepository customersModel, IPayoutRepository payoutsModel, IInvoiceRepository invoicesModel)
        {
            _reportsModel = reportsModel;
            _projectsModel = projectsModel;
            _projectExpensesModel = projectExpensesModel;
            _customersModel = customersModel;
            _payoutsModel = payoutsModel;
            _invoicesModel = invoicesModel;
        }

        // GET: Report
        public ActionResult Index()
        {
            if (Session["ReportType"] != null)
            {
                Session.Remove("ReportType");
            }

            if (Session["ReportPeriod"] != null)
            {
                Session.Remove("ReportPeriod");
            }

            if (Session["PageSize"] != null)
            {
                Session.Remove("PageSize");
            }

            return RedirectToAction("Options");
        }

        //GET: Options
        public ActionResult Options()
        {
            string reportType = "";
            if (Session["ReportType"] != null)
            {
                reportType = Session["ReportType"].ToString();
            }

            string reportPeriod = DateTime.Now.ToString("M/yyyy");
            if (Session["ReportPeriod"] != null)
            {
                reportPeriod = Session["ReportPeriod"].ToString();
            }
            string reportGST = "";
            if (Session["ReportGST"] != null)
            {
                reportGST = Session["ReportGST"].ToString();
            }

            string reportDateRangeFrom = DateTime.Now.ToString("dd/MM/yyyy");
            if (Session["ReportDateRangeFrom"] != null)
            {
                reportDateRangeFrom = Session["ReportDateRangeFrom"].ToString();
            }

            string reportDateRangeTo = DateTime.Now.ToString("dd/MM/yyyy");
            if (Session["ReportDateRangeTo"] != null)
            {
                reportDateRangeTo = Session["ReportDateRangeTo"].ToString();
            }

            string reportCustomerAgency = "";
            if (Session["ReportCustomerAgency"] != null)
            {
                reportCustomerAgency = Session["ReportCustomerAgency"].ToString();
            }

            string reportFinancialType = "";
            if (Session["ReportFinancialType"] != null)
            {
                reportFinancialType = Session["ReportFinancialType"].ToString();
            }

            string reportProjectCode = "";
            if (Session["ReportProjectCode"] != null)
            {
                reportProjectCode = Session["ReportProjectCode"].ToString();
            }

            Dropdown[] reportProjectCodeDDL = ReportProjectCodeDDL();
            ViewData["ReportProjectCodeDropdown"] = new SelectList(reportProjectCodeDDL, "val", "name", reportProjectCode);

            Dropdown[] reportFinancialTypeDDL = ReportFinancialTypeDDL();
            ViewData["ReportFinancialDropdown"] = new SelectList(reportFinancialTypeDDL, "val", "name", reportFinancialType);

            Dropdown[] reportGSTDDL = ReportGSTDDL();
            ViewData["ReportGSTDropdown"] = new SelectList(reportGSTDDL, "val", "name", reportGST);

            Dropdown[] reportTypeDDL = ReportTypeDDL();
            ViewData["ReportTypeDropdown"] = new SelectList(reportTypeDDL, "val", "name", reportType);

            Dropdown[] reportPeriodDDL = ReportPeriodDDL();
            ViewData["ReportPeriodDropdown"] = new SelectList(reportPeriodDDL, "val", "name", reportPeriod);

            //Commission Report Dropdown (Agency
            Dropdown[] reportCustomerAgencyDDL = ReportCustomerAgencyDDL();
            ViewData["ReportCustomerAgencyDropdown"] = new SelectList(reportCustomerAgencyDDL, "val", "name", reportCustomerAgency);

            ViewData["ReportDateRangeFrom"] = reportDateRangeFrom;
            ViewData["ReportDateRangeTo"] = reportDateRangeTo;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString() + " :: Reports";
            return View();
        }

        //POST: Options
        [HttpPost]
        public ActionResult Options(FormCollection form)
        {
            if (string.IsNullOrEmpty(form["ReportType"]))
            {
                ModelState.AddModelError("ReportType", "Report Type is required!");
            }
            else
            {

                if (form["ReportType"].ToString() == "ExpenseReports")
                {

                    if(string.IsNullOrEmpty(form["ReportDateRangeFrom"]))
                    {
                        ModelState.AddModelError("ReportDateRangeFrom", "Date Range From is required!");
                    }

                    if (string.IsNullOrEmpty(form["ReportDateRangeTo"]))
                    {
                        ModelState.AddModelError("ReportDateRangeTo", "Date Range To is required!");
                    }

                    //If both DateRange are not empty, check daterangeTo and daterangeFrom
                    if (!string.IsNullOrEmpty(form["ReportDateRangeFrom"]) && !string.IsNullOrEmpty(form["ReportDateRangeTo"]))
                    {
                        DateTime ReportDateRangeFrom = Convert.ToDateTime(form["ReportDateRangeFrom"].ToString());
                        DateTime ReportDateRangeTo = Convert.ToDateTime(form["ReportDateRangeTo"].ToString());

                        if (ReportDateRangeFrom > ReportDateRangeTo)
                        {
                            ModelState.AddModelError("ReportDateRangeTo", "Date Range To cannot earlier than Date Range From!");
                        }
                    }

                    if (string.IsNullOrEmpty(form["ReportGST"]))
                    {
                        ModelState.AddModelError("ReportGST", "GST is required!");
                    }
                }

                if (form["ReportType"].ToString() == "CommissionReports")
                {
                    if (string.IsNullOrEmpty(form["ReportPeriod"]))
                    {
                        ModelState.AddModelError("ReportPeriod", "Report Period is required!");
                    }
                }
            }

            if (ModelState.IsValid)
            {
                Session.Add("ReportType", form["ReportType"].ToString());
                Session.Add("ReportDateRangeFrom", form["ReportDateRangeFrom"].ToString());
                Session.Add("ReportDateRangeTo", form["ReportDateRangeTo"].ToString());
                Session.Add("ReportGST", form["ReportGST"].ToString());
                Session.Add("ReportCustomerAgency", form["ReportCustomerAgency"].ToString());
                Session.Add("ReportPeriod", form["ReportPeriod"].ToString());
                Session.Add("ReportFinancialType", form["ReportFinancialType"].ToString());
                Session.Add("ReportProjectCode", form["ReportProjectCode"].ToString());

                return RedirectToAction(form["ReportType"].ToString());
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            Dropdown[] reportProjectCodeDDL = ReportProjectCodeDDL();
            ViewData["ReportProjectCodeDropdown"] = new SelectList(reportProjectCodeDDL, "val", "name", form["ReportProjectCode"]);

            Dropdown[] reportFinancialTypeDDL = ReportFinancialTypeDDL();
            ViewData["ReportFinancialDropdown"] = new SelectList(reportFinancialTypeDDL, "val", "name", form["ReportFinancialType"]);

            Dropdown[] reportTypeDDL = ReportTypeDDL();
            ViewData["ReportTypeDropdown"] = new SelectList(reportTypeDDL, "val", "name", form["ReportType"]);

            Dropdown[] reportPeriodDDL = ReportPeriodDDL();
            ViewData["ReportPeriodDropdown"] = new SelectList(reportTypeDDL, "val", "name", form["ReportPeriod"]);

            Dropdown[] reportGSTDDL = ReportGSTDDL();
            ViewData["ReportGSTDropdown"] = new SelectList(reportGSTDDL, "val", "name", form["ReportGST"]);

            ViewData["ReportDateRangeFrom"] = form["ReportDateRangeFrom"];
            ViewData["ReportDateRangeTo"] = form["ReportDateRangeTo"];

            //Commission Report Dropdown (Agency
            Dropdown[] reportCustomerAgencyDDL = ReportCustomerAgencyDDL();
            ViewData["ReportCustomerAgencyDropdown"] = new SelectList(reportCustomerAgencyDDL, "val", "name", form["ReportCustomerAgency"]);

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString() + " :: Reports";
            return View();
        }

        //GET: SetPeriodSession
        public ActionResult SetPeriodSession(string reportType, string reportPeriod, string reportAgency, string reportDateRangeFrom, string reportDateRangeTo, string reportGST, string reportProjectType, string reportProjectCode)
        {
            Session.Add("ReportPeriod", reportPeriod);
            Session.Add("ReportCustomerAgency", reportAgency);

            Session.Add("ReportDateRangeFrom", reportDateRangeFrom);
            Session.Add("ReportDateRangeTo", reportDateRangeTo);
            Session.Add("ReportGST", reportGST);

            Session.Add("ReportFinancialType", reportProjectType);
            Session.Add("ReportProjectCode", reportProjectCode);

            switch (reportType)
            {
                case "CommissionReports": return RedirectToAction("DownloadCommissionReports");
                case "ExpenseReports": return RedirectToAction("DownloadExpenseReports");
                case "ProjectFinancialReports": return RedirectToAction("DownloadProjectFinancialReports");
                case "PayoutReports": return RedirectToAction("DownloadPayoutReports");
                case "RetainedAmountReports": return RedirectToAction("DownloadRetainedAmountReports");
                default: Session.Add("Result", "danger|Report Type does not exist!"); return RedirectToAction("Options");
            }
        }

        //GET: ExpensesReport
        public ActionResult ExpenseReports(int page = 1)
        {
            int pageSize = 20;
            if (Session["PageSize"] != null)
            {
                pageSize = Convert.ToInt32(Session["PageSize"]);
            }
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            Dropdown[] pageSizeDDL = PageSizeDDL();
            ViewData["PageSizeDropdown"] = new SelectList(pageSizeDDL, "val", "name", pageSize);

            DateTime DateRangeFrom = Convert.ToDateTime(Session["ReportDateRangeFrom"].ToString());
            DateTime DateRangeTo = Convert.ToDateTime(Session["ReportDateRangeTo"].ToString());
            string GST = Session["ReportGST"].ToString();

            //changes here
            IList<Project> projects = _projectsModel.GetAll().Where(e => e.Status == "Completed").ToList();

            List <ExpenseReport> expenseReports = new List<ExpenseReport>();

            foreach(Project _projects in projects)
            {

                IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetPersonalAllReport(_projects.ID, DateRangeFrom, DateRangeTo, GST);

                foreach(ProjectProjectExpenses _projectExpenses in projectExpenses)
                {
                    ExpenseReport report = new ExpenseReport();

                    report.ProjectName = _projects.ProjectTitle;
                    report.ProjectID = _projects.ProjectID;
                    report.SupplierName = _projectExpenses.SupplierName;
                    report.Description = _projectExpenses.Description;
                    report.Cost = _projectExpenses.Cost.ToString("#,##0.00");
                    report.GST = _projectExpenses.GST;
                    report.Status = _projectExpenses.Status;
                    expenseReports.Add(report);
                }
 
            }

            IPagedList<ExpenseReport> reports = _reportsModel.GetExpenseReportPaged(expenseReports, page, pageSize);
            ViewData["ExpenseReport"] = reports;

            ViewData["DateRangeFrom"] = DateRangeFrom.ToString("dd/MM/yyyy");
            ViewData["DateRangeTo"] = DateRangeTo.ToString("dd/MM/yyyy");

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString() + " :: Reports - Expense Reports";
            return View();
        }

        //GET: DownloadExpenseReports
        public void DownloadExpenseReports()
        {
            //string[] period = Session["ReportPeriod"].ToString().Split('/');

            //DateTime startDate = new DateTime(Convert.ToInt32(period[1]), Convert.ToInt32(period[0]), 1, 0, 0, 0, 0);
            //int lastDay = DateTime.DaysInMonth(Convert.ToInt32(period[1]), Convert.ToInt32(period[0]));
            //DateTime endDate = new DateTime(Convert.ToInt32(period[1]), Convert.ToInt32(period[0]), lastDay, 23, 59, 59, 999);

            DateTime DateRangeFrom = Convert.ToDateTime(Session["ReportDateRangeFrom"].ToString());
            DateTime DateRangeTo = Convert.ToDateTime(Session["ReportDateRangeTo"].ToString());
            string GST = Session["ReportGST"].ToString();

            //changes here
            IList<Project> projects = _projectsModel.GetAll().Where(e => e.Status == "Completed").ToList();

            List<ExpenseReport> expenseReports = new List<ExpenseReport>();

            foreach (Project _projects in projects)
            {

                IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetPersonalAllReport(_projects.ID, DateRangeFrom, DateRangeTo, GST);

                foreach (ProjectProjectExpenses _projectExpenses in projectExpenses)
                {
                    ExpenseReport report = new ExpenseReport();

                    report.ProjectName = _projects.ProjectTitle;
                    report.ProjectID = _projects.ProjectID;
                    report.SupplierName = _projectExpenses.SupplierName;
                    report.Description = _projectExpenses.Description;
                    report.Cost = _projectExpenses.Cost.ToString("#,##0.00");
                    report.GST = _projectExpenses.GST;
                    report.Status = _projectExpenses.Status;
                    expenseReports.Add(report);
                }

            }

            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create Worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Expense Reports");

                //Set first row name
                ws.Cells[1, 1].Style.Font.Bold = true;
                ws.Cells[1, 1].Value = "Project Name";
                ws.Cells[1, 2].Style.Font.Bold = true;
                ws.Cells[1, 2].Value = "Project ID";
                ws.Cells[1, 3].Style.Font.Bold = true;
                ws.Cells[1, 3].Value = "Supplier Name";
                ws.Cells[1, 4].Style.Font.Bold = true;
                ws.Cells[1, 4].Value = "Description";
                ws.Cells[1, 5].Style.Font.Bold = true;
                ws.Cells[1, 5].Value = "Cost (before GST)";
                ws.Cells[1, 6].Style.Font.Bold = true;
                ws.Cells[1, 6].Value = "GST";
                ws.Cells[1, 7].Style.Font.Bold = true;
                ws.Cells[1, 7].Value = "Status";

                int rowCount = 2;

                foreach (ExpenseReport report in expenseReports)
                {
                    //Dump Data
                    ws.Cells[rowCount, 1].Value = report.ProjectName;
                    ws.Cells[rowCount, 2].Value = report.ProjectID;
                    ws.Cells[rowCount, 3].Value = report.SupplierName;
                    ws.Cells[rowCount, 4].Value = report.Description;
                    ws.Cells[rowCount, 5].Value = Convert.ToDecimal(report.Cost).ToString("#,##0.00");
                    ws.Cells[rowCount, 6].Value = report.GST;
                    ws.Cells[rowCount, 7].Value = report.Status;
                    rowCount++;
                }

                ws.Cells[ws.Dimension.Address].AutoFitColumns();

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=expense-reports-" + DateRangeFrom.ToString("yyyyMMdd") + "-" + DateRangeTo.ToString("yyyyMMdd") + ".xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }

        //GET: CommissionReports
        public ActionResult CommissionReports(int page = 1)
        {
            int pageSize = 20;
            if (Session["PageSize"] != null)
            {
                pageSize = Convert.ToInt32(Session["PageSize"]);
            }
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            Dropdown[] pageSizeDDL = PageSizeDDL();
            ViewData["PageSizeDropdown"] = new SelectList(pageSizeDDL, "val", "name", pageSize);

            string agency = Session["ReportCustomerAgency"].ToString();

            string[] period = Session["ReportPeriod"].ToString().Split('/');

            DateTime startDate = new DateTime(Convert.ToInt32(period[1]), Convert.ToInt32(period[0]), 1, 0, 0, 0, 0);
            int lastDay = DateTime.DaysInMonth(Convert.ToInt32(period[1]), Convert.ToInt32(period[0]));
            DateTime endDate = new DateTime(Convert.ToInt32(period[1]), Convert.ToInt32(period[0]), lastDay, 23, 59, 59, 999);

            //changes here
            IList<Customer> customers = _customersModel.GetAllCommissionReport(agency, startDate.ToString(), endDate.ToString());

            List<CommissionReport> commissionReports = new List<CommissionReport>();

            foreach (Customer _customers in customers)
            {
                CommissionReport report = new CommissionReport();

                if (!string.IsNullOrEmpty(_customers.CompanyName))
                {
                    report.Name = _customers.CompanyName;
                }
                else
                {
                    report.Name = _customers.ContactPerson;
                }
                
                if(!string.IsNullOrEmpty(_customers.Email))
                {
                    report.NRIC = _customers.Email;
                }
                else
                {
                    report.NRIC = "-";
                }
                
                report.CommissionValue = _customers.TotalCompletedProfit.ToString("#,##0.00");

                commissionReports.Add(report);
            }

            IPagedList<CommissionReport> reports = _reportsModel.GetCommissionReportPaged(commissionReports, page, pageSize);
            ViewData["CommissionReport"] = reports;

            ViewData["Period"] = Convert.ToDateTime(Session["ReportPeriod"]).ToString("MMMM yyyy");
            ViewData["ReportCustomerAgency"] = agency;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString() + " :: Reports - Commission Reports";
            return View();
        }

        //GET: DownloadCommissionReports
        public void DownloadCommissionReports()
        {
            string[] period = Session["ReportPeriod"].ToString().Split('/');

            DateTime startDate = new DateTime(Convert.ToInt32(period[1]), Convert.ToInt32(period[0]), 1, 0, 0, 0, 0);
            int lastDay = DateTime.DaysInMonth(Convert.ToInt32(period[1]), Convert.ToInt32(period[0]));
            DateTime endDate = new DateTime(Convert.ToInt32(period[1]), Convert.ToInt32(period[0]), lastDay, 23, 59, 59, 999);

            string agency = Session["ReportCustomerAgency"].ToString();

            //changes here
            IList<Customer> customers = _customersModel.GetAllCommissionReport(agency, startDate.ToString(), endDate.ToString());

            List<CommissionReport> commissionReports = new List<CommissionReport>();

            foreach (Customer _customers in customers)
            {

                CommissionReport report = new CommissionReport();

                if (!string.IsNullOrEmpty(_customers.CompanyName))
                {
                    report.Name = _customers.CompanyName;
                }
                else
                {
                    report.Name = _customers.ContactPerson;
                }

                report.Name = _customers.ContactPerson;

                if (!string.IsNullOrEmpty(_customers.Email))
                {
                    report.NRIC = _customers.Email;
                }
                else
                {
                    report.NRIC = "-";
                }

                report.CommissionValue = _customers.TotalCompletedProfit.ToString("#,##0.00");

                commissionReports.Add(report);

            }

            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create Worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Commission Reports");

                //Set first row name
                ws.Cells[1, 1].Style.Font.Bold = true;
                ws.Cells[1, 1].Value = "Name";
                ws.Cells[1, 2].Style.Font.Bold = true;
                ws.Cells[1, 2].Value = "Email";
                ws.Cells[1, 3].Style.Font.Bold = true;
                ws.Cells[1, 3].Value = "Commission Value";

                int rowCount = 2;

                foreach (CommissionReport report in commissionReports)
                {
                    //Dump Data
                    ws.Cells[rowCount, 1].Value = report.Name;
                    ws.Cells[rowCount, 2].Value = report.NRIC;
                    ws.Cells[rowCount, 3].Value = report.CommissionValue;
                    rowCount++;
                }

                ws.Cells[ws.Dimension.Address].AutoFitColumns();

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=commission-reports-" + Convert.ToDateTime(Session["ReportPeriod"]).ToString("M/yyyy") + ".xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }

        //GET: PayoutReport
        public ActionResult PayoutReports(int page = 1)
        {
            int pageSize = 20;
            if (Session["PageSize"] != null)
            {
                pageSize = Convert.ToInt32(Session["PageSize"]);
            }
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            Dropdown[] pageSizeDDL = PageSizeDDL();
            ViewData["PageSizeDropdown"] = new SelectList(pageSizeDDL, "val", "name", pageSize);

            string[] period = Session["ReportPeriod"].ToString().Split('/');

            DateTime startDate = new DateTime(Convert.ToInt32(period[1]), Convert.ToInt32(period[0]), 1, 0, 0, 0, 0);
            int lastDay = DateTime.DaysInMonth(Convert.ToInt32(period[1]), Convert.ToInt32(period[0]));
            DateTime endDate = new DateTime(Convert.ToInt32(period[1]), Convert.ToInt32(period[0]), lastDay, 23, 59, 59, 999);

            //changes here
            IList<Payout> payouts = _payoutsModel.GetAll(0, null);

            List<PayoutReport> payoutReports = new List<PayoutReport>();

            foreach (Payout _payouts in payouts)
            {

                PayoutReport report = new PayoutReport();

                string Name = "-";
                string NRIC = "-";
                string BankNumber = "-";
                string BankName = "-";

                if(_payouts.UserType == "Customer")
                {
                    if(_payouts.Customer.CompanyName != null)
                    {
                        Name = _payouts.Customer.CompanyName;
                    }
                    else
                    {
                        Name = _payouts.Customer.ContactPerson;
                    }

                    NRIC = _payouts.Customer.Email;
                    BankNumber = _payouts.Customer.BankAccNo;
                    BankName = _payouts.Customer.BankName;

                }
                else
                {
                    if (_payouts.Salesperson != null)
                    {
                        Name = _payouts.Salesperson.Name;
                    }

                    NRIC = _payouts.Salesperson.Email;
                    BankNumber = _payouts.Salesperson.BankAccNo;
                    BankName = _payouts.Salesperson.BankName;
                }

                report.Name = Name;
                report.NRIC = NRIC;
                report.BankNumber = BankNumber;
                report.BankName = BankName;
                report.CommissionAmount = _payouts.TotalCommissionAmount.ToString("#,##0.00");
                payoutReports.Add(report);

            }

            IPagedList<PayoutReport> reports = _reportsModel.GetPayoutReportPaged(payoutReports, page, pageSize);
            ViewData["PayoutReport"] = reports;

            ViewData["Period"] = Convert.ToDateTime(Session["ReportPeriod"]).ToString("MMMM yyyy");

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString() + " :: Reports - Payout Reports";
            return View();
        }

        //GET: DownloadPayoutReports
        public void DownloadPayoutReports()
        {
            string[] period = Session["ReportPeriod"].ToString().Split('/');

            DateTime startDate = new DateTime(Convert.ToInt32(period[1]), Convert.ToInt32(period[0]), 1, 0, 0, 0, 0);
            int lastDay = DateTime.DaysInMonth(Convert.ToInt32(period[1]), Convert.ToInt32(period[0]));
            DateTime endDate = new DateTime(Convert.ToInt32(period[1]), Convert.ToInt32(period[0]), lastDay, 23, 59, 59, 999);

            //changes here
            IList<Payout> payouts = _payoutsModel.GetAll(0, null);

            List<PayoutReport> payoutReports = new List<PayoutReport>();

            foreach (Payout _payouts in payouts)
            {

                PayoutReport report = new PayoutReport();

                string Name = "-";
                string NRIC = "-";
                string BankNumber = "-";
                string BankName = "-";

                if (_payouts.UserType == "Customer")
                {
                    if (_payouts.Customer.CompanyName != null)
                    {
                        Name = _payouts.Customer.CompanyName;
                    }
                    else
                    {
                        Name = _payouts.Customer.ContactPerson;
                    }

                    if (_payouts.Customer.Email != null)
                    {
                        NRIC = _payouts.Customer.Email;
                    }

                    if(_payouts.Customer.BankAccNo != null)
                    {
                        BankNumber = _payouts.Customer.BankAccNo;
                    }

                    if (_payouts.Customer.BankName != null)
                    {
                        BankName = _payouts.Customer.BankName;
                    }
                }
                else
                {
                    if (_payouts.Salesperson != null)
                    {
                        Name = _payouts.Salesperson.Name;
                    }

                    if (_payouts.Salesperson.Email != null)
                    {
                        NRIC = _payouts.Salesperson.Email;
                    }

                    if (_payouts.Salesperson.BankAccNo != null)
                    {
                        BankNumber = _payouts.Salesperson.BankAccNo;
                    }

                    if (_payouts.Salesperson.BankName != null)
                    {
                        BankName = _payouts.Salesperson.BankName;
                    }
                    
                }

                report.Name = Name;
                report.NRIC = NRIC;
                report.BankNumber = BankNumber;
                report.BankName = BankName;
                report.CommissionAmount = _payouts.TotalCommissionAmount.ToString("#,##0.00");
                payoutReports.Add(report);

            }

            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create Worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Payout Reports");

                //Set first row name
                ws.Cells[1, 1].Style.Font.Bold = true;
                ws.Cells[1, 1].Value = "Name";
                ws.Cells[1, 2].Style.Font.Bold = true;
                ws.Cells[1, 2].Value = "Email";
                ws.Cells[1, 3].Style.Font.Bold = true;
                ws.Cells[1, 3].Value = "Bank Number";
                ws.Cells[1, 4].Style.Font.Bold = true;
                ws.Cells[1, 4].Value = "Bank Name";
                ws.Cells[1, 5].Style.Font.Bold = true;
                ws.Cells[1, 5].Value = "Commission Value";

                int rowCount = 2;

                foreach (PayoutReport report in payoutReports)
                {
                    //Dump Data
                    ws.Cells[rowCount, 1].Value = report.Name;
                    ws.Cells[rowCount, 2].Value = report.NRIC;
                    ws.Cells[rowCount, 3].Value = report.BankNumber;
                    ws.Cells[rowCount, 4].Value = report.BankName;
                    ws.Cells[rowCount, 5].Value = report.CommissionAmount;
                    rowCount++;
                }

                ws.Cells[ws.Dimension.Address].AutoFitColumns();

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=payout-reports-" + Convert.ToDateTime(Session["ReportPeriod"]).ToString("M/yyyy") + ".xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }

        //GET: Project Financial Report
        public ActionResult ProjectFinancialReports(int iPage = 1, int ePage = 1)
        {
            int pageSize = 20;
            if (Session["PageSize"] != null)
            {
                pageSize = Convert.ToInt32(Session["PageSize"]);
            }

            Session.Add("iPage", iPage);
            Session.Add("ePage", ePage);
            Session.Add("PageSize", pageSize);

            Dropdown[] pageSizeDDL = PageSizeDDL();
            ViewData["PageSizeDropdown"] = new SelectList(pageSizeDDL, "val", "name", pageSize);

            string ProjectCode = Session["ReportProjectCode"].ToString();
            string FinancialType = Session["ReportFinancialType"].ToString();

            ViewData["FinancialType"] = FinancialType;

            //changes here
            Project project = _projectsModel.GetSingle(Convert.ToInt32(ProjectCode));

            List<ProjectFinancialReport> projectFinancialReports = new List<ProjectFinancialReport>();
            List<ProjectFinancialReport.ExpenseReport> expenseReports = new List<ProjectFinancialReport.ExpenseReport>();
            
            if (FinancialType == "Invoice")
            {

                IList<Invoice> invoice = _invoicesModel.getAllProjectPaymentSummary(project.QuotationReferenceId);

                foreach(Invoice _invoice in invoice)
                {
                    ProjectFinancialReport report = new ProjectFinancialReport();

                    foreach (InvoiceItemDescription _itemDescription in _invoice.ItemDescriptions)
                    {
                        report.InvoiceID = _invoice.InvoiceID;
                        report.Description = _itemDescription.Description;

                        if(string.IsNullOrEmpty(_itemDescription.PercentageOfQuotation))
                        {
                            report.Percentage = "-";
                        }
                        else
                        {
                            report.Percentage = _itemDescription.PercentageOfQuotation + "%";
                        }

                        report.Status = _invoice.Status;
                        report.Amount = _itemDescription.Amount.ToString("#,##0.00");
                        projectFinancialReports.Add(report);
                    }

                }

                IPagedList<ProjectFinancialReport> reports = _reportsModel.GetProjectFinancialReportPaged(projectFinancialReports, iPage, pageSize);
                ViewData["FinancialReport"] = reports;
            }
            else if (FinancialType == "Expense")
            {
                IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetPersonalAll(project.ID);

                foreach (ProjectProjectExpenses _projectExpenes in projectExpenses)
                {
                    ProjectFinancialReport.ExpenseReport report = new ProjectFinancialReport.ExpenseReport();

                    report.Description = _projectExpenes.Description;
                    report.SupplierName = _projectExpenes.SupplierName;
                    report.InvoiceNo = _projectExpenes.InvoiceNo;
                    report.Date = _projectExpenes.Date;
                    report.GST = _projectExpenes.GST;
                    report.Cost = _projectExpenes.Cost.ToString("#,##0.00");
                    report.Status = _projectExpenes.Status;
                    expenseReports.Add(report);
                }

                IPagedList<ProjectFinancialReport.ExpenseReport> reports = _reportsModel.GetProjectExpenseReportPaged(expenseReports, ePage, pageSize);
                ViewData["FinancialReport"] = reports;
            }
            else
            {
                //Invoice
                IList<Invoice> invoice = _invoicesModel.getAllProjectPaymentSummary(project.QuotationReferenceId);

                foreach (Invoice _invoice in invoice)
                {
                    ProjectFinancialReport report = new ProjectFinancialReport();

                    foreach (InvoiceItemDescription _itemDescription in _invoice.ItemDescriptions)
                    {
                        report.InvoiceID = _invoice.InvoiceID;
                        report.Description = _itemDescription.Description;

                        if (string.IsNullOrEmpty(_itemDescription.PercentageOfQuotation))
                        {
                            report.Percentage = "-";
                        }
                        else
                        {
                            report.Percentage = _itemDescription.PercentageOfQuotation + "%";
                        }

                        report.Status = _invoice.Status;
                        report.Amount = _itemDescription.Amount.ToString("#,##0.00");
                        report.BillDate = Convert.ToDateTime(_invoice.Date).ToString();
                        projectFinancialReports.Add(report);
                    }

                }

                //Project Expense
                IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetPersonalAll(project.ID);

                foreach (ProjectProjectExpenses _projectExpenes in projectExpenses)
                {
                    ProjectFinancialReport.ExpenseReport report = new ProjectFinancialReport.ExpenseReport();

                    report.Description = _projectExpenes.Description;
                    report.SupplierName = _projectExpenes.SupplierName;
                    report.InvoiceNo = _projectExpenes.InvoiceNo;
                    report.Date = Convert.ToDateTime(_projectExpenes.Date);
                    report.GST = _projectExpenes.GST;
                    report.Cost = _projectExpenes.Cost.ToString("#,##0.00");
                    report.Status = _projectExpenes.Status;
                    expenseReports.Add(report);
                }

                IPagedList<ProjectFinancialReport> pageFinancialReports = _reportsModel.GetProjectFinancialReportPaged(projectFinancialReports, iPage, pageSize);
                IPagedList<ProjectFinancialReport.ExpenseReport> pageExpenseReports = _reportsModel.GetProjectExpenseReportPaged(expenseReports, ePage, pageSize);
                
                ViewData["FinancialReport"] = pageFinancialReports;
                ViewData["FinancialExpenseReport"] = pageExpenseReports;
            }
            ViewData["ProjectCode"] = project.ProjectID;
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString() + " :: Reports - Project Financial Reports";
            return View();
        }

        //GET: DownloadFinancialReports
        public void DownloadProjectFinancialReports()
        {

            string ProjectCode = Session["ReportProjectCode"].ToString();
            string FinancialType = Session["ReportFinancialType"].ToString();

            ViewData["FinancialType"] = FinancialType;

            //changes here
            Project project = _projectsModel.GetSingle(Convert.ToInt32(ProjectCode));

            List<ProjectFinancialReport> projectFinancialReports = new List<ProjectFinancialReport>();
            List<ProjectFinancialReport.ExpenseReport> expenseReports = new List<ProjectFinancialReport.ExpenseReport>();

            if (FinancialType == "Invoice")
            {

                IList<Invoice> invoice = _invoicesModel.getAllProjectPaymentSummary(project.QuotationReferenceId);

                foreach (Invoice _invoice in invoice)
                {
                    ProjectFinancialReport report = new ProjectFinancialReport();

                    foreach (InvoiceItemDescription _itemDescription in _invoice.ItemDescriptions)
                    {
                        report.InvoiceID = _invoice.InvoiceID;
                        report.Description = _itemDescription.Description;

                        if (string.IsNullOrEmpty(_itemDescription.PercentageOfQuotation))
                        {
                            report.Percentage = "-";
                        }
                        else
                        {
                            report.Percentage = _itemDescription.PercentageOfQuotation + "%";
                        }

                        report.Status = _invoice.Status;
                        report.Amount = _itemDescription.Amount.ToString("#,##0.00");
                        report.BillDate = Convert.ToDateTime(_invoice.Date).ToString();
                        projectFinancialReports.Add(report);
                    }

                }

            }
            else if (FinancialType == "Expense")
            {
                IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetPersonalAll(project.ID);

                foreach (ProjectProjectExpenses _projectExpenes in projectExpenses)
                {
                    ProjectFinancialReport.ExpenseReport report = new ProjectFinancialReport.ExpenseReport();

                    report.Description = _projectExpenes.Description;
                    report.SupplierName = _projectExpenes.SupplierName;
                    report.InvoiceNo = _projectExpenes.InvoiceNo;
                    report.Date = Convert.ToDateTime(_projectExpenes.Date);
                    report.GST = _projectExpenes.GST;
                    report.Cost = _projectExpenes.Cost.ToString("#,##0.00");
                    report.Status = _projectExpenes.Status;
                    expenseReports.Add(report);
                }

            }
            else
            {
                //Invoice
                IList<Invoice> invoice = _invoicesModel.getAllProjectPaymentSummary(project.QuotationReferenceId);

                foreach (Invoice _invoice in invoice)
                {
                    ProjectFinancialReport report = new ProjectFinancialReport();

                    foreach (InvoiceItemDescription _itemDescription in _invoice.ItemDescriptions)
                    {
                        report.InvoiceID = _invoice.InvoiceID;
                        report.Description = _itemDescription.Description;

                        if (string.IsNullOrEmpty(_itemDescription.PercentageOfQuotation))
                        {
                            report.Percentage = "-";
                        }
                        else
                        {
                            report.Percentage = _itemDescription.PercentageOfQuotation + "%";
                        }

                        report.Status = _invoice.Status;
                        report.Amount = _itemDescription.Amount.ToString("#,##0.00");
                        report.BillDate = Convert.ToDateTime(_invoice.Date).ToString("dd/MM/yyyy");
                        projectFinancialReports.Add(report);
                    }

                }

                //Project Expense
                IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetPersonalAll(project.ID);

                foreach (ProjectProjectExpenses _projectExpenes in projectExpenses)
                {
                    ProjectFinancialReport.ExpenseReport report = new ProjectFinancialReport.ExpenseReport();

                    report.Description = _projectExpenes.Description;
                    report.SupplierName = _projectExpenes.SupplierName;
                    report.InvoiceNo = _projectExpenes.InvoiceNo;
                    report.Date = Convert.ToDateTime(_projectExpenes.Date.ToString("dd/MM/yyyy"));
                    report.GST = _projectExpenes.GST;
                    report.Cost = _projectExpenes.Cost.ToString("#,##0.00");
                    report.Status = _projectExpenes.Status;
                    expenseReports.Add(report);
                }

            }

            using (ExcelPackage pck = new ExcelPackage())
            {

                if (FinancialType == "Invoice")
                {
                    //Create Worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Invoice");

                    //Set first row name
                    ws.Cells[1, 1].Style.Font.Bold = true;
                    ws.Cells[1, 1].Value = "Invoice ID";
                    ws.Cells[1, 2].Style.Font.Bold = true;
                    ws.Cells[1, 2].Value = "Description";
                    ws.Cells[1, 3].Style.Font.Bold = true;
                    ws.Cells[1, 3].Value = "Percentage";
                    ws.Cells[1, 4].Style.Font.Bold = true;
                    ws.Cells[1, 4].Value = "Status";
                    ws.Cells[1, 5].Style.Font.Bold = true;
                    ws.Cells[1, 5].Value = "Amount";
                    ws.Cells[1, 6].Style.Font.Bold = true;
                    ws.Cells[1, 6].Value = "Bill Date";

                    int rowCount = 2;

                    foreach (ProjectFinancialReport report in projectFinancialReports)
                    {
                        //Dump Data
                        ws.Cells[rowCount, 1].Value = report.InvoiceID;
                        ws.Cells[rowCount, 2].Value = report.Description;
                        ws.Cells[rowCount, 3].Value = report.Percentage;
                        ws.Cells[rowCount, 4].Value = report.Status;
                        ws.Cells[rowCount, 5].Value = report.Amount;
                        ws.Cells[rowCount, 6].Value = report.BillDate;
                        rowCount++;
                    }

                    ws.Cells[ws.Dimension.Address].AutoFitColumns();
                }
                else if (FinancialType == "Expense")
                {
                    //Create Worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Expense");

                    //Set first row name
                    ws.Cells[1, 1].Style.Font.Bold = true;
                    ws.Cells[1, 1].Value = "Description";
                    ws.Cells[1, 2].Style.Font.Bold = true;
                    ws.Cells[1, 2].Value = "Supplier Name";
                    ws.Cells[1, 3].Style.Font.Bold = true;
                    ws.Cells[1, 3].Value = "Invoice No";
                    ws.Cells[1, 4].Style.Font.Bold = true;
                    ws.Cells[1, 4].Value = "Date";
                    ws.Cells[1, 5].Style.Font.Bold = true;
                    ws.Cells[1, 5].Value = "GST";
                    ws.Cells[1, 6].Style.Font.Bold = true;
                    ws.Cells[1, 6].Value = "Cost (before GST)";
                    ws.Cells[1, 7].Style.Font.Bold = true;
                    ws.Cells[1, 7].Value = "Status";

                    int rowCount = 2;

                    foreach (ProjectFinancialReport.ExpenseReport report in expenseReports)
                    {
                        //Dump Data
                        ws.Cells[rowCount, 1].Value = report.Description;
                        ws.Cells[rowCount, 2].Value = report.SupplierName;
                        ws.Cells[rowCount, 3].Value = report.InvoiceNo;
                        ws.Cells[rowCount, 4].Value = Convert.ToDateTime(report.Date).ToString("dd/MM/yyyy");
                        ws.Cells[rowCount, 5].Value = report.GST;
                        ws.Cells[rowCount, 6].Value = report.Cost;
                        ws.Cells[rowCount, 7].Value = report.Status;
                        rowCount++;
                    }

                    ws.Cells[ws.Dimension.Address].AutoFitColumns();
                }
                else
                {
                    //Create Worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Invoice");
                    ExcelWorksheet ws2 = pck.Workbook.Worksheets.Add("Expense");

                    //Invoice
                    //Set first row name
                    ws.Cells[1, 1].Style.Font.Bold = true;
                    ws.Cells[1, 1].Value = "Invoice ID";
                    ws.Cells[1, 2].Style.Font.Bold = true;
                    ws.Cells[1, 2].Value = "Description";
                    ws.Cells[1, 3].Style.Font.Bold = true;
                    ws.Cells[1, 3].Value = "Percentage";
                    ws.Cells[1, 4].Style.Font.Bold = true;
                    ws.Cells[1, 4].Value = "Status";
                    ws.Cells[1, 5].Style.Font.Bold = true;
                    ws.Cells[1, 5].Value = "Amount";
                    ws.Cells[1, 6].Style.Font.Bold = true;
                    ws.Cells[1, 6].Value = "Bill Date";

                    int rowCount = 2;

                    foreach (ProjectFinancialReport report in projectFinancialReports)
                    {
                        //Dump Data
                        ws.Cells[rowCount, 1].Value = report.InvoiceID;
                        ws.Cells[rowCount, 2].Value = report.Description;
                        ws.Cells[rowCount, 3].Value = report.Percentage;
                        ws.Cells[rowCount, 4].Value = report.Status;
                        ws.Cells[rowCount, 5].Value = report.Amount;
                        ws.Cells[rowCount, 6].Value = Convert.ToDateTime(report.BillDate).ToString("dd/MM/yyyy");
                        rowCount++;
                    }

                    //Expense
                    //Set first row name
                    ws2.Cells[1, 1].Style.Font.Bold = true;
                    ws2.Cells[1, 1].Value = "Description";
                    ws2.Cells[1, 2].Style.Font.Bold = true;
                    ws2.Cells[1, 2].Value = "Supplier Name";
                    ws2.Cells[1, 3].Style.Font.Bold = true;
                    ws2.Cells[1, 3].Value = "Invoice No";
                    ws2.Cells[1, 4].Style.Font.Bold = true;
                    ws2.Cells[1, 4].Value = "Date";
                    ws2.Cells[1, 5].Style.Font.Bold = true;
                    ws2.Cells[1, 5].Value = "GST";
                    ws2.Cells[1, 6].Style.Font.Bold = true;
                    ws2.Cells[1, 6].Value = "Cost (before GST)";
                    ws2.Cells[1, 7].Style.Font.Bold = true;
                    ws2.Cells[1, 7].Value = "Status";

                    int rowCount2 = 2;

                    foreach (ProjectFinancialReport.ExpenseReport report in expenseReports)
                    {
                        //Dump Data
                        ws2.Cells[rowCount2, 1].Value = report.Description;
                        ws2.Cells[rowCount2, 2].Value = report.SupplierName;
                        ws2.Cells[rowCount2, 3].Value = report.InvoiceNo;
                        ws2.Cells[rowCount2, 4].Value = Convert.ToDateTime(report.Date).ToString("dd/MM/yyyy");
                        ws2.Cells[rowCount2, 5].Value = report.GST;
                        ws2.Cells[rowCount2, 6].Value = report.Cost;
                        ws2.Cells[rowCount2, 7].Value = report.Status;
                        rowCount2++;
                    }
                    ws.Cells[ws.Dimension.Address].AutoFitColumns();
                    ws2.Cells[ws2.Dimension.Address].AutoFitColumns();
                }

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=project-financial-reports-" + project.ProjectID + "-" + FinancialType + ".xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }

        //GET: RetainedAmountReport
        public ActionResult RetainedAmountReports(int page = 1)
        {
            int pageSize = 20;
            if (Session["PageSize"] != null)
            {
                pageSize = Convert.ToInt32(Session["PageSize"]);
            }
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            Dropdown[] pageSizeDDL = PageSizeDDL();
            ViewData["PageSizeDropdown"] = new SelectList(pageSizeDDL, "val", "name", pageSize);

            DateTime DateRangeFrom = Convert.ToDateTime(Session["ReportDateRangeFrom"].ToString());
            DateTime DateRangeTo = Convert.ToDateTime(Session["ReportDateRangeTo"].ToString());

            //changes here
            IList<Payout> payouts = _payoutsModel.GetAll(0, null);

            List<RetainedAmountReport> retainedAmountReports = new List<RetainedAmountReport>();

            foreach (Payout _payouts in payouts)
            {

                RetainedAmountReport report = new RetainedAmountReport();

                string Name = "-";
                string Email = "-";
                string RetainAmount = "-";

                if (_payouts.UserType == "Customer")
                {
                    if (_payouts.Customer.CompanyName != null)
                    {
                        Name = _payouts.Customer.CompanyName;
                    }
                    else
                    {
                        Name = _payouts.Customer.ContactPerson;
                    }

                    Email = _payouts.Customer.Email;
                    RetainAmount = _payouts.TotalRetainedAmount.ToString("#,##0.00");
                }

                report.Name = Name;
                report.Email = Email;
                report.RetainAmount = RetainAmount;
                retainedAmountReports.Add(report);

            }

            IPagedList<RetainedAmountReport> reports = _reportsModel.GetRetainedAmountReportPaged(retainedAmountReports, page, pageSize);
            ViewData["RetainedAmountReport"] = reports;

            ViewData["DateRangeFrom"] = DateRangeFrom.ToString("dd/MM/yyyy");
            ViewData["DateRangeTo"] = DateRangeTo.ToString("dd/MM/yyyy");

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString() + " :: Reports - Retained Amount Reports";
            return View();
        }

        //GET: DownloadRetainedAmountReports
        public void DownloadRetainedAmountReports()
        {

            DateTime DateRangeFrom = Convert.ToDateTime(Session["ReportDateRangeFrom"].ToString());
            DateTime DateRangeTo = Convert.ToDateTime(Session["ReportDateRangeTo"].ToString());

            //changes here
            IList<Payout> payouts = _payoutsModel.GetAll(0, null);

            List<RetainedAmountReport> retainedAmountReports = new List<RetainedAmountReport>();

            foreach (Payout _payouts in payouts)
            {

                RetainedAmountReport report = new RetainedAmountReport();

                string Name = "-";
                string Email = "-";
                string RetainAmount = "-";

                if (_payouts.UserType == "Customer")
                {
                    if (_payouts.Customer.CompanyName != null)
                    {
                        Name = _payouts.Customer.CompanyName;
                    }
                    else
                    {
                        Name = _payouts.Customer.ContactPerson;
                    }

                    Email = _payouts.Customer.Email;
                    RetainAmount = _payouts.TotalRetainedAmount.ToString("#,##0.00");
                }

                report.Name = Name;
                report.Email = Email;
                report.RetainAmount = RetainAmount;
                retainedAmountReports.Add(report);

            }

            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create Worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Retained Amount Reports");

                //Set first row name
                ws.Cells[1, 1].Style.Font.Bold = true;
                ws.Cells[1, 1].Value = "Sales Name";
                ws.Cells[1, 2].Style.Font.Bold = true;
                ws.Cells[1, 2].Value = "Email";
                ws.Cells[1, 3].Style.Font.Bold = true;
                ws.Cells[1, 3].Value = "Retain Amount";

                int rowCount = 2;

                foreach (RetainedAmountReport report in retainedAmountReports)
                {
                    //Dump Data
                    ws.Cells[rowCount, 1].Value = report.Name;
                    ws.Cells[rowCount, 2].Value = report.Email;
                    ws.Cells[rowCount, 3].Value = report.RetainAmount;

                    rowCount++;
                }

                ws.Cells[ws.Dimension.Address].AutoFitColumns();

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=retained-amount-reports-" + DateRangeFrom.ToString("yyyyMMdd") + "-" + DateRangeTo.ToString("yyyyMMdd") + ".xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }

        //Report Type Dropdown
        public Dropdown[] ReportTypeDDL()
        {
            Dropdown[] ddl = new Dropdown[5];
            ddl[0] = new Dropdown { name = "Commission Reports", val = "CommissionReports" };
            ddl[1] = new Dropdown { name = "Expense Reports", val = "ExpenseReports" };
            ddl[2] = new Dropdown { name = "Project Financial Reports", val = "ProjectFinancialReports" };
            ddl[3] = new Dropdown { name = "Payout Reports", val = "PayoutReports" };
            ddl[4] = new Dropdown { name = "Retained Amount Reports", val = "RetainedAmountReports" };
            return ddl;
        }

        //Report Customer Agency Dropdown
        public Dropdown[] ReportCustomerAgencyDDL()
        {
            Dropdown[] ddl = new Dropdown[3];

            ddl[0] = new Dropdown { name = "No Agency", val = "No Agency" };
            ddl[1] = new Dropdown { name = "Era", val = "Era" };
            ddl[2] = new Dropdown { name = "Propnex", val = "Propnex" };

            return ddl;
        }

        //Report Period Dropdown
        public Dropdown[] ReportPeriodDDL()
        {
            DateTime today = DateTime.Now;

            int month = 60; // For 5 years

            Dropdown[] ddl = new Dropdown[month];

            for (int count = 0; count < month; count++)
            {
                ddl[count] = new Dropdown { name = today.AddMonths(-count).ToString("MMMM yyyy"), val = today.AddMonths(-count).ToString("M/yyyy") };
            }

            return ddl;
        }

        //Report GST Dropdown
        public Dropdown[] ReportGSTDDL()
        {

            Dropdown[] ddl = new Dropdown[3];

            ddl[0] = new Dropdown { name = "Yes", val = "Yes" };
            ddl[1] = new Dropdown { name = "No", val = "No" };
            ddl[2] = new Dropdown { name = "Both", val = "Both" };

            return ddl;
        }

        //Report Financial Type Dropdown
        public Dropdown[] ReportFinancialTypeDDL()
        {

            Dropdown[] ddl = new Dropdown[3];

            ddl[0] = new Dropdown { name = "Invoice", val = "Invoice" };
            ddl[1] = new Dropdown { name = "Expense", val = "Expense" };
            ddl[2] = new Dropdown { name = "Both", val = "Both" };

            return ddl;
        }

        //Report Project Code Dropdown
        public Dropdown[] ReportProjectCodeDDL()
        {

            IList<Project> projects = _projectsModel.GetAll();

            Dropdown[] ddl = new Dropdown[projects.Count];

            int count = 0;
            foreach (Project _projects in projects)
            {
                ddl[count] = new Dropdown { name = _projects.ProjectID, val = Convert.ToString(_projects.ID) };
                count++;
            }

            return ddl;
        }

        //Page Size Dropdown
        public Dropdown[] PageSizeDDL()
        {
            Dropdown[] ddl = new Dropdown[5];
            ddl[0] = new Dropdown { name = "20", val = "20" };
            ddl[1] = new Dropdown { name = "40", val = "40" };
            ddl[2] = new Dropdown { name = "60", val = "60" };
            ddl[3] = new Dropdown { name = "80", val = "80" };
            ddl[4] = new Dropdown { name = "100", val = "100" };
            return ddl;
        }
    }
}