﻿using DataAccess;
using DataAccess.POCO;
using Newtonsoft.Json;
using NODC_OMS.Controllers;
using NODC_OMS.Helper;
using NODC_OMS.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NODC_OMS.Controllers
{
    [HandleError]
    [RedirectingAction]
    public class ProjectController : ControllerBase
    {
        private IUserRepository _usersModel;
        private IProjectRepository _projectsModel;
        private IProjectProjectExpensesRepository _projectExpensesModel;
        private IProjectEstimatorRepository _projectEstimatorsModel;
        private ISystemSettingRepository _settingsModel;
        private IQuotationRepository _quotationsModel;
        private IQuotationItemDescriptionRepository _quotationItemDescriptionsModel;
        private ICustomerRepository _customersModel;
        private IInvoiceRepository _invoicesModel;

        // GET: Project
        public ProjectController()
            : this(new UserRepository(), new ProjectRepository(), new ProjectProjectExpensesRepository(), new ProjectEstimatorRepository(), new SystemSettingRepository(), new QuotationRepository(), new QuotationItemDescriptionRepository(), new CustomerRepository(), new InvoiceRepository())
        {

        }

        public ProjectController(IUserRepository usersModel, IProjectRepository projectsModel, IProjectProjectExpensesRepository projectExpensesModel, IProjectEstimatorRepository projectEstimatorsModel, ISystemSettingRepository settingsModel, IQuotationRepository quotationsModel, IQuotationItemDescriptionRepository quotationItemDescriptionsModel, ICustomerRepository customersModel, IInvoiceRepository invoicesModel)
        {
            _usersModel = usersModel;
            _projectsModel = projectsModel;
            _projectExpensesModel = projectExpensesModel;
            _projectEstimatorsModel = projectEstimatorsModel;
            _settingsModel = settingsModel;
            _quotationsModel = quotationsModel;
            _quotationItemDescriptionsModel = quotationItemDescriptionsModel;
            _customersModel = customersModel;
            _invoicesModel = invoicesModel;
        }

        // GET: Project
        public ActionResult Index()
        {
            if (Session["SearchKeyword"] != null)
            {
                Session.Remove("SearchKeyword");
            }

            if (Session["SearchStatus"] != null)
            {
                Session.Remove("SearchStatus");
            }

            return RedirectToAction("Listing");
        }

        //GET: Listing
        public ActionResult Listing(int page = 1)
        {
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            //Check is superadmin or sales
            //0 is for super admin
            int RoleTypeId = 0;

            User user = _usersModel.GetSingle(loginMainID);
            ViewData["IsAllowDelete"] = "No";

            if(user.Role == "Super Admin")
            {
                ViewData["IsAllowDelete"] = "Yes";
            }
            else
            {
                RoleTypeId = loginMainID;
            }

            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);

            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = "";
            ViewData["SearchStatus"] = "";
            ViewData["GetStatusValue"] = "All Projects";

            if (Session["SearchKeyword"] != null)
            {
                ViewData["SearchKeyword"] = Session["SearchKeyword"];
            }

            if (Session["SearchStatus"] != null)
            {

                if (Session["SearchStatus"].ToString() == "Active")
                {
                    ViewData["SearchStatus"] = "Active";
                    ViewData["GetStatusValue"] = "Active Only";
                }
                else if (Session["SearchStatus"].ToString() == "Completed")
                {
                    ViewData["SearchStatus"] = "Complete";
                    ViewData["GetStatusValue"] = "Completed Only";
                }
                else
                {
                    ViewData["SearchStatus"] = Session["SearchStatus"];
                }
            }

            List<int> SalesId = new List<int>();

            if(user.Role == "Super Admin")
            {
                //SalesId.Add(0);
            }
            else
            {
                SalesId.Add(loginMainID);

                //sales and customer, filter

                //first, get all upline id is same with login main ID.

                List<User> getSameUplineID = _usersModel.GetAllSameUplineID(loginMainID); //6

                foreach (User _getSameUplineID in getSameUplineID)
                {

                    SalesId.Add(_getSameUplineID.ID);
                }
            }

            IPagedList<Project> project = _projectsModel.GetPaged(ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), 0, SalesId, page, pageSize);
            
            ViewData["Project"] = project;

            IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetAll();

            ViewData["ProjectExpenses"] = projectExpenses;

            IList<QuotationItemDescription> quotationItemDescription = _quotationItemDescriptionsModel.GetAll();

            ViewData["QuotationItemDescription"] = quotationItemDescription;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Listing
        [HttpPost]
        public ActionResult Listing(FormCollection form)
        {
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            int RoleTypeId = 0;

            User user = _usersModel.GetSingle(loginMainID);

            ViewData["IsAllowDelete"] = "No";

            if (user.Role == "Super Admin")
            {
                ViewData["IsAllowDelete"] = "Yes";
            }
            else
            {
                RoleTypeId = loginMainID;
            }

            int page = 1;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = form["SearchKeyword"].Trim();
            Session.Add("SearchKeyword", form["SearchKeyword"].Trim());

            ViewData["GetStatusValue"] = "All Projects";


            if (form["SearchStatusValue"].Trim() == "Active")
            {
                ViewData["GetStatusValue"] = "Active Only";
            }

            if (form["SearchStatusValue"].Trim() == "Complete")
            {
                ViewData["GetStatusValue"] = "Completed Only";
            }

            string item = "";

            if (form["SearchStatusValue"].Trim() != "All")
            {
                item = form["SearchStatusValue"].Trim();
            }

            ViewData["SearchStatus"] = item;
            Session.Add("SearchStatus", item);

            List<int> SalesId = new List<int>();

            if (user.Role == "Super Admin")
            {
                //SalesId.Add(0);
            }
            else
            {
                SalesId.Add(loginMainID);

                //sales and customer, filter

                //first, get all upline id is same with login main ID.

                List<User> getSameUplineID = _usersModel.GetAllSameUplineID(loginMainID); //6

                foreach (User _getSameUplineID in getSameUplineID)
                {

                    SalesId.Add(_getSameUplineID.ID);
                }
            }

            IPagedList<Project> project = _projectsModel.GetPaged(ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), 0, SalesId, page, pageSize);
            
            ViewData["Project"] = project;

            IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetAll();

            ViewData["ProjectExpenses"] = projectExpenses;

            IList<QuotationItemDescription> quotationItemDescription = _quotationItemDescriptionsModel.GetAll();

            ViewData["QuotationItemDescription"] = quotationItemDescription;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Create
        public ActionResult Create()
        {
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            User user = _usersModel.GetSingle(loginMainID);

            ViewData["PreparedBy"] = user.Name;

            SystemSetting settings = _settingsModel.GetSingle("COMPANY_NAME");

            ViewData["Company"] = settings.Value;

            List<ProjectProjectExpensesTable> projectExpensesTable = new List<ProjectProjectExpensesTable>();

            //projectExpensesTable.Add(new ProjectProjectExpensesTable()
            //{
            //    RowId = 1,
            //    Description = null,
            //    SupplierName = null,
            //    Date = DateTime.Now.ToString("dd/MM/yyyy"),
            //    GST = new SelectList(GSTDDL(), "val", "name", "Yes"),
            //    CostBeforeGST = null,
            //    Status = new SelectList(ProjectProjectExpensesStatusDDL(), "val", "name", "Unpaid"),
            //});

            ViewData["ProjectExpensesTable"] = projectExpensesTable;

            Dropdown[] quotationDDL = QuotationDDL();
            ViewData["QuotationDropdown"] = new SelectList(quotationDDL, "val", "name", null);

            Dropdown[] customerDDL = CustomerDDL();

            //Drop Quotation Customer ID



            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", null);

            Dropdown[] coordinatorDDL = CoordinatorDDL();
            ViewData["CoordinatorDropdown"] = new SelectList(coordinatorDDL, "val", "name", loginMainID);

            Dropdown[] projectTypeDDL = ProjectTypeDDL();
            ViewData["ProjectTypeDropdown"] = new SelectList(projectTypeDDL, "val", "name", null);

            ViewData["PhotosUploadFiles"] = null;
            ViewData["FloorPlanUploadsFiles"] = null;
            ViewData["ThreeDDrawingUploadsFiles"] = null;
            ViewData["AsBuildDrawingUploadsFiles"] = null;
            ViewData["SubmissionStageUploadsFiles"] = null;
            ViewData["OtherUploadsFiles"] = null;

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", null);

            ViewData["TotalRevenue"] = "0.00";
            ViewData["TotalExpenses"] = "0.00";
            ViewData["TotalProfit"] = "0.00";
            ViewData["AddClass"] = "No";

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Create
        [HttpPost]
        public ActionResult Create(Project project, FormCollection form)
        {
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            User user = _usersModel.GetSingle(loginMainID);

            ViewData["PreparedBy"] = user.Name;

            SystemSetting settings = _settingsModel.GetSingle("COMPANY_NAME");

            ViewData["Company"] = settings.Value;

            //Validation Part
            if(project.QuotationReferenceId == 0)
            {
                ModelState.AddModelError("project.QuotationReferenceId", "Quotation is required!");
            }

            if (project.CustomerId == 0)
            {
                ModelState.AddModelError("project.CustomerId", "Customer is required!");
            }

            if (project.CoordinatorId == 0)
            {
                ModelState.AddModelError("project.CoordinatorId", "Coordinator is required!");
            }

            if(string.IsNullOrEmpty(project.ProjectTitle))
            {
                ModelState.AddModelError("project.ProjectTitle", "Project Title is required!");
            }

            if (string.IsNullOrEmpty(project.Remarks))
            {
                project.Remarks = null;
            }

            //Project Expenses Validation Part
            List<string> projectExpensesKeys = form.AllKeys.Where(e => e.Contains("ProjectExpensesDescription_")).ToList();

            List<ProjectProjectExpensesTable> projectExpensesTable = new List<ProjectProjectExpensesTable>();

            //For PostBack Use
            decimal TotalExpenses = 0;

            foreach(string _projectExpensesKeys in projectExpensesKeys)
            {
                string rowId = _projectExpensesKeys.Split('_')[1];

                string Description = form["ProjectExpensesDescription_" + rowId];
                string SupplierName = form["ProjectExpensesSupplierName_" + rowId];
                string InvoiceNo = form["ProjectExpensesInvoiceNo_" + rowId];
                string Date = form["ProjectExpensesDate_" + rowId];
                string GST = form["ProjectExpensesGST_" + rowId];
                string CostBeforeGST = form["ProjectExpensesCostBeforeGST_" + rowId];
                string ProjectExpensesStatus = form["ProjectExpensesStatus_" + rowId];

                if(string.IsNullOrEmpty(Description))
                {
                    ModelState.AddModelError("ProjectExpensesDescription_" + rowId, "Description is required!");
                }

                if(string.IsNullOrEmpty(SupplierName))
                {
                    ModelState.AddModelError("ProjectExpensesSupplierName_" + rowId, "Supplier Name is required!");
                }

                if (string.IsNullOrEmpty(InvoiceNo))
                {
                    ModelState.AddModelError("ProjectExpensesInvoiceNo_" + rowId, "Invoice No is required!");
                }

                if(string.IsNullOrEmpty(Date))
                {
                    ModelState.AddModelError("ProjectExpensesDate_" + rowId, "Date is required!");
                }

                if (string.IsNullOrEmpty(GST))
                {
                    ModelState.AddModelError("ProjectExpensesGST_" + rowId, "GST is required!");
                }

                if (string.IsNullOrEmpty(CostBeforeGST))
                {
                    ModelState.AddModelError("ProjectExpensesCostBeforeGST_" + rowId, "Cost Before GST is required!");
                }
                else
                {
                    //Check format
                    //Check is decimal or not
                    bool checkFormat = FormValidationHelper.AmountFormat(CostBeforeGST);

                    if (!checkFormat)
                    {
                        ModelState.AddModelError("ProjectExpensesCostBeforeGST_" + rowId, "Invalid Cost Before GST Format!");
                    }
                    else
                    {
                        TotalExpenses += Convert.ToDecimal(CostBeforeGST);
                    }
                }

                projectExpensesTable.Add(new ProjectProjectExpensesTable()
                {
                    RowId = Convert.ToInt32(rowId),
                    Description = Description,
                    SupplierName = SupplierName,
                    InvoiceNo = InvoiceNo,
                    Date = Date,
                    GST = new SelectList(GSTDDL(), "val", "name", GST),
                    CostBeforeGST = CostBeforeGST,
                    Status = new SelectList(ProjectProjectExpensesStatusDDL(), "val", "name", ProjectExpensesStatus),
                });
            }

            if(ModelState.IsValid)
            {

                //For store Project Expenses Item
                ProjectProjectExpenses projectProjectExpensesRecord = new ProjectProjectExpenses();

                //Assign Project ID
                string prefix = _settingsModel.GetCodeValue("PREFIX_PROJECT_ID");

                Project lastRecord = _projectsModel.GetLast();

                //Convert year to last two digit
                var date = DateTime.Now;
                int last_two_digit = date.Year % 100;

                if (lastRecord != null)
                {
                    project.ProjectID = prefix + last_two_digit + (lastRecord.ID + 1).ToString().PadLeft(5, '0');
                }
                else
                {
                    project.ProjectID = prefix + last_two_digit + "00001";
                }

                project.CompanyName = settings.Value;
                project.PreparedById = loginMainID;

                if (project.Status == "Completed")
                {
                    project.CompletedDate = DateTime.Now;
                }
                else
                {
                    project.CompletedDate = null;
                }

                //Save into DB (Project)
                bool result = _projectsModel.Add(project);

                if(result)
                {
                    string userRole = Session["UserGroup"].ToString();
                    if (userRole == "Sales")
                    {
                        userRole += " " + Session["Position"].ToString();
                    }
                    string tableAffected = "Projects";
                    string description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Project [" + project.ProjectID + "]";

                    bool create_log = AuditLogHelper.WriteAuditLog(loginMainID, userRole, tableAffected, description);

                    //Check Photo Uploads
                    if (!string.IsNullOrEmpty(project.PhotoUploads))
                    {
                        string[] PhotoUploads = project.PhotoUploads.Split(',');

                        foreach (string file in PhotoUploads)
                        {
                            string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                            string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["ProjectPhotoUploadsFolder"].ToString()), file);

                            System.IO.File.Move(sourceFile, destinationFile);
                        }
                    }

                    //Check Floor Plans Uploads
                    if (!string.IsNullOrEmpty(project.FloorPlanUploads))
                    {
                        string[] FloorPlanUploads = project.FloorPlanUploads.Split(',');

                        foreach (string file in FloorPlanUploads)
                        {
                            string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                            string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["ProjectFloorPlanUploadsFolder"].ToString()), file);

                            System.IO.File.Move(sourceFile, destinationFile);
                        }
                    }

                    //Check 3D Drawings Uploads
                    if (!string.IsNullOrEmpty(project.ThreeDDrawingUploads))
                    {
                        string[] ThreeDDrawingUploads = project.ThreeDDrawingUploads.Split(',');

                        foreach (string file in ThreeDDrawingUploads)
                        {
                            string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                            string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["ProjectThreeDDrawingUploadsFolder"].ToString()), file);

                            System.IO.File.Move(sourceFile, destinationFile);
                        }
                    }

                    //Check As Build Drawing Uploads
                    if (!string.IsNullOrEmpty(project.AsBuildDrawingUploads))
                    {
                        string[] AsBuildDrawingUploads = project.AsBuildDrawingUploads.Split(',');

                        foreach (string file in AsBuildDrawingUploads)
                        {
                            string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                            string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["ProjectAsBuildDrawingUploadsFolder"].ToString()), file);

                            System.IO.File.Move(sourceFile, destinationFile);
                        }
                    }

                    //Check As Submission Stage Uploads
                    if (!string.IsNullOrEmpty(project.SubmissionStageUploads))
                    {
                        string[] SubmissionStageUploads = project.SubmissionStageUploads.Split(',');

                        foreach (string file in SubmissionStageUploads)
                        {
                            string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                            string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["ProjectSubmissionStageUploadsFolder"].ToString()), file);

                            System.IO.File.Move(sourceFile, destinationFile);
                        }
                    }

                    //Check Others Uploads
                    if (!string.IsNullOrEmpty(project.OtherUploads))
                    {
                        string[] OtherUploads = project.OtherUploads.Split(',');

                        foreach (string file in OtherUploads)
                        {
                            string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                            string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["ProjectOtherUploadsFolder"].ToString()), file);

                            System.IO.File.Move(sourceFile, destinationFile);
                        }
                    }

                    //Store Project Expenses Item

                    bool hasProjectExpenses = false;
                    foreach (ProjectProjectExpensesTable _projectExpensesTable in projectExpensesTable)
                    {

                        projectProjectExpensesRecord.ProjectId = project.ID;
                        projectProjectExpensesRecord.Description = _projectExpensesTable.Description;
                        projectProjectExpensesRecord.SupplierName = _projectExpensesTable.SupplierName;
                        projectProjectExpensesRecord.InvoiceNo = _projectExpensesTable.InvoiceNo;
                        projectProjectExpensesRecord.Date = Convert.ToDateTime(_projectExpensesTable.Date);
                        projectProjectExpensesRecord.GST = Convert.ToString(_projectExpensesTable.GST.SelectedValue);
                        projectProjectExpensesRecord.Cost = Convert.ToDecimal(_projectExpensesTable.CostBeforeGST);
                        projectProjectExpensesRecord.Status = Convert.ToString(_projectExpensesTable.Status.SelectedValue);

                        bool add_project_projectExpenses = _projectExpensesModel.Add(projectProjectExpensesRecord);

                        if (add_project_projectExpenses)
                        {
                            if (!hasProjectExpenses)
                            {
                                hasProjectExpenses = true;
                            }
                        }
                    }

                    if (hasProjectExpenses)
                    {
                        userRole = Session["UserGroup"].ToString();
                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }
                        tableAffected = "ProjectProjectExpenses";
                        description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Project Project Expenses [" + project.ProjectID + "]";

                        bool itemDescription_create_log = AuditLogHelper.WriteAuditLog(loginMainID, userRole, tableAffected, description);
                    }

                    Session.Add("Result", "success|" + project.ProjectID + " has been successfully created!");
                    return RedirectToAction("Index");
                }
                else
                {
                    Session.Add("Result", "danger|An error occured while save project records!");
                }
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            ViewData["ProjectExpensesTable"] = projectExpensesTable;

            Dropdown[] quotationDDL = QuotationDDL();
            ViewData["QuotationDropdown"] = new SelectList(quotationDDL, "val", "name", project.QuotationReferenceId);

            Dropdown[] customerDDL = CustomerDDL();
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", project.CustomerId);

            Dropdown[] coordinatorDDL = CoordinatorDDL();

            ViewData["CoordinatorDropdown"] = new SelectList(coordinatorDDL, "val", "name", project.CoordinatorId);

            Dropdown[] projectTypeDDL = ProjectTypeDDL();
            ViewData["ProjectTypeDropdown"] = new SelectList(projectTypeDDL, "val", "name", project.ProjectType);

            ViewData["PhotosUploadFiles"] = project.PhotoUploads;
            ViewData["FloorPlanUploadsFiles"] = project.FloorPlanUploads;
            ViewData["ThreeDDrawingUploadsFiles"] = project.ThreeDDrawingUploads;
            ViewData["AsBuildDrawingUploadsFiles"] = project.AsBuildDrawingUploads;
            ViewData["SubmissionStageUploadsFiles"] = project.SubmissionStageUploads;
            ViewData["OtherUploadsFiles"] = project.OtherUploads;

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", project.Status);

            //Calcaulate TotalRevenue Postback
            List<QuotationItemDescription> quotation = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(project.QuotationReferenceId);
            decimal totalAmount = 0;

            foreach (QuotationItemDescription _quotation in quotation)
            {
                totalAmount += (decimal)_quotation.QuotationItemAmount;
            }

            ViewData["TotalRevenue"] = totalAmount.ToString("#,##0.00");
            ViewData["TotalExpenses"] = TotalExpenses.ToString("#,##0.00");

            decimal TotalProfit = totalAmount - TotalExpenses;

            ViewData["TotalProfit"] = TotalProfit.ToString("#,##0.00");

            decimal CheckMargin = 0;
            ViewData["AddClass"] = "No";

            if(totalAmount > 0 && TotalExpenses > 0)
            {
                CheckMargin = (TotalProfit / totalAmount) * 100;

                //Check Margin Color
                if (CheckMargin != 0)
                {
                    if (CheckMargin < 17)
                    {
                        ViewData["AddClass"] = "Yes";
                    }
                }
            }
            else
            {
                if(TotalExpenses != 0)
                {
                    ViewData["AddClass"] = "Yes";
                }
            }

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: View
        public ActionResult View(int id)
        {
            //Get Main ID first
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            //Get Project Item
            Project project = _projectsModel.GetSingle(id);

            if(project == null)
            {
                Session.Add("Result", "danger|Project not found!");
                return RedirectToAction("Index");
            }

            User getUserRole = _usersModel.GetSingle(loginMainID);

            //If not completed, still can edit.
            if (getUserRole.Role != "Super Admin" && getUserRole.Role != "Accounts")
            {
                if (project.Status != "Completed")
                {
                    return RedirectToAction("Edit/" + id);
                }
            }
            else
            {
                if (project.Status != "Completed")
                {
                    return RedirectToAction("Edit/" + id);
                }
            }

            //if super admin, can view
            //else sales only can view own record
            if (getUserRole.Role != "Super Admin" && getUserRole.Role != "Accounts")
            {
                //If the project is not assign to the login user
                if (project.CoordinatorId != loginMainID)
                {
                    Session.Add("Result", "danger|You are not the project coordinator!");
                    return RedirectToAction("Index");
                }
            }

            ViewData["Project"] = project;

            //Get Project Expenses Item
            IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetPersonalAll(project.ID);

            List<ProjectProjectExpensesTable> projectExpensesTable = new List<ProjectProjectExpensesTable>();

            int count = 1;
            foreach(ProjectProjectExpenses _projectExpenses in projectExpenses)
            {
                projectExpensesTable.Add(new ProjectProjectExpensesTable()
                {
                    RowId = count,
                    Description = _projectExpenses.Description,
                    SupplierName = _projectExpenses.SupplierName,
                    InvoiceNo = _projectExpenses.InvoiceNo,
                    Date = string.Format("{0:dd/MM/yyyy}", _projectExpenses.Date),
                    GST = new SelectList(GSTDDL(), "val", "name", _projectExpenses.GST),
                    CostBeforeGST = Convert.ToString(_projectExpenses.Cost),
                    Status = new SelectList(ProjectProjectExpensesStatusDDL(), "val", "name", _projectExpenses.Status),
                });
                count++;
            }

            ViewData["ProjectExpensesTable"] = projectExpensesTable;

            User preparedBy = _usersModel.GetSingle(loginMainID);

            ViewData["PreparedBy"] = preparedBy.Name;

            string fromView = "Yes";

            Dropdown[] quotationDDL = QuotationDDL(fromView);
            ViewData["QuotationDropdown"] = new SelectList(quotationDDL, "val", "name", project.QuotationReferenceId);

            Dropdown[] customerDDL = CustomerDDL(fromView);
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", project.CustomerId);

            Dropdown[] coordinatorDDL = CoordinatorDDL();
            ViewData["CoordinatorDropdown"] = new SelectList(coordinatorDDL, "val", "name", project.CoordinatorId);

            Dropdown[] projectTypeDDL = ProjectTypeDDL();
            ViewData["ProjectTypeDropdown"] = new SelectList(projectTypeDDL, "val", "name", project.ProjectTitle);

            ViewData["PhotosUploadFiles"] = project.PhotoUploads;
            ViewData["FloorPlanUploadsFiles"] = project.FloorPlanUploads;
            ViewData["ThreeDDrawingUploadsFiles"] = project.ThreeDDrawingUploads;
            ViewData["AsBuildDrawingUploadsFiles"] = project.AsBuildDrawingUploads;
            ViewData["SubmissionStageUploadsFiles"] = project.SubmissionStageUploads;
            ViewData["OtherUploadsFiles"] = project.OtherUploads;

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", project.Status);

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Edit
        public ActionResult Edit(int id)
        {
            //Get Main ID first
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            //Get Project Item
            Project project = _projectsModel.GetSingle(id);

            if (project == null)
            {
                Session.Add("Result", "danger|Project not found!");
                return RedirectToAction("Index");
            }

            User getUserRole = _usersModel.GetSingle(loginMainID);
            ViewData["UserRole"] = getUserRole.Role;

            if(getUserRole.Role != "Super Admin" && getUserRole.Role != "Sales")
            {
                //If status is completed, redirect to view page.
                if (project.Status == "Completed")
                {
                    return RedirectToAction("View/" + id);
                }
            }

            //if super admin, can edit
            //else sales only can edit own record
            if(getUserRole.Role != "Super Admin")
            {
                //If the project is not assign to the login user
                if (project.CoordinatorId != loginMainID)
                {
                    Session.Add("Result", "danger|You are not the project coordinator!");
                    return RedirectToAction("Index");
                }
            }

            ViewData["Project"] = project;

            //Get Project Expenses Item
            IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetPersonalAll(project.ID);
            
            List<ProjectProjectExpensesTable> projectExpensesTable = new List<ProjectProjectExpensesTable>();

            ViewData["ProjectExpensesFromDatabase"] = projectExpenses;

            ViewData["ProjectExpensesTable"] = projectExpensesTable;

            User preparedBy = _usersModel.GetSingle(project.PreparedById);

            ViewData["PreparedBy"] = preparedBy.Name;

            Dropdown[] quotationDDL = QuotationDDL();
            ViewData["QuotationDropdown"] = new SelectList(quotationDDL, "val", "name", project.QuotationReferenceId);

            Dropdown[] customerDDL = CustomerDDL(null, project.CustomerId);
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", project.CustomerId);

            Dropdown[] gstDDL = GSTDDL();
            ViewData["GSTDropdown"] = new SelectList(gstDDL, "val", "name", null);

            Dropdown[] coordinatorDDL = CoordinatorDDL();
            ViewData["CoordinatorDropdown"] = new SelectList(coordinatorDDL, "val", "name", project.CoordinatorId);

            Dropdown[] projectTypeDDL = ProjectTypeDDL();
            ViewData["ProjectTypeDropdown"] = new SelectList(projectTypeDDL, "val", "name", project.ProjectTitle);

            ViewData["PhotosUploadFiles"] = project.PhotoUploads;
            ViewData["FloorPlanUploadsFiles"] = project.FloorPlanUploads;
            ViewData["ThreeDDrawingUploadsFiles"] = project.ThreeDDrawingUploads;
            ViewData["AsBuildDrawingUploadsFiles"] = project.AsBuildDrawingUploads;
            ViewData["SubmissionStageUploadsFiles"] = project.SubmissionStageUploads;
            ViewData["OtherUploadsFiles"] = project.OtherUploads;

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", project.Status);

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Edit
        [HttpPost]
        public ActionResult Edit(int id, Project project, FormCollection form)
        {
            //Get Main ID first
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            User getUserRole = _usersModel.GetSingle(loginMainID);

            ViewData["UserRole"] = getUserRole.Role;

            Project oldData = _projectsModel.GetSingle(id);

            //This is disabled field used
            project.ProjectID = oldData.ProjectID;
            project.CompanyName = oldData.CompanyName;
            project.QuotationReferenceId = oldData.QuotationReferenceId;

            string old_PhotoUploads = oldData.PhotoUploads;
            string old_FloorPlanUploads = oldData.FloorPlanUploads;
            string old_ThreeDDrawingUploads = oldData.ThreeDDrawingUploads;
            string old_AsBuildDrawingUploads = oldData.AsBuildDrawingUploads;
            string old_SubmissionStageUploads = oldData.SubmissionStageUploads;
            string old_OtherUploads = oldData.OtherUploads;
            //project.CoordinatorId = oldData.CoordinatorId;

            //Validation Part
            //if (project.QuotationReferenceId == 0)
            //{
            //    ModelState.AddModelError("project.QuotationReferenceId", "Quotation is required!");
            //}

            if (project.CustomerId == 0)
            {
                ModelState.AddModelError("project.CustomerId", "Customer is required!");
            }

            if (project.CoordinatorId == 0)
            {
                ModelState.AddModelError("project.CoordinatorId", "Coordinator is required!");
            }

            if (string.IsNullOrEmpty(project.ProjectTitle))
            {
                ModelState.AddModelError("project.ProjectTitle", "Project Title is required!");
            }

            if (string.IsNullOrEmpty(project.Remarks))
            {
                project.Remarks = null;
            }

            //Project Expenses Validation Part
            List<string> projectExpensesKeys = form.AllKeys.Where(e => e.Contains("ProjectExpensesDescription_")).ToList();

            List<ProjectProjectExpensesTable> projectExpensesTable = new List<ProjectProjectExpensesTable>();

            foreach (string _projectExpensesKeys in projectExpensesKeys)
            {
                string rowId = _projectExpensesKeys.Split('_')[1];

                string Description = form["ProjectExpensesDescription_" + rowId];
                string SupplierName = form["ProjectExpensesSupplierName_" + rowId];
                string InvoiceNo = form["ProjectExpensesInvoiceNo_" + rowId];
                string Date = form["ProjectExpensesDate_" + rowId];
                string GST = form["ProjectExpensesGST_" + rowId];
                string CostBeforeGST = form["ProjectExpensesCostBeforeGST_" + rowId];
                string ProjectExpensesStatus = form["ProjectExpensesStatus_" + rowId];
                string FromDb = form["ProjectExpensesFromDb_" + rowId];

                if (string.IsNullOrEmpty(Description))
                {
                    ModelState.AddModelError("ProjectExpensesDescription_" + rowId, "Description is required!");
                }

                if (string.IsNullOrEmpty(SupplierName))
                {
                    ModelState.AddModelError("ProjectExpensesSupplierName_" + rowId, "Supplier Name is required!");
                }

                if (string.IsNullOrEmpty(InvoiceNo))
                {
                    ModelState.AddModelError("ProjectExpensesInvoiceNo_" + rowId, "Invoice No is required!");
                }

                if (string.IsNullOrEmpty(Date))
                {
                    ModelState.AddModelError("ProjectExpensesDate_" + rowId, "Date is required!");
                }

                if (string.IsNullOrEmpty(GST))
                {
                    ModelState.AddModelError("ProjectExpensesGST_" + rowId, "GST is required!");
                }

                if (string.IsNullOrEmpty(CostBeforeGST))
                {
                    ModelState.AddModelError("ProjectExpensesCostBeforeGST_" + rowId, "Cost Before GST is required!");
                }
                else
                {
                    //Check format
                    //Check is decimal or not
                    bool checkFormat = FormValidationHelper.AmountFormat(CostBeforeGST);

                    if (!checkFormat)
                    {
                        ModelState.AddModelError("ProjectExpensesCostBeforeGST_" + rowId, "Invalid Cost Before GST Format!");
                    }
                }

                projectExpensesTable.Add(new ProjectProjectExpensesTable()
                {
                    RowId = Convert.ToInt32(rowId),
                    Description = Description,
                    SupplierName = SupplierName,
                    InvoiceNo = InvoiceNo,
                    Date = Date,
                    GST = new SelectList(GSTDDL(), "val", "name", GST),
                    CostBeforeGST = CostBeforeGST,
                    Status = new SelectList(ProjectProjectExpensesStatusDDL(), "val", "name", ProjectExpensesStatus),
                });
            }

            //Model is valid
            if(ModelState.IsValid)
            {

                //For store Project Expenses Item
                ProjectProjectExpenses projectProjectExpensesRecord = new ProjectProjectExpenses();

                //Check Status

                if(project.Status == "Completed")
                {
                    project.CompletedDate = DateTime.Now;
                }
                else
                {
                    project.CompletedDate = null;
                }

                bool result = _projectsModel.Update(oldData.ID, project);

                if(result)
                {

                    string userRole = Session["UserGroup"].ToString();
                    if (userRole == "Sales")
                    {
                        userRole += " " + Session["Position"].ToString();
                    }
                    string tableAffected = "Projects";
                    string description = userRole + " [" + Session["LoginID"].ToString() + "] Edited Project [" + oldData.ProjectID + "]";

                    bool create_log = AuditLogHelper.WriteAuditLog(loginMainID, userRole, tableAffected, description);

                    //Move Project Item Uploads File Check
                    //Check Photo Uploads
                    if (!string.IsNullOrEmpty(project.PhotoUploads))
                    {
                        if (project.PhotoUploads != old_PhotoUploads)
                        {
                            string[] PhotoUploads = project.PhotoUploads.Split(',');

                            foreach (string file in PhotoUploads)
                            {
                                string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                                string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["ProjectPhotoUploadsFolder"].ToString()), file);

                                System.IO.File.Move(sourceFile, destinationFile);
                            }
                        }
                    }

                    //Check Floor Plans Uploads
                    if (!string.IsNullOrEmpty(project.FloorPlanUploads))
                    {
                        if (project.FloorPlanUploads != old_FloorPlanUploads)
                        {
                            string[] FloorPlanUploads = project.FloorPlanUploads.Split(',');

                            foreach (string file in FloorPlanUploads)
                            {
                                string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                                string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["ProjectFloorPlanUploadsFolder"].ToString()), file);

                                System.IO.File.Move(sourceFile, destinationFile);
                            }
                        }
                    }

                    //Check 3D Drawings Uploads
                    if (!string.IsNullOrEmpty(project.ThreeDDrawingUploads))
                    {
                        if (project.ThreeDDrawingUploads != old_ThreeDDrawingUploads)
                        {
                            string[] ThreeDDrawingUploads = project.ThreeDDrawingUploads.Split(',');

                            foreach (string file in ThreeDDrawingUploads)
                            {
                                string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                                string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["ProjectThreeDDrawingUploadsFolder"].ToString()), file);

                                System.IO.File.Move(sourceFile, destinationFile);
                            }
                        }
                    }

                    //Check As Build Drawing Uploads
                    if (!string.IsNullOrEmpty(project.AsBuildDrawingUploads))
                    {
                        if (project.AsBuildDrawingUploads != old_AsBuildDrawingUploads)
                        {
                            string[] AsBuildDrawingUploads = project.AsBuildDrawingUploads.Split(',');

                            foreach (string file in AsBuildDrawingUploads)
                            {
                                string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                                string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["ProjectAsBuildDrawingUploadsFolder"].ToString()), file);

                                System.IO.File.Move(sourceFile, destinationFile);
                            }
                        }
                    }

                    //Check As Submission Stage Uploads
                    if (!string.IsNullOrEmpty(project.SubmissionStageUploads))
                    {
                        if (project.SubmissionStageUploads != old_SubmissionStageUploads)
                        {
                            string[] SubmissionStageUploads = project.SubmissionStageUploads.Split(',');

                            foreach (string file in SubmissionStageUploads)
                            {
                                string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                                string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["ProjectSubmissionStageUploadsFolder"].ToString()), file);

                                System.IO.File.Move(sourceFile, destinationFile);
                            }
                        }
                    }

                    //Check Others Uploads
                    if (!string.IsNullOrEmpty(project.OtherUploads))
                    {
                        if (project.OtherUploads != old_OtherUploads)
                        {
                            string[] OtherUploads = project.OtherUploads.Split(',');

                            foreach (string file in OtherUploads)
                            {
                                string sourceFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), file);
                                string destinationFile = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["ProjectOtherUploadsFolder"].ToString()), file);

                                System.IO.File.Move(sourceFile, destinationFile);
                            }
                        }
                    }

                    if (ViewData["UserRole"].ToString() != "Sales")
                    {
                        //Remove ProjectExpensesTable before add new
                        IList<ProjectProjectExpenses> RemoveProjectExpenses = _projectExpensesModel.GetPersonalAll(oldData.ID);

                        foreach (ProjectProjectExpenses _projectExpenses in RemoveProjectExpenses)
                        {
                            bool remove_project_expenses = _projectExpensesModel.Delete(_projectExpenses.ID);
                        }
                    }

                    bool hasProjectExpenses = false;

                    foreach (ProjectProjectExpensesTable _projectExpensesTable in projectExpensesTable)
                    {

                        projectProjectExpensesRecord.ProjectId = oldData.ID;
                        projectProjectExpensesRecord.Description = _projectExpensesTable.Description;
                        projectProjectExpensesRecord.SupplierName = _projectExpensesTable.SupplierName;
                        projectProjectExpensesRecord.InvoiceNo = _projectExpensesTable.InvoiceNo;
                        projectProjectExpensesRecord.Date = Convert.ToDateTime(_projectExpensesTable.Date);
                        projectProjectExpensesRecord.GST = Convert.ToString(_projectExpensesTable.GST.SelectedValue);
                        projectProjectExpensesRecord.Cost = Convert.ToDecimal(_projectExpensesTable.CostBeforeGST);
                        projectProjectExpensesRecord.Status = Convert.ToString(_projectExpensesTable.Status.SelectedValue);

                        bool add_project_projectExpenses = _projectExpensesModel.Add(projectProjectExpensesRecord);

                        if (add_project_projectExpenses)
                        {
                            if (!hasProjectExpenses)
                            {
                                hasProjectExpenses = true;
                            }
                        }
                    }

                    if (hasProjectExpenses)
                    {
                        userRole = Session["UserGroup"].ToString();
                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }
                        tableAffected = "ProjectProjectExpenses";
                        description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Project Project Expenses [" + oldData.ProjectID + "]";

                        bool projectProjectExpenses_create_log = AuditLogHelper.WriteAuditLog(loginMainID, userRole, tableAffected, description);
                    }

                    Session.Add("Result", "success|" + oldData.ProjectID + " has been successfully edited!");
                    return RedirectToAction("Index");
                }
                else
                {
                    Session.Add("Result", "danger|Something wrong when edit project!");
                }
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }


            //if super admin, dont need add into default database field
            if(getUserRole.Role != "Super Admin")
            {
                //Get Project Expenses Item
                IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetPersonalAll(oldData.ID);

                ViewData["ProjectExpensesFromDatabase"] = projectExpenses;
            }

            ViewData["ProjectExpensesTable"] = projectExpensesTable; //either super admin or sales will show all

            Dropdown[] quotationDDL = QuotationDDL();
            ViewData["QuotationDropdown"] = new SelectList(quotationDDL, "val", "name", project.QuotationReferenceId);

            Dropdown[] customerDDL = CustomerDDL(null, oldData.CustomerId);
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", project.CustomerId);

            Dropdown[] coordinatorDDL = CoordinatorDDL();
            ViewData["CoordinatorDropdown"] = new SelectList(coordinatorDDL, "val", "name", project.CoordinatorId);

            Dropdown[] projectTypeDDL = ProjectTypeDDL();
            ViewData["ProjectTypeDropdown"] = new SelectList(projectTypeDDL, "val", "name", project.ProjectTitle);

            ViewData["PhotosUploadFiles"] = project.PhotoUploads;
            ViewData["FloorPlanUploadsFiles"] = project.FloorPlanUploads;
            ViewData["ThreeDDrawingUploadsFiles"] = project.ThreeDDrawingUploads;
            ViewData["AsBuildDrawingUploadsFiles"] = project.AsBuildDrawingUploads;
            ViewData["SubmissionStageUploadsFiles"] = project.SubmissionStageUploads;
            ViewData["OtherUploadsFiles"] = project.OtherUploads;

            Dropdown[] gstDDL = GSTDDL();
            ViewData["GSTDropdown"] = new SelectList(gstDDL, "val", "name", null);

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", project.Status);

            User preparedBy = _usersModel.GetSingle(oldData.PreparedById);

            ViewData["PreparedBy"] = preparedBy.Name;

            ViewData["Project"] = project;

            //ViewData["ProjectExpensesFromDatabase"] = projectExpenses;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Delete
        public ActionResult Delete(int id)
        {

            int loginMainID = Convert.ToInt32(Session["MainID"]);

            User user = _usersModel.GetSingle(loginMainID);

            if(user.Role != "Super Admin")
            {
                Session.Add("Result", "danger|You are not allow to delete this project!");
                return RedirectToAction("Index");
            }

            Project project = _projectsModel.GetSingle(id);
            bool result = _projectsModel.Delete(id);

            if (result)
            {

                int MainID = Convert.ToInt32(Session["MainID"]);
                string userRole = "User";
                string tableAffected = "Projects";
                string description = userRole + " [" + Session["LoginID"].ToString() + "] Delete Project [" + project.ProjectID + "]";

                bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                Session.Add("Result", "success|" + project.ProjectID + " successfully deleted!");
            }
            else
            {
                Session.Add("Result", "danger|Project record not found!");
            }

            return RedirectToAction("Index");
        }

        //GET: Complete
        public ActionResult Complete(int id, string fromTaskList)
        {

            int loginMainID = Convert.ToInt32(Session["MainID"]);

            User user = _usersModel.GetSingle(loginMainID);

            if (user.Role != "Super Admin")
            {
                Session.Add("Result", "danger|You are not allow to mark this project to complete!");
                return RedirectToAction("Index");
            }

            Project project = _projectsModel.GetSingle(id);
            bool result = _projectsModel.Complete(id);

            if (result)
            {

                int MainID = Convert.ToInt32(Session["MainID"]);
                string userRole = Session["UserGroup"].ToString();

                if (userRole == "Sales")
                {
                    userRole += " " + Session["Position"].ToString();
                }

                string tableAffected = "Projects";
                string description = userRole + " [" + Session["LoginID"].ToString() + "] Mark Project [" + project.ProjectID + "] as Completed";

                bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                Session.Add("Result", "success|" + project.ProjectID + " mark as Completed!");
            }
            else
            {
                Session.Add("Result", "danger|Project record not found!");
            }

            if(fromTaskList != null)
            {
                return RedirectToAction("Index","TaskList");
            }
            else
            {
                return RedirectToAction("Index");
            }

            
        }

        //GET: GetProjectExpenses
        public ActionResult GetProjectExpenses(int id)
        {
            ViewData["RowId"] = id;

            Dropdown[] gstDDL = GSTDDL();
            ViewData["GSTDropdown"] = new SelectList(gstDDL, "val", "name", null);

            Dropdown[] projectExpensesStatusDDL = ProjectProjectExpensesStatusDDL();
            ViewData["ProjectExpensesStatusDropdown"] = new SelectList(projectExpensesStatusDDL, "val", "name", null);

            return View();
        }

        //GET: Project Estimator
        public ActionResult ProjectEstimator(FormCollection form)
        {

            Dropdown[] projectEstimatorProjectIdDDL = ProjectEstimatorProjectIdDDL();
            ViewData["ProjectEstimatorProjectIdDropdown"] = new SelectList(projectEstimatorProjectIdDDL, "val", "name", null);

            ViewData["QuotationItemDescriptionTable"] = null;

            ViewData["NoSummaryItem"] = "Yes";

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Project Estimator
        [HttpPost]
        public ActionResult ProjectEstimator(Project project, FormCollection form)
        {

            int MainID = Convert.ToInt32(Session["MainID"]);

            string ProjectId = null;
            int GetID = 0;

            ViewData["NoSummaryItem"] = "No";

            if (string.IsNullOrEmpty(form["ProjectEstimatorProjectId"]))
            {
                ModelState.AddModelError("ProjectEstimatorProjectId", "Project ID is required!");
                ViewData["NoSummaryItem"] = "Yes";
            }
            else
            {
                ProjectId = form["ProjectEstimatorProjectId"].ToString();
                GetID = Convert.ToInt32(ProjectId);
            }

            Project getProject = _projectsModel.GetSingle(GetID);

            //Item Description Validatin Part
            List<string> ItemDescriptionKeys = form.AllKeys.Where(e => e.Contains("BigTableItemName-")).ToList();
            List<string> ItemDescriptionInsideKeys = form.AllKeys.Where(e => e.Contains("QuotationItemDescriptionDescription_")).ToList();

            //For PostBack Use
            //List<QuotationItemDescriptionTable> quotationItemDescriptionTable = new List<QuotationItemDescriptionTable>();
            List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();

            //Summary Costing Table
            List<string> CostingSummaryKeys = form.AllKeys.Where(e => e.Contains("SummaryItemMain-")).ToList();
            List<ProjectEstimatorCostingSummaryTable> projectEstimatorCostingSummaryTable = new List<ProjectEstimatorCostingSummaryTable>();

            decimal FinalTotalAmount = 0;
            decimal FinalTotalProjectedCost = 0;
            decimal FinalTotalAdjCost = 0;
            decimal FinalTotalAdjProfit = 0;

            foreach (string _CostingSummaryKeys in CostingSummaryKeys)
            {
                string rowId = _CostingSummaryKeys.Split('-')[1];
                string CategoryName = form["QuotationItemName-" + rowId];
                string TotalAmount = form["QuotationTotalAmount-" + rowId];
                string TotalProjectedCost = form["QuotationTotalProjectedCost-" + rowId];
                string TotalAdjCost = form["QuotationTotalAdjCost2-" + rowId];
                string TotalAdjProfit = form["QuotationTotalAdjProfit2-" + rowId];

                FinalTotalAmount += Convert.ToDecimal(TotalAmount);
                FinalTotalProjectedCost += Convert.ToDecimal(TotalProjectedCost);
                FinalTotalAdjCost += Convert.ToDecimal(TotalAdjCost);
                FinalTotalAdjProfit += Convert.ToDecimal(TotalAdjProfit);

                projectEstimatorCostingSummaryTable.Add(new ProjectEstimatorCostingSummaryTable()
                {
                    CategoryName = CategoryName,
                    Amount = TotalAmount,
                    ProjectedCost = TotalProjectedCost,
                    AdjCost = TotalAdjCost,
                    AdjProfit = TotalAdjProfit
                });
            }
            ViewData["FinalTotalAmount"] = FinalTotalAmount;
            ViewData["FinalTotalProjectedCost"] = FinalTotalProjectedCost;
            ViewData["FinalTotalAdjCost"] = FinalTotalAdjCost;
            ViewData["FinalTotalAdjProfit"] = FinalTotalAdjProfit;

            foreach (string _ItemDescriptionKeys in ItemDescriptionKeys)
            {
                string rowId = _ItemDescriptionKeys.Split('-')[1];
                string ItemName = form["BigTableItemName-" + rowId];
                string IsBasic = form["BigTableItemNameFromDB-" + rowId];

                //Check From here
                List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                //Foreach Item
                foreach (string _ItemDescriptionInsideKeys in ItemDescriptionInsideKeys)
                {
                    string insideRowId = _ItemDescriptionInsideKeys.Split('_')[1];
                    string insideRowId2 = _ItemDescriptionInsideKeys.Split('_')[2];

                    //if same, then start validation. (Same Table Item)
                    if (string.IsNullOrEmpty(ItemName))
                    {
                        ModelState.AddModelError("BigTableItemName" + insideRowId, "Item Name cannot be empty!");
                    }

                    if (insideRowId == rowId)
                    {

                        string Description = form["QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2];
                        string Amount = form["QuotationItemDescriptionAmount2_" + insideRowId + "_" + insideRowId2];
                        string ProjectedCost = form["QuotationItemDescriptionEstCost2_" + insideRowId + "_" + insideRowId2];
                        string AdjCost = form["QuotationItemDescriptionAdjCost_" + insideRowId + "_" + insideRowId2];
                        string Profit = form["QuotationItemDescriptionProfit2_" + insideRowId + "_" + insideRowId2];

                        if (string.IsNullOrEmpty(Description))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionDescription_" + insideRowId + "_" + insideRowId2, "Item Description cannot be empty!");
                        }

                        if (string.IsNullOrEmpty(ProjectedCost))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionEstCost_" + insideRowId + "_" + insideRowId2, "Est Cost cannot be empty!");
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(ProjectedCost);

                            if (!checkFormat)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionEstCost_" + insideRowId + "_" + insideRowId2, "Invalid Est Cost Format!");
                            }
                        }

                        if (string.IsNullOrEmpty(Amount))
                        {
                            Amount = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Amount);

                            if (checkFormat)
                            {
                                Amount = Convert.ToDecimal(Amount).ToString("#,##0.00");
                            }
                        }

                        if(string.IsNullOrEmpty(AdjCost))
                        {
                            ModelState.AddModelError("QuotationItemDescriptionAdjCost_" + insideRowId + "_" + insideRowId2, "Adj Cost is required!");
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(AdjCost);

                            if (!checkFormat)
                            {
                                ModelState.AddModelError("QuotationItemDescriptionAdjCost_" + insideRowId + "_" + insideRowId2, "Invalid Est Cost Format!");
                            }
                        }

                        if (string.IsNullOrEmpty(Profit))
                        {
                            Profit = null;
                        }
                        else
                        {
                            //Check is decimal or not
                            bool checkFormat = FormValidationHelper.AmountFormat(Profit);

                            if (checkFormat)
                            {
                                Profit = Convert.ToDecimal(Profit).ToString("#,##0.00");
                            }
                        }

                        ItemDescriptionRows.Add(new ItemDescriptionRow()
                        {
                            RowId = Convert.ToInt32(insideRowId2),
                            Description = Description,
                            Qty = null,
                            Unit = null,
                            Rate = null,
                            Amount = Amount,
                            Est_Cost = ProjectedCost,
                            AdjCost = AdjCost,
                            Est_Profit = Profit,
                            Remarks = null,
                            Mgr_Comments = null
                        });

                    }
                }
                bool ConvertIsBasic = true;

                if (IsBasic == "True")
                {
                    ConvertIsBasic = Convert.ToBoolean("True");
                }
                else
                {
                    ConvertIsBasic = Convert.ToBoolean("False");
                }

                quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                {
                    TableID = Convert.ToInt32(rowId),
                    CategoryName = ItemName,
                    IsBasic = Convert.ToBoolean(ConvertIsBasic),
                    ItemDescriptionRows = ItemDescriptionRows
                });
            }

            if (ModelState.IsValid)
            {
                ProjectId = form["ProjectEstimatorProjectId"].ToString();

                Project getQuotationId = _projectsModel.GetSingle(Convert.ToInt32(ProjectId));

                //Store into Project Estimator there
                //Once Quotation Confirm, the data cant be edited

                //Check Project Estimator have same Project ID or not
                //if have, remove and add
                //if not, add

                //Remove All QuotationItemDescription Item
                //Get Database Quotation Items Description based on quotation ID
                IList<ProjectEstimator> getProjectEstimator = _projectEstimatorsModel.GetAllSameProjectID(Convert.ToInt32(ProjectId));

                if(getProjectEstimator != null)
                {
                    foreach (ProjectEstimator _getProjectEstimator in getProjectEstimator)
                    {
                        int projectEstimatorID = _getProjectEstimator.ID;
                        bool delete_project_estimator = _projectEstimatorsModel.Delete(projectEstimatorID);
                    }
                }

                //For store Project Estimator Items
                ProjectEstimator projectEstimatorRecord = new ProjectEstimator();

                bool hasItemDescription = false;

                //Loop BigTable
                foreach (ItemDescriptionBigTable _quotationItemDescriptionBigTable in quotationItemDescriptionBigTable)
                {
                    foreach (ItemDescriptionRow _quotationItemDesriptionRow in _quotationItemDescriptionBigTable.ItemDescriptionRows)
                    {

                        projectEstimatorRecord.ProjectId = Convert.ToInt32(ProjectId);
                        projectEstimatorRecord.ProjectExpensesCategoryName = _quotationItemDescriptionBigTable.CategoryName;
                        projectEstimatorRecord.ProjectExpensesDescription = _quotationItemDesriptionRow.Description;
                        projectEstimatorRecord.ProjectExpensesAmount = Convert.ToDecimal(_quotationItemDesriptionRow.Amount);
                        projectEstimatorRecord.ProjectExpensesProjectedCost = Convert.ToDecimal(_quotationItemDesriptionRow.Est_Cost);
                        projectEstimatorRecord.ProjectExpensesAdjCost = Convert.ToDecimal(_quotationItemDesriptionRow.AdjCost);
                        projectEstimatorRecord.ProjectExpensesProfit = Convert.ToDecimal(_quotationItemDesriptionRow.Est_Profit);

                        bool add_project_estimator = _projectEstimatorsModel.Add(projectEstimatorRecord);

                        hasItemDescription = true;
                    }
                }

                if (hasItemDescription)
                {
                    string userRole = Session["UserGroup"].ToString();
                    if (userRole == "Sales")
                    {
                        userRole += " " + Session["Position"].ToString();
                    }
                    string tableAffected = "ProjectEstimators";
                    string description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Project Estimators [" + getProject.ProjectID + "]";

                    bool itemDescription_create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                }

                Session.Add("Result", "success|Project Estimator has been successfully edited!");
                return RedirectToAction("Index");
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            ViewData["QuotationItemDescription"] = quotationItemDescriptionBigTable;
            ViewData["projectEstimatorCostingSummaryTable"] = projectEstimatorCostingSummaryTable;

            Dropdown[] projectEstimatorProjectIdDDL = ProjectEstimatorProjectIdDDL();
            ViewData["ProjectEstimatorProjectIdDropdown"] = new SelectList(projectEstimatorProjectIdDDL, "val", "name", Convert.ToInt32(ProjectId));

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: QuotationItemDescriptionDetai
        public string GetQuotationItemDescriptionDetail(int id)
        {
            //Get Quotation Item Description

            Project project = _projectsModel.GetSingle(id);

            ProjectEstimatorTable projectEstimator = new ProjectEstimatorTable();

            if(project != null)
            {
                projectEstimator.Result = true;

                Quotation quotation = _quotationsModel.GetSingle(project.QuotationReferenceId);

                projectEstimator.QuotationItemDescriptionView = JsonConvert.SerializeObject(GetQuotationItemDescriptionProjectExpenses(project.QuotationReferenceId, project.ID));
                projectEstimator.QuotationCostingSummaryView = JsonConvert.SerializeObject(GetQuotationItemCostingSummary(project.QuotationReferenceId, project.ID));
            }
            else
            {
                projectEstimator.Result = false;
                projectEstimator.ErrorMessage = "Project record not found!";
            }

            return JsonConvert.SerializeObject(projectEstimator);
        }

        //GET: ProjectEstimator (Loop Costing Summary)
        public string GetQuotationItemCostingSummary(int id, int ProjectID)
        {
            //New Get Quotation Item Description
            List<QuotationItemDescription> quotationItemDescription = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(id);
            List<ProjectEstimatorCostingSummaryTable> projectEstimatorCostingSummaryTable = new List<ProjectEstimatorCostingSummaryTable>();

            IList<ProjectEstimator> projectEstimator = _projectEstimatorsModel.GetAllSameProjectID(ProjectID);

            //Start
            int resetNumber = 1;

            decimal FinalTotalAmount = 0;
            decimal FinalTotalProjectedCost = 0;
            decimal FinalTotalAdjCost = 0;
            decimal FinalTotalAdjProfit = 0;

            foreach (QuotationItemDescription _quotationItemDescription in quotationItemDescription)//14 items
            {
                string ItemName = _quotationItemDescription.CategoryName;
                int ItemQty = (int)_quotationItemDescription.QuotationItemQty;
                decimal ItemRate = (decimal)_quotationItemDescription.QuotationItemRate;
                decimal ItemEstCost = (decimal)_quotationItemDescription.QuotationItemEstCost;

                List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                if (ItemQty == null)
                {
                    ItemQty = 0;
                }

                if (ItemRate == null)
                {
                    ItemRate = 0;
                }

                if (ItemEstCost == null)
                {
                    ItemEstCost = 0;
                }

                int RowId = resetNumber;
                string CategoryName = _quotationItemDescription.CategoryName;
                decimal TotalAmount = 0;
                decimal TotalProjectedCost = 0;
                decimal TotalAdjCost = 0;
                decimal TotalAdjProfit = 0;


                //Check if have ProjectEstimator Data, loop Project Estimator Table
                //Else Loop Item Description Table and AdjCost = 0

                if (projectEstimator.Count != 0)
                {
                    foreach (ProjectEstimator _projectEstimator in projectEstimator)
                    {
                        if (_projectEstimator.ProjectExpensesCategoryName == _quotationItemDescription.CategoryName)
                        {
                            TotalAmount += _projectEstimator.ProjectExpensesAmount;
                            TotalProjectedCost += _projectEstimator.ProjectExpensesProjectedCost;
                            TotalAdjCost += _projectEstimator.ProjectExpensesAdjCost;
                            TotalAdjProfit += (_projectEstimator.ProjectExpensesAmount - _projectEstimator.ProjectExpensesAdjCost);
                        }
                    }
                }
                else
                {
                    foreach (QuotationItemDescription quotationItemDescription2 in quotationItemDescription)
                    {
                        if (quotationItemDescription2.CategoryName == _quotationItemDescription.CategoryName)
                        {
                            TotalAmount += (decimal)quotationItemDescription2.QuotationItemAmount;
                            TotalProjectedCost += (decimal)quotationItemDescription2.QuotationItemEstCost;
                            TotalAdjProfit += (decimal)quotationItemDescription2.QuotationItemAmount;
                        }
                    }
                }

                if (projectEstimatorCostingSummaryTable.Count == 0)
                {
                    projectEstimatorCostingSummaryTable.Add(new ProjectEstimatorCostingSummaryTable()
                    {
                        RowId = resetNumber,
                        CategoryName = CategoryName,
                        Amount = TotalAmount.ToString("#,##0.00"),
                        ProjectedCost = TotalProjectedCost.ToString("#,##0.00"),
                        AdjCost = TotalAdjCost.ToString("#,##0.00"),
                        AdjProfit = TotalAdjProfit.ToString("#,##0.00")
                    });

                    FinalTotalAmount += TotalAmount;
                    FinalTotalProjectedCost += TotalProjectedCost;
                    FinalTotalAdjCost += TotalAdjCost;
                    FinalTotalAdjProfit += TotalAdjProfit;
                }
                else
                {
                    //if same
                    if (projectEstimatorCostingSummaryTable.Where(e => e.CategoryName == _quotationItemDescription.CategoryName).FirstOrDefault() == null)
                    {
                        projectEstimatorCostingSummaryTable.Add(new ProjectEstimatorCostingSummaryTable()
                        {
                            RowId = resetNumber,
                            CategoryName = CategoryName,
                            Amount = TotalAmount.ToString("#,##0.00"),
                            ProjectedCost = TotalProjectedCost.ToString("#,##0.00"),
                            AdjCost = TotalAdjCost.ToString("#,##0.00"),
                            AdjProfit = TotalAdjProfit.ToString("#,##0.00")
                        });

                        FinalTotalAmount += TotalAmount;
                        FinalTotalProjectedCost += TotalProjectedCost;
                        FinalTotalAdjCost += TotalAdjCost;
                        FinalTotalAdjProfit += TotalAdjProfit;
                    }
                }
                resetNumber++;
                //End
            }

            ViewData["FinalTotalAmount"] = FinalTotalAmount.ToString("#,##0.00");
            ViewData["FinalTotalProjectedCost"] = FinalTotalProjectedCost.ToString("#,##0.00");
            ViewData["FinalTotalAdjCost"] = FinalTotalAdjCost.ToString("#,##0.00");
            ViewData["FinalTotalAdjProfit"] = FinalTotalAdjProfit.ToString("#,##0.00");

            ViewData["projectEstimatorCostingSummaryTable"] = projectEstimatorCostingSummaryTable;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, "GetQuotationItemCostingSummary");
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        //GET: ProjectEstimator (Loop Quotation Item Description)
        public string GetQuotationItemDescriptionProjectExpenses(int id, int ProjectID)
        {

            //New Get Quotation Item Description
            List<QuotationItemDescription> quotationItemDescription = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(id);
            List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();

            IList<ProjectEstimator> projectEstimator = _projectEstimatorsModel.GetAllSameProjectID(ProjectID);

            int countItemDescription = 1;
            int resetNumber = 1;

            //Test 
            foreach(QuotationItemDescription _quotationItemDescription in quotationItemDescription)
            {
                ProjectEstimator getExistingItem = _projectEstimatorsModel.GetExistingProjectExpensesItem(ProjectID, _quotationItemDescription.CategoryName, _quotationItemDescription.QuotationItem_Description);

                List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                string AdjCost = null;
                decimal Profit = (decimal)_quotationItemDescription.QuotationItemAmount;

                if(getExistingItem != null)
                {
                    AdjCost = getExistingItem.ProjectExpensesAdjCost.ToString("#,##0.00");
                    Profit = (decimal)_quotationItemDescription.QuotationItemAmount - getExistingItem.ProjectExpensesAdjCost;
                }

                if (quotationItemDescriptionBigTable.Count == 0)
                {
                    
                    ItemDescriptionRows.Add(new ItemDescriptionRow()
                    {
                        RowId = resetNumber,
                        Description = _quotationItemDescription.QuotationItem_Description,
                        Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                        Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                        AdjCost = AdjCost,
                        Est_Profit = Profit.ToString("#,##0.00")
                    });

                    quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                    {
                        TableID = countItemDescription,
                        CategoryName = _quotationItemDescription.CategoryName,
                        ItemDescriptionRows = ItemDescriptionRows
                    });
                    countItemDescription++;
                    //resetNumber++;
                }
                else
                {
                    if (quotationItemDescriptionBigTable.Where(e => e.CategoryName == _quotationItemDescription.CategoryName).FirstOrDefault() != null)
                    {
                        foreach (ItemDescriptionBigTable _loopBigTable in quotationItemDescriptionBigTable)
                        {
                            if (_loopBigTable.CategoryName == _quotationItemDescription.CategoryName)
                            {
                                _loopBigTable.ItemDescriptionRows.Add(new ItemDescriptionRow()
                                {
                                    RowId = resetNumber,
                                    Description = _quotationItemDescription.QuotationItem_Description,
                                    Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                                    Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                                    AdjCost = AdjCost,
                                    Est_Profit = Profit.ToString("#,##0.00")
                                });
                                break;
                            }
                        }
                    }
                    else
                    {
                        resetNumber = 1;
                        ItemDescriptionRows.Add(new ItemDescriptionRow()
                        {
                            RowId = resetNumber,
                            Description = _quotationItemDescription.QuotationItem_Description,
                            Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                            Est_Cost = Convert.ToDecimal(_quotationItemDescription.QuotationItemEstCost).ToString("#,##0.00"),
                            AdjCost = AdjCost,
                            Est_Profit = Profit.ToString("#,##0.00")
                        });

                        quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                        {
                            TableID = countItemDescription,
                            CategoryName = _quotationItemDescription.CategoryName,
                            ItemDescriptionRows = ItemDescriptionRows
                        });
                        countItemDescription++;
                    }
                }
                resetNumber++;
            }

            ViewData["QuotationItemDescription"] = quotationItemDescriptionBigTable;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, "GetQuotationItemDescriptionProjectExpenses");
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        //GET: GetCustomerInfo
        public string GetCustomerInfo(int id)
        {
            string result = "";

            Customer getCustomerInfo = _customersModel.GetSingle(id);

            string item = "\"ContactPerson\": \"" + getCustomerInfo.ContactPerson + "\", \"Address\": \"" + getCustomerInfo.Address + "\", \"Tel\": \"" + getCustomerInfo.Tel + "\", \"Fax\": \"" + getCustomerInfo.Fax + "\"";
            result += "[{" + item + "}]";

            return result;
        }

        //GET: GetCustomerID
        public string GetCustomerID(int id)
        {
            string result = "";

            Quotation getQuotationCustomerID = _quotationsModel.GetSingle(id);

            if (getQuotationCustomerID != null)
            {
                result = getQuotationCustomerID.CustomerId.ToString();
            }

            return result;
        }

        //GET: GetCoordinatorInfo
        public string GetCoordinatorInfo(int id)
        {
            string result = null;
            string item = null;

            User getUserInfo = _usersModel.GetSingle(id);

            item = "\"Email\": \"" + getUserInfo.Email + "\", \"Tel\": \"" + getUserInfo.Tel + "\"";

            result = "[{" + item + "}]";

            return result;
        }

        //GET: GetPaymentSummary
        public ActionResult GetPaymentSummaryProject(int id)
        {

            IList<Invoice> invoice = _invoicesModel.getAllProjectPaymentSummary(id);

            ViewData["Invoice"] = null;

            if (invoice.Count > 0)
            {
                ViewData["Invoice"] = invoice;
            }

            return View();
        }

        //Quotation Dropdown
        public Dropdown[] QuotationDDL(string fromView = null)
        {

            int loginMainID = Convert.ToInt32(Session["MainID"]);

            User user = _usersModel.GetSingle(loginMainID);

            if (user.Role == "Super Admin")
            {
                IList<Quotation> ApprovedQuotation = null;

                if(!string.IsNullOrEmpty(fromView))
                {
                    //this is from view
                    ApprovedQuotation = _quotationsModel.GetAllLatestQuotationID().ToList();
                }
                else
                {
                    ApprovedQuotation = _quotationsModel.GetAllLatestQuotationID().Where(e => e.Status == "Confirmed" && e.IsDeleted != "Y").GroupBy(e => e.PreviousQuotationId).Select(eGroup => eGroup.OrderByDescending(e => e.CreatedOn).FirstOrDefault()).OrderByDescending(e => e.CreatedOn).ToList();
                }

                Dropdown[] ddl = new Dropdown[ApprovedQuotation.Count];

                int count = 0;

                foreach (Quotation _ApprovedQuotation in ApprovedQuotation)
                {
                    ddl[count] = new Dropdown { name = _ApprovedQuotation.QuotationID, val = Convert.ToString(_ApprovedQuotation.ID) };
                    count++;
                }

                return ddl;
            }
            else
            {

                IList<Quotation> ApprovedQuotation = null;

                if (!string.IsNullOrEmpty(fromView))
                {
                    ApprovedQuotation = _quotationsModel.GetAll().ToList();
                }
                else
                {
                    ApprovedQuotation = _quotationsModel.GetAll().Where(e => e.Status == "Confirmed" && e.SalesPersonId == loginMainID && e.IsDeleted != "Y").GroupBy(e => e.PreviousQuotationId).Select(eGroup => eGroup.OrderByDescending(e => e.CreatedOn).FirstOrDefault()).OrderByDescending(e => e.CreatedOn).ToList();
                }

                Dropdown[] ddl = new Dropdown[ApprovedQuotation.Count];

                int count = 0;

                foreach (Quotation _ApprovedQuotation in ApprovedQuotation)
                {
                    ddl[count] = new Dropdown { name = _ApprovedQuotation.QuotationID, val = Convert.ToString(_ApprovedQuotation.ID) };
                    count++;
                }

                return ddl;
            }

        }

        //Customer Dropdown
        public Dropdown[] CustomerDDL(string fromView = null, int SelectedCustomer = 0)
        {
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            User user = _usersModel.GetSingle(loginMainID);

            if (user.Role == "Super Admin")
            {

                IList<Customer> customer = _customersModel.GetAll();

                Dropdown[] ddl = new Dropdown[customer.Count + 1];

                int count = 1;

                foreach (Customer _customer in customer)
                {
                    if (string.IsNullOrEmpty(_customer.CompanyName))
                    {
                        ddl[count] = new Dropdown { name = _customer.CustomerID + " - " + _customer.ContactPerson, val = Convert.ToString(_customer.ID) };
                    }
                    else
                    {
                        ddl[count] = new Dropdown { name = _customer.CustomerID + " - " + _customer.CompanyName, val = Convert.ToString(_customer.ID) };
                    }

                    count++;
                }

                return ddl;
            }
            else
            {

                if (!string.IsNullOrEmpty(fromView))
                {
                    IList<Customer> customer = _customersModel.GetAll();

                    Dropdown[] ddl = new Dropdown[customer.Count];

                    int count = 0;

                    foreach (Customer _customer in customer)
                    {
                        if (string.IsNullOrEmpty(_customer.CompanyName))
                        {
                            ddl[count] = new Dropdown { name = _customer.CustomerID + " - " + _customer.ContactPerson, val = Convert.ToString(_customer.ID) };
                        }
                        else
                        {
                            ddl[count] = new Dropdown { name = _customer.CustomerID + " - " + _customer.CompanyName, val = Convert.ToString(_customer.ID) };
                        }

                        count++;
                    }

                    return ddl;
                }
                else
                {
                    IList<Customer> customer = _customersModel.GetAll().Where(e => e.AssignToId == loginMainID || e.UplineId == loginMainID).ToList();
                    
                    int size = customer.Count;

                    Customer getSelectedCustomer = null;

                    if (SelectedCustomer > 0)
                    {
                        size++;
                        getSelectedCustomer = _customersModel.GetSingle(SelectedCustomer);
                    }

                    Dropdown[] ddl = new Dropdown[size + 1];

                    int count = 1;
                    
                    if(SelectedCustomer > 0)
                    {
                        ddl[count] = new Dropdown { name = getSelectedCustomer.CustomerID + " - " + getSelectedCustomer.ContactPerson, val = Convert.ToString(getSelectedCustomer.ID) };
                        count++;

                        customer = customer.Where(c => c.ID != getSelectedCustomer.ID).ToList();
                    }

                    foreach (Customer _customer in customer)
                    {
                        if (string.IsNullOrEmpty(_customer.CompanyName))
                        {
                            ddl[count] = new Dropdown { name = _customer.CustomerID + " - " + _customer.ContactPerson, val = Convert.ToString(_customer.ID) };
                        }
                        else
                        {
                            ddl[count] = new Dropdown { name = _customer.CustomerID + " - " + _customer.CompanyName, val = Convert.ToString(_customer.ID) };
                        }

                        count++;
                    }

                    return ddl;
                }
            }

        }

        //Coordinator Dropdown
        public Dropdown[] CoordinatorDDL()
        {
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            User checkUser = _usersModel.GetSingle(loginMainID);
            IList<User> getAllUser = null;

            if (checkUser.Role == "Super Admin")
            {
                getAllUser = _usersModel.GetAll().Where(e => e.Role != "Accounts").ToList();
            }
            else
            {
                getAllUser = _usersModel.ListOwnAndDownline(loginMainID).Where(e => e.Role != "Accounts").ToList();
            }

            int count = 1;
            Dropdown[] ddl = new Dropdown[getAllUser.Count + 1];

            foreach (User _getAllUser in getAllUser)
            {
                ddl[count] = new Dropdown { name = _getAllUser.UserID + " - " + _getAllUser.Name, val = Convert.ToString(_getAllUser.ID) };
                count++;
            }

            return ddl;
        }

        //Project Type Dropdown
        public Dropdown[] ProjectTypeDDL()
        {

            Dropdown[] ddl = new Dropdown[2];

            ddl[0] = new Dropdown { name = "Commercial", val = "Commercial" };
            ddl[1] = new Dropdown { name = "Residential", val = "Residential" };

            return ddl;
        }

        //Status Dropdown
        public Dropdown[] StatusDDL()
        {

            Dropdown[] ddl = new Dropdown[2];

            ddl[0] = new Dropdown { name = "Active", val = "Active" };
            ddl[1] = new Dropdown { name = "Complete", val = "Completed" };

            return ddl;
        }

        //GST Dropdown
        public Dropdown[] GSTDDL()
        {

            Dropdown[] ddl = new Dropdown[2];

            ddl[0] = new Dropdown { name = "Yes", val = "Yes" };
            ddl[1] = new Dropdown { name = "No", val = "No" };

            return ddl;
        }

        //GET Project_ProjectExpenses Status Dropdown
        public Dropdown[] ProjectProjectExpensesStatusDDL()
        {
            //Get Main ID first
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            User getUserRole = _usersModel.GetSingle(loginMainID);

            Dropdown[] ddl = null;

            if(getUserRole.Role == "Super Admin")
            {
                ddl = new Dropdown[2];

                ddl[0] = new Dropdown { name = "Unpaid", val = "Unpaid" };
                ddl[1] = new Dropdown { name = "Paid", val = "Paid" };
            }
            else
            {
                ddl = new Dropdown[1];

                ddl[0] = new Dropdown { name = "Unpaid", val = "Unpaid" };
            }

            return ddl;
        }

        //Project Estimator Project Id Dropdown
        public Dropdown[] ProjectEstimatorProjectIdDDL()
        {
            //Loop All Project
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            User user = _usersModel.GetSingle(loginMainID);

            if (user.Role == "Super Admin")
            {
                IList<Project> project = _projectsModel.GetAll();

                Dropdown[] ddl = new Dropdown[project.Count];

                int count = 0;
                foreach (Project _project in project)
                {
                    ddl[count] = new Dropdown { name = _project.ProjectID + " - " + _project.Customer.ContactPerson, val = Convert.ToString(_project.ID) };
                    count++;
                }

                return ddl;
            }
            else
            {

                List<int> Coordinatorid = new List<int>();

                Coordinatorid.Add(loginMainID);

                //sales and customer, filter

                //first, get all upline id is same with login main ID.

                List<User> getSameUplineID = _usersModel.GetAllSameUplineID(loginMainID); //6

                foreach (User _getSameUplineID in getSameUplineID)
                {

                    Coordinatorid.Add(_getSameUplineID.ID);
                }

                IList<Project> project = _projectsModel.getOwnAndDownline(Coordinatorid);

                Dropdown[] ddl = new Dropdown[project.Count];

                int count = 0;
                foreach (Project _project in project)
                {
                    ddl[count] = new Dropdown { name = _project.ProjectID + " - " + _project.Customer.ContactPerson, val = Convert.ToString(_project.ID) };
                    count++;
                }

                return ddl;
            }

        }

        //GET: Quotation Total Amount
        public string GetQuotationTotalAmount(int id)
        {
            string result = "";

            List<QuotationItemDescription> quotation = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(id);
            decimal totalAmount = 0;

            foreach(QuotationItemDescription _quotation in quotation)
            {
                totalAmount += (decimal)_quotation.QuotationItemAmount;
            }

            result = totalAmount.ToString("#,##0.00");

            return result;
        }

        //GET: Coordinator ID
        public string getCoordinatorID(int id)
        {
            string result = "";

            Quotation quotation = _quotationsModel.GetSingle(id);

            User user = _usersModel.GetSingle(Convert.ToInt32(quotation.SalesPersonId));

            result = Convert.ToString(user.ID);

            return result;
        }

        //POST: FileUploader
        [HttpPost]
        public void FileUploader()
        {
            string filesUploaded = "";

            try
            {
                if (Request.Files.Count > 0)
                {
                    foreach (string key in Request.Files)
                    {
                        HttpPostedFileBase attachment = Request.Files[key];

                        if (!string.IsNullOrEmpty(attachment.FileName))
                        {
                            string mimeType = attachment.ContentType;
                            int fileLength = attachment.ContentLength;

                            string[] allowedTypes = ConfigurationManager.AppSettings["AllowedFileTypes"].ToString().Split(',');

                            if (allowedTypes.Contains(mimeType))
                            {
                                if (fileLength <= Convert.ToInt32(ConfigurationManager.AppSettings["MaxFileSize"]) * 1024 * 1024)
                                {
                                    string file = attachment.FileName.Substring(attachment.FileName.LastIndexOf(@"\") + 1, attachment.FileName.Length - (attachment.FileName.LastIndexOf(@"\") + 1));
                                    string fileName = Path.GetFileNameWithoutExtension(file);
                                    string newFileName = FileHelper.sanitiseFilename(fileName) + "_" + DateTime.Now.ToString("yyMMddHHmmss") + Path.GetExtension(file).ToLower();
                                    string path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), newFileName);

                                    if (!System.IO.File.Exists(path))
                                    {
                                        string oriPath = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["TempFolder"].ToString()), newFileName);

                                        attachment.SaveAs(oriPath);

                                        filesUploaded += newFileName + ",";
                                    }
                                    else
                                    {
                                        Response.Write("{\"result\":\"error\",\"msg\":\"" + newFileName + " already exists.\"}");
                                        break;
                                    }
                                }
                                else
                                {
                                    Response.Write("{\"result\":\"error\",\"msg\":\"File size exceeds 2MB.\"}");
                                    break;
                                }
                            }
                            else
                            {
                                Response.Write("{\"result\":\"error\",\"msg\":\"Invalid file type.\"}");
                                break;
                            }
                        }
                        else
                        {
                            Response.Write("{\"result\":\"error\",\"msg\":\"Please select a file to upload.\"}");
                            break;
                        }
                    }
                }
                else
                {
                    Response.Write("{\"result\":\"error\",\"msg\":\"Please select a file to upload.\"}");
                }

                if (!string.IsNullOrEmpty(filesUploaded))
                {
                    Response.Write("{\"result\":\"success\",\"msg\":\"" + filesUploaded.Substring(0, filesUploaded.Length - 1) + "\"}");
                }
            }
            catch
            {
                Response.Write("{\"result\":\"error\",\"msg\":\"An error occured while uploading file.\"}");
            }
        }

        //GET: ValidateAmount
        public string ValidateAmount(string amount)
        {
            string result = "";

            bool checkFormat = FormValidationHelper.AmountFormat(amount);

            if (checkFormat)
            {
                bool checkRange = FormValidationHelper.AmountFormat(amount, 20);

                if (checkRange)
                {
                    string newAmount = FormValidationHelper.AmountFormatter(Convert.ToDecimal(amount), 2);

                    result = "{\"result\":true,\"newAmount\":\"" + newAmount + "\"}";
                }
                else
                {
                    result = "{\"result\":false}";
                }
            }
            else
            {
                result = "{\"result\":false}";
            }

            return result;
        }
    }
}