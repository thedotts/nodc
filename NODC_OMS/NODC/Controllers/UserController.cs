﻿using DataAccess;
using DataAccess.POCO;
using NODC_OMS.Helper;
using NODC_OMS.Controllers;
using NODC_OMS.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using PagedList;

namespace NODC_OMS.Controllers
{
    [HandleError]
    [RedirecttingActionAdmin]
    public class UserController : ControllerBase
    {
        private IUserRepository _usersModel;
        private ISystemSettingRepository _settingsModel;
        private IProjectRepository _projectsModel;
        private IProjectProjectExpensesRepository _projectExpensesModel;
        private IQuotationRepository _quotationsModel;
        private IQuotationCoBrokeRepository _quotationCoBrokesModel;
        private IQuotationItemDescriptionRepository _quotationItemDescriptionsModel;
        private ICustomerRepository _customersModel;

        // GET: Project
        public UserController()
            : this(new UserRepository(), new SystemSettingRepository(), new ProjectRepository(), new ProjectProjectExpensesRepository(), new QuotationRepository(), new QuotationCoBrokeRepository(), new QuotationItemDescriptionRepository(), new CustomerRepository())
        {

        }

        public UserController(IUserRepository usersModel, ISystemSettingRepository settingsModel, IProjectRepository projectsModel, IProjectProjectExpensesRepository projectExpensesModel, IQuotationRepository quotationsModel, IQuotationCoBrokeRepository quotationCoBrokesModel, IQuotationItemDescriptionRepository quotationsItemDescriptionsModel, ICustomerRepository customersModel)
        {
            _usersModel = usersModel;
            _settingsModel = settingsModel;
            _projectsModel = projectsModel;
            _projectExpensesModel = projectExpensesModel;
            _quotationsModel = quotationsModel;
            _quotationCoBrokesModel = quotationCoBrokesModel;
            _quotationItemDescriptionsModel = quotationsItemDescriptionsModel;
            _customersModel = customersModel;
        }

        // GET: User
        public ActionResult Index()
        {

            if (Session["SearchKeyword"] != null)
            {
                Session.Remove("SearchKeyword");
            }

            if (Session["SearchStatus"] != null)
            {
                Session.Remove("SearchStatus");
            }

            return RedirectToAction("Listing");
        }

        //GET: Listing
        public ActionResult Listing(int page = 1)
        {
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = "";
            ViewData["SearchStatus"] = "";
            ViewData["GetStatusValue"] = "All Users";

            if (Session["SearchKeyword"] != null)
            {
                ViewData["SearchKeyword"] = Session["SearchKeyword"];
            }

            if (Session["SearchStatus"] != null)
            {

                if (Session["SearchStatus"].ToString() == "Active")
                {
                    ViewData["SearchStatus"] = "Active";
                    ViewData["GetStatusValue"] = "Active Only";
                }
                else if (Session["SearchStatus"].ToString() == "Suspend")
                {
                    ViewData["SearchStatus"] = "Suspend";
                    ViewData["GetStatusValue"] = "Suspended Only";
                }
                else
                {
                    ViewData["SearchStatus"] = Session["SearchStatus"];
                }
            }

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", null, null, null);

            IPagedList<User> users = _usersModel.GetPaged(ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), page, pageSize);
            ViewData["User"] = users;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            return View();
        }

        //POST: Listing
        [HttpPost]
        public ActionResult Listing(FormCollection form)
        {
            int page = 1;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = form["SearchKeyword"].Trim();
            Session.Add("SearchKeyword", form["SearchKeyword"].Trim());

            ViewData["GetStatusValue"] = "All Users";

            if (form["SearchStatusValue"].Trim() == "Active")
            {
                ViewData["GetStatusValue"] = "Active Only";
            }

            if (form["SearchStatusValue"].Trim() == "Suspend")
            {
                ViewData["GetStatusValue"] = "Suspended Only";
            }

            string item = "";

            if (form["SearchStatusValue"].Trim() != "All")
            {
                item = form["SearchStatusValue"].Trim();
            }

            ViewData["SearchStatus"] = item;
            Session.Add("SearchStatus", item);

            IPagedList<User> users = _usersModel.GetPaged(ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), page, pageSize);
            ViewData["User"] = users;

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", ViewData["SearchStatus"].ToString());

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Create
        public ActionResult Create()
        {
            //Get Main ID first
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            Dropdown[] uplineDDL = UplineDDL();
            ViewData["UplineDropdown"] = new SelectList(uplineDDL, "val", "name", loginMainID);

            Dropdown[] roleDropdown = RoleDDL();
            ViewData["RoleDropdown"] = new SelectList(roleDropdown, "val", "name", null);

            Dropdown[] positionDDL = PositionDDL();
            ViewData["PositionDropdown"] = new SelectList(positionDDL, "val", "name", null);

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", null);

            ViewData["CheckIsAgencyLeader"] = "";
            ViewData["CheckReminder"] = "";

            User getTotalCompletedProfit = _usersModel.GetSingle(loginMainID);

            ViewData["TotalCompletedProfit"] = getTotalCompletedProfit.TotalCompletedProfit.ToString("#,##0.00");

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Create
        [HttpPost]
        public ActionResult Create(User user, FormCollection form)
        {

            //Get Main ID first
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            Dropdown[] uplineDDL = UplineDDL();
            ViewData["UplineDropdown"] = new SelectList(uplineDDL, "val", "name", user.UplineId);

            Dropdown[] roleDropdown = RoleDDL();
            ViewData["RoleDropdown"] = new SelectList(roleDropdown, "val", "name", null);

            Dropdown[] positionDDL = PositionDDL();
            ViewData["PositionDropdown"] = new SelectList(positionDDL, "val", "name", null);

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", null);

            ViewData["CheckIsAgencyLeader"] = "";
            ViewData["CheckReminder"] = "";

            //Validation Part
            if(string.IsNullOrEmpty(user.LoginID)){
                ModelState.AddModelError("user.LoginID","Login ID is required!");
            }
            else
            {

                if (user.LoginID.Contains(" "))
                {
                    ModelState.AddModelError("user.LoginID", "Login ID cannot have space in between or anywhere!");
                }
                else
                {
                    User checkLoginID = _usersModel.FindLoginID(user.LoginID);

                    //check unique login ID
                    if(checkLoginID != null){
                        ModelState.AddModelError("user.LoginID", "Login ID already existed!!");
                    }
                }

            }

            if(string.IsNullOrEmpty(user.Password)){

                ModelState.AddModelError("user.Password", "Password is required!");
            }

            if(string.IsNullOrEmpty(form["RepeatPassword"])){
                ModelState.AddModelError("RepeatPassword", "Repeat Password is required!");
            }

            if(user.Password != form["RepeatPassword"].ToString()){
                ModelState.AddModelError("user.Password", "Password does not match Repeat Password!");
                ModelState.AddModelError("RepeatPassword", "Repeat Password does not match Password!");
            }

            if(string.IsNullOrEmpty(user.Name)){
                ModelState.AddModelError("user.Name","Name is required!");
            }

            if(!string.IsNullOrEmpty(user.Email)){

                bool checkFormatEmail = FormValidationHelper.EmailFormat(user.Email);

                if (checkFormatEmail)
                {
                    User checkUniqueEmail = _usersModel.GetUniqueEmail(user.Email);

                    if (checkUniqueEmail != null)
                    {
                        ModelState.AddModelError("user.Email", "Email already existed!");
                    }
                }
                else
                {
                    ModelState.AddModelError("user.Email", "Email is not valid!");
                }
            }
            else
            {
                ModelState.AddModelError("user.Email", "Email is required!");
            }

            if(string.IsNullOrEmpty(user.Tel)){
                ModelState.AddModelError("user.Tel","Tel is required!");
            }

            if (!string.IsNullOrEmpty(form["user.Reminder"]))
            {
                user.Reminder = "Yes";
                ViewData["CheckReminder"] = "checked";
            }
            else
            {
                user.Reminder = "No";
            }

            if (!string.IsNullOrEmpty(form["user.IsAgencyLeader"]))
            {
                user.IsAgencyLeader = "Yes";
                ViewData["CheckIsAgencyLeader"] = "checked";
            }
            else
            {
                user.IsAgencyLeader = "No";
            }

            //If Account, skip upline validation
            if (user.Role != "Accounts")
            {
                if (user.UplineId <= 0)
                {
                    ModelState.AddModelError("user.UplineId", "Upline is required!");
                }
            }
            else
            {
                ModelState UplineState = ModelState["user.UplineId"];

                if (UplineState.Errors.Count > 0)
                {
                    UplineState.Errors.Clear();
                }

                user.UplineId = 1;//Preset Accounts Upline ID
            }

            if(user.Role != "Sales")
            {//if this is not sales, then position will empty
                user.Position = null;
            }

            if(user.Role == "Super Admin")
            {
                user.IsAgencyLeader = "Yes"; //force to "Yes" if is super admin
                ViewData["CheckIsAgencyLeader"] = "checked";
            }
            else if(user.Role == "Accounts")
            {
                user.IsAgencyLeader = "No"; //force to "Yes" if is super admin
                ViewData["CheckIsAgencyLeader"] = "";
            }

            ModelState presetValueState = ModelState["user.PresetValue"];
            if (presetValueState.Errors.Count > 0)
            {
                presetValueState.Errors.Clear();
            }

            string Amount = form["user.PresetValue"];

            if (!string.IsNullOrEmpty(Amount))
            {
                bool checkFormat = FormValidationHelper.AmountFormat(Amount);

                if (!checkFormat)
                {
                    ModelState.AddModelError("user.PresetValue", "Invalid Format!");
                }
                else
                {
                    user.PresetValue = Convert.ToDecimal(Amount);
                }
            }
            else
            {
                ModelState.AddModelError("user.PresetValue", "Preset Value is required!");
            }

            if(ModelState.IsValid)
            {

                //Assign User ID
                string prefix = _settingsModel.GetCodeValue("PREFIX_USER_ID");

                User lastRecord = _usersModel.GetLast();

                //Convert year to last two digit
                var date = DateTime.Now;
                int last_two_digit = date.Year % 100;

                if (lastRecord != null)
                {
                    user.UserID = prefix + last_two_digit + (lastRecord.ID + 1).ToString().PadLeft(5, '0');
                }
                else
                {
                    user.UserID = prefix + last_two_digit + "00001";
                }

                user.Password = EncryptionHelper.Encrypt(user.Password);

                bool result = _usersModel.Add(user);

                if(result)
                {

                    int MainID = Convert.ToInt32(Session["MainID"]);
                    string userRole = Session["UserGroup"].ToString();
                    if (userRole == "Sales")
                    {
                        userRole += " " + Session["Position"].ToString();
                    }
                    string tableAffected = "Users";
                    string description = userRole + " [" + Session["LoginID"].ToString() + "] Created New User [" + user.UserID + "]";
                    
                    bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                    if(user.Role == "Accounts")
                    {
                        //Update Accounts Upline ID
                        bool updateAccountsUplineID = _usersModel.UpdateAccountsUplineID(user.ID);

                        if (updateAccountsUplineID)
                        {
                            Session.Add("Result", "success|" + user.UserID + " has been successfully created!");

                            return RedirectToAction("Index");
                        }
                    }
                    else
                    {
                        Session.Add("Result", "success|" + user.UserID + " has been successfully created!");

                        return RedirectToAction("Index");
                    }

                }
                else
                {
                    Session.Add("Result", "danger|An error occured while save user records!");
                }

            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            User getTotalCompletedProfit = _usersModel.GetSingle(loginMainID);

            ViewData["TotalCompletedProfit"] = getTotalCompletedProfit.TotalCompletedProfit.ToString("#,##0.00");

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: View
        public ActionResult View(int id)
        {
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            User user = _usersModel.GetSingle(id);

            if(user != null)
            {

                ViewData["CheckIsAgencyLeader"] = "";
                ViewData["CheckReminder"] = "";

                Dropdown[] uplineDDL = UplineDDL();
                ViewData["UplineDropdown"] = ViewData["UplineDropdown"] = new SelectList(uplineDDL, "val", "name", null, user.UplineId, null);

                Dropdown[] roleDropdown = RoleDDL();
                ViewData["RoleDropdown"] = new SelectList(roleDropdown, "val", "name", null, null, null);

                Dropdown[] positionDDL = PositionDDL();
                ViewData["PositionDropdown"] = new SelectList(positionDDL, "val", "name", null, null, null);

                Dropdown[] statusDDL = StatusDDL();
                ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", null, user.Status, null);

                if(user.IsAgencyLeader == "Yes"){
                    ViewData["CheckIsAgencyLeader"] = "checked";
                }

                if(user.Reminder == "Yes"){
                    ViewData["CheckReminder"] = "checked";
                }
                ViewData["User"] = user;
                return View();
            }
            else
            {
                Session.Add("Result", "danger|User record not found!");
            }

            return RedirectToAction("Index");
        }

        //GET: Edit
        public ActionResult Edit(int id)
        {
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            User user = _usersModel.GetSingle(id);

            int MainID = Convert.ToInt32(Session["MainID"]);

            ViewData["AbleToUpdate"] = "No";

            User checkUserRole = _usersModel.GetSingle(MainID);

            if (checkUserRole.Role == "Super Admin")
            {
                ViewData["AbleToUpdate"] = "Yes";
            }

            if (user != null)
            {

                ViewData["CheckIsAgencyLeader"] = "";
                ViewData["CheckReminder"] = "";

                Dropdown[] uplineDDL = UplineDDL();
                ViewData["UplineDropdown"] = ViewData["UplineDropdown"] = new SelectList(uplineDDL, "val", "name", user.UplineId);

                Dropdown[] roleDropdown = RoleDDL();
                ViewData["RoleDropdown"] = new SelectList(roleDropdown, "val", "name", null);

                Dropdown[] positionDDL = PositionDDL();
                ViewData["PositionDropdown"] = new SelectList(positionDDL, "val", "name", null);

                Dropdown[] statusDDL = StatusDDL();
                ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", user.Status);

                if (user.IsAgencyLeader == "Yes")
                {
                    ViewData["CheckIsAgencyLeader"] = "checked";
                }

                if (user.Reminder == "Yes")
                {
                    ViewData["CheckReminder"] = "checked";
                }
                ViewData["User"] = user;
                return View();
            }
            else
            {
                Session.Add("Result", "danger|User record not found!");
            }

            return RedirectToAction("Index");
        }

        //POST: Edit
        [HttpPost]
        public ActionResult Edit(int id, User user, FormCollection form)
        {
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            ViewData["CheckIsAgencyLeader"] = "";
            ViewData["CheckReminder"] = "";

            int MainLoginID = Convert.ToInt32(Session["MainID"]);

            User checkSuperAdmin = _usersModel.GetSingle(MainLoginID);

            ViewData["AbleToUpdate"] = "No";

            if (checkSuperAdmin.Role == "Super Admin")
            {
                ViewData["AbleToUpdate"] = "Yes";
            }

            //get all old data first
            User oldData = _usersModel.GetSingle(id);
            string password = oldData.Password;

            //Validation Part
            if (string.IsNullOrEmpty(user.LoginID))
            {
                ModelState.AddModelError("user.LoginID", "Login ID is required!");
            }
            else
            {

                if (user.LoginID.Contains(" "))
                {
                    ModelState.AddModelError("user.LoginID", "Login ID cannot have space in between or anywhere!");
                }
                else
                {
                    User checkLoginID = _usersModel.FindPersonalLoginID(id, user.LoginID);

                    //check unique login ID
                    if (checkLoginID != null)
                    {
                        ModelState.AddModelError("user.LoginID", "Login ID already existed!!");
                    }
                }

            }

            if (string.IsNullOrEmpty(user.Name))
            {
                ModelState.AddModelError("user.Name", "Name is required!");
            }

            if (!string.IsNullOrEmpty(user.Email))
            {

                bool checkFormatEmail = FormValidationHelper.EmailFormat(user.Email);

                if (checkFormatEmail)
                {
                    User checkUniqueEmail = _usersModel.GetPersonalUniqueEmail(id, user.Email);

                    if (checkUniqueEmail != null)
                    {
                        ModelState.AddModelError("user.Email", "Email already existed!");
                    }
                }
                else
                {
                    ModelState.AddModelError("user.Email", "Email is not valid!");
                }
            }
            else
            {
                ModelState.AddModelError("user.Email", "Email is required!");
            }

            if (string.IsNullOrEmpty(user.Tel))
            {
                ModelState.AddModelError("user.Tel", "Tel is required!");
            }

            if (!string.IsNullOrEmpty(form["user.Reminder"]))
            {
                user.Reminder = "Yes";
                ViewData["CheckReminder"] = "checked";
            }
            else
            {
                user.Reminder = "No";
            }

            if (!string.IsNullOrEmpty(form["user.IsAgencyLeader"]))
            {
                user.IsAgencyLeader = "Yes";
                ViewData["CheckIsAgencyLeader"] = "checked";
            }
            else
            {
                user.IsAgencyLeader = "No";
            }

            if (user.Role != "Accounts")
            {
                if (user.UplineId <= 0)
                {
                    ModelState.AddModelError("user.UplineId", "Upline is required!");
                }
            }
            else
            {
                ModelState UplineState = ModelState["user.UplineId"];

                if (UplineState.Errors.Count > 0)
                {
                    UplineState.Errors.Clear();
                }

                user.UplineId = id;
            }

            if (user.Role != "Sales")
            {//if this is not sales, then position will empty
                user.Position = null;
            }

            if (user.Role == "Super Admin")
            {
                user.IsAgencyLeader = "Yes"; //force to "Yes" if is super admin
                ViewData["CheckIsAgencyLeader"] = "checked";
            }
            else if(user.Role == "Accounts")
            {
                user.IsAgencyLeader = "No"; //force to "No" if is account, because account cannot be agency leader
                ViewData["CheckIsAgencyLeader"] = "";
            }

            ModelState presetValueState = ModelState["user.PresetValue"];
            if (presetValueState.Errors.Count > 0)
            {
                presetValueState.Errors.Clear();
            }

            string Amount = form["user.PresetValue"];

            if (!string.IsNullOrEmpty(Amount))
            {
                bool checkFormat = FormValidationHelper.AmountFormat(Amount);

                if (!checkFormat)
                {
                    ModelState.AddModelError("user.PresetValue", "Invalid Format!");
                }
                else
                {
                    user.PresetValue = Convert.ToDecimal(Amount);
                }
            }
            else
            {
                ModelState.AddModelError("user.PresetValue", "Preset Value is required!");
            }

            if (ModelState.IsValid)
            {

                //If password not null
                if (ViewData["AbleToUpdate"] == "Yes")
                {
                    if (!string.IsNullOrEmpty(form["Password"].ToString()))
                    {
                        user.Password = EncryptionHelper.Encrypt(form["Password"].ToString());
                        password = form["Password"].ToString();
                    }
                    else
                    {
                        //if blank, then assign to original password
                        user.Password = oldData.Password;
                        password = EncryptionHelper.Decrypt(oldData.Password);
                    }
                }
                else
                {
                    password = EncryptionHelper.Decrypt(oldData.Password);
                }

                bool result = _usersModel.UpdateData(id, user);

                if (result)
                {

                    int MainID = Convert.ToInt32(Session["MainID"]);
                    string userRole = Session["UserGroup"].ToString();
                    if (userRole == "Sales")
                    {
                        userRole += " " + Session["Position"].ToString();
                    }
                    string tableAffected = "Users";
                    string description = userRole + " [" + Session["LoginID"].ToString() + "] Edited User [" + oldData.UserID + "]";

                    bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                    Session.Add("Result", "success|" + oldData.UserID + " has been successfully edited!");

                    return RedirectToAction("Index");
                }
                else
                {
                    Session.Add("Result", "danger|An error occured while save user records!");
                }

            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            Dropdown[] uplineDDL = UplineDDL();
            ViewData["UplineDropdown"] = ViewData["UplineDropdown"] = new SelectList(uplineDDL, "val", "name", user.UplineId);

            Dropdown[] roleDropdown = RoleDDL();
            ViewData["RoleDropdown"] = new SelectList(roleDropdown, "val", "name", user.Role);

            Dropdown[] positionDDL = PositionDDL();
            ViewData["PositionDropdown"] = new SelectList(positionDDL, "val", "name", user.Position);

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name",  user.Status);

            if (user.IsAgencyLeader == "Yes")
            {
                ViewData["CheckIsAgencyLeader"] = "checked";
            }

            if (user.Reminder == "Yes")
            {
                ViewData["CheckReminder"] = "checked";
            }

            ViewData["User"] = user;

            return View();
        }

        //GET: Delete
        public ActionResult Delete(int id)
        {
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            User CheckUserRole = _usersModel.GetSingle(loginMainID);

            if (CheckUserRole.Role != "Super Admin")
            {
                Session.Add("Result", "danger|You are not allow to delete this user!");
                return RedirectToAction("Index");
            }

            User user = _usersModel.GetSingle(id);

            //Check Link Part (User)
            IList<User> AllUser = _usersModel.GetAll();

            foreach (User _AllUser in AllUser)
            {
                if(_AllUser.UplineId == id)
                {
                    Session.Add("Result", "danger|" + user.LoginID + " cannot delete because it link with User Upline!");
                    return RedirectToAction("Index");
                }
            }

            //Check Linked Part (Customer)
            IList<Customer> customer = _customersModel.GetAll();

            foreach (Customer _customer in customer)
            {
                if(_customer.UplineUserType == "User")
                {
                    if(_customer.UplineId == id)
                    {
                        Session.Add("Result", "danger|" + user.LoginID + " cannot delete because it link with Customer Upline!");
                        return RedirectToAction("Index");
                    }
                }

                if(_customer.AssignToId == id)
                {
                    Session.Add("Result", "danger|" + user.LoginID + " cannot delete because it link with Customer Assign To!");
                    return RedirectToAction("Index");
                }
            }

            //Check Linked Part (Quotations)
            IList<Quotation> quotations = _quotationsModel.GetAll();

            foreach(Quotation _quotations in quotations)
            {
                if(_quotations.SalesPersonId == id)
                {
                    Session.Add("Result", "danger|" + user.LoginID + " cannot delete because it link with Quotation Sales Person!");
                    return RedirectToAction("Index");
                }
            }

            //Check Linked Part (Quotations Co-Broke)

            IList<QuotationCoBroke> coBroke = _quotationCoBrokesModel.GetAll();

            foreach (QuotationCoBroke _coBroke in coBroke)
            {
                if(_coBroke.QuotationCoBrokeUserId == id)
                {
                    Session.Add("Result", "danger|" + user.LoginID + " cannot delete because it link with Quotation Co-Broke!");
                    return RedirectToAction("Index");
                }
            }

            //Check Linked Part (Project)
            IList<Project> project = _projectsModel.GetAll();

            foreach (Project _project in project)
            {
                if (_project.CoordinatorId == id)
                {
                    Session.Add("Result", "danger|" + user.LoginID + " cannot delete because it link with Project Coordinator!");
                    return RedirectToAction("Index");
                }
            }

            bool result = _usersModel.Delete(id);

            if(result){

                int MainID = Convert.ToInt32(Session["MainID"]);
                string userRole = Session["UserGroup"].ToString();
                if (userRole == "Sales")
                {
                    userRole += " " + Session["Position"].ToString();
                }
                string tableAffected = "Users";
                string description = userRole + " [" + Session["LoginID"].ToString() + "] Deleted User[" + user.LoginID + "]";

                bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                Session.Add("Result", "success|" + user.LoginID + " successfully deleted!");
            }
            else
            {
                Session.Add("Result", "danger|User record not found!");
            }

            return RedirectToAction("Index");
        }

        //GET: View
        public ActionResult ViewSalesCalendar()
        {
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //Upline Dropdown
        public Dropdown[] UplineDDL()
        {

            int userid = Convert.ToInt32(Session["MainID"]);

            User getRole = _usersModel.GetSingle(userid);

            IList<User> getAllUser = null;

            //if is super admin, can choose ownself,
            //if is salesperson, cannot choose ownself.

            if(getRole.Role == "Super Admin")
            {
                getAllUser = _usersModel.GetAll().Where(e => e.Role != "Accounts").ToList();
            }
            else
            {
                getAllUser = _usersModel.GetAll().Where(e => e.Role != "Accounts" && e.ID != getRole.ID).ToList();
            }

            Dropdown[] ddl = new Dropdown[getAllUser.Count + 1];
            int count = 1;

            foreach (User _getAllUser in getAllUser)
            {
                ddl[count] = new Dropdown { name = _getAllUser.UserID + " - " + _getAllUser.Name, val = _getAllUser.ID.ToString() };
                count++;
            }

            return ddl;
        }

        //Role Dropdown
        public Dropdown[] RoleDDL()
        {

            Dropdown[] ddl = new Dropdown[3];

            ddl[0] = new Dropdown { name = "Sales", val = "Sales" };
            ddl[1] = new Dropdown { name = "Accounts", val = "Accounts" };
            ddl[2] = new Dropdown { name = "Super Admin", val = "Super Admin" };

            return ddl;
        }

        //Position Dropdown
        public Dropdown[] PositionDDL()
        {

            Dropdown[] ddl = new Dropdown[5];

            ddl[0] = new Dropdown { name = "Consultant", val = "Consultant" };
            ddl[1] = new Dropdown { name = "Assistant Manager", val = "Assistant Manager" };
            ddl[2] = new Dropdown { name = "Manager", val = "Manager" };
            ddl[3] = new Dropdown { name = "Senior Manager", val = "Senior Manager" };
            ddl[4] = new Dropdown { name = "Director", val = "Director" };

            return ddl;
        }

        //Status Dropdown
        public Dropdown[] StatusDDL()
        {

            Dropdown[] ddl = new Dropdown[2];

            ddl[0] = new Dropdown { name = "Active", val = "Active" };
            ddl[1] = new Dropdown { name = "Suspend", val = "Suspend" };


            return ddl;
        }

        //GET: ValidateAmount
        public string ValidateAmount(string amount)
        {
            string result = "";

            bool checkFormat = FormValidationHelper.AmountFormat(amount);

            if (checkFormat)
            {
                bool checkRange = FormValidationHelper.AmountFormat(amount, 20);

                if (checkRange)
                {
                    string newAmount = FormValidationHelper.AmountFormatter(Convert.ToDecimal(amount), 2);

                    result = "{\"result\":true,\"newAmount\":\"" + newAmount + "\"}";
                }
                else
                {
                    result = "{\"result\":false}";
                }
            }
            else
            {
                result = "{\"result\":false}";
            }

            return result;
        }

    }
}