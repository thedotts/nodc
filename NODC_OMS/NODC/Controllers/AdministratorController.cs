﻿using DataAccess;
using DataAccess.POCO;
using NODC_OMS.Controllers;
using NODC_OMS.Helper;
using NODC_OMS.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NODC_OMS.Controllers
{
    [HandleError]
    [RedirectingAction]
    public class AdministratorController : ControllerBase
    {
        private IUserRepository _usersModel;

        // GET: Administrator
        public AdministratorController()
            : this(new UserRepository())
        {

        }

        public AdministratorController(IUserRepository usersModel)
        {
            _usersModel = usersModel;
        }

        // GET: TaskList
        public ActionResult Index()
        {
            return RedirectToAction("UserProfile");
        }

        //GET: Listing
        public ActionResult UserProfile()
        {
            int uid = Convert.ToInt32(Session["MainID"]);

            User getUserData = _usersModel.GetSingle(uid);

            ViewData["UserID"] = "";
            ViewData["LoginID"] = "";
            ViewData["Status"] = "";

            if(getUserData != null)
            {
                ViewData["UserID"] = getUserData.UserID;
                ViewData["LoginID"] = getUserData.LoginID;
                ViewData["Status"] = getUserData.Status;
            }
            else
            {
                Session.Add("Result", "danger|User record not found!");
            }


            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: UserProfile
        [HttpPost]
        public ActionResult UserProfile(FormCollection form)
        {
            int uid = Convert.ToInt32(Session["MainID"]);

            User getUserData = _usersModel.GetSingle(uid);

            if (!string.IsNullOrEmpty(form["Password"]))
            {
                if (!string.IsNullOrEmpty(form["RepeatPassword"]))
                {
                    if (form["Password"].ToString() != form["RepeatPassword"].ToString())
                    {
                        ModelState.AddModelError("Password", "Password does not match Repeat Password!");
                        ModelState.AddModelError("RepeatPassword", "Repeat Password does not match Password!");
                    }
                }
                else
                {
                    ModelState.AddModelError("RepeatPassword", "Repeat Password is required!");
                }
            }

            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(form["Password"]))
                {
                    getUserData.Password = EncryptionHelper.Encrypt(form["Password"].ToString());
                }

                bool result = _usersModel.Update(getUserData.ID, getUserData);

                if (result)
                {
                    int userTriggering = getUserData.ID;
                    string userRole = Session["UserGroup"].ToString();
                    if (userRole == "Sales")
                    {
                        userRole += " " + Session["Position"].ToString();
                    }
                    string tableAffected = "Users";
                    string description = userRole + " User [" + getUserData.Name + "] Updated Admin Profile";
                    bool profile_log = AuditLogHelper.WriteAuditLog(userTriggering, userRole, tableAffected, description);

                    Session.Add("Result", "success|Your Profile has been successfully updated!");
                }
                else
                {
                    Session.Add("Result", "danger|Error while updating Admin Profile!");
                }
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            ViewData["UserID"] = getUserData.UserID;
            ViewData["LoginID"] = getUserData.LoginID;
            ViewData["Status"] = getUserData.Status;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }
    }
}