﻿using DataAccess;
using DataAccess.POCO;
using NODC_OMS.Helper;
using NODC_OMS.Models;
using Rotativa;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NODC.Controllers
{
    public class SendEmailQuotationController : Controller
    {

        private IUserRepository _usersModel;
        private IQuotationRepository _quotationsModel;
        private IQuotationCoBrokeRepository _quotationCoBrokesModel;
        private IQuotationItemDescriptionRepository _quotationItemDescriptionsModel;
        private ISystemSettingRepository _settingsModel;
        private ICustomerRepository _customersModel;

        // GET: Quotation
        public SendEmailQuotationController()
            : this(new UserRepository(), new QuotationRepository(), new QuotationCoBrokeRepository(), new QuotationItemDescriptionRepository(), new SystemSettingRepository(), new CustomerRepository())
        {

        }

        public SendEmailQuotationController(IUserRepository usersModel, IQuotationRepository quotationsModel, IQuotationCoBrokeRepository quotationCoBrokesModel, IQuotationItemDescriptionRepository quotationItemDescriptionsModel, ISystemSettingRepository settingsModel, ICustomerRepository customersModel)
        {
            _usersModel = usersModel;
            _quotationsModel = quotationsModel;
            _quotationCoBrokesModel = quotationCoBrokesModel;
            _quotationItemDescriptionsModel = quotationItemDescriptionsModel;
            _settingsModel = settingsModel;
            _customersModel = customersModel;
        }

        //Send Email
        public ActionResult SendEmail(int id, string from = null)
        {

            if(id < 0)
            {
                Session.Add("Result", "danger|Quotation not found!");
                return RedirectToAction("Index", "Quotation");
            }

            Quotation quotation = _quotationsModel.GetSingle(id);

            if (quotation != null)
            {
                string CustomerName = "";

                if (!string.IsNullOrEmpty(quotation.Customer.CompanyName))
                {
                    CustomerName = quotation.Customer.CompanyName;
                }
                else
                {
                    CustomerName = quotation.Customer.ContactPerson;
                }

                if (!string.IsNullOrEmpty(quotation.Customer.Email))
                {

                    var attachment = SendQuotationEmail(id);
                    string fileName = attachment.FileName;
                    byte[] pdf = attachment.BuildPdf(ControllerContext);

                    string subject = "Quotation [" + quotation.QuotationID + "] with attachment";
                    string body = System.IO.File.ReadAllText(Server.MapPath("~/Templates/QuotationWithAttachmentNotification.html"));
                    string recipient = quotation.Customer.Email;
                    string name = CustomerName;
                    string quotationID = Convert.ToString(quotation.QuotationID);
                    string status = quotation.Status;

                    ListDictionary replacements = new ListDictionary();

                    replacements.Add("<%Name%>", name);
                    replacements.Add("<%QuotationID%>", quotationID);
                    replacements.Add("<%Status%>", status);
                    replacements.Add("<%Url%>", Url.Action("ViewQuotation", "Customers", new { @id = quotation.ID }, "http"));


                    bool sent = EmailHelper.SendEmail(subject, body, replacements, recipient, null, null, pdf, fileName);


                    Session.Add("Result", "success|Email with attachment has been sent to Customer " + CustomerName + " email address!");
                }
                else
                {
                    Session.Add("Result", "danger|Email with attachment unable to sent to Customer " + CustomerName + " because it doesn't have email address!");
                }
            }
            else
            {
                Session.Add("Result", "danger|Quotation not found!");
                return RedirectToAction("Index", "Quotation");
            }

            //from view site
            if(from != null)
            {
                return RedirectToAction("View/" + id, "Quotation");
            }
            else
            {
                return RedirectToAction("Edit/" + id, "Quotation");
            }
        }

        //Send Email All Super Admin
        public ActionResult SendEmailAllSuperAdmin(int id)
        {
            Quotation quotation = _quotationsModel.GetSingle(id);
            IList<User> user = _usersModel.GetAll();

            if (quotation != null)
            {

                foreach (User _user in user)
                {
                    if(_user.Role == "Super Admin")
                    {
                        var attachment = SendQuotationEmail(id);
                        string fileName = attachment.FileName;
                        byte[] pdf = attachment.BuildPdf(ControllerContext);

                        string subject = "Quotation [" + quotation.QuotationID + "] with attachment";
                        string body = System.IO.File.ReadAllText(Server.MapPath("~/Templates/QuotationWithAttachmentNotification.html"));
                        string recipient = _user.Email;
                        string name = _user.Name;
                        string quotationID = Convert.ToString(quotation.QuotationID);
                        string status = quotation.Status;

                        ListDictionary replacements = new ListDictionary();

                        replacements.Add("<%Name%>", name);
                        replacements.Add("<%QuotationID%>", quotationID);
                        replacements.Add("<%Status%>", status);
                        replacements.Add("<%Url%>", Url.Action("Edit", "Quotation", new { @id = quotation.ID }, "http"));

                        bool sent = EmailHelper.SendEmail(subject, body, replacements, recipient, null, null, pdf, fileName);
                    }
                }

                Session.Add("Result", "success|Quotation " + quotation.QuotationID + " has marked as confirmed and email with attachment has been sent to all super admin email address!");
            }
            else
            {
                Session.Add("Result", "danger|Quotation not found!");
            }

            return RedirectToAction("Index", "Quotation");
        }

        public ActionResult HtmlView(int id)
        {

            //Get Quotation 
            Quotation quotation = _quotationsModel.GetSingle(id);

            if(quotation == null)
            {
                return RedirectToAction("Index", "Quotation");
            }

            ViewData["Quotation"] = quotation;

            if (quotation.HasGST == "Yes")
            {
                SystemSetting setting = _settingsModel.GetSingle("GST");
                ViewData["GST"] = setting.Value;
            }

            SystemSetting getTermsAndCondition = _settingsModel.GetSingle("TERM_AND_CONDITION");
            ViewData["TermAndCondition"] = getTermsAndCondition.Value;

            SystemSetting getCompanyName = _settingsModel.GetSingle("COMPANY_NAME");
            ViewData["SystemSettingCompanyName"] = getCompanyName.Value;

			string[] listOfCompanies = _settingsModel.GetCodeValue("LIST_OF_COMPANY").Split(';');

			ViewData["CompanyName"] = "MY RENOBUDDY (SG) PTE LTD";
			ViewData["CompanyInfo"] = "Office / Factory: 1007 Eunos Avenue 7 #01-25 S(409578)|Email: Enquiries@myrenobuddy.com.sg|Contact / Fax: +65 6741 3503 (O) +6741 3060 (F)|Co. Reg. No: 201534632H";

			string CompanyValue = "";

			if (!string.IsNullOrEmpty(getCompanyName.Value))
			{
				CompanyValue = getCompanyName.Value.ToString();
			}

			foreach (string company in listOfCompanies)
			{
				string[] comp = company.Split('|');

				if (comp[0] == CompanyValue)
				{
					ViewData["CompanyName"] = comp[0];
					ViewData["CompanyInfo"] = comp[2].Replace("\r\n", "|");
					break;
				}
			}

			//Get Quotation CoBroke Item
			List<QuotationCoBroke> quotationCoBroke = _quotationCoBrokesModel.GetAllRelatedQuotationId(id);
            ViewData["QuotationCoBroke"] = quotationCoBroke;

            //Get Quotation Item Description

            //New Get Quotation Item Description
            List<QuotationItemDescription> quotationItemDescription = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(id);
            List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();

            //Loop Item Description Items
            IList<SystemSetting> listItem = _settingsModel.GetAll();
            SystemSetting CategoryList = listItem.Where(e => e.Code == "QUOTATION_CATEGORIES").FirstOrDefault();

            string[] categoryList = CategoryList.Value.Split('|');

            //Start
            int countItemDescription = 1;
            int resetNumber = 1;
            foreach (QuotationItemDescription _quotationItemDescription in quotationItemDescription)//14 items
            {
                string ItemName = _quotationItemDescription.CategoryName;
                int ItemQty = (int)_quotationItemDescription.QuotationItemQty;
                decimal ItemRate = (decimal)_quotationItemDescription.QuotationItemRate;
                decimal ItemEstCost = (decimal)_quotationItemDescription.QuotationItemEstCost;
                bool IsBasic = false; //default set to false means it is not from db item.

                List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                if (ItemQty == null)
                {
                    ItemQty = 0;
                }

                if (ItemRate == null)
                {
                    ItemRate = 0;
                }

                if (ItemEstCost == null)
                {
                    ItemEstCost = 0;
                }


                foreach (string _categoryList in categoryList)
                {
                    if (_categoryList == _quotationItemDescription.CategoryName)
                    {
                        IsBasic = true;
                    }
                }

                if (quotationItemDescriptionBigTable.Count == 0)
                {
                    resetNumber = 1;
                    ItemDescriptionRows.Add(new ItemDescriptionRow()
                    {
                        RowId = resetNumber,
                        Description = _quotationItemDescription.QuotationItem_Description,
                        Qty = Convert.ToString(_quotationItemDescription.QuotationItemQty),
                        Unit = _quotationItemDescription.QuotationItemUnit,
                        Rate = Convert.ToString(_quotationItemDescription.QuotationItemRate),
                        Amount = Convert.ToString(_quotationItemDescription.QuotationItemAmount),
                        Est_Cost = Convert.ToString(_quotationItemDescription.QuotationItemEstCost),
                        AdjCost = null,
                        Est_Profit = Convert.ToString(_quotationItemDescription.QuotationItemEstProfit),
                        Remarks = _quotationItemDescription.QuotationItemRemarks,
                        Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                    });

                    quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                    {
                        TableID = countItemDescription,
                        CategoryName = _quotationItemDescription.CategoryName,
                        IsBasic = IsBasic,
                        ItemDescriptionRows = ItemDescriptionRows
                    });
                    countItemDescription++;
                }
                else
                {
                    if (quotationItemDescriptionBigTable.Where(e => e.CategoryName == _quotationItemDescription.CategoryName).FirstOrDefault() != null)
                    {
                        foreach (ItemDescriptionBigTable _loopBigTable in quotationItemDescriptionBigTable)
                        {
                            if (_loopBigTable.CategoryName == _quotationItemDescription.CategoryName)
                            {
                                _loopBigTable.ItemDescriptionRows.Add(new ItemDescriptionRow()
                                {
                                    RowId = resetNumber,
                                    Description = _quotationItemDescription.QuotationItem_Description,
                                    Qty = Convert.ToString(_quotationItemDescription.QuotationItemQty),
                                    Unit = _quotationItemDescription.QuotationItemUnit,
                                    Rate = Convert.ToString(_quotationItemDescription.QuotationItemRate),
                                    Amount = Convert.ToString(_quotationItemDescription.QuotationItemAmount),
                                    Est_Cost = Convert.ToString(_quotationItemDescription.QuotationItemEstCost),
                                    AdjCost = null,
                                    Est_Profit = Convert.ToString(_quotationItemDescription.QuotationItemEstProfit),
                                    Remarks = _quotationItemDescription.QuotationItemRemarks,
                                    Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                                });
                                break;
                            }
                        }
                    }
                    else
                    {
                        resetNumber = 1;
                        ItemDescriptionRows.Add(new ItemDescriptionRow()
                        {
                            RowId = resetNumber,
                            Description = _quotationItemDescription.QuotationItem_Description,
                            Qty = Convert.ToString(_quotationItemDescription.QuotationItemQty),
                            Unit = _quotationItemDescription.QuotationItemUnit,
                            Rate = Convert.ToString(_quotationItemDescription.QuotationItemRate),
                            Amount = Convert.ToString(_quotationItemDescription.QuotationItemAmount),
                            Est_Cost = Convert.ToString(_quotationItemDescription.QuotationItemEstCost),
                            AdjCost = null,
                            Est_Profit = Convert.ToString(_quotationItemDescription.QuotationItemEstProfit),
                            Remarks = _quotationItemDescription.QuotationItemRemarks,
                            Mgr_Comments = _quotationItemDescription.QuotationItemMgrComments
                        });

                        quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                        {
                            TableID = countItemDescription,
                            CategoryName = _quotationItemDescription.CategoryName,
                            IsBasic = IsBasic,
                            ItemDescriptionRows = ItemDescriptionRows
                        });
                        countItemDescription++;
                    }
                }
                resetNumber++;
                //End
            }

            ViewData["QuotationItemDescription"] = quotationItemDescriptionBigTable;

            return View();
        }

        //Send Attachment PDF
        public ActionAsPdf SendQuotationEmail(int id)
        {

            Quotation quotation = _quotationsModel.GetSingle(id);

            return new Rotativa.ActionAsPdf("HtmlView", new { id = id })
            {
                FileName = quotation.QuotationID + "-quotation-" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf",
                PageSize = Rotativa.Options.Size.A4
            };
        }
    }
}