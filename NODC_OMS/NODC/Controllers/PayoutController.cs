﻿using DataAccess;
using DataAccess.POCO;
using Newtonsoft.Json;
using NODC_OMS.Helper;
using NODC_OMS.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NODC_OMS.Controllers
{
    [HandleError]
    [RedirectingAction]
    public class PayoutController : ControllerBase
    {
        private IPayoutRepository _payoutsModel;
        private IPayoutCalculationRepository _payoutCalculationsModel;
        private IEarlyPayoutRepository _earlyPayoutsModel;
        private IProjectRepository _projectsModel;
        private ICustomerRepository _customersModel;
        private IUserRepository _usersModel;

        public PayoutController():this(
            new PayoutRepository(),
            new PayoutCalculationRepository(),
            new EarlyPayoutRepository(),
            new ProjectRepository(),
            new CustomerRepository(),
            new UserRepository())
        {

        }

        public PayoutController(
            IPayoutRepository payoutsModel, 
            IPayoutCalculationRepository payoutCalculationsModel, 
            IEarlyPayoutRepository earlyPayoutsModel,
            IProjectRepository projectsModel,
            ICustomerRepository customersModel,
            IUserRepository usersModel)
        {
            _payoutsModel = payoutsModel;
            _payoutCalculationsModel = payoutCalculationsModel;
            _earlyPayoutsModel = earlyPayoutsModel;
            _projectsModel = projectsModel;
            _customersModel = customersModel;
            _usersModel = usersModel;
        }

        // GET: Payout
        public ActionResult Index()
        {
            if (Session["PayoutPayToId"] != null)
            {
                Session.Remove("PayoutPayToId");
            }

            if (Session["PayoutUserType"] != null)
            {
                Session.Remove("PayoutUserType");
            }

            string userRole = Session["UserGroup"].ToString();

            if (userRole == "Super Admin" || userRole == "Accounts")
            {
                return RedirectToAction("Select");
            }
            else
            {
                int userId = Convert.ToInt32(Session["MainID"]);
                string userType = "Salesperson";

                Session.Add("PayoutPayToId", userId);
                Session.Add("PayoutUserType", userType);

                return RedirectToAction("ViewPayout");
            }
        }

        //GET: Select
        public ActionResult Select()
        {
            string userRole = Session["UserGroup"].ToString();

            if (userRole != "Super Admin" && userRole != "Accounts")
            {
                return RedirectToAction("Index");
            }

            Dropdown[] payToDDL = PayToDDL();
            ViewData["PayToDropdown"] = new SelectList(payToDDL, "val", "name", "optGroup", null, null);

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Select
        [HttpPost]
        public ActionResult Select(FormCollection form)
        {
            if (string.IsNullOrEmpty(form["PayTo"]))
            {
                ModelState.AddModelError("PayTo", "Please select a user to view payout!");
            }

            if (ModelState.IsValid)
            {
                string[] payTo = form["PayTo"].ToString().Split('|');
                int userId = Convert.ToInt32(payTo[1]);
                string userType = payTo[0];

                Session.Add("PayoutPayToId", userId);
                Session.Add("PayoutUserType", userType);

                return RedirectToAction("ViewPayout");
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            Dropdown[] payToDDL = PayToDDL();
            ViewData["PayToDropdown"] = new SelectList(payToDDL, "val", "name", "optGroup", form["PayTo"], null);

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: ViewPayout
        public ActionResult ViewPayout(int page = 1)
        {
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            if (Session["PayoutPayToId"] == null)
            {
                return RedirectToAction("Index");
            }

            int payToId = Convert.ToInt32(Session["PayoutPayToId"]);
            string userType = Session["PayoutUserType"].ToString();

            IPagedList<Payout> payouts = _payoutsModel.GetPaged(payToId, userType, page, pageSize);
            ViewData["Payout"] = payouts;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: MakePayment
        public ActionResult MakePayment(int id)
        {
            if (Session["UserGroup"].ToString() != "Accounts" && Session["UserGroup"].ToString() != "Super Admin")
            {
                return RedirectToAction("Logout", "Access", new { @msg = "You have no access to this module!" });
            }

            Payout payout = _payoutsModel.GetSingle(id);

            if (payout != null)
            {
                if (payout.Status == "Unpaid")
                {
                    payout.Status = "Completed";

                    bool result = _payoutsModel.Update(id, payout);

                    if (result)
                    {
                        int userId = Convert.ToInt32(Session["MainID"]);
                        string userRole = Session["UserGroup"].ToString();
                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }
                        string tableAffected = "Payouts";
                        string description = userRole + " [" + Session["LoginID"].ToString() + "] Made Payment to Payout [" + payout.PayoutID + "]";

                        bool payout_update_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);

                        Session.Add("Result", "success|" + payout.PayoutID + " has been successfully paid!");
                    }
                    else
                    {
                        Session.Add("Result", "success|An error occured while updating payout record!");
                    }
                }
                else
                {
                    Session.Add("Result", "danger|" + payout.PayoutID + " has been processed!");
                }
            }
            else
            {
                Session.Add("Result", "danger|Payout record not found!");
            }

            int page = 1;

            if (Session["Page"] != null)
            {
                page = Convert.ToInt32(Session["Page"]);
            }

            return RedirectToAction("ViewPayout", new { @page = page });
        }

        //GET: MakePayment
        public ActionResult CancelPayment(int id)
        {
            if (Session["UserGroup"].ToString() != "Accounts" && Session["UserGroup"].ToString() != "Super Admin")
            {
                return RedirectToAction("Logout", "Access", new { @msg = "You have no access to this module!" });
            }

            Payout payout = _payoutsModel.GetSingle(id);

            if (payout != null)
            {
                if (payout.Status == "Completed")
                {
                    payout.Status = "Unpaid";

                    bool result = _payoutsModel.Update(id, payout);

                    if (result)
                    {
                        int userId = Convert.ToInt32(Session["MainID"]);
                        string userRole = Session["UserGroup"].ToString();
                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }
                        string tableAffected = "Payouts";
                        string description = userRole + " [" + Session["LoginID"].ToString() + "] Cancelled Payment to Payout [" + payout.PayoutID + "]";

                        bool payout_update_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);

                        Session.Add("Result", "success|" + payout.PayoutID + " has been successfully cancelled!");
                    }
                    else
                    {
                        Session.Add("Result", "success|An error occured while updating payout record!");
                    }
                }
                else
                {
                    Session.Add("Result", "danger|" + payout.PayoutID + " has not been processed!");
                }
            }
            else
            {
                Session.Add("Result", "danger|Payout record not found!");
            }

            int page = 1;

            if (Session["Page"] != null)
            {
                page = Convert.ToInt32(Session["Page"]);
            }

            return RedirectToAction("ViewPayout", new { @page = page });
        }

        //GET: GetPayoutCalculation
        public string GetPayoutCalculation(int id)
        {
            PayoutSummary summary = new PayoutSummary();

            Payout payout = _payoutsModel.GetSingle(id);

            if (payout != null)
            {
                decimal totalRevenue = 0;
                decimal totalCommission = 0;

                int userid = Convert.ToInt32(Session["MainID"]);
                string userRole = Session["UserGroup"].ToString();

                if (userRole == "Super Admin" || userRole == "Accounts")
                {
                    userid = payout.PayToId;
                }

                List<int[]> tiers = new List<int[]>();

                foreach (PayoutCalculation payoutCalculation in payout.PayoutCalculations.ToList())
                {
                    //Search Tier number, referring the level of downline
                    int tier = 1;
                    bool isDownline = false;

                    if (payoutCalculation.UserId == userid && payoutCalculation.UserType == "Salesperson")
                    {
                        isDownline = true;
                    }
                    else
                    {
                        tier++;

                        bool searchUpline = true;
                        int uplineId = payoutCalculation.UserId;
                        string uplineType = payoutCalculation.UserType;

                        do
                        {
                            if (uplineType == "Salesperson")
                            {
                                User uplineUser = _usersModel.GetSingle(uplineId);

                                if (uplineUser.UplineId == userid)
                                {
                                    searchUpline = false;
                                    isDownline = true;
                                }
                                else
                                {
                                    if (uplineUser.Role == "Super Admin" || (uplineUser.ID == uplineUser.UplineId))
                                    {
                                        searchUpline = false;
                                    }
                                    else
                                    {
                                        tier++;
                                        uplineId = uplineUser.UplineId;
                                    }
                                }
                            }
                            else
                            {
                                Customer uplineCustomer = _customersModel.GetSingle(uplineId);

                                if (uplineCustomer.UplineId == userid && uplineCustomer.UplineUserType == uplineType)
                                {
                                    searchUpline = false;
                                    isDownline = true;
                                }
                                else
                                {
                                    if (uplineCustomer.ID == uplineCustomer.UplineId)
                                    {
                                        searchUpline = false;
                                    }
                                    else
                                    {
                                        tier++;
                                        uplineId = uplineCustomer.UplineId;
                                        uplineType = uplineCustomer.UplineUserType;
                                    }
                                }
                            }
                        } while (searchUpline);
                    }

                    if (isDownline)
                    {
                        tiers.Add(new int[2] { tier, payoutCalculation.ID });

                        totalRevenue += payoutCalculation.Revenue;
                        totalCommission += payoutCalculation.Commission;
                    }
                    else
                    {
                        payout.PayoutCalculations.Remove(payoutCalculation);
                    }
                }

                ViewData["PayoutCalculation"] = payout.PayoutCalculations.ToList();
                ViewData["PayoutCalculationTier"] = tiers.OrderBy(e => e[0]).ToList();
                ViewData["TotalRevenue"] = totalRevenue.ToString("#,##0.00");
                ViewData["TotalCommission"] = totalCommission.ToString("#,##0.00");

                string payoutSummaryView = "";

                using (var sw = new StringWriter())
                {
                    var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, "GetPayoutCalculation");
                    var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                    viewResult.View.Render(viewContext, sw);
                    viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                    payoutSummaryView = sw.GetStringBuilder().ToString();
                }

                summary.Result = true;
                summary.PayoutSummaryView = payoutSummaryView;
            }
            else
            {
                summary.Result = false;
                summary.ErrorMessage = "Payout record not found!";
            }

            return JsonConvert.SerializeObject(summary);
        }

        //Pay To Dropdown
        public Dropdown[] PayToDDL()
        {
            IList<Customer> customers = _customersModel.GetActiveCustomers();
            IList<User> salepersons = _usersModel.GetAll("Sales", "Active");

            int size = customers.Count + salepersons.Count + 1;

            Dropdown[] ddl = new Dropdown[size];

            int count = 1;

            foreach (Customer customer in customers)
            {
                if (!string.IsNullOrEmpty(customer.CompanyName))
                {
                    ddl[count] = new Dropdown { name = customer.CustomerID + " - " + customer.CompanyName, val = "Customer|" + customer.ID, optGroup = "Customers" };
                }
                else
                {
                    ddl[count] = new Dropdown { name = customer.CustomerID + " - " + customer.ContactPerson, val = "Customer|" + customer.ID, optGroup = "Customers" };
                }

                count++;
            }

            foreach (User saleperson in salepersons)
            {
                ddl[count] = new Dropdown { name = saleperson.UserID + " - " + saleperson.Name, val = "Salesperson|" + saleperson.ID, optGroup = "Salesperson" };

                count++;
            }

            return ddl;
        }
    }
}