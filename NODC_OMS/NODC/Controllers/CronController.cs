﻿using DataAccess;
using DataAccess.PayoutDataTable;
using DataAccess.POCO;
using NODC_OMS.Helper;
using NODC_OMS.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NODC_OMS.Controllers
{
    public class CronController : Controller
    {
        private IPayoutRepository _payoutsModel;
        private IPayoutCalculationRepository _payoutCalculationsModel;
        private IEarlyPayoutRepository _earlyPayoutsModel;
        private IProjectRepository _projectsModel;
        private ICustomerRepository _customersModel;
        private IUserRepository _usersModel;
        private ISystemSettingRepository _settingsModel;

        public CronController():this(
            new PayoutRepository(),
            new PayoutCalculationRepository(),
            new EarlyPayoutRepository(),
            new ProjectRepository(),
            new CustomerRepository(),
            new UserRepository(),
            new SystemSettingRepository())
        {

        }

        public CronController(
            IPayoutRepository payoutsModel, 
            IPayoutCalculationRepository payoutCalculationsModel, 
            IEarlyPayoutRepository earlyPayoutsModel,
            IProjectRepository projectsModel,
            ICustomerRepository customersModel,
            IUserRepository usersModel,
            ISystemSettingRepository settingsModel)
        {
            _payoutsModel = payoutsModel;
            _payoutCalculationsModel = payoutCalculationsModel;
            _earlyPayoutsModel = earlyPayoutsModel;
            _projectsModel = projectsModel;
            _customersModel = customersModel;
            _usersModel = usersModel;
            _settingsModel = settingsModel;
        }

        //Task: Calculate Monthly Payout (Old Version)
        //public void CalculateMonthlyPayout(string execDate = null)
        //{
        //    DateTime now = DateTime.Now;
        //    if (!string.IsNullOrEmpty(execDate))
        //    {
        //        now = Convert.ToDateTime(execDate);
        //    }

        //    //Create Payout for Customers
        //    IList<Customer> customers = _customersModel.GetActiveCustomers();

        //    foreach (Customer customer in customers)
        //    {
        //        IList<Project> customerProjects = _projectsModel.GetAllRelatedCustomer(customer.ID, now);

        //        if (customerProjects.Count > 0)
        //        {
        //            decimal totalRevenue = 0;
        //            decimal totalExpense = 0;
        //            decimal totalProfit = 0;
        //            decimal totalComm = 0;
        //            decimal totalRetained = 0;
        //            decimal totalEarlyPayout = 0;
        //            decimal totalPayout = 0;

        //            foreach (Project project in customerProjects)
        //            {
        //                List<QuotationCoBroke> userCoBroke = project.Quotation.CoBrokes.Where(e => e.QuotationCoBrokeUserId == customer.ID && e.QuotationCoBrokeUserType == "Customer").ToList();

        //                if (userCoBroke.Count > 0)
        //                {
        //                    decimal projectRevenue = 0;
        //                    decimal percentage = 0;

        //                    foreach (QuotationCoBroke coBroke in userCoBroke)
        //                    {
        //                        percentage += coBroke.QuotationCoBrokePercentOfProfit;
        //                    }

        //                    percentage /= 100;

        //                    foreach (QuotationItemDescription revenue in project.Quotation.ItemDescriptions)
        //                    {
        //                        projectRevenue += revenue.QuotationItemAmount;
        //                    }

        //                    totalRevenue += projectRevenue * percentage;

        //                    foreach (ProjectProjectExpenses expense in project.ProjectedExpenses)
        //                    {
        //                        totalExpense += expense.Cost;
        //                    }
        //                }
        //            }

        //            totalProfit = totalRevenue - totalExpense;

        //            if (totalProfit != 0)
        //            {
        //                decimal commissionRate = Convert.ToDecimal(5.0 / 100);
        //                int tier = 1; //There is no overriding comm for customer, hence 1

        //                if (customer.Position == "Referral tier 2")
        //                {
        //                    commissionRate = Convert.ToDecimal(8.0 / 100);
        //                }
        //                else if (customer.Position == "Professional")
        //                {
        //                    commissionRate = Convert.ToDecimal(10.0 / 100);
        //                }

        //                totalComm = totalProfit * commissionRate;

        //                IList<EarlyPayout> customerEarlyPayouts = _earlyPayoutsModel.GetRelatedEarlyPayouts(customer.ID, "Customer", now);

        //                foreach (EarlyPayout earlyPayout in customerEarlyPayouts)
        //                {
        //                    totalEarlyPayout += earlyPayout.Amount;
        //                }

        //                //totalRetained = totalComm * 5 / 100;
        //                totalPayout = totalComm - totalRetained - totalEarlyPayout;

        //                string payoutID = _settingsModel.GetCodeValue("PREFIX_PAYOUT_ID") + DateTime.Now.ToString("yy");

        //                Payout lastRecord = _payoutsModel.GetLast();

        //                if (lastRecord != null)
        //                {
        //                    payoutID += (lastRecord.ID + 1).ToString().PadLeft(5, '0');
        //                }
        //                else
        //                {
        //                    payoutID += "00001";
        //                }

        //                Payout payout = new Payout()
        //                {
        //                    PayoutID = payoutID,
        //                    PayToId = customer.ID,
        //                    UserType = "Customer",
        //                    Period = now,
        //                    TotalCommissionAmount = totalComm,
        //                    TotalRetainedAmount = totalRetained,
        //                    TotalEarlyPayoutAmount = totalEarlyPayout,
        //                    TotalPayoutAmount = totalPayout,
        //                    Status = "Unpaid"
        //                };

        //                bool result = _payoutsModel.Add(payout);

        //                if (result)
        //                {
        //                    int userId = 0;
        //                    string userRole = "System";
        //                    string tableAffected = "Payouts";
        //                    string description = userRole + " Calculated Customer [" + customer.CustomerID + "] Monthly Payout for " + now.ToString("MMM yyyy");

        //                    bool payout_create_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);

        //                    PayoutCalculation payoutCalculation = new PayoutCalculation()
        //                    {
        //                        PayoutId = payout.ID,
        //                        Tier = tier,
        //                        UserId = customer.ID,
        //                        UserType = "Customer",
        //                        Revenue = totalRevenue,
        //                        PayoutPercentage = commissionRate * 100,
        //                        Commission = totalComm
        //                    };

        //                    bool result_PayoutCalculation = _payoutCalculationsModel.Add(payoutCalculation);

        //                    if (result_PayoutCalculation)
        //                    {
        //                        userId = 0;
        //                        userRole = "System";
        //                        tableAffected = "PayoutCalculations";
        //                        description = userRole + " Calculated Customer [" + customer.CustomerID + "] Monthly Payout Calculations for " + now.ToString("MMM yyyy");

        //                        bool payoutCalculation_create_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);
        //                    }

        //                    bool hasCompletedEarlyPayout = false;

        //                    foreach (EarlyPayout earlyPayout in customerEarlyPayouts)
        //                    {
        //                        bool result_complete_earlyPayout = _earlyPayoutsModel.CompletePayout(earlyPayout.ID);

        //                        if (result_complete_earlyPayout)
        //                        {
        //                            if (!hasCompletedEarlyPayout)
        //                            {
        //                                hasCompletedEarlyPayout = true;
        //                            }
        //                        }
        //                    }

        //                    if (hasCompletedEarlyPayout)
        //                    {
        //                        userId = 0;
        //                        userRole = "System";
        //                        tableAffected = "EarlyPayouts";
        //                        description = userRole + " Completed Customer [" + customer.CustomerID + "] Early Payouts for " + now.ToString("MMM yyyy");

        //                        bool earlyPayout_complete_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);
        //                    }

        //                    decimal totalProfitCompleted = customer.TotalCompletedProfit + payout.TotalCommissionAmount;
        //                    string promotion = "Referral Tier 1";
        //                    bool hasPromoted = false;

        //                    if (customer.Position == "Referral Tier 1")
        //                    {
        //                        if (totalProfitCompleted >= 1 && totalProfitCompleted < 20000)
        //                        {
        //                            promotion = "Referral Tier 2";
        //                            hasPromoted = true;
        //                        }
        //                        else if (totalProfitCompleted >= 20000)
        //                        {
        //                            promotion = "Professional";
        //                            hasPromoted = true;
        //                        }
        //                    }
        //                    else if (customer.Position == "Referral Tier 2")
        //                    {
        //                        if (totalProfitCompleted >= 20000)
        //                        {
        //                            promotion = "Professional";
        //                            hasPromoted = true;
        //                        }
        //                    }

        //                    bool result_customer_promotion = _customersModel.PromoteCustomerPosition(customer.ID, totalProfitCompleted, promotion);

        //                    if (result_customer_promotion)
        //                    {
        //                        if (hasPromoted)
        //                        {
        //                            userId = 0;
        //                            userRole = "System";
        //                            tableAffected = "Customers";
        //                            description = userRole + " Promoted Customer [" + customer.CustomerID + " - " + customer.Position + "] for " + now.ToString("MMM yyyy");

        //                            bool customerPromotion_update_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);
        //                        }
        //                        else
        //                        {
        //                            userId = 0;
        //                            userRole = "System";
        //                            tableAffected = "Customers";
        //                            description = userRole + " Updated Customer Total Completed Profit [" + customer.CustomerID + "] for " + now.ToString("MMM yyyy");

        //                            bool customerPromotion_update_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    //Create Payout for Salesperson
        //    List<MonthlyPayout> monthlyPayouts = new List<MonthlyPayout>();

        //    IList<User> salespersons = _usersModel.GetAll(new List<string>() { "Sales", "Super Admin" }, "Active");

        //    foreach (User salesperson in salespersons)
        //    {
        //        monthlyPayouts.Add(new MonthlyPayout()
        //        {
        //            UserId = salesperson.ID,
        //            UserID = salesperson.UserID,
        //            UserType = "Sales",
        //            UserRole = salesperson.Role,
        //            UserPosition = salesperson.Position,
        //            UserUplineId = salesperson.UplineId,
        //            HasPayout = false,
        //            Payout = new Payout()
        //            {
        //                PayToId = salesperson.ID,
        //                UserType = "Sales",
        //                Period = now,
        //                TotalCommissionAmount = 0,
        //                TotalRetainedAmount = 0,
        //                TotalEarlyPayoutAmount = 0,
        //                TotalPayoutAmount = 0,
        //                Status = "Unpaid",
        //                PayoutCalculations = new List<PayoutCalculation>()
        //            }
        //        });
        //    }

        //    foreach (MonthlyPayout userPayout in monthlyPayouts)
        //    {
        //        IList<Project> salespersonProjects = _projectsModel.GetAllRelatedSalesperson(userPayout.UserId, now);

        //        if (salespersonProjects.Count > 0)
        //        {
        //            userPayout.HasPayout = true;

        //            decimal totalRevenue = 0;
        //            decimal totalExpense = 0;
        //            decimal totalProfit = 0;
        //            decimal totalComm = 0;
        //            decimal totalEarlyPayout = 0;

        //            foreach (Project project in salespersonProjects)
        //            {
        //                foreach (QuotationItemDescription revenue in project.Quotation.ItemDescriptions)
        //                {
        //                    totalRevenue += (decimal)revenue.QuotationItemAmount;
        //                }

        //                foreach (ProjectProjectExpenses expense in project.ProjectedExpenses)
        //                {
        //                    totalExpense += expense.Cost;
        //                }
        //            }

        //            totalProfit = totalRevenue - totalExpense;

        //            decimal commissionRate = Convert.ToDecimal(60.0 / 100);
        //            int tier = 1; //Baseline

        //            if (userPayout.UserRole != "Super Admin")
        //            {
        //                switch (userPayout.UserPosition)
        //                {
        //                    case "Consultant":
        //                        commissionRate = Convert.ToDecimal(40.0 / 100);
        //                        break;
        //                    case "Assistant Manager":
        //                        commissionRate = Convert.ToDecimal(45.0 / 100);
        //                        break;
        //                    case "Manager":
        //                        commissionRate = Convert.ToDecimal(50.0 / 100);
        //                        break;
        //                    case "Senior Manager":
        //                        commissionRate = Convert.ToDecimal(55.0 / 100);
        //                        break;
        //                    default: break;
        //                }
        //            }

        //            totalComm = totalProfit * commissionRate;

        //            IList<EarlyPayout> salespersonEarlyPayouts = _earlyPayoutsModel.GetRelatedEarlyPayouts(userPayout.UserId, "Sales", now);

        //            foreach (EarlyPayout earlyPayout in salespersonEarlyPayouts)
        //            {
        //                totalEarlyPayout += earlyPayout.Amount;
        //            }

        //            userPayout.Payout.TotalCommissionAmount += totalComm;
        //            userPayout.Payout.TotalEarlyPayoutAmount += totalEarlyPayout;
        //            userPayout.Payout.PayoutCalculations.Add(new PayoutCalculation()
        //            {
        //                Tier = tier,
        //                UserId = userPayout.UserId,
        //                UserType = "Sales",
        //                Revenue = totalProfit,
        //                PayoutPercentage = commissionRate * 100,
        //                Commission = totalComm
        //            });

        //            //Search for Upline
        //            bool searchUpline = true;
        //            decimal commRemaining = Convert.ToDecimal(0.6) - commissionRate;

        //            if (commRemaining <= 0)
        //            {
        //                searchUpline = false;
        //            }

        //            int uplineId = userPayout.UserUplineId;

        //            while (searchUpline)
        //            {
        //                User upline = _usersModel.GetSingle(uplineId);

        //                if (upline != null)
        //                {
        //                    tier++;
        //                    decimal uplineComm = 0;
        //                    decimal uplineCommRate = Convert.ToDecimal(60.0 / 100) - commissionRate;

        //                    if (upline.Role != "Super Admin")
        //                    {
        //                        switch (upline.Position)
        //                        {
        //                            case "Consultant":
        //                                uplineCommRate = Convert.ToDecimal(40.0 / 100) - commissionRate;
        //                                break;
        //                            case "Assistant Manager":
        //                                uplineCommRate = Convert.ToDecimal(45.0 / 100) - commissionRate;
        //                                break;
        //                            case "Manager":
        //                                uplineCommRate = Convert.ToDecimal(50.0 / 100) - commissionRate;
        //                                break;
        //                            case "Senior Manager":
        //                                uplineCommRate = Convert.ToDecimal(55.0 / 100) - commissionRate;
        //                                break;
        //                            default: break;
        //                        }
        //                    }

        //                    //Reset Upline Comm Rate
        //                    commissionRate = Convert.ToDecimal(60.0 / 100);

        //                    if (upline.Role != "Super Admin")
        //                    {
        //                        switch (upline.Position)
        //                        {
        //                            case "Consultant":
        //                                commissionRate = Convert.ToDecimal(40.0 / 100);
        //                                break;
        //                            case "Assistant Manager":
        //                                commissionRate = Convert.ToDecimal(45.0 / 100);
        //                                break;
        //                            case "Manager":
        //                                commissionRate = Convert.ToDecimal(50.0 / 100);
        //                                break;
        //                            case "Senior Manager":
        //                                commissionRate = Convert.ToDecimal(55.0 / 100);
        //                                break;
        //                            default: break;
        //                        }
        //                    }

        //                    //If Upline Position is lower, upline' Comm Rate will be negative, hence no comm receive
        //                    if (uplineCommRate < 0)
        //                    {
        //                        uplineCommRate = 0;
        //                    }

        //                    //Check if comm rate reaches 60%
        //                    if (uplineCommRate >= commRemaining)
        //                    {
        //                        uplineCommRate = commRemaining;
        //                        searchUpline = false;
        //                    }
        //                    else
        //                    {
        //                        commRemaining -= uplineCommRate;
        //                    }

        //                    uplineComm = totalProfit * uplineCommRate;

        //                    //Add Comm to correspond salesperson
        //                    MonthlyPayout uplinePayout = monthlyPayouts.Where(e => e.UserId == upline.ID && e.UserType == "Sales").FirstOrDefault();

        //                    if (uplinePayout == null)
        //                    {
        //                        monthlyPayouts.Add(new MonthlyPayout()
        //                        {
        //                            UserId = upline.ID,
        //                            UserID = upline.UserID,
        //                            UserType = "Sales",
        //                            UserRole = upline.Role,
        //                            UserPosition = upline.Position,
        //                            UserUplineId = upline.UplineId,
        //                            HasPayout = true,
        //                            Payout = new Payout()
        //                            {
        //                                PayToId = upline.ID,
        //                                UserType = "Sales",
        //                                Period = now,
        //                                TotalCommissionAmount = uplineComm,
        //                                TotalRetainedAmount = 0,
        //                                TotalEarlyPayoutAmount = 0,
        //                                TotalPayoutAmount = 0,
        //                                Status = "Unpaid",
        //                                PayoutCalculations = new List<PayoutCalculation>(){
        //                                    new PayoutCalculation()
        //                                    {
        //                                        Tier = tier,
        //                                        UserId = userPayout.UserId,
        //                                        UserType = "Sales",
        //                                        Revenue = totalProfit,
        //                                        PayoutPercentage = uplineCommRate * 100,
        //                                        Commission = uplineComm
        //                                    }
        //                                }
        //                            }
        //                        });
        //                    }
        //                    else
        //                    {
        //                        foreach (MonthlyPayout upPayout in monthlyPayouts.Where(e => e.UserId == upline.ID && e.UserType == "Sales").ToList())
        //                        {
        //                            upPayout.HasPayout = true;
        //                            upPayout.Payout.TotalCommissionAmount += uplineComm;
        //                            upPayout.Payout.PayoutCalculations.Add(new PayoutCalculation()
        //                            {
        //                                Tier = tier,
        //                                UserId = userPayout.UserId,
        //                                UserType = "Sales",
        //                                Revenue = totalProfit,
        //                                PayoutPercentage = uplineCommRate * 100,
        //                                Commission = uplineComm
        //                            });
        //                        }
        //                    }

        //                    uplineId = upline.UplineId;
        //                }
        //                else
        //                {
        //                    searchUpline = false;
        //                }
        //            }
        //        }
        //    }

        //    //Remove salesperson that have no payout
        //    monthlyPayouts = monthlyPayouts.Where(e => e.HasPayout).ToList();

        //    //Create Payout for Salesperson
        //    foreach (MonthlyPayout salespersonPayout in monthlyPayouts)
        //    {
        //        salespersonPayout.Payout.TotalRetainedAmount = salespersonPayout.Payout.TotalCommissionAmount * Convert.ToDecimal(0.05);
        //        salespersonPayout.Payout.TotalPayoutAmount = salespersonPayout.Payout.TotalCommissionAmount - salespersonPayout.Payout.TotalRetainedAmount - salespersonPayout.Payout.TotalEarlyPayoutAmount;

        //        //Assign Payout ID
        //        string payoutID = _settingsModel.GetCodeValue("PREFIX_PAYOUT_ID") + DateTime.Now.ToString("yy");

        //        Payout lastRecord = _payoutsModel.GetLast();

        //        if (lastRecord != null)
        //        {
        //            payoutID += (lastRecord.ID + 1).ToString().PadLeft(5, '0');
        //        }
        //        else
        //        {
        //            payoutID += "00001";
        //        }

        //        Payout payout = new Payout()
        //        {
        //            PayoutID = payoutID,
        //            PayToId = salespersonPayout.UserId,
        //            UserType = "Sales",
        //            Period = now,
        //            TotalCommissionAmount = salespersonPayout.Payout.TotalCommissionAmount,
        //            TotalRetainedAmount = salespersonPayout.Payout.TotalRetainedAmount,
        //            TotalEarlyPayoutAmount = salespersonPayout.Payout.TotalEarlyPayoutAmount,
        //            TotalPayoutAmount = salespersonPayout.Payout.TotalPayoutAmount,
        //            Status = "Unpaid"
        //        };

        //        bool result = _payoutsModel.Add(payout);

        //        if (result)
        //        {
        //            int userId = 0;
        //            string userRole = "System";
        //            string tableAffected = "Payouts";
        //            string description = userRole + " Calculated Salesperson [" + salespersonPayout.UserID + "] Monthly Payout for " + now.ToString("MMM yyyy");

        //            bool payout_create_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);

        //            bool hasPayoutCalculation = false;

        //            foreach (PayoutCalculation payoutCalculation in salespersonPayout.Payout.PayoutCalculations)
        //            {
        //                payoutCalculation.PayoutId = payout.ID;

        //                bool result_payoutCalculation = _payoutCalculationsModel.Add(payoutCalculation);

        //                if (result_payoutCalculation)
        //                {
        //                    if (!hasPayoutCalculation)
        //                    {
        //                        hasPayoutCalculation = true;
        //                    }
        //                }
        //            }

        //            if (hasPayoutCalculation)
        //            {
        //                userId = 0;
        //                userRole = "System";
        //                tableAffected = "PayoutCalculations";
        //                description = userRole + " Calculated Salesperson [" + salespersonPayout.UserID + "] Monthly Payout Calculations for " + now.ToString("MMM yyyy");

        //                bool payoutCalculation_create_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);
        //            }

        //            IList<EarlyPayout> salespersonEarlyPayouts = _earlyPayoutsModel.GetRelatedEarlyPayouts(salespersonPayout.UserId, "Sales", now);

        //            bool hasCompletedEarlyPayout = false;

        //            foreach (EarlyPayout earlyPayout in salespersonEarlyPayouts)
        //            {
        //                bool result_complete_earlyPayout = _earlyPayoutsModel.CompletePayout(earlyPayout.ID);

        //                if (result_complete_earlyPayout)
        //                {
        //                    if (!hasCompletedEarlyPayout)
        //                    {
        //                        hasCompletedEarlyPayout = true;
        //                    }
        //                }
        //            }

        //            if (hasCompletedEarlyPayout)
        //            {
        //                userId = 0;
        //                userRole = "System";
        //                tableAffected = "EarlyPayouts";
        //                description = userRole + " Completed Salesperson [" + salespersonPayout.UserID + "] Early Payouts for " + now.ToString("MMM yyyy");

        //                bool earlyPayout_complete_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);
        //            }

        //            //Update and Promote Salesperson
        //            User salespersonPromo = _usersModel.GetSingle(salespersonPayout.UserId);

        //            decimal totalCompletedProfit = salespersonPromo.TotalCompletedProfit + salespersonPayout.Payout.TotalCommissionAmount;
        //            string promotion = salespersonPromo.Position;
        //            bool hasPromoted = false;

        //            if (salespersonPromo.Role == "Sales")
        //            {
        //                IList<User> managersAbove = _usersModel.GetDownlineUsers(salespersonPromo.ID, new List<string>() { "Manager", "Senior Manager", "Director" });
        //                int managerAboveCount = managersAbove.Count;
        //                int seniorManagerAboveCount = managersAbove.Where(e => e.Position != "Manager").ToList().Count;

        //                if (salespersonPromo.Position == "Consultant")
        //                {
        //                    if (seniorManagerAboveCount >= 3)
        //                    {
        //                        promotion = "Director";
        //                        hasPromoted = true;
        //                    }
        //                    else
        //                    {
        //                        if (totalCompletedProfit >= 60000 && totalCompletedProfit < 120000)
        //                        {
        //                            promotion = "Assistant Manager";
        //                            hasPromoted = true;
        //                        }
        //                        else if (totalCompletedProfit >= 120000 && totalCompletedProfit < 370000)
        //                        {
        //                            promotion = "Manager";
        //                            hasPromoted = true;
        //                        }
        //                        else if (totalCompletedProfit >= 370000)
        //                        {
        //                            if (managerAboveCount >= 3)
        //                            {
        //                                promotion = "Senior Manager";
        //                                hasPromoted = true;
        //                            }
        //                            else
        //                            {
        //                                promotion = "Manager";
        //                                hasPromoted = true;
        //                            }
        //                        }
        //                    }
        //                }
        //                else if (salespersonPromo.Position == "Assistant Manager")
        //                {
        //                    if (seniorManagerAboveCount >= 3)
        //                    {
        //                        promotion = "Director";
        //                        hasPromoted = true;
        //                    }
        //                    else
        //                    {
        //                        if (totalCompletedProfit >= 120000 && totalCompletedProfit < 370000)
        //                        {
        //                            promotion = "Manager";
        //                            hasPromoted = true;
        //                        }
        //                        else if (totalCompletedProfit >= 370000)
        //                        {
        //                            if (managerAboveCount >= 3)
        //                            {
        //                                promotion = "Senior Manager";
        //                                hasPromoted = true;
        //                            }
        //                            else
        //                            {
        //                                promotion = "Manager";
        //                                hasPromoted = true;
        //                            }
        //                        }
        //                    }
        //                }
        //                else if (salespersonPromo.Position == "Manager")
        //                {
        //                    if (seniorManagerAboveCount >= 3)
        //                    {
        //                        promotion = "Director";
        //                        hasPromoted = true;
        //                    }
        //                    else
        //                    {
        //                        if (totalCompletedProfit >= 370000)
        //                        {
        //                            if (managerAboveCount >= 3)
        //                            {
        //                                promotion = "Senior Manager";
        //                                hasPromoted = true;
        //                            }
        //                        }
        //                    }
        //                }
        //                else if (salespersonPromo.Position == "Senior Manager")
        //                {
        //                    if (seniorManagerAboveCount >= 3)
        //                    {
        //                        promotion = "Director";
        //                        hasPromoted = true;
        //                    }
        //                }
        //            }

        //            bool result_salesperson_promotion = _usersModel.PromoteUser(salespersonPromo.ID, totalCompletedProfit, promotion);

        //            if (result_salesperson_promotion)
        //            {
        //                if (hasPromoted)
        //                {
        //                    userId = 0;
        //                    userRole = "System";
        //                    tableAffected = "Users";
        //                    description = userRole + " Promoted Salesperson [" + salespersonPromo.UserID + " - " + salespersonPromo.Position + "] for " + now.ToString("MMM yyyy");

        //                    bool salespersonPromotion_update_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);
        //                }
        //                else
        //                {
        //                    userId = 0;
        //                    userRole = "System";
        //                    tableAffected = "Users";
        //                    description = userRole + " Updated Salesperson Total Completed Profit [" + salespersonPromo.UserID + "] for " + now.ToString("MMM yyyy");

        //                    bool salespersonPromotion_update_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);
        //                }
        //            }
        //        }
        //    }
        //}


        //Task: Calculate Monthly Payout (New Version after 1 Aug 2017)
        public void CalculateMonthlyPayout(string execDate = null)
        {
            DateTime now = DateTime.Now;

            if (!string.IsNullOrEmpty(execDate))
            {
                now = Convert.ToDateTime(execDate);
            }

            Response.Write("Calculate Monthly Payout executed on " + now.ToString("dd/MM/yyyy HH:mm:ss"));

            List<Payout> payouts = new List<Payout>();

            IList<Project> completedProjects = _projectsModel.GetCompletedProjects(now);

            foreach (Project project in completedProjects)
            {
                decimal totalRevenue = 0;
                decimal totalExpense = 0;
                decimal totalProjectProfit = 0;

                foreach (QuotationItemDescription revenue in project.Quotation.ItemDescriptions)
                {
                    totalRevenue += revenue.QuotationItemAmount;
                }

                foreach (ProjectProjectExpenses expense in project.ProjectedExpenses)
                {
                    totalExpense += expense.Cost;
                }

                totalProjectProfit = totalRevenue - totalExpense;

                decimal projectProfit = Convert.ToDecimal(ConfigurationManager.AppSettings["ProjectMaxProfit"]);

                //Customer Payout
                decimal maxCustomerProfit = Convert.ToDecimal(ConfigurationManager.AppSettings["CustomerMaxProfit"]);
                decimal customerProfitAcquired = 0;
                decimal customerCurrentComm = 0;

                bool searchCustomerUpline = true;

                Customer projectCustomer = _customersModel.GetSingle(project.CustomerId);

                int uplineCustomerId = projectCustomer.UplineId;

                if (projectCustomer.UplineUserType != "Customer")
                {
                    searchCustomerUpline = false;
                }

                while (searchCustomerUpline)
                {
                    Customer uplineCustomer = _customersModel.GetSingle(uplineCustomerId);

                    decimal customerRate = 10;

                    switch (uplineCustomer.Position)
                    {
                        case "Premium Customer": customerRate = 8; break;
                        case "Customer": customerRate = 5; break;
                        default: break;
                    }

                    decimal overridingComm = customerRate - customerCurrentComm;
                    if (customerProfitAcquired + overridingComm > maxCustomerProfit)
                    {
                        overridingComm = (customerProfitAcquired + overridingComm) - maxCustomerProfit;
                    }

                    //Continue if only Overriding Comm is positive
                    if (overridingComm > 0)
                    {
                        customerCurrentComm = customerRate;
                        customerProfitAcquired += overridingComm;

                        decimal customerComm = totalProjectProfit * overridingComm / 100;

                        if (payouts.Count > 0)
                        {
                            Payout payout = payouts.Where(e => e.PayToId == uplineCustomer.ID && e.UserType == "Customer").FirstOrDefault();

                            if (payout != null)
                            {
                                payout.PayoutCalculations.Add(new PayoutCalculation()
                                {
                                    UserId = project.CustomerId,
                                    UserType = "Customer",
                                    Revenue = totalProjectProfit,
                                    PayoutPercentage = overridingComm,
                                    Commission = customerComm
                                });
                            }
                            else
                            {
                                payouts.Add(new Payout()
                                {
                                    PayToId = uplineCustomer.ID,
                                    UserType = "Customer",
                                    Period = now,
                                    TotalCommissionAmount = 0,
                                    TotalRetainedAmount = 0,
                                    TotalEarlyPayoutAmount = 0,
                                    TotalPayoutAmount = 0,
                                    Status = "Unpaid",
                                    PayoutCalculations = new List<PayoutCalculation> {
                                    new PayoutCalculation() {
                                        UserId = project.CustomerId,
                                        UserType = "Customer",
                                        Revenue = totalProjectProfit,
                                        PayoutPercentage = overridingComm,
                                        Commission = customerComm
                                    }
                                }
                                });
                            }
                        }
                        else
                        {
                            payouts.Add(new Payout()
                            {
                                PayToId = uplineCustomer.ID,
                                UserType = "Customer",
                                Period = now,
                                TotalCommissionAmount = 0,
                                TotalRetainedAmount = 0,
                                TotalEarlyPayoutAmount = 0,
                                TotalPayoutAmount = 0,
                                Status = "Unpaid",
                                PayoutCalculations = new List<PayoutCalculation> {
                                    new PayoutCalculation() {
                                        UserId = project.CustomerId,
                                        UserType = "Customer",
                                        Revenue = totalProjectProfit,
                                        PayoutPercentage = overridingComm,
                                        Commission = customerComm
                                    }
                                }
                            });
                        }
                    }

                    if (customerProfitAcquired >= maxCustomerProfit || uplineCustomer.UplineUserType != "Customer")
                    {
                        searchCustomerUpline = false;
                    }
                    else
                    {
                        uplineCustomerId = uplineCustomer.UplineId;
                    }
                }

                projectProfit -= customerProfitAcquired;

                //Salesperson Payout
                decimal salespersonProfitAcquired = 0;
                decimal salespersonCurrentComm = 0;

                //User salesperson = _usersModel.GetSingle(project.CoordinatorId);

                int salespersonUplineId = project.CoordinatorId;
                bool searchSalespersonUpline = true;

                while (searchSalespersonUpline)
                {
                    User salespersonUpline = _usersModel.GetSingle(salespersonUplineId);

                    decimal salespersonRate = 60;

                    if (salespersonUpline.Role != "Super Admin")
                    {
                        switch (salespersonUpline.Position)
                        {
                            case "Senior Manager": salespersonRate = 55; break;
                            case "Manager": salespersonRate = 50; break;
                            case "Assistant Manager": salespersonRate = 45; break;
                            case "Consultant": salespersonRate = 40; break;
                            default: break;
                        }
                    }

                    decimal salespersonComm = salespersonRate - salespersonCurrentComm;
                    if (salespersonComm > projectProfit)
                    {
                        salespersonComm = projectProfit;
                    }

                    salespersonProfitAcquired += salespersonComm;
                    salespersonCurrentComm = salespersonRate;

                    if (salespersonComm > 0)
                    {
                        List<QuotationCoBroke> coBrokes = project.Quotation.CoBrokes.Where(e => e.QuotationCoBrokeUserId == salespersonUpline.ID).ToList();

                        if (coBrokes.Count > 0)
                        {
                            decimal salespersonCommAmt = totalProjectProfit * salespersonComm / 100;

                            decimal totalCoBroke = 0;

                            foreach (QuotationCoBroke coBroke in coBrokes)
                            {
                                totalCoBroke += coBroke.QuotationCoBrokePercentOfProfit;
                            }

                            salespersonCommAmt *= totalCoBroke / 100;
                            salespersonComm *= totalCoBroke / 100;

                            if (payouts.Count > 0)
                            {
                                Payout payout = payouts.Where(e => e.PayToId == salespersonUpline.ID && e.UserType == "Salesperson").FirstOrDefault();

                                if (payout != null)
                                {
                                    payout.PayoutCalculations.Add(new PayoutCalculation()
                                    {
                                        UserId = project.CoordinatorId,
                                        UserType = "Salesperson",
                                        Revenue = totalProjectProfit,
                                        PayoutPercentage = salespersonComm,
                                        Commission = salespersonCommAmt
                                    });
                                }
                                else
                                {
                                    payouts.Add(new Payout()
                                    {
                                        PayToId = salespersonUpline.ID,
                                        UserType = "Salesperson",
                                        Period = now,
                                        TotalCommissionAmount = 0,
                                        TotalRetainedAmount = 0,
                                        TotalEarlyPayoutAmount = 0,
                                        TotalPayoutAmount = 0,
                                        Status = "Unpaid",
                                        PayoutCalculations = new List<PayoutCalculation> {
                                            new PayoutCalculation() {
                                                UserId = project.CoordinatorId,
                                                UserType = "Salesperson",
                                                Revenue = totalProjectProfit,
                                                PayoutPercentage = salespersonComm,
                                                Commission = salespersonCommAmt
                                            }
                                        }
                                    });
                                }
                            }
                            else
                            {
                                payouts.Add(new Payout()
                                {
                                    PayToId = salespersonUpline.ID,
                                    UserType = "Salesperson",
                                    Period = now,
                                    TotalCommissionAmount = 0,
                                    TotalRetainedAmount = 0,
                                    TotalEarlyPayoutAmount = 0,
                                    TotalPayoutAmount = 0,
                                    Status = "Unpaid",
                                    PayoutCalculations = new List<PayoutCalculation> {
                                        new PayoutCalculation() {
                                            UserId = project.CoordinatorId,
                                            UserType = "Salesperson",
                                            Revenue = totalProjectProfit,
                                            PayoutPercentage = salespersonComm,
                                            Commission = salespersonCommAmt
                                        }
                                    }
                                });
                            }

                            projectProfit -= salespersonComm;
                        }
                    }

                    if (projectProfit <= 0 || salespersonUpline.Role == "Super Admin")
                    {
                        searchSalespersonUpline = false;
                    }
                    else
                    {
                        salespersonUplineId = salespersonUpline.UplineId;
                    }
                }
            }

            if (payouts.Count > 0)
            {
                foreach (Payout payout in payouts)
                {
                    string userID = "";

                    foreach (PayoutCalculation payoutCalculation in payout.PayoutCalculations)
                    {
                        payout.TotalCommissionAmount += payoutCalculation.Commission;
                    }

                    if (payout.UserType == "Customer")
                    {
                        Customer customer = _customersModel.GetSingle(payout.PayToId);
                        userID = customer.CustomerID;

                        IList<EarlyPayout> earlyPayouts = _earlyPayoutsModel.GetRelatedEarlyPayouts(customer.ID, "Customer", now, "Unpaid");

                        foreach (EarlyPayout earlyPayout in earlyPayouts)
                        {
                            payout.TotalEarlyPayoutAmount += earlyPayout.Amount;
                        }
                    }
                    else
                    {
                        User user = _usersModel.GetSingle(payout.PayToId);
                        userID = user.UserID;

                        IList<EarlyPayout> earlyPayouts = _earlyPayoutsModel.GetRelatedEarlyPayouts(user.ID, "Salesperson", now, "Unpaid");

                        foreach (EarlyPayout earlyPayout in earlyPayouts)
                        {
                            payout.TotalEarlyPayoutAmount += earlyPayout.Amount;
                        }

                        payout.TotalRetainedAmount = payout.TotalCommissionAmount * Convert.ToDecimal(ConfigurationManager.AppSettings["RetainedAmountPercentage"]) / 100;
                    }

                    payout.TotalPayoutAmount = payout.TotalCommissionAmount - payout.TotalRetainedAmount - payout.TotalEarlyPayoutAmount;

                    //Assign Payout ID
                    string payoutID = _settingsModel.GetCodeValue("PREFIX_PAYOUT_ID") + DateTime.Now.ToString("yy");

                    Payout lastRecord = _payoutsModel.GetLast();

                    if (lastRecord != null)
                    {
                        payoutID += (lastRecord.ID + 1).ToString().PadLeft(5, '0');
                    }
                    else
                    {
                        payoutID += "00001";
                    }

                    Payout newPayout = new Payout();
                    newPayout.PayoutID = payoutID;
                    newPayout.PayToId = payout.PayToId;
                    newPayout.UserType = payout.UserType;
                    newPayout.Period = payout.Period;
                    newPayout.TotalCommissionAmount = payout.TotalCommissionAmount;
                    newPayout.TotalRetainedAmount = payout.TotalRetainedAmount;
                    newPayout.TotalEarlyPayoutAmount = payout.TotalEarlyPayoutAmount;
                    newPayout.TotalPayoutAmount = payout.TotalPayoutAmount;
                    newPayout.Status = payout.Status;

                    bool result = _payoutsModel.Add(newPayout);

                    if (result)
                    {
                        int userId = 0;
                        string userRole = "System";
                        string tableAffected = "Payouts";
                        string description = userRole + " Calculated " + newPayout.UserType + " [" + userID + "] Monthly Payout for " + now.ToString("MMM yyyy");

                        bool payout_create_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);

                        bool hasPayoutCalculation = false;

                        foreach (PayoutCalculation payoutCalculation in payout.PayoutCalculations)
                        {
                            payoutCalculation.PayoutId = newPayout.ID;

                            bool result_payoutCalculation = _payoutCalculationsModel.Add(payoutCalculation);

                            if (result_payoutCalculation)
                            {
                                if (!hasPayoutCalculation)
                                {
                                    hasPayoutCalculation = true;
                                }
                            }
                        }

                        if (hasPayoutCalculation)
                        {
                            userId = 0;
                            userRole = "System";
                            tableAffected = "PayoutCalculations";
                            description = userRole + " Calculated " + newPayout.UserType + " [" + userID + "] Monthly Payout Calculations for " + now.ToString("MMM yyyy");

                            bool payoutCalculation_create_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);
                        }

                        //Clear Early Payout
                        IList<EarlyPayout> earlyPayouts = _earlyPayoutsModel.GetRelatedEarlyPayouts(newPayout.PayToId, newPayout.UserType, now);

                        bool hasCompletedEarlyPayout = false;

                        foreach (EarlyPayout earlyPayout in earlyPayouts)
                        {
                            bool result_complete_earlyPayout = _earlyPayoutsModel.CompletePayout(earlyPayout.ID);

                            if (result_complete_earlyPayout)
                            {
                                if (!hasCompletedEarlyPayout)
                                {
                                    hasCompletedEarlyPayout = true;
                                }
                            }
                        }

                        if (hasCompletedEarlyPayout)
                        {
                            userId = 0;
                            userRole = "System";
                            tableAffected = "EarlyPayouts";
                            description = userRole + " Completed " + newPayout.UserType + " [" + userID + "] Early Payouts for " + now.ToString("MMM yyyy");

                            bool earlyPayout_complete_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);
                        }

                        //Update and Promotion if Salesperson/User
                        if (newPayout.UserType == "Customer")
                        {
                            Customer customer = _customersModel.GetSingle(newPayout.PayToId);

                            decimal totalProfitCompleted = customer.TotalCompletedProfit + payout.TotalCommissionAmount;
                            string promotion = "Customer";
                            bool hasPromoted = false;

                            if (customer.Position == "Customer")
                            {
                                if (totalProfitCompleted >= 1 && totalProfitCompleted < 20000)
                                {
                                    promotion = "Premium Customer";
                                    hasPromoted = true;
                                }
                                else if (totalProfitCompleted >= 20000)
                                {
                                    promotion = "Professional";
                                    hasPromoted = true;
                                }
                            }
                            else if (customer.Position == "Premium Customer")
                            {
                                if (totalProfitCompleted >= 20000)
                                {
                                    promotion = "Professional";
                                    hasPromoted = true;
                                }
                            }

                            bool result_customer_promotion = _customersModel.PromoteCustomerPosition(customer.ID, totalProfitCompleted, promotion);

                            if (result_customer_promotion)
                            {
                                if (hasPromoted)
                                {
                                    userId = 0;
                                    userRole = "System";
                                    tableAffected = "Customers";
                                    description = userRole + "Updated Customer Total Completed Profit and Promoted Customer [" + customer.CustomerID + " - " + customer.Position + "] for " + now.ToString("MMM yyyy");

                                    bool customerPromotion_update_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);
                                }
                                else
                                {
                                    userId = 0;
                                    userRole = "System";
                                    tableAffected = "Customers";
                                    description = userRole + " Updated Customer Total Completed Profit [" + customer.CustomerID + "] for " + now.ToString("MMM yyyy");

                                    bool customerPromotion_update_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);
                                }
                            }
                        }
                        else
                        {
                            User salespersonPromo = _usersModel.GetSingle(newPayout.PayToId);

                            decimal totalCompletedProfit = salespersonPromo.TotalCompletedProfit + payout.TotalCommissionAmount;
                            string promotion = salespersonPromo.Position;
                            bool hasPromoted = false;

                            if (salespersonPromo.Role == "Sales")
                            {
                                IList<User> managersAbove = _usersModel.GetDownlineUsers(salespersonPromo.ID, new List<string>() { "Manager", "Senior Manager", "Director" });
                                int managerAboveCount = managersAbove.Count;
                                int seniorManagerAboveCount = managersAbove.Where(e => e.Position != "Manager").ToList().Count;

                                if (salespersonPromo.Position == "Consultant")
                                {
                                    if (seniorManagerAboveCount >= 3)
                                    {
                                        promotion = "Director";
                                        hasPromoted = true;
                                    }
                                    else
                                    {
                                        if (totalCompletedProfit >= 60000 && totalCompletedProfit < 120000)
                                        {
                                            promotion = "Assistant Manager";
                                            hasPromoted = true;
                                        }
                                        else if (totalCompletedProfit >= 120000 && totalCompletedProfit < 370000)
                                        {
                                            promotion = "Manager";
                                            hasPromoted = true;
                                        }
                                        else if (totalCompletedProfit >= 370000)
                                        {
                                            if (managerAboveCount >= 3)
                                            {
                                                promotion = "Senior Manager";
                                                hasPromoted = true;
                                            }
                                            else
                                            {
                                                promotion = "Manager";
                                                hasPromoted = true;
                                            }
                                        }
                                    }
                                }
                                else if (salespersonPromo.Position == "Assistant Manager")
                                {
                                    if (seniorManagerAboveCount >= 3)
                                    {
                                        promotion = "Director";
                                        hasPromoted = true;
                                    }
                                    else
                                    {
                                        if (totalCompletedProfit >= 120000 && totalCompletedProfit < 370000)
                                        {
                                            promotion = "Manager";
                                            hasPromoted = true;
                                        }
                                        else if (totalCompletedProfit >= 370000)
                                        {
                                            if (managerAboveCount >= 3)
                                            {
                                                promotion = "Senior Manager";
                                                hasPromoted = true;
                                            }
                                            else
                                            {
                                                promotion = "Manager";
                                                hasPromoted = true;
                                            }
                                        }
                                    }
                                }
                                else if (salespersonPromo.Position == "Manager")
                                {
                                    if (seniorManagerAboveCount >= 3)
                                    {
                                        promotion = "Director";
                                        hasPromoted = true;
                                    }
                                    else
                                    {
                                        if (totalCompletedProfit >= 370000)
                                        {
                                            if (managerAboveCount >= 3)
                                            {
                                                promotion = "Senior Manager";
                                                hasPromoted = true;
                                            }
                                        }
                                    }
                                }
                                else if (salespersonPromo.Position == "Senior Manager")
                                {
                                    if (seniorManagerAboveCount >= 3)
                                    {
                                        promotion = "Director";
                                        hasPromoted = true;
                                    }
                                }
                            }

                            bool result_salesperson_promotion = _usersModel.PromoteUser(salespersonPromo.ID, totalCompletedProfit, promotion);

                            if (result_salesperson_promotion)
                            {
                                if (hasPromoted)
                                {
                                    userId = 0;
                                    userRole = "System";
                                    tableAffected = "Users";
                                    description = userRole + " Updated Salesperson Total Completed Profit and Promoted Salesperson [" + salespersonPromo.UserID + " - " + salespersonPromo.Position + "] for " + now.ToString("MMM yyyy");

                                    bool salespersonPromotion_update_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);
                                }
                                else
                                {
                                    userId = 0;
                                    userRole = "System";
                                    tableAffected = "Users";
                                    description = userRole + " Updated Salesperson Total Completed Profit [" + salespersonPromo.UserID + "] for " + now.ToString("MMM yyyy");

                                    bool salespersonPromotion_update_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);
                                }
                            }
                        }
                    }
                }
            }
        }

        //Task: Sales Reminder
        public void SalesReminder(string execDate = null)
        {
            DateTime now = DateTime.Now;

            if (!string.IsNullOrEmpty(execDate))
            {
                now = Convert.ToDateTime(execDate);
            }

            Response.Write("Sales Reminder executed on " + now.ToString("dd/MM/yyyy HH:mm:ss"));

            IList<User> salesStaffs = _usersModel.GetAll("Sales", "Active").Where(e => e.Reminder == "Yes" && e.Email != null).ToList();

            foreach (User sales in salesStaffs)
            {
                //Get Salesperson project
                IList<Project> projects = _projectsModel.GetSalesReminder(sales.ID, "Active", null);

                if (projects.Count > 0)
                {
                    List<string> activeProjects = new List<string>();

                    foreach (Project project in projects)
                    {
                        activeProjects.Add("<li style='margin-bottom:10px;'><a href='" + Url.Action("Edit", "Project", new { @id = project.ID }, "http") + "' target='_blank'>" + project.ProjectTitle + " - " + project.ProjectID + "</a></li>");
                    }

                    string subject = "Sales Reminder " + now.ToString("MMMM yyyy");
                    string body = System.IO.File.ReadAllText(Server.MapPath("~/Templates/SalesReminder.html"));
                    string recipient = sales.Email;
                    string name = sales.Name;

                    ListDictionary replacements = new ListDictionary();

                    replacements.Add("<%Name%>", name);
                    replacements.Add("<%Projects%>", String.Join("", activeProjects));


                    bool sent = EmailHelper.SendEmail(subject, body, replacements, recipient);
                }
            }
        }

        //Task: Customer Events Reminder
        public void CustomerEventsReminder(string execDate = null)
        {
            DateTime now = DateTime.Now;

            if (!string.IsNullOrEmpty(execDate))
            {
                now = Convert.ToDateTime(execDate);
            }

            Response.Write("Customer Events Reminder executed on " + now.ToString("dd/MM/yyyy HH:mm:ss"));

            IList<User> salesStaffs = _usersModel.GetAll("Sales", "Active").Where(e => e.Email != null).ToList();

            foreach (User sales in salesStaffs)
            {
                //Get Salesperson Assigned Customer
                IList<Customer> customers = _customersModel.GetCustomerEventsReminder(sales.ID, new List<string>() { "Active" }, now.Date);

                if (customers.Count > 0)
                {
                    List<string> customersList = new List<string>();

                    foreach (Customer customer in customers)
                    {
                        List<string> reminderList = new List<string>();

                        foreach (CustomerReminder reminder in customer.CustomerReminders)
                        {
                            DateTime reminderDate = reminder.Date;
                            bool isMatch = false;

                            if (reminder.Repeat == "Yearly")
                            {
                                for (int addYear = 0; reminderDate.AddYears(addYear).Date <= now.Date && !isMatch;addYear++)
                                {
                                    if (reminderDate.AddYears(addYear).Date == now.Date)
                                    {
                                        isMatch = true;
                                        break;
                                    }
                                }
                            }
                            else if (reminder.Repeat == "Monthly")
                            {
                                for (int addMonth = 0; reminderDate.AddMonths(addMonth).Date <= now.Date && !isMatch; addMonth++)
                                {
                                    if (reminderDate.AddMonths(addMonth).Date == now.Date)
                                    {
                                        isMatch = true;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                if (reminderDate.Date <= now.Date)
                                {
                                    isMatch = true;
                                }
                            }

                            if (isMatch)
                            {
                                if (reminderList.Count == 0)
                                {
                                    reminderList.Add("<div style='padding-left:15px;margin-top:5px;margin-bottom:5px;'>- " + reminder.Description + " - " + reminder.Date.ToString("dd/MM/yyyy") + " " + reminder.Repeat + "</div>");
                                }
                                else
                                {
                                    reminderList.Add("<div style='padding-left:15px;margin-bottom:5px;'>- " + reminder.Description + " - " + reminder.Date.ToString("dd/MM/yyyy") + " " + reminder.Repeat + "</div>");
                                }
                            }
                        }

                        if (reminderList.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(customer.CompanyName))
                            {
                                customersList.Add("<li style='margin-bottom:15px;'><a href='" + Url.Action("Edit", "Customer", new { @id = customer.ID }, "http") + "' target='_blank'>" + customer.CompanyName + " - " + customer.CustomerID + "</a>" + String.Join("", reminderList) + "</li>");
                            }
                            else
                            {
                                customersList.Add("<li style='margin-bottom:15px;'><a href='" + Url.Action("Edit", "Customer", new { @id = customer.ID }, "http") + "' target='_blank'>" + customer.ContactPerson + " - " + customer.CustomerID + "</a>" + String.Join("", reminderList) + "</li>");
                            }
                        }
                    }

                    if (customersList.Count > 0)
                    {
                        string subject = "Customer Events Reminder " + now.ToString("d MMMM yyyy");
                        string body = System.IO.File.ReadAllText(Server.MapPath("~/Templates/CustomerEventsReminder.html"));
                        string recipient = sales.Email;
                        string name = sales.Name;

                        ListDictionary replacements = new ListDictionary();

                        replacements.Add("<%Name%>", name);
                        replacements.Add("<%Date%>", now.ToString("d MMMM yyyy"));
                        replacements.Add("<%Reminders%>", String.Join("", customersList));


                        bool sent = EmailHelper.SendEmail(subject, body, replacements, recipient);
                    }
                }
            }
        }
    }
}