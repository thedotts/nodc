﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Web.Services;
using System.Configuration;
using System.Data.SqlClient;
using NODC_OMS.Models;
using DataAccess;
using DataAccess.POCO;
using Newtonsoft.Json;

namespace NODC_OMS.Controllers
{
    [HandleError]
    [RedirectingAction]
    public class OrganisationController : ControllerBase
    {

        private IUserRepository _usersModel;
        private ICustomerRepository _customersModel;

        // GET: Organisation
        public OrganisationController()
            : this(new UserRepository(), new CustomerRepository())
        {

        }

        public OrganisationController(IUserRepository usersModel, ICustomerRepository customersModel)
        {
            _usersModel = usersModel;
            _customersModel = customersModel;
        }

        // GET: Organisation
        public ActionResult Index()
        {
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //Try Organisation Chart
        [WebMethod]
        public string GetChartData()
        {
            //if login user role is super admin, run all.
            //if login user role not super admin, start from the current login id and roll.

            int uid = Convert.ToInt32(Session["MainID"]);

            User getSinglerUser = _usersModel.GetSingle(uid);

            List<Data> chartData = new List<Data>();

            if(getSinglerUser.Role == "Super Admin")
            {
                IList<User> getAllUser = _usersModel.GetAll();
                IList<Customer> getAllCustomer = _customersModel.GetAll();

                foreach (User _getAllUser in getAllUser)
                {

                    string Role = _getAllUser.Role;

                    if (Role == "Sales")
                    {
                        Role += " " + _getAllUser.Position;
                    }

                    string AgencyLeader = _getAllUser.IsAgencyLeader;

                    if (AgencyLeader == "Yes")
                    {
                        AgencyLeader = "Agency Leader";
                    }
                    else
                    {
                        AgencyLeader = "";
                    }

                    if (_getAllUser.ID == _getAllUser.UplineId)
                    {
                        chartData.Add(new Data("U-" + _getAllUser.ID, null, _getAllUser.Name, Role, _getAllUser.UserID, AgencyLeader));
                    }
                    else
                    {
                        chartData.Add(new Data("U-" + _getAllUser.ID, "U-" + _getAllUser.UplineId, _getAllUser.Name, Role, _getAllUser.UserID, AgencyLeader));
                    }
                }

                foreach (Customer _getAllCustomer in getAllCustomer)
                {
                    //string AgencyLeader = "Customer";

                    if (_getAllCustomer.UplineUserType == "User")
                    {
                        User getSingleUser = _usersModel.GetSingle(_getAllCustomer.UplineId);

                        chartData.Add(new Data("C-" + _getAllCustomer.ID, "U-" + getSingleUser.ID, _getAllCustomer.ContactPerson, "Customer", _getAllCustomer.CustomerID, "Agency Leader"));
                    }
                    else
                    {
                        //customer

                        chartData.Add(new Data("C-" + _getAllCustomer.ID, "C-" + _getAllCustomer.UplineId, _getAllCustomer.ContactPerson, "Customer", _getAllCustomer.CustomerID, ""));
                    }
                }
            }
            else
            {
                //not super admin part.

                string AgencyLeader = getSinglerUser.IsAgencyLeader;

                if (AgencyLeader == "Yes")
                {
                    AgencyLeader = "Agency Leader";
                }
                else
                {
                    AgencyLeader = "";
                }

                string Role = getSinglerUser.Role;

                if (Role == "Sales")
                {
                    Role += " " + getSinglerUser.Position;
                }

                chartData.Add(new Data("U-" + getSinglerUser.ID, "", getSinglerUser.Name, Role, getSinglerUser.UserID, AgencyLeader));


                IList<User> getAllUserDownlineWithType = _usersModel.GetAllSameUplineID(getSinglerUser.ID);
                
                while (getAllUserDownlineWithType.Count > 0)
                {
                    foreach (User _getAllUserDownlineWithType in getAllUserDownlineWithType)
                    {

                        string UplineType = "U-";

                        Role = _getAllUserDownlineWithType.Role;

                        if (Role == "Sales")
                        {
                            Role += " " + _getAllUserDownlineWithType.Position;
                        }

                        AgencyLeader = _getAllUserDownlineWithType.IsAgencyLeader;

                        if (AgencyLeader == "Yes")
                        {
                            AgencyLeader = "Agency Leader";
                        }
                        else
                        {
                            AgencyLeader = "";
                        }

                        /* 
                         *  id = _id;
                            parentId = _parentId;
                            Name = _name;
                            title = _title;
                            UserID = _UserID;
                            AgencyLeader = _AgencyLeader;
                         */

                        chartData.Add(new Data("U-" + _getAllUserDownlineWithType.ID, UplineType + _getAllUserDownlineWithType.UplineId, _getAllUserDownlineWithType.Name, Role, _getAllUserDownlineWithType.UserID, AgencyLeader));

                        getAllUserDownlineWithType = _usersModel.GetAllSameUplineID(_getAllUserDownlineWithType.ID);
                    }
                }

                IList<Customer> getAllCustomerDownlineWithType = _customersModel.GetCustomerDownlineWithUserType(getSinglerUser.ID, "User");

                if (getAllCustomerDownlineWithType.Count > 0)
                {
                    foreach (Customer _getAllCustomerDownlineWithType in getAllCustomerDownlineWithType)
                    {

                        string UplineType = "C-";

                        if(_getAllCustomerDownlineWithType.UplineUserType == "User")
                        {
                            UplineType = "U-";
                        }

                        /* 
                         *  id = _id;
                            parentId = _parentId;
                            Name = _name;
                            title = _title;
                            UserID = _UserID;
                            AgencyLeader = _AgencyLeader;
                         */

                        chartData.Add(new Data("C-" + _getAllCustomerDownlineWithType.ID, UplineType + _getAllCustomerDownlineWithType.UplineId, _getAllCustomerDownlineWithType.ContactPerson, "Customer", _getAllCustomerDownlineWithType.CustomerID, "Customer"));

                        IList<Customer> getAllCustomerDownlineWithTypeCheck = _customersModel.GetCustomerDownlineWithUserType(_getAllCustomerDownlineWithType.ID, "Customer");

                        while (getAllCustomerDownlineWithTypeCheck.Count > 0)
                        {
                            foreach (Customer _getAllCustomerDownlineWithTypeCheck in getAllCustomerDownlineWithTypeCheck)
                            {

                                string UplineTypeCheck = "C-";

                                if (_getAllCustomerDownlineWithTypeCheck.UplineUserType == "User")
                                {
                                    UplineTypeCheck = "U-";
                                }

                                /* 
                                 *  id = _id;
                                    parentId = _parentId;
                                    Name = _name;
                                    title = _title;
                                    UserID = _UserID;
                                    AgencyLeader = _AgencyLeader;
                                 */

                                chartData.Add(new Data("C-" + _getAllCustomerDownlineWithTypeCheck.ID, UplineTypeCheck + _getAllCustomerDownlineWithTypeCheck.UplineId, _getAllCustomerDownlineWithTypeCheck.ContactPerson, "Customer", _getAllCustomerDownlineWithTypeCheck.CustomerID, "Customer"));

                                getAllCustomerDownlineWithTypeCheck = _customersModel.GetCustomerDownlineWithUserType(_getAllCustomerDownlineWithTypeCheck.ID, "Customer");

                            }
                        }
                    }
                }
            }

            return JsonConvert.SerializeObject(chartData);

        }

        public class Data
        {
            public string id = null;
            public string parentId = null;
            public string Name = "";
            public string title = "";
            public string UserID = "";
            public string AgencyLeader = "";

            public Data(string _id, string _parentId, string _name, string _title, string _UserID, string _AgencyLeader)
            {
                id = _id;
                parentId = _parentId;
                Name = _name;
                title = _title;
                UserID = _UserID;
                AgencyLeader = _AgencyLeader;
            }
        }
    }
}