﻿using DataAccess.POCO;
using NODC_OMS.Models;
using NODC_OMS.Controllers;
using PagedList;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NODC_OMS.Controllers
{
    [HandleError]
    [RedirecttingActionAdmin]
    [RedirecttingActionAuditLog]
    public class AuditLogController : ControllerBase
    {
        private IAuditLogRepository _auditLogsModel;
        private IUserRepository _usersModel;

        public AuditLogController()
            : this(new AuditLogRepository(), new UserRepository())
        {

        }

        public AuditLogController(IAuditLogRepository auditLogsModel, IUserRepository usersModel)
        {
            _auditLogsModel = auditLogsModel;
            _usersModel = usersModel;
        }

        // GET: AuditLog
        public ActionResult Index()
        {
            if (Session["LogTable"] != null)
            {
                Session.Remove("LogTable");
            }

            if (Session["FromDate"] != null)
            {
                Session.Remove("FromDate");
            }

            if (Session["ToDate"] != null)
            {
                Session.Remove("ToDate");
            }

            if (Session["Page"] != null)
            {
                Session.Remove("Page");
            }

            if (Session["PageSize"] != null)
            {
                Session.Remove("PageSize");
            }

            return RedirectToAction("Options");
        }

        //GET: Options
        public ActionResult Options()
        {
            string logTable = "";

            if (Session["LogTable"] != null)
            {
                logTable = Session["LogTable"].ToString();
            }

            ViewData["FromDate"] = DateTime.Now.ToString("dd/MM/yyyy");

            if (Session["FromDate"] != null)
            {
                ViewData["FromDate"] = Session["FromDate"];
            }

            ViewData["ToDate"] = DateTime.Now.ToString("dd/MM/yyyy");

            if (Session["ToDate"] != null)
            {
                ViewData["ToDate"] = Session["ToDate"];
            }

            Dropdown[] logTableDDL = LogTableDropdown();
            ViewData["LogTableDropdown"] = new SelectList(logTableDDL, "val", "name", logTable);

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Options
        [HttpPost]
        public ActionResult Options(FormCollection form)
        {
            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;

            if (!string.IsNullOrEmpty(form["FromDate"]))
            {
                try
                {
                    fromDate = Convert.ToDateTime(form["FromDate"].ToString());
                }
                catch
                {
                    ModelState.AddModelError("FromDate", "From Date is not valid!");
                }
            }

            if (!string.IsNullOrEmpty(form["ToDate"]))
            {
                try
                {
                    toDate = Convert.ToDateTime(form["ToDate"].ToString());
                }
                catch
                {
                    ModelState.AddModelError("ToDate", "To Date is not valid!");
                }
            }

            if (fromDate > toDate)
            {
                ModelState.AddModelError("ToDate", "To Date is earlier than From Date!");
            }

            if (ModelState.IsValid)
            {
                Session.Add("LogTable", form["LogTable"].ToString());
                Session.Add("FromDate", form["FromDate"].ToString());
                Session.Add("ToDate", form["ToDate"].ToString());
                return RedirectToAction("Listing");
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            Dropdown[] logTableDDL = LogTableDropdown();
            ViewData["LogTableDropdown"] = new SelectList(logTableDDL, "val", "name", form["LogTable"].ToString());

            ViewData["FromDate"] = form["FromDate"].ToString();
            Session.Add("FromDate", form["FromDate"].ToString());

            ViewData["ToDate"] = form["ToDate"].ToString();
            Session.Add("ToDate", form["ToDate"].ToString());

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Listing
        public ActionResult Listing(int page = 1)
        {
            int pageSize = 20;
            if (Session["PageSize"] != null)
            {
                pageSize = Convert.ToInt32(Session["PageSize"]);
            }
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            string tableAffected = Session["LogTable"].ToString();
            string fromDate = Session["FromDate"].ToString();
            string toDate = Session["ToDate"].ToString();

            IPagedList<AuditLog> logs = _auditLogsModel.GetPaged(tableAffected, fromDate, toDate, page, pageSize);
            ViewData["AuditLogs"] = logs;

            Dropdown[] pageSizeDDL = PageSizeDDL();
            ViewData["PageSizeDropdown"] = new SelectList(pageSizeDDL, "val", "name", pageSize.ToString());

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Listing
        [HttpPost]
        public ActionResult Listing(FormCollection form)
        {
            int page = 1;
            int pageSize = Convert.ToInt32(form["PageSize"]);
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            string tableAffected = Session["LogTable"].ToString();
            string fromDate = Session["FromDate"].ToString();
            string toDate = Session["ToDate"].ToString();

            IPagedList<AuditLog> logs = _auditLogsModel.GetPaged(tableAffected, fromDate, toDate, page, pageSize);
            ViewData["AuditLogs"] = logs;

            Dropdown[] pageSizeDDL = PageSizeDDL();
            ViewData["PageSizeDropdown"] = new SelectList(pageSizeDDL, "val", "name", pageSize.ToString());

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: ExportExcel
        public void ExportExcel(string logTable, string fromDate, string toDate, int page = 0, int pageSize = 0)
        {
            IList<AuditLog> logs = null;

            if (page <= 0)
            {
                logs = _auditLogsModel.GetAll(logTable, fromDate, toDate);
            }
            else
            {
                logs = _auditLogsModel.GetPaged(logTable, fromDate, toDate, page, pageSize).ToList();
            }

            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create the worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Audit Logs");

                //set first row name
                ws.Cells[1, 1].Value = "Number";
                ws.Cells[1, 2].Value = "Log ID";
                ws.Cells[1, 3].Value = "IP Address";
                ws.Cells[1, 4].Value = "Timestamp";
                ws.Cells[1, 5].Value = "User Triggering";
                ws.Cells[1, 6].Value = "Table Affected";
                ws.Cells[1, 7].Value = "Description";

                int rowCount = 2;
                int NumCount = 1;
                foreach (AuditLog log in logs)
                {
                    ws.Cells[rowCount, 1].Value = NumCount;
                    ws.Cells[rowCount, 2].Value = log.ID;
                    ws.Cells[rowCount, 3].Value = log.IPAddress;
                    ws.Cells[rowCount, 4].Value = log.Timestamp.ToString("dd/MM/yyyy HH:mm:ss");
                    if (log.UserRole == "System")
                    {
                        ws.Cells[rowCount, 5].Value = "System";
                    }
                    else if(log.UserRole == "Customer")
                    {
                       ws.Cells[rowCount, 5].Value = log.Customer.CustomerID;
                    }
                    else
                    {
                       ws.Cells[rowCount, 5].Value = log.User.UserID;
                    }
                   
                    ws.Cells[rowCount, 6].Value = log.TableAffected;
                    ws.Cells[rowCount, 7].Value = log.Description;
                    rowCount++;
                    NumCount++;
                }

                ws.Cells[ws.Dimension.Address].AutoFitColumns();
                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=auditlogs-" + DateTime.Now.ToString("yyyyMMddHHmm") + ".xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }

        //LogTable Dropdown
        public Dropdown[] LogTableDropdown()
        {
            IList<AuditLog> uniqueTables = _auditLogsModel.GetUniqueTable().Where(e => e.TableAffected != "QuotationItemDescriptions").ToList();

            Dropdown[] ddl = new Dropdown[uniqueTables.Count + 1];
            ddl[0] = new Dropdown { name = "All Tables", val = "" };

            int count = 1;

            foreach (AuditLog log in uniqueTables)
            {
                ddl[count] = new Dropdown { name = log.TableAffected, val = log.TableAffected };
                count++;
            }

            return ddl;
        }

        //PageSize Dropdown
        public Dropdown[] PageSizeDDL()
        {
            Dropdown[] ddl = new Dropdown[5];
            ddl[0] = new Dropdown { name = "20", val = "20" };
            ddl[1] = new Dropdown { name = "40", val = "40" };
            ddl[2] = new Dropdown { name = "60", val = "60" };
            ddl[3] = new Dropdown { name = "80", val = "80" };
            ddl[4] = new Dropdown { name = "100", val = "100" };
            return ddl;
        }
    }

    public class RedirecttingActionAuditLogAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (HttpContext.Current.Session["LoginID"] == null)
            {
                HttpContext.Current.Session.Add("Result", "danger|Please login to continue!");

                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Access",
                    action = "Index"
                }));
            }
            else
            {
                List<string> auditLogMod = new List<string>() { "View Only", "Super Admin" };
                if (!auditLogMod.Contains(HttpContext.Current.Session["UserGroup"].ToString()))
                {
                    HttpContext.Current.Session.RemoveAll();
                    HttpContext.Current.Session.Add("Result", "danger|You have no access to this module!");

                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "Access",
                        action = "Index"
                    }));
                }
            }
        }
    }
}