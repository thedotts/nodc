﻿using DataAccess;
using DataAccess.POCO;
using Newtonsoft.Json;
using NODC_OMS.Controllers;
using NODC_OMS.Helper;
using NODC_OMS.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NODC_OMS.Controllers
{
    [HandleError]
    [RedirectingAction]
    public class ProjectFinanceController : ControllerBase
    {
        private IUserRepository _usersModel;
        private IProjectRepository _projectsModel;
        private IProjectProjectExpensesRepository _projectExpensesModel;
        private IProjectEstimatorRepository _projectEstimatorsModel;
        private ISystemSettingRepository _settingsModel;
        private IQuotationRepository _quotationsModel;
        private IQuotationItemDescriptionRepository _quotationItemDescriptionsModel;
        private ICustomerRepository _customersModel;
        private IInvoiceRepository _invoicesModel;

        // GET: Project
        public ProjectFinanceController()
            : this(new UserRepository(), new ProjectRepository(), new ProjectProjectExpensesRepository(), new ProjectEstimatorRepository(), new SystemSettingRepository(), new QuotationRepository(), new QuotationItemDescriptionRepository(), new CustomerRepository(), new InvoiceRepository())
        {

        }

        public ProjectFinanceController(IUserRepository usersModel, IProjectRepository projectsModel, IProjectProjectExpensesRepository projectExpensesModel, IProjectEstimatorRepository projectEstimatorsModel, ISystemSettingRepository settingsModel, IQuotationRepository quotationsModel, IQuotationItemDescriptionRepository quotationItemDescriptionsModel, ICustomerRepository customersModel, IInvoiceRepository invoicesModel)
        {
            _usersModel = usersModel;
            _projectsModel = projectsModel;
            _projectExpensesModel = projectExpensesModel;
            _projectEstimatorsModel = projectEstimatorsModel;
            _settingsModel = settingsModel;
            _quotationsModel = quotationsModel;
            _quotationItemDescriptionsModel = quotationItemDescriptionsModel;
            _customersModel = customersModel;
            _invoicesModel = invoicesModel;
        }

        // GET: Create
        public ActionResult Create()
        {

            Dropdown[] projectDDL = ProjectDDL();

            ViewData["ProjectDropdown"] = new SelectList(projectDDL, "val", "name", null);
            ViewData["Status"] = "";

            ViewData["Invoice"] = null;
            ViewData["ProjectExpensesTable"] = null;

            ViewData["TotalRevenue"] = "0.00";
            ViewData["TotalExpenses"] = "0.00";
            ViewData["TotalProfit"] = "0.00";
            ViewData["Highlight"] = "";

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            return View();
        }

        //POST: Create
        [HttpPost]
        public ActionResult Create(Project project,FormCollection form)
        {
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            Project oldData = _projectsModel.GetSingle(project.ID);

            ViewData["Status"] = "";

            ViewData["Invoice"] = null;
            ViewData["ProjectExpensesTable"] = null;

            ViewData["TotalRevenue"] = "0.00";
            ViewData["TotalExpenses"] = "0.00";
            ViewData["TotalProfit"] = "0.00";
            ViewData["Highlight"] = "";

            if(project.ID == 0)
            {
                ModelState.AddModelError("project.ID", "Projectt ID is required!");
            }
            else
            {
                ViewData["Status"] = oldData.Status;

                //This is Invoice Part
                IList<Invoice> invoice = _invoicesModel.getAllProjectPaymentSummary(oldData.ID);

                if (invoice.Count > 0)
                {
                    ViewData["Invoice"] = invoice;
                }

                //This is Profit And Loss Part
                if (project != null)
                {

                    Quotation quotation = _quotationsModel.GetSingle(oldData.QuotationReferenceId);

                    decimal TotalRevenue = 0;

                    if (quotation != null)
                    {
                        foreach (QuotationItemDescription _quotationItemDescription in quotation.ItemDescriptions)
                        {
                            TotalRevenue += (decimal)_quotationItemDescription.QuotationItemAmount;
                        }
                    }

                    ViewData["TotalRevenue"] = TotalRevenue.ToString("#,##0.00");

                    IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetPersonalAll(oldData.ID);

                    decimal TotalExpenses = 0;

                    foreach (ProjectProjectExpenses _projectExpenses in projectExpenses)
                    {
                        TotalExpenses += _projectExpenses.Cost;
                    }

                    ViewData["TotalExpenses"] = TotalExpenses.ToString("#,##0.00");

                    ViewData["TotalProfit"] = (TotalRevenue - TotalExpenses).ToString("#,##0.00");

                    decimal TotalProfit = TotalRevenue - TotalExpenses;
                    decimal Percentage = 0;

                    if (TotalRevenue != 0)
                    {
                        Percentage = (TotalProfit / TotalRevenue) * 100;
                    }

                    if (Percentage < 17)
                    {
                        ViewData["Highlight"] = "class=bg-red-200";
                    }
                }
            }

            Dropdown[] projectDDL = ProjectDDL();
            ViewData["ProjectDropdown"] = new SelectList(projectDDL, "val", "name", project.ID);

            //Project Expenses Validation Part
            List<string> projectExpensesKeys = form.AllKeys.Where(e => e.Contains("ProjectExpensesDescription_")).ToList();

            List<ProjectProjectExpensesTable> projectExpensesTable = new List<ProjectProjectExpensesTable>();

            decimal TotalExpensesCheck = 0;

            foreach (string _projectExpensesKeys in projectExpensesKeys)
            {
                string rowId = _projectExpensesKeys.Split('_')[1];

                string Description = form["ProjectExpensesDescription_" + rowId];
                string SupplierName = form["ProjectExpensesSupplierName_" + rowId];
                string InvoiceNo = form["ProjectExpensesInvoiceNo_" + rowId];
                string Date = form["ProjectExpensesDate_" + rowId];
                string GST = form["ProjectExpensesGST_" + rowId];
                string CostBeforeGST = form["ProjectExpensesCostBeforeGST_" + rowId];
                string ProjectExpensesStatus = form["ProjectExpensesStatus_" + rowId];

                if (string.IsNullOrEmpty(Description))
                {
                    ModelState.AddModelError("ProjectExpensesDescription_" + rowId, "Description is required!");
                }

                if (string.IsNullOrEmpty(SupplierName))
                {
                    ModelState.AddModelError("ProjectExpensesSupplierName_" + rowId, "Supplier Name is required!");
                }

                if (string.IsNullOrEmpty(InvoiceNo))
                {
                    ModelState.AddModelError("ProjectExpensesInvoiceNo_" + rowId, "Invoice No is required!");
                }

                if (string.IsNullOrEmpty(Date))
                {
                    ModelState.AddModelError("ProjectExpensesDate_" + rowId, "Date is required!");
                }

                if (string.IsNullOrEmpty(GST))
                {
                    ModelState.AddModelError("ProjectExpensesGST_" + rowId, "GST is required!");
                }

                if (string.IsNullOrEmpty(CostBeforeGST))
                {
                    ModelState.AddModelError("ProjectExpensesCostBeforeGST_" + rowId, "Cost Before GST is required!");
                }
                else
                {
                    //Check format
                    //Check is decimal or not
                    bool checkFormat = FormValidationHelper.AmountFormat(CostBeforeGST);

                    if (!checkFormat)
                    {
                        ModelState.AddModelError("ProjectExpensesCostBeforeGST_" + rowId, "Invalid Cost Before GST Format!");
                    }
                    else
                    {
                        TotalExpensesCheck += Convert.ToDecimal(CostBeforeGST);
                    }
                }

                projectExpensesTable.Add(new ProjectProjectExpensesTable()
                {
                    RowId = Convert.ToInt32(rowId),
                    Description = Description,
                    SupplierName = SupplierName,
                    InvoiceNo = InvoiceNo,
                    Date = Date,
                    GST = new SelectList(GSTDDL(), "val", "name", GST),
                    CostBeforeGST = CostBeforeGST,
                    Status = new SelectList(ProjectProjectExpensesStatusDDL(), "val", "name", ProjectExpensesStatus),
                });

                ViewData["TotalExpenses"] = TotalExpensesCheck.ToString("#,##0.00");

                decimal TotalRevenue = Convert.ToDecimal(ViewData["TotalRevenue"]);
                decimal TotalProfit = TotalRevenue - TotalExpensesCheck;
                decimal Percentage = 0;

                if (TotalRevenue != 0)
                {
                    Percentage = (TotalProfit / TotalRevenue) * 100;
                }

                if (Percentage < 17)
                {
                    ViewData["Highlight"] = "class=bg-red-200";
                }

                ViewData["TotalProfit"] = TotalProfit.ToString("#,##0.00");
                ViewData["ProjectExpensesTable"] = projectExpensesTable;
            }

            if (ModelState.IsValid)
            {
                ProjectProjectExpenses projectProjectExpensesRecord = new ProjectProjectExpenses();

                //Remove ProjectExpensesTable before add new
                IList<ProjectProjectExpenses> RemoveProjectExpenses = _projectExpensesModel.GetPersonalAll(project.ID);

                foreach (ProjectProjectExpenses _projectExpenses in RemoveProjectExpenses)
                {
                    bool remove_project_expenses = _projectExpensesModel.Delete(_projectExpenses.ID);
                }

                foreach (ProjectProjectExpensesTable _projectExpensesTable in projectExpensesTable)
                {
                    projectProjectExpensesRecord.ProjectId = project.ID;
                    projectProjectExpensesRecord.Description = _projectExpensesTable.Description;
                    projectProjectExpensesRecord.SupplierName = _projectExpensesTable.SupplierName;
                    projectProjectExpensesRecord.InvoiceNo = _projectExpensesTable.InvoiceNo;
                    projectProjectExpensesRecord.Date = Convert.ToDateTime(_projectExpensesTable.Date);
                    projectProjectExpensesRecord.GST = Convert.ToString(_projectExpensesTable.GST.SelectedValue);
                    projectProjectExpensesRecord.Cost = Convert.ToDecimal(_projectExpensesTable.CostBeforeGST);
                    projectProjectExpensesRecord.Status = Convert.ToString(_projectExpensesTable.Status.SelectedValue);

                    bool add_project_projectExpenses = _projectExpensesModel.Add(projectProjectExpensesRecord);
                }

                string userRole = Session["UserGroup"].ToString();
                if (userRole == "Sales")
                {
                    userRole += " " + Session["Position"].ToString();
                }
                string tableAffected = "ProjectFinances";
                string description = userRole + " [" + Session["LoginID"].ToString() + "] Edited Project Finance [" + oldData.ProjectID + "]";

                bool create_log = AuditLogHelper.WriteAuditLog(loginMainID, userRole, tableAffected, description);

                //if (create_log)
                //{
                //    Quotation quotation = _quotationsModel.GetSingle(oldData.QuotationReferenceId);

                //    decimal TotalRevenue = 0;

                //    if (quotation != null)
                //    {
                //        foreach (QuotationItemDescription _quotationItemDescription in quotation.ItemDescriptions)
                //        {
                //            TotalRevenue += (decimal)_quotationItemDescription.QuotationItemAmount;
                //        }
                //    }

                //    ViewData["TotalRevenue"] = TotalRevenue.ToString("#,##0.00");

                //    IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetPersonalAll(oldData.ID);

                //    decimal TotalExpenses = 0;

                //    foreach (ProjectProjectExpenses _projectExpenses in projectExpenses)
                //    {
                //        TotalExpenses += _projectExpenses.Cost;
                //    }

                //    ViewData["TotalExpenses"] = TotalExpenses.ToString("#,##0.00");

                //    ViewData["TotalProfit"] = (TotalRevenue - TotalExpenses).ToString("#,##0.00");

                //    decimal TotalProfit = TotalRevenue - TotalExpenses;
                //    decimal Percentage = 0;

                //    if (TotalRevenue != 0)
                //    {
                //        Percentage = (TotalProfit / TotalRevenue) * 100;
                //    }

                //    if (Percentage < 17)
                //    {
                //        ViewData["Highlight"] = "class=bg-red-200";
                //    } 
                //}

                Session.Add("Result", "success|" + oldData.ProjectID + " has been successfully updated!");
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Project Info
        public string GetProjectInfo(int id)
        {

            Project project = _projectsModel.GetSingle(id);

            ProjectFinanceTable projectFinance = new ProjectFinanceTable();

            if(project != null)
            {
                projectFinance.Result = true;

                Quotation quotation = _quotationsModel.GetSingle(project.QuotationReferenceId);

                decimal TotalRevenue = 0;

                if(quotation != null)
                {
                    foreach (QuotationItemDescription _quotationItemDescription in quotation.ItemDescriptions)
                    {
                        TotalRevenue += (decimal)_quotationItemDescription.QuotationItemAmount;
                    }
                }

                projectFinance.projectStatus = project.Status;
                projectFinance.TotalRevenue = TotalRevenue.ToString("#,##0.00");

                IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetPersonalAll(project.ID);

                decimal TotalExpenses = 0;

                foreach(ProjectProjectExpenses _projectExpenses in projectExpenses)
                {
                    TotalExpenses += _projectExpenses.Cost;
                }

                projectFinance.TotalExpenses = TotalExpenses.ToString("#,##0.00");

                projectFinance.TotalProfit = (TotalRevenue - TotalExpenses).ToString("#,##0.00");

                decimal TotalProfit = TotalRevenue - TotalExpenses;
                decimal Percentage = 0;

                if(TotalRevenue != 0)
                {
                    Percentage = (TotalProfit / TotalRevenue) * 100;
                }

                if(Percentage < 17)
                {
                    projectFinance.highlight = "bg-red-200";
                }

                projectFinance.ProjectExpensesView = JsonConvert.SerializeObject(GetProjectExpenses(id));

            }
            else
            {
                projectFinance.Result = false;
                projectFinance.ErrorMessage = "Project record not found!";
            }

            return JsonConvert.SerializeObject(projectFinance);
        }

        public string GetProjectExpenses(int id)
        {
            //Get Project Expenses Item
            IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetPersonalAll(id);

            List<ProjectProjectExpensesTable> projectExpensesTable = new List<ProjectProjectExpensesTable>();

            int count = 1;
            foreach (ProjectProjectExpenses _projectExpenses in projectExpenses)
            {
                projectExpensesTable.Add(new ProjectProjectExpensesTable()
                {
                    RowId = count,
                    Description = _projectExpenses.Description,
                    SupplierName = _projectExpenses.SupplierName,
                    InvoiceNo = _projectExpenses.InvoiceNo,
                    Date = string.Format("{0:dd/MM/yyyy}", _projectExpenses.Date),
                    GST = new SelectList(GSTDDL(), "val", "name", _projectExpenses.GST),
                    CostBeforeGST = Convert.ToString(_projectExpenses.Cost),
                    Status = new SelectList(ProjectProjectExpensesStatusDDL(), "val", "name", _projectExpenses.Status),
                });
                count++;
            }

            ViewData["ProjectExpensesTable"] = projectExpensesTable;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, "GetProjectExpenses");
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        //GET: GetProjectExpenses
        public ActionResult GetProjectExpensesNew(int id)
        {
            ViewData["RowId"] = id;

            Dropdown[] gstDDL = GSTDDL();
            ViewData["GSTDropdown"] = new SelectList(gstDDL, "val", "name", null);

            Dropdown[] projectExpensesStatusDDL = ProjectProjectExpensesStatusDDL();
            ViewData["ProjectExpensesStatusDropdown"] = new SelectList(projectExpensesStatusDDL, "val", "name", null);

            return View();
        }

        //GET: Quotation Total Amount
        public string GetQuotationTotalAmount(int id)
        {
            string result = "";

            Project project = _projectsModel.GetSingle(id);

            List<QuotationItemDescription> quotation = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(project.QuotationReferenceId);
            decimal totalAmount = 0;

            foreach (QuotationItemDescription _quotation in quotation)
            {
                totalAmount += (decimal)_quotation.QuotationItemAmount;
            }

            result = Convert.ToString(totalAmount);

            return result;
        }

        //GET: GetPaymentSummary
        public ActionResult GetPaymentSummaryProject(int id)
        {
            Project project = _projectsModel.GetSingle(id);

            IList<Invoice> invoice = _invoicesModel.getAllProjectPaymentSummary(project.QuotationReferenceId);

            ViewData["Invoice"] = null;

            if (invoice.Count > 0)
            {
                ViewData["Invoice"] = invoice;
            }

            return View();
        }

        //GET Project_ProjectExpenses Status Dropdown
        public Dropdown[] ProjectProjectExpensesStatusDDL()
        {
            //Get Main ID first
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            User getUserRole = _usersModel.GetSingle(loginMainID);

            Dropdown[] ddl = null;

            if (getUserRole.Role == "Super Admin" || getUserRole.Role == "Accounts")
            {
                ddl = new Dropdown[2];

                ddl[0] = new Dropdown { name = "Unpaid", val = "Unpaid" };
                ddl[1] = new Dropdown { name = "Paid", val = "Paid" };
            }
            else
            {
                ddl = new Dropdown[1];

                ddl[0] = new Dropdown { name = "Unpaid", val = "Unpaid" };
            }

            return ddl;
        }

        //GST Dropdown
        public Dropdown[] GSTDDL()
        {

            Dropdown[] ddl = new Dropdown[2];

            ddl[0] = new Dropdown { name = "Yes", val = "Yes" };
            ddl[1] = new Dropdown { name = "No", val = "No" };

            return ddl;
        }

        //Quotation Dropdown
        public Dropdown[] ProjectDDL()
        {

            IList<Project> project = _projectsModel.GetAll();

            Dropdown[] ddl = new Dropdown[project.Count];

            int count = 0;

            foreach (Project _project in project)
            {
                ddl[count] = new Dropdown { name = _project.ProjectID + " - " + _project.CompanyName, val = Convert.ToString(_project.ID) };
                count++;
            }

            return ddl;
        }

        //GET: ValidateAmount
        public string ValidateAmount(string amount)
        {
            string result = "";

            bool checkFormat = FormValidationHelper.AmountFormat(amount);

            if (checkFormat)
            {
                bool checkRange = FormValidationHelper.AmountFormat(amount, 20);

                if (checkRange)
                {
                    string newAmount = FormValidationHelper.AmountFormatter(Convert.ToDecimal(amount), 2);

                    result = "{\"result\":true,\"newAmount\":\"" + newAmount + "\"}";
                }
                else
                {
                    result = "{\"result\":false}";
                }
            }
            else
            {
                result = "{\"result\":false}";
            }

            return result;
        }

    }
}