﻿using DataAccess;
using DataAccess.POCO;
using NODC_OMS.Controllers;
using NODC_OMS.Helper;
using NODC_OMS.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NODC_OMS.Controllers
{
    [HandleError]
    [RedirectingAction]
    public class CustomerController : ControllerBase
    {
        private ICustomerRepository _customersModel;
        private IUserRepository _usersModel;
        private ISystemSettingRepository _settingsModel;
        private ICustomerReminderRepository _customerRemindersModel;
        private IQuotationRepository _quotationsModel;
        private IProjectRepository _projectsModel;

        // GET: Customer
        public CustomerController()
            : this(new CustomerRepository(), new UserRepository(), new SystemSettingRepository(), new CustomerReminderRepository(), new QuotationRepository(), new ProjectRepository())
        {

        }

        public CustomerController(ICustomerRepository customersModel, IUserRepository usersModel, ISystemSettingRepository settingsModel, ICustomerReminderRepository customerRemindersModel, IQuotationRepository quotationsModel, IProjectRepository projectsModel)
        {
            _customersModel = customersModel;
            _usersModel = usersModel;
            _settingsModel = settingsModel;
            _customerRemindersModel = customerRemindersModel;
            _quotationsModel = quotationsModel;
            _projectsModel = projectsModel;
        }

        // GET: Customer
        public ActionResult Index()
        {

            if (Session["SearchKeyword"] != null)
            {
                Session.Remove("SearchKeyword");
            }

            if (Session["SearchStatus"] != null)
            {
                Session.Remove("SearchStatus");
            }

            return RedirectToAction("Listing");
        }

        //GET: Listing
        public ActionResult Listing(int page = 1)
        {

            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = "";
            ViewData["SearchStatus"] = "";
            ViewData["GetStatusValue"] = "All Customers";

            if (Session["SearchKeyword"] != null)
            {
                ViewData["SearchKeyword"] = Session["SearchKeyword"];
            }

            if (Session["SearchStatus"] != null)
            {

                if (Session["SearchStatus"].ToString() == "Active")
                {
                    ViewData["SearchStatus"] = "Active";
                    ViewData["GetStatusValue"] = "Active Only";
                }
                else if (Session["SearchStatus"].ToString() == "Suspend")
                {
                    ViewData["SearchStatus"] = "Suspend";
                    ViewData["GetStatusValue"] = "Suspended Only";
                }
                else if (Session["SearchStatus"].ToString() == "Lead")
                {
                    ViewData["SearchStatus"] = "Lead";
                    ViewData["GetStatusValue"] = "Leads Only";
                }
                else
                {
                    ViewData["SearchStatus"] = Session["SearchStatus"];
                }
            }

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", null, null, null);

            int MainID = Convert.ToInt32(Session["MainID"]);

            //check is super admin or not
            //if yes, can see all
            User checkSuperAdmin = _usersModel.GetSingle(MainID);

            List<int> OwnAndUplineID = new List<int>();

            if(checkSuperAdmin.Role == "Super Admin")
            {
                //OwnAndUplineID.Add(0);
            }
            else
            {

                //IList<User> getDownlineID = _usersModel.GetAllSameUplineID(MainID);

                OwnAndUplineID.Add(MainID);

                //foreach (User _getDownlineID in getDownlineID)
                //{
                //    OwnAndUplineID.Add(_getDownlineID.ID);
                //}

            }


            IPagedList<Customer> customers = _customersModel.GetPaged(OwnAndUplineID, ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), page, pageSize);
            ViewData["Customer"] = customers;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Listing
        [HttpPost]
        public ActionResult Listing(FormCollection form)
        {
            int page = 1;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = form["SearchKeyword"].Trim();
            Session.Add("SearchKeyword", form["SearchKeyword"].Trim());

            ViewData["GetStatusValue"] = "All Customers";


            if (form["SearchStatusValue"].Trim() == "Active")
            {
                ViewData["GetStatusValue"] = "Active Only";
            }

            if (form["SearchStatusValue"].Trim() == "Suspend")
            {
                ViewData["GetStatusValue"] = "Suspended Only";
            }

            if (form["SearchStatusValue"].Trim() == "Lead")
            {
                ViewData["GetStatusValue"] = "Leads Only";
            }

            string item = "";

            if (form["SearchStatusValue"].Trim() != "All")
            {
                item = form["SearchStatusValue"].Trim();
            }

            ViewData["SearchStatus"] = item;
            Session.Add("SearchStatus", item);

            int MainID = Convert.ToInt32(Session["MainID"]);

            //check is super admin or not
            //if yes, can see all
            User checkSuperAdmin = _usersModel.GetSingle(MainID);

            List<int> OwnAndUplineID = new List<int>();

            if (checkSuperAdmin.Role == "Super Admin")
            {
                //OwnAndUplineID.Add(0);
            }
            else
            {

                //IList<User> getDownlineID = _usersModel.GetAllSameUplineID(MainID);

                OwnAndUplineID.Add(MainID);

                //foreach (User _getDownlineID in getDownlineID)
                //{
                //    OwnAndUplineID.Add(_getDownlineID.ID);
                //}

            }

            IPagedList<Customer> customers = _customersModel.GetPaged(OwnAndUplineID, ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), page, pageSize);
            ViewData["Customer"] = customers;

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", ViewData["SearchStatus"].ToString());

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Create
        public ActionResult Create()
        {
            int MainID = Convert.ToInt32(Session["MainID"]);

            List<CustomerReminderTable> customerReminderTable = new List<CustomerReminderTable>();

            //customerReminderTable.Add(new CustomerReminderTable() { 
            //    RowId = 1,
            //    Description = null,
            //    Date = null,
            //    RepeatDDL = new SelectList(RepeatDDL(), "val", "name", "Yearly")
            //});

            ViewData["CustomerReminderTable"] = customerReminderTable;

            Dropdown[] agencyDDL = AgencyDDL();
            ViewData["AgencyDropdown"] = new SelectList(agencyDDL, "val", "name", null, null, null);

            //Default must display the login User ID at upline dropdown list.
            User checkUser = _usersModel.GetSingle(MainID);

            ViewData["CheckUserRole"] = checkUser.Role;

            string uplineID = "U-" + checkUser.ID;

            Dropdown[] uplineDDL = UplineDDL();
            ViewData["UplineDropdown"] = new SelectList(uplineDDL, "val", "name", null, uplineID, null);

            Dropdown[] positionDDL = PositionDDL();
            ViewData["PositionDropdown"] = new SelectList(positionDDL, "val", "name", null, null, null);

            Dropdown[] assignToDDL = AssignToDDL();
            ViewData["AssignToDropdown"] = new SelectList(assignToDDL, "val", "name", null, MainID, null);

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", null, null, null);

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Create
        [HttpPost]
        public ActionResult Create(Customer customer, FormCollection form)
        {


            //Validation Part
            if (string.IsNullOrEmpty(customer.LoginID))
            {
                ModelState.AddModelError("customer.LoginID", "Login ID is required!");
            }
            else
            {

                if (customer.LoginID.Contains(" "))
                {
                    ModelState.AddModelError("customer.LoginID", "Login ID cannot have space in between or anywhere!");
                }
                else
                {
                    Customer checkLoginID = _customersModel.FindLoginID(customer.LoginID);

                    //check unique login ID
                    if (checkLoginID != null)
                    {
                        ModelState.AddModelError("customer.LoginID", "Login ID already existed!!");
                    }
                }
            }

            if (string.IsNullOrEmpty(customer.Password))
            {
                ModelState.AddModelError("customer.Password", "Password is required!");
            }

            if (string.IsNullOrEmpty(form["RepeatPassword"]))
            {
                ModelState.AddModelError("RepeatPassword", "Repeat Password is required!");
            }

            if (customer.Password != form["RepeatPassword"].ToString())
            {
                ModelState.AddModelError("customer.Password", "Password does not match Repeat Password!");
                ModelState.AddModelError("RepeatPassword", "Repeat Password does not match Password!");
            }

            if (customer.Agency == null)
            {
                ModelState.AddModelError("customer.Agency", "Agency is required!");
            }

            if (string.IsNullOrEmpty(customer.ContactPerson))
            {
                ModelState.AddModelError("customer.ContactPerson", "Contact Person is required!");
            }

            if (string.IsNullOrEmpty(customer.Tel))
            {
                ModelState.AddModelError("customer.Tel", "Tel is required!");
            }

            string uplineID = null;
            
            if (string.IsNullOrEmpty(form["UplineID"]))
            {
                ModelState.AddModelError("UplineID", "Upline is required!");
            }
            else
            {
                string[] SalesPersonCheck = Convert.ToString(form["UplineID"]).Split('-');

                if (SalesPersonCheck[0] == "C")
                {
                    customer.UplineUserType = "Customer";
                }
                else
                {
                    customer.UplineUserType = "User";

                }

                customer.UplineId = Convert.ToInt32(SalesPersonCheck[1]);

                uplineID = SalesPersonCheck[0] + "-" + SalesPersonCheck[1];
            }

            int AssignToId = 0;

            if (customer.AssignToId <= 0)
            {
                ModelState.AddModelError("customer.AssignToId", "Assign To is required!");
            }
            else
            {
                AssignToId = customer.AssignToId;
            }

            //Customer Reminder Validation Part
            List<string> customerReminderKeys = form.AllKeys.Where(e => e.Contains("CustomerReminderDescription_")).ToList();

            List<CustomerReminderTable> customerReminderTable = new List<CustomerReminderTable>();
            
            foreach (string _customerReminderKeys in customerReminderKeys){

                string rowId = _customerReminderKeys.Split('_')[1];

                string Description = form["CustomerReminderDescription_" + rowId];
                string Date = form["CustomerReminderDate_" + rowId];
                string Repeat = form["CustomerReminderRepeat_" + rowId];

                if (string.IsNullOrEmpty(Description))
                {
                    ModelState.AddModelError("CustomerReminderDescription_" + rowId, "Description is required!");
                }

                if (string.IsNullOrEmpty(Date))
                {
                    ModelState.AddModelError("CustomerReminderDate_" + rowId, "Date is required!");
                }

                if (string.IsNullOrEmpty(Repeat))
                {
                    ModelState.AddModelError("CustomerReminderRepeat_" + rowId, "Date is required!");
                }

                customerReminderTable.Add(new CustomerReminderTable()
                {
                    RowId = Convert.ToInt32(rowId),
                    Description = Description,
                    Date = Date,
                    RepeatDDL = new SelectList(RepeatDDL(), "val", "name", Repeat)
                });
            }

            ViewData["CustomerReminderTable"] = customerReminderTable;

            Dropdown[] agencyDDL = AgencyDDL();
            ViewData["AgencyDropdown"] = new SelectList(agencyDDL, "val", "name", null, null, null);

            Dropdown[] uplineDDL = UplineDDL();
            ViewData["UplineDropdown"] = new SelectList(uplineDDL, "val", "name", null, uplineID, null);

            Dropdown[] positionDDL = PositionDDL();
            ViewData["PositionDropdown"] = new SelectList(positionDDL, "val", "name", null, null, null);

            Dropdown[] assignToDDL = AssignToDDL();
            ViewData["AssignToDropdown"] = new SelectList(assignToDDL, "val", "name", null, AssignToId, null);

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", null, null, null);

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            int MainID = Convert.ToInt32(Session["MainID"]);

            User checkUser = _usersModel.GetSingle(MainID);

            ViewData["CheckUserRole"] = checkUser.Role;

            //If valid
            if (ModelState.IsValid)
            {
                CustomerReminder customerReminderRecord = new CustomerReminder();

                //Assign User ID
                string prefix = _settingsModel.GetCodeValue("PREFIX_CUSTOMER_ID");

                Customer lastRecord = _customersModel.GetLast();

                //Convert year to last two digit
                var date = DateTime.Now;
                int last_two_digit = date.Year % 100;

                if (lastRecord != null)
                {
                    customer.CustomerID = prefix + last_two_digit + (lastRecord.ID + 1).ToString().PadLeft(5, '0');
                }
                else
                {
                    customer.CustomerID = prefix + last_two_digit + "00001";
                }

                customer.Password = EncryptionHelper.Encrypt(customer.Password);


                //Check user role and reassign the customer position if role is Sales

                if (checkUser.Role == "Sales")
                {
                    customer.Position = "Customer";
                }

                bool result = _customersModel.Add(customer);

                if (result)
                {
                    string userRole = Session["UserGroup"].ToString();
                    if (userRole == "Sales")
                    {
                        userRole += " " + Session["Position"].ToString();
                    }

                    string tableAffected = "Customers";
                    string description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Customers [" + customer.CustomerID + "]";

                    bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                    bool hasCustomerReminder = false;

                    foreach (CustomerReminderTable _customerReminderTable in customerReminderTable) {

                        customerReminderRecord.CustomerId = customer.ID;
                        customerReminderRecord.Description = _customerReminderTable.Description;
                        customerReminderRecord.Date = Convert.ToDateTime(_customerReminderTable.Date);
                        customerReminderRecord.Repeat = Convert.ToString(_customerReminderTable.RepeatDDL.SelectedValue);

                        bool add_customer_reminder = _customerRemindersModel.Add(customerReminderRecord);

                        if (add_customer_reminder)
                        {
                            if (!hasCustomerReminder)
                            {
                                hasCustomerReminder = true;
                            }
                        }
                    }


                    if (hasCustomerReminder)
                    {
                        userRole = Session["UserGroup"].ToString();
                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }
                        tableAffected = "CustomerCustomerReminders";
                        description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Customer Customer Reminders [" + customer.CustomerID + "]";

                        bool customerCustomerReminder_create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                    }

                    //Email to "Assign To" User when customer status is Lead.
                    if(customer.Status == "Lead")
                    {
                        //Send email to assign to user.
                        User getUserInfo = _usersModel.GetSingle(customer.AssignToId);

                        if(getUserInfo != null)
                        {
                            string subject = "Task List [" + customer.LoginID + "] Customer Lead Status";
                            string body = System.IO.File.ReadAllText(Server.MapPath("~/Templates/TaskListNotification.html"));

                            string recipient = getUserInfo.Email;
                            string name = getUserInfo.Name;
                            string customerID = customer.CustomerID;
                            string status = customer.Status;
                            ListDictionary replacements = new ListDictionary();

                            replacements.Add("<%Name%>", name);
                            replacements.Add("<%CustomerID%>", customerID);
                            replacements.Add("<%Status%>", status);
                            replacements.Add("<%Url%>", Url.Action("Edit", "Customer", new { @id = customer.ID }, "http"));

                            bool sent = EmailHelper.SendEmail(subject, body, replacements, recipient);
                        }
                        else
                        {
                            Session.Add("Result", "danger|Assign to user record not found!");
                            return View();
                        }
                    }

                    Session.Add("Result", "success|" + customer.CustomerID + " has been successfully created!");
                    return RedirectToAction("Index");
                }
                else
                {
                    Session.Add("Result", "danger|An error occured while save customer records!");
                }
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            return View();
        }

        //GET: Edit
        public ActionResult Edit(int id)
        {

            int MainID = Convert.ToInt32(Session["MainID"]);

            ViewData["AbleToUpdate"] = "No";

            User user = _usersModel.GetSingle(MainID);

            if (user.Role == "Super Admin")
            {
                ViewData["AbleToUpdate"] = "Yes";
            }

            ViewData["CheckUserRole"] = user.Role;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            Customer customer = _customersModel.GetSingle(id);

            if (customer != null)
            {

                if(customer.AssignToId == MainID && customer.UplineUserType == "User")
                {
                    ViewData["AbleToUpdate"] = "Yes";
                }

                //Check is super admin or the main sales person or not
                User checkSuperAdmin = _usersModel.GetSingle(MainID);

                if (checkSuperAdmin.Role != "Super Admin")
                {
                    if (customer.UplineId != checkSuperAdmin.ID && customer.AssignToId != checkSuperAdmin.ID)
                    {
                        Session.Add("Result", "danger|You are not allow to edit this customer!");
                        return RedirectToAction("Index");
                    }
                }

                IList<CustomerReminder> getCustomerReminder = _customerRemindersModel.GetPersonalAll(id);

                List<CustomerReminderTable> customerReminderTable = new List<CustomerReminderTable>();


                int count = 1;
                foreach (CustomerReminder _getCustomerReminder in getCustomerReminder)
                {
                    CustomerReminderTable saveData = new CustomerReminderTable();

                    saveData.RowId = count;
                    saveData.Description = _getCustomerReminder.Description;
                    saveData.Date = _getCustomerReminder.Date.ToString("dd/MM/yyyy");

                    Dropdown[] repeatDDL = RepeatDDL();
                    saveData.RepeatDDL = new SelectList(repeatDDL, "val", "name", _getCustomerReminder.Repeat);
                    count++;

                    customerReminderTable.Add(saveData);
                }
                ViewData["Customer"] = customer;
                ViewData["CustomerReminderTable"] = customerReminderTable;

                Dropdown[] agencyDDL = AgencyDDL();
                ViewData["AgencyDropdown"] = new SelectList(agencyDDL, "val", "name", null, null, null);

                string UplineID = null;

                if(customer.UplineUserType == "Customer")
                {
                    UplineID = "C-" + customer.UplineId;
                }
                else
                {
                    UplineID = "U-" + customer.UplineId;
                }

                //if only match assign to ID,
                //disable upline dropdown.

                string fromView = null;
                string userType = customer.UplineUserType;
                int userUplineId = customer.UplineId;

                ViewData["MatchAssignToOnly"] = null;

                if (customer.UplineId != checkSuperAdmin.ID )
                {
                    //this only match assign to id
                    if (customer.AssignToId == checkSuperAdmin.ID)
                    {
                        ViewData["MatchAssignToOnly"] = "Yes";
                        fromView = "Yes";
                    }
                    
                }

                Dropdown[] uplineDDL = UplineDDL(fromView, userType, userUplineId);
                ViewData["UplineDropdown"] = new SelectList(uplineDDL, "val", "name", null, UplineID, null);

                Dropdown[] positionDDL = PositionDDL();
                ViewData["PositionDropdown"] = new SelectList(positionDDL, "val", "name", null, null, null);

                Dropdown[] assignToDDL = AssignToDDL();
                ViewData["AssignToDropdown"] = new SelectList(assignToDDL, "val", "name", null, null, null);

                Dropdown[] statusDDL = StatusDDL();
                ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", null, null, null);
            }
            else
            {
                Session.Add("Result", "danger|Customer record not found!");
            }

            return View();
        }

        //POST: Edit
        [HttpPost]
        public ActionResult Edit(int id,Customer customer, FormCollection form)
        {
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            int MainID = Convert.ToInt32(Session["MainID"]);

            User checkUser = _usersModel.GetSingle(MainID);

            ViewData["CheckUserRole"] = checkUser.Role;

            Customer oldData = _customersModel.GetSingle(id);
            string password = oldData.Password;

            int MainLoginID = Convert.ToInt32(Session["MainID"]);

            User checkSuperAdmin = _usersModel.GetSingle(MainLoginID);

            ViewData["AbleToUpdate"] = "No";

            if (checkSuperAdmin.Role == "Super Admin")
            {
                ViewData["AbleToUpdate"] = "Yes";
            }

            string fromView = null;
            string userType = null;
            int userUplineId = 0;

            ViewData["MatchAssignToOnly"] = null;

            string formUplineID = null;

            //if upline got data
            if (form["UplineID"] != null)
            {
                string[] SalesPersonCheck = Convert.ToString(form["UplineID"]).Split('-');

                customer.UplineId = Convert.ToInt32(SalesPersonCheck[1]);

                if (SalesPersonCheck[0] == "C")
                {
                    customer.UplineUserType = "Customer";
                    formUplineID = "C-" + customer.UplineId;
                }
                else
                {
                    customer.UplineUserType = "User";
                    formUplineID = "U-" + customer.UplineId;

                }
            }


            //Validation Part
            if (string.IsNullOrEmpty(customer.LoginID))
            {
                ModelState.AddModelError("customer.LoginID", "Login ID is required!");
            }
            else
            {

                if (customer.LoginID.Contains(" "))
                {
                    ModelState.AddModelError("customer.LoginID", "Login ID cannot have space in between or anywhere!");
                }
                else
                {
                    Customer checkLoginID = _customersModel.FindPersonalLoginID(id, customer.LoginID);

                    //check unique login ID
                    if (checkLoginID != null)
                    {
                        ModelState.AddModelError("customer.LoginID", "Login ID already existed!!");
                    }
                }
            }

            if (customer.Agency == null)
            {
                ModelState.AddModelError("customer.Agency", "Agency is required!");
            }

            if (string.IsNullOrEmpty(customer.ContactPerson))
            {
                ModelState.AddModelError("customer.ContactPerson", "Contact Person is required!");
            }

            if (string.IsNullOrEmpty(customer.Tel))
            {
                ModelState.AddModelError("customer.Tel", "Tel is required!");
            }

            string UplineID = null;

            if (string.IsNullOrEmpty(form["UplineID"]))
            {
                if (form["IsOnlyAssignToUse"] != "Yes")
                {
                    ModelState.AddModelError("UplineID", "Upline is required!");
                }
                else
                {
                    //if is assign to only 
                    ViewData["MatchAssignToOnly"] = "Yes";
                    fromView = "Yes";
                    customer.UplineUserType = oldData.UplineUserType;
                    customer.UplineId = oldData.UplineId; //force set upline id if only match assign to id

                    userType = oldData.UplineUserType;
                    userUplineId = oldData.UplineId;
                }
            }
            else
            {
                string[] SalesPersonCheck = Convert.ToString(form["UplineID"]).Split('-');

                customer.UplineId = Convert.ToInt32(SalesPersonCheck[1]);

                if (SalesPersonCheck[0] == "C")
                {
                    customer.UplineUserType = "Customer";
                    UplineID = "C-" + customer.UplineId;
                    userType = "Customer";
                }
                else
                {
                    customer.UplineUserType = "User";
                    UplineID = "U-" + customer.UplineId;
                    userType = "User";
                }

                userUplineId = customer.UplineId;
                
            }

            int AssignToId = 0;
            if (customer.AssignToId <= 0)
            {
                ModelState.AddModelError("customer.AssignToId", "Assign To is required!");
            }
            else
            {
                AssignToId = customer.AssignToId;
            }

            //Customer Reminder Validation Part
            List<string> customerReminderKeys = form.AllKeys.Where(e => e.Contains("CustomerReminderDescription_")).ToList();

            List<CustomerReminderTable> customerReminderTable = new List<CustomerReminderTable>();

            foreach (string _customerReminderKeys in customerReminderKeys)
            {

                string rowId = _customerReminderKeys.Split('_')[1];
                string Description = form["CustomerReminderDescription_" + rowId];
                string Date = form["CustomerReminderDate_" + rowId];
                string Repeat = form["CustomerReminderRepeat_" + rowId];

                if (string.IsNullOrEmpty(Description))
                {
                    ModelState.AddModelError("CustomerReminderDescription_" + rowId, "Description is required!");
                }

                if (string.IsNullOrEmpty(Date))
                {
                    ModelState.AddModelError("CustomerReminderDate_" + rowId, "Date is required!");
                }

                if (string.IsNullOrEmpty(Repeat))
                {
                    ModelState.AddModelError("CustomerReminderRepeat_" + rowId, "Date is required!");
                }

                customerReminderTable.Add(new CustomerReminderTable()
                {
                    RowId = Convert.ToInt32(rowId),
                    Description = Description,
                    Date = Date,
                    RepeatDDL = new SelectList(RepeatDDL(), "val", "name", Repeat)
                });
            }

            if(ModelState.IsValid)
            {
                //If password not null
                if (ViewData["AbleToUpdate"].ToString() == "Yes")
                {
                    if (!string.IsNullOrEmpty(form["Password"].ToString()))
                    {
                        customer.Password = EncryptionHelper.Encrypt(form["Password"].ToString());
                        password = form["Password"].ToString();
                    }
                    else
                    {
                        //if blank, then assign to original password
                        customer.Password = oldData.Password;
                        password = EncryptionHelper.Decrypt(oldData.Password);
                    }
                }
                else
                {
                    password = EncryptionHelper.Decrypt(oldData.Password);
                    customer.Password = oldData.Password;
                }

                if (checkUser.Role == "Sales")
                {
                    customer.Position = oldData.Position;
                }

                bool result = _customersModel.UpdateData(id, customer);

                CustomerReminder customerReminderRecord = new CustomerReminder();

                //Get Database Customer Reminder Items based on customer ID
                IList<CustomerReminder> getCustomerReminder = _customerRemindersModel.GetPersonalAll(id);

                int getCount = getCustomerReminder.Count;
                int getFrontEndCount = customerReminderTable.Count;

                if(result)
                {
                    string userRole = Session["UserGroup"].ToString();
                    if (userRole == "Sales")
                    {
                        userRole += " " + Session["Position"].ToString();
                    }
                    string tableAffected = "Customers";
                    string description = userRole + " [" + Session["LoginID"].ToString() + "] Updated Customer [" + oldData.CustomerID + "]";

                    bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                    //Remove all the database item based on the customerId and add new items from the table;

                    foreach (CustomerReminder _getCustomerReminder in getCustomerReminder)
                    {
                        int customerReminderID = _getCustomerReminder.ID;
                        bool delete_customer_reminder = _customerRemindersModel.Delete(customerReminderID);
                    }

                    bool hasCustomerReminder = false;

                    //Add customer Reminder Record
                    foreach (CustomerReminderTable _customerReminderTable in customerReminderTable)
                    {

                        customerReminderRecord.CustomerId = oldData.ID;
                        customerReminderRecord.Description = _customerReminderTable.Description;
                        customerReminderRecord.Date = Convert.ToDateTime(_customerReminderTable.Date);
                        customerReminderRecord.Repeat = Convert.ToString(_customerReminderTable.RepeatDDL.SelectedValue);

                        bool add_customer_reminder = _customerRemindersModel.Add(customerReminderRecord);

                        if (add_customer_reminder)
                        {
                            if (!hasCustomerReminder)
                            {
                                hasCustomerReminder = true;
                            }
                        }
                    }

                    if (hasCustomerReminder)
                    {
                        userRole = Session["UserGroup"].ToString();
                        if (userRole == "Sales")
                        {
                            userRole += " " + Session["Position"].ToString();
                        }
                        tableAffected = "CustomerCustomerReminders";
                        description = userRole + " [" + Session["LoginID"].ToString() + "] Created New Customer Customer Reminders [" + oldData.CustomerID + "]";

                        bool customerCustomerReminder_create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);
                    }

                        //loop item if have customer reminder item in database
                        //if(getCount != 0){

                        //    //This is Customer Reminder Update Part.
                        //    foreach (CustomerReminderTable _customerReminderTable in customerReminderTable)
                        //    {
                        //        int count = 1;

                        //        //loop database item and compare (For Update)
                        //        foreach (CustomerReminder _getCustomerReminder in getCustomerReminder)
                        //        {
                        //            if (_customerReminderTable.RowId == count)
                        //            {

                        //                int DatabaseID = _getCustomerReminder.ID;

                        //                customerReminderRecord.CustomerId = _getCustomerReminder.CustomerId;
                        //                customerReminderRecord.Description = _customerReminderTable.Description;
                        //                customerReminderRecord.Date = Convert.ToDateTime(_customerReminderTable.Date);
                        //                customerReminderRecord.Repeat = Convert.ToString(_customerReminderTable.RepeatDDL.SelectedValue);

                        //                bool update_customer_reminder = _customerRemindersModel.Update(DatabaseID, customerReminderRecord);
                        //            }
                        //            count++;
                        //        }

                        //        //For Add New Item
                        //        if (_customerReminderTable.RowId > getCount)//if have add new item
                        //        {
                        //                customerReminderRecord.CustomerId = oldData.ID;
                        //                customerReminderRecord.Description = _customerReminderTable.Description;
                        //                customerReminderRecord.Date = Convert.ToDateTime(_customerReminderTable.Date);
                        //                customerReminderRecord.Repeat = Convert.ToString(_customerReminderTable.RepeatDDL.SelectedValue);

                        //                bool add_customer_reminder = _customerRemindersModel.Add(customerReminderRecord);
                        //        }


                        //    }

                        //}
                        //else
                        //{
                        //    foreach (CustomerReminderTable _customerReminderTable in customerReminderTable)
                        //    {

                        //        customerReminderRecord.CustomerId = oldData.ID;
                        //        customerReminderRecord.Description = _customerReminderTable.Description;
                        //        customerReminderRecord.Date = Convert.ToDateTime(_customerReminderTable.Date);
                        //        customerReminderRecord.Repeat = Convert.ToString(_customerReminderTable.RepeatDDL.SelectedValue);

                        //        bool add_customer_reminder = _customerRemindersModel.Add(customerReminderRecord);
                        //    }
                        //}

                    //Email to "Assign To" User when customer status is Lead.
                    if (customer.Status == "Lead")
                    {
                        //Send email to assign to user.
                        User getUserInfo = _usersModel.GetSingle(customer.AssignToId);

                        if (getUserInfo != null)
                        {

                            if (!string.IsNullOrEmpty(getUserInfo.Email))
                            {

                                string subject = "Task List [" + customer.LoginID + "] Customer Lead Status";
                                string body = System.IO.File.ReadAllText(Server.MapPath("~/Templates/TaskListNotification.html"));
                                string recipient = getUserInfo.Email;
                                string name = getUserInfo.Name;
                                string customerID = oldData.CustomerID;
                                string status = customer.Status;
                                ListDictionary replacements = new ListDictionary();

                                replacements.Add("<%Name%>", name);
                                replacements.Add("<%CustomerID%>", customerID);
                                replacements.Add("<%Status%>", status);
                                replacements.Add("<%Url%>", Url.Action("Edit", "Customer", new { @id = oldData.CustomerID }, "http"));

                                bool sent = EmailHelper.SendEmail(subject, body, replacements, recipient);
                            }
                            else
                            {


                                IList<User> getSuperAdminInfo = _usersModel.GetAll().Where(e => e.Role == "Super Admin").ToList();

                                foreach (User _getSuperAdminInfo in getSuperAdminInfo)
                                {
                                    //Start To Email to all super admin
                                    string subject = "Task List [" + customer.LoginID + "] Customer Lead Status";
                                    string body = System.IO.File.ReadAllText(Server.MapPath("~/Templates/TaskListNotification.html"));
                                    string recipient = _getSuperAdminInfo.Email;
                                    string name = _getSuperAdminInfo.Name;
                                    string customerID = oldData.CustomerID;
                                    string status = customer.Status;

                                    ListDictionary replacements = new ListDictionary();

                                    replacements.Add("<%Name%>", name);
                                    replacements.Add("<%CustomerID%>", customerID);
                                    replacements.Add("<%Status%>", status);
                                    replacements.Add("<%Url%>", Url.Action("Edit", "Customer", new { @id = oldData.CustomerID }, "http"));

                                    bool sent = EmailHelper.SendEmail(subject, body, replacements, recipient);
                                }

                                Session.Add("Result", "success|Assign to user doesn't have an email, so will email to all super admin to update customer status!");
                            }

                        }
                        else
                        {
                            Session.Add("Result", "danger|Assign to user record not found!");
                            return View();
                        }
                    }

                    Session.Add("Result", "success|" + oldData.CustomerID + " has been successfully updated!");

                    return RedirectToAction("Index");
                }
                else
                {
                    Session.Add("Result", "danger|An error occured while update customer records!");
                }

            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            ViewData["CustomerReminderTable"] = customerReminderTable;

            Dropdown[] agencyDDL = AgencyDDL();
            ViewData["AgencyDropdown"] = new SelectList(agencyDDL, "val", "name", null, null, null);

            Dropdown[] uplineDDL = UplineDDL(fromView, userType, userUplineId);
            ViewData["UplineDropdown"] = new SelectList(uplineDDL, "val", "name", null, UplineID, null);

            Dropdown[] positionDDL = PositionDDL();
            ViewData["PositionDropdown"] = new SelectList(positionDDL, "val", "name", null, null, null);

            Dropdown[] assignToDDL = AssignToDDL();
            ViewData["AssignToDropdown"] = new SelectList(assignToDDL, "val", "name", null, AssignToId, null);

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", null, null, null);

            return View();
        }

        //GET: View
        public ActionResult View(int id)
        {
            int MainID = Convert.ToInt32(Session["MainID"]);

            User checkUser = _usersModel.GetSingle(MainID);

            ViewData["CheckUserRole"] = checkUser.Role;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            Customer customer = _customersModel.GetSingle(id);

            if(customer != null){

                //Check is super admin or the main sales person or not
                User checkSuperAdmin = _usersModel.GetSingle(MainID);

                if (checkSuperAdmin.Role != "Super Admin" && customer.AssignToId != checkSuperAdmin.ID)
                {
                    Session.Add("Result", "danger|You are not allow to view this customer!");
                    return RedirectToAction("Index");
                }

                IList<CustomerReminder> getCustomerReminder = _customerRemindersModel.GetPersonalAll(id);

                List<CustomerReminderTable> customerReminderTable = new List<CustomerReminderTable>();

                
                int count = 1;
                foreach(CustomerReminder _getCustomerReminder in getCustomerReminder){
                    CustomerReminderTable saveData = new CustomerReminderTable();

                    saveData.RowId = count;
                    saveData.Description = _getCustomerReminder.Description;
                    saveData.Date = _getCustomerReminder.Date.ToString("dd/MM/yyyy");

                    Dropdown[] repeatDDL = RepeatDDL();
                    saveData.RepeatDDL = new SelectList(repeatDDL, "val", "name", _getCustomerReminder.Repeat);
                    count++;

                    customerReminderTable.Add(saveData);
                }
                ViewData["Customer"] = customer;
                ViewData["CustomerReminderTable"] = customerReminderTable;

                Dropdown[] agencyDDL = AgencyDDL();
                ViewData["AgencyDropdown"] = new SelectList(agencyDDL, "val", "name", null, null, null);

                string UplineID = null;

                if (customer.UplineUserType == "Customer")
                {
                    UplineID = "C-" + customer.UplineId;
                }
                else
                {
                    UplineID = "U-" + customer.UplineId;
                }

                string fromView = "Yes";

                Dropdown[] uplineDDL = UplineDDL(fromView);
                ViewData["UplineDropdown"] = new SelectList(uplineDDL, "val", "name", UplineID);

                Dropdown[] positionDDL = PositionDDL();
                ViewData["PositionDropdown"] = new SelectList(positionDDL, "val", "name", null, null, null);

                Dropdown[] assignToDDL = AssignToDDL();
                ViewData["AssignToDropdown"] = new SelectList(assignToDDL, "val", "name", null, null, customer.AssignToId.ToString());

                Dropdown[] statusDDL = StatusDDL();
                ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", null, null, null);
            }
            else
            {
                Session.Add("Result", "danger|Customer record not found!");
            }

            return View();
        }

        //GET: GetCustomerReminder
        public ActionResult GetCustomerReminder(int id)
        {
            ViewData["RowId"] = id;

            Dropdown[] repeatDDL = RepeatDDL();
            ViewData["RepeatDropdown"] = new SelectList(repeatDDL, "val", "name", null, null, null);

            return View();
        }

        //GET: Delete
        public ActionResult Delete(int id)
        {
            int MainID = Convert.ToInt32(Session["MainID"]);

            Customer customer = _customersModel.GetSingle(id);

            //Check Quotation have link with same Customer ID or not
            List<Quotation> quotation = _quotationsModel.getAllSameCustomerId(id);

            //get the latest quotation one, if previous one, just show all.
            if(quotation.Count > 0)
            {
                Session.Add("Result", "danger|You are not allow to delete this customer because you got link with Quotation!");
                return RedirectToAction("Index");
            }

            //Check Project have link with same Customer ID or not
            IList<Project> project = _projectsModel.GetAllRelatedCustomer(id);

            if(project.Count > 0)
            {
                Session.Add("Result", "danger|You are not allow to delete this customer because you got link with Project!");
                return RedirectToAction("Index");
            }

            if(customer != null)
            {
                //Check is super admin or accounts or the main sales person or not
                User checkSuperAdmin = _usersModel.GetSingle(MainID);

                if ((checkSuperAdmin.Role != "Super Admin" && checkSuperAdmin.Role != "Accounts") && customer.AssignToId != checkSuperAdmin.ID)
                {
                    Session.Add("Result", "danger|You are not allow to delete this customer!");
                    return RedirectToAction("Index");
                }

                bool result = _customersModel.Delete(id);

                if (result)
                {

                    string userRole = Session["UserGroup"].ToString();
                    if (userRole == "Sales")
                    {
                        userRole += " " + Session["Position"].ToString();
                    }
                    string tableAffected = "Customers";
                    string description = userRole + " [" + Session["LoginID"].ToString() + "] Deleted Customer [" + customer.LoginID + "]";

                    bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                    Session.Add("Result", "success|" + customer.LoginID + "successfully deleted!");
                }
                else
                {
                    Session.Add("Result", "danger|Customer record not found!");
                }
            }
            else
            {
                Session.Add("Result", "danger|Customer record not found!");
            }

            return RedirectToAction("Index");
        }

        //GET: Active
        public ActionResult Active(int id)
        {

            Customer customer = _customersModel.GetSingle(id);
            bool result = _customersModel.Active(id);

            if (result)
            {

                int MainID = Convert.ToInt32(Session["MainID"]);
                string userRole = Session["UserGroup"].ToString();
                if (userRole == "Sales")
                {
                    userRole += " " + Session["Position"].ToString();
                }
                string tableAffected = "Customers";
                string description = userRole + " [" + Session["LoginID"].ToString() + "] Activated Customer [" + customer.LoginID + "]";

                bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                Session.Add("Result", "success|Customer " + customer.LoginID + " successfully Activated!");
            }
            else
            {
                Session.Add("Result", "danger|Customer record not found!");
            }

            return RedirectToAction("Index","TaskList");
        }

        //Status Dropdown
        public Dropdown[] StatusDDL()
        {

            Dropdown[] ddl = new Dropdown[3];

            ddl[0] = new Dropdown { name = "Lead", val = "Lead" };
            ddl[1] = new Dropdown { name = "Active", val = "Active" };
            ddl[2] = new Dropdown { name = "Suspend", val = "Suspend" };

            return ddl;
        }

        //Agency Dropdown
        public Dropdown[] AgencyDDL()
        {

            Dropdown[] ddl = new Dropdown[3];

            ddl[0] = new Dropdown { name = "No Agency", val = "No Agency" };
            ddl[1] = new Dropdown { name = "Era", val = "Era" };
            ddl[2] = new Dropdown { name = "Propnex", val = "Propnex" };

            return ddl;
        }

        //Repeat Dropdown
        public Dropdown[] RepeatDDL()
        {

            Dropdown[] ddl = new Dropdown[3];

            ddl[0] = new Dropdown { name = "Yearly", val = "Yearly" };
            ddl[1] = new Dropdown { name = "Monthly", val = "Monthly" };
            ddl[2] = new Dropdown { name = "Daily", val = "Daily" };

            return ddl;
        }

        //Position Dropdown
        public Dropdown[] PositionDDL()
        {

            Dropdown[] ddl = new Dropdown[3];

            ddl[0] = new Dropdown { name = "Customer", val = "Customer" };
            ddl[1] = new Dropdown { name = "Premium Customer", val = "Premium Customer" };
            ddl[2] = new Dropdown { name = "Professional", val = "Professional" };

            return ddl;
        }

        //Assign To Dropdown
        public Dropdown[] AssignToDDL()
        {

            int userid = Convert.ToInt32(Session["MainID"]);

            IList<User> getAllUser = _usersModel.GetAll().Where(e => e.Role != "Accounts").ToList();

            Dropdown[] ddl = new Dropdown[getAllUser.Count + 1];
            int count = 1;

            foreach (User _getAllUser in getAllUser)
            {
                ddl[count] = new Dropdown { name = _getAllUser.UserID + " - " + _getAllUser.Name, val = _getAllUser.ID.ToString() };
                count++;
            }

            return ddl;
        }

        //Upline Dropdown
        public Dropdown[] UplineDDL(string fromView = null, string userType = null, int userUplineId = 0)
        {

            if (!string.IsNullOrEmpty(fromView))
            {
                IList<User> getAllUser = _usersModel.GetAll(new List<string>(), "Active").Where(e => e.Role != "Accounts").ToList();

                IList<Customer> getAllCustomer = _customersModel.GetAll();

                Dropdown[] ddl = new Dropdown[getAllUser.Count + getAllCustomer.Count];
                int count = 0;

                foreach (User _getAllUser in getAllUser)
                {
                    ddl[count] = new Dropdown { name = _getAllUser.UserID + " - " + _getAllUser.Name, val = "U-" + _getAllUser.ID.ToString() };
                    count++;
                }

                foreach (Customer _getAllCustomer in getAllCustomer)
                {
                    if (!string.IsNullOrEmpty(_getAllCustomer.CompanyName))
                    {
                        ddl[count] = new Dropdown { name = _getAllCustomer.CustomerID + " - " + _getAllCustomer.CompanyName, val = "C-" + _getAllCustomer.ID.ToString() };
                    }
                    else
                    {
                        ddl[count] = new Dropdown { name = _getAllCustomer.CustomerID + " - " + _getAllCustomer.ContactPerson, val = "C-" + _getAllCustomer.ID.ToString() };
                    }

                    count++;
                }

                return ddl;
            }
            else
            {

                int userid = Convert.ToInt32(Session["MainID"]);

                User getUserInfo = _usersModel.GetSingle(userid);

                if (getUserInfo.Role == "Super Admin")
                {
                    IList<User> getAllUser = _usersModel.GetAll(new List<string>(), "Active").Where(e => e.Role != "Accounts").ToList();

                    IList<Customer> getAllCustomer = _customersModel.GetAll();

                    Dropdown[] ddl = new Dropdown[getAllUser.Count + getAllCustomer.Count];
                    int count = 0;

                    foreach (User _getAllUser in getAllUser)
                    {
                        ddl[count] = new Dropdown { name = _getAllUser.UserID + " - " + _getAllUser.Name, val = "U-" + _getAllUser.ID.ToString() };
                        count++;
                    }

                    foreach (Customer _getAllCustomer in getAllCustomer)
                    {
                        if (!string.IsNullOrEmpty(_getAllCustomer.CompanyName))
                        {
                            ddl[count] = new Dropdown { name = _getAllCustomer.CustomerID + " - " + _getAllCustomer.CompanyName, val = "C-" + _getAllCustomer.ID.ToString() };
                        }
                        else
                        {
                            ddl[count] = new Dropdown { name = _getAllCustomer.CustomerID + " - " + _getAllCustomer.ContactPerson, val = "C-" + _getAllCustomer.ID.ToString() };
                        }

                        count++;
                    }

                    return ddl;
                }
                //else if (getUserInfo.Role == "Sales")
                //{
                //    User getUser = _usersModel.GetSingle(userid);

                //    //IList<Customer> getAllCustomer = _customersModel.GetAll().Where(e => e.UplineUserType == "User" && e.UplineId == userid).ToList();

                //    Dropdown[] ddl = new Dropdown[1];

                //    ddl[0] = new Dropdown { name = getUser.UserID + " - " + getUser.Name, val = "U-" + getUser.ID.ToString() };

                //    return ddl;
                //}
                else
                {
                    //if not super admin, insert customer record upline id to display front end.
                    //this is because to show those who are not the same upline id, but want to edit purpose.
                    //purpose: add customer record upline id to show front end.

                    IList<User> getAllUser = _usersModel.ListOwnAndDownline(userid).Where(e => e.Role != "Accounts").ToList();

                    IList<Customer> getAllCustomer = _customersModel.GetAll();

                    //if pass from edit ( have userType and userUplineId
                    if (!string.IsNullOrEmpty(userType))
                    {
                        int size = 0;

                        if (userType == "Customer")
                        {
                            Customer getSelectedCustomer = null;

                            size = getAllCustomer.Count;

                            if (userUplineId > 0)
                            {
                                size++;
                                getSelectedCustomer = _customersModel.GetSingle(userUplineId);
                            }

                            Dropdown[] ddl = new Dropdown[getAllUser.Count + size + 1];

                            int count = 1;

                            if (userUplineId > 0)
                            {
                                ddl[count] = new Dropdown { name = getSelectedCustomer.CustomerID + " - " + getSelectedCustomer.ContactPerson, val = Convert.ToString(getSelectedCustomer.ID) };

                                if (!string.IsNullOrEmpty(getSelectedCustomer.CompanyName))
                                {
                                    ddl[count] = new Dropdown { name = getSelectedCustomer.CustomerID + " - " + getSelectedCustomer.CompanyName, val = "C-" + getSelectedCustomer.ID.ToString() };
                                }
                                else
                                {
                                    ddl[count] = new Dropdown { name = getSelectedCustomer.CustomerID + " - " + getSelectedCustomer.ContactPerson, val = "C-" + getSelectedCustomer.ID.ToString() };
                                }
                                
                                
                                count++;

                                getAllCustomer = getAllCustomer.Where(c => c.ID != getSelectedCustomer.ID).ToList();
                            }

                            foreach (User _getAllUser in getAllUser)
                            {
                                ddl[count] = new Dropdown { name = _getAllUser.UserID + " - " + _getAllUser.Name, val = "U-" + _getAllUser.ID.ToString() };
                                count++;
                            }

                            foreach (Customer _getAllCustomer in getAllCustomer)
                            {
                                if (!string.IsNullOrEmpty(_getAllCustomer.CompanyName))
                                {
                                    ddl[count] = new Dropdown { name = _getAllCustomer.CustomerID + " - " + _getAllCustomer.CompanyName, val = "C-" + _getAllCustomer.ID.ToString() };
                                }
                                else
                                {
                                    ddl[count] = new Dropdown { name = _getAllCustomer.CustomerID + " - " + _getAllCustomer.ContactPerson, val = "C-" + _getAllCustomer.ID.ToString() };
                                }

                                count++;
                            }
                            return ddl;
                        }
                        else
                        {
                            User getSelectedUser = null;

                            size = getAllUser.Count;

                            if (userUplineId > 0)
                            {
                                size++;
                                getSelectedUser = _usersModel.GetSingle(userUplineId);
                            }

                            Dropdown[] ddl = new Dropdown[getAllCustomer.Count + size + 1];

                            int count = 1;

                            if (userUplineId > 0)
                            {
                                ddl[count] = new Dropdown { name = getSelectedUser.UserID + " - " + getSelectedUser.Name, val = "U-" + getSelectedUser.ID.ToString() };

                                count++;

                                getAllUser = getAllUser.Where(c => c.ID != getSelectedUser.ID).ToList();
                            }

                            foreach (User _getAllUser in getAllUser)
                            {
                                ddl[count] = new Dropdown { name = _getAllUser.UserID + " - " + _getAllUser.Name, val = "U-" + _getAllUser.ID.ToString() };
                                count++;
                            }

                            foreach (Customer _getAllCustomer in getAllCustomer)
                            {
                                if (!string.IsNullOrEmpty(_getAllCustomer.CompanyName))
                                {
                                    ddl[count] = new Dropdown { name = _getAllCustomer.CustomerID + " - " + _getAllCustomer.CompanyName, val = "C-" + _getAllCustomer.ID.ToString() };
                                }
                                else
                                {
                                    ddl[count] = new Dropdown { name = _getAllCustomer.CustomerID + " - " + _getAllCustomer.ContactPerson, val = "C-" + _getAllCustomer.ID.ToString() };
                                }

                                count++;
                            }
                            return ddl;
                        }

                    }
                    else
                    {
                        //This is UserType = User.
                        Dropdown[] ddl = new Dropdown[getAllUser.Count + getAllCustomer.Count];
                        int count = 0;

                        foreach (User _getAllUser in getAllUser)
                        {
                            ddl[count] = new Dropdown { name = _getAllUser.UserID + " - " + _getAllUser.Name, val = "U-" + _getAllUser.ID.ToString() };
                            count++;
                        }

                        foreach (Customer _getAllCustomer in getAllCustomer)
                        {
                            if (!string.IsNullOrEmpty(_getAllCustomer.CompanyName))
                            {
                                ddl[count] = new Dropdown { name = _getAllCustomer.CustomerID + " - " + _getAllCustomer.CompanyName, val = "C-" + _getAllCustomer.ID.ToString() };
                            }
                            else
                            {
                                ddl[count] = new Dropdown { name = _getAllCustomer.CustomerID + " - " + _getAllCustomer.ContactPerson, val = "C-" + _getAllCustomer.ID.ToString() };
                            }

                            count++;
                        }
                        return ddl;
                    }

                    //IList<User> getAllUser = _usersModel.ListOwnAndDownline(userid).Where(e => e.Role != "Accounts").ToList();

                    //IList<Customer> getAllCustomer = _customersModel.GetAll().Where(e => e.UplineUserType == "User" && e.UplineId == userid).ToList();

                    //Dropdown[] ddl = new Dropdown[getAllUser.Count + getAllCustomer.Count];
                    //int count = 0;

                    //foreach (User _getAllUser in getAllUser)
                    //{
                    //    ddl[count] = new Dropdown { name = _getAllUser.UserID + " - " + _getAllUser.Name, val = "U-" + _getAllUser.ID.ToString() };
                    //    count++;
                    //}

                    //foreach (Customer _getAllCustomer in getAllCustomer)
                    //{
                    //    if (!string.IsNullOrEmpty(_getAllCustomer.CompanyName))
                    //    {
                    //        ddl[count] = new Dropdown { name = _getAllCustomer.CustomerID + " - " + _getAllCustomer.CompanyName, val = "C-" + _getAllCustomer.ID.ToString() };
                    //    }
                    //    else
                    //    {
                    //        ddl[count] = new Dropdown { name = _getAllCustomer.CustomerID + " - " + _getAllCustomer.ContactPerson, val = "C-" + _getAllCustomer.ID.ToString() };
                    //    }

                    //    count++;
                    //}
                    //return ddl;
                }
            }
        }
    }
}