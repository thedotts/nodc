﻿using DataAccess.POCO;
using NODC_OMS.Helper;
using NODC_OMS.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NODC_OMS.Controllers
{
    [HandleError]
    [RedirecttingActionAdmin]
    public class ManagePayoutController : ControllerBase
    {
        private IEarlyPayoutRepository _earlyPayoutsModel;
        private IUserRepository _usersModel;
        private ICustomerRepository _customersModel;
        private IProjectRepository _projectsModel;
        private ISystemSettingRepository _settingsModel;

        public ManagePayoutController():this(
            new EarlyPayoutRepository(),
            new UserRepository(),
            new CustomerRepository(),
            new ProjectRepository(),
            new SystemSettingRepository())
        {

        }

        public ManagePayoutController(
            IEarlyPayoutRepository earlyPayoutsModel,
            IUserRepository usersModel,
            ICustomerRepository customersModel,
            IProjectRepository projectsModel,
            ISystemSettingRepository settingsModel)
        {
            _earlyPayoutsModel = earlyPayoutsModel;
            _usersModel = usersModel;
            _customersModel = customersModel;
            _projectsModel = projectsModel;
            _settingsModel = settingsModel;
        }

        // GET: ManagePayout
        public ActionResult Index()
        {
            if (Session["SearchKeyword"] != null)
            {
                Session.Remove("SearchKeyword");
            }

            if (Session["SearchStatus"] != null)
            {
                Session.Remove("SearchStatus");
            }

            if (Session["SearchPeriod"] != null)
            {
                Session.Remove("SearchPeriod");
            }

            return RedirectToAction("Listing");
        }

        //GET: Listing
        public ActionResult Listing(int page = 1)
        {
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = "";
            if (Session["SearchKeyword"] != null)
            {
                ViewData["SearchKeyword"] = Session["SearchKeyword"];
            }

            ViewData["SearchStatus"] = "";
            if (Session["SearchStatus"] != null)
            {
                ViewData["SearchStatus"] = Session["SearchStatus"];
            }

            ViewData["GetStatusValue"] = "All Payouts";
            if (ViewData["SearchStatus"].ToString() == "Unpaid")
            {
                ViewData["GetStatusValue"] = "Unpaid Only";
            }

            ViewData["SearchPeriod"] = "";
            if (Session["SearchPeriod"] != null)
            {
                ViewData["SearchPeriod"] = Session["SearchPeriod"];
            }

            ViewData["GetPeriodValue"] = "Current Month";
            if (string.IsNullOrEmpty(ViewData["GetPeriodValue"].ToString()))
            {
                ViewData["GetPeriodValue"] = "All";
            }
            else
            {
                if (ViewData["GetPeriodValue"].ToString() == "Last Month")
                {
                    ViewData["GetPeriodValue"] = "Last Month";
                }
                else if (ViewData["GetPeriodValue"].ToString() == "Last Quarter")
                {
                    ViewData["GetPeriodValue"] = "Last Quarter";
                }
            }

            IPagedList<EarlyPayout> earlyPayouts = _earlyPayoutsModel.GetPaged(ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), ViewData["SearchStatus"].ToString(), page, pageSize);
            ViewData["EarlyPayout"] = earlyPayouts;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Listing
        [HttpPost]
        public ActionResult Listing(FormCollection form)
        {
            int page = 1;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = form["SearchKeyword"].Trim();
            Session.Add("SearchKeyword", form["SearchKeyword"].Trim());

            ViewData["SearchStatus"] = form["SearchStatusValue"].Trim();
            Session.Add("SearchStatus", form["SearchStatusValue"].Trim());

            ViewData["GetStatusValue"] = "All Early Payouts";
            if (ViewData["SearchStatus"].ToString() == "Unpaid")
            {
                ViewData["GetStatusValue"] = "Unpaid Only";
            }

            ViewData["SearchPeriod"] = form["SearchPeriodValue"].Trim();
            Session.Add("SearchPeriod", form["SearchPeriodValue"].Trim());

            ViewData["GetPeriodValue"] = "Current Month";
            if (string.IsNullOrEmpty(ViewData["SearchPeriod"].ToString()))
            {
                ViewData["GetPeriodValue"] = "All";
            }
            else
            {
                if (ViewData["SearchPeriod"].ToString() == "Last Month")
                {
                    ViewData["GetPeriodValue"] = "Last Month";
                }
                else if (ViewData["SearchPeriod"].ToString() == "Last Quarter")
                {
                    ViewData["GetPeriodValue"] = "Last Quarter";
                }
            }

            IPagedList<EarlyPayout> earlyPayouts = _earlyPayoutsModel.GetPaged(ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), ViewData["SearchPeriod"].ToString(), page, pageSize);
            ViewData["EarlyPayout"] = earlyPayouts;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: EarlyPayout
        public ActionResult EarlyPayout()
        {
            Dropdown[] projectDDL = ProjectDDL();
            ViewData["ProjectDropdown"] = new SelectList(projectDDL, "val", "name");

            Dropdown[] payToDDL = PayToDDL();
            ViewData["PayToDropdown"] = new SelectList(payToDDL, "val", "name", "optGroup", null, null);

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: EarlyPayout
        [HttpPost]
        public ActionResult EarlyPayout(EarlyPayout earlyPayout, FormCollection form)
        {
            ModelState amountState = ModelState["earlyPayout.Amount"];
            if (amountState.Errors.Count > 0)
            {
                amountState.Errors.Clear();
            }

            if (string.IsNullOrEmpty(form["PayTo"]))
            {
                ModelState.AddModelError("earlyPayout.PayToId", "Pay To is required!");
            }

            if (!string.IsNullOrEmpty(form["earlyPayout.PayoutDate"]))
            {
                DateTime now = DateTime.Now;
                DateTime start = new DateTime(now.Year, now.Month, 1);
                DateTime end = start.AddMonths(1).AddDays(-1);

                if (earlyPayout.PayoutDate < start || earlyPayout.PayoutDate > end)
                {
                    ModelState.AddModelError("earlyPayout.PayoutDate", "Payout Date is out of acceptable range!");
                }
            }

            if (string.IsNullOrEmpty(form["earlyPayout.Amount"]))
            {
                ModelState.AddModelError("earlyPayout.Amount", "Amount is required!");
            }
            else
            {
                string amount = form["earlyPayout.Amount"].ToString();

                bool checkFormat = FormValidationHelper.AmountFormat(amount);

                if (!checkFormat)
                {
                    ModelState.AddModelError("earlyPayout.Amount", "'" + amount + "' is not a valid Amount!");
                }
            }

            if (ModelState.IsValid)
            {
                string prefix = _settingsModel.GetCodeValue("PREFIX_EARLY_PAYOUT_ID") + DateTime.Now.ToString("yy");

                EarlyPayout lastRecord = _earlyPayoutsModel.GetLast();

                if (lastRecord != null)
                {
                    earlyPayout.EarlyPayoutID = prefix + (lastRecord.ID + 1).ToString().PadLeft(5, '0');
                }
                else
                {
                    earlyPayout.EarlyPayoutID = prefix + "00001";
                }

                string[] payTo = form["PayTo"].ToString().Split('|');

                earlyPayout.PayToId = Convert.ToInt32(payTo[1]);
                earlyPayout.UserType = payTo[0];

                earlyPayout.Amount = Convert.ToDecimal(form["earlyPayout.Amount"]);
                earlyPayout.Status = "Unpaid";

                bool result = _earlyPayoutsModel.Add(earlyPayout);

                if (result)
                {
                    int userId = Convert.ToInt32(Session["MainID"]);
                    string userRole = Session["UserGroup"].ToString();
                    if (userRole == "Sales")
                    {
                        userRole += " " + Session["Position"].ToString();
                    }
                    string tableAffected = "EarlyPayouts";
                    string description = userRole + " [" + Session["LoginID"].ToString() + "] Created Early Payout [" + earlyPayout.EarlyPayoutID + "]";

                    bool earlyPayout_create_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);

                    Session.Add("Result", "success|" + earlyPayout.EarlyPayoutID + " has been successfully created!");

                    int page = 1;

                    if (Session["Page"] != null)
                    {
                        page = Convert.ToInt32(Session["Page"]);
                    }

                    return RedirectToAction("Listing", new { @page = page });
                }
                else
                {
                    Session.Add("Result", "danger|An error occured while saving early payout records!");
                }
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            Dropdown[] projectDDL = ProjectDDL();
            ViewData["ProjectDropdown"] = new SelectList(projectDDL, "val", "name", earlyPayout.ProjectId);

            Dropdown[] payToDDL = PayToDDL();
            ViewData["PayToDropdown"] = new SelectList(payToDDL, "val", "name", "optGroup", form["PayTo"], null);

            ViewData["IsPostBack"] = true;
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Edit
        public ActionResult Edit(int id)
        {
            EarlyPayout earlyPayout = _earlyPayoutsModel.GetSingle(id);

            if (earlyPayout == null)
            {
                Session.Add("Result", "danger|Early Payout record not found!");

                int page = 1;

                if (Session["Page"] != null)
                {
                    page = Convert.ToInt32(Session["Page"]);
                }

                return RedirectToAction("Listing", new { @page = page });
            }
            else if (earlyPayout.Status == "Completed")
            {
                return RedirectToAction("View", new { @id = id });
            }

            Dropdown[] projectDDL = ProjectDDL();
            ViewData["ProjectDropdown"] = new SelectList(projectDDL, "val", "name", earlyPayout.ProjectId);

            Dropdown[] payToDDL = PayToDDL();
            ViewData["PayToDropdown"] = new SelectList(payToDDL, "val", "name", "optGroup", earlyPayout.UserType + "|" + earlyPayout.PayToId, null);

            ViewData["EarlyPayout"] = earlyPayout;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Edit
        [HttpPost]
        public ActionResult Edit(int id, EarlyPayout earlyPayout, FormCollection form)
        {
            EarlyPayout oldData = _earlyPayoutsModel.GetSingle(id);
            earlyPayout.ID = oldData.ID;
            earlyPayout.EarlyPayoutID = oldData.EarlyPayoutID;
            earlyPayout.Status = oldData.Status;

            ModelState amountState = ModelState["earlyPayout.Amount"];
            if (amountState.Errors.Count > 0)
            {
                amountState.Errors.Clear();
            }

            if (string.IsNullOrEmpty(form["PayTo"]))
            {
                ModelState.AddModelError("earlyPayout.PayToId", "Pay To is required!");
            }

            if (!string.IsNullOrEmpty(form["earlyPayout.PayoutDate"]))
            {
                DateTime now = DateTime.Now;
                DateTime start = new DateTime(now.Year, now.Month, 1);
                DateTime end = start.AddMonths(1).AddDays(-1);

                if (earlyPayout.PayoutDate < start || earlyPayout.PayoutDate > end)
                {
                    if (earlyPayout.PayoutDate != oldData.PayoutDate)
                    {
                        ModelState.AddModelError("earlyPayout.PayoutDate", "Payout Date is out of acceptable range!");
                    }
                }
            }

            if (string.IsNullOrEmpty(form["earlyPayout.Amount"]))
            {
                ModelState.AddModelError("earlyPayout.Amount", "Amount is required!");
            }
            else
            {
                string amount = form["earlyPayout.Amount"].ToString();

                bool checkFormat = FormValidationHelper.AmountFormat(amount);

                if (!checkFormat)
                {
                    ModelState.AddModelError("earlyPayout.Amount", "'" + amount + "' is not a valid Amount!");
                }
            }

            if (ModelState.IsValid)
            {
                string[] payTo = form["PayTo"].ToString().Split('|');

                earlyPayout.PayToId = Convert.ToInt32(payTo[1]);
                earlyPayout.UserType = payTo[0];

                earlyPayout.Amount = Convert.ToDecimal(form["earlyPayout.Amount"]);

                bool result = _earlyPayoutsModel.Update(id, earlyPayout);

                if (result)
                {
                    int userId = Convert.ToInt32(Session["MainID"]);
                    string userRole = Session["UserGroup"].ToString();
                    if (userRole == "Sales")
                    {
                        userRole += " " + Session["Position"].ToString();
                    }
                    string tableAffected = "EarlyPayouts";
                    string description = userRole + " [" + Session["LoginID"].ToString() + "] Updated Early Payout [" + earlyPayout.EarlyPayoutID + "]";

                    bool earlyPayout_update_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);

                    Session.Add("Result", "success|" + earlyPayout.EarlyPayoutID + " has been successfully updated!");

                    int page = 1;

                    if (Session["Page"] != null)
                    {
                        page = Convert.ToInt32(Session["Page"]);
                    }

                    return RedirectToAction("Listing", new { @page = page });
                }
                else
                {
                    Session.Add("Result", "danger|An error occured while saving early payout records!");
                }
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            Dropdown[] projectDDL = ProjectDDL();
            ViewData["ProjectDropdown"] = new SelectList(projectDDL, "val", "name", earlyPayout.ProjectId);

            Dropdown[] payToDDL = PayToDDL();
            ViewData["PayToDropdown"] = new SelectList(payToDDL, "val", "name", "optGroup", form["PayTo"], null);

            ViewData["EarlyPayout"] = earlyPayout;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Delete
        public ActionResult Delete(int id)
        {
            EarlyPayout earlyPayout = _earlyPayoutsModel.GetSingle(id);

            if (earlyPayout == null)
            {
                Session.Add("Result", "danger|Early Payout record not found!");
            }
            else if (earlyPayout.Status == "Completed")
            {
                Session.Add("Result", "danger|Completed Early Payout cannot be deleted!");
            }
            else
            {
                bool result = _earlyPayoutsModel.Delete(id);

                if (result)
                {
                    int userId = Convert.ToInt32(Session["MainID"]);
                    string userRole = Session["UserGroup"].ToString();
                    if (userRole == "Sales")
                    {
                        userRole += " " + Session["Position"].ToString();
                    }
                    string tableAffected = "EarlyPayouts";
                    string description = userRole + " [" + Session["LoginID"].ToString() + "] Updated Early Payout [" + earlyPayout.EarlyPayoutID + "]";

                    bool earlyPayout_update_log = AuditLogHelper.WriteAuditLog(userId, userRole, tableAffected, description);

                    Session.Add("Result", "success|" + earlyPayout.EarlyPayoutID + " has been successfully updated!");
                }
                else
                {
                    Session.Add("Result", "danger|An error occured while deleting early payout record!");
                }
            }

            int page = 1;

            if (Session["Page"] != null)
            {
                page = Convert.ToInt32(Session["Page"]);
            }

            return RedirectToAction("Listing", new { @page = page });
        }

        //GET: View
        public ActionResult View(int id)
        {
            EarlyPayout earlyPayout = _earlyPayoutsModel.GetSingle(id);

            if (earlyPayout == null)
            {
                Session.Add("Result", "danger|Early Payout record not found!");

                int page = 1;

                if (Session["Page"] != null)
                {
                    page = Convert.ToInt32(Session["Page"]);
                }

                return RedirectToAction("Listing", new { @page = page });
            }

            Dropdown[] projectDDL = ProjectDDL();
            ViewData["ProjectDropdown"] = new SelectList(projectDDL, "val", "name", earlyPayout.ProjectId);

            Dropdown[] payToDDL = PayToDDL();
            ViewData["PayToDropdown"] = new SelectList(payToDDL, "val", "name", "optGroup", earlyPayout.UserType + "|" + earlyPayout.PayToId, null);

            ViewData["EarlyPayout"] = earlyPayout;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: ValidateAmount
        public string ValidateAmount(string amount)
        {
            bool result = FormValidationHelper.AmountFormat(amount);

            string res = "";

            if (result)
            {
                string newAmount = FormValidationHelper.AmountFormatter(Convert.ToDecimal(amount), 2);

                res = "{\"Result\":true,\"NewAmount\":\"" + newAmount + "\"}";
            }
            else
            {
                res = "{\"Result\":false,\"ErrorMessage\":\"'" + amount + "' is not a valid amount!\"}";
            }

            return res;
        }

        //Project Dropdown
        public Dropdown[] ProjectDDL()
        {
            IList<Project> projects = _projectsModel.GetCompletedProjects();

            Dropdown[] ddl = new Dropdown[projects.Count + 1];

            int count = 1;

            foreach (Project project in projects)
            {
                if (!string.IsNullOrEmpty(project.Customer.CompanyName))
                {
                    ddl[count] = new Dropdown { name = project.ProjectID + " - " + project.Customer.CompanyName, val = project.ID.ToString() };
                }
                else
                {
                    ddl[count] = new Dropdown { name = project.ProjectID + " - " + project.Customer.ContactPerson, val = project.ID.ToString() };
                }

                count++;
            }

            return ddl;
        }

        //Pay To Dropdown
        public Dropdown[] PayToDDL()
        {
            IList<Customer> customers = _customersModel.GetActiveCustomers();
            IList<User> salepersons = _usersModel.GetAll("Sales", "Active");

            int size = customers.Count + salepersons.Count + 1;

            Dropdown[] ddl = new Dropdown[size];

            int count = 1;

            foreach (Customer customer in customers)
            {
                if (!string.IsNullOrEmpty(customer.CompanyName))
                {
                    ddl[count] = new Dropdown { name = customer.CustomerID + " - " + customer.CompanyName, val = "Customer|" + customer.ID, optGroup = "Customers" };
                }
                else
                {
                    ddl[count] = new Dropdown { name = customer.CustomerID + " - " + customer.ContactPerson, val = "Customer|" + customer.ID, optGroup = "Customers" };
                }

                count++;
            }

            foreach (User saleperson in salepersons)
            {
                ddl[count] = new Dropdown { name = saleperson.UserID + " - " + saleperson.Name, val = "Salesperson|" + saleperson.ID, optGroup = "Sales" };

                count++;
            }

            return ddl;
        }
    }
}