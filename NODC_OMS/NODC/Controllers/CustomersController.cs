﻿using DataAccess;
using DataAccess.POCO;
using Newtonsoft.Json;
using NODC_OMS.Controllers;
using NODC_OMS.Helper;
using NODC_OMS.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Services;

namespace NODC_OMS.Controllers
{
    [HandleError]
    [RedirectingActionCustomer]
    public class CustomersController : ControllerBase
    {
        private ICustomerRepository _customersModel;
        private IUserRepository _usersModel;
        private IQuotationRepository _quotationsModel;
        private IQuotationCoBrokeRepository _quotationCoBrokesModel;
        private IQuotationItemDescriptionRepository _quotationItemDescriptionsModel;
        private IProjectRepository _projectsModel;
        private IProjectProjectExpensesRepository _projectExpensesModel;
        private IInvoiceRepository _invoicesModel;
        private IInvoiceItemDescriptionRepository _invoiceItemsModel;
        private ISystemSettingRepository _settingsModel;
        private IPayoutRepository _payoutsModel;
        private IPayoutCalculationRepository _payoutCalculationsModel;

        // GET: TaskList
        public CustomersController()
            : this(new CustomerRepository(), new UserRepository(), new QuotationRepository(),new QuotationCoBrokeRepository(), new QuotationItemDescriptionRepository(), new ProjectRepository(), new ProjectProjectExpensesRepository(), new InvoiceRepository(), new InvoiceItemDescriptionRepository(), new SystemSettingRepository(), new PayoutRepository(), new PayoutCalculationRepository())
        {

        }

        public CustomersController(ICustomerRepository customersModel, IUserRepository usersModel, IQuotationRepository quotationsModel,IQuotationCoBrokeRepository quotationCoBrokesModel, IQuotationItemDescriptionRepository quotationItemDescriptionsModel, IProjectRepository projectsModel, IProjectProjectExpensesRepository projectExpensesModel, IInvoiceRepository invoicesModel, IInvoiceItemDescriptionRepository invoiceItemsModel, ISystemSettingRepository settingsModel, IPayoutRepository payoutsModel, IPayoutCalculationRepository payoutCalculationsModel)
        {
            _customersModel = customersModel;
            _usersModel = usersModel;
            _quotationsModel = quotationsModel;
            _quotationCoBrokesModel = quotationCoBrokesModel;
            _quotationItemDescriptionsModel = quotationItemDescriptionsModel;
            _projectsModel = projectsModel;
            _projectExpensesModel = projectExpensesModel;
            _invoicesModel = invoicesModel;
            _invoiceItemsModel = invoiceItemsModel;
            _settingsModel = settingsModel;
            _payoutsModel = payoutsModel;
            _payoutCalculationsModel = payoutCalculationsModel;
        }

        // GET: Index
        public ActionResult Index(string mod = null)
        {
            if (Session["SearchKeyword"] != null)
            {
                Session.Remove("SearchKeyword");
            }

            switch (mod)
            {
                case "qo": return RedirectToAction("ListingQuotation");
                case "pr": return RedirectToAction("ListingProject");
                case "py": return RedirectToAction("ListingPayout");
                case "iv": return RedirectToAction("ListingInvoice");
                default: return RedirectToAction("ListingQuotation");
            }
        }

        //GET: ListingQuotation
        public ActionResult ListingQuotation(int page = 1)
        {

            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);

            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = "";
            ViewData["SearchStatus"] = "";

            string SalesPersonType = "Customer";

            int MainID = Convert.ToInt32(Session["MainID"]);

            IPagedList<Quotation> quotations = _quotationsModel.GetPagedCustomer(ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), MainID, page, pageSize);

            List<QuotationItemDescription> newTableItemDescriptions = new List<QuotationItemDescription>();

            foreach (Quotation _quotations in quotations)
            {
                int mainID = _quotations.ID;

                List<QuotationItemDescription> quotationsAmount = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(mainID);

                QuotationItemDescription itemRecord = new QuotationItemDescription();

                foreach (QuotationItemDescription _quotationsAmount in quotationsAmount)
                {
                    itemRecord.ID = _quotationsAmount.ID;
                    itemRecord.QuotationId = _quotationsAmount.QuotationId;
                    itemRecord.QuotationItemAmount += _quotationsAmount.QuotationItemAmount;
                }

                newTableItemDescriptions.Add(itemRecord);
            }

            ViewData["Quotation"] = quotations;
            ViewData["GetListingAmount"] = newTableItemDescriptions;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: ListingQuotation
        [HttpPost]
        public ActionResult ListingQuotation(FormCollection form)
        {
            int page = 1;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);

            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = form["SearchKeyword"].Trim();
            Session.Add("SearchKeyword", form["SearchKeyword"].Trim());

            ViewData["SearchStatus"] = "";

            //string SalesPersonType = "Customer";

            int MainID = Convert.ToInt32(Session["MainID"]);

            IPagedList<Quotation> quotations = _quotationsModel.GetPagedCustomer(ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), MainID, page, pageSize);

            List<QuotationItemDescription> newTableItemDescriptions = new List<QuotationItemDescription>();

            foreach (Quotation _quotations in quotations)
            {
                int mainID = _quotations.ID;

                List<QuotationItemDescription> quotationsAmount = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(mainID);

                QuotationItemDescription itemRecord = new QuotationItemDescription();

                foreach (QuotationItemDescription _quotationsAmount in quotationsAmount)
                {
                    itemRecord.ID = _quotationsAmount.ID;
                    itemRecord.QuotationId = _quotationsAmount.QuotationId;
                    itemRecord.QuotationItemAmount += _quotationsAmount.QuotationItemAmount;
                }

                newTableItemDescriptions.Add(itemRecord);
            }

            ViewData["Quotation"] = quotations;
            ViewData["GetListingAmount"] = newTableItemDescriptions;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: ViewQuotation
        public ActionResult ViewQuotation(int id)
        {

            int MainID = Convert.ToInt32(Session["MainID"]);

            //Get Quotation 
            Quotation quotation = _quotationsModel.GetSingle(id);
            ViewData["Quotation"] = quotation;

            //Get Quotation CoBroke Item
            List<QuotationCoBroke> quotationCoBroke = _quotationCoBrokesModel.GetAllRelatedQuotationId(id);
            ViewData["QuotationCoBroke"] = quotationCoBroke;

            //Get Quotation Item Description

            //New Get Quotation Item Description
            List<QuotationItemDescription> quotationItemDescription = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(id);
            List<ItemDescriptionBigTable> quotationItemDescriptionBigTable = new List<ItemDescriptionBigTable>();

            //Loop Item Description Items
            IList<SystemSetting> listItem = _settingsModel.GetAll();
            SystemSetting CategoryList = listItem.Where(e => e.Code == "QUOTATION_CATEGORIES").FirstOrDefault();

            string[] categoryList = CategoryList.Value.Split('|');

            //Start
            int countItemDescription = 1;
            int resetNumber = 1;
            foreach (QuotationItemDescription _quotationItemDescription in quotationItemDescription)//14 items
            {
                string ItemName = _quotationItemDescription.CategoryName;
                decimal ItemQty = (decimal)_quotationItemDescription.QuotationItemQty;
                decimal ItemRate = (decimal)_quotationItemDescription.QuotationItemRate;
                decimal ItemEstCost = (decimal)_quotationItemDescription.QuotationItemEstCost;
                bool IsBasic = false; //default set to false means it is not from db item.

                List<ItemDescriptionRow> ItemDescriptionRows = new List<ItemDescriptionRow>();

                if (ItemQty == null)
                {
                    ItemQty = 0;
                }

                if (ItemRate == null)
                {
                    ItemRate = 0;
                }

                if (ItemEstCost == null)
                {
                    ItemEstCost = 0;
                }


                foreach (string _categoryList in categoryList)
                {
                    if (_categoryList == _quotationItemDescription.CategoryName)
                    {
                        IsBasic = true;
                    }
                }

                if (quotationItemDescriptionBigTable.Count == 0)
                {
                    resetNumber = 1;
                    ItemDescriptionRows.Add(new ItemDescriptionRow()
                    {
                        RowId = resetNumber,
                        Description = _quotationItemDescription.QuotationItem_Description,
                        Qty = Convert.ToDecimal(_quotationItemDescription.QuotationItemQty).ToString("#,##0.0"),
                        Unit = _quotationItemDescription.QuotationItemUnit,
                        Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                        Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                        Est_Cost = null,
                        AdjCost = null,
                        Est_Profit = null,
                        Remarks = null,
                        Mgr_Comments = null
                    });

                    quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                    {
                        TableID = countItemDescription,
                        CategoryName = _quotationItemDescription.CategoryName,
                        IsBasic = IsBasic,
                        ItemDescriptionRows = ItemDescriptionRows
                    });
                    countItemDescription++;
                }
                else
                {
                    if (quotationItemDescriptionBigTable.Where(e => e.CategoryName == _quotationItemDescription.CategoryName).FirstOrDefault() != null)
                    {
                        foreach (ItemDescriptionBigTable _loopBigTable in quotationItemDescriptionBigTable)
                        {
                            if (_loopBigTable.CategoryName == _quotationItemDescription.CategoryName)
                            {
                                _loopBigTable.ItemDescriptionRows.Add(new ItemDescriptionRow()
                                {
                                    RowId = resetNumber,
                                    Description = _quotationItemDescription.QuotationItem_Description,
                                    Qty = Convert.ToDecimal(_quotationItemDescription.QuotationItemQty).ToString("#,##0.0"),
                                    Unit = _quotationItemDescription.QuotationItemUnit,
                                    Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                                    Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                                    Est_Cost = null,
                                    AdjCost = null,
                                    Est_Profit = null,
                                    Remarks = null,
                                    Mgr_Comments = null
                                });
                                break;
                            }
                        }
                    }
                    else
                    {
                        resetNumber = 1;
                        ItemDescriptionRows.Add(new ItemDescriptionRow()
                        {
                            RowId = resetNumber,
                            Description = _quotationItemDescription.QuotationItem_Description,
                            Qty = Convert.ToDecimal(_quotationItemDescription.QuotationItemQty).ToString("#,##0.0"),
                            Unit = _quotationItemDescription.QuotationItemUnit,
                            Rate = Convert.ToDecimal(_quotationItemDescription.QuotationItemRate).ToString("#,##0.00"),
                            Amount = Convert.ToDecimal(_quotationItemDescription.QuotationItemAmount).ToString("#,##0.00"),
                            Est_Cost = null,
                            AdjCost = null,
                            Est_Profit = null,
                            Remarks = null,
                            Mgr_Comments = null
                        });

                        quotationItemDescriptionBigTable.Add(new ItemDescriptionBigTable()
                        {
                            TableID = countItemDescription,
                            CategoryName = _quotationItemDescription.CategoryName,
                            IsBasic = IsBasic,
                            ItemDescriptionRows = ItemDescriptionRows
                        });
                        countItemDescription++;
                    }
                }
                resetNumber++;
                //End
            }

            ViewData["QuotationItemDescription"] = quotationItemDescriptionBigTable;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();

            ViewData["SalesDocumentFiles"] = quotation.SalesDocumentUploads;

            ViewData["PreparedBy"] = quotation.PreparedBy.Name;

            ViewData["QuotationDate"] = DateTime.Now.ToString("dd/MM/yyyy");

            Dropdown[] companyDDL = CompanyDDL();
            ViewData["CompanyDropdown"] = new SelectList(companyDDL, "val", "name", null);

            Dropdown[] customerDDL = CustomerDDL();
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", null);

            Dropdown[] userDDL = UserDDL();
            ViewData["UserDropdown"] = new SelectList(userDDL, "val", "name", null);

            return View();
        }

        ////Approve Quotation
        //[HttpPost]
        //public ActionResult ApproveQuotation(int id, FormCollection form)
        //{
        //    int MainID = Convert.ToInt32(Session["MainID"]);

        //    Quotation quotation = _quotationsModel.GetSingle(id);

        //    if (quotation.Status != "Pending Customer Confirmation")
        //    {
        //        Session.Add("Result", "danger|Quotation " + quotation.QuotationID + " is not allow to do approval action!");

        //        return RedirectToAction("Index");
        //    }

        //    quotation.Status = "Confirmed";

        //    //string SalesDocumentUpload = quotation.SalesDocumentUploads;

        //    bool update_quotation_status = _quotationsModel.UpdateQuotationApprovalStatus(quotation.ID, quotation, quotation.SalesDocumentUploads);

        //    if (update_quotation_status)
        //    {
        //        string userRole = "Customer";
        //        string tableAffected = "Quotations";
        //        string description = userRole + " [" + Session["LoginID"].ToString() + "] Confirmed [" + quotation.QuotationID + "] Quotation";

        //        bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

        //        Session.Add("Result", "success|Quotation " + quotation.QuotationID + " Confirmed!");
        //    }
        //    else
        //    {
        //        Session.Add("Result", "danger|Quotation " + quotation.QuotationID + " failed to confirm due to some issue!");
        //    }


        //    return RedirectToAction("Index", "Customers");
        //}

        //Reject Quotation
        //[HttpPost]
        //public ActionResult RejectQuotation(int id, FormCollection form)
        //{
        //    int MainID = Convert.ToInt32(Session["MainID"]);

        //    Quotation quotation = _quotationsModel.GetSingle(id);

        //    if (quotation.Status != "Pending Customer Confirmation")
        //    {
        //        Session.Add("Result", "danger|Quotation " + quotation.QuotationID + " is not allow to do reject action!");

        //        return RedirectToAction("Index");
        //    }

        //    quotation.Status = "Cancelled by Customer";

        //    bool update_quotation_status = _quotationsModel.UpdateQuotationApprovalStatus(quotation.ID, quotation);

        //    if (update_quotation_status)
        //    {
        //        string userRole = "Customer";
        //        string tableAffected = "Quotations";
        //        string description = userRole + " [" + Session["LoginID"].ToString() + "] Cancelled [" + quotation.QuotationID + "] Quotation";

        //        bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

        //        Session.Add("Result", "success|Quotation " + quotation.QuotationID + " Cancelled!");
        //    }
        //    else
        //    {
        //        Session.Add("Result", "danger|Quotation " + quotation.QuotationID + " failed to cancel due to some issue!");
        //    }


        //    return RedirectToAction("Index", "Customers");
        //}

        //GET: ListingProject
        public ActionResult ListingProject(int page = 1)
        {
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);

            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = "";
            ViewData["SearchStatus"] = "";


            //get Customer ID first
            int MainID = Convert.ToInt32(Session["MainID"]);

            List<int> SalesId = new List<int>();
            IPagedList<Project> project = _projectsModel.GetPaged(ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), MainID, SalesId, page, pageSize);

            ViewData["Project"] = project;

            IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetAll();

            ViewData["ProjectExpenses"] = projectExpenses;

            IList<QuotationItemDescription> quotationItemDescription = _quotationItemDescriptionsModel.GetAll();

            ViewData["QuotationItemDescription"] = quotationItemDescription;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: ListingProject
        [HttpPost]
        public ActionResult ListingProject(FormCollection form)
        {
            int MainID = Convert.ToInt32(Session["MainID"]);

            int page = 1;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = form["SearchKeyword"].Trim();
            Session.Add("SearchKeyword", form["SearchKeyword"].Trim());

            ViewData["SearchStatus"] = "";

            List<int> SalesId = new List<int>();

            IPagedList<Project> project = _projectsModel.GetPaged(ViewData["SearchKeyword"].ToString(), ViewData["SearchStatus"].ToString(), MainID, SalesId, page, pageSize);

            ViewData["Project"] = project;

            IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetAll();

            ViewData["ProjectExpenses"] = projectExpenses;

            IList<QuotationItemDescription> quotationItemDescription = _quotationItemDescriptionsModel.GetAll();

            ViewData["QuotationItemDescription"] = quotationItemDescription;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: View Project
        public ActionResult ViewProject(int id)
        {
            //Get Main ID first
            int loginMainID = Convert.ToInt32(Session["MainID"]);

            //Get Project Item
            Project project = _projectsModel.GetSingle(id);

            if (project == null)
            {
                Session.Add("Result", "danger|Project not found!");
                return RedirectToAction("Index");
            }

            User getUserRole = _usersModel.GetSingle(loginMainID);

            ViewData["Project"] = project;

            IList<Invoice> invoice = _invoicesModel.getAllProjectPaymentSummary(project.QuotationReferenceId);

            ViewData["Invoice"] = invoice;

            User preparedBy = _usersModel.GetSingle(project.PreparedById);

            ViewData["PreparedBy"] = preparedBy.Name;

            Dropdown[] quotationDDL = QuotationDDL();
            ViewData["QuotationDropdown"] = new SelectList(quotationDDL, "val", "name", project.QuotationReferenceId);

            Dropdown[] customerDDL = CustomerDDL();
            ViewData["CustomerDropdown"] = new SelectList(customerDDL, "val", "name", project.CustomerId);

            Dropdown[] coordinatorDDL = CoordinatorDDL();
            ViewData["CoordinatorDropdown"] = new SelectList(coordinatorDDL, "val", "name", project.CoordinatorId);

            Dropdown[] projectTypeDDL = ProjectTypeDDL();
            ViewData["ProjectTypeDropdown"] = new SelectList(projectTypeDDL, "val", "name", project.ProjectTitle);

            ViewData["PhotosUploadFiles"] = project.PhotoUploads;
            ViewData["FloorPlanUploadsFiles"] = project.FloorPlanUploads;
            ViewData["ThreeDDrawingUploadsFiles"] = project.ThreeDDrawingUploads;
            ViewData["AsBuildDrawingUploadsFiles"] = project.AsBuildDrawingUploads;
            ViewData["SubmissionStageUploadsFiles"] = project.SubmissionStageUploads;
            ViewData["OtherUploadsFiles"] = project.OtherUploads;

            Dropdown[] statusDDL = StatusDDL();
            ViewData["StatusDropdown"] = new SelectList(statusDDL, "val", "name", project.Status);

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: ListingInvoice
        public ActionResult ListingInvoice(int page = 1)
        {
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = "";
            if (Session["SearchKeyword"] != null)
            {
                ViewData["SearchKeyword"] = Session["SearchKeyword"];
            }

            int MainID = Convert.ToInt32(Session["MainID"]);

            IPagedList<Invoice> invoices = _invoicesModel.GetPaged(ViewData["SearchKeyword"].ToString(), "All", new List<int>(), MainID, page, pageSize);
            ViewData["Invoice"] = invoices;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: ListingInvoice
        [HttpPost]
        public ActionResult ListingInvoice(FormCollection form)
        {
            int page = 1;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            ViewData["SearchKeyword"] = form["SearchKeyword"].Trim();
            Session.Add("SearchKeyword", form["SearchKeyword"].Trim());

            int MainID = Convert.ToInt32(Session["MainID"]);

            IPagedList<Invoice> invoices = _invoicesModel.GetPaged(ViewData["SearchKeyword"].ToString(), null, new List<int>(), MainID, page, pageSize);
            ViewData["Invoice"] = invoices;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: ViewInvoice
        public ActionResult ViewInvoice(int id)
        {
            Invoice invoice = _invoicesModel.GetSingle(id);

            if (invoice == null)
            {
                Session.Add("Result", "danger|Invoice record not found!");

                int page = 1;

                if (Session["Page"] != null)
                {
                    page = Convert.ToInt32(Session["Page"]);
                }

                return RedirectToAction("ListingInvoice", new { @page = page });
            }
            else
            {
                if (invoice.CustomerId != Convert.ToInt32(Session["MainID"]))
                {
                    Session.Add("Result", "danger|You cannot view other customer's Invoice!");

                    int page = 1;

                    if (Session["Page"] != null)
                    {
                        page = Convert.ToInt32(Session["Page"]);
                    }

                    return RedirectToAction("ListingInvoice", new { @page = page });
                }
            }

            Quotation quotation = _quotationsModel.GetSingle(invoice.QuotationId);
            Customer customer = _customersModel.GetSingle(invoice.CustomerId);

            List<InvoiceItemDescriptionTable> itemDescriptionsTable = new List<InvoiceItemDescriptionTable>();

            int rowId = 1;

            foreach (InvoiceItemDescription itemDescription in invoice.ItemDescriptions)
            {
                itemDescriptionsTable.Add(new InvoiceItemDescriptionTable()
                {
                    RowId = rowId,
                    Description = itemDescription.Description,
                    PercentageOfQuotation = itemDescription.PercentageOfQuotation,
                    Amount = itemDescription.Amount.ToString("#,##0.00")
                });

                rowId++;
            }

            ViewData["InvoiceItemDescriptionTable"] = itemDescriptionsTable;

            ViewData["QuotationID"] = "";
            ViewData["QuotationCompany"] = "";
            ViewData["CustomerID"] = "";
            ViewData["CustomerAddress"] = "";
            ViewData["CustomerTel"] = "";
            ViewData["CustomerFax"] = "";
            ViewData["TotalQuotationAmount"] = "";

            if (quotation != null)
            {
                if (!string.IsNullOrEmpty(quotation.Customer.CompanyName))
                {
                    ViewData["QuotationID"] = quotation.QuotationID + " - " + quotation.Customer.CompanyName;
                }
                else
                {
                    ViewData["QuotationID"] = quotation.QuotationID + " - " + quotation.Customer.ContactPerson;
                }

                ViewData["QuotationCompany"] = quotation.CompanyName;

                decimal totalQuotationAmount = 0;

                foreach (QuotationItemDescription itemDescription in quotation.ItemDescriptions)
                {
                    totalQuotationAmount += Convert.ToDecimal(itemDescription.QuotationItemAmount);
                }

                ViewData["TotalQuotationAmount"] = totalQuotationAmount.ToString("#,##0.00");
            }

            if (customer != null)
            {
                if (!string.IsNullOrEmpty(customer.CompanyName))
                {
                    ViewData["CustomerID"] = customer.CompanyName;
                }
                else
                {
                    ViewData["CustomerID"] = customer.ContactPerson;
                }

                ViewData["CustomerAddress"] = customer.Address;
                ViewData["CustomerTel"] = customer.Tel;
                ViewData["CustomerFax"] = customer.Fax;
            }

            //if no project, dont show project field
            if (invoice.ProjectId != 0)
            {
                Dropdown[] projectDDL = ProjectDDL(invoice.ProjectId);
                ViewData["ProjectDropdown"] = new SelectList(projectDDL, "val", "name", invoice.ProjectId);
            }

            ViewData["Invoice"] = invoice;
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: GenerateInvoice
        public ActionResult GenerateInvoice(int id)
        {
            Invoice invoice = _invoicesModel.GetSingle(id);
            ViewData["Invoice"] = invoice;

            string footer = Server.MapPath("~/Views/Invoice/InvoicePDFFooter.html");
            string customSwitch = string.Format("--footer-html \"{0}\" " + "--footer-spacing \"0\" ", footer);

            string[] listOfCompanies = _settingsModel.GetCodeValue("LIST_OF_COMPANY").Split(';');

			ViewData["CompanyName"] = "MY RENOBUDDY (SG) PTE LTD";
			ViewData["CompanyInfo"] = "Office / Factory: 1007 Eunos Avenue 7 #01-25 S(409578)|Email: Enquiries@myrenobuddy.com.sg|Contact / Fax: +65 6741 3503 (O) +6741 3060 (F) 201534632H";

			foreach (string company in listOfCompanies)
            {
                string[] comp = company.Split('|');

                if (comp[0] == invoice.Company)
                {
                    ViewData["CompanyName"] = comp[0];
                    ViewData["CompanyInfo"] = comp[2].Replace("\r\n", "|");
                    break;
                }
            }

            ViewData["ProjectTitle"] = null;

            if (invoice.ProjectId != 0)
            {
                Project project = _projectsModel.GetAllSingle(invoice.ProjectId);

                if (project != null)
                {
                    ViewData["ProjectTitle"] = project.ProjectTitle;
                }
            }

            //return View();
            return new Rotativa.ViewAsPdf("GenerateInvoice")
            {
                FileName = invoice.InvoiceID + "-invoice-" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf",
                PageSize = Rotativa.Options.Size.A4,
                CustomSwitches = customSwitch
            };
        }

        //GET: ListingPayout
        public ActionResult ListingPayout(int page = 1)
        {
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            int payToId = Convert.ToInt32(Session["MainID"]);
            string userType = "Customer";

            IPagedList<Payout> payouts = _payoutsModel.GetPaged(payToId, userType, page, pageSize);
            ViewData["Payout"] = payouts;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: GetPayoutCalculation
        public string GetPayoutCalculation(int id)
        {
            PayoutSummary summary = new PayoutSummary();

            Payout payout = _payoutsModel.GetSingle(id);

            if (payout != null)
            {
                decimal totalRevenue = 0;
                decimal totalCommission = 0;

                int userid = Convert.ToInt32(Session["MainID"]);
                string userRole = Session["UserGroup"].ToString();

                if (userRole == "Super Admin" || userRole == "Accounts")
                {
                    userid = payout.PayToId;
                }

                List<int[]> tiers = new List<int[]>();

                foreach (PayoutCalculation payoutCalculation in payout.PayoutCalculations.ToList())
                {
                    //Search Tier number, referring the level of downline
                    int tier = 1;
                    bool isDownline = false;

                    if (payoutCalculation.UserId == userid && payoutCalculation.UserType == "Salesperson")
                    {
                        isDownline = true;
                    }
                    else
                    {
                        tier++;

                        bool searchUpline = true;
                        int uplineId = payoutCalculation.UserId;
                        string uplineType = payoutCalculation.UserType;

                        do
                        {
                            if (uplineType == "Salesperson")
                            {
                                User uplineUser = _usersModel.GetSingle(uplineId);

                                if (uplineUser.UplineId == userid)
                                {
                                    searchUpline = false;
                                    isDownline = true;
                                }
                                else
                                {
                                    if (uplineUser.Role == "Super Admin" || (uplineUser.ID == uplineUser.UplineId))
                                    {
                                        searchUpline = false;
                                    }
                                    else
                                    {
                                        tier++;
                                        uplineId = uplineUser.UplineId;
                                    }
                                }
                            }
                            else
                            {
                                Customer uplineCustomer = _customersModel.GetSingle(uplineId);

                                if (uplineCustomer.UplineId == userid && uplineCustomer.UplineUserType == uplineType)
                                {
                                    searchUpline = false;
                                    isDownline = true;
                                }
                                else
                                {
                                    if (uplineCustomer.ID == uplineCustomer.UplineId)
                                    {
                                        searchUpline = false;
                                    }
                                    else
                                    {
                                        tier++;
                                        uplineId = uplineCustomer.UplineId;
                                        uplineType = uplineCustomer.UplineUserType;
                                    }
                                }
                            }
                        } while (searchUpline);
                    }

                    if (isDownline)
                    {
                        tiers.Add(new int[2] { tier, payoutCalculation.ID });

                        totalRevenue += payoutCalculation.Revenue;
                        totalCommission += payoutCalculation.Commission;
                    }
                    else
                    {
                        payout.PayoutCalculations.Remove(payoutCalculation);
                    }
                }

                ViewData["PayoutCalculation"] = payout.PayoutCalculations.ToList();
                ViewData["PayoutCalculationTier"] = tiers.OrderBy(e => e[0]).ToList();
                ViewData["TotalRevenue"] = totalRevenue.ToString("#,##0.00");
                ViewData["TotalCommission"] = totalCommission.ToString("#,##0.00");

                string payoutSummaryView = "";

                using (var sw = new StringWriter())
                {
                    var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, "GetPayoutCalculation");
                    var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                    viewResult.View.Render(viewContext, sw);
                    viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                    payoutSummaryView = sw.GetStringBuilder().ToString();
                }

                summary.Result = true;
                summary.PayoutSummaryView = payoutSummaryView;
            }
            else
            {
                summary.Result = false;
                summary.ErrorMessage = "Payout record not found!";
            }

            return JsonConvert.SerializeObject(summary);
        }

        //GET: CustomerProfile
        public ActionResult CustomerProfile()
        {
            int uid = Convert.ToInt32(Session["MainID"]);

            Customer getCustomerData = _customersModel.GetSingle(uid);

            ViewData["UserID"] = "";
            ViewData["LoginID"] = "";
            ViewData["Status"] = "";

            if (getCustomerData != null)
            {
                ViewData["UserID"] = getCustomerData.CustomerID;
                ViewData["LoginID"] = getCustomerData.LoginID;
                ViewData["Status"] = getCustomerData.Status;
            }
            else
            {
                Session.Add("Result", "danger|User record not found!");
            }


            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: UserProfile
        [HttpPost]
        public ActionResult CustomerProfile(FormCollection form)
        {
            int uid = Convert.ToInt32(Session["MainID"]);

            Customer getCustomerData = _customersModel.GetSingle(uid);

            if (!string.IsNullOrEmpty(form["Password"]))
            {
                if (!string.IsNullOrEmpty(form["RepeatPassword"]))
                {
                    if (form["Password"].ToString() != form["RepeatPassword"].ToString())
                    {
                        ModelState.AddModelError("Password", "Password does not match Repeat Password!");
                        ModelState.AddModelError("RepeatPassword", "Repeat Password does not match Password!");
                    }
                }
                else
                {
                    ModelState.AddModelError("RepeatPassword", "Repeat Password is required!");
                }
            }

            if (ModelState.IsValid)
            {

                if (!string.IsNullOrEmpty(form["Password"]))
                {
                    getCustomerData.Password = EncryptionHelper.Encrypt(form["Password"].ToString());
                }

                bool result = _customersModel.Update(getCustomerData.ID, getCustomerData);

                if (result)
                {
                    int userTriggering = getCustomerData.ID;
                    string userRole = "Customer";
                    string tableAffected = "Customers";
                    string description = userRole + " Customer [" + getCustomerData.LoginID + "] Updated Profile";
                    bool profile_log = AuditLogHelper.WriteAuditLog(userTriggering, userRole, tableAffected, description);

                    Session.Add("Result", "success|Your Profile has been successfully updated!");
                }
                else
                {
                    Session.Add("Result", "danger|Error while updating Admin Profile!");
                }
            }
            else
            {
                Session.Add("Result", "danger|There is something wrong in the form!");
            }

            ViewData["UserID"] = getCustomerData.CustomerID;
            ViewData["LoginID"] = getCustomerData.LoginID;
            ViewData["Status"] = getCustomerData.Status;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Organisation Chart
        public ActionResult Organisation()
        {
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        // GET: GetChartData
        [WebMethod]
        public string GetChartData()
        {
            int uid = Convert.ToInt32(Session["MainID"]);

            //first to get all the current login customer downline ID and type
            Customer getCurrentCustomerInfo = _customersModel.GetSingle(uid);

            IList<Customer> getAllCustomerDownlineWithType = _customersModel.GetCustomerDownlineWithUserType(uid, "Customer");

            List<Data> chartData = new List<Data>();

            chartData.Add(new Data("C-" + getCurrentCustomerInfo.ID, "", getCurrentCustomerInfo.ContactPerson, "Customer", getCurrentCustomerInfo.CustomerID, "Customer"));

            while (getAllCustomerDownlineWithType.Count > 0)
            {
                foreach (Customer _getAllCustomerDownlineWithType in getAllCustomerDownlineWithType)
                {

                    string UplineType = "U-";

                    if (_getAllCustomerDownlineWithType.UplineUserType == "Customer")
                    {
                        UplineType = "C-";
                    }

                    /* 
                     *  id = _id;
                        parentId = _parentId;
                        Name = _name;
                        title = _title;
                        UserID = _UserID;
                        AgencyLeader = _AgencyLeader;
                     */

                    chartData.Add(new Data("C-" + _getAllCustomerDownlineWithType.ID, UplineType + _getAllCustomerDownlineWithType.UplineId, _getAllCustomerDownlineWithType.ContactPerson, "Customer", _getAllCustomerDownlineWithType.CustomerID, "Customer"));

                    getAllCustomerDownlineWithType = _customersModel.GetCustomerDownlineWithUserType(_getAllCustomerDownlineWithType.ID, _getAllCustomerDownlineWithType.UplineUserType);
                }
            }

            return JsonConvert.SerializeObject(chartData);

        }

        //Organisation Chart Data
        public class Data
        {
            public string id = null;
            public string parentId = null;
            public string Name = "";
            public string title = "";
            public string UserID = "";
            public string AgencyLeader = "";

            public Data(string _id, string _parentId, string _name, string _title, string _UserID, string _AgencyLeader)
            {
                id = _id;
                parentId = _parentId;
                Name = _name;
                title = _title;
                UserID = _UserID;
                AgencyLeader = _AgencyLeader;
            }
        }

        //GET: GetCustomerInfo (View Project)
        public string GetCustomerInfo(int id)
        {
            string result = "";

            Customer getCustomerInfo = _customersModel.GetSingle(id);

            string item = "\"ContactPerson\": \"" + getCustomerInfo.ContactPerson + "\", \"Address\": \"" + getCustomerInfo.Address + "\", \"Tel\": \"" + getCustomerInfo.Tel + "\", \"Fax\": \"" + getCustomerInfo.Fax + "\"";
            result += "[{" + item + "}]";

            return result;
        }

        //GET: GetCoordinatorInfo (View Project)
        public string GetCoordinatorInfo(string type, int id)
        {
            string result = null;
            string item = null;
            //check type U=User, C=Customer

            if (type == "U")
            {
                User getUserInfo = _usersModel.GetSingle(id);

                item = "\"Email\": \"" + getUserInfo.Email + "\", \"Tel\": \"" + getUserInfo.Tel + "\"";
            }
            else
            {
                Customer getCustomerInfo = _customersModel.GetSingle(id);

                item = "\"Email\": \"" + getCustomerInfo.Email + "\", \"Tel\": \"" + getCustomerInfo.Tel + "\"";
            }

            result = "[{" + item + "}]";

            return result;
        }

        //Company Dropdown
        public Dropdown[] CompanyDDL()
        {

            Dropdown[] ddl = new Dropdown[2];

            IList<SystemSetting> settings = _settingsModel.GetAll();

            //COMPANY GST
            SystemSetting CompanyList = settings.Where(e => e.Code == "LIST_OF_COMPANY").FirstOrDefault();
            string[] companyList = CompanyList.Value.Split(';');

            int count = 0;

            foreach (string _companyList in companyList)
            {
                string[] val = _companyList.Split('|');

                ddl[count] = new Dropdown { name = val[0], val = val[0] + "-" + val[1] };
                count++;
            }

            return ddl;
        }

        //Customer Dropdown
        public Dropdown[] CustomerDDL()
        {

            IList<Customer> getAllCustomer = _customersModel.GetAll();

            int count = 0;
            Dropdown[] ddl = new Dropdown[getAllCustomer.Count];

            foreach (Customer _getAllCustomer in getAllCustomer)
            {
                if (!string.IsNullOrEmpty(_getAllCustomer.CompanyName))
                {
                    ddl[count] = new Dropdown { name = _getAllCustomer.ContactPerson + " - " + _getAllCustomer.CompanyName, val = Convert.ToString(_getAllCustomer.ID) };
                }
                else
                {
                    ddl[count] = new Dropdown { name = _getAllCustomer.ContactPerson, val = Convert.ToString(_getAllCustomer.ID) };
                }

                count++;
            }

            return ddl;
        }

        //User Dropdown
        public Dropdown[] UserDDL()
        {

            IList<User> getAllUser = _usersModel.GetAll();
            IList<Customer> getAllCustomer = _customersModel.GetAll();

            int count = 0;
            Dropdown[] ddl = new Dropdown[getAllUser.Count + getAllCustomer.Count];

            foreach (User _getAllUser in getAllUser)
            {
                ddl[count] = new Dropdown { name = _getAllUser.UserID + " - " + _getAllUser.Name, val = "U-" + Convert.ToString(_getAllUser.ID) };
                count++;
            }

            foreach (Customer _getAllCustomer in getAllCustomer)
            {
                ddl[count] = new Dropdown { name = _getAllCustomer.CustomerID + " - " + _getAllCustomer.ContactPerson, val = "C-" + Convert.ToString(_getAllCustomer.ID) };
                count++;
            }

            return ddl;
        }

        //Quotation Dropdown
        public Dropdown[] QuotationDDL()
        {

            IList<Quotation> ApprovedQuotation = _quotationsModel.GetAll().Where(e => e.Status == "Confirmed").ToList();

            Dropdown[] ddl = new Dropdown[ApprovedQuotation.Count];

            int count = 0;

            foreach (Quotation _ApprovedQuotation in ApprovedQuotation)
            {
                ddl[count] = new Dropdown { name = _ApprovedQuotation.QuotationID, val = Convert.ToString(_ApprovedQuotation.ID) };
                count++;
            }

            return ddl;
        }

        //Coordinator Dropdown
        public Dropdown[] CoordinatorDDL()
        {

            IList<User> getAllUser = _usersModel.GetAll();

            int count = 0;
            Dropdown[] ddl = new Dropdown[getAllUser.Count];

            foreach (User _getAllUser in getAllUser)
            {
                ddl[count] = new Dropdown { name = _getAllUser.UserID + " - " + _getAllUser.Name, val = "U-" + Convert.ToString(_getAllUser.ID) };
                count++;
            }

            return ddl;
        }

        //Project Type Dropdown
        public Dropdown[] ProjectTypeDDL()
        {

            Dropdown[] ddl = new Dropdown[2];

            ddl[0] = new Dropdown { name = "Commercial", val = "Commercial" };
            ddl[1] = new Dropdown { name = "Residential", val = "Residential" };

            return ddl;
        }

        //Status Dropdown (Project)
        public Dropdown[] StatusDDL()
        {

            Dropdown[] ddl = new Dropdown[2];

            ddl[0] = new Dropdown { name = "Active", val = "Active" };
            ddl[1] = new Dropdown { name = "Complete", val = "Complete" };

            return ddl;
        }

        //Project Dropdown
        public Dropdown[] ProjectDDL(int pid = 0)
        {

            Dropdown[] ddl = new Dropdown[0];

            Project getProject = _projectsModel.GetAllSingle(pid);

            ddl = new Dropdown[1];

            if (getProject.IsDeleted == "Y")
            {
                ddl[0] = new Dropdown { name = getProject.ProjectID + " - " + getProject.ProjectTitle + " (Deleted)", val = getProject.ProjectID };
            }
            else
            {
                ddl[0] = new Dropdown { name = getProject.ProjectID + " - " + getProject.ProjectTitle, val = getProject.ProjectID };
            }
            
            return ddl;
        }
    }
}