﻿using DataAccess;
using DataAccess.POCO;
using NODC_OMS.Controllers;
using NODC_OMS.Helper;
using NODC_OMS.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NODC_OMS.Controllers
{
    [HandleError]
    [RedirectingAction]
    public class TaskListController : ControllerBase
    {
        private IUserRepository _usersModel;
        private ICustomerRepository _customersModel;
        private IQuotationRepository _quotationsModel;
        private IQuotationItemDescriptionRepository _quotationItemDescriptionsModel;
        private ITaskListLoadAllRepository _taskListsLoadAllModel;
        private IProjectRepository _projectsModel;
        private IProjectProjectExpensesRepository _projectExpensesModel;
        private IPayoutRepository _payoutsModel;

        // GET: TaskList
        public TaskListController()
            : this(new UserRepository(), new CustomerRepository(), new QuotationRepository(), new QuotationItemDescriptionRepository(), new TaskListLoadAllRepository(), new ProjectRepository(), new ProjectProjectExpensesRepository(), new PayoutRepository())
        {

        }

        public TaskListController(IUserRepository usersModel, ICustomerRepository customersModel, IQuotationRepository quotationsModel, IQuotationItemDescriptionRepository quotationItemDescriptionsModel, ITaskListLoadAllRepository taskListsLoadAllModel, IProjectRepository projectsModel, IProjectProjectExpensesRepository projectExpensesModel, IPayoutRepository payoutsModel)
        {
            _usersModel = usersModel;
            _customersModel = customersModel;
            _quotationsModel = quotationsModel;
            _quotationItemDescriptionsModel = quotationItemDescriptionsModel;
            _taskListsLoadAllModel = taskListsLoadAllModel;
            _projectsModel = projectsModel;
            _projectExpensesModel = projectExpensesModel;
            _payoutsModel = payoutsModel;
        }

        // GET: TaskList
        public ActionResult Index()
        {
            return RedirectToAction("Listing");
        }

        //GET: Listing
        public ActionResult Listing(int page = 1)
        {

            int loginMainID = Convert.ToInt32(Session["MainID"]);

            User user = _usersModel.GetSingle(loginMainID);

            //the 4 boxes value
            decimal RealizedProfit = 0; //profit has been paid ( check from project status is completed and get all the amount from quotation )
            decimal PendingPayout = 0; //profit not yet pay ( check from payout status is Unpaid )
            decimal ActiveProjectAmount = 0; //amount has been paid ( check from payout status is Completed )
            decimal ToNextTier = 0; // amount needed to promote to next level 

            //Active Project Amount
            List <Project> getProjectQuotationId = _projectsModel.GetAll().Where(e => e.CoordinatorId == loginMainID && e.Status == "Active" && e.IsDeleted == "N").ToList();

            foreach (Project _getProjectQuotationId in getProjectQuotationId)
            {
                //get quotationID, sum all the quotation item description amount
                List<QuotationItemDescription> getAllItemDescription = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(_getProjectQuotationId.QuotationReferenceId);

                foreach(QuotationItemDescription _getAllItemDescrition in getAllItemDescription)
                {
                    ActiveProjectAmount += _getAllItemDescrition.QuotationItemAmount;
                }
            }

            ViewData["ActiveProjectAmount"] = ActiveProjectAmount.ToString("#,##0.00");

            //Get Realized Profit ( from payout already paid )
            List<Payout> getAllPayout = null;

            if (user.Role == "Sales")
            {
                getAllPayout = _payoutsModel.GetAll(loginMainID, "Salesperson").Where(e => e.Status == "Completed").ToList();
            }
            else if(user.Role == "Customer")
            {
                _payoutsModel.GetAll(loginMainID, "Customer").Where(e => e.Status == "Completed").ToList();
            }

            if(getAllPayout != null)
            {
                foreach (Payout _getAllPayout in getAllPayout)
                {
                    RealizedProfit += _getAllPayout.TotalPayoutAmount;
                }
            }

            ViewData["RealizedProfit"] = RealizedProfit.ToString("#,##0.00");

            //Get Pending Payout ( from payout not yet paid == Unpaid)
            List<Payout> getAllUnpaidPayout = null;

            if(user.Role == "Sales")
            {
                getAllUnpaidPayout = _payoutsModel.GetAll(loginMainID, "Salesperson").Where(e => e.Status == "Unpaid").ToList();
            }
            else if (user.Role == "Customer")
            {
                getAllUnpaidPayout = _payoutsModel.GetAll(loginMainID, "Customer").Where(e => e.Status == "Unpaid").ToList();
            }

            if(getAllUnpaidPayout != null)
            {
                foreach (Payout _getAllUnpaidPayout in getAllUnpaidPayout)
                {
                    PendingPayout += _getAllUnpaidPayout.TotalPayoutAmount;
                }
            }

            ViewData["PendingPayout"] = PendingPayout.ToString("#,##0.00");

            //Get To next tier promotion calculation
            if(user.Role == "Sales")
            {
                string userPosition = user.Position;
                decimal totalCompletedProfit = user.TotalCompletedProfit;

                if(userPosition == "Consultant")
                {
                    if (totalCompletedProfit < 120000)
                    {
                        ToNextTier = 119999 - totalCompletedProfit;
                        //promotion = "Assistant Manager";
                        //hasPromoted = true;
                    }
                    else if (totalCompletedProfit >= 120000 && totalCompletedProfit < 370000)
                    {
                        //promotion = "Manager";
                        //hasPromoted = true;
                        ToNextTier = 370000 - totalCompletedProfit;
                    }
                    else if (totalCompletedProfit >= 370000)
                    {

                        //ToNextTier = 369999 - totalCompletedProfit;
                        //if (managerAboveCount >= 3)
                        //{
                        //    promotion = "Senior Manager";
                        //    hasPromoted = true;
                        //}
                        //else
                        //{
                        //    promotion = "Manager";
                        //    hasPromoted = true;
                        //}
                    }
                }
                else if(userPosition == "Assistant Manager")
                {
                    if (totalCompletedProfit >= 120000 && totalCompletedProfit < 370000)
                    {
                        ToNextTier = 370000 - totalCompletedProfit;
                        //promotion = "Manager";
                        //hasPromoted = true;
                    }
                    else if (totalCompletedProfit >= 370000)
                    {
                        //ToNextTier = 369999 - totalCompletedProfit;
                        //if (managerAboveCount >= 3)
                        //{
                        //    promotion = "Senior Manager";
                        //    hasPromoted = true;
                        //}
                        //else
                        //{
                        //    promotion = "Manager";
                        //    hasPromoted = true;
                        //}
                    }
                }
            }

            ViewData["ToNextTier"] = ToNextTier.ToString("#,##0.00");

            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            Session.Add("Page", page);
            Session.Add("PageSize", pageSize);

            //List All Related Items to New List
            IList<Customer> AllCustomer = _customersModel.GetAllTaskList().Where(e => e.AssignToId == loginMainID).ToList();

            List<TaskListLoadAllRecord> list = new List<TaskListLoadAllRecord>();

            //Loop Customer Information
            foreach (Customer _AllCustomer in AllCustomer)
            {
                int id = _AllCustomer.ID;
                string itemID = _AllCustomer.CustomerID;
                string name = _AllCustomer.ContactPerson;
                string date = String.Format("{0:dd/MM/yyyy}", _AllCustomer.CreatedOn);
                string amount = null;
                string status = _AllCustomer.Status;

                list.Add(new TaskListLoadAllRecord()
                {
                    ID = id,
                    ItemID = itemID,
                    Name = name,
                    Date = date,
                    Amount = amount,
                    Status = status,
                    Type = "Customer"
                });
            }

        //if(user.Role == "Super Admin")
        //{
            //Loop all quotations item
            //get latest quotation, filter out confirmed quotation.
            IList<Quotation> getAllQuotation = _quotationsModel.GetAllLatestQuotationID().Where(e => e.Status != "Confirmed" && e.IsDeleted != "Y").ToList();

            foreach(Quotation _getAllQuotation in getAllQuotation)
            {

                int id = _getAllQuotation.ID;
                string itemID = _getAllQuotation.QuotationID;
                string name = null;

                if (!string.IsNullOrEmpty(_getAllQuotation.CompanyName))
                {
                    name = _getAllQuotation.CompanyName;
                }
                else
                {
                    name = _getAllQuotation.ContactPerson;
                }

                string date = String.Format("{0:dd/MM/yyyy}", _getAllQuotation.QuotationDate);
                decimal TotalAmount = 0;
                string status = _getAllQuotation.Status;

                List<QuotationItemDescription> quotationsAmount = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(id);

                foreach (QuotationItemDescription _quotationsAmount in quotationsAmount)
                {
                    TotalAmount += (decimal)_quotationsAmount.QuotationItemAmount;
                }

                if(_getAllQuotation.Status.Contains("Pending Manager Approval"))
                {

                    string Result = "";

                    int findAgencyID = (int)_getAllQuotation.SalesPersonId;
                    do
                    {
                        User searchAgencyLeader = _usersModel.GetSingle(findAgencyID);

                        if (searchAgencyLeader != null)
                        {
                            if (searchAgencyLeader.IsAgencyLeader != "Yes")
                            {
                                //if not agency leader, find their upline and search again.
                                Result = "No";
                                findAgencyID = searchAgencyLeader.UplineId;
                            }
                            else
                            {
                                //if get agency leader, compare the login user id is same with this agency leader id or not.
                                Result = "Yes";
                                if (searchAgencyLeader.ID == loginMainID)
                                {
                                    list.Add(new TaskListLoadAllRecord()
                                    {
                                        ID = id,
                                        ItemID = itemID,
                                        Name = name,
                                        Date = date,
                                        Amount = Convert.ToString(TotalAmount),
                                        Status = status,
                                        Type = "Quotation"
                                    });
                                }
                            }
                        }
                    } while (Result != "Yes");
                }
                else if (_getAllQuotation.Status.Contains("Pending Customer Confirmation") || _getAllQuotation.Status.Contains("Pending Sales Revision"))
                {
                    User checkUser = _usersModel.GetSingle(user.ID);

                    //if sales person match with login user id, show
                    //if prepared by person match with login user id, show
                    if (_getAllQuotation.SalesPersonId == loginMainID || _getAllQuotation.PreparedById == loginMainID)
                    {
                        list.Add(new TaskListLoadAllRecord()
                        {
                            ID = id,
                            ItemID = itemID,
                            Name = name,
                            Date = date,
                            Amount = Convert.ToString(TotalAmount),
                            Status = status,
                            Type = "Quotation"
                        });
                    }
                }
                else if (_getAllQuotation.Status == "Draft")
                {
                    User checkUser = _usersModel.GetSingle(user.ID);

                    //if prepared by person match with login user id, show
                    if (_getAllQuotation.PreparedById == loginMainID)
                    {
                        list.Add(new TaskListLoadAllRecord()
                        {
                            ID = id,
                            ItemID = itemID,
                            Name = name,
                            Date = date,
                            Amount = Convert.ToString(TotalAmount),
                            Status = status,
                            Type = "Quotation"
                        });
                    }
                }
            }
        //}
        //else
        //{
        //    //List All Related Items to New List (Quotation Pending Sales Revision)
        //    //get downline id
        //    IList<User> getUser = _usersModel.GetAllLatestQuotationID();

        //    foreach (User _getUser in getUser)
        //    {

        //        IList<Quotation> AllQuotation = _quotationsModel.GetAllTaskList(_getUser.ID);

        //        //Loop Quotation Information
        //        foreach (Quotation _AllQuotation in AllQuotation)
        //        {

        //            int id = _AllQuotation.ID;
        //            string itemID = _AllQuotation.QuotationID;
        //            string name = _AllQuotation.ContactPerson;
        //            string date = String.Format("{0:dd/MM/yyyy}", _AllQuotation.QuotationDate);
        //            decimal TotalAmount = 0;
        //            string status = _AllQuotation.Status;

        //            List<QuotationItemDescription> quotationsAmount = _quotationItemDescriptionsModel.getAllRelatedIdGetAllRelatedQuotationId(id);

        //            foreach (QuotationItemDescription _quotationsAmount in quotationsAmount)
        //            {
        //                TotalAmount += (decimal)_quotationsAmount.QuotationItemAmount;
        //            }

        //            if (_AllQuotation.Status.Contains("Pending Manager Approval"))
        //            {

        //                string Result = "";

        //                int findAgencyID = (int)_AllQuotation.SalesPersonId;
        //                do
        //                {
        //                    User searchAgencyLeader = _usersModel.GetSingle(findAgencyID);

        //                    if (searchAgencyLeader != null)
        //                    {
        //                        if (searchAgencyLeader.IsAgencyLeader != "Yes")
        //                        {
        //                            //if not agency leader, find their upline and search again.
        //                            Result = "No";
        //                            findAgencyID = searchAgencyLeader.UplineId;
        //                        }
        //                        else
        //                        {
        //                            //if get agency leader, compare the login user id is same with this agency leader id or not.
        //                            Result = "Yes";
        //                            if (searchAgencyLeader.ID == loginMainID)
        //                            {
        //                                list.Add(new TaskListLoadAllRecord()
        //                                {
        //                                    ID = id,
        //                                    ItemID = itemID,
        //                                    Name = name,
        //                                    Date = date,
        //                                    Amount = Convert.ToString(TotalAmount),
        //                                    Status = status,
        //                                    Type = "Quotation"
        //                                });
        //                            }
        //                        }
        //                    }

        //                } while (Result != "Yes");

        //            }
        //            else if (_AllQuotation.Status.Contains("Pending Customer Confirmation") || _AllQuotation.Status.Contains("Pending Sales Revision"))
        //            {
        //                User checkUser = _usersModel.GetSingle(user.ID);

        //                if (_AllQuotation.SalesPersonId == loginMainID)
        //                {
        //                    list.Add(new TaskListLoadAllRecord()
        //                    {
        //                        ID = id,
        //                        ItemID = itemID,
        //                        Name = name,
        //                        Date = date,
        //                        Amount = Convert.ToString(TotalAmount),
        //                        Status = status,
        //                        Type = "Quotation"
        //                    });
        //                }
        //            }
        //        }
        //    }
        //}

            if (user.Role == "Super Admin")
            {
                //List All Related Items to New List (Project)
                IList<Project> AllProject = _projectsModel.GetAll().Where(e => e.Status != "Completed").ToList();

                foreach (Project _AllProject in AllProject)
                {

                    int id = _AllProject.ID;
                    string itemID = _AllProject.ProjectID;
                    string name = _AllProject.CompanyName;
                    string date = String.Format("{0:dd/MM/yyyy}", _AllProject.Date);
                    decimal TotalAmount = 0;
                    string status = _AllProject.Status;

                    IList<ProjectProjectExpenses> projectExpenses = _projectExpensesModel.GetPersonalAll(_AllProject.ID);

                    foreach (ProjectProjectExpenses _projectExpenses in projectExpenses)
                    {
                        TotalAmount += (decimal)_projectExpenses.Cost;
                    }

                    list.Add(new TaskListLoadAllRecord()
                    {
                        ID = id,
                        ItemID = itemID,
                        Name = name,
                        Date = date,
                        Amount = Convert.ToString(TotalAmount),
                        Status = status,
                        Type = "Project"
                    });
                }
            }

            IPagedList<TaskListLoadAllRecord> loadAll = _taskListsLoadAllModel.GetPaged(list, page, pageSize);
            ViewData["loadAll"] = loadAll;

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Delete
        public ActionResult Delete(int id)
        {
            //Only Super Admin can delete Quotation (included Draft)

            int loginMainID = Convert.ToInt32(Session["MainID"]);

            User user = _usersModel.GetSingle(loginMainID);

            Quotation quotation = _quotationsModel.GetSingle(id);

            //check is draft or not

            if(quotation != null)
            {
                if(quotation.Status == "Draft")
                {

                    if (user.Role != "Super Admin")
                    {
                        if (quotation.PreparedById != loginMainID)
                        {
                            Session.Add("Result", "danger|You are not allow to delete this draft!");
                            return RedirectToAction("Index");
                        }
                    }

                    bool result = _quotationsModel.Delete(quotation.ID);

                    if (result)
                    {

                        int MainID = Convert.ToInt32(Session["MainID"]);
                        string userRole = "User";
                        string tableAffected = "Quotations";
                        string description = userRole + " [" + Session["LoginID"].ToString() + "] Delete Quotation Draft [" + quotation.ID + "]";

                        bool create_log = AuditLogHelper.WriteAuditLog(MainID, userRole, tableAffected, description);

                        Session.Add("Result", "success|Draft successfully deleted!");
                    }
                    else
                    {
                        Session.Add("Result", "danger|Draft record not found!");
                    }
                }
                else
                {
                    //not draft
                    Session.Add("Result", "danger|This quotation is not draft!");
                }
            }
            else
            {
                Session.Add("Result", "danger|Draft record not found!");
            }

            return RedirectToAction("Index");
        }
    }
}