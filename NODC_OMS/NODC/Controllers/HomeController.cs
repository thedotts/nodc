﻿using NODC_OMS.Models;
using DataAccess.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using NODC_OMS.Helper;
using System.Collections.Specialized;

namespace NODC_OMS.Controllers
{
    public class HomeController : Controller
    {

        private ICustomerRepository _customersModel;

        public HomeController()
            : this(new CustomerRepository())
        {

        }

        public HomeController(ICustomerRepository customersModel)
        {
            _customersModel = customersModel;
        }

        // GET: Access
        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        //GET: Login
        public ActionResult Login()
        {
            //if have cookie stored in the client site.
            if (Request.Cookies["NODC_Customer"] != null)
            {

                var value = Request.Cookies["NODC_Customer"];

                Customer customer = _customersModel.GetSingle(Convert.ToInt32(value.Values["UserId"]));

                if (customer != null)
                {//true
                    if (customer.Status == "Active")
                    {

                        Session.Add("MainID", customer.ID);//main ID
                        Session.Add("LoginID", customer.CustomerID);//User ID
                        Session.Add("UserGroup", "Customer"); //UserGroup

                        int user = customer.ID;
                        string userRole = "Customer";
                        string tableAffected = "Login Transactions";
                        string description = userRole + " [" + customer.CustomerID + "] Logged In";
                        bool result = AuditLogHelper.WriteAuditLog(user, userRole, tableAffected, description);

                        return RedirectToAction("Index", "Customers");
                    }
                    else if(customer.Status == "Lead")
                    {
                        Session.Add("Result", "danger|User is not allow to login due to status is lead!");
                    }
                    else
                    {
                        Session.Add("Result", "danger|User suspended!");
                    }
                }
                else
                {
                    Session.Add("Result", "danger|User not found!");
                }
            }

            ViewData["LoginID"] = "";
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(FormCollection form)
        {
            if (string.IsNullOrEmpty(form["LoginID"]))
            {
                ModelState.AddModelError("LoginID", "Login ID is required!");
            }

            if (string.IsNullOrEmpty(form["Password"]))
            {
                ModelState.AddModelError("Password", "Password is required!");
            }

            if (ModelState.IsValid)
            {

                string LoginID = form["LoginID"].ToString();
                string password = form["Password"].ToString();

                Customer customer = _customersModel.FindLoginID(LoginID);

                if (customer != null)
                {
                    password = EncryptionHelper.Encrypt(password);

                    if (customer.Password == password)
                    {
                        if (customer.Status == "Active")
                        {
                            Session.Add("MainID", customer.ID);//main ID
                            Session.Add("LoginID", customer.LoginID);//User ID
                            Session.Add("UserGroup", "Customer");//UserGroup

                            int user = customer.ID;
                            string userRole = "Customer";
                            string tableAffected = "Login Transactions";
                            string description = userRole + " [" + customer.LoginID + "] Logged In";
                            bool result = AuditLogHelper.WriteAuditLog(user, userRole, tableAffected, description);

                            if (!string.IsNullOrEmpty(form["remember"]))
                            {
                                int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["RememberMeTimeOut"]);
                                HttpCookie cookie = new HttpCookie("NODC_Customer");
                                cookie.Values.Add("UserId", customer.ID.ToString());
                                cookie.Expires = DateTime.Now.AddDays(timeout);
                                Response.Cookies.Add(cookie);
                            }

                            return RedirectToAction("Index", "Customers");
                        }
                        else if (customer.Status == "Lead")
                        {
                            Session.Add("Result", "danger|User is not allow to login due to status is lead!");
                        }
                        else
                        {
                            Session.Add("Result", "danger|User suspended!");
                        }
                    }
                    else
                    {
                        Session.Add("Result", "danger|Password incorrect!");
                    }
                }
                else
                {
                    Session.Add("Result", "danger|Customer record not found!");
                }
            }

            ViewData["LoginID"] = form["LoginID"].ToString();
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //GET: Logout
        public ActionResult Logout()
        {

            if (Response.Cookies["NODC_Customer"] != null)
            {
                HttpCookie cookie = Response.Cookies["NODC_Customer"];
                cookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(cookie);
            }

            Session.RemoveAll();

            return RedirectToAction("Login", "Home");
        }

        //GET: Forgot Password
        public ActionResult ForgotPassword()
        {
            ViewData["LoginID"] = "";
            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        // POST: Forget Password
        [HttpPost]
        public ActionResult ForgotPassword(FormCollection form)
        {

            string loginID = form["LoginID"].Trim();

            ViewData["LoginID"] = loginID;

            if (string.IsNullOrEmpty(loginID))
            {
                ModelState.AddModelError("LoginID", "Login ID field is required.");
            }

            if (ModelState.IsValid)
            {
                Customer customerData = _customersModel.FindLoginID(loginID);

                if (customerData != null)
                {

                    if (!string.IsNullOrEmpty(customerData.Email))
                    {
                        // Email
                        string key = EncryptionHelper.GenerateRandomString(8);
                        string token = EncryptionHelper.GenerateHash(key, ConfigurationManager.AppSettings["Salt"].ToString());

                        bool updated = _customersModel.AssignResetPassword(customerData.ID, token);

                        if (updated)
                        {
                            int user = customerData.ID;
                            string userRole = "Customer";
                            string tableAffected = "Customers";

                            string description = userRole + " [" + customerData.LoginID + "] Forgot Password ||"; ;

                            string subject = "Reset Password for Website Administration System";
                            string body = System.IO.File.ReadAllText(Server.MapPath("~/Templates/ForgotPassword.html"));
                            string recipient = customerData.Email;
                            string name = customerData.ContactPerson;
                            ListDictionary replacements = new ListDictionary();
                            replacements.Add("<%Name%>", name);
                            replacements.Add("<%Url%>", Url.Action("ResetPassword", "Home", new { @loginID = loginID, @key = key }, "http"));

                            bool sent = EmailHelper.SendEmail(subject, body, replacements, recipient);

                            if (sent)
                            {
                                Session.Add("Result", "success|An email has been sent to you regarding instructions on how to reset your password.");

                                description = userRole + " [" + customerData.LoginID + "] Forgot Password || Email Sent";
                                bool result = AuditLogHelper.WriteAuditLog(user, userRole, tableAffected, description);

                                return RedirectToAction("Login", "Home");
                            }
                            else
                            {
                                description = userRole + " [" + customerData.LoginID + "] Forgot Password || Email Not Sent";
                                bool result = AuditLogHelper.WriteAuditLog(user, userRole, tableAffected, description);

                                Session.Add("Result", "danger|An error occurred while sending email.");
                            }
                        }
                        else
                        {
                            Session.Add("Result", "danger|An error occurred while generating email.");
                        }
                    }
                    else
                    {
                        Session.Add("Result", "danger|Email not found. Please contact system admin.");
                    }
                }
                else
                {
                    Session.Add("Result", "danger|User record not found.");
                }
            }
            else
            {
                Session.Add("Result", "danger|There are errors in the form!");
            }

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        // GET: Reset Password

        public ActionResult ResetPassword(string loginID = "", string key = "")
        {
            ViewData["LoginID"] = loginID;
            ViewData["key"] = key;

            if (String.IsNullOrEmpty(key))
            {
                Session.Add("Result", "danger|No key provided!");

                return RedirectToAction("ForgotPassword", "Home");
            }
            else
            {
                string token = EncryptionHelper.GenerateHash(key, ConfigurationManager.AppSettings["Salt"].ToString());

                Customer customerData = _customersModel.FindLoginID(loginID);

                if (customerData != null)
                {
                    if (customerData.ResetPasswordToken != token)
                    {
                        Session.Add("Result", "danger|Invalid key provided!");

                        return RedirectToAction("ForgotPassword", "Home");
                    }
                }
                else
                {
                    Session.Add("Result", "danger|User record not found!");

                    return RedirectToAction("ForgotPassword", "Home");
                }
            }

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }

        //POST: ResetPassword

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(FormCollection form)
        {
            string loginID = form["LoginID"].Trim();
            string password = form["Password"].Trim();
            string confirmPassword = form["ConfirmPassword"].Trim();
            string tokenCheck = form["tokenCheck"].Trim();

            ViewData["LoginID"] = loginID;
            ViewData["key"] = tokenCheck;

            if (string.IsNullOrEmpty(loginID))
            {
                ModelState.AddModelError("LoginID", "Login ID is required!");
            }

            if (string.IsNullOrEmpty(password))
            {
                ModelState.AddModelError("Password", "Password is required!");
            }

            if (string.IsNullOrEmpty(confirmPassword))
            {
                ModelState.AddModelError("ConfirmPassword", "Confirm Password is required!");
            }
            else
            {
                if (!string.IsNullOrEmpty(password) && password != confirmPassword)
                {
                    ModelState.AddModelError("Password", "New Password and Confirm Password not match!");
                }
                else
                {
                    password = EncryptionHelper.Encrypt(password);
                }
            }

            if (ModelState.IsValid)
            {
                Customer customerData = _customersModel.FindLoginID(loginID);

                string token = EncryptionHelper.GenerateHash(tokenCheck, ConfigurationManager.AppSettings["Salt"].ToString());

                if (customerData != null)
                {

                    if (!string.IsNullOrEmpty(customerData.ResetPasswordToken))
                    {
                        if (customerData.ResetPasswordToken == token)
                        {
                            customerData.Password = password;
                            customerData.ResetPasswordToken = null;

                            bool updated = _customersModel.Update(customerData.ID, customerData);

                            if (updated)
                            {
                                int user = customerData.ID;
                                string userRole = "Customer";
                                string tableAffected = "Customers";

                                string description = userRole + " [" + customerData.LoginID + "] Reset Password";

                                bool result = AuditLogHelper.WriteAuditLog(user, userRole, tableAffected, description);

                                Session.Add("Result", "success|Password updated! You may login using your new password!");

                                return RedirectToAction("Login", "Home");
                            }
                            else
                            {
                                Session.Add("Result", "danger|An error occurred while updating password!");
                            }
                        }
                        else
                        {
                            Session.Add("Result", "danger|Please enter your reset email address!");
                        }
                    }
                    else
                    {
                        Session.Add("Result", "danger|This email address never request for reset password!");
                    }
                }
                else
                {
                    Session.Add("Result", "danger|User record not found!");
                }
            }
            else
            {
                Session.Add("Result", "danger|There are errors in the form!");
            }

            ViewData["SiteName"] = ConfigurationManager.AppSettings["SiteName"].ToString();
            return View();
        }
    }
}