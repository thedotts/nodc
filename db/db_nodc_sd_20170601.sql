USE [NODC_OMS]
GO
/****** Object:  Table [dbo].[AuditLogs]    Script Date: 2/6/2017 1:56:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditLogs](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IPAddress] [nvarchar](255) NOT NULL,
	[Timestamp] [datetime2](7) NOT NULL,
	[UserTriggeringId] [int] NOT NULL,
	[UserRole] [nvarchar](255) NOT NULL,
	[TableAffected] [nvarchar](255) NOT NULL,
	[Description] [ntext] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerReminders]    Script Date: 2/6/2017 1:56:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerReminders](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[Repeat] [nvarchar](255) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customers]    Script Date: 2/6/2017 1:56:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [nvarchar](255) NOT NULL,
	[LoginID] [nvarchar](255) NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[CompanyName] [nvarchar](255) NULL,
	[Agency] [nvarchar](255) NOT NULL,
	[ContactPerson] [nvarchar](255) NOT NULL,
	[Address] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[Tel] [nvarchar](255) NOT NULL,
	[Fax] [nvarchar](255) NULL,
	[BankName] [nvarchar](255) NULL,
	[BankAccNo] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[UplineUserType] [nvarchar](255) NULL,
	[UplineId] [int] NOT NULL,
	[Position] [nvarchar](255) NOT NULL,
	[AssignToId] [int] NOT NULL,
	[TotalCompletedProfit] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL,
	[ResetPasswordToken] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EarlyPayouts]    Script Date: 2/6/2017 1:56:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EarlyPayouts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EarlyPayoutID] [nvarchar](255) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[PayToId] [int] NOT NULL,
	[UserType] [nvarchar](255) NOT NULL,
	[PayoutDate] [datetime2](7) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[Remarks] [ntext] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoiceItemDescriptions]    Script Date: 2/6/2017 1:56:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceItemDescriptions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceId] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[PercentageOfQuotation] [nvarchar](255) NOT NULL,
	[Amount] [decimal](20, 2) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Invoices]    Script Date: 2/6/2017 1:56:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoices](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceID] [nvarchar](255) NOT NULL,
	[Company] [nvarchar](255) NOT NULL,
	[QuotationId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[ContactPerson] [nvarchar](255) NOT NULL,
	[PreparedById] [int] NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[Remarks] [ntext] NULL,
	[Notes] [ntext] NULL,
	[GST] [nvarchar](255) NOT NULL,
	[PaymentMade] [decimal](18, 2) NULL,
	[SubtotalAmount] [decimal](18, 2) NOT NULL,
	[GSTAmount] [decimal](18, 2) NOT NULL,
	[GrandTotalAmount] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PayoutCalculations]    Script Date: 2/6/2017 1:56:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayoutCalculations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PayoutId] [int] NOT NULL,
	[Tier] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[UserType] [nvarchar](255) NOT NULL,
	[Revenue] [decimal](18, 2) NOT NULL,
	[PayoutPercentage] [decimal](18, 2) NOT NULL,
	[Commission] [decimal](18, 2) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Payouts]    Script Date: 2/6/2017 1:56:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payouts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PayoutID] [nvarchar](255) NOT NULL,
	[PayToId] [int] NOT NULL,
	[UserType] [nvarchar](255) NOT NULL,
	[Period] [datetime2](7) NOT NULL,
	[TotalCommissionAmount] [decimal](18, 2) NOT NULL,
	[TotalRetainedAmount] [decimal](18, 2) NOT NULL,
	[TotalEarlyPayoutAmount] [decimal](18, 2) NOT NULL,
	[TotalPayoutAmount] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProjectEstimators]    Script Date: 2/6/2017 1:56:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProjectEstimators](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NULL,
	[ProjectExpensesDescription] [nvarchar](255) NULL,
	[ProjectExpensesCategoryName] [nvarchar](255) NULL,
	[ProjectExpensesAmount] [decimal](18, 2) NULL,
	[ProjectExpensesProjectedCost] [decimal](18, 2) NULL,
	[ProjectExpensesAdjCost] [decimal](18, 2) NULL,
	[ProjectExpensesProfit] [decimal](18, 2) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProjectProjectExpenses]    Script Date: 2/6/2017 1:56:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProjectProjectExpenses](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[SupplierName] [nvarchar](255) NOT NULL,
	[InvoiceNo] [nvarchar](255) NOT NULL,
	[Date] [datetime] NOT NULL,
	[GST] [nvarchar](255) NOT NULL,
	[Cost] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Projects]    Script Date: 2/6/2017 1:56:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Projects](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [nvarchar](255) NOT NULL,
	[CompanyName] [nvarchar](255) NOT NULL,
	[QuotationReferenceId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[PreparedById] [int] NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[CoordinatorId] [int] NOT NULL,
	[ProjectTitle] [nvarchar](255) NOT NULL,
	[ProjectDescription] [ntext] NULL,
	[ProjectType] [nvarchar](255) NOT NULL,
	[PhotoUploads] [nvarchar](255) NULL,
	[FloorPlanUploads] [nvarchar](255) NULL,
	[ThreeDDrawingUploads] [nvarchar](255) NULL,
	[AsBuildDrawingUploads] [nvarchar](255) NULL,
	[SubmissionStageUploads] [nvarchar](255) NULL,
	[OtherUploads] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CompletedDate] [datetime2](7) NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuotationCoBrokes]    Script Date: 2/6/2017 1:56:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuotationCoBrokes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationId] [int] NOT NULL,
	[QuotationCoBrokeUserType] [nvarchar](255) NULL,
	[QuotationCoBrokeUserId] [int] NULL,
	[QuotationCoBrokePercentOfProfit] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuotationItemDescriptions]    Script Date: 2/6/2017 1:56:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuotationItemDescriptions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationId] [int] NOT NULL,
	[RowId] [int] NOT NULL,
	[CategoryName] [nvarchar](255) NOT NULL,
	[QuotationItem_Description] [nvarchar](255) NOT NULL,
	[QuotationItemQty] [int] NULL,
	[QuotationItemUnit] [nvarchar](255) NULL,
	[QuotationItemRate] [decimal](18, 2) NULL,
	[QuotationItemAmount] [decimal](18, 2) NULL,
	[QuotationItemEstCost] [decimal](18, 2) NULL,
	[QuotationItemEstProfit] [decimal](18, 2) NULL,
	[QuotationItemRemarks] [ntext] NULL,
	[QuotationItemMgrComments] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Quotations]    Script Date: 2/6/2017 1:56:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quotations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationID] [nvarchar](255) NOT NULL,
	[CompanyName] [nvarchar](255) NULL,
	[HasGST] [nvarchar](255) NULL,
	[CustomerId] [int] NULL,
	[ContactPerson] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Tel] [nvarchar](255) NULL,
	[Fax] [nvarchar](255) NULL,
	[PreparedById] [int] NOT NULL,
	[SalesPersonType] [nvarchar](255) NULL,
	[SalesPersonId] [int] NULL,
	[QuotationDate] [datetime2](7) NULL,
	[SalesDocumentUploads] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[TermsAndConditions] [ntext] NULL,
	[Status] [nvarchar](255) NOT NULL,
	[ReviseQuotationTimes] [int] NOT NULL,
	[VariationOrderTimes] [int] NOT NULL,
	[PreviousQuotationId] [int] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SystemSettings]    Script Date: 2/6/2017 1:56:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemSettings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](255) NOT NULL,
	[Value] [ntext] NULL,
	[DataType] [nvarchar](255) NOT NULL,
	[IsRequired] [nvarchar](255) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 2/6/2017 1:56:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [nvarchar](255) NOT NULL,
	[LoginID] [nvarchar](255) NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Address] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[Tel] [nvarchar](255) NOT NULL,
	[Mobile] [nvarchar](255) NULL,
	[BankName] [nvarchar](255) NULL,
	[BankAccNo] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[Reminder] [nvarchar](255) NOT NULL,
	[IsAgencyLeader] [nvarchar](255) NOT NULL,
	[UplineId] [int] NOT NULL,
	[Role] [nvarchar](255) NOT NULL,
	[Position] [nvarchar](255) NULL,
	[PresetValue] [decimal](18, 2) NOT NULL,
	[TotalCompletedProfit] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL,
	[ResetPasswordToken] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[CustomerReminders] ON 

INSERT [dbo].[CustomerReminders] ([ID], [CustomerId], [Description], [Date], [Repeat]) VALUES (1, 2, N'Reminder 1', CAST(0x070000000000BD3C0B AS DateTime2), N'Monthly')
SET IDENTITY_INSERT [dbo].[CustomerReminders] OFF
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (1, N'C1700001', N'tds', N'Rn5sE6W5YkI=', N'The Dott Solutions Pte Ltd', N'Era', N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'elvinong@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'User', 2, N'Referral Tier 1', 16, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07FF83914692BD3C0B AS DateTime2), CAST(0x075E49B2D487CC3C0B AS DateTime2), N'N', N'CSeBOj1cenu47VW1gYnZUw==')
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (2, N'C1700002', N'nodc', N'Rn5sE6W5YkI=', N'NODC Pte Ltd', N'Era', N'Number One Design + Consultancy', N'81 UBI AVENUE 4, #07-06, Singapore 408830', NULL, N'+658029579', N'+658029579', N'Bank of Singapore', N'123456789', NULL, N'User', 2, N'Referral Tier 1', 17, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x073D3086F492BD3C0B AS DateTime2), CAST(0x070FBCA94593BD3C0B AS DateTime2), N'N', NULL)
SET IDENTITY_INSERT [dbo].[Customers] OFF
SET IDENTITY_INSERT [dbo].[ProjectProjectExpenses] ON 

INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (1, 1, N'Colin''s Expenses 1', N'Colins', N'Invoice1', CAST(0x0000A76200000000 AS DateTime), N'Yes', CAST(200.00 AS Decimal(18, 2)), N'Paid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (2, 2, N'Bobby''s Expenses', N'Bobby', N'Invoice2', CAST(0x0000A76200000000 AS DateTime), N'Yes', CAST(300.00 AS Decimal(18, 2)), N'Paid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (3, 3, N'Aaron''s Expenses', N'Aaron', N'Invoice3', CAST(0x0000A76200000000 AS DateTime), N'Yes', CAST(500.00 AS Decimal(18, 2)), N'Paid')
SET IDENTITY_INSERT [dbo].[ProjectProjectExpenses] OFF
SET IDENTITY_INSERT [dbo].[Projects] ON 

INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (1, N'P1700001', N'NODC Pte Ltd', 1, 1, 1, CAST(0x070000000000BD3C0B AS DateTime2), 6, N'Colin''s Project $1,200', N'Total Quotation of $1,200', N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', NULL, CAST(0x07A5B587F297BD3C0B AS DateTime2), CAST(0x07A5B587F297BD3C0B AS DateTime2), N'N')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (2, N'P1700002', N'NODC Pte Ltd', 2, 1, 1, CAST(0x070000000000BD3C0B AS DateTime2), 5, N'Bobby''s Project $1,500', N'Bobby''s Quotation $1,500', N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', NULL, CAST(0x07390F5D2698BD3C0B AS DateTime2), CAST(0x07390F5D2698BD3C0B AS DateTime2), N'N')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (3, N'P1700003', N'NODC Pte Ltd', 3, 2, 1, CAST(0x070000000000BD3C0B AS DateTime2), 3, N'Aaron''s Project $2,000', N'Aaron''s Quotation $2,000', N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', NULL, CAST(0x07D1A8EB5C98BD3C0B AS DateTime2), CAST(0x07D1A8EB5C98BD3C0B AS DateTime2), N'N')
SET IDENTITY_INSERT [dbo].[Projects] OFF
SET IDENTITY_INSERT [dbo].[QuotationCoBrokes] ON 

INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (1, 1, NULL, 6, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (2, 2, NULL, 5, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (3, 3, NULL, 2, 45)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (4, 3, NULL, 3, 10)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (5, 3, NULL, 1, 45)
SET IDENTITY_INSERT [dbo].[QuotationCoBrokes] OFF
SET IDENTITY_INSERT [dbo].[QuotationItemDescriptions] ON 

INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (1, 1, 0, N'Preliminaries', N'Preliminaries', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (2, 1, 0, N'Authority Submission', N'Authority Submission', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (3, 1, 0, N'Partitioning & Ceiling Work', N'Partitioning & Ceiling Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (4, 1, 0, N'Flooring Work', N'Flooring Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (5, 1, 0, N'Painting Work', N'Painting Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (6, 1, 0, N'Mill Work', N'Mill Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (7, 1, 0, N'Glazing Work', N'Glazing Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (8, 1, 0, N'Electrical', N'Electrical', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (9, 1, 0, N'Plumbing & Sanitory', N'Plumbing & Sanitory', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (10, 1, 0, N'ACMV Work', N'ACMV Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (11, 1, 0, N'Fire Protection Work', N'Fire Protection Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (12, 1, 0, N'Miscellaneous', N'Miscellaneous', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (13, 2, 0, N'Preliminaries', N'Preliminaries 1', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (14, 2, 0, N'Preliminaries', N'Preliminaries 2', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (15, 2, 0, N'Authority Submission', N'Authority Submission 1', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (16, 2, 0, N'Authority Submission', N'Authority Submission 2', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (17, 2, 0, N'Partitioning & Ceiling Work', N'Partitioning & Ceiling Work 1', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (18, 2, 0, N'Partitioning & Ceiling Work', N'Partitioning & Ceiling Work 2', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (19, 2, 0, N'Flooring Work', N'Flooring Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (20, 2, 0, N'Painting Work', N'Painting Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (21, 2, 0, N'Mill Work', N'Mill Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (22, 2, 0, N'Glazing Work', N'Glazing Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (23, 2, 0, N'Electrical', N'Electrical', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (24, 2, 0, N'Plumbing & Sanitory', N'Plumbing & Sanitory', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (25, 2, 0, N'ACMV Work', N'ACMV Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (26, 2, 0, N'Fire Protection Work', N'Fire Protection Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (27, 2, 0, N'Miscellaneous', N'Miscellaneous', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (28, 3, 0, N'Preliminaries', N'Preliminaries', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (29, 3, 0, N'Authority Submission', N'Authority Submission', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (30, 3, 0, N'Partitioning & Ceiling Work', N'Partitioning & Ceiling Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (31, 3, 0, N'Flooring Work', N'Flooring Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (32, 3, 0, N'Painting Work', N'Painting Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (33, 3, 0, N'Mill Work', N'Mill Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (34, 3, 0, N'Glazing Work', N'Glazing Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (35, 3, 0, N'Electrical', N'Electrical', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (36, 3, 0, N'Plumbing & Sanitory', N'Plumbing & Sanitory', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (37, 3, 0, N'ACMV Work', N'ACMV Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (38, 3, 0, N'Fire Protection Work', N'Fire Protection Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (39, 3, 0, N'Miscellaneous', N'Miscellaneous', 1, N'lot', CAST(900.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
SET IDENTITY_INSERT [dbo].[QuotationItemDescriptions] OFF
SET IDENTITY_INSERT [dbo].[Quotations] ON 

INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (1, N'Q1700001', N'NODC Pte Ltd', N'Yes', 1, N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'65 9192 9394', N'65 9192 9393', 2, NULL, 6, CAST(0x070000000000BD3C0B AS DateTime2), N'UserGuide_170427174953.pdf', N'Colins''s Quotation on 27/04/2017', NULL, N'Confirmed', 0, 0, 1, CAST(0x079B00561594BD3C0B AS DateTime2), CAST(0x07200BAB7995BD3C0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (2, N'Q1700002', N'NODC Pte Ltd', N'Yes', 2, N'Elvin Ong', N'81 UBI AVENUE 4, #07-06, Singapore 408830', N'+658029579', N'+658029579', 1, NULL, 5, CAST(0x070000000000BD3C0B AS DateTime2), N'UserGuide_170427175744.pdf', N'Bobby''s Quotation on 27/04/2017', NULL, N'Pending Customer Confirmation', 0, 0, 2, CAST(0x0783A9886796BD3C0B AS DateTime2), CAST(0x0757E6209296BD3C0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (3, N'Q1700003', N'NODC Pte Ltd', N'Yes', 2, N'Elvin Ong', N'81 UBI AVENUE 4, #07-06, Singapore 408830', N'+658029579', N'+658029579', 2, NULL, 3, CAST(0x070000000000BD3C0B AS DateTime2), N'UserGuide_170427175854.pdf', N'Aaron''s Quotation on 27/04/2017', NULL, N'Confirmed', 0, 0, 3, CAST(0x07BE52200A97BD3C0B AS DateTime2), CAST(0x071DBC8E2997BD3C0B AS DateTime2), N'N')
SET IDENTITY_INSERT [dbo].[Quotations] OFF
SET IDENTITY_INSERT [dbo].[SystemSettings] ON 

INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (1, N'COMPANY_NAME', N'NODC Pte Ltd', N'string', N'Y', CAST(0x07A7C77AC487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (2, N'COMPANY_ADDRESS', N'123', N'string', N'Y', CAST(0x07F04F8EC487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (3, N'COMPANY_POSTAL_CODE', N'12345', N'string', N'Y', CAST(0x0704ED97C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (4, N'DEFAULT_EMAIL', N'email@gmail.com', N'string', N'Y', CAST(0x07A478A0C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (5, N'MAIN_CONTACT_PERSON', N'lim', N'string', N'N', CAST(0x0797C7A9C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (6, N'MAIN_CONTACT_NO', N'123', N'string', N'N', CAST(0x077AEFB2C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (7, N'PREFIX_INVOICE_ID', N'INV', N'string', N'N', CAST(0x07F82CBBC487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (8, N'PREFIX_QUOTATION_ID', N'Q', N'string', N'N', CAST(0x070359C2C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (9, N'PREFIX_PROJECT_ID', N'P', N'string', N'N', CAST(0x0793BDCAC487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (10, N'PREFIX_CUSTOMER_ID', N'C', N'string', N'N', CAST(0x07E085D2C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (11, N'PREFIX_USER_ID', N'U', N'string', N'N', CAST(0x07FBD8D9C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (12, N'PREFIX_PAYOUT_ID', N'P', N'string', N'N', CAST(0x0759C8E1C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (13, N'PREFIX_EARLY_PAYOUT_ID', N'E', N'string', N'N', CAST(0x07D805EAC487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (14, N'TERM_AND_CONDITION', N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'string', N'N', CAST(0x0725CEF1C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (15, N'PERCENTAGE_HIGHLIGH_RED', N'17', N'string', N'Y', CAST(0x07181DFBC487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (16, N'GST', N'7', N'string', N'Y', CAST(0x0714DD06C587DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (17, N'AGENCY_LIST', N'Propnex|ERA', N'string', N'Y', CAST(0x0782F30EC587DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (18, N'QUOTATION_CATEGORIES', N'ggg 1|asd 2|asdd 3', N'string', N'Y', CAST(0x07D0BB16C587DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (19, N'LIST_OF_COMPANY', N'NODC Pte Ltd|Yes|81 Ubi Avenue 4 UB.One #07-06 Singapore 408830
Tel: +65 6802 9579 Email: admin@kspm.com.sg
Co & GST Reg No: 201500075E;No 1 Design Consultancy Pte Ltd|Yes|81 Ubi Avenue 4 UB.One #07-06 Singapore 408830
Tel: +65 6802 9579 Email: admin@kspm.com.sg
Co & GST Reg No: 201500075E', N'string', N'Y', CAST(0x07C4C524C587DA3C0B AS DateTime2))
SET IDENTITY_INSERT [dbo].[SystemSettings] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (1, N'U1700001', N'hongguan', N'Rn5sE6W5YkI=', N'Lim Hong Guan', N'46 Tagore Lane, 787900 Singapore', N'hongguan@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'Yes', 1, N'Super Admin', NULL, CAST(1000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x070000000000BD3C0B AS DateTime2), CAST(0x07E4A0030E96C23C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (2, N'U1700002', N'erntay2', N'Rn5sE6W5YkI=', N'Stephen Tay Chan Ern', N'46 Tagore Lane, 787900 Singapore', N'chanern@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'Yes', 1, N'Super Admin', NULL, CAST(1000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x074F56267B8DBD3C0B AS DateTime2), CAST(0x074F56267B8DBD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (3, N'U1700003', N'aaron', N'Rn5sE6W5YkI=', N'Aaron', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'aaron@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'No', 1, N'Sales', N'Director', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07A5A9456F91BD3C0B AS DateTime2), CAST(0x07A5A9456F91BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (4, N'U1700004', N'accounts', N'Rn5sE6W5YkI=', N'accounts', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'accounts@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'No', 1, N'Accounts', NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x078573B9EA90BD3C0B AS DateTime2), CAST(0x078573B9EA90BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (5, N'U1700005', N'bobby', N'Rn5sE6W5YkI=', N'Bobby', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'bobby@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'No', 3, N'Sales', N'Senior Manger', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07F077F58A91BD3C0B AS DateTime2), CAST(0x07F077F58A91BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (6, N'U1700006', N'colins', N'Rn5sE6W5YkI=', N'Colins', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'colins@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'No', 5, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x0751A5499D91BD3C0B AS DateTime2), CAST(0x0751A5499D91BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (7, N'U1700007', N'elvin', N'Rn5sE6W5YkI=', N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'elvinong@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'Yes', 1, N'Super Admin', NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07B7F0F9B695C23C0B AS DateTime2), CAST(0x07B7F0F9B695C23C0B AS DateTime2), N'N', NULL)
SET IDENTITY_INSERT [dbo].[Users] OFF
