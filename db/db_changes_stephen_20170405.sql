/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Invoices
	(
	ID int NOT NULL IDENTITY (1, 1),
	InvoiceID nvarchar(255) NULL,
	QuotationId int NULL,
	CustomerId int NULL,
	ContactPerson nvarchar(255) NULL,
	PreparedById int NOT NULL,
	Date datetime2(7) NULL,
	Remarks ntext NULL,
	Notes ntext NULL,
	GST nvarchar(255) NULL,
	PaymentMade decimal(18, 2) NULL,
	SubtotalAmount decimal(18, 2) NOT NULL,
	GSTAmount decimal(18, 2) NOT NULL,
	GrantTotalAmount decimal(18, 2) NOT NULL,
	Status nvarchar(255) NOT NULL,
	CreatedOn datetime2(7) NOT NULL,
	UpdatedOn datetime2(7) NOT NULL,
	IsDeleted nvarchar(255) NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Invoices SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Invoices ON
GO
IF EXISTS(SELECT * FROM dbo.Invoices)
	 EXEC('INSERT INTO dbo.Tmp_Invoices (ID, InvoiceID, QuotationId, CustomerId, ContactPerson, PreparedById, Date, Remarks, Notes, GST, PaymentMade, SubtotalAmount, GSTAmount, GrantTotalAmount, Status, CreatedOn, UpdatedOn, IsDeleted)
		SELECT ID, InvoiceID, QuotationId, CustomerId, ContactPerson, PreparedById, Date, Remarks, Notes, HaveGST, PaymentMade, SubTotal, GSTTotal, GrantTotal, Status, CreatedOn, UpdatedOn, IsDeleted FROM dbo.Invoices WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Invoices OFF
GO
DROP TABLE dbo.Invoices
GO
EXECUTE sp_rename N'dbo.Tmp_Invoices', N'Invoices', 'OBJECT' 
GO
COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_InvoiceItemDescriptions
	(
	ID int NOT NULL IDENTITY (1, 1),
	InvoiceId int NOT NULL,
	Description nvarchar(255) NULL,
	PercentageOfQuotation nvarchar(255) NULL,
	Amount decimal(20, 2) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_InvoiceItemDescriptions SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_InvoiceItemDescriptions ON
GO
IF EXISTS(SELECT * FROM dbo.InvoiceItemDescriptions)
	 EXEC('INSERT INTO dbo.Tmp_InvoiceItemDescriptions (ID, InvoiceId, Description, PercentageOfQuotation, Amount)
		SELECT ID, InvoiceId, Description, CONVERT(nvarchar(255), PercentageQuotation), Amount FROM dbo.InvoiceItemDescriptions WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_InvoiceItemDescriptions OFF
GO
DROP TABLE dbo.InvoiceItemDescriptions
GO
EXECUTE sp_rename N'dbo.Tmp_InvoiceItemDescriptions', N'InvoiceItemDescriptions', 'OBJECT' 
GO
COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Invoices
	(
	ID int NOT NULL IDENTITY (1, 1),
	InvoiceID nvarchar(255) NULL,
	Company nvarchar(255) NULL,
	QuotationId int NULL,
	CustomerId int NULL,
	ContactPerson nvarchar(255) NULL,
	PreparedById int NOT NULL,
	Date datetime2(7) NULL,
	Remarks ntext NULL,
	Notes ntext NULL,
	GST nvarchar(255) NULL,
	PaymentMade decimal(18, 2) NULL,
	SubtotalAmount decimal(18, 2) NOT NULL,
	GSTAmount decimal(18, 2) NOT NULL,
	GrantTotalAmount decimal(18, 2) NOT NULL,
	Status nvarchar(255) NOT NULL,
	CreatedOn datetime2(7) NOT NULL,
	UpdatedOn datetime2(7) NOT NULL,
	IsDeleted nvarchar(255) NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Invoices SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Invoices ON
GO
IF EXISTS(SELECT * FROM dbo.Invoices)
	 EXEC('INSERT INTO dbo.Tmp_Invoices (ID, InvoiceID, QuotationId, CustomerId, ContactPerson, PreparedById, Date, Remarks, Notes, GST, PaymentMade, SubtotalAmount, GSTAmount, GrantTotalAmount, Status, CreatedOn, UpdatedOn, IsDeleted)
		SELECT ID, InvoiceID, QuotationId, CustomerId, ContactPerson, PreparedById, Date, Remarks, Notes, GST, PaymentMade, SubtotalAmount, GSTAmount, GrantTotalAmount, Status, CreatedOn, UpdatedOn, IsDeleted FROM dbo.Invoices WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Invoices OFF
GO
DROP TABLE dbo.Invoices
GO
EXECUTE sp_rename N'dbo.Tmp_Invoices', N'Invoices', 'OBJECT' 
GO
COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Invoices
	(
	ID int NOT NULL IDENTITY (1, 1),
	InvoiceID nvarchar(255) NOT NULL,
	Company nvarchar(255) NOT NULL,
	QuotationId int NOT NULL,
	CustomerId int NOT NULL,
	ContactPerson nvarchar(255) NOT NULL,
	PreparedById int NOT NULL,
	Date datetime2(7) NOT NULL,
	Remarks ntext NULL,
	Notes ntext NULL,
	GST nvarchar(255) NOT NULL,
	PaymentMade decimal(18, 2) NULL,
	SubtotalAmount decimal(18, 2) NOT NULL,
	GSTAmount decimal(18, 2) NOT NULL,
	GrantTotalAmount decimal(18, 2) NOT NULL,
	Status nvarchar(255) NOT NULL,
	CreatedOn datetime2(7) NOT NULL,
	UpdatedOn datetime2(7) NOT NULL,
	IsDeleted nvarchar(255) NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Invoices SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Invoices ON
GO
IF EXISTS(SELECT * FROM dbo.Invoices)
	 EXEC('INSERT INTO dbo.Tmp_Invoices (ID, InvoiceID, Company, QuotationId, CustomerId, ContactPerson, PreparedById, Date, Remarks, Notes, GST, PaymentMade, SubtotalAmount, GSTAmount, GrantTotalAmount, Status, CreatedOn, UpdatedOn, IsDeleted)
		SELECT ID, InvoiceID, Company, QuotationId, CustomerId, ContactPerson, PreparedById, Date, Remarks, Notes, GST, PaymentMade, SubtotalAmount, GSTAmount, GrantTotalAmount, Status, CreatedOn, UpdatedOn, IsDeleted FROM dbo.Invoices WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Invoices OFF
GO
DROP TABLE dbo.Invoices
GO
EXECUTE sp_rename N'dbo.Tmp_Invoices', N'Invoices', 'OBJECT' 
GO
COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_InvoiceItemDescriptions
	(
	ID int NOT NULL IDENTITY (1, 1),
	InvoiceId int NOT NULL,
	Description nvarchar(255) NOT NULL,
	PercentageOfQuotation nvarchar(255) NOT NULL,
	Amount decimal(20, 2) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_InvoiceItemDescriptions SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_InvoiceItemDescriptions ON
GO
IF EXISTS(SELECT * FROM dbo.InvoiceItemDescriptions)
	 EXEC('INSERT INTO dbo.Tmp_InvoiceItemDescriptions (ID, InvoiceId, Description, PercentageOfQuotation, Amount)
		SELECT ID, InvoiceId, Description, PercentageOfQuotation, Amount FROM dbo.InvoiceItemDescriptions WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_InvoiceItemDescriptions OFF
GO
DROP TABLE dbo.InvoiceItemDescriptions
GO
EXECUTE sp_rename N'dbo.Tmp_InvoiceItemDescriptions', N'InvoiceItemDescriptions', 'OBJECT' 
GO
COMMIT