/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.Payouts.CommissionAmount', N'Tmp_TotalCommissionAmount', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Payouts.RetainedAmount', N'Tmp_TotalRetainedAmount_1', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Payouts.PayoutAmount', N'Tmp_TotalPayoutAmount_2', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Payouts.Tmp_TotalCommissionAmount', N'TotalCommissionAmount', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Payouts.Tmp_TotalRetainedAmount_1', N'TotalRetainedAmount', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Payouts.Tmp_TotalPayoutAmount_2', N'TotalPayoutAmount', 'COLUMN' 
GO
ALTER TABLE dbo.Payouts SET (LOCK_ESCALATION = TABLE)
GO
COMMIT