USE [NODC_OMS]
GO
/****** Object:  Table [dbo].[AuditLogs]    Script Date: 26/7/2017 2:37:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditLogs](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IPAddress] [nvarchar](255) NOT NULL,
	[Timestamp] [datetime2](7) NOT NULL,
	[UserTriggeringId] [int] NOT NULL,
	[UserRole] [nvarchar](255) NOT NULL,
	[TableAffected] [nvarchar](255) NOT NULL,
	[Description] [ntext] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerReminders]    Script Date: 26/7/2017 2:37:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerReminders](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[Repeat] [nvarchar](255) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customers]    Script Date: 26/7/2017 2:37:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [nvarchar](255) NOT NULL,
	[LoginID] [nvarchar](255) NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[CompanyName] [nvarchar](255) NULL,
	[Agency] [nvarchar](255) NOT NULL,
	[ContactPerson] [nvarchar](255) NOT NULL,
	[Address] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[Tel] [nvarchar](255) NOT NULL,
	[Fax] [nvarchar](255) NULL,
	[BankName] [nvarchar](255) NULL,
	[BankAccNo] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[UplineUserType] [nvarchar](255) NULL,
	[UplineId] [int] NOT NULL,
	[Position] [nvarchar](255) NOT NULL,
	[AssignToId] [int] NOT NULL,
	[TotalCompletedProfit] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL,
	[ResetPasswordToken] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EarlyPayouts]    Script Date: 26/7/2017 2:37:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EarlyPayouts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EarlyPayoutID] [nvarchar](255) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[PayToId] [int] NOT NULL,
	[UserType] [nvarchar](255) NOT NULL,
	[PayoutDate] [datetime2](7) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[Remarks] [ntext] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoiceItemDescriptions]    Script Date: 26/7/2017 2:37:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceItemDescriptions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceId] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[PercentageOfQuotation] [nvarchar](255) NOT NULL,
	[Amount] [decimal](20, 2) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Invoices]    Script Date: 26/7/2017 2:37:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoices](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceID] [nvarchar](255) NOT NULL,
	[Company] [nvarchar](255) NOT NULL,
	[QuotationId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[ContactPerson] [nvarchar](255) NOT NULL,
	[PreparedById] [int] NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[Remarks] [ntext] NULL,
	[Notes] [ntext] NULL,
	[GST] [nvarchar](255) NOT NULL,
	[PaymentMade] [decimal](18, 2) NULL,
	[SubtotalAmount] [decimal](18, 2) NOT NULL,
	[GSTAmount] [decimal](18, 2) NOT NULL,
	[GrandTotalAmount] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PayoutCalculations]    Script Date: 26/7/2017 2:37:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayoutCalculations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PayoutId] [int] NOT NULL,
	[Tier] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[UserType] [nvarchar](255) NOT NULL,
	[Revenue] [decimal](18, 2) NOT NULL,
	[PayoutPercentage] [decimal](18, 2) NOT NULL,
	[Commission] [decimal](18, 2) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Payouts]    Script Date: 26/7/2017 2:37:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payouts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PayoutID] [nvarchar](255) NOT NULL,
	[PayToId] [int] NOT NULL,
	[UserType] [nvarchar](255) NOT NULL,
	[Period] [datetime2](7) NOT NULL,
	[TotalCommissionAmount] [decimal](18, 2) NOT NULL,
	[TotalRetainedAmount] [decimal](18, 2) NOT NULL,
	[TotalEarlyPayoutAmount] [decimal](18, 2) NOT NULL,
	[TotalPayoutAmount] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProjectEstimators]    Script Date: 26/7/2017 2:37:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProjectEstimators](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NULL,
	[ProjectExpensesDescription] [nvarchar](255) NULL,
	[ProjectExpensesCategoryName] [nvarchar](255) NULL,
	[ProjectExpensesAmount] [decimal](18, 2) NULL,
	[ProjectExpensesProjectedCost] [decimal](18, 2) NULL,
	[ProjectExpensesAdjCost] [decimal](18, 2) NULL,
	[ProjectExpensesProfit] [decimal](18, 2) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProjectProjectExpenses]    Script Date: 26/7/2017 2:37:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProjectProjectExpenses](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[SupplierName] [nvarchar](255) NOT NULL,
	[InvoiceNo] [nvarchar](255) NOT NULL,
	[Date] [datetime] NOT NULL,
	[GST] [nvarchar](255) NOT NULL,
	[Cost] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Projects]    Script Date: 26/7/2017 2:37:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Projects](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [nvarchar](255) NOT NULL,
	[CompanyName] [nvarchar](255) NOT NULL,
	[QuotationReferenceId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[PreparedById] [int] NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[CoordinatorId] [int] NOT NULL,
	[ProjectTitle] [nvarchar](255) NOT NULL,
	[ProjectDescription] [ntext] NULL,
	[ProjectType] [nvarchar](255) NOT NULL,
	[PhotoUploads] [nvarchar](255) NULL,
	[FloorPlanUploads] [nvarchar](255) NULL,
	[ThreeDDrawingUploads] [nvarchar](255) NULL,
	[AsBuildDrawingUploads] [nvarchar](255) NULL,
	[SubmissionStageUploads] [nvarchar](255) NULL,
	[OtherUploads] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CompletedDate] [datetime2](7) NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuotationCoBrokes]    Script Date: 26/7/2017 2:37:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuotationCoBrokes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationId] [int] NOT NULL,
	[QuotationCoBrokeUserType] [nvarchar](255) NULL,
	[QuotationCoBrokeUserId] [int] NULL,
	[QuotationCoBrokePercentOfProfit] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuotationItemDescriptions]    Script Date: 26/7/2017 2:37:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuotationItemDescriptions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationId] [int] NOT NULL,
	[RowId] [int] NOT NULL,
	[CategoryName] [nvarchar](255) NOT NULL,
	[QuotationItem_Description] [nvarchar](255) NOT NULL,
	[QuotationItemQty] [decimal](18, 1) NULL,
	[QuotationItemUnit] [nvarchar](255) NULL,
	[QuotationItemRate] [decimal](18, 2) NULL,
	[QuotationItemAmount] [decimal](18, 2) NULL,
	[QuotationItemEstCost] [decimal](18, 2) NULL,
	[QuotationItemEstProfit] [decimal](18, 2) NULL,
	[QuotationItemRemarks] [ntext] NULL,
	[QuotationItemMgrComments] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Quotations]    Script Date: 26/7/2017 2:37:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quotations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationID] [nvarchar](255) NULL,
	[CompanyName] [nvarchar](255) NULL,
	[HasGST] [nvarchar](255) NULL,
	[CustomerId] [int] NULL,
	[ContactPerson] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Tel] [nvarchar](255) NULL,
	[Fax] [nvarchar](255) NULL,
	[PreparedById] [int] NOT NULL,
	[SalesPersonType] [nvarchar](255) NULL,
	[SalesPersonId] [int] NULL,
	[QuotationDate] [datetime2](7) NULL,
	[SalesDocumentUploads] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[TermsAndConditions] [ntext] NULL,
	[Status] [nvarchar](255) NOT NULL,
	[ReviseQuotationTimes] [int] NOT NULL,
	[VariationOrderTimes] [int] NOT NULL,
	[PreviousQuotationId] [int] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SystemSettings]    Script Date: 26/7/2017 2:37:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemSettings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](255) NOT NULL,
	[Value] [ntext] NULL,
	[DataType] [nvarchar](255) NOT NULL,
	[IsRequired] [nvarchar](255) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 26/7/2017 2:37:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [nvarchar](255) NOT NULL,
	[LoginID] [nvarchar](255) NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Address] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[Tel] [nvarchar](255) NOT NULL,
	[Mobile] [nvarchar](255) NULL,
	[BankName] [nvarchar](255) NULL,
	[BankAccNo] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[Reminder] [nvarchar](255) NOT NULL,
	[IsAgencyLeader] [nvarchar](255) NOT NULL,
	[UplineId] [int] NOT NULL,
	[Role] [nvarchar](255) NOT NULL,
	[Position] [nvarchar](255) NULL,
	[PresetValue] [decimal](18, 2) NOT NULL,
	[TotalCompletedProfit] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL,
	[ResetPasswordToken] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[AuditLogs] ON 

INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (1, N'::1', CAST(0x07DD19176F92E13C0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (2, N'::1', CAST(0x07406BAA9250113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (3, N'::1', CAST(0x0750F9914853113D0B AS DateTime2), 3, N'User', N'Login Transactions', N'Sales - Director [aaron] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (4, N'::1', CAST(0x0724D9B53E54113D0B AS DateTime2), 3, N'User', N'Login Transactions', N'Sales - Director [aaron] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (5, N'::1', CAST(0x07296818FF54113D0B AS DateTime2), 3, N'User', N'Login Transactions', N'Sales - Director [aaron] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (6, N'::1', CAST(0x0797BCC94255113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (7, N'::1', CAST(0x074F0AFB6D55113D0B AS DateTime2), 1, N'Super Admin', N'Users', N'Super Admin [hongguan] Created New User [U1700008]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (8, N'::1', CAST(0x0703F7283C57113D0B AS DateTime2), 3, N'User', N'Login Transactions', N'Sales - Director [aaron] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (9, N'::1', CAST(0x0780A57B4458113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (10, N'::1', CAST(0x074736FD5A59113D0B AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (11, N'::1', CAST(0x073834A6865D113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (12, N'::1', CAST(0x078903624861113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (13, N'::1', CAST(0x07209F34E364113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (14, N'::1', CAST(0x07936EEEC577113D0B AS DateTime2), 4, N'User', N'Login Transactions', N'Accounts [accounts] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (15, N'::1', CAST(0x071DF6D97079113D0B AS DateTime2), 4, N'Accounts', N'Invoices', N'Accounts [accounts] Created New Invoice [INV1700001]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (16, N'::1', CAST(0x07AC90F77079113D0B AS DateTime2), 4, N'Accounts', N'InvoiceItemDescriptions', N'Accounts [accounts] Created New Invoice Item Descriptions [INV1700001]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (17, N'::1', CAST(0x075383FAC979113D0B AS DateTime2), 4, N'Accounts', N'Invoices', N'Accounts [accounts] Created New Invoice [INV1700002]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (18, N'::1', CAST(0x07E65D0CCA79113D0B AS DateTime2), 4, N'Accounts', N'InvoiceItemDescriptions', N'Accounts [accounts] Created New Invoice Item Descriptions [INV1700002]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (19, N'::1', CAST(0x07294E49BE7A113D0B AS DateTime2), 4, N'User', N'Login Transactions', N'Accounts [accounts] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (20, N'::1', CAST(0x07B861A9D57A113D0B AS DateTime2), 4, N'Accounts', N'Invoices', N'Accounts [accounts] Updated Invoice [INV1700001]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (21, N'::1', CAST(0x079CB0E1D57A113D0B AS DateTime2), 4, N'Accounts', N'InvoiceItemDescriptions', N'Accounts [accounts] Deleted Invoice Item Descriptions [INV1700001]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (22, N'::1', CAST(0x07F99FE9D57A113D0B AS DateTime2), 4, N'Accounts', N'InvoiceItemDescriptions', N'Accounts [accounts] Created New Invoice Item Descriptions [INV1700001]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (23, N'::1', CAST(0x0730F715257B113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (24, N'::1', CAST(0x073BC2F9BA7B113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (25, N'::1', CAST(0x07051990C47C113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (26, N'::1', CAST(0x0714DAC7907D113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (27, N'::1', CAST(0x0721E66D377E113D0B AS DateTime2), 1, N'Super Admin', N'Projects', N'Super Admin [hongguan] Edited Project [P1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (28, N'::1', CAST(0x07D56BC4377E113D0B AS DateTime2), 1, N'Super Admin', N'ProjectProjectExpenses', N'Super Admin [hongguan] Created New Project Project Expenses [P1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (29, N'::1', CAST(0x07380236497E113D0B AS DateTime2), 1, N'Super Admin', N'Projects', N'Super Admin [hongguan] Edited Project [P1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (30, N'::1', CAST(0x075EB759497E113D0B AS DateTime2), 1, N'Super Admin', N'ProjectProjectExpenses', N'Super Admin [hongguan] Created New Project Project Expenses [P1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (31, N'::1', CAST(0x078BADFDF37F113D0B AS DateTime2), 1, N'Super Admin', N'Projects', N'Super Admin [hongguan] Edited Project [P1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (32, N'::1', CAST(0x0749382CF47F113D0B AS DateTime2), 1, N'Super Admin', N'ProjectProjectExpenses', N'Super Admin [hongguan] Created New Project Project Expenses [P1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (33, N'::1', CAST(0x07CCF8760180113D0B AS DateTime2), 1, N'Super Admin', N'Projects', N'Super Admin [hongguan] Edited Project [P1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (34, N'::1', CAST(0x073D27C80180113D0B AS DateTime2), 1, N'Super Admin', N'ProjectProjectExpenses', N'Super Admin [hongguan] Created New Project Project Expenses [P1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (35, N'::1', CAST(0x07EF89204080113D0B AS DateTime2), 4, N'User', N'Login Transactions', N'Accounts [accounts] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (36, N'::1', CAST(0x07F5EDF95780113D0B AS DateTime2), 3, N'User', N'Login Transactions', N'Sales - Director [aaron] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (37, N'::1', CAST(0x07A0F5B16880113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (38, N'::1', CAST(0x07C391EF7280113D0B AS DateTime2), 1, N'Super Admin', N'Projects', N'Super Admin [hongguan] Edited Project [P1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (39, N'::1', CAST(0x075B093A7380113D0B AS DateTime2), 1, N'Super Admin', N'ProjectProjectExpenses', N'Super Admin [hongguan] Created New Project Project Expenses [P1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (40, N'::1', CAST(0x07A27CEB7880113D0B AS DateTime2), 3, N'User', N'Login Transactions', N'Sales - Director [aaron] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (41, N'::1', CAST(0x070811E9AC82113D0B AS DateTime2), 3, N'User', N'Login Transactions', N'Sales - Director [aaron] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (42, N'::1', CAST(0x07FC3A260283113D0B AS DateTime2), 3, N'Sales Director', N'Projects', N'Sales Director [aaron] Edited Project [P1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (43, N'::1', CAST(0x07F7BEA83E84113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (44, N'::1', CAST(0x07DA4D582E85113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (45, N'::1', CAST(0x0739278B5485113D0B AS DateTime2), 3, N'User', N'Login Transactions', N'Sales - Director [aaron] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (46, N'::1', CAST(0x077C8E331A87113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (47, N'::1', CAST(0x070ED7983487113D0B AS DateTime2), 3, N'User', N'Login Transactions', N'Sales - Director [aaron] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (48, N'::1', CAST(0x0778EBF16F89113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (49, N'::1', CAST(0x07162E82FF8A113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (50, N'::1', CAST(0x076E65E7808B113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (51, N'::1', CAST(0x075549CCB18B113D0B AS DateTime2), 1, N'Super Admin', N'Customers', N'Super Admin [hongguan] Created New Customers [C1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (52, N'::1', CAST(0x073BB797BD8B113D0B AS DateTime2), 1, N'Super Admin', N'Customers', N'Super Admin [hongguan] Updated Customer [C1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (53, N'::1', CAST(0x07702E14588C113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (54, N'::1', CAST(0x070B380B018D113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (55, N'::1', CAST(0x078761BF7A91113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (56, N'::1', CAST(0x0730C1997493113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (57, N'::1', CAST(0x077E76057594113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (58, N'::1', CAST(0x07473FAD0C95113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (59, N'::1', CAST(0x0760C85DCD95113D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (60, N'::1', CAST(0x07B052B88652123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (61, N'::1', CAST(0x07AF8DE51A53123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (115, N'::1', CAST(0x078A5A12C488123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (116, N'::1', CAST(0x07981211E388123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Updated Quotation [Q1700009-V1]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (117, N'::1', CAST(0x07E29A24E388123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700009-V1]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (118, N'::1', CAST(0x07831747E388123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700009-V1]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (119, N'::1', CAST(0x071DDD8C0189123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotation [Q1700010]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (120, N'::1', CAST(0x07027B9F0189123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700010]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (121, N'::1', CAST(0x075AA5C30189123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700010]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (122, N'::1', CAST(0x0775710D1989123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Updated Quotation [Q1700012]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (123, N'::1', CAST(0x076AB1301989123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700012]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (124, N'::1', CAST(0x07ACAA751989123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700012]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (165, N'::1', CAST(0x07EF068EE64C173D0B AS DateTime2), 5, N'User', N'Login Transactions', N'Sales - Senior Manger [bobby] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (166, N'::1', CAST(0x07A7D285FC4C173D0B AS DateTime2), 5, N'Sales Senior Manger', N'Quotations', N'Sales Senior Manger [bobby] Created New Quotation [Q1700005]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (167, N'::1', CAST(0x07942696FC4C173D0B AS DateTime2), 5, N'Sales Senior Manger', N'QuotationCoBrokes', N'Sales Senior Manger [bobby] Created New Quotation CoBrokes [Q1700005]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (62, N'::1', CAST(0x0763963B9053123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (63, N'::1', CAST(0x0722FF84EB53123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Approved Quotation [Q1700001]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (64, N'::1', CAST(0x0772466CF353123D0B AS DateTime2), 6, N'User', N'Login Transactions', N'Sales - Consultant [colins] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (65, N'::1', CAST(0x079B85A85854123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (66, N'::1', CAST(0x074D1971CD54123D0B AS DateTime2), 2, N'User', N'Login Transactions', N'Super Admin [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (67, N'::1', CAST(0x074245117855123D0B AS DateTime2), 2, N'User', N'Login Transactions', N'Sales - Senior Manger [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (68, N'::1', CAST(0x07D844208757123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (69, N'::1', CAST(0x07979E49A757123D0B AS DateTime2), 2, N'User', N'Login Transactions', N'Sales - Senior Manger [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (70, N'::1', CAST(0x07482F548058123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (71, N'::1', CAST(0x0738EAF2D658123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (72, N'::1', CAST(0x07363F9A3659123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (73, N'::1', CAST(0x0784B38E6F59123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (74, N'::1', CAST(0x070B8A968059123D0B AS DateTime2), 2, N'User', N'Login Transactions', N'Sales - Senior Manger [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (75, N'::1', CAST(0x07F00427E659123D0B AS DateTime2), 2, N'User', N'Login Transactions', N'Sales - Senior Manger [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (76, N'::1', CAST(0x070DDC25825A123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (77, N'::1', CAST(0x070B5391905A123D0B AS DateTime2), 2, N'User', N'Login Transactions', N'Sales - Senior Manger [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (78, N'::1', CAST(0x07003A991D5B123D0B AS DateTime2), 2, N'User', N'Login Transactions', N'Sales - Senior Manger [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (79, N'::1', CAST(0x07294A123F5B123D0B AS DateTime2), 5, N'User', N'Login Transactions', N'Sales - Senior Manger [bobby] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (80, N'::1', CAST(0x0727E4E88278123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (81, N'::1', CAST(0x079BDBB1A378123D0B AS DateTime2), 2, N'User', N'Login Transactions', N'Sales - Senior Manger [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (82, N'::1', CAST(0x073F3908CC78123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (83, N'::1', CAST(0x078FFB418379123D0B AS DateTime2), 2, N'User', N'Login Transactions', N'Sales - Senior Manger [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (84, N'::1', CAST(0x0722A60C8F79123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Sales - Senior Manger [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (85, N'::1', CAST(0x0738AF1F5C7A123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Sales - Senior Manger [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (86, N'::1', CAST(0x079434B9107D123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Sales - Senior Manger [hongguan] Logged In')
GO
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (87, N'::1', CAST(0x0740C8CA397E123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Sales - Senior Manger [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (88, N'::1', CAST(0x07E3CCAABD7E123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Sales - Senior Manger [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (89, N'::1', CAST(0x07C0FC6FBF7F123D0B AS DateTime2), 2, N'User', N'Login Transactions', N'Sales - Senior Manger [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (90, N'::1', CAST(0x07A3E76B4B81123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Sales - Senior Manger [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (91, N'::1', CAST(0x07FA1E23A182123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (92, N'::1', CAST(0x07F4CF79BC82123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotation [Q1700004]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (93, N'::1', CAST(0x0732629BBC82123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700004]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (94, N'::1', CAST(0x076F2AD2BC82123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700004]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (95, N'::1', CAST(0x07F6F05D2685123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotations Draft')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (96, N'::1', CAST(0x074134762685123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [ Draft:5 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (97, N'::1', CAST(0x07CC8E9F2685123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [ Draft:5 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (98, N'::1', CAST(0x072F72F33885123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Updated Quotation [Q1700006]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (99, N'::1', CAST(0x07C33D1F3985123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700006]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (100, N'::1', CAST(0x076C976E3985123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700006]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (101, N'::1', CAST(0x0758B6165485123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotation [Q1700006]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (102, N'::1', CAST(0x0726772B5485123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700006]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (103, N'::1', CAST(0x07693A5B5485123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700006]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (104, N'::1', CAST(0x070AF03FE285123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (105, N'::1', CAST(0x07672B8DED85123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotations Draft')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (106, N'::1', CAST(0x070DE8A3ED85123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [ Draft:7 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (107, N'::1', CAST(0x07130ACCED85123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [ Draft:7 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (108, N'::1', CAST(0x07C260137887123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (109, N'::1', CAST(0x07776DA38887123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotations Draft')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (110, N'::1', CAST(0x07452EB88887123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [ Draft:8 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (111, N'::1', CAST(0x075ABCDB8887123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [ Draft:8 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (112, N'::1', CAST(0x07EDE8DFA987123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotation [Q1700009]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (113, N'::1', CAST(0x07C25FF2A987123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700009]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (114, N'::1', CAST(0x07214014AA87123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700009]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (125, N'::1', CAST(0x07F7A8605289123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (126, N'::1', CAST(0x074FFC798A89123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotation [Q1700012]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (127, N'::1', CAST(0x07144C8C8A89123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700012]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (128, N'::1', CAST(0x07A5A1AE8A89123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700012]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (129, N'::1', CAST(0x07633E26B889123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotation [Q1700001]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (130, N'::1', CAST(0x07739B3BB889123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700001]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (131, N'::1', CAST(0x07BA9E5FB889123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700001]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (132, N'::1', CAST(0x079D03C8C389123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotations Draft')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (133, N'::1', CAST(0x07FE68D9C389123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [ Draft:2 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (134, N'::1', CAST(0x074EDDFFC389123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [ Draft:2 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (135, N'::1', CAST(0x073955AACF89123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotation [Q1700002]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (136, N'::1', CAST(0x0737D0BACF89123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700002]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (137, N'::1', CAST(0x07A892E1CF89123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700002]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (138, N'::1', CAST(0x0710A3BFE689123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Updated Quotation [Q1700004]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (139, N'::1', CAST(0x071E7BE5E689123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700004]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (140, N'::1', CAST(0x07F5D32BE789123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700004]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (141, N'::1', CAST(0x076EE83D038A123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotations Draft')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (142, N'::1', CAST(0x0701C34F038A123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [ Draft:4 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (143, N'::1', CAST(0x07D4B472038A123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [ Draft:4 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (144, N'::1', CAST(0x07115CCCAA8A123D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (145, N'::1', CAST(0x0710E4F4C18A123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotation [Q1700001]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (146, N'::1', CAST(0x0717D007C28A123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700001]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (147, N'::1', CAST(0x07E30B2DC28A123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700001]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (148, N'::1', CAST(0x07C43299CC8A123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotations Draft')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (149, N'::1', CAST(0x07A21AAECC8A123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [ Draft:2 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (150, N'::1', CAST(0x072404D5CC8A123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [ Draft:2 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (151, N'::1', CAST(0x0757AD3DDA8A123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotation [Q1700002]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (152, N'::1', CAST(0x07913580DA8A123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700002]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (153, N'::1', CAST(0x071164A2DA8A123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700002]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (154, N'::1', CAST(0x07E15F69F38A123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Updated Quotation [Q1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (155, N'::1', CAST(0x076E7597F38A123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (156, N'::1', CAST(0x07C881EFF38A123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (157, N'::1', CAST(0x07E77050128B123D0B AS DateTime2), 1, N'Super Admin', N'Projects', N'Super Admin [hongguan] Updated Projects [P1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (158, N'::1', CAST(0x07CB8973128B123D0B AS DateTime2), 1, N'Super Admin', N'Invoices', N'Super Admin [hongguan] Updated Invoices [INV1700001]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (159, N'::1', CAST(0x07141287128B123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Updated Quotation [Q1700002-V1]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (160, N'::1', CAST(0x07128D97128B123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700002-V1]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (161, N'::1', CAST(0x0792BBB9128B123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700002-V1]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (162, N'::1', CAST(0x0729F949218B123D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotation [Q1700004]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (163, N'::1', CAST(0x07CDFA5B218B123D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700004]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (164, N'::1', CAST(0x078FC57E218B123D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700004]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (168, N'::1', CAST(0x075EA7B6FC4C173D0B AS DateTime2), 5, N'Sales Senior Manger', N'QuotationItemDescriptions', N'Sales Senior Manger [bobby] Created New Quotation Item Descriptions [Q1700005]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (169, N'::1', CAST(0x07C99D4C0F4D173D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (170, N'::1', CAST(0x07AC1C8A174D173D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Approved Quotation [Q1700005]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (171, N'::1', CAST(0x07DAC7121F4D173D0B AS DateTime2), 5, N'User', N'Login Transactions', N'Sales - Senior Manger [bobby] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (172, N'::1', CAST(0x07AF679E2F4D173D0B AS DateTime2), 5, N'Sales Senior Manger', N'Quotations', N'Sales Senior Manger [bobby] Approved Quotation [Q1700005]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (173, N'::1', CAST(0x07AA20D28B50173D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (174, N'::1', CAST(0x07D38DD19950173D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotation [Q1700006]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (175, N'::1', CAST(0x079DD8DC9950173D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700006]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (176, N'::1', CAST(0x07E81BF59950173D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700006]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (177, N'::1', CAST(0x07D2365BA550173D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Approved Quotation [Q1700006]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (178, N'::1', CAST(0x074E0E9BAE50173D0B AS DateTime2), 5, N'User', N'Login Transactions', N'Sales - Senior Manger [bobby] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (184, N'::1', CAST(0x07E71B86AB57173D0B AS DateTime2), 3, N'User', N'Login Transactions', N'Sales - Director [aaron] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (185, N'::1', CAST(0x07086F85BD58173D0B AS DateTime2), 3, N'User', N'Login Transactions', N'Sales - Director [aaron] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (186, N'::1', CAST(0x07572DF11859173D0B AS DateTime2), 3, N'Sales Director', N'Quotations', N'Sales Director [aaron] Approved Quotation [Q1700007]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (187, N'::1', CAST(0x0788506A4E59173D0B AS DateTime2), 5, N'User', N'Login Transactions', N'Sales - Senior Manger [bobby] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (188, N'127.0.0.1', CAST(0x07E1AB38C059173D0B AS DateTime2), 5, N'User', N'Login Transactions', N'Sales - Senior Manger [bobby] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (189, N'127.0.0.1', CAST(0x07584F20E159173D0B AS DateTime2), 3, N'User', N'Login Transactions', N'Sales - Director [aaron] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (190, N'127.0.0.1', CAST(0x07C6A4FA665A173D0B AS DateTime2), 5, N'User', N'Login Transactions', N'Sales - Senior Manger [bobby] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (191, N'127.0.0.1', CAST(0x07FD40547D5A173D0B AS DateTime2), 5, N'Sales Senior Manger', N'Quotations', N'Sales Senior Manger [bobby] Approved Quotation [Q1700007]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (194, N'::1', CAST(0x072D4999D762173D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (195, N'::1', CAST(0x07D3CD4FA164173D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (196, N'::1', CAST(0x071FED7AD864173D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotation [Q1700008]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (197, N'::1', CAST(0x07509890D864173D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700008]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (198, N'::1', CAST(0x077D03B2D864173D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700008]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (199, N'::1', CAST(0x079A4BA13465173D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Updated Quotation [Q1700008]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (200, N'::1', CAST(0x073D83C83465173D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700008]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (201, N'::1', CAST(0x079914103565173D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700008]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (202, N'::1', CAST(0x077D66ED4E65173D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (203, N'::1', CAST(0x07B00480A465173D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotations Draft')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (204, N'::1', CAST(0x070AB493A465173D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [ Draft:10 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (205, N'::1', CAST(0x0727F8B4A465173D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [ Draft:10 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (206, N'::1', CAST(0x07B1E0B3D165173D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
GO
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (207, N'::1', CAST(0x077B649AE165173D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotations Draft')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (208, N'::1', CAST(0x07196BB3E165173D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [ Draft:11 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (209, N'::1', CAST(0x07A20AD8E165173D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [ Draft:11 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (210, N'::1', CAST(0x07E8AD3B3266173D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (211, N'::1', CAST(0x07EB14B10667173D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (212, N'::1', CAST(0x071D1A2C7567173D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Edited Quotation [ Draft: 0 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (213, N'::1', CAST(0x07351E597567173D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [ Draft: 0 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (214, N'::1', CAST(0x07E572A17567173D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [ Draft: 0 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (215, N'::1', CAST(0x07B03D5A8E67173D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Updated Quotation [Q1700009]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (216, N'::1', CAST(0x075A2B7F8E67173D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [Q1700009]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (217, N'::1', CAST(0x07956EC68E67173D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [Q1700009]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (218, N'::1', CAST(0x074DB910B467173D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotations Draft')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (219, N'::1', CAST(0x0775F323B467173D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [ Draft:12 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (220, N'::1', CAST(0x07D8CDF39268173D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (221, N'::1', CAST(0x0785AD6CA668173D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotations Draft')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (222, N'::1', CAST(0x07CF3580A668173D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [ Draft:13 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (223, N'::1', CAST(0x07EF2B4CC668173D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotations Draft')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (224, N'::1', CAST(0x0761B85DC668173D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [ Draft:14 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (179, N'::1', CAST(0x072F33CA1E57173D0B AS DateTime2), 5, N'User', N'Login Transactions', N'Sales - Senior Manger [bobby] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (180, N'::1', CAST(0x07BAF3EC3057173D0B AS DateTime2), 5, N'Sales Senior Manger', N'Quotations', N'Sales Senior Manger [bobby] Created New Quotation [Q1700007]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (181, N'::1', CAST(0x07E072FB3057173D0B AS DateTime2), 5, N'Sales Senior Manger', N'QuotationCoBrokes', N'Sales Senior Manger [bobby] Created New Quotation CoBrokes [Q1700007]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (182, N'::1', CAST(0x0771C81D3157173D0B AS DateTime2), 5, N'Sales Senior Manger', N'QuotationItemDescriptions', N'Sales Senior Manger [bobby] Created New Quotation Item Descriptions [Q1700007]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (183, N'::1', CAST(0x07C4D43A4A57173D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (192, N'127.0.0.1', CAST(0x0798DADE165B173D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (193, N'::1', CAST(0x07D9C7528961173D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (225, N'::1', CAST(0x075998665469173D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (226, N'::1', CAST(0x07C52797006B173D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (227, N'::1', CAST(0x07EA8486206B173D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotations Draft')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (228, N'::1', CAST(0x07A0689D206B173D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [ Draft:15 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (229, N'::1', CAST(0x079D6B41516C173D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (230, N'::1', CAST(0x0716504E656C173D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Created New Quotations Draft')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (231, N'::1', CAST(0x073F4566656C173D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [ Draft:16 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (232, N'::1', CAST(0x0767B58E656C173D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [ Draft:16 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (233, N'::1', CAST(0x07D0A005CA79173D0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (234, N'::1', CAST(0x0799097AE779173D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Edited Quotation [ Draft: 0 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (235, N'::1', CAST(0x077EDDA1E779173D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [ Draft: 0 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (236, N'::1', CAST(0x07385EF1E779173D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [ Draft: 0 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (237, N'::1', CAST(0x07F879CCF579173D0B AS DateTime2), 1, N'Super Admin', N'Quotations', N'Super Admin [hongguan] Edited Quotation [ Draft: 0 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (238, N'::1', CAST(0x07F9A0FBF579173D0B AS DateTime2), 1, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [hongguan] Created New Quotation CoBrokes [ Draft: 0 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (239, N'::1', CAST(0x070F564EF679173D0B AS DateTime2), 1, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [hongguan] Created New Quotation Item Descriptions [ Draft: 0 ]')
SET IDENTITY_INSERT [dbo].[AuditLogs] OFF
SET IDENTITY_INSERT [dbo].[CustomerReminders] ON 

INSERT [dbo].[CustomerReminders] ([ID], [CustomerId], [Description], [Date], [Repeat]) VALUES (1, 2, N'Reminder 1', CAST(0x070000000000BD3C0B AS DateTime2), N'Monthly')
SET IDENTITY_INSERT [dbo].[CustomerReminders] OFF
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (1, N'C1700001', N'tds', N'Rn5sE6W5YkI=', N'The Dott Solutions Pte Ltd', N'Era', N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'elvinong@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'User', 4, N'Referral Tier 1', 3, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07FF83914692BD3C0B AS DateTime2), CAST(0x075E49B2D487CC3C0B AS DateTime2), N'N', N'CSeBOj1cenu47VW1gYnZUw==')
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (2, N'C1700002', N'nodc', N'Rn5sE6W5YkI=', N'NODC Pte Ltd', N'Era', N'Number One Design + Consultancy', N'81 UBI AVENUE 4, #07-06, Singapore 408830', NULL, N'+658029579', N'+658029579', N'Bank of Singapore', N'123456789', NULL, N'User', 2, N'Referral Tier 1', 5, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x073D3086F492BD3C0B AS DateTime2), CAST(0x070FBCA94593BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (3, N'C1700003', N'aaron123', N'Rn5sE6W5YkI=', NULL, N'No Agency', N'123', NULL, NULL, N'123', NULL, NULL, NULL, NULL, N'User', 1, N'Referral Tier 1', 2, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07FD54BDB18B113D0B AS DateTime2), CAST(0x07F66E78BD8B113D0B AS DateTime2), N'N', NULL)
SET IDENTITY_INSERT [dbo].[Customers] OFF
SET IDENTITY_INSERT [dbo].[InvoiceItemDescriptions] ON 

INSERT [dbo].[InvoiceItemDescriptions] ([ID], [InvoiceId], [Description], [PercentageOfQuotation], [Amount]) VALUES (3, 1, N'1234', N'5', CAST(100.00 AS Decimal(20, 2)))
INSERT [dbo].[InvoiceItemDescriptions] ([ID], [InvoiceId], [Description], [PercentageOfQuotation], [Amount]) VALUES (2, 2, N'1233', N'100', CAST(1200.00 AS Decimal(20, 2)))
SET IDENTITY_INSERT [dbo].[InvoiceItemDescriptions] OFF
SET IDENTITY_INSERT [dbo].[Invoices] ON 

INSERT [dbo].[Invoices] ([ID], [InvoiceID], [Company], [QuotationId], [CustomerId], [ContactPerson], [PreparedById], [Date], [Remarks], [Notes], [GST], [PaymentMade], [SubtotalAmount], [GSTAmount], [GrandTotalAmount], [Status], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (1, N'INV1700001', N'NODC Pte Ltd', 4, 1, N'Elvin Ong', 4, CAST(0x070000000000113D0B AS DateTime2), NULL, NULL, N'Yes', CAST(107.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(7.00 AS Decimal(18, 2)), CAST(107.00 AS Decimal(18, 2)), N'Completed', CAST(0x07BE15B87079113D0B AS DateTime2), CAST(0x0743DB68128B123D0B AS DateTime2), N'N')
INSERT [dbo].[Invoices] ([ID], [InvoiceID], [Company], [QuotationId], [CustomerId], [ContactPerson], [PreparedById], [Date], [Remarks], [Notes], [GST], [PaymentMade], [SubtotalAmount], [GSTAmount], [GrandTotalAmount], [Status], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (2, N'INV1700002', N'NODC Pte Ltd', 1, 1, N'Elvin Ong', 4, CAST(0x070000000000113D0B AS DateTime2), NULL, NULL, N'Yes', CAST(1284.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(84.00 AS Decimal(18, 2)), CAST(1284.00 AS Decimal(18, 2)), N'Completed', CAST(0x074E52ECC979113D0B AS DateTime2), CAST(0x074E52ECC979113D0B AS DateTime2), N'N')
SET IDENTITY_INSERT [dbo].[Invoices] OFF
SET IDENTITY_INSERT [dbo].[ProjectProjectExpenses] ON 

INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (1, 1, N'Colin''s Expenses 1', N'Colins', N'Invoice1', CAST(0x0000A76200000000 AS DateTime), N'Yes', CAST(200.00 AS Decimal(18, 2)), N'Paid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (2, 2, N'Bobby''s Expenses', N'Bobby', N'Invoice2', CAST(0x0000A76200000000 AS DateTime), N'Yes', CAST(300.00 AS Decimal(18, 2)), N'Paid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (8, 3, N'Aaron''s Expenses 3', N'Aaron', N'Invoice3', CAST(0x0000A76200000000 AS DateTime), N'Yes', CAST(500.00 AS Decimal(18, 2)), N'Paid')
SET IDENTITY_INSERT [dbo].[ProjectProjectExpenses] OFF
SET IDENTITY_INSERT [dbo].[Projects] ON 

INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (1, N'P1700001', N'NODC Pte Ltd', 1, 1, 1, CAST(0x070000000000BD3C0B AS DateTime2), 6, N'Colin''s Project $1,200', N'Total Quotation of $1,200', N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', NULL, CAST(0x07A5B587F297BD3C0B AS DateTime2), CAST(0x07A5B587F297BD3C0B AS DateTime2), N'N')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (2, N'P1700002', N'NODC Pte Ltd', 2, 1, 1, CAST(0x070000000000BD3C0B AS DateTime2), 5, N'Bobby''s Project $1,500', N'Bobby''s Quotation $1,500', N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', NULL, CAST(0x07390F5D2698BD3C0B AS DateTime2), CAST(0x07390F5D2698BD3C0B AS DateTime2), N'N')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (3, N'P1700003', N'NODC Pte Ltd', 4, 1, 1, CAST(0x070000000000BD3C0B AS DateTime2), 3, N'Aaron''s Project $2,000', N'Aaron''s Quotation $2,000', N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(0x07523E1B0283113D0B AS DateTime2), CAST(0x07D1A8EB5C98BD3C0B AS DateTime2), CAST(0x07693348128B123D0B AS DateTime2), N'N')
SET IDENTITY_INSERT [dbo].[Projects] OFF
SET IDENTITY_INSERT [dbo].[QuotationCoBrokes] ON 

INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (1, 1, NULL, 1, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (4, 2, NULL, 1, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (3, 3, NULL, 1, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (5, 4, NULL, 1, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (6, 5, NULL, 1, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (7, 6, NULL, 5, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (8, 7, NULL, 1, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (9, 8, NULL, 5, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (11, 9, NULL, 1, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (12, 10, NULL, 1, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (15, 11, NULL, 1, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (16, 12, NULL, 1, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (17, 13, NULL, 1, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (18, 14, NULL, 1, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (19, 15, NULL, 1, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (22, 16, NULL, 1, 100)
SET IDENTITY_INSERT [dbo].[QuotationCoBrokes] OFF
SET IDENTITY_INSERT [dbo].[QuotationItemDescriptions] ON 

INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (1, 1, 0, N'ggg 1', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (2, 1, 0, N'asd 2', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (3, 1, 0, N'asdd 3', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (10, 2, 0, N'ggg 1', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (11, 2, 0, N'asd 2', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (12, 2, 0, N'asdd 3', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (7, 3, 0, N'ggg 1', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (8, 3, 0, N'asd 2', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (9, 3, 0, N'asdd 3', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (13, 4, 0, N'ggg 1', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (14, 4, 0, N'asd 2', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (15, 4, 0, N'asdd 3', N'12', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (16, 5, 0, N'ggg 1', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (17, 5, 0, N'asd 2', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (18, 5, 0, N'asdd 3', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (19, 6, 0, N'ggg 1', N'123', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (20, 6, 0, N'asd 2', N'123', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (21, 6, 0, N'asdd 3', N'123', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (22, 7, 0, N'ggg 1', N'123', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (23, 7, 0, N'asd 2', N'123', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (24, 7, 0, N'asdd 3', N'123', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (25, 8, 0, N'ggg 1', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (26, 8, 0, N'asd 2', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (27, 8, 0, N'asdd 3', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (31, 9, 0, N'ggg 1', N'1', CAST(3.2 AS Decimal(18, 1)), N'lot', CAST(2.00 AS Decimal(18, 2)), CAST(6.40 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(5.40 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (32, 9, 0, N'asd 2', N'1', CAST(4.3 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(4.30 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(3.30 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (33, 9, 0, N'asdd 3', N'1', CAST(5.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (34, 10, 0, N'ggg 1', N'', CAST(0.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (35, 10, 0, N'asd 2', N'', CAST(0.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (36, 10, 0, N'asdd 3', N'', CAST(0.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (43, 11, 0, N'ggg 1', N'1', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(3.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(3.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (44, 11, 0, N'asd 2', N'1', CAST(4.4 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(4.40 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(4.40 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (45, 11, 0, N'asdd 3', N'1', CAST(4.5 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(4.50 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(4.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (52, 16, 1, N'ggg 1', N'', CAST(0.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (53, 16, 2, N'asd 2', N'', CAST(0.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (54, 16, 3, N'asdd 3', N'', CAST(0.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
SET IDENTITY_INSERT [dbo].[QuotationItemDescriptions] OFF
SET IDENTITY_INSERT [dbo].[Quotations] ON 

INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (1, N'Q1700001', N'NODC Pte Ltd', N'Yes', 1, N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'65 9192 9394', N'65 9192 9393', 1, NULL, 1, CAST(0x070000000000123D0B AS DateTime2), NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 1, CAST(0x07E533D8C18A123D0B AS DateTime2), CAST(0x070D6EEBC18A123D0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (2, N'Q1700003', N'No 1 Design Consultancy Pte Ltd', NULL, 1, N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'65 9192 9394', N'65 9192 9393', 1, NULL, 1, CAST(0x070000000000123D0B AS DateTime2), NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 2, CAST(0x07375380CC8A123D0B AS DateTime2), CAST(0x07105F60F38A123D0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (3, N'Q1700002', N'NODC Pte Ltd', N'Yes', 1, N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'65 9192 9394', N'65 9192 9393', 1, NULL, 1, CAST(0x070000000000123D0B AS DateTime2), NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Confirmed', 0, 0, 3, CAST(0x07FC4225DA8A123D0B AS DateTime2), CAST(0x0753387E128B123D0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (4, N'Q1700002-V1', N'NODC Pte Ltd', N'Yes', 1, N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'65 9192 9394', N'65 9192 9393', 1, NULL, 1, CAST(0x070000000000123D0B AS DateTime2), NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Confirmed', 0, 1, 3, CAST(0x075ED12B128B123D0B AS DateTime2), CAST(0x075ED12B128B123D0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (5, N'Q1700004', N'NODC Pte Ltd', N'Yes', 1, N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'65 9192 9394', N'65 9192 9393', 1, NULL, 1, CAST(0x070000000000123D0B AS DateTime2), NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 5, CAST(0x07333437218B123D0B AS DateTime2), CAST(0x0757F840218B123D0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (6, N'Q1700005', N'NODC Pte Ltd', N'Yes', 2, N'Number One Design + Consultancy', N'81 UBI AVENUE 4, #07-06, Singapore 408830', N'+658029579', N'+658029579', 5, NULL, 5, CAST(0x070000000000173D0B AS DateTime2), N'Capture_170726091227.png', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Confirmed', 0, 0, 6, CAST(0x07A89C70FC4C173D0B AS DateTime2), CAST(0x07990F902F4D173D0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (7, N'Q1700006', N'NODC Pte Ltd', N'Yes', 1, N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'65 9192 9394', N'65 9192 9393', 1, NULL, 5, CAST(0x070000000000173D0B AS DateTime2), NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Customer Confirmation ( Bobby )', 0, 0, 7, CAST(0x07B67FC59950173D0B AS DateTime2), CAST(0x072BF554A550173D0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (8, N'Q1700007', N'NODC Pte Ltd', N'Yes', 2, N'Number One Design + Consultancy', N'81 UBI AVENUE 4, #07-06, Singapore 408830', N'+658029579', N'+658029579', 5, NULL, 5, CAST(0x070000000000173D0B AS DateTime2), N'Capture_170726104740.png', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Confirmed', 0, 0, 8, CAST(0x0751D8DD3057173D0B AS DateTime2), CAST(0x075D7F367D5A173D0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (9, N'Q1700008', N'NODC Pte Ltd', N'Yes', 2, N'Number One Design + Consultancy', N'81 UBI AVENUE 4, #07-06, Singapore 408830', N'+658029579', N'+658029579', 1, NULL, 1, CAST(0x070000000000173D0B AS DateTime2), NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 9, CAST(0x07C38262D864173D0B AS DateTime2), CAST(0x07B723983465173D0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Draft', 0, 0, 10, CAST(0x0741FD5DA465173D0B AS DateTime2), CAST(0x07EF2A77A465173D0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (11, N'Q1700009', N'NODC Pte Ltd', NULL, 1, N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'65 9192 9394', N'65 9192 9393', 1, NULL, 1, CAST(0x070000000000173D0B AS DateTime2), NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 11, CAST(0x07131376E165173D0B AS DateTime2), CAST(0x07CD15518E67173D0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Draft', 0, 0, 12, CAST(0x072704EDB367173D0B AS DateTime2), CAST(0x07B4E305B467173D0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Draft', 0, 0, 13, CAST(0x075AFD4FA668173D0B AS DateTime2), CAST(0x07EDD761A668173D0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Draft', 0, 0, 14, CAST(0x07163F30C668173D0B AS DateTime2), CAST(0x07FCDC42C668173D0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Draft', 0, 0, 15, CAST(0x074C7E6D206B173D0B AS DateTime2), CAST(0x0729AB7D206B173D0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Draft', 0, 0, 16, CAST(0x076D1D2E656C173D0B AS DateTime2), CAST(0x07A240C2F579173D0B AS DateTime2), N'N')
SET IDENTITY_INSERT [dbo].[Quotations] OFF
SET IDENTITY_INSERT [dbo].[SystemSettings] ON 

INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (1, N'COMPANY_NAME', N'NODC Pte Ltd', N'string', N'Y', CAST(0x07A7C77AC487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (2, N'COMPANY_ADDRESS', N'123', N'string', N'Y', CAST(0x07F04F8EC487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (3, N'COMPANY_POSTAL_CODE', N'12345', N'string', N'Y', CAST(0x0704ED97C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (4, N'DEFAULT_EMAIL', N'email@gmail.com', N'string', N'Y', CAST(0x07A478A0C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (5, N'MAIN_CONTACT_PERSON', N'lim', N'string', N'N', CAST(0x0797C7A9C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (6, N'MAIN_CONTACT_NO', N'123', N'string', N'N', CAST(0x077AEFB2C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (7, N'PREFIX_INVOICE_ID', N'INV', N'string', N'N', CAST(0x07F82CBBC487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (8, N'PREFIX_QUOTATION_ID', N'Q', N'string', N'N', CAST(0x070359C2C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (9, N'PREFIX_PROJECT_ID', N'P', N'string', N'N', CAST(0x0793BDCAC487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (10, N'PREFIX_CUSTOMER_ID', N'C', N'string', N'N', CAST(0x07E085D2C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (11, N'PREFIX_USER_ID', N'U', N'string', N'N', CAST(0x07FBD8D9C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (12, N'PREFIX_PAYOUT_ID', N'P', N'string', N'N', CAST(0x0759C8E1C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (13, N'PREFIX_EARLY_PAYOUT_ID', N'E', N'string', N'N', CAST(0x07D805EAC487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (14, N'TERM_AND_CONDITION', N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'string', N'N', CAST(0x0725CEF1C487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (15, N'PERCENTAGE_HIGHLIGH_RED', N'17', N'string', N'Y', CAST(0x07181DFBC487DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (16, N'GST', N'7', N'string', N'Y', CAST(0x0714DD06C587DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (17, N'AGENCY_LIST', N'Propnex|ERA', N'string', N'Y', CAST(0x0782F30EC587DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (18, N'QUOTATION_CATEGORIES', N'ggg 1|asd 2|asdd 3', N'string', N'Y', CAST(0x07D0BB16C587DA3C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (19, N'LIST_OF_COMPANY', N'NODC Pte Ltd|Yes|81 Ubi Avenue 4 UB.One #07-06 Singapore 408830
Tel: +65 6802 9579 Email: admin@kspm.com.sg
Co & GST Reg No: 201500075E;No 1 Design Consultancy Pte Ltd|Yes|81 Ubi Avenue 4 UB.One #07-06 Singapore 408830
Tel: +65 6802 9579 Email: admin@kspm.com.sg
Co & GST Reg No: 201500075E', N'string', N'Y', CAST(0x07C4C524C587DA3C0B AS DateTime2))
SET IDENTITY_INSERT [dbo].[SystemSettings] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (1, N'U1700001', N'hongguan', N'Rn5sE6W5YkI=', N'Lim Hong Guan', N'46 Tagore Lane, 787900 Singapore', N'hongguan@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'Yes', 1, N'Super Admin', NULL, CAST(1000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x070000000000BD3C0B AS DateTime2), CAST(0x07E4A0030E96C23C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (2, N'U1700002', N'erntay2', N'Rn5sE6W5YkI=', N'Stephen Tay Chan Ern', N'46 Tagore Lane, 787900 Singapore', N'chanern@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'Yes', 2, N'Super Admin', NULL, CAST(1000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x074F56267B8DBD3C0B AS DateTime2), CAST(0x074F56267B8DBD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (3, N'U1700016', N'aaron', N'Rn5sE6W5YkI=', N'Aaron', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'aaron@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'Yes', 1, N'Sales', N'Director', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07A5A9456F91BD3C0B AS DateTime2), CAST(0x07A5A9456F91BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (4, N'U1700014', N'accounts', N'Rn5sE6W5YkI=', N'accounts', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'accounts@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'No', 1, N'Accounts', NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x078573B9EA90BD3C0B AS DateTime2), CAST(0x078573B9EA90BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (5, N'U1700017', N'bobby', N'Rn5sE6W5YkI=', N'Bobby', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'bobby@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'No', 3, N'Sales', N'Senior Manger', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07F077F58A91BD3C0B AS DateTime2), CAST(0x07F077F58A91BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (6, N'U1700018', N'colins', N'Rn5sE6W5YkI=', N'Colins', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'colins@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'No', 1, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x0751A5499D91BD3C0B AS DateTime2), CAST(0x0751A5499D91BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (7, N'U1700019', N'elvin', N'Rn5sE6W5YkI=', N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'elvinong@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'Yes', 1, N'Super Admin', NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07B7F0F9B695C23C0B AS DateTime2), CAST(0x07B7F0F9B695C23C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (8, N'U1700008', N'test1', N'B93bOdmo2WiO5bOQwE7g1g==', N'test1', NULL, N'test1@gmail.com', N'123', NULL, NULL, NULL, NULL, N'No', N'No', 1, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x078EFADC6D55113D0B AS DateTime2), CAST(0x078EFADC6D55113D0B AS DateTime2), N'N', NULL)
SET IDENTITY_INSERT [dbo].[Users] OFF
