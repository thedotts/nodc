USE [NODC_OMS]
GO
/****** Object:  Table [dbo].[AuditLogs]    Script Date: 09/08/2017 18:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditLogs](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IPAddress] [nvarchar](255) NOT NULL,
	[Timestamp] [datetime2](7) NOT NULL,
	[UserTriggeringId] [int] NOT NULL,
	[UserRole] [nvarchar](255) NOT NULL,
	[TableAffected] [nvarchar](255) NOT NULL,
	[Description] [ntext] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerReminders]    Script Date: 09/08/2017 18:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerReminders](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[Repeat] [nvarchar](255) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customers]    Script Date: 09/08/2017 18:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [nvarchar](255) NOT NULL,
	[LoginID] [nvarchar](255) NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[CompanyName] [nvarchar](255) NULL,
	[Agency] [nvarchar](255) NOT NULL,
	[ContactPerson] [nvarchar](255) NOT NULL,
	[Address] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[Tel] [nvarchar](255) NOT NULL,
	[Fax] [nvarchar](255) NULL,
	[BankName] [nvarchar](255) NULL,
	[BankAccNo] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[UplineUserType] [nvarchar](255) NULL,
	[UplineId] [int] NOT NULL,
	[Position] [nvarchar](255) NOT NULL,
	[AssignToId] [int] NOT NULL,
	[TotalCompletedProfit] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL,
	[ResetPasswordToken] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EarlyPayouts]    Script Date: 09/08/2017 18:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EarlyPayouts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EarlyPayoutID] [nvarchar](255) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[PayToId] [int] NOT NULL,
	[UserType] [nvarchar](255) NOT NULL,
	[PayoutDate] [datetime2](7) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[Remarks] [ntext] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoiceItemDescriptions]    Script Date: 09/08/2017 18:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceItemDescriptions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceId] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[PercentageOfQuotation] [nvarchar](255) NOT NULL,
	[Amount] [decimal](20, 2) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Invoices]    Script Date: 09/08/2017 18:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoices](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceID] [nvarchar](255) NOT NULL,
	[Company] [nvarchar](255) NOT NULL,
	[QuotationId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[ContactPerson] [nvarchar](255) NOT NULL,
	[PreparedById] [int] NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[Remarks] [ntext] NULL,
	[Notes] [ntext] NULL,
	[GST] [nvarchar](255) NOT NULL,
	[PaymentMade] [decimal](18, 2) NULL,
	[SubtotalAmount] [decimal](18, 2) NOT NULL,
	[GSTAmount] [decimal](18, 2) NOT NULL,
	[GrandTotalAmount] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PayoutCalculations]    Script Date: 09/08/2017 18:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayoutCalculations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PayoutId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[UserType] [nvarchar](255) NOT NULL,
	[Revenue] [decimal](18, 2) NOT NULL,
	[PayoutPercentage] [decimal](18, 2) NOT NULL,
	[Commission] [decimal](18, 2) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Payouts]    Script Date: 09/08/2017 18:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payouts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PayoutID] [nvarchar](255) NOT NULL,
	[PayToId] [int] NOT NULL,
	[UserType] [nvarchar](255) NOT NULL,
	[Period] [datetime2](7) NOT NULL,
	[TotalCommissionAmount] [decimal](18, 2) NOT NULL,
	[TotalRetainedAmount] [decimal](18, 2) NOT NULL,
	[TotalEarlyPayoutAmount] [decimal](18, 2) NOT NULL,
	[TotalPayoutAmount] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProjectEstimators]    Script Date: 09/08/2017 18:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProjectEstimators](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NULL,
	[ProjectExpensesDescription] [nvarchar](255) NULL,
	[ProjectExpensesCategoryName] [nvarchar](255) NULL,
	[ProjectExpensesAmount] [decimal](18, 2) NULL,
	[ProjectExpensesProjectedCost] [decimal](18, 2) NULL,
	[ProjectExpensesAdjCost] [decimal](18, 2) NULL,
	[ProjectExpensesProfit] [decimal](18, 2) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProjectProjectExpenses]    Script Date: 09/08/2017 18:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProjectProjectExpenses](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[SupplierName] [nvarchar](255) NOT NULL,
	[InvoiceNo] [nvarchar](255) NOT NULL,
	[Date] [datetime] NOT NULL,
	[GST] [nvarchar](255) NOT NULL,
	[Cost] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Projects]    Script Date: 09/08/2017 18:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Projects](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [nvarchar](255) NOT NULL,
	[CompanyName] [nvarchar](255) NOT NULL,
	[QuotationReferenceId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[PreparedById] [int] NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[CoordinatorId] [int] NOT NULL,
	[ProjectTitle] [nvarchar](255) NOT NULL,
	[ProjectDescription] [ntext] NULL,
	[ProjectType] [nvarchar](255) NOT NULL,
	[PhotoUploads] [nvarchar](255) NULL,
	[FloorPlanUploads] [nvarchar](255) NULL,
	[ThreeDDrawingUploads] [nvarchar](255) NULL,
	[AsBuildDrawingUploads] [nvarchar](255) NULL,
	[SubmissionStageUploads] [nvarchar](255) NULL,
	[OtherUploads] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CompletedDate] [datetime2](7) NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuotationCoBrokes]    Script Date: 09/08/2017 18:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuotationCoBrokes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationId] [int] NOT NULL,
	[QuotationCoBrokeUserType] [nvarchar](255) NULL,
	[QuotationCoBrokeUserId] [int] NULL,
	[QuotationCoBrokePercentOfProfit] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuotationItemDescriptions]    Script Date: 09/08/2017 18:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuotationItemDescriptions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationId] [int] NOT NULL,
	[RowId] [int] NOT NULL,
	[CategoryName] [nvarchar](255) NOT NULL,
	[QuotationItem_Description] [nvarchar](255) NOT NULL,
	[QuotationItemQty] [decimal](18, 1) NULL,
	[QuotationItemUnit] [nvarchar](255) NULL,
	[QuotationItemRate] [decimal](18, 2) NULL,
	[QuotationItemAmount] [decimal](18, 2) NULL,
	[QuotationItemEstCost] [decimal](18, 2) NULL,
	[QuotationItemEstProfit] [decimal](18, 2) NULL,
	[QuotationItemRemarks] [ntext] NULL,
	[QuotationItemMgrComments] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Quotations]    Script Date: 09/08/2017 18:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quotations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationID] [nvarchar](255) NULL,
	[CompanyName] [nvarchar](255) NULL,
	[HasGST] [nvarchar](255) NULL,
	[CustomerId] [int] NULL,
	[ContactPerson] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Tel] [nvarchar](255) NULL,
	[Fax] [nvarchar](255) NULL,
	[PreparedById] [int] NOT NULL,
	[SalesPersonType] [nvarchar](255) NULL,
	[SalesPersonId] [int] NULL,
	[QuotationDate] [datetime2](7) NULL,
	[SalesDocumentUploads] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[TermsAndConditions] [ntext] NULL,
	[Status] [nvarchar](255) NOT NULL,
	[ReviseQuotationTimes] [int] NOT NULL,
	[VariationOrderTimes] [int] NOT NULL,
	[PreviousQuotationId] [int] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SystemSettings]    Script Date: 09/08/2017 18:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemSettings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](255) NOT NULL,
	[Value] [ntext] NULL,
	[DataType] [nvarchar](255) NOT NULL,
	[IsRequired] [nvarchar](255) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 09/08/2017 18:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [nvarchar](255) NOT NULL,
	[LoginID] [nvarchar](255) NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Address] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[Tel] [nvarchar](255) NOT NULL,
	[Mobile] [nvarchar](255) NULL,
	[BankName] [nvarchar](255) NULL,
	[BankAccNo] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[Reminder] [nvarchar](255) NOT NULL,
	[IsAgencyLeader] [nvarchar](255) NOT NULL,
	[UplineId] [int] NOT NULL,
	[Role] [nvarchar](255) NOT NULL,
	[Position] [nvarchar](255) NULL,
	[PresetValue] [decimal](18, 2) NOT NULL,
	[TotalCompletedProfit] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL,
	[ResetPasswordToken] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[AuditLogs] ON 

INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (1, N'60.50.28.177', CAST(N'2017-07-20 10:48:57.7657961' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (2, N'60.50.28.177', CAST(N'2017-07-20 10:51:09.9913961' AS DateTime2), 7, N'Super Admin', N'SystemSettings', N'Super Admin [Elvin Ong] Updated System Settings')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (3, N'222.165.58.122', CAST(N'2017-07-20 10:56:42.7393961' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (4, N'118.189.170.152', CAST(N'2017-07-20 11:37:49.7077961' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (5, N'118.189.170.152', CAST(N'2017-07-20 11:56:57.3841961' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (6, N'118.189.170.152', CAST(N'2017-07-20 12:00:30.9949961' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (8, N'60.50.28.177', CAST(N'2017-07-20 14:11:57.8557961' AS DateTime2), 4, N'User', N'Login Transactions', N'Accounts [accounts] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (9, N'118.189.170.152', CAST(N'2017-07-20 14:15:28.7989961' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (10, N'118.189.170.152', CAST(N'2017-07-20 14:19:49.8649961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700008]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (20, N'60.50.28.177', CAST(N'2017-07-20 14:48:59.4205961' AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (21, N'118.189.170.152', CAST(N'2017-07-20 14:50:23.8789961' AS DateTime2), 12, N'User', N'Login Transactions', N'Sales - Senior Manger [SM11] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (22, N'118.189.170.152', CAST(N'2017-07-20 14:55:59.2633961' AS DateTime2), 8, N'User', N'Login Transactions', N'Sales - Director [DirectorH] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (23, N'118.189.170.152', CAST(N'2017-07-20 14:59:27.7417961' AS DateTime2), 12, N'Sales Senior Manger', N'Customers', N'Sales Senior Manger [SM11] Created New Customers [C1700003]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (27, N'118.189.170.152', CAST(N'2017-07-20 15:12:45.0733961' AS DateTime2), 8, N'Sales Director', N'QuotationItemDescriptions', N'Sales Director [DirectorH] Created New Quotation Item Descriptions [Q1700004]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (28, N'118.189.170.152', CAST(N'2017-07-20 15:14:28.5013961' AS DateTime2), 10, N'User', N'Login Transactions', N'Sales - Director [Director1] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (36, N'118.189.170.152', CAST(N'2017-07-20 15:44:53.6857961' AS DateTime2), 9, N'User', N'Login Transactions', N'Sales - Senior Manger [SM3] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (37, N'118.189.170.152', CAST(N'2017-07-20 15:46:03.2305961' AS DateTime2), 12, N'Sales Senior Manger', N'Customers', N'Sales Senior Manger [SM11] Created New Customers [C1700006]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (38, N'118.189.170.152', CAST(N'2017-07-20 15:54:17.6413961' AS DateTime2), 12, N'Sales Senior Manger', N'Quotations', N'Sales Senior Manger [SM11] Created New Quotation [Q1700007]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (42, N'118.189.170.152', CAST(N'2017-07-20 16:00:47.3293961' AS DateTime2), 13, N'User', N'Login Transactions', N'Sales - Senior Manger [SM12] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (44, N'60.50.28.177', CAST(N'2017-07-20 16:03:05.2645961' AS DateTime2), 3, N'User', N'Login Transactions', N'Sales - Director [aaron] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (45, N'60.50.28.177', CAST(N'2017-07-20 16:03:42.1585961' AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (47, N'118.189.170.152', CAST(N'2017-07-20 16:06:02.5429961' AS DateTime2), 9, N'Sales Senior Manger', N'QuotationCoBrokes', N'Sales Senior Manger [SM3] Created New Quotation CoBrokes [Q1700008]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (50, N'118.189.170.152', CAST(N'2017-07-20 16:15:55.2181961' AS DateTime2), 14, N'Sales Senior Manger', N'Customers', N'Sales Senior Manger [SM13] Created New Customers [C1700008]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (51, N'118.189.170.152', CAST(N'2017-07-20 16:25:15.3985961' AS DateTime2), 13, N'User', N'Login Transactions', N'Sales - Senior Manger [SM12] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (58, N'118.189.170.152', CAST(N'2017-07-20 17:05:07.0657961' AS DateTime2), 13, N'Sales Senior Manger', N'Quotations', N'Sales Senior Manger [SM12] Created New Quotation [Q1700010]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (64, N'60.50.28.177', CAST(N'2017-07-20 17:11:05.1169961' AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (65, N'118.189.170.152', CAST(N'2017-07-20 17:20:20.4925961' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (67, N'118.189.170.152', CAST(N'2017-07-20 17:28:39.8485961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700016]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (88, N'118.189.170.152', CAST(N'2017-07-21 15:39:59.6849136' AS DateTime2), 15, N'Sales Senior Manger', N'Customers', N'Sales Senior Manger [SM21] Created New Customers [C1700011]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (89, N'118.189.170.152', CAST(N'2017-07-21 15:40:36.4541136' AS DateTime2), 11, N'Sales Director', N'Customers', N'Sales Director [Director2] Created New Customers [C1700012]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (94, N'118.189.170.152', CAST(N'2017-07-21 16:24:22.1993136' AS DateTime2), 11, N'User', N'Login Transactions', N'Sales - Director [Director2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (97, N'118.189.170.152', CAST(N'2017-07-21 16:36:52.5749136' AS DateTime2), 11, N'Customer', N'Login Transactions', N'Customer [tds9] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (98, N'60.50.28.177', CAST(N'2017-07-21 16:39:51.7721136' AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (99, N'118.189.170.152', CAST(N'2017-07-21 16:39:53.6753136' AS DateTime2), 15, N'User', N'Login Transactions', N'Sales - Senior Manger [SM21] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (103, N'118.189.170.152', CAST(N'2017-07-21 16:49:01.7501136' AS DateTime2), 15, N'User', N'Login Transactions', N'Sales - Senior Manger [SM21] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (104, N'118.189.170.152', CAST(N'2017-07-21 16:55:10.0661136' AS DateTime2), 11, N'User', N'Login Transactions', N'Sales - Director [Director2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (106, N'118.189.170.152', CAST(N'2017-07-21 16:58:29.7617136' AS DateTime2), 15, N'Sales Senior Manger', N'QuotationCoBrokes', N'Sales Senior Manger [SM21] Created New Quotation CoBrokes [Q1700014]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (107, N'118.189.170.152', CAST(N'2017-07-21 16:58:30.0737136' AS DateTime2), 15, N'Sales Senior Manger', N'QuotationItemDescriptions', N'Sales Senior Manger [SM21] Created New Quotation Item Descriptions [Q1700014]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (108, N'118.189.170.152', CAST(N'2017-07-21 17:09:15.0401136' AS DateTime2), 15, N'Sales Senior Manger', N'Customers', N'Sales Senior Manger [SM21] Created New Customers [C1700014]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (114, N'118.189.170.152', CAST(N'2017-07-21 17:20:47.2433136' AS DateTime2), 15, N'Sales Senior Manger', N'QuotationCoBrokes', N'Sales Senior Manger [SM21] Created New Quotation CoBrokes [Q1700016]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (116, N'118.189.170.152', CAST(N'2017-07-21 17:22:34.6805136' AS DateTime2), 17, N'User', N'Login Transactions', N'Sales - Senior Manger [SM23] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (124, N'118.189.170.152', CAST(N'2017-07-21 17:48:19.8605136' AS DateTime2), 16, N'Sales Senior Manger', N'QuotationCoBrokes', N'Sales Senior Manger [SM22] Created New Quotation CoBrokes [Q1700018]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (125, N'118.189.170.152', CAST(N'2017-07-21 17:48:20.3441136' AS DateTime2), 16, N'Sales Senior Manger', N'QuotationItemDescriptions', N'Sales Senior Manger [SM22] Created New Quotation Item Descriptions [Q1700018]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (151, N'60.50.28.177', CAST(N'2017-07-26 14:10:45.5017849' AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (155, N'60.50.28.177', CAST(N'2017-07-26 15:12:20.4888000' AS DateTime2), 2, N'User', N'Login Transactions', N'Super Admin [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (156, N'222.165.58.122', CAST(N'2017-07-26 15:14:08.5344000' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (157, N'60.50.28.177', CAST(N'2017-07-26 16:16:45.1080000' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (158, N'60.50.28.177', CAST(N'2017-07-26 17:08:47.2608000' AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (159, N'60.50.28.177', CAST(N'2017-07-26 17:09:00.1620000' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (160, N'60.50.28.177', CAST(N'2017-07-26 17:09:15.8868000' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (161, N'118.189.170.152', CAST(N'2017-07-26 17:48:22.8912000' AS DateTime2), 8, N'User', N'Login Transactions', N'Sales - Director [DirectorH] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (162, N'118.189.170.152', CAST(N'2017-07-26 17:48:41.0808000' AS DateTime2), 8, N'Sales Director', N'Quotations', N'Sales Director [DirectorH] Approved Quotation [Q1700004]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (163, N'60.50.28.177', CAST(N'2017-07-27 10:10:35.3972893' AS DateTime2), 2, N'User', N'Login Transactions', N'Super Admin [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (164, N'60.50.28.177', CAST(N'2017-07-27 10:15:20.4423165' AS DateTime2), 2, N'User', N'Login Transactions', N'Super Admin [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (165, N'60.50.28.177', CAST(N'2017-07-27 10:15:45.0124740' AS DateTime2), 2, N'User', N'Login Transactions', N'Super Admin [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (166, N'60.50.28.177', CAST(N'2017-07-27 10:44:49.4780564' AS DateTime2), 2, N'User', N'Login Transactions', N'Super Admin [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (167, N'110.159.7.139', CAST(N'2017-08-01 11:28:46.1073132' AS DateTime2), 2, N'User', N'Login Transactions', N'Super Admin [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (168, N'110.159.7.139', CAST(N'2017-08-01 12:15:04.5765132' AS DateTime2), 2, N'User', N'Login Transactions', N'Super Admin [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (169, N'110.159.7.158', CAST(N'2017-08-02 10:22:15.7559283' AS DateTime2), 2, N'User', N'Login Transactions', N'Super Admin [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (170, N'115.134.96.41', CAST(N'2017-08-09 15:26:08.2040006' AS DateTime2), 2, N'User', N'Login Transactions', N'Super Admin [erntay2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (7, N'118.189.170.152', CAST(N'2017-07-20 12:19:14.7097961' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (11, N'118.189.170.152', CAST(N'2017-07-20 14:22:55.2241961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700009]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (12, N'118.189.170.152', CAST(N'2017-07-20 14:23:11.4013961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Edited User [U1700009]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (13, N'118.189.170.152', CAST(N'2017-07-20 14:24:30.8365961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700010]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (14, N'118.189.170.152', CAST(N'2017-07-20 14:25:37.7137961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700011]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (15, N'118.189.170.152', CAST(N'2017-07-20 14:27:25.8685961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700012]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (16, N'118.189.170.152', CAST(N'2017-07-20 14:28:01.4053961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Edited User [U1700012]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (17, N'118.189.170.152', CAST(N'2017-07-20 14:29:17.5489961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700013]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (18, N'118.189.170.152', CAST(N'2017-07-20 14:42:03.1813961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700014]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (19, N'118.189.170.152', CAST(N'2017-07-20 14:47:41.3581961' AS DateTime2), 8, N'User', N'Login Transactions', N'Sales - Director [DirectorH] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (24, N'118.189.170.152', CAST(N'2017-07-20 15:04:06.9661961' AS DateTime2), 8, N'Sales Director', N'Customers', N'Sales Director [DirectorH] Created New Customers [C1700004]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (25, N'118.189.170.152', CAST(N'2017-07-20 15:12:44.2153961' AS DateTime2), 8, N'Sales Director', N'Quotations', N'Sales Director [DirectorH] Created New Quotation [Q1700004]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (26, N'118.189.170.152', CAST(N'2017-07-20 15:12:44.4649961' AS DateTime2), 8, N'Sales Director', N'QuotationCoBrokes', N'Sales Director [DirectorH] Created New Quotation CoBrokes [Q1700004]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (29, N'118.189.170.152', CAST(N'2017-07-20 15:20:07.5361961' AS DateTime2), 10, N'Sales Director', N'Customers', N'Sales Director [Director1] Created New Customers [C1700005]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (30, N'118.189.170.152', CAST(N'2017-07-20 15:36:23.4097961' AS DateTime2), 12, N'Sales Senior Manger', N'Quotations', N'Sales Senior Manger [SM11] Created New Quotation [Q1700005]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (31, N'118.189.170.152', CAST(N'2017-07-20 15:36:23.5345961' AS DateTime2), 12, N'Sales Senior Manger', N'QuotationCoBrokes', N'Sales Senior Manger [SM11] Created New Quotation CoBrokes [Q1700005]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (32, N'118.189.170.152', CAST(N'2017-07-20 15:36:25.5001961' AS DateTime2), 12, N'Sales Senior Manger', N'QuotationItemDescriptions', N'Sales Senior Manger [SM11] Created New Quotation Item Descriptions [Q1700005]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (33, N'118.189.170.152', CAST(N'2017-07-20 15:41:12.2749961' AS DateTime2), 10, N'Sales Director', N'Quotations', N'Sales Director [Director1] Created New Quotation [Q1700006]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (34, N'118.189.170.152', CAST(N'2017-07-20 15:41:12.3997961' AS DateTime2), 10, N'Sales Director', N'QuotationCoBrokes', N'Sales Director [Director1] Created New Quotation CoBrokes [Q1700006]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (35, N'118.189.170.152', CAST(N'2017-07-20 15:41:14.4121961' AS DateTime2), 10, N'Sales Director', N'QuotationItemDescriptions', N'Sales Director [Director1] Created New Quotation Item Descriptions [Q1700006]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (39, N'118.189.170.152', CAST(N'2017-07-20 15:54:17.7349961' AS DateTime2), 12, N'Sales Senior Manger', N'QuotationCoBrokes', N'Sales Senior Manger [SM11] Created New Quotation CoBrokes [Q1700007]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (40, N'118.189.170.152', CAST(N'2017-07-20 15:54:18.4681961' AS DateTime2), 12, N'Sales Senior Manger', N'QuotationItemDescriptions', N'Sales Senior Manger [SM11] Created New Quotation Item Descriptions [Q1700007]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (41, N'118.189.170.152', CAST(N'2017-07-20 15:57:30.3481961' AS DateTime2), 3, N'Customer', N'Login Transactions', N'Customer [tds1] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (43, N'118.189.170.152', CAST(N'2017-07-20 16:01:07.8745961' AS DateTime2), 9, N'Sales Senior Manger', N'Customers', N'Sales Senior Manger [SM3] Created New Customers [C1700007]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (46, N'118.189.170.152', CAST(N'2017-07-20 16:06:02.3713961' AS DateTime2), 9, N'Sales Senior Manger', N'Quotations', N'Sales Senior Manger [SM3] Created New Quotation [Q1700008]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (48, N'118.189.170.152', CAST(N'2017-07-20 16:06:02.9953961' AS DateTime2), 9, N'Sales Senior Manger', N'QuotationItemDescriptions', N'Sales Senior Manger [SM3] Created New Quotation Item Descriptions [Q1700008]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (49, N'118.189.170.152', CAST(N'2017-07-20 16:06:53.3053961' AS DateTime2), 14, N'User', N'Login Transactions', N'Sales - Senior Manger [SM13] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (52, N'118.189.170.152', CAST(N'2017-07-20 16:26:53.6785961' AS DateTime2), 13, N'Sales Senior Manger', N'Customers', N'Sales Senior Manger [SM12] Created New Customers [C1700009]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (53, N'118.189.170.152', CAST(N'2017-07-20 16:30:52.3741961' AS DateTime2), 14, N'Sales Senior Manger', N'Quotations', N'Sales Senior Manger [SM13] Created New Quotation [Q1700009]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (54, N'118.189.170.152', CAST(N'2017-07-20 16:30:52.4989961' AS DateTime2), 14, N'Sales Senior Manger', N'QuotationCoBrokes', N'Sales Senior Manger [SM13] Created New Quotation CoBrokes [Q1700009]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (55, N'118.189.170.152', CAST(N'2017-07-20 16:30:53.4817961' AS DateTime2), 14, N'Sales Senior Manger', N'QuotationItemDescriptions', N'Sales Senior Manger [SM13] Created New Quotation Item Descriptions [Q1700009]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (56, N'118.189.170.152', CAST(N'2017-07-20 16:31:39.8449961' AS DateTime2), 12, N'User', N'Login Transactions', N'Sales - Senior Manger [SM11] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (57, N'118.189.170.152', CAST(N'2017-07-20 16:49:34.0141961' AS DateTime2), 12, N'Sales Senior Manger', N'Customers', N'Sales Senior Manger [SM11] Created New Customers [C1700010]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (59, N'118.189.170.152', CAST(N'2017-07-20 17:05:07.2685961' AS DateTime2), 13, N'Sales Senior Manger', N'QuotationCoBrokes', N'Sales Senior Manger [SM12] Created New Quotation CoBrokes [Q1700010]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (60, N'118.189.170.152', CAST(N'2017-07-20 17:05:10.1701961' AS DateTime2), 13, N'Sales Senior Manger', N'QuotationItemDescriptions', N'Sales Senior Manger [SM12] Created New Quotation Item Descriptions [Q1700010]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (61, N'118.189.170.152', CAST(N'2017-07-20 17:07:27.3721961' AS DateTime2), 12, N'Sales Senior Manger', N'Quotations', N'Sales Senior Manger [SM11] Created New Quotation [Q1700011]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (62, N'118.189.170.152', CAST(N'2017-07-20 17:07:27.5749961' AS DateTime2), 12, N'Sales Senior Manger', N'QuotationCoBrokes', N'Sales Senior Manger [SM11] Created New Quotation CoBrokes [Q1700011]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (63, N'118.189.170.152', CAST(N'2017-07-20 17:07:29.0101961' AS DateTime2), 12, N'Sales Senior Manger', N'QuotationItemDescriptions', N'Sales Senior Manger [SM11] Created New Quotation Item Descriptions [Q1700011]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (66, N'118.189.170.152', CAST(N'2017-07-20 17:27:14.4385961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700015]')
GO
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (68, N'118.189.170.152', CAST(N'2017-07-20 17:29:51.0625961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700017]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (69, N'118.189.170.152', CAST(N'2017-07-20 17:31:50.7145961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700018]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (70, N'118.189.170.152', CAST(N'2017-07-20 17:32:55.5325961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700019]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (71, N'118.189.170.152', CAST(N'2017-07-20 17:34:27.2449961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700020]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (72, N'118.189.170.152', CAST(N'2017-07-20 17:35:32.6713961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700021]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (73, N'118.189.170.152', CAST(N'2017-07-20 17:35:32.8897961' AS DateTime2), 7, N'Super Admin', N'Quotations', N'Super Admin [elvin] Created New Quotation [Q1700004]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (74, N'118.189.170.152', CAST(N'2017-07-20 17:35:32.9521961' AS DateTime2), 7, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [elvin] Created New Quotation CoBrokes [Q1700004]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (75, N'118.189.170.152', CAST(N'2017-07-20 17:35:33.4669961' AS DateTime2), 7, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [elvin] Created New Quotation Item Descriptions [Q1700004]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (76, N'118.189.170.152', CAST(N'2017-07-20 17:38:59.1217961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700022]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (77, N'118.189.170.152', CAST(N'2017-07-20 17:40:30.0853961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700023]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (78, N'118.189.170.152', CAST(N'2017-07-20 17:43:21.9037961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700024]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (79, N'118.189.170.152', CAST(N'2017-07-20 17:44:31.1209961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700025]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (80, N'118.189.170.152', CAST(N'2017-07-20 17:45:58.9021961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700026]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (81, N'118.189.170.152', CAST(N'2017-07-20 17:47:37.2445961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700027]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (82, N'118.189.170.152', CAST(N'2017-07-20 17:49:42.1693961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700028]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (83, N'118.189.170.152', CAST(N'2017-07-20 17:50:54.8653961' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700029]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (84, N'60.50.28.177', CAST(N'2017-07-21 15:19:56.0513136' AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (85, N'222.165.58.122', CAST(N'2017-07-21 15:29:59.1317136' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (86, N'118.189.170.152', CAST(N'2017-07-21 15:36:01.4573136' AS DateTime2), 15, N'User', N'Login Transactions', N'Sales - Senior Manger [SM21] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (87, N'118.189.170.152', CAST(N'2017-07-21 15:36:05.0141136' AS DateTime2), 11, N'User', N'Login Transactions', N'Sales - Director [Director2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (90, N'118.189.170.152', CAST(N'2017-07-21 15:50:50.8757136' AS DateTime2), 15, N'Sales Senior Manger', N'Quotations', N'Sales Senior Manger [SM21] Created New Quotation [Q1700013]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (91, N'118.189.170.152', CAST(N'2017-07-21 15:50:50.9069136' AS DateTime2), 15, N'Sales Senior Manger', N'QuotationCoBrokes', N'Sales Senior Manger [SM21] Created New Quotation CoBrokes [Q1700013]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (92, N'118.189.170.152', CAST(N'2017-07-21 15:50:51.0317136' AS DateTime2), 15, N'Sales Senior Manger', N'QuotationItemDescriptions', N'Sales Senior Manger [SM21] Created New Quotation Item Descriptions [Q1700013]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (96, N'118.189.170.152', CAST(N'2017-07-21 16:29:51.8585136' AS DateTime2), 13, N'User', N'Login Transactions', N'Sales - Senior Manger [SM12] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (100, N'118.189.170.152', CAST(N'2017-07-21 16:43:23.4953136' AS DateTime2), 15, N'Sales Senior Manger', N'Customers', N'Sales Senior Manger [SM21] Created New Customers [C1700013]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (102, N'60.50.28.177', CAST(N'2017-07-21 16:49:00.5489136' AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (110, N'118.189.170.152', CAST(N'2017-07-21 17:18:13.5989136' AS DateTime2), 11, N'Sales Director', N'QuotationCoBrokes', N'Sales Director [Director2] Created New Quotation CoBrokes [Q1700015]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (111, N'118.189.170.152', CAST(N'2017-07-21 17:18:14.2541136' AS DateTime2), 11, N'Sales Director', N'QuotationItemDescriptions', N'Sales Director [Director2] Created New Quotation Item Descriptions [Q1700015]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (112, N'118.189.170.152', CAST(N'2017-07-21 17:20:24.5765136' AS DateTime2), 8, N'User', N'Login Transactions', N'Sales - Director [DirectorH] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (117, N'118.189.170.152', CAST(N'2017-07-21 17:25:33.0509136' AS DateTime2), 17, N'Sales Senior Manger', N'Customers', N'Sales Senior Manger [SM23] Created New Customers [C1700015]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (142, N'60.50.28.177', CAST(N'2017-07-26 09:32:50.8177849' AS DateTime2), 5, N'User', N'Login Transactions', N'Sales - Senior Manger [bobby] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (143, N'60.50.28.177', CAST(N'2017-07-26 09:51:52.0045849' AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (144, N'60.50.28.177', CAST(N'2017-07-26 10:03:44.9089849' AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (145, N'60.50.28.177', CAST(N'2017-07-26 10:04:05.4385849' AS DateTime2), 8, N'User', N'Login Transactions', N'Sales - Director [DirectorH] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (146, N'60.50.28.177', CAST(N'2017-07-26 10:07:45.1021849' AS DateTime2), 8, N'Sales Director', N'Quotations', N'Sales Director [DirectorH] Created New Quotation [Q1700019]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (147, N'60.50.28.177', CAST(N'2017-07-26 10:07:45.1177849' AS DateTime2), 8, N'Sales Director', N'QuotationCoBrokes', N'Sales Director [DirectorH] Created New Quotation CoBrokes [Q1700019]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (148, N'60.50.28.177', CAST(N'2017-07-26 10:07:45.1957849' AS DateTime2), 8, N'Sales Director', N'QuotationItemDescriptions', N'Sales Director [DirectorH] Created New Quotation Item Descriptions [Q1700019]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (149, N'183.90.37.19', CAST(N'2017-07-26 10:43:50.8033849' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (93, N'118.189.170.152', CAST(N'2017-07-21 16:00:23.5361136' AS DateTime2), 13, N'User', N'Login Transactions', N'Sales - Senior Manger [SM12] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (95, N'118.189.170.152', CAST(N'2017-07-21 16:25:16.0817136' AS DateTime2), 13, N'User', N'Login Transactions', N'Sales - Senior Manger [SM12] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (101, N'60.50.28.177', CAST(N'2017-07-21 16:47:38.8517136' AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (105, N'118.189.170.152', CAST(N'2017-07-21 16:58:29.7149136' AS DateTime2), 15, N'Sales Senior Manger', N'Quotations', N'Sales Senior Manger [SM21] Created New Quotation [Q1700014]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (109, N'118.189.170.152', CAST(N'2017-07-21 17:18:13.5521136' AS DateTime2), 11, N'Sales Director', N'Quotations', N'Sales Director [Director2] Created New Quotation [Q1700015]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (113, N'118.189.170.152', CAST(N'2017-07-21 17:20:47.2121136' AS DateTime2), 15, N'Sales Senior Manger', N'Quotations', N'Sales Senior Manger [SM21] Created New Quotation [Q1700016]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (118, N'118.189.170.152', CAST(N'2017-07-21 17:34:50.0333136' AS DateTime2), 16, N'User', N'Login Transactions', N'Sales - Senior Manger [SM22] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (120, N'118.189.170.152', CAST(N'2017-07-21 17:39:21.0053136' AS DateTime2), 17, N'Sales Senior Manger', N'Quotations', N'Sales Senior Manger [SM23] Created New Quotation [Q1700017]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (123, N'118.189.170.152', CAST(N'2017-07-21 17:48:19.8137136' AS DateTime2), 16, N'Sales Senior Manger', N'Quotations', N'Sales Senior Manger [SM22] Created New Quotation [Q1700018]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (126, N'118.189.170.152', CAST(N'2017-07-25 16:49:15.4187132' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (127, N'118.189.170.152', CAST(N'2017-07-25 16:56:39.2231132' AS DateTime2), 8, N'User', N'Login Transactions', N'Sales - Director [DirectorH] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (128, N'118.189.170.152', CAST(N'2017-07-25 16:58:04.4147132' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (129, N'118.189.170.152', CAST(N'2017-07-25 17:03:23.7779132' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Created New User [U1700030]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (130, N'118.189.170.152', CAST(N'2017-07-25 17:08:09.7103132' AS DateTime2), 7, N'Super Admin', N'Quotations', N'Super Admin [elvin] Created New Quotations Draft')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (131, N'118.189.170.152', CAST(N'2017-07-25 17:08:09.7259132' AS DateTime2), 7, N'Super Admin', N'QuotationCoBrokes', N'Super Admin [elvin] Created New Quotation CoBrokes [ Draft:19 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (132, N'118.189.170.152', CAST(N'2017-07-25 17:08:09.7883132' AS DateTime2), 7, N'Super Admin', N'QuotationItemDescriptions', N'Super Admin [elvin] Created New Quotation Item Descriptions [ Draft:19 ]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (133, N'118.189.170.152', CAST(N'2017-07-25 17:08:44.6543132' AS DateTime2), 8, N'User', N'Login Transactions', N'Sales - Director [DirectorH] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (134, N'118.189.170.152', CAST(N'2017-07-25 17:09:52.5611132' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (135, N'118.189.170.152', CAST(N'2017-07-25 17:12:04.8179132' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Edited User [U1700008]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (136, N'118.189.170.152', CAST(N'2017-07-25 17:12:33.0539132' AS DateTime2), 8, N'User', N'Login Transactions', N'Sales - Director [DirectorH] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (137, N'118.189.170.152', CAST(N'2017-07-25 17:12:49.5743132' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (138, N'118.189.170.152', CAST(N'2017-07-25 17:13:57.8711132' AS DateTime2), 7, N'Super Admin', N'Users', N'Super Admin [elvin] Edited User [U1700011]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (139, N'118.189.170.152', CAST(N'2017-07-25 17:14:12.4259132' AS DateTime2), 11, N'User', N'Login Transactions', N'Sales - Director [Director2] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (140, N'118.189.170.152', CAST(N'2017-07-25 17:36:08.3951132' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (141, N'60.50.28.177', CAST(N'2017-07-26 09:17:22.8361849' AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (150, N'222.165.58.122', CAST(N'2017-07-26 13:55:47.5657849' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (152, N'222.165.58.122', CAST(N'2017-07-26 14:45:19.1473849' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (153, N'60.50.28.177', CAST(N'2017-07-26 14:48:39.6385849' AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (154, N'60.50.28.177', CAST(N'2017-07-26 14:49:23.5525849' AS DateTime2), 7, N'User', N'Login Transactions', N'Super Admin [elvin] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (115, N'118.189.170.152', CAST(N'2017-07-21 17:20:47.6957136' AS DateTime2), 15, N'Sales Senior Manger', N'QuotationItemDescriptions', N'Sales Senior Manger [SM21] Created New Quotation Item Descriptions [Q1700016]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (119, N'118.189.170.152', CAST(N'2017-07-21 17:38:16.2809136' AS DateTime2), 16, N'Sales Senior Manger', N'Customers', N'Sales Senior Manger [SM22] Created New Customers [C1700016]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (121, N'118.189.170.152', CAST(N'2017-07-21 17:39:21.0521136' AS DateTime2), 17, N'Sales Senior Manger', N'QuotationCoBrokes', N'Sales Senior Manger [SM23] Created New Quotation CoBrokes [Q1700017]')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (122, N'118.189.170.152', CAST(N'2017-07-21 17:39:21.4889136' AS DateTime2), 17, N'Sales Senior Manger', N'QuotationItemDescriptions', N'Sales Senior Manger [SM23] Created New Quotation Item Descriptions [Q1700017]')
SET IDENTITY_INSERT [dbo].[AuditLogs] OFF
SET IDENTITY_INSERT [dbo].[CustomerReminders] ON 

INSERT [dbo].[CustomerReminders] ([ID], [CustomerId], [Description], [Date], [Repeat]) VALUES (1, 2, N'Reminder 1', CAST(N'2017-04-27 00:00:00.0000000' AS DateTime2), N'Monthly')
SET IDENTITY_INSERT [dbo].[CustomerReminders] OFF
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (1, N'C1700001', N'tds', N'Rn5sE6W5YkI=', N'The Dott Solutions Pte Ltd', N'Era', N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'elvinong@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'User', 2, N'Referral Tier 1', 16, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-04-27 17:27:04.9166847' AS DateTime2), CAST(N'2017-05-12 16:12:18.9038942' AS DateTime2), N'N', N'CSeBOj1cenu47VW1gYnZUw==')
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (2, N'C1700002', N'nodc', N'Rn5sE6W5YkI=', N'NODC Pte Ltd', N'Era', N'Number One Design + Consultancy', N'81 UBI AVENUE 4, #07-06, Singapore 408830', NULL, N'+658029579', N'+658029579', N'Bank of Singapore', N'123456789', NULL, N'User', 2, N'Referral Tier 1', 17, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-04-27 17:31:56.7660093' AS DateTime2), CAST(N'2017-04-27 17:34:12.8944143' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (3, N'C1700003', N'tds1', N'Rn5sE6W5YkI=', N'The Art Haus', N'No Agency', N'Slone Lim', N'50 East Coast Road, #01-110 , #01-134 Roxy Square, Singapore 428769', NULL, N'9222 3605', NULL, NULL, NULL, NULL, N'User', 12, N'Professional', 12, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 14:59:27.5701961' AS DateTime2), CAST(N'2017-07-20 14:59:27.5701961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (4, N'C1700004', N'tds2', N'Rn5sE6W5YkI=', N'MENCK ', N'No Agency', N'MR CHRISTIAN FRANK', N'107 TUAS SOUTH AVE 8 OFFSHORE MARINE CENTRE SINGAPORE 637036', NULL, N'67055800', N'67948345', NULL, NULL, NULL, N'User', 8, N'Professional', 8, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 15:04:06.8413961' AS DateTime2), CAST(N'2017-07-20 15:04:06.8413961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (5, N'C1700005', N'tds3', N'Rn5sE6W5YkI=', N'AceCom Technologies Pte Ltd', N'No Agency', N'Ms Haze Tan', N'1 Ubi View #03-10, Focus One Singapore 408555', NULL, N'63537767', NULL, NULL, NULL, NULL, N'User', 10, N'Referral Tier 1', 10, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 15:20:07.4581961' AS DateTime2), CAST(N'2017-07-20 15:20:07.4581961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (10, N'C1700010', N'tds8', N'Rn5sE6W5YkI=', N'My Banh Mi Singapore Pte Ltd', N'No Agency', N'Attn: Miss Tay Ying Hui', N'81 Ubi Ave 4 #07-06 Singapore 408830', NULL, N'94554784', NULL, NULL, NULL, NULL, N'User', 12, N'Professional', 12, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 16:49:33.9205961' AS DateTime2), CAST(N'2017-07-20 16:49:33.9205961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (11, N'C1700011', N'tds9', N'Rn5sE6W5YkI=', N'Grains Pte Ltd', N'No Agency', N'Jake Chia', N'703 Ang Mo Kio Ave 5#04-40 Singapore 569880 Northstar@AMK', NULL, N'9144 6336', NULL, NULL, NULL, NULL, N'User', 15, N'Referral Tier 1', 15, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-21 15:39:59.6381136' AS DateTime2), CAST(N'2017-07-21 15:39:59.6381136' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (12, N'C1700012', N'tds10', N'Rn5sE6W5YkI=', N'24 Pte Ltd', N'No Agency', N'Miss Swathi Mathur', N'21 Media Circle #05-06, Infinite Studios Singapore 138562', NULL, N'6589 8744', NULL, NULL, NULL, NULL, N'User', 11, N'Referral Tier 1', 11, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-21 15:40:36.4073136' AS DateTime2), CAST(N'2017-07-21 15:40:36.4073136' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (13, N'C1700013', N'tds11', N'Rn5sE6W5YkI=', N'Orange Lantern', N'No Agency', N'Alvin', N'1006 Aljunied Ave 5', NULL, N'9675 9990', NULL, NULL, NULL, NULL, N'User', 15, N'Referral Tier 1', 15, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-21 16:43:23.4329136' AS DateTime2), CAST(N'2017-07-21 16:43:23.4329136' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (14, N'C1700014', N'tds12', N'Rn5sE6W5YkI=', N'Pezzo Singapore Pte Ltd', N'No Agency', N'Chiang Zhan Yi', N'51 Imbiah Road, Singapore 099702', NULL, N'9640 7401', NULL, NULL, NULL, NULL, N'User', 15, N'Referral Tier 1', 15, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-21 17:09:15.0089136' AS DateTime2), CAST(N'2017-07-21 17:09:15.0089136' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (15, N'C1700015', N'tds13', N'Rn5sE6W5YkI=', N'Hidden Chefs Pte Ltd', N'No Agency', N'Kelly Bok', N'216 Boon Lay Avenue	 #01-01 Singapore 640216	', NULL, N'9631 7106', NULL, NULL, NULL, NULL, N'User', 17, N'Professional', 17, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-21 17:25:33.0197136' AS DateTime2), CAST(N'2017-07-21 17:25:33.0197136' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (16, N'C1700016', N'tds14', N'Rn5sE6W5YkI=', N'Sanfrance F&B Pte Ltd', N'No Agency', N'Michael', N'1 Jalan Anak Bukit #B1-52, Singapore 588996', NULL, N'9636 2377', NULL, NULL, NULL, NULL, N'User', 16, N'Referral Tier 1', 16, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-21 17:38:16.2497136' AS DateTime2), CAST(N'2017-07-21 17:38:16.2497136' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (6, N'C1700006', N'tds4', N'Rn5sE6W5YkI=', N'Outdoor Venture Pte Ltd', N'No Agency', N'Walter Tan', N'17 Kaki Bukit Crescent #02-02 Kaki Bukit Techpark 1, Singapore 416248', NULL, N'9819 6468', NULL, NULL, NULL, NULL, N'User', 12, N'Referral Tier 1', 12, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 15:46:03.1369961' AS DateTime2), CAST(N'2017-07-20 15:46:03.1369961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (7, N'C1700007', N'tds5', N'Rn5sE6W5YkI=', N'Now Communications Group Pte Ltd', N'No Agency', N'Mr Hendra Chong, Miss Trixy Tan', N'114 Lavender Street CT Hub 2, #08-53 Singapore 338729	', NULL, N'87820766', NULL, NULL, NULL, NULL, N'User', 9, N'Referral Tier 1', 9, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 16:01:07.7809961' AS DateTime2), CAST(N'2017-07-20 16:01:07.7809961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (8, N'C1700008', N'tds7', N'Rn5sE6W5YkI=', N'Outdoor Venture Pte Ltd', N'No Agency', N'Attn: Mr Walter Tan', N'17 Kaki Bukit Crescent #02-02 Kaki Bukit Techpark 1 Singapore 416248', NULL, N'98196468', NULL, NULL, NULL, NULL, N'User', 14, N'Professional', 14, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 16:15:55.1245961' AS DateTime2), CAST(N'2017-07-20 16:15:55.1245961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (9, N'C1700009', N'tds6', N'Rn5sE6W5YkI=', N'JE Education Pte Ltd', N'No Agency', N'Jacky Lim', N'224 Westwood Ave #09-15 Singapore 648366', NULL, N'9383 5543', NULL, NULL, NULL, NULL, N'User', 13, N'Referral Tier 1', 13, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 16:26:53.6317961' AS DateTime2), CAST(N'2017-07-20 16:26:53.6317961' AS DateTime2), N'N', NULL)
SET IDENTITY_INSERT [dbo].[Customers] OFF
SET IDENTITY_INSERT [dbo].[ProjectProjectExpenses] ON 

INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (1, 1, N'Colin''s Expenses 1', N'Colins', N'Invoice1', CAST(N'2017-04-27 00:00:00.000' AS DateTime), N'Yes', CAST(200.00 AS Decimal(18, 2)), N'Paid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (2, 2, N'Bobby''s Expenses', N'Bobby', N'Invoice2', CAST(N'2017-04-27 00:00:00.000' AS DateTime), N'Yes', CAST(300.00 AS Decimal(18, 2)), N'Paid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (3, 3, N'Aaron''s Expenses', N'Aaron', N'Invoice3', CAST(N'2017-04-27 00:00:00.000' AS DateTime), N'Yes', CAST(500.00 AS Decimal(18, 2)), N'Paid')
SET IDENTITY_INSERT [dbo].[ProjectProjectExpenses] OFF
SET IDENTITY_INSERT [dbo].[Projects] ON 

INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (1, N'P1700001', N'NODC Pte Ltd', 1, 1, 1, CAST(N'2017-04-27 00:00:00.0000000' AS DateTime2), 6, N'Colin''s Project $1,200', N'Total Quotation of $1,200', N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-04-27 18:07:40.9041829' AS DateTime2), CAST(N'2017-04-27 18:07:40.9041829' AS DateTime2), CAST(N'2017-04-27 18:07:40.9041829' AS DateTime2), N'N')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (2, N'P1700002', N'NODC Pte Ltd', 2, 1, 1, CAST(N'2017-04-27 00:00:00.0000000' AS DateTime2), 5, N'Bobby''s Project $1,500', N'Bobby''s Quotation $1,500', N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-05-27 18:09:07.8661945' AS DateTime2), CAST(N'2017-04-27 18:09:07.8661945' AS DateTime2), CAST(N'2017-04-27 18:09:07.8661945' AS DateTime2), N'N')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (3, N'P1700003', N'NODC Pte Ltd', 3, 2, 1, CAST(N'2017-04-27 00:00:00.0000000' AS DateTime2), 3, N'Aaron''s Project $2,000', N'Aaron''s Quotation $2,000', N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-06-27 18:10:39.3977041' AS DateTime2), CAST(N'2017-04-27 18:10:39.3977041' AS DateTime2), CAST(N'2017-04-27 18:10:39.3977041' AS DateTime2), N'N')
SET IDENTITY_INSERT [dbo].[Projects] OFF
SET IDENTITY_INSERT [dbo].[QuotationCoBrokes] ON 

INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (1, 1, NULL, 6, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (2, 2, NULL, 5, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (3, 3, NULL, 2, 45)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (4, 3, NULL, 3, 10)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (5, 3, NULL, 1, 45)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (6, 4, NULL, 8, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (7, 5, NULL, 12, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (8, 6, NULL, 10, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (11, 9, NULL, 14, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (16, 14, NULL, 15, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (17, 15, NULL, 11, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (18, 16, NULL, 15, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (19, 17, NULL, 17, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (9, 7, NULL, 12, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (10, 8, NULL, 9, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (12, 10, NULL, 13, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (13, 11, NULL, 12, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (14, 12, NULL, 7, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (15, 13, NULL, 15, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (21, 19, NULL, 7, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (22, 20, NULL, 8, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (20, 18, NULL, 16, 100)
SET IDENTITY_INSERT [dbo].[QuotationCoBrokes] OFF
SET IDENTITY_INSERT [dbo].[QuotationItemDescriptions] ON 

INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (1, 1, 0, N'Preliminaries', N'Preliminaries', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (2, 1, 0, N'Authority Submission', N'Authority Submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (3, 1, 0, N'Partitioning & Ceiling Work', N'Partitioning & Ceiling Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (4, 1, 0, N'Flooring Work', N'Flooring Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (5, 1, 0, N'Painting Work', N'Painting Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (6, 1, 0, N'Mill Work', N'Mill Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (7, 1, 0, N'Glazing Work', N'Glazing Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (8, 1, 0, N'Electrical', N'Electrical', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (9, 1, 0, N'Plumbing & Sanitory', N'Plumbing & Sanitory', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (10, 1, 0, N'ACMV Work', N'ACMV Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (11, 1, 0, N'Fire Protection Work', N'Fire Protection Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (12, 1, 0, N'Miscellaneous', N'Miscellaneous', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (13, 2, 0, N'Preliminaries', N'Preliminaries 1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (14, 2, 0, N'Preliminaries', N'Preliminaries 2', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (15, 2, 0, N'Authority Submission', N'Authority Submission 1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (16, 2, 0, N'Authority Submission', N'Authority Submission 2', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (17, 2, 0, N'Partitioning & Ceiling Work', N'Partitioning & Ceiling Work 1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (18, 2, 0, N'Partitioning & Ceiling Work', N'Partitioning & Ceiling Work 2', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (19, 2, 0, N'Flooring Work', N'Flooring Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (20, 2, 0, N'Painting Work', N'Painting Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (21, 2, 0, N'Mill Work', N'Mill Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (22, 2, 0, N'Glazing Work', N'Glazing Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (23, 2, 0, N'Electrical', N'Electrical', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (24, 2, 0, N'Plumbing & Sanitory', N'Plumbing & Sanitory', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (25, 2, 0, N'ACMV Work', N'ACMV Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (26, 2, 0, N'Fire Protection Work', N'Fire Protection Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (27, 2, 0, N'Miscellaneous', N'Miscellaneous', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (28, 3, 0, N'Preliminaries', N'Preliminaries', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (29, 3, 0, N'Authority Submission', N'Authority Submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (30, 3, 0, N'Partitioning & Ceiling Work', N'Partitioning & Ceiling Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (31, 3, 0, N'Flooring Work', N'Flooring Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (32, 3, 0, N'Painting Work', N'Painting Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (33, 3, 0, N'Mill Work', N'Mill Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (34, 3, 0, N'Glazing Work', N'Glazing Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (35, 3, 0, N'Electrical', N'Electrical', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (36, 3, 0, N'Plumbing & Sanitory', N'Plumbing & Sanitory', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (37, 3, 0, N'ACMV Work', N'ACMV Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (38, 3, 0, N'Fire Protection Work', N'Fire Protection Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (39, 3, 0, N'Miscellaneous', N'Miscellaneous', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(900.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (40, 4, 0, N'PARTITION / WALL WORK', N'"Supply and install 75mm thick gypsum partition c/w plastering work and paint finishes (Compactus to be removed temporary by Client for the installation work)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(950.00 AS Decimal(18, 2)), CAST(950.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(250.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (41, 4, 0, N'PARTITION / WALL WORK', N'Supply and install selected range wallpaper (Easy-washed range)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2800.00 AS Decimal(18, 2)), CAST(2800.00 AS Decimal(18, 2)), CAST(1320.00 AS Decimal(18, 2)), CAST(1480.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (43, 4, 0, N'GLASS  WORK', N'Supply and install framing to glass as per existing office glass detail', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(600.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (46, 5, 0, N'Preliminaries', N'Design Consultancy including 3D for landlord submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (101, 7, 0, N'Builders Work', N'"Supply labour to install 9 set of wire mesh onto ceiling (Wire mesh by other)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1350.00 AS Decimal(18, 2)), CAST(1350.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(950.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (104, 7, 0, N'Electrical', N'Supply and install S/S/O', CAST(5.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (106, 7, 0, N'Electrical', N'Supply and install lighting point', CAST(9.0 AS Decimal(18, 1)), N'lot', CAST(90.00 AS Decimal(18, 2)), CAST(810.00 AS Decimal(18, 2)), CAST(630.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (108, 7, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (113, 8, 0, N'WALL WORK', N'"Supply and install 20mm thick laminate writing panel mounted onto existing framework. Dimension 1550 x 1300 Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(550.00 AS Decimal(18, 2)), CAST(550.00 AS Decimal(18, 2)), CAST(440.00 AS Decimal(18, 2)), CAST(110.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (115, 9, 0, N'Preliminaries', N'Design submission to landlord for approval including 3D', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (42, 4, 0, N'GLASS  WORK', N'Supply and install 10mm thick glass c/w u-channel onto existing wall and floor including frosted film (2pieces) "PS: Create swing door c/w necessary iron mongery.  (additional $350)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2200.00 AS Decimal(18, 2)), CAST(2200.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (44, 4, 0, N'FLOOR WORK', N'Supply and install 5mm thick vinyl clip system flooring including removal of existing carpet tiles', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2870.00 AS Decimal(18, 2)), CAST(2870.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(1870.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (45, 5, 0, N'Preliminaries', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(800.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (47, 5, 0, N'Preliminaries', N'Installation of 12mm thick gypsum hoarding c/w 1 no of swing door and white paint finishes up to 3000mm height', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2700.00 AS Decimal(18, 2)), CAST(2700.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (48, 5, 0, N'Preliminaries', N'Final Cleaning, Removal of hoarding and Clearing of Debris', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (49, 5, 0, N'Authority Submission', N'Building Plan Submission ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3750.00 AS Decimal(18, 2)), CAST(3750.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (50, 5, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel, and plastering work up to slab at 4200mm Ht Area: Leaseline and inter-tenancy wall"', CAST(700.0 AS Decimal(18, 1)), N'lot', CAST(4.50 AS Decimal(18, 2)), CAST(3150.00 AS Decimal(18, 2)), CAST(3100.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (52, 5, 0, N'Glazing Work', N'Supply and install 12mm thick tempered glass c/w u-channel support top and bottom, 2400mm Ht', CAST(125.0 AS Decimal(18, 1)), N'lot', CAST(16.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (54, 5, 0, N'Joinery Work', N'Supply and install 1500mm ht reception counter cabinet in laminate finishes an solid surface top', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1875.00 AS Decimal(18, 2)), CAST(1875.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(375.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (55, 5, 0, N'Joinery Work', N'"Supply and install sink cabinet in selected laminate finishes c/w solid surface finishing, 900mm ht (Sink and Faucet by other)"', CAST(16.0 AS Decimal(18, 1)), N'lot', CAST(250.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(3200.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (57, 5, 0, N'Wall and Floor Finishes', N'Supply labour and material to repaint entire area in emulsion white finishes c/w sealant', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (60, 5, 0, N'Air Conditioning Work', N'"To supply and install FCU ducts c/w 25mm thick 32kg insulation inclusive of night work charges PS: Landlord to approved on the tee-off point from FCU outside the unit"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5460.00 AS Decimal(18, 2)), CAST(5460.00 AS Decimal(18, 2)), CAST(4200.00 AS Decimal(18, 2)), CAST(1260.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (61, 5, 0, N'Air Conditioning Work', N'To supply and install 300mm dia flexible ducts c/w 4 ways diffuser in white colour finishing', CAST(9.0 AS Decimal(18, 1)), N'lot', CAST(300.00 AS Decimal(18, 2)), CAST(2700.00 AS Decimal(18, 2)), CAST(2250.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (62, 5, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On, Inclusive of sub main cable', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2550.00 AS Decimal(18, 2)), CAST(2550.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(550.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (63, 5, 0, N'Electrical', N'Supply and install T/S/O', CAST(8.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(880.00 AS Decimal(18, 2)), CAST(720.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (64, 5, 0, N'Electrical', N'Supply and install 15amp S/S/O', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(150.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (65, 5, 0, N'Electrical', N'Supply and install lighting point', CAST(16.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(1280.00 AS Decimal(18, 2)), CAST(960.00 AS Decimal(18, 2)), CAST(320.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (66, 5, 0, N'Electrical', N'Supply and install timer for shop front signage', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(60.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), CAST(45.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (67, 5, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (68, 5, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (71, 5, 0, N'Electrical', N'Supply labour and material to install light fitting', CAST(16.0 AS Decimal(18, 1)), N'lot', CAST(15.00 AS Decimal(18, 2)), CAST(240.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (72, 5, 0, N'Fire Protection Work and Plumbing Work', N'Supply and install 3 nos PVC outlet pipe and PPR inlet pipe as per SP requirement', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3500.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(2800.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (73, 6, 0, N'Preliminaries and Demolition Work', N'To supply labour for general cleaning work upon completion of renovation work including dismantling of partition', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (75, 6, 0, N'Preliminaries and Demolition Work', N'Design Consultancy including 3D for submission to landlord for permit to work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2300.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (77, 6, 0, N'Authority Submission (provision sum)', N'MAA Submission (Pending SCDF approval)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (79, 6, 0, N'Builders Work-Ceiling and Partition', N'Supply and install 19mm thick gypsum ceiling c/w cove light design at 3000Ht', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(7770.00 AS Decimal(18, 2)), CAST(7770.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(770.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (81, 6, 0, N'Builders Work-Foor Finishing', N'"Supply and install slected range carpet tiles PC Rate: $2/sqft range"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5895.00 AS Decimal(18, 2)), CAST(5895.00 AS Decimal(18, 2)), CAST(5171.00 AS Decimal(18, 2)), CAST(724.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (83, 6, 0, N'Builders Work-Carpentry', N'Supply labour and install 1200Ht x 1200ht reception counter in laminate finishing c/w tempered glass top', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1488.00 AS Decimal(18, 2)), CAST(512.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (51, 5, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel, and plastering work up to slab at 3000mm Ht Area: Internal partitioning wall"', CAST(510.0 AS Decimal(18, 1)), N'lot', CAST(4.00 AS Decimal(18, 2)), CAST(2040.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (53, 5, 0, N'Glazing Work', N'Supply and install 2100ht, 10mm thick tempered glass door c/w selected range floor spring, 600mm ht handle bar and lockset.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1500.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1360.00 AS Decimal(18, 2)), CAST(140.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (56, 5, 0, N'Joinery Work', N'Supply and install LED signage at shop front,', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3200.00 AS Decimal(18, 2)), CAST(3200.00 AS Decimal(18, 2)), CAST(2800.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (58, 5, 0, N'Wall and Floor Finishes', N'"Supply labour and material to screed up existing flooring (night delivery of material)"', CAST(1060.0 AS Decimal(18, 1)), N'lot', CAST(5.00 AS Decimal(18, 2)), CAST(5300.00 AS Decimal(18, 2)), CAST(4240.00 AS Decimal(18, 2)), CAST(1060.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (59, 5, 0, N'Wall and Floor Finishes', N'Supply labour and install selected range 3mm thick vinyl flooring', CAST(1060.0 AS Decimal(18, 1)), N'lot', CAST(4.00 AS Decimal(18, 2)), CAST(4240.00 AS Decimal(18, 2)), CAST(3180.00 AS Decimal(18, 2)), CAST(1060.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (69, 5, 0, N'Electrical', N'Lighting point include supply & install of E-light fitting', CAST(6.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(480.00 AS Decimal(18, 2)), CAST(360.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (70, 5, 0, N'Electrical', N'Lighting point include supply & install of Exit light fitting', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(240.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (74, 6, 0, N'Preliminaries and Demolition Work', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (76, 6, 0, N'Preliminaries and Demolition Work', N'Supply labour and material to demolished existing fixture and disposed off-site and terminate all electricity to DB', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4500.00 AS Decimal(18, 2)), CAST(4500.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (78, 6, 0, N'Authority Submission (provision sum)', N'Plan fees (Reimbursement)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(90.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (80, 6, 0, N'Builders Work-Ceiling and Partition', N'Supply and install 75mm thick gypsum partition using 50mm u-channel c/w 40kg rock wools up to 3000Ht', CAST(1430.0 AS Decimal(18, 1)), N'sqft', CAST(5.00 AS Decimal(18, 2)), CAST(7150.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (82, 6, 0, N'Builders Work-Foor Finishing', N'"Supply labour and material to overlay outdoor flooring inclusive tiles wastage PC Rate: $3/sqft range"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(6080.00 AS Decimal(18, 2)), CAST(6080.00 AS Decimal(18, 2)), CAST(6000.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (84, 6, 0, N'Builders Work-Carpentry', N'"Supply and install Top and bottom cabinet in laminate finishes and internal white polykem c/w  glass back splash and solid surface top PC Rate: solid surface $80/ftrun"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4700.00 AS Decimal(18, 2)), CAST(4700.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (85, 6, 0, N'Builders Work-Carpentry', N'Supply and install full height cabinet in selected laminate finishes and internal colour pvc c/w individual lockers and compartment to detail (up to 2400ht)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(8640.00 AS Decimal(18, 2)), CAST(8640.00 AS Decimal(18, 2)), CAST(8000.00 AS Decimal(18, 2)), CAST(640.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (86, 6, 0, N'Builders Work-Carpentry', N'Supply and install full height cabinet in selected laminate finishes and internal colour pvc c/w individual pigeon hole lockers and casement cabinet to detail (up to 2400ht)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (87, 6, 0, N'Builders Work-Carpentry', N'"Supply and install white board c/w marker holder to detail at meeting room 2000L x 1200Ht"', CAST(3.0 AS Decimal(18, 1)), N'nos', CAST(950.00 AS Decimal(18, 2)), CAST(2850.00 AS Decimal(18, 2)), CAST(2166.00 AS Decimal(18, 2)), CAST(684.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (88, 6, 0, N'Painting Work', N'Supply labour and materail to paint existing office using nippon paint', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4000.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (89, 6, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (90, 6, 0, N'Electrical', N'Supply and install S/S/O', CAST(3.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), CAST(230.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (91, 6, 0, N'Electrical', N'Supply and install T/S/O', CAST(35.0 AS Decimal(18, 1)), N'nos', CAST(110.00 AS Decimal(18, 2)), CAST(3850.00 AS Decimal(18, 2)), CAST(3150.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (92, 6, 0, N'Electrical', N'Supply and install lighting point for signage and LED Strip and Kebab Machine', CAST(22.0 AS Decimal(18, 1)), N'nos', CAST(80.00 AS Decimal(18, 2)), CAST(1760.00 AS Decimal(18, 2)), CAST(1600.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (93, 6, 0, N'Electrical', N'Supply and install Telephone Line (Non PABx System)', CAST(35.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(3100.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
GO
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (94, 6, 0, N'Electrical', N'Supply and install Cat 5E Data Line', CAST(35.0 AS Decimal(18, 1)), N'nos', CAST(110.00 AS Decimal(18, 2)), CAST(3850.00 AS Decimal(18, 2)), CAST(3700.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (95, 7, 0, N'Preliminaries', N'Design submission to landlord for approval including 3D', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (96, 7, 0, N'Preliminaries', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(800.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (97, 7, 0, N'Preliminaries', N'Supply labour and material to demolish hoarding and general cleaning upon completion', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(2700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (98, 7, 0, N'Authority Submission (provision sum)', N'Building Plan Submission ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2880.00 AS Decimal(18, 2)), CAST(2880.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), CAST(2230.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (99, 7, 0, N'Builders Work', N'"Supply labour and material to install racking and boltless shelves PS: assuming partition reinforcement still intact"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(750.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (100, 7, 0, N'Builders Work', N'Hanging 2 set of fitting room curtain', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(350.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), CAST(250.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (102, 7, 0, N'Builders Work', N'Supply and install grey exhibition carpet onto existing floor', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3120.00 AS Decimal(18, 2)), CAST(3120.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1120.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (103, 7, 0, N'Electrical', N'To alter existing DB c/w necessary MCB and MCCB including temp supply turn on by NODC appointed LEW, PE endorsement on Singleline drawing and perm turn on testing', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (105, 7, 0, N'Electrical', N'Supply and install T/S/O', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(220.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (107, 7, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (109, 8, 0, N'WALL WORK', N'"Supply and install 65mm thick acoustic wall panels c/w acoustic fabrics and fibre glass mounted onto existing framework. Dimension 2950 x 2600. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3240.00 AS Decimal(18, 2)), CAST(3240.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(2240.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (110, 8, 0, N'WALL WORK', N'"Supply and install 65mm thick acoustic wall panels c/w acoustic fabrics and fibre glass mounted onto existing framework. Dimension 1550 x 1300 Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(860.00 AS Decimal(18, 2)), CAST(860.00 AS Decimal(18, 2)), CAST(660.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (111, 8, 0, N'WALL WORK', N'"Supply and install 65mm thick acoustic wall panels c/w acoustic fabrics and fibre glass mounted onto existing framework. Dimension 1880 x 2080. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1640.00 AS Decimal(18, 2)), CAST(1640.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(1040.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (112, 8, 0, N'WALL WORK', N'"Supply and install 20mm thick laminate writing panel mounted onto existing framework. Dimension 2950 x 2600. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2075.00 AS Decimal(18, 2)), CAST(2075.00 AS Decimal(18, 2)), CAST(875.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (114, 8, 0, N'WALL WORK', N'"Supply and install 20mm thick laminate writing panel mounted onto existing framework. Dimension 1880 x 2080. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1050.00 AS Decimal(18, 2)), CAST(1050.00 AS Decimal(18, 2)), CAST(840.00 AS Decimal(18, 2)), CAST(210.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (116, 9, 0, N'Preliminaries', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(800.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (117, 9, 0, N'Preliminaries', N'Supply labour and material to demolish hoarding and general cleaning upon completion', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(2700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (120, 9, 0, N'Builders Work', N'Hanging 2 set of fitting room curtain', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(350.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), CAST(250.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (121, 9, 0, N'Builders Work', N'"Supply labour to install 9 set of wire mesh onto ceiling (Wire mesh by other)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1350.00 AS Decimal(18, 2)), CAST(1350.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (122, 9, 0, N'Builders Work', N'Supply and install grey exhibition carpet onto existing floor', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3120.00 AS Decimal(18, 2)), CAST(3120.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), CAST(720.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (124, 9, 0, N'Electrical', N'Supply and install S/S/O', CAST(5.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (125, 9, 0, N'Electrical', N'Supply and install T/S/O', CAST(2.0 AS Decimal(18, 1)), N'nos', CAST(110.00 AS Decimal(18, 2)), CAST(220.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (127, 9, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'no', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (128, 9, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (130, 10, 0, N'Preliminaries', N'Final Cleaning, Removal of hoarding and Clearing of Debris', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2200.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (133, 10, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel, single side double layer claded to existing wall, 80kg/m3 rockwool and plastering work up to 3650mm Ht Area: Non Mirror Side"', CAST(485.0 AS Decimal(18, 1)), N'lot', CAST(5.00 AS Decimal(18, 2)), CAST(2425.00 AS Decimal(18, 2)), CAST(2300.00 AS Decimal(18, 2)), CAST(125.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (135, 10, 0, N'Partition and Ceiling Work', N'"Supply and install 100mm thick gypsum partition wall c/w 50mm aluminium u-channel, double side double layer claded to wall, 80kg/m3 rockwool and plastering work up to 3650mm Ht Area: Mirror Side"', CAST(170.0 AS Decimal(18, 1)), N'lot', CAST(6.50 AS Decimal(18, 2)), CAST(1105.00 AS Decimal(18, 2)), CAST(884.00 AS Decimal(18, 2)), CAST(221.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (138, 10, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum drop down partition wall c/w 50mm aluminium u-channel and plastering work up to 2700mm Ht to 3650Ht Area: Mirror Side"', CAST(92.0 AS Decimal(18, 1)), N'lot', CAST(4.00 AS Decimal(18, 2)), CAST(368.00 AS Decimal(18, 2)), CAST(276.00 AS Decimal(18, 2)), CAST(92.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (140, 10, 0, N'Glazing Work', N'Supply and instal 2700mm ht, 12mm thick tempered glass c/w top and bottom embeded aluminium u-channel.', CAST(256.0 AS Decimal(18, 1)), N'lot', CAST(15.00 AS Decimal(18, 2)), CAST(3840.00 AS Decimal(18, 2)), CAST(3072.00 AS Decimal(18, 2)), CAST(768.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (141, 10, 0, N'Glazing Work', N'Supply and install 2100ht, 10mm thick tempered glass door c/w selected range floor spring, 600mm ht handle bar and lockset.', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(1250.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (143, 10, 0, N'Joinery Work', N'Supply and install 1200mm ht reception counter cabinet in laminate finishes an solid surface top', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1400.00 AS Decimal(18, 2)), CAST(1400.00 AS Decimal(18, 2)), CAST(1125.00 AS Decimal(18, 2)), CAST(275.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (145, 10, 0, N'Joinery Work', N'"Supply and install laminated door in laminate and mirror finishes Area: Reception Counter"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(750.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (118, 9, 0, N'Authority Submission (provision sum)Preparation of drawings for Authority Submission', N'Building Plan Submission ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2880.00 AS Decimal(18, 2)), CAST(2880.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(1880.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (119, 9, 0, N'Builders Work', N'"Supply labour and material to install racking and boltless shelves PS: assuming partition reinforcement still intact"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(750.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (123, 9, 0, N'Electrical', N'To alter existing DB c/w necessary MCB and MCCB including temp supply turn on by NODC appointed LEW, PE endorsement on Singleline drawing and perm turn on testing', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), CAST(1250.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (126, 9, 0, N'Electrical', N'Supply and install lighting point', CAST(9.0 AS Decimal(18, 1)), N'nos', CAST(90.00 AS Decimal(18, 2)), CAST(810.00 AS Decimal(18, 2)), CAST(530.00 AS Decimal(18, 2)), CAST(280.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (129, 10, 0, N'Preliminaries', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(800.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (131, 10, 0, N'Authority Submission', N'Building Plan Submission ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3600.00 AS Decimal(18, 2)), CAST(3600.00 AS Decimal(18, 2)), CAST(3286.00 AS Decimal(18, 2)), CAST(314.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (132, 10, 0, N'Authority Submission', N'QP endorsement and calculation on heat load calculation.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (134, 10, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel, single side double layer claded to existing wall, 80kg/m3 rockwool and plastering work up to 3650mm Ht Area: Mirror Side"', CAST(265.0 AS Decimal(18, 1)), N'lot', CAST(5.00 AS Decimal(18, 2)), CAST(1325.00 AS Decimal(18, 2)), CAST(1180.00 AS Decimal(18, 2)), CAST(145.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (136, 10, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel,  and plastering work up to 3650mm Ht Area: TV Wall Column"', CAST(40.0 AS Decimal(18, 1)), N'lot', CAST(4.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (137, 10, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel,  and plastering work up to 3650mm Ht Area: Counter Door Area"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(550.00 AS Decimal(18, 2)), CAST(550.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (139, 10, 0, N'Partition and Ceiling Work', N'"Supply and install 9mm thick gypsum ceiling c/w 50mm aluminium u-channel and plastering work to entire ceiling Area: General"', CAST(745.0 AS Decimal(18, 1)), N'lot', CAST(4.00 AS Decimal(18, 2)), CAST(2980.00 AS Decimal(18, 2)), CAST(2235.00 AS Decimal(18, 2)), CAST(745.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (142, 10, 0, N'Glazing Work', N'Supply and install 2700mm ht, 6mm thick clear mirror onto new erected partition', CAST(315.0 AS Decimal(18, 1)), N'lot', CAST(9.00 AS Decimal(18, 2)), CAST(2835.00 AS Decimal(18, 2)), CAST(2405.00 AS Decimal(18, 2)), CAST(430.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (144, 10, 0, N'Joinery Work', N'"Supply and install 950mm height MDF Spray painted bulk head Area: Outside Conference room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(6150.00 AS Decimal(18, 2)), CAST(6150.00 AS Decimal(18, 2)), CAST(5620.00 AS Decimal(18, 2)), CAST(530.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (146, 10, 0, N'Wall and Floor Finishes', N'Supply labour and material to repaint entire area in emulsion white finishes c/w sealant', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (147, 10, 0, N'Wall and Floor Finishes', N'"Supply labour and material to screed up existing flooring (night delivery of material)"', CAST(745.0 AS Decimal(18, 1)), N'lot', CAST(5.00 AS Decimal(18, 2)), CAST(3725.00 AS Decimal(18, 2)), CAST(2980.00 AS Decimal(18, 2)), CAST(745.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (148, 10, 0, N'Wall and Floor Finishes', N'Supply labour and material to apply self leveling screed to receive 5mm thick vinyl floor finishes', CAST(745.0 AS Decimal(18, 1)), N'lot', CAST(2.00 AS Decimal(18, 2)), CAST(1490.00 AS Decimal(18, 2)), CAST(1117.50 AS Decimal(18, 2)), CAST(372.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (149, 10, 0, N'Wall and Floor Finishes', N'Supply labour and install 5mm thick vinyl flooring', CAST(745.0 AS Decimal(18, 1)), N'lot', CAST(6.50 AS Decimal(18, 2)), CAST(4842.50 AS Decimal(18, 2)), CAST(3725.00 AS Decimal(18, 2)), CAST(1117.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (150, 10, 0, N'Air Conditioning Work', N'To supply and install 1 nos of FCU ducts c/w 25mm thick 32kg insulation c/w night work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2725.00 AS Decimal(18, 2)), CAST(2725.00 AS Decimal(18, 2)), CAST(2180.00 AS Decimal(18, 2)), CAST(545.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (151, 10, 0, N'Air Conditioning Work', N'To supply and install 300mm dia flexible ducts c/w 4 ways diffuser in white colour finishing', CAST(8.0 AS Decimal(18, 1)), N'lot', CAST(300.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (152, 10, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On, Inclusive of sub main cable', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2550.00 AS Decimal(18, 2)), CAST(2550.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(550.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (153, 10, 0, N'Electrical', N'Supply and install S/S/O', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), CAST(30.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (154, 10, 0, N'Electrical', N'Supply and install T/S/O', CAST(5.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(550.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (155, 10, 0, N'Electrical', N'Supply and install 15amp S/S/O', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(150.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (156, 10, 0, N'Electrical', N'Supply and install 20amp Single phase isolator', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(250.00 AS Decimal(18, 2)), CAST(250.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (157, 10, 0, N'Electrical', N'Supply and install lighting point', CAST(24.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(1920.00 AS Decimal(18, 2)), CAST(1440.00 AS Decimal(18, 2)), CAST(480.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (158, 10, 0, N'Electrical', N'Supply and install timer for 2 shop front signage', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(60.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(30.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (159, 10, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (160, 10, 0, N'Electrical', N'Supply and install Data Line', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(170.00 AS Decimal(18, 2)), CAST(30.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (161, 10, 0, N'Electrical', N'Lighting point include supply & install of E-light fitting', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (162, 10, 0, N'Electrical', N'Lighting point include supply & install of Exit light fitting', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(240.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (163, 10, 0, N'Electrical', N'Supply labour and material to install light fitting', CAST(24.0 AS Decimal(18, 1)), N'lot', CAST(15.00 AS Decimal(18, 2)), CAST(360.00 AS Decimal(18, 2)), CAST(240.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (164, 10, 0, N'Electrical', N'Supply labour and material to install signages', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(300.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (165, 11, 0, N'Preliminaries', N'"Supply labour and material to construct hoarding in 12mm thick gypsum board c/w swing door and emulsion paint  (By outgoing Tenant)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1326.00 AS Decimal(18, 2)), CAST(1326.00 AS Decimal(18, 2)), CAST(1020.00 AS Decimal(18, 2)), CAST(306.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (166, 11, 0, N'Preliminaries', N'"To supply and install graphics print to hoarding (Graphics by Owner)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(650.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), CAST(455.00 AS Decimal(18, 2)), CAST(195.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (167, 11, 0, N'Preliminaries', N'Public Liability Insurance ($2 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(600.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (168, 11, 0, N'Preliminaries', N'Design Consultancy including 3D for submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (169, 11, 0, N'Authority Submission (provision sum)', N'Building Plan Submission ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2880.00 AS Decimal(18, 2)), CAST(2880.00 AS Decimal(18, 2)), CAST(2880.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (170, 11, 0, N'Builders Work', N'Demolish existing partition and to to remove all unwanted fixtures and dispose off site', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(800.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (171, 11, 0, N'Builders Work', N'Touch up to damage ceiling work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(300.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (172, 11, 0, N'Builders Work', N'Supply and install new partition wall', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(767.00 AS Decimal(18, 2)), CAST(767.00 AS Decimal(18, 2)), CAST(590.00 AS Decimal(18, 2)), CAST(177.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (173, 11, 0, N'Builders Work', N'Supply and install shopfront counter in solid surface top and vinyl strip finishes c/w POS counter and flip top', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2899.00 AS Decimal(18, 2)), CAST(2899.00 AS Decimal(18, 2)), CAST(2230.00 AS Decimal(18, 2)), CAST(669.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (174, 11, 0, N'Builders Work', N'Supply and install back feature wall c/w conceal door in laminate finishes and nyatoh wood', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4797.00 AS Decimal(18, 2)), CAST(4797.00 AS Decimal(18, 2)), CAST(3690.00 AS Decimal(18, 2)), CAST(1107.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (175, 11, 0, N'Builders Work', N'Supply and install TV Brackets mounted onto wall', CAST(3.0 AS Decimal(18, 1)), N'nos', CAST(50.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (176, 11, 0, N'Builders Work', N'Supply and install My Banh Mi Logo', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (177, 11, 0, N'Builders Work', N'Supply and install stainless backing onto wall at BOH', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (178, 11, 0, N'Plumbing', N'Supply and install PPR water inlet and PVC outlet point including fees for license plumbing endorsement as per drawing', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (179, 11, 0, N'ACMV Work (optional)', N'Relocation of Exhaust hood', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(400.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (180, 11, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2400.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), CAST(1800.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (181, 11, 0, N'Electrical', N'Supply and install S/S/O', CAST(7.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(490.00 AS Decimal(18, 2)), CAST(210.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (182, 11, 0, N'Electrical', N'Supply and install T/S/O', CAST(4.0 AS Decimal(18, 1)), N'nos', CAST(110.00 AS Decimal(18, 2)), CAST(440.00 AS Decimal(18, 2)), CAST(360.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (183, 11, 0, N'Electrical', N'Supply and install 15amp S/S/O', CAST(4.0 AS Decimal(18, 1)), N'nos', CAST(150.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(360.00 AS Decimal(18, 2)), CAST(240.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (184, 11, 0, N'Electrical', N'Supply and install 20amp 3 phase isolator', CAST(1.0 AS Decimal(18, 1)), N'nos', CAST(320.00 AS Decimal(18, 2)), CAST(320.00 AS Decimal(18, 2)), CAST(250.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (185, 11, 0, N'Electrical', N'Supply and install lighting point for signage, carpentry, ceiling, and Kebab Machine', CAST(11.0 AS Decimal(18, 1)), N'nos', CAST(80.00 AS Decimal(18, 2)), CAST(880.00 AS Decimal(18, 2)), CAST(660.00 AS Decimal(18, 2)), CAST(220.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (186, 11, 0, N'Electrical', N'Supply and install timer for signage', CAST(1.0 AS Decimal(18, 1)), N'nos', CAST(80.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (187, 11, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'no', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (188, 11, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'no', CAST(110.00 AS Decimal(18, 2)), CAST(110.00 AS Decimal(18, 2)), CAST(105.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (189, 12, 0, N'Builders Work', N'"Supply and install Exhaust hood Cladding as per detail including mosiac tiles  Tiles PC rate @ $6.30 per sqft (Pending availability) (Exhaust hood by other)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (190, 12, 0, N'Builders Work', N'Supply and install 10mm thick tempered glass partition at 3 side of exhaust area', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1100.00 AS Decimal(18, 2)), CAST(1100.00 AS Decimal(18, 2)), CAST(742.50 AS Decimal(18, 2)), CAST(357.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (191, 12, 0, N'Builders Work', N'Supply and install front counter c/w tiles finishing, black stainless steel base inclusive of lavastone and mosiac tiles', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(6100.00 AS Decimal(18, 2)), CAST(6100.00 AS Decimal(18, 2)), CAST(4002.50 AS Decimal(18, 2)), CAST(2097.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (192, 12, 0, N'Builders Work', N'Supply and install 1 set of 10mm acrylic signages (logo and name)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (193, 12, 0, N'Builders Work', N'Supply and install 1 set of acrylic back lit signage (logo and name) at exhaust hood area', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(1700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (194, 12, 0, N'Builders Work', N'"Supply labour and material to construct subframe to suspend 2 nos of TV onto ceiling including TV bracket (TV by others)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (195, 12, 0, N'Builders Work', N'Supply and install POS counter c/w bottom cabinet in laminate finishes and internal white polykem', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1300.00 AS Decimal(18, 2)), CAST(1300.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (196, 12, 0, N'Plumbing', N'Copper water supply  & copper drainage pipe installation work including fees for license plumbing endorsement.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3600.00 AS Decimal(18, 2)), CAST(3600.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), N'"Note: 
Building Consultant fee, if any, to be borned by Client
PE endorsement will be billed directly to client with a 10% P&A
"
', NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (197, 12, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (198, 12, 0, N'Electrical', N'Supply and install S/S/O', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (199, 12, 0, N'Electrical', N'Supply and install T/S/O', CAST(6.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(660.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(460.00 AS Decimal(18, 2)), NULL, NULL)
GO
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (200, 12, 0, N'Electrical', N'Supply and install 32amp 3 phase isolator', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(475.00 AS Decimal(18, 2)), CAST(1425.00 AS Decimal(18, 2)), CAST(1140.00 AS Decimal(18, 2)), CAST(285.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (201, 12, 0, N'Electrical', N'Supply and install lighting point for signage and LED Strip and Kebab Machine', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (202, 12, 0, N'Electrical', N'Supply and install timer for signage', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (203, 12, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (204, 12, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(110.00 AS Decimal(18, 2)), CAST(105.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (205, 13, 0, N'Preliminaries', N'To supply labour for general cleaning work upon completion of renovation work including dismantling of partition', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(431.30 AS Decimal(18, 2)), CAST(568.70 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (206, 13, 0, N'Preliminaries', N'"To supply and install graphics print to hoarding (Design by Client)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1550.00 AS Decimal(18, 2)), CAST(1550.00 AS Decimal(18, 2)), CAST(690.00 AS Decimal(18, 2)), CAST(860.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (207, 13, 0, N'Preliminaries', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(700.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (208, 13, 0, N'Builders Work', N'"Supply and install Exhaust hood Cladding as per detail including mosiac tiles  Tiles PC rate @ $6.30 per sqft (Pending availability) (Exhaust hood by other)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(400.80 AS Decimal(18, 2)), CAST(2099.20 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (209, 13, 0, N'Builders Work', N'Supply and install 10mm thick tempered glass partition at 3 side of exhaust area', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1100.00 AS Decimal(18, 2)), CAST(1100.00 AS Decimal(18, 2)), CAST(840.00 AS Decimal(18, 2)), CAST(260.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (210, 13, 0, N'Builders Work', N'Supply and install front counter c/w tiles finishing, black stainless steel base inclusive of lavastone and mosiac tiles', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(6100.00 AS Decimal(18, 2)), CAST(6100.00 AS Decimal(18, 2)), CAST(4682.50 AS Decimal(18, 2)), CAST(1417.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (211, 13, 0, N'Builders Work', N'Supply and install 1 set of 10mm acrylic signages (logo and name)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (212, 13, 0, N'Builders Work', N'Supply and install 1 set of acrylic back lit signage (logo and name) at exhaust hood area', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(1700.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (213, 13, 0, N'Builders Work', N'"Supply labour and material to construct subframe to suspend 2 nos of TV onto ceiling including TV bracket (TV by others)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (214, 13, 0, N'Builders Work', N'Supply and install POS counter c/w bottom cabinet in laminate finishes and internal white polykem', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1300.00 AS Decimal(18, 2)), CAST(1300.00 AS Decimal(18, 2)), CAST(1100.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (215, 13, 0, N'Plumbing', N'Copper water supply  & copper drainage pipe installation work including fees for license plumbing endorsement.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3600.00 AS Decimal(18, 2)), CAST(3600.00 AS Decimal(18, 2)), CAST(2600.40 AS Decimal(18, 2)), CAST(999.60 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (216, 13, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (217, 13, 0, N'Electrical', N'Supply and install S/S/O', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(210.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (218, 13, 0, N'Electrical', N'Supply and install T/S/O', CAST(6.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(660.00 AS Decimal(18, 2)), CAST(540.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (219, 13, 0, N'Electrical', N'Supply and install 32amp 3 phase isolator', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(475.00 AS Decimal(18, 2)), CAST(1425.00 AS Decimal(18, 2)), CAST(1140.00 AS Decimal(18, 2)), CAST(285.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (220, 13, 0, N'Electrical', N'Supply and install lighting point for signage and LED Strip and Kebab Machine', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (221, 13, 0, N'Electrical', N'Supply and install timer for signage', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (222, 13, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (223, 13, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(110.00 AS Decimal(18, 2)), CAST(105.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (226, 14, 0, N'Preliminaries', N'Supply labour to do general cleaning during renovation and upon completion', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (228, 14, 0, N'Preliminaries', N'Public liability of 1 million coverage and Contractor All Risk', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (229, 14, 0, N'Preliminaries', N'Supply and installation of scaffold, 4000Ht', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (234, 14, 0, N'Demolition and Wet Work', N'Supply labour and material to install wall tiles including chipping of wall to screed up from 1800 to 2700 height', CAST(880.0 AS Decimal(18, 1)), N'lot', CAST(5.50 AS Decimal(18, 2)), CAST(4840.00 AS Decimal(18, 2)), CAST(3802.00 AS Decimal(18, 2)), CAST(1038.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (239, 15, 0, N'Preliminaries', N'Design Consultancy including 3D for MCST submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3500.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(3023.00 AS Decimal(18, 2)), CAST(477.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (224, 14, 0, N'Preliminaries', N'To design and and produce full drawing for construction including M&E planning for HDB approval', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(1980.50 AS Decimal(18, 2)), CAST(519.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (231, 14, 0, N'Demolition and Wet Work', N'Supply and install hollow block wall up to 2700Ht', CAST(885.0 AS Decimal(18, 1)), N'lot', CAST(8.00 AS Decimal(18, 2)), CAST(7080.00 AS Decimal(18, 2)), CAST(6817.00 AS Decimal(18, 2)), CAST(263.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (232, 14, 0, N'Demolition and Wet Work', N'"Supply labour and material to install floor tiles including chipping of floor to receive finishing (Tiles PC Sum: $3.00/sqft)"', CAST(1225.0 AS Decimal(18, 1)), N'lot', CAST(11.00 AS Decimal(18, 2)), CAST(13475.00 AS Decimal(18, 2)), CAST(10657.50 AS Decimal(18, 2)), CAST(2817.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (233, 14, 0, N'Demolition and Wet Work', N'"Supply labour and material to install wall tiles including chipping of wall to receive finishing up to 1800ht (Tiles PC Sum: $3.00/sqft)"', CAST(1775.0 AS Decimal(18, 1)), N'lot', CAST(11.00 AS Decimal(18, 2)), CAST(19525.00 AS Decimal(18, 2)), CAST(18000.50 AS Decimal(18, 2)), CAST(1524.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (236, 14, 0, N'False ceiling Works', N'Supply and install 600 x 1200 x 6mm thick calcium silicate ceiling board', CAST(1225.0 AS Decimal(18, 1)), N'lot', CAST(5.50 AS Decimal(18, 2)), CAST(6737.50 AS Decimal(18, 2)), CAST(6000.00 AS Decimal(18, 2)), CAST(737.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (237, 14, 0, N'Painting Works', N'To supply and apply 2coats of white matex paint including sealant to all wall', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4000.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(3200.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (240, 15, 0, N'Preliminaries', N'Site Supervision and project management', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (241, 15, 0, N'Preliminaries', N'Final Cleaning and Clearing of Debris', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (243, 15, 0, N'Demolition Work and Structural Work', N'Demolish existing parapet wall', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2285.00 AS Decimal(18, 2)), CAST(2285.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(285.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (244, 15, 0, N'Demolition Work and Structural Work', N'"Supply and install new mezzanine work c/w PE calculation and endorsement with 25mm plywood decking. Approximately 1,000 sqft BCA Submission Excluded due to government regulation on mezzanine"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(39000.00 AS Decimal(18, 2)), CAST(39000.00 AS Decimal(18, 2)), CAST(38000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (247, 15, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel, 40kg/m3 rockwool and plastering work to 3000mm Ht Area: Conference Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1725.00 AS Decimal(18, 2)), CAST(1725.00 AS Decimal(18, 2)), CAST(1380.00 AS Decimal(18, 2)), CAST(345.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (249, 15, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel and plastering work to 3000mm Ht Area: Pantry"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(470.00 AS Decimal(18, 2)), CAST(470.00 AS Decimal(18, 2)), CAST(375.00 AS Decimal(18, 2)), CAST(95.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (251, 15, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel and plastering work to 3000mm Ht Area: Stair Case Area"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1575.00 AS Decimal(18, 2)), CAST(1575.00 AS Decimal(18, 2)), CAST(1260.00 AS Decimal(18, 2)), CAST(315.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (252, 15, 0, N'Glazing Work', N'Supply and install 12mm thick tempered glass, 1200mm Ht along glass mezzanine edges c/w necessary structure support', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2080.00 AS Decimal(18, 2)), CAST(2080.00 AS Decimal(18, 2)), CAST(1900.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (253, 15, 0, N'Glazing Work', N'"Supply and install 12mm thick tempered glass along glass 1200mm Ht along mezzanine edges c/w subframe support from ceiling slab 3700L x 2400Ht Area: Conference Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3840.00 AS Decimal(18, 2)), CAST(3840.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(340.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (259, 15, 0, N'Builders Work', N'Supply and install low height standalone cabinet in laminate finishes c/w ABS edging and 4-sides exposed laminate', CAST(6.0 AS Decimal(18, 1)), N'ftrun', CAST(145.00 AS Decimal(18, 2)), CAST(870.00 AS Decimal(18, 2)), CAST(110.00 AS Decimal(18, 2)), CAST(760.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (261, 15, 0, N'Builders Work', N'"Supply and install 2400Ht store cabinet in laminate finishes and ABS edging Area: Stairs Area"', CAST(14.0 AS Decimal(18, 1)), N'ftrun', CAST(320.00 AS Decimal(18, 2)), CAST(4480.00 AS Decimal(18, 2)), CAST(4400.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (262, 15, 0, N'Wall and Floor Finishes', N'Supply labour and material to repaint entire area in emulsion white finishes c/w sealant', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3750.00 AS Decimal(18, 2)), CAST(3750.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (263, 15, 0, N'Wall and Floor Finishes', N'Supply and install selected range wallpaper finishing at selected mezzanine area', CAST(550.0 AS Decimal(18, 1)), N'sqft', CAST(3.50 AS Decimal(18, 2)), CAST(1925.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(425.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (265, 15, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On, Inclusive of sub main cable', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3300.00 AS Decimal(18, 2)), CAST(3300.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (266, 15, 0, N'Electrical', N'Supply and install T/S/O', CAST(18.0 AS Decimal(18, 1)), N'nos', CAST(110.00 AS Decimal(18, 2)), CAST(1980.00 AS Decimal(18, 2)), CAST(1620.00 AS Decimal(18, 2)), CAST(360.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (267, 15, 0, N'Electrical', N'Supply and install lighting point', CAST(32.0 AS Decimal(18, 1)), N'nos', CAST(80.00 AS Decimal(18, 2)), CAST(2560.00 AS Decimal(18, 2)), CAST(2300.00 AS Decimal(18, 2)), CAST(260.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (268, 15, 0, N'Electrical', N'Supply and install Telephone Line', CAST(15.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1475.00 AS Decimal(18, 2)), CAST(25.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (270, 15, 0, N'Electrical', N'Supply labour and material to install light fitting', CAST(32.0 AS Decimal(18, 1)), N'nos', CAST(15.00 AS Decimal(18, 2)), CAST(480.00 AS Decimal(18, 2)), CAST(320.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (271, 15, 0, N'Fire Protection Work', N'Design and build fire sprinkler work (Water discharge fees will be borne by client)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5900.00 AS Decimal(18, 2)), CAST(5900.00 AS Decimal(18, 2)), CAST(5700.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (273, 16, 0, N'Preliminaries', N'Design Adapation including 3D for submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (274, 16, 0, N'Preliminaries', N'Final cleaning of outlet', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(400.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (225, 14, 0, N'Preliminaries', N'Supply labour and material to protect existing mall flooring and common properties', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (227, 14, 0, N'Preliminaries', N'Project Management Fees', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3500.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (230, 14, 0, N'Demolition and Wet Work', N'Demolition of existing tiles in toilet area including removal of toilet fitting', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1550.00 AS Decimal(18, 2)), CAST(1550.00 AS Decimal(18, 2)), CAST(1300.00 AS Decimal(18, 2)), CAST(250.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (235, 14, 0, N'Carpentry', N'To supply and install single leaf laminate door including door frame and iron mongery', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(3600.00 AS Decimal(18, 2)), CAST(3050.00 AS Decimal(18, 2)), CAST(550.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (238, 15, 0, N'Preliminaries', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (242, 15, 0, N'Authority Submission', N'Building Plan Submission ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3600.00 AS Decimal(18, 2)), CAST(3600.00 AS Decimal(18, 2)), CAST(3300.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (245, 15, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel, 40kg/m3 rockwool and plastering work up to 3000mm Ht Area: CEO Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1525.00 AS Decimal(18, 2)), CAST(1525.00 AS Decimal(18, 2)), CAST(1420.00 AS Decimal(18, 2)), CAST(105.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (246, 15, 0, N'Partition and Ceiling Work', N'"Supply and install 9mm thick gypsum ceiling c/w 40kg rockwool and plastering work Area: CEO Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(440.00 AS Decimal(18, 2)), CAST(440.00 AS Decimal(18, 2)), CAST(352.00 AS Decimal(18, 2)), CAST(88.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (248, 15, 0, N'Partition and Ceiling Work', N'"Supply and install 9mm thick gypsum ceiling c/w 40kg rockwool and plastering work Area: Conference Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1150.00 AS Decimal(18, 2)), CAST(1150.00 AS Decimal(18, 2)), CAST(920.00 AS Decimal(18, 2)), CAST(230.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (250, 15, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel and plastering work to 3000mm Ht Area: Reception Area"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(470.00 AS Decimal(18, 2)), CAST(470.00 AS Decimal(18, 2)), CAST(375.00 AS Decimal(18, 2)), CAST(95.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (254, 15, 0, N'Glazing Work', N'"Supply and install 10mm thick tempered glass screen  1200Ht x 1000L Area: CEO Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(180.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(130.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (255, 15, 0, N'Builders Work', N'Supply and install top hung and bottom pantry cabinet in laminate finishes c/w soft close hinge and solid surface top', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4200.00 AS Decimal(18, 2)), CAST(4200.00 AS Decimal(18, 2)), CAST(4060.00 AS Decimal(18, 2)), CAST(140.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (256, 15, 0, N'Builders Work', N'"Supply and install 40mm thick, 4 -seaters high bench in laminate finishes and ABS edging Area: Outside Conference room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(570.00 AS Decimal(18, 2)), CAST(570.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (257, 15, 0, N'Builders Work', N'"Supply and install glass writing wall c/w stainless marker tray 1500Ht x 1200L"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(875.00 AS Decimal(18, 2)), CAST(875.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(175.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (258, 15, 0, N'Builders Work', N'Supply and install low height standalone cabinet in laminate finishes c/w ABS edging and 5-sides exposed laminate', CAST(27.0 AS Decimal(18, 1)), N'ftrun', CAST(175.00 AS Decimal(18, 2)), CAST(4725.00 AS Decimal(18, 2)), CAST(4500.00 AS Decimal(18, 2)), CAST(225.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (260, 15, 0, N'Builders Work', N'"Supply and install 2400Ht filing cabinet in laminate finishes and ABS edging Area: Filing Room, Level 7 main office area"', CAST(35.0 AS Decimal(18, 1)), N'ftrun', CAST(320.00 AS Decimal(18, 2)), CAST(11200.00 AS Decimal(18, 2)), CAST(11000.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (264, 15, 0, N'Wall and Floor Finishes', N'Supply and install selected range 600 by 600 carpet tiles onto both mezzanine and Level 7', CAST(2280.0 AS Decimal(18, 1)), N'sqft', CAST(3.50 AS Decimal(18, 2)), CAST(7980.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(980.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (269, 15, 0, N'Electrical', N'Supply and install Data Line', CAST(15.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1275.00 AS Decimal(18, 2)), CAST(225.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (272, 16, 0, N'Preliminaries', N'Public Liability Insurance and Contractor All Risk ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(800.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(700.50 AS Decimal(18, 2)), CAST(99.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (275, 16, 0, N'Builders Work', N'Supply and install Shop frontage and side wall in pure black solid surface top 100mm drop 40mm, graphics diamond print and pure black solid surface skirting 100mm turn in 20mm including cowboy/flip door and LED Strip', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(7450.00 AS Decimal(18, 2)), CAST(7450.00 AS Decimal(18, 2)), CAST(2430.00 AS Decimal(18, 2)), CAST(5020.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (278, 16, 0, N'Builders Work', N'Supply and install 1500mm wide POS counter for Pezzo c/w based for drink display', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1800.00 AS Decimal(18, 2)), CAST(1800.00 AS Decimal(18, 2)), CAST(1300.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (280, 16, 0, N'Builders Work', N'Supply and install double side light box with internal leD, front and back printed rainbow strips direct paste on and 3D mini acrylic lighted texts install at both front and back', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3800.00 AS Decimal(18, 2)), CAST(3800.00 AS Decimal(18, 2)), CAST(2980.00 AS Decimal(18, 2)), CAST(820.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (281, 16, 0, N'Builders Work', N'"Supply and install 12mm thick, 1550 ht tempered glass at escalator side c/w 2 fins to the existing metal frame. 600L,6150L,600L x 1550Ht"', CAST(136.0 AS Decimal(18, 1)), N'lot', CAST(15.00 AS Decimal(18, 2)), CAST(2040.00 AS Decimal(18, 2)), CAST(1632.00 AS Decimal(18, 2)), CAST(408.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (282, 16, 0, N'Builders Work', N'Supply and install matt black laminate frame work onto back glass', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (285, 16, 0, N'Electrical', N'Supply and install S/S/O', CAST(4.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(280.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (286, 16, 0, N'Electrical', N'Supply and install T/S/O', CAST(5.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(550.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (287, 16, 0, N'Electrical', N'Supply and install 15amp S/S/O', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(150.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (290, 16, 0, N'Electrical', N'Supply and install DP swtich for TV and Signages', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(65.00 AS Decimal(18, 2)), CAST(130.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(30.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (276, 16, 0, N'Builders Work', N'Supply and install back wall, 1000mm height c/w black solid surface', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2950.00 AS Decimal(18, 2)), CAST(2950.00 AS Decimal(18, 2)), CAST(2357.50 AS Decimal(18, 2)), CAST(592.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (277, 16, 0, N'Builders Work', N'Supply and install stainless steel back for back wall, 6150 L x 1000mm height including cutting of profile for M&E services.', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(300.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (279, 16, 0, N'Builders Work', N'Supply wall mount TV Bracket including installation of 3 x 43" TV onto existing framework', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(60.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (283, 16, 0, N'Plumbing', N'Supply and install PPR water inlet and PVC outlet point including fees for license plumbing endorsement as per drawing', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1700.00 AS Decimal(18, 2)), CAST(1700.00 AS Decimal(18, 2)), CAST(1300.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (284, 16, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (288, 16, 0, N'Electrical', N'Supply and install 20amp 3 phase isolator', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(320.00 AS Decimal(18, 2)), CAST(640.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(140.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (289, 16, 0, N'Electrical', N'Supply and install lighting point for signage and carpentry', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(240.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (291, 16, 0, N'Electrical', N'Supply and install timer for signage', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (292, 16, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (294, 17, 0, N'Preliminaries', N'To supply labour for general cleaning work upon completion of renovation work.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1500.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1300.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (296, 17, 0, N'Preliminaries', N'Design Fees for Submission to necessary authorities for permit to work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1800.00 AS Decimal(18, 2)), CAST(1800.00 AS Decimal(18, 2)), CAST(1500.18 AS Decimal(18, 2)), CAST(299.82 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (297, 17, 0, N'Authority Submission', N'Building Plan design and submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(7800.00 AS Decimal(18, 2)), CAST(7800.00 AS Decimal(18, 2)), CAST(6900.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (299, 17, 0, N'Wet Trade', N'Supply and install cement screed flooring for shop front.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4200.00 AS Decimal(18, 2)), CAST(4200.00 AS Decimal(18, 2)), CAST(3800.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (300, 17, 0, N'Wet Trade', N'"Supply and install floor tiles for new kitchen area including 10% wastage (PC rate: $3.50/sqft tiles)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2300.00 AS Decimal(18, 2)), CAST(2300.00 AS Decimal(18, 2)), CAST(2025.20 AS Decimal(18, 2)), CAST(274.80 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (301, 17, 0, N'Painting', N'Supply labour and material to paint exposed wall and ceiling in matex white finishing', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1600.00 AS Decimal(18, 2)), CAST(1600.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (303, 17, 0, N'Carpentry', N'Supply and install T.V bracket. (T.V to be provided by Owner)', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(200.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(375.00 AS Decimal(18, 2)), CAST(225.00 AS Decimal(18, 2)), NULL, NULL)
GO
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (304, 17, 0, N'Carpentry', N'Supply and install shop counter low height wooden partition in selected laminated finishing c/w solid surface top and swing door.', CAST(34.0 AS Decimal(18, 1)), N'run', CAST(180.00 AS Decimal(18, 2)), CAST(6120.00 AS Decimal(18, 2)), CAST(4590.00 AS Decimal(18, 2)), CAST(1530.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (306, 17, 0, N'Carpentry', N'Supply and install low ht. storage cabinet in laminated finishing.', CAST(7.0 AS Decimal(18, 1)), N'ftrun', CAST(182.14 AS Decimal(18, 2)), CAST(1274.98 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(274.98 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (308, 17, 0, N'Carpentry', N'Supply and install open display/storage cabinet in laminated finishing up to 2400mmHt', CAST(7.0 AS Decimal(18, 1)), N'ftrun', CAST(321.43 AS Decimal(18, 2)), CAST(2250.01 AS Decimal(18, 2)), CAST(2050.00 AS Decimal(18, 2)), CAST(200.01 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (310, 17, 0, N'Plumbing', N'Supply labour to dismantle existing sink and reconnect back', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(200.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (312, 17, 0, N'ACMV', N'To supply and install 3500cmh exhaust fan (Hood by Others)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4000.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(3200.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (313, 17, 0, N'ACMV', N'To supply and install 2500 cmh air cleaner', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3125.00 AS Decimal(18, 2)), CAST(3125.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(625.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (316, 18, 0, N'3D Rendering Work', N'Shop drawing for', CAST(18.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(1800.00 AS Decimal(18, 2)), CAST(1660.00 AS Decimal(18, 2)), CAST(140.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (318, 18, 0, N'Joinery Works', N'Item 1: Supply and install Reception Feature wall in laminate finishes', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(2800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (319, 18, 0, N'Joinery Works', N'Item 14: Supply and install PALACE signage logo c/w LED Strip', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3900.00 AS Decimal(18, 2)), CAST(3900.00 AS Decimal(18, 2)), CAST(3700.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (321, 18, 0, N'Joinery Works', N'Item 3: Supply and install dressing table c/w mirror in laminate finishes 7.5ft long', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1350.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (322, 18, 0, N'Joinery Works', N'Item 4: Supply and install Singer''s cabinet  in laminate finishes 11ft long', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3520.00 AS Decimal(18, 2)), CAST(3520.00 AS Decimal(18, 2)), CAST(2750.00 AS Decimal(18, 2)), CAST(770.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (323, 18, 0, N'Joinery Works', N'Item 5: Supply and install TV Console in laminate finishes 8ft long', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1440.00 AS Decimal(18, 2)), CAST(1440.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(640.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (324, 18, 0, N'Joinery Works', N'Item 6: Supply and install TV Console in laminate finishes', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(750.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1490.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (327, 18, 0, N'Joinery Works', N'Item 9: Supply and install box up to existing inlet and outlet pipe', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (328, 18, 0, N'Joinery Works', N'Item 10: Supply and install TV feature wall in selected laminate finishes', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2750.00 AS Decimal(18, 2)), CAST(2750.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (329, 18, 0, N'Joinery Works', N'Item 11: Supply and install mini bar counter in selected laminate finishes', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1050.00 AS Decimal(18, 2)), CAST(1050.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (330, 18, 0, N'Joinery Works', N'Item 12: Supply and install wooden platform in laminate finishes', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(700.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (293, 16, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(110.00 AS Decimal(18, 2)), CAST(105.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (298, 17, 0, N'Authority Submission', N'Plan fee reimbursement x 3 nos', CAST(3.0 AS Decimal(18, 1)), N'nos', CAST(90.00 AS Decimal(18, 2)), CAST(270.00 AS Decimal(18, 2)), CAST(272.79 AS Decimal(18, 2)), CAST(-2.79 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (302, 17, 0, N'Painting', N'Supply labour and material to paint matt/gloss lacquer over cement screed flooring', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(900.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (305, 17, 0, N'Carpentry', N'Supply and install POS counter in selected laminated finish c/w solid surface top.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1100.00 AS Decimal(18, 2)), CAST(1100.00 AS Decimal(18, 2)), CAST(910.00 AS Decimal(18, 2)), CAST(190.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (307, 17, 0, N'Carpentry', N'Supply and install top hung display/storage cabinet in laminated finishing up to 900mmHt', CAST(26.0 AS Decimal(18, 1)), N'ftrun', CAST(173.27 AS Decimal(18, 2)), CAST(4505.02 AS Decimal(18, 2)), CAST(4180.00 AS Decimal(18, 2)), CAST(325.02 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (311, 17, 0, N'ACMV', N'Supply labour and material to construct exhaust duct out of unit ending at the existing ducting point', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (314, 17, 0, N'Gas and Fire Supression Work', N'Supply and install 2.5gallon wet chemical system to exhaust hood est size (1200 x 750 x 500ht) up to max 8 nozzle complete with "PSB" and piping work accessories', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3850.00 AS Decimal(18, 2)), CAST(3850.00 AS Decimal(18, 2)), CAST(2950.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (315, 17, 0, N'Glass', N'Supply and install 12mm thickness clear tempered glass panel c/w 2 swing door with floor spring and selected range iron mongeries and lock set', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5050.00 AS Decimal(18, 2)), CAST(5050.00 AS Decimal(18, 2)), CAST(4040.00 AS Decimal(18, 2)), CAST(1010.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (317, 18, 0, N'3D Rendering Work', N'Design and 3D for the following carpentry', CAST(11.0 AS Decimal(18, 1)), N'lot', CAST(200.00 AS Decimal(18, 2)), CAST(2200.00 AS Decimal(18, 2)), CAST(1650.00 AS Decimal(18, 2)), CAST(550.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (320, 18, 0, N'Joinery Works', N'Item 2: Supply and install L-shape DJ console in laminate finishes 12ft long', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3240.00 AS Decimal(18, 2)), CAST(3240.00 AS Decimal(18, 2)), CAST(2300.00 AS Decimal(18, 2)), CAST(940.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (325, 18, 0, N'Joinery Works', N'Item 7: Supply and install Vanity Top in laminate finishes and solid surface finishes including mirror excluding sink and tap', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(2150.00 AS Decimal(18, 2)), CAST(850.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (331, 18, 0, N'Joinery Works', N'Item 13: Supply and install shopfront signage', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(7000.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(6700.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (334, 18, 0, N'Joinery Works', N'Double Leaf Door', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (337, 18, 0, N'Gas Work', N'Supply labour and material to alter gas pipe including necessary gas leak detection system and solenoid valve', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(15000.00 AS Decimal(18, 2)), CAST(15000.00 AS Decimal(18, 2)), CAST(13000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (338, 19, 0, N'Preliminaries', N'Waa', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (339, 19, 0, N'Authority Submission', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (340, 19, 0, N'Partitioning and Ceiling Work', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (341, 19, 0, N'Flooring Work', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (342, 19, 0, N'Painting Work', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (343, 19, 0, N'Mill Work', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (344, 19, 0, N'Glazing Work', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (345, 19, 0, N'Electrical', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (346, 19, 0, N'Plumbing & Sanitory', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (347, 19, 0, N'ACMV Work', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (348, 19, 0, N'Fire Protection Work', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (349, 19, 0, N'Miscellaneous', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (350, 20, 0, N'Preliminaries', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (351, 20, 0, N'Authority Submission', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (352, 20, 0, N'Partitioning and Ceiling Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (353, 20, 0, N'Flooring Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (354, 20, 0, N'Painting Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (355, 20, 0, N'Mill Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (356, 20, 0, N'Glazing Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (357, 20, 0, N'Electrical', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (358, 20, 0, N'Plumbing & Sanitory', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (359, 20, 0, N'ACMV Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (360, 20, 0, N'Fire Protection Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (361, 20, 0, N'Miscellaneous', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (295, 17, 0, N'Preliminaries', N'Provision of Site Supervisor to supervise site progress and to liaise with direct supplier, i.e Kitchen Specialise, CCTV, Furniture etc', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1700.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (309, 17, 0, N'Plumbing', N'Supply and install PPR inlet and PVC drainage pipe. Installation work including fees for license plumbing endorsement and water turn on fees', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3500.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(3101.84 AS Decimal(18, 2)), CAST(398.16 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (326, 18, 0, N'Joinery Works', N'Item 8: Supply and install POS counter in selected laminate finishes', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(900.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (333, 18, 0, N'Joinery Works', N'Single Leaf Door', CAST(4.0 AS Decimal(18, 1)), N'lot', CAST(750.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(2700.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (335, 18, 0, N'Joinery Works', N'Supply and install Sliding Door', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (332, 18, 0, N'Joinery Works', N'Item 18: Supply and install Coat Hangar Cabinet 9 ft long in laminate finishes', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3500.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(3250.00 AS Decimal(18, 2)), CAST(250.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (336, 18, 0, N'Joinery Works', N'Supply and install Louver Door', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1600.00 AS Decimal(18, 2)), CAST(1600.00 AS Decimal(18, 2)), CAST(1300.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
SET IDENTITY_INSERT [dbo].[QuotationItemDescriptions] OFF
SET IDENTITY_INSERT [dbo].[Quotations] ON 

INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (1, N'Q1700001', N'NODC Pte Ltd', N'Yes', 1, N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'65 9192 9394', N'65 9192 9393', 2, NULL, 6, CAST(N'2017-04-27 00:00:00.0000000' AS DateTime2), N'UserGuide_170427174953.pdf', N'Colins''s Quotation on 27/04/2017', NULL, N'Confirmed', 0, 0, 1, CAST(N'2017-04-27 17:40:01.3117595' AS DateTime2), CAST(N'2017-04-27 17:49:59.1379744' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (2, N'Q1700002', N'NODC Pte Ltd', N'Yes', 2, N'Elvin Ong', N'81 UBI AVENUE 4, #07-06, Singapore 408830', N'+658029579', N'+658029579', 1, NULL, 5, CAST(N'2017-04-27 00:00:00.0000000' AS DateTime2), N'UserGuide_170427175744.pdf', N'Bobby''s Quotation on 27/04/2017', NULL, N'Pending Customer Confirmation', 0, 0, 2, CAST(N'2017-04-27 17:56:38.2103939' AS DateTime2), CAST(N'2017-04-27 17:57:49.6724055' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (3, N'Q1700003', N'NODC Pte Ltd', N'Yes', 2, N'Elvin Ong', N'81 UBI AVENUE 4, #07-06, Singapore 408830', N'+658029579', N'+658029579', 2, NULL, 3, CAST(N'2017-04-27 00:00:00.0000000' AS DateTime2), N'UserGuide_170427175854.pdf', N'Aaron''s Quotation on 27/04/2017', NULL, N'Confirmed', 0, 0, 3, CAST(N'2017-04-27 18:01:10.9952190' AS DateTime2), CAST(N'2017-04-27 18:02:03.7281821' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (4, N'Q1700004', N'NODC Pte Ltd', N'Yes', 4, N'MR CHRISTIAN FRANK', N'107 TUAS SOUTH AVE 8 OFFSHORE MARINE CENTRE SINGAPORE 637036', N'67055800', N'67948345', 8, NULL, 8, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_16-0005_-_Menck_(Dir_Head)_170720150622.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 4, CAST(N'2017-07-20 15:12:43.9189961' AS DateTime2), CAST(N'2017-07-20 15:12:44.1217961' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (5, N'Q1700005', N'NODC Pte Ltd', N'Yes', 3, N'Slone Lim', N'50 East Coast Road, #01-110 , #01-134 Roxy Square, Singapore 428769', N'9222 3605', NULL, 12, NULL, 12, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0054_-_The_Art_Haus_(Snr_Mgr_1.1)_170720151528.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 5, CAST(N'2017-07-20 15:36:23.1913961' AS DateTime2), CAST(N'2017-07-20 15:36:23.3629961' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (6, N'Q1700006', N'NODC Pte Ltd', N'Yes', 5, N'Ms Haze Tan', N'1 Ubi View #03-10, Focus One Singapore 408555', N'63537767', NULL, 10, NULL, 10, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_16-0008_-_AceCom_Technologies_(Director_1)_170720152158.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 6, CAST(N'2017-07-20 15:41:12.1189961' AS DateTime2), CAST(N'2017-07-20 15:41:12.1969961' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (7, N'Q1700007', N'NODC Pte Ltd', N'Yes', 6, N'Walter Tan', N'17 Kaki Bukit Crescent #02-02 Kaki Bukit Techpark 1, Singapore 416248', N'9819 6468', NULL, 12, NULL, 12, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0055_-_Carpenter_and_Cook_Lor_Kilat_(Snr_Mgr_1.1)_170720154634.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 7, CAST(N'2017-07-20 15:54:17.5165961' AS DateTime2), CAST(N'2017-07-20 15:54:17.5789961' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (8, N'Q1700008', N'NODC Pte Ltd', N'Yes', 7, N' Mr Hendra Chong, Miss Trixy Tan', N'114 Lavender Street CT Hub 2, #08-53 Singapore 338729	', N'87820766', NULL, 9, NULL, 9, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0015_-_Now_Comms_(Snr_Mgr_3)_170720160126.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 8, CAST(N'2017-07-20 16:06:02.2465961' AS DateTime2), CAST(N'2017-07-20 16:06:02.2933961' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (9, N'Q1700009', N'NODC Pte Ltd', N'Yes', 8, N'Attn: Mr Walter Tan', N'17 Kaki Bukit Crescent #02-02 Kaki Bukit Techpark 1 Singapore 416248', N'98196468', NULL, 14, NULL, 14, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0052_-_Liv_Activ_@_CCP_(Snr_Mgr_1.3)_170720161627.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 9, CAST(N'2017-07-20 16:30:52.2181961' AS DateTime2), CAST(N'2017-07-20 16:30:52.2961961' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (11, N'Q1700011', N'NODC Pte Ltd', N'Yes', 10, N'Attn: Miss Tay Ying Hui', N'81 Ubi Ave 4 #07-06 Singapore 408830', N'94554784', NULL, 12, NULL, 12, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0038_-_My_Banh_Mi_at_TPC_(Snr_Mgr_1.1)_170720165129.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 11, CAST(N'2017-07-20 17:07:27.2473961' AS DateTime2), CAST(N'2017-07-20 17:07:27.3253961' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (14, N'Q1700014', N'NODC Pte Ltd', N'Yes', 13, N'Alvin', N'1006 Aljunied Ave 5', N'9675 9990', NULL, 15, NULL, 15, CAST(N'2017-07-21 00:00:00.0000000' AS DateTime2), N'NODC_17-0014_-_Orange_Lantern_Central_Kitchen_(Snr_Mgr_2.1)_170721164953.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 14, CAST(N'2017-07-21 16:58:29.6057136' AS DateTime2), CAST(N'2017-07-21 16:58:29.6681136' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (15, N'Q1700015', N'NODC Pte Ltd', N'Yes', 12, N'Miss Swathi Mathur', N'21 Media Circle #05-06, Infinite Studios Singapore 138562', N'6589 8744', NULL, 11, NULL, 11, CAST(N'2017-07-21 00:00:00.0000000' AS DateTime2), N'NODC_17-004_-_24_@_Henderson_-_(Director_2)_170721165745.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 15, CAST(N'2017-07-21 17:18:13.5209136' AS DateTime2), CAST(N'2017-07-21 17:18:13.5365136' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (16, N'Q1700016', N'NODC Pte Ltd', N'Yes', 14, N'Chiang Zhan Yi', N'51 Imbiah Road, Singapore 099702', N'9640 7401', NULL, 15, NULL, 15, CAST(N'2017-07-21 00:00:00.0000000' AS DateTime2), N'NODC_17-0059_-_Pezzo_@_Sembawang_(Snr_Mgr_2.1)_170721170946.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 16, CAST(N'2017-07-21 17:20:47.1653136' AS DateTime2), CAST(N'2017-07-21 17:20:47.1965136' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (17, N'Q1700017', N'NODC Pte Ltd', N'Yes', 15, N'Kelly Bok', N'216 Boon Lay Avenue	 #01-01 Singapore 640216	', N'9631 7106', NULL, 17, NULL, 17, CAST(N'2017-07-21 00:00:00.0000000' AS DateTime2), N'NODC17-0058__-_Box_Kitchen_(Snr_Mgr_2.3)_170721172622.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 17, CAST(N'2017-07-21 17:39:20.9585136' AS DateTime2), CAST(N'2017-07-21 17:39:20.9897136' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (10, N'Q1700010', N'NODC Pte Ltd', N'Yes', 9, N'Jacky Lim', N'224 Westwood Ave #09-15 Singapore 648366', N'9383 5543', NULL, 13, NULL, 13, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0044_-_DF_Academy_(Snr_Mgr_1.2)_170720162735.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 10, CAST(N'2017-07-20 17:05:06.8629961' AS DateTime2), CAST(N'2017-07-20 17:05:06.9877961' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (12, N'Q1700004', N'NODC Pte Ltd', N'Yes', 4, N'Mr Jake Chia ', NULL, NULL, NULL, 7, NULL, 21, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_16-0007_-_Grains_Pte_Ltd_(Snr_Mgr_2.1)_170720104126.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Customer Confirmation ( CUST 1.1.2.1.1 )', 0, 0, 12, CAST(N'2017-07-20 17:35:32.7961961' AS DateTime2), CAST(N'2017-07-26 17:48:41.0340000' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, 7, NULL, NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Draft', 0, 0, 19, CAST(N'2017-07-25 17:08:09.6479132' AS DateTime2), CAST(N'2017-07-25 17:08:09.6791132' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (13, N'Q1700013', N'NODC Pte Ltd', N'Yes', 11, N'Jake Chia', N'703 Ang Mo Kio Ave 5#04-40 Singapore 569880 Northstar@AMK', N'9144 6336', NULL, 15, NULL, 15, CAST(N'2017-07-21 00:00:00.0000000' AS DateTime2), N'NODC_16-0007_-_Grains_Pte_Ltd_(Snr_Mgr_2.1)_170721154037.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 13, CAST(N'2017-07-21 15:50:50.5793136' AS DateTime2), CAST(N'2017-07-21 15:50:50.8289136' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (18, N'Q1700018', N'NODC Pte Ltd', N'Yes', 16, N'Michael', N'1 Jalan Anak Bukit #B1-52, Singapore 588996', N'9636 2377', NULL, 16, NULL, 16, CAST(N'2017-07-21 00:00:00.0000000' AS DateTime2), N'NODC_17-0061R1_-_212_Desert_Loft_(Snr_Mgr_2.2)_170721173850.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 18, CAST(N'2017-07-21 17:48:19.7825136' AS DateTime2), CAST(N'2017-07-21 17:48:19.7981136' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (20, N'Q1700019', N'NODC Pte Ltd', N'Yes', 4, N'MR CHRISTIAN FRANK', N'107 TUAS SOUTH AVE 8 OFFSHORE MARINE CENTRE SINGAPORE 637036', N'67055800', N'67948345', 8, NULL, 9, CAST(N'2017-07-26 00:00:00.0000000' AS DateTime2), NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Tay Ying Yi )', 0, 0, 20, CAST(N'2017-07-26 10:07:45.0241849' AS DateTime2), CAST(N'2017-07-26 10:07:45.0709849' AS DateTime2), N'N')
SET IDENTITY_INSERT [dbo].[Quotations] OFF
SET IDENTITY_INSERT [dbo].[SystemSettings] ON 

INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (1, N'COMPANY_NAME', N'NODC Pte Ltd', N'string', N'Y', CAST(N'2017-07-20 10:51:09.8197961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (2, N'COMPANY_ADDRESS', N'123', N'string', N'Y', CAST(N'2017-07-20 10:51:09.9133961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (3, N'COMPANY_POSTAL_CODE', N'12345', N'string', N'Y', CAST(N'2017-07-20 10:51:09.9289961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (4, N'DEFAULT_EMAIL', N'email@gmail.com', N'string', N'Y', CAST(N'2017-07-20 10:51:09.9289961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (5, N'MAIN_CONTACT_PERSON', N'lim', N'string', N'N', CAST(N'2017-07-20 10:51:09.9289961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (6, N'MAIN_CONTACT_NO', N'123', N'string', N'N', CAST(N'2017-07-20 10:51:09.9289961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (7, N'PREFIX_INVOICE_ID', N'INV', N'string', N'N', CAST(N'2017-07-20 10:51:09.9289961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (8, N'PREFIX_QUOTATION_ID', N'Q', N'string', N'N', CAST(N'2017-07-20 10:51:09.9289961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (9, N'PREFIX_PROJECT_ID', N'P', N'string', N'N', CAST(N'2017-07-20 10:51:09.9445961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (10, N'PREFIX_CUSTOMER_ID', N'C', N'string', N'N', CAST(N'2017-07-20 10:51:09.9445961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (11, N'PREFIX_USER_ID', N'U', N'string', N'N', CAST(N'2017-07-20 10:51:09.9601961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (12, N'PREFIX_PAYOUT_ID', N'P', N'string', N'N', CAST(N'2017-07-20 10:51:09.9601961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (13, N'PREFIX_EARLY_PAYOUT_ID', N'E', N'string', N'N', CAST(N'2017-07-20 10:51:09.9601961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (14, N'TERM_AND_CONDITION', N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'string', N'N', CAST(N'2017-07-20 10:51:09.9601961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (15, N'PERCENTAGE_HIGHLIGH_RED', N'17', N'string', N'Y', CAST(N'2017-07-20 10:51:09.9757961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (16, N'GST', N'7', N'string', N'Y', CAST(N'2017-07-20 10:51:09.9757961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (17, N'AGENCY_LIST', N'Propnex|ERA', N'string', N'Y', CAST(N'2017-07-20 10:51:09.9757961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (18, N'QUOTATION_CATEGORIES', N'Preliminaries|Authority Submission|Partitioning and Ceiling Work|Flooring Work|Painting Work|Mill Work|Glazing Work|Electrical|Plumbing & Sanitory|ACMV Work|Fire Protection Work|Miscellaneous', N'string', N'Y', CAST(N'2017-07-20 10:51:09.9757961' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (19, N'LIST_OF_COMPANY', N'NODC Pte Ltd|Yes|81 Ubi Avenue 4 UB.One #07-06 Singapore 408830
Tel: +65 6802 9579 Email: admin@kspm.com.sg
Co & GST Reg No: 201500075E;No 1 Design Consultancy Pte Ltd|Yes|81 Ubi Avenue 4 UB.One #07-06 Singapore 408830
Tel: +65 6802 9579 Email: admin@kspm.com.sg
Co & GST Reg No: 201500075E', N'string', N'Y', CAST(N'2017-07-20 10:51:09.9913961' AS DateTime2))
SET IDENTITY_INSERT [dbo].[SystemSettings] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (1, N'U1700001', N'hongguan', N'Rn5sE6W5YkI=', N'Lim Hong Guan', N'46 Tagore Lane, 787900 Singapore', N'hongguan@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'Yes', 1, N'Super Admin', NULL, CAST(1000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-04-27 00:00:00.0000000' AS DateTime2), CAST(N'2017-05-02 17:54:08.0213220' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (2, N'U1700002', N'erntay2', N'Rn5sE6W5YkI=', N'Stephen Tay Chan Ern', N'46 Tagore Lane, 787900 Singapore', N'chanern@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'Yes', 1, N'Super Admin', NULL, CAST(1000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-04-27 16:52:45.6498767' AS DateTime2), CAST(N'2017-04-27 16:52:45.6498767' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (3, N'U1700003', N'aaron', N'Rn5sE6W5YkI=', N'Aaron', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'aaron@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'No', 1, N'Sales', N'Director', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-04-27 17:21:03.7094309' AS DateTime2), CAST(N'2017-04-27 17:21:03.7094309' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (4, N'U1700004', N'accounts', N'Rn5sE6W5YkI=', N'accounts', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'accounts@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'No', 1, N'Accounts', NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-04-27 17:17:21.3312901' AS DateTime2), CAST(N'2017-04-27 17:17:21.3312901' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (5, N'U1700005', N'bobby', N'Rn5sE6W5YkI=', N'Bobby', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'bobby@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'No', 3, N'Sales', N'Senior Manger', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-04-27 17:21:50.1600752' AS DateTime2), CAST(N'2017-04-27 17:21:50.1600752' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (6, N'U1700006', N'colins', N'Rn5sE6W5YkI=', N'Colins', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'colins@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'No', 5, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-04-27 17:22:20.9107281' AS DateTime2), CAST(N'2017-04-27 17:22:20.9107281' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (7, N'U1700007', N'elvin', N'Rn5sE6W5YkI=', N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'elvinong@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'Yes', 1, N'Super Admin', NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-05-02 17:51:41.9960503' AS DateTime2), CAST(N'2017-05-02 17:51:41.9960503' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (8, N'U1700008', N'DirectorH', N'Rn5sE6W5YkI=', N'Tay Ying Yi', NULL, N'davis.tay@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'Yes', 1, N'Sales', N'Director', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 14:19:49.0069961' AS DateTime2), CAST(N'2017-07-25 17:12:04.8023132' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (9, N'U1700009', N'SM3', N'phWjm/FyrHc=', N'Loh Chiat Min', NULL, N'chiatmn.loh@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 8, N'Sales', N'Senior Manger', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 14:22:55.1773961' AS DateTime2), CAST(N'2017-07-20 14:23:11.3233961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (10, N'U1700010', N'Director1', N'phWjm/FyrHc=', N'Director1', NULL, N'000@nodc.com', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 8, N'Sales', N'Director', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 14:24:30.7897961' AS DateTime2), CAST(N'2017-07-20 14:24:30.7897961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (11, N'U1700011', N'Director2', N'phWjm/FyrHc=', N'Director2', NULL, N'Director2@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'Yes', 8, N'Sales', N'Director', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 14:25:37.6201961' AS DateTime2), CAST(N'2017-07-25 17:13:57.8711132' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (12, N'U1700012', N'SM11', N'phWjm/FyrHc=', N'SNR MGR 1.1', NULL, N'SM1.1@nosc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 10, N'Sales', N'Senior Manger', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 14:27:25.7905961' AS DateTime2), CAST(N'2017-07-20 14:28:01.3273961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (13, N'U1700013', N'SM12', N'phWjm/FyrHc=', N'SNR MGR 1.2', NULL, N'SNRMGR@NODC.COM', N'0000', NULL, NULL, NULL, NULL, N'No', N'No', 10, N'Sales', N'Senior Manger', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 14:29:17.4241961' AS DateTime2), CAST(N'2017-07-20 14:29:17.4241961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (14, N'U1700014', N'SM13', N'phWjm/FyrHc=', N'SNR MGR 1.3', NULL, N'SM13@NODC.COM.DG', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 10, N'Sales', N'Senior Manger', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 14:42:03.1033961' AS DateTime2), CAST(N'2017-07-20 14:42:03.1033961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (16, N'U1700016', N'SM22', N'phWjm/FyrHc=', N'SNR MGR 2.2', NULL, N'SNRMGR3@NODC.COM', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 11, N'Sales', N'Senior Manger', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:28:39.7549961' AS DateTime2), CAST(N'2017-07-20 17:28:39.7549961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (17, N'U1700017', N'SM23', N'phWjm/FyrHc=', N'SNR MGR 2.3', NULL, N'SNRMGR4@NODC.COM', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 11, N'Sales', N'Senior Manger', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:29:50.9377961' AS DateTime2), CAST(N'2017-07-20 17:29:50.9377961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (18, N'U1700018', N'PROF111', N'phWjm/FyrHc=', N'PropNex 1.1.1', NULL, N'propnex@nods.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 12, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:31:50.6053961' AS DateTime2), CAST(N'2017-07-20 17:31:50.6053961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (19, N'U1700019', N'PROF112', N'phWjm/FyrHc=', N'PropNex 1.1.2', NULL, N'propnex1@nods.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 12, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:32:55.4233961' AS DateTime2), CAST(N'2017-07-20 17:32:55.4233961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (15, N'U1700015', N'SM21', N'phWjm/FyrHc=', N'SNR MGR 2.1', NULL, N'SNRMGR1@NODC.COM', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 11, N'Sales', N'Senior Manger', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:27:14.3449961' AS DateTime2), CAST(N'2017-07-20 17:27:14.3449961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (20, N'U1700020', N'CUST1121', N'phWjm/FyrHc=', N'CUST 1.1.2.1', NULL, N'cust@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 19, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:34:27.1825961' AS DateTime2), CAST(N'2017-07-20 17:34:27.1825961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (21, N'U1700021', N'CUST11211', N'phWjm/FyrHc=', N'CUST 1.1.2.1.1', NULL, N'cust1@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 20, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:35:32.6401961' AS DateTime2), CAST(N'2017-07-20 17:35:32.6401961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (22, N'U1700022', N'CON1.3.1', N'phWjm/FyrHc=', N'CONS 1.3.1', NULL, N'cust5@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 14, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:38:59.0593961' AS DateTime2), CAST(N'2017-07-20 17:38:59.0593961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (23, N'U1700023', N'PROF1311', N'phWjm/FyrHc=', N'PropNex 1.3.1.1', NULL, N'propnex11@nods.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 22, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:40:30.0541961' AS DateTime2), CAST(N'2017-07-20 17:40:30.0541961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (24, N'U1700024', N'PROF211', N'phWjm/FyrHc=', N'ERA 2.1.1', NULL, N'ERA1@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 15, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:43:21.8881961' AS DateTime2), CAST(N'2017-07-20 17:43:21.8881961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (25, N'U1700025', N'PROF212', N'phWjm/FyrHc=', N'ERA 2.1.2', NULL, N'ERA2@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 15, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:44:31.0897961' AS DateTime2), CAST(N'2017-07-20 17:44:31.0897961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (26, N'U1700026', N'CUST2121', N'phWjm/FyrHc=', N'CUST 2.1.2.1', NULL, N'custi@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 25, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:45:58.8709961' AS DateTime2), CAST(N'2017-07-20 17:45:58.8709961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (27, N'U1700027', N'CUST21211', N'phWjm/FyrHc=', N'CUST 2.1.2.1.1', NULL, N'cust6@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 26, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:47:37.2289961' AS DateTime2), CAST(N'2017-07-20 17:47:37.2289961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (28, N'U1700028', N'CON231', N'phWjm/FyrHc=', N'CONS 2.3.1', NULL, N'const@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 17, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:49:42.1537961' AS DateTime2), CAST(N'2017-07-20 17:49:42.1537961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (29, N'U1700029', N'PROF2311', N'phWjm/FyrHc=', N'ERA 2.3.1.1', NULL, N'ERA4@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 28, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:50:54.8497961' AS DateTime2), CAST(N'2017-07-20 17:50:54.8497961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (30, N'U1700030', N'CUST13111', N'phWjm/FyrHc=', N'CUST1.3.1.1', NULL, N'nodc@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 23, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-25 17:03:23.6999132' AS DateTime2), CAST(N'2017-07-25 17:03:23.6999132' AS DateTime2), N'N', NULL)
SET IDENTITY_INSERT [dbo].[Users] OFF
