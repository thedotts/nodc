USE [NODC_OMS]
GO
/****** Object:  Table [dbo].[AuditLogs]    Script Date: 5/5/2017 11:57:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditLogs](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IPAddress] [nvarchar](255) NOT NULL,
	[Timestamp] [datetime2](7) NOT NULL,
	[UserTriggeringId] [int] NOT NULL,
	[UserRole] [nvarchar](255) NOT NULL,
	[TableAffected] [nvarchar](255) NOT NULL,
	[Description] [ntext] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerReminders]    Script Date: 5/5/2017 11:57:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerReminders](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[Repeat] [nvarchar](255) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customers]    Script Date: 5/5/2017 11:57:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [nvarchar](255) NOT NULL,
	[LoginID] [nvarchar](255) NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[CompanyName] [nvarchar](255) NULL,
	[Agency] [nvarchar](255) NOT NULL,
	[ContactPerson] [nvarchar](255) NOT NULL,
	[Address] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[Tel] [nvarchar](255) NOT NULL,
	[Fax] [nvarchar](255) NULL,
	[BankName] [nvarchar](255) NULL,
	[BankAccNo] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[UplineUserType] [nvarchar](255) NULL,
	[UplineId] [int] NOT NULL,
	[Position] [nvarchar](255) NOT NULL,
	[AssignToId] [int] NOT NULL,
	[TotalCompletedProfit] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL,
	[ResetPasswordToken] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EarlyPayouts]    Script Date: 5/5/2017 11:57:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EarlyPayouts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EarlyPayoutID] [nvarchar](255) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[PayToId] [int] NOT NULL,
	[UserType] [nvarchar](255) NOT NULL,
	[PayoutDate] [datetime2](7) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[Remarks] [ntext] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoiceItemDescriptions]    Script Date: 5/5/2017 11:57:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceItemDescriptions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceId] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[PercentageOfQuotation] [nvarchar](255) NOT NULL,
	[Amount] [decimal](20, 2) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Invoices]    Script Date: 5/5/2017 11:57:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoices](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceID] [nvarchar](255) NOT NULL,
	[Company] [nvarchar](255) NOT NULL,
	[QuotationId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[ContactPerson] [nvarchar](255) NOT NULL,
	[PreparedById] [int] NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[Remarks] [ntext] NULL,
	[Notes] [ntext] NULL,
	[GST] [nvarchar](255) NOT NULL,
	[PaymentMade] [decimal](18, 2) NULL,
	[SubtotalAmount] [decimal](18, 2) NOT NULL,
	[GSTAmount] [decimal](18, 2) NOT NULL,
	[GrandTotalAmount] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PayoutCalculations]    Script Date: 5/5/2017 11:57:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayoutCalculations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PayoutId] [int] NOT NULL,
	[Tier] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[UserType] [nvarchar](255) NOT NULL,
	[Revenue] [decimal](18, 2) NOT NULL,
	[PayoutPercentage] [decimal](18, 2) NOT NULL,
	[Commission] [decimal](18, 2) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Payouts]    Script Date: 5/5/2017 11:57:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payouts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PayoutID] [nvarchar](255) NOT NULL,
	[PayToId] [int] NOT NULL,
	[UserType] [nvarchar](255) NOT NULL,
	[Period] [datetime2](7) NOT NULL,
	[TotalCommissionAmount] [decimal](18, 2) NOT NULL,
	[TotalRetainedAmount] [decimal](18, 2) NOT NULL,
	[TotalEarlyPayoutAmount] [decimal](18, 2) NOT NULL,
	[TotalPayoutAmount] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProjectEstimators]    Script Date: 5/5/2017 11:57:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProjectEstimators](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NULL,
	[ProjectExpensesDescription] [nvarchar](255) NULL,
	[ProjectExpensesCategoryName] [nvarchar](255) NULL,
	[ProjectExpensesAmount] [decimal](18, 2) NULL,
	[ProjectExpensesProjectedCost] [decimal](18, 2) NULL,
	[ProjectExpensesAdjCost] [decimal](18, 2) NULL,
	[ProjectExpensesProfit] [decimal](18, 2) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProjectProjectExpenses]    Script Date: 5/5/2017 11:57:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProjectProjectExpenses](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[SupplierName] [nvarchar](255) NOT NULL,
	[Date] [datetime] NOT NULL,
	[GST] [nvarchar](255) NOT NULL,
	[Cost] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Projects]    Script Date: 5/5/2017 11:57:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Projects](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [nvarchar](255) NOT NULL,
	[CompanyName] [nvarchar](255) NOT NULL,
	[QuotationReferenceId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[PreparedById] [int] NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[CoordinatorId] [int] NOT NULL,
	[ProjectTitle] [nvarchar](255) NOT NULL,
	[ProjectDescription] [ntext] NULL,
	[ProjectType] [nvarchar](255) NOT NULL,
	[PhotoUploads] [nvarchar](255) NULL,
	[FloorPlanUploads] [nvarchar](255) NULL,
	[ThreeDDrawingUploads] [nvarchar](255) NULL,
	[AsBuildDrawingUploads] [nvarchar](255) NULL,
	[SubmissionStageUploads] [nvarchar](255) NULL,
	[OtherUploads] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CompletedDate] [datetime2](7) NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuotationCoBrokes]    Script Date: 5/5/2017 11:57:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuotationCoBrokes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationId] [int] NOT NULL,
	[QuotationCoBrokeUserType] [nvarchar](255) NULL,
	[QuotationCoBrokeUserId] [int] NULL,
	[QuotationCoBrokePercentOfProfit] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuotationItemDescriptions]    Script Date: 5/5/2017 11:57:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuotationItemDescriptions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationId] [int] NOT NULL,
	[RowId] [int] NOT NULL,
	[CategoryName] [nvarchar](255) NOT NULL,
	[QuotationItem_Description] [nvarchar](255) NOT NULL,
	[QuotationItemQty] [int] NULL,
	[QuotationItemUnit] [nvarchar](255) NULL,
	[QuotationItemRate] [decimal](18, 2) NULL,
	[QuotationItemAmount] [decimal](18, 2) NULL,
	[QuotationItemEstCost] [decimal](18, 2) NULL,
	[QuotationItemEstProfit] [decimal](18, 2) NULL,
	[QuotationItemRemarks] [ntext] NULL,
	[QuotationItemMgrComments] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Quotations]    Script Date: 5/5/2017 11:57:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quotations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationID] [nvarchar](255) NOT NULL,
	[CompanyName] [nvarchar](255) NULL,
	[HasGST] [nvarchar](255) NULL,
	[CustomerId] [int] NULL,
	[ContactPerson] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Tel] [nvarchar](255) NULL,
	[Fax] [nvarchar](255) NULL,
	[PreparedById] [int] NOT NULL,
	[SalesPersonType] [nvarchar](255) NULL,
	[SalesPersonId] [int] NULL,
	[QuotationDate] [datetime2](7) NULL,
	[SalesDocumentUploads] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[TermsAndConditions] [ntext] NULL,
	[Status] [nvarchar](255) NOT NULL,
	[ReviseQuotationTimes] [int] NOT NULL,
	[VariationOrderTimes] [int] NOT NULL,
	[PreviousQuotationId] [int] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SystemSettings]    Script Date: 5/5/2017 11:57:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemSettings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](255) NOT NULL,
	[Value] [nvarchar](255) NULL,
	[DataType] [nvarchar](255) NOT NULL,
	[IsRequired] [nvarchar](255) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 5/5/2017 11:57:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [nvarchar](255) NOT NULL,
	[LoginID] [nvarchar](255) NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Address] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[Tel] [nvarchar](255) NOT NULL,
	[Mobile] [nvarchar](255) NULL,
	[BankName] [nvarchar](255) NULL,
	[BankAccNo] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[Reminder] [nvarchar](255) NOT NULL,
	[IsAgencyLeader] [nvarchar](255) NOT NULL,
	[UplineId] [int] NOT NULL,
	[Role] [nvarchar](255) NOT NULL,
	[Position] [nvarchar](255) NULL,
	[PresetValue] [decimal](18, 2) NOT NULL,
	[TotalCompletedProfit] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL,
	[ResetPasswordToken] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[CustomerReminders] ON 

INSERT [dbo].[CustomerReminders] ([ID], [CustomerId], [Description], [Date], [Repeat]) VALUES (1, 2, N'Reminder 1', CAST(0x070000000000BD3C0B AS DateTime2), N'Monthly')
SET IDENTITY_INSERT [dbo].[CustomerReminders] OFF
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (1, N'C1700001', N'tds', N'Rn5sE6W5YkI=', N'The Dott Solutions Pte Ltd', N'Era', N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'elvinong@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'User', 2, N'Referral Tier 1', 16, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07FF83914692BD3C0B AS DateTime2), CAST(0x072126E83793BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (2, N'C1700002', N'nodc', N'Rn5sE6W5YkI=', N'NODC Pte Ltd', N'Era', N'Number One Design + Consultancy', N'81 UBI AVENUE 4, #07-06, Singapore 408830', N'nodc@gmail.com', N'+658029579', N'+658029579', N'Bank of Singapore', N'123456789', NULL, N'User', 2, N'Referral Tier 1', 17, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x073D3086F492BD3C0B AS DateTime2), CAST(0x070FBCA94593BD3C0B AS DateTime2), N'N', NULL)
SET IDENTITY_INSERT [dbo].[Customers] OFF
SET IDENTITY_INSERT [dbo].[ProjectProjectExpenses] ON 

INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [Date], [GST], [Cost], [Status]) VALUES (1, 1, N'Colin''s Expenses 1', N'Colins', CAST(0x0000A76200000000 AS DateTime), N'Yes', CAST(200.00 AS Decimal(18, 2)), N'Paid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [Date], [GST], [Cost], [Status]) VALUES (2, 2, N'Bobby''s Expenses', N'Bobby', CAST(0x0000A76200000000 AS DateTime), N'Yes', CAST(300.00 AS Decimal(18, 2)), N'Paid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [Date], [GST], [Cost], [Status]) VALUES (3, 3, N'Aaron''s Expenses', N'Aaron', CAST(0x0000A76200000000 AS DateTime), N'Yes', CAST(500.00 AS Decimal(18, 2)), N'Paid')
SET IDENTITY_INSERT [dbo].[ProjectProjectExpenses] OFF
SET IDENTITY_INSERT [dbo].[Projects] ON 

INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (1, N'P1700001', N'NODC Pte Ltd', 1, 1, 1, CAST(0x070000000000BD3C0B AS DateTime2), 18, N'Colin''s Project $1,200', N'Total Quotation of $1,200', N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', NULL, CAST(0x07A5B587F297BD3C0B AS DateTime2), CAST(0x07A5B587F297BD3C0B AS DateTime2), N'N')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (2, N'P1700002', N'NODC Pte Ltd', 2, 1, 1, CAST(0x070000000000BD3C0B AS DateTime2), 17, N'Bobby''s Project $1,500', N'Bobby''s Quotation $1,500', N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', NULL, CAST(0x07390F5D2698BD3C0B AS DateTime2), CAST(0x07390F5D2698BD3C0B AS DateTime2), N'N')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (3, N'P1700003', N'NODC Pte Ltd', 3, 2, 1, CAST(0x070000000000BD3C0B AS DateTime2), 16, N'Aaron''s Project $2,000', N'Aaron''s Quotation $2,000', N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', NULL, CAST(0x07D1A8EB5C98BD3C0B AS DateTime2), CAST(0x07D1A8EB5C98BD3C0B AS DateTime2), N'N')
SET IDENTITY_INSERT [dbo].[Projects] OFF
SET IDENTITY_INSERT [dbo].[QuotationCoBrokes] ON 

INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (1, 1, NULL, 18, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (2, 2, NULL, 17, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (3, 3, NULL, 2, 45)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (4, 3, NULL, 16, 10)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (5, 3, NULL, 1, 45)
SET IDENTITY_INSERT [dbo].[QuotationCoBrokes] OFF
SET IDENTITY_INSERT [dbo].[QuotationItemDescriptions] ON 

INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (1, 1, 0, N'Preliminaries', N'Preliminaries', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (2, 1, 0, N'Authority Submission', N'Authority Submission', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (3, 1, 0, N'Partitioning & Ceiling Work', N'Partitioning & Ceiling Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (4, 1, 0, N'Flooring Work', N'Flooring Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (5, 1, 0, N'Painting Work', N'Painting Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (6, 1, 0, N'Mill Work', N'Mill Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (7, 1, 0, N'Glazing Work', N'Glazing Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (8, 1, 0, N'Electrical', N'Electrical', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (9, 1, 0, N'Plumbing & Sanitory', N'Plumbing & Sanitory', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (10, 1, 0, N'ACMV Work', N'ACMV Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (11, 1, 0, N'Fire Protection Work', N'Fire Protection Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (12, 1, 0, N'Miscellaneous', N'Miscellaneous', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (13, 2, 0, N'Preliminaries', N'Preliminaries 1', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (14, 2, 0, N'Preliminaries', N'Preliminaries 2', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (15, 2, 0, N'Authority Submission', N'Authority Submission 1', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (16, 2, 0, N'Authority Submission', N'Authority Submission 2', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (17, 2, 0, N'Partitioning & Ceiling Work', N'Partitioning & Ceiling Work 1', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (18, 2, 0, N'Partitioning & Ceiling Work', N'Partitioning & Ceiling Work 2', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (19, 2, 0, N'Flooring Work', N'Flooring Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (20, 2, 0, N'Painting Work', N'Painting Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (21, 2, 0, N'Mill Work', N'Mill Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (22, 2, 0, N'Glazing Work', N'Glazing Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (23, 2, 0, N'Electrical', N'Electrical', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (24, 2, 0, N'Plumbing & Sanitory', N'Plumbing & Sanitory', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (25, 2, 0, N'ACMV Work', N'ACMV Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (26, 2, 0, N'Fire Protection Work', N'Fire Protection Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (27, 2, 0, N'Miscellaneous', N'Miscellaneous', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (28, 3, 0, N'Preliminaries', N'Preliminaries', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (29, 3, 0, N'Authority Submission', N'Authority Submission', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (30, 3, 0, N'Partitioning & Ceiling Work', N'Partitioning & Ceiling Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (31, 3, 0, N'Flooring Work', N'Flooring Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (32, 3, 0, N'Painting Work', N'Painting Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (33, 3, 0, N'Mill Work', N'Mill Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (34, 3, 0, N'Glazing Work', N'Glazing Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (35, 3, 0, N'Electrical', N'Electrical', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (36, 3, 0, N'Plumbing & Sanitory', N'Plumbing & Sanitory', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (37, 3, 0, N'ACMV Work', N'ACMV Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (38, 3, 0, N'Fire Protection Work', N'Fire Protection Work', 1, N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (39, 3, 0, N'Miscellaneous', N'Miscellaneous', 1, N'lot', CAST(900.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
SET IDENTITY_INSERT [dbo].[QuotationItemDescriptions] OFF
SET IDENTITY_INSERT [dbo].[Quotations] ON 

INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (1, N'Q1700001', N'NODC Pte Ltd', N'Yes', 1, N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'65 9192 9394', N'65 9192 9393', 2, NULL, 18, CAST(0x070000000000BD3C0B AS DateTime2), N'UserGuide_170427174953.pdf', N'Colins''s Quotation on 27/04/2017', NULL, N'Confirmed', 0, 0, 1, CAST(0x079B00561594BD3C0B AS DateTime2), CAST(0x07200BAB7995BD3C0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (2, N'Q1700002', N'NODC Pte Ltd', N'Yes', 2, N'Elvin Ong', N'81 UBI AVENUE 4, #07-06, Singapore 408830', N'+658029579', N'+658029579', 1, NULL, 17, CAST(0x070000000000BD3C0B AS DateTime2), N'UserGuide_170427175744.pdf', N'Bobby''s Quotation on 27/04/2017', NULL, N'Confirmed', 0, 0, 2, CAST(0x0783A9886796BD3C0B AS DateTime2), CAST(0x0757E6209296BD3C0B AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (3, N'Q1700003', N'NODC Pte Ltd', N'Yes', 2, N'Elvin Ong', N'81 UBI AVENUE 4, #07-06, Singapore 408830', N'+658029579', N'+658029579', 2, NULL, 16, CAST(0x070000000000BD3C0B AS DateTime2), N'UserGuide_170427175854.pdf', N'Aaron''s Quotation on 27/04/2017', NULL, N'Confirmed', 0, 0, 3, CAST(0x07BE52200A97BD3C0B AS DateTime2), CAST(0x071DBC8E2997BD3C0B AS DateTime2), N'N')
SET IDENTITY_INSERT [dbo].[Quotations] OFF
SET IDENTITY_INSERT [dbo].[SystemSettings] ON 

INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (1, N'COMPANY_NAME', N'NODC Pte Ltd', N'string', N'Y', CAST(0x07B91421CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (2, N'COMPANY_ADDRESS', N'123', N'string', N'Y', CAST(0x071B7A32CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (3, N'COMPANY_POSTAL_CODE', N'12345', N'string', N'Y', CAST(0x07608C3CCD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (4, N'DEFAULT_EMAIL', N'email@gmail.com', N'string', N'Y', CAST(0x07001845CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (5, N'MAIN_CONTACT_PERSON', N'lim', N'string', N'N', CAST(0x072D924CCD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (6, N'MAIN_CONTACT_NO', N'123', N'string', N'N', CAST(0x077A5A54CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (7, N'PREFIX_INVOICE_ID', N'INV', N'string', N'N', CAST(0x07F9975CCD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (8, N'PREFIX_QUOTATION_ID', N'Q', N'string', N'N', CAST(0x07568764CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (9, N'PREFIX_PROJECT_ID', N'P', N'string', N'N', CAST(0x0739AF6DCD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (10, N'PREFIX_CUSTOMER_ID', N'C', N'string', N'N', CAST(0x07E96176CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (11, N'PREFIX_USER_ID', N'U', N'string', N'N', CAST(0x0747517ECD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (12, N'PREFIX_PAYOUT_ID', N'P', N'string', N'N', CAST(0x07B56786CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (13, N'PREFIX_EARLY_PAYOUT_ID', N'E', N'string', N'N', CAST(0x07B56786CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (14, N'TERM_AND_CONDITION', N'123', N'string', N'N', CAST(0x0734A58ECD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (15, N'PERCENTAGE_HIGHLIGH_RED', N'17', N'string', N'Y', CAST(0x07B3E296CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (16, N'GST', N'7', N'string', N'Y', CAST(0x0721F99ECD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (17, N'AGENCY_LIST', N'Propnex|ERA', N'string', N'Y', CAST(0x07D2ABA7CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (18, N'QUOTATION_CATEGORIES', N'Preliminaries|Authority Submission|Partitioning & Ceiling Work|Flooring Work|Painting Work|Mill Work|Glazing Work|Electrical|Plumbing & Sanitory|ACMV Work|Fire Protection Work|Miscellaneous', N'string', N'Y', CAST(0x07FE25AFCD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (19, N'LIST_OF_COMPANY', N'NODC Pte Ltd|Yes;No 1 Design Consultancy Pte Ltd|Yes', N'string', N'Y', CAST(0x077D63B7CD56993C0B AS DateTime2))
SET IDENTITY_INSERT [dbo].[SystemSettings] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (1, N'U1700001', N'hongguan', N'Rn5sE6W5YkI=', N'Lim Hong Guan', N'46 Tagore Lane, 787900 Singapore', N'hongguan@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'Yes', 1, N'Super Admin', NULL, CAST(1000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x070000000000BD3C0B AS DateTime2), CAST(0x07E4A0030E96C23C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (2, N'U1700002', N'erntay2', N'Rn5sE6W5YkI=', N'Stephen Tay Chan Ern', N'46 Tagore Lane, 787900 Singapore', N'chanern@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'Yes', 1, N'Super Admin', NULL, CAST(1000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x074F56267B8DBD3C0B AS DateTime2), CAST(0x074F56267B8DBD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (3, N'U1700003', N'sd_AL', N'Rn5sE6W5YkI=', N'sd_AL', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'sd_AL@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'Yes', 1, N'Sales', N'Director', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x0700E064E78FBD3C0B AS DateTime2), CAST(0x0700E064E78FBD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (4, N'U1700004', N'sd', N'Rn5sE6W5YkI=', N'sd', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'sd@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'No', 1, N'Sales', N'Director', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x070765DA0390BD3C0B AS DateTime2), CAST(0x070765DA0390BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (5, N'U1700005', N'ssm_AL', N'Rn5sE6W5YkI=', N'ssm_AL', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'ssm_AL@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'Yes', 3, N'Sales', N'Senior Manger', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x0788EE341E90BD3C0B AS DateTime2), CAST(0x0788EE341E90BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (6, N'U1700006', N'ssm', N'Rn5sE6W5YkI=', N'ssm', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'ssm@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'No', 4, N'Sales', N'Senior Manger', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07F8ABF13090BD3C0B AS DateTime2), CAST(0x07F8ABF13090BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (7, N'U1700007', N'sm_AL', N'Rn5sE6W5YkI=', N'sm_AL', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'sm_AL@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'Yes', 5, N'Sales', N'Manager', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07E250965290BD3C0B AS DateTime2), CAST(0x07E250965290BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (8, N'U1700008', N'sm', N'Rn5sE6W5YkI=', N'sm', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'sm@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'No', 6, N'Sales', N'Manager', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07D130D66590BD3C0B AS DateTime2), CAST(0x07D130D66590BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (9, N'U1700009', N'sam_AL', N'Rn5sE6W5YkI=', N'sam_AL', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'sam_AL@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'Yes', 7, N'Sales', N'Assistant Manager', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07A889D97C90BD3C0B AS DateTime2), CAST(0x07A889D97C90BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (10, N'U1700010', N'sam', N'Rn5sE6W5YkI=', N'sam', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'sam@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'No', 8, N'Sales', N'Assistant Manager', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07BB3B629090BD3C0B AS DateTime2), CAST(0x07BB3B629090BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (11, N'U1700011', N'sc_AL', N'Rn5sE6W5YkI=', N'sc_AL', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'sc_AL@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'Yes', 9, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07CA7BD8A490BD3C0B AS DateTime2), CAST(0x07CA7BD8A490BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (12, N'U1700012', N'sc', N'Rn5sE6W5YkI=', N'sc', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'sc@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'No', 10, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x075EE8E1B590BD3C0B AS DateTime2), CAST(0x075EE8E1B590BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (13, N'U1700013', N'accounts_AL', N'Rn5sE6W5YkI=', N'accounts_AL', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'accounts_AL@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'Yes', 1, N'Accounts', NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x0700C5C0D590BD3C0B AS DateTime2), CAST(0x0700C5C0D590BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (16, N'U1700016', N'aaron', N'Rn5sE6W5YkI=', N'Aaron', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'aaron@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'No', 1, N'Sales', N'Director', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07A5A9456F91BD3C0B AS DateTime2), CAST(0x07A5A9456F91BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (14, N'U1700014', N'accounts', N'Rn5sE6W5YkI=', N'accounts', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'accounts@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'No', 1, N'Accounts', NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x078573B9EA90BD3C0B AS DateTime2), CAST(0x078573B9EA90BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (15, N'U1700015', N'superadmin_AL', N'Rn5sE6W5YkI=', N'superadmin_AL', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'superadmin_AL@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'Yes', 1, N'Super Admin', NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x073EBDF01891BD3C0B AS DateTime2), CAST(0x073EBDF01891BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (17, N'U1700017', N'bobby', N'Rn5sE6W5YkI=', N'Bobby', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'bobby@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'No', 16, N'Sales', N'Senior Manger', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07F077F58A91BD3C0B AS DateTime2), CAST(0x07F077F58A91BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (18, N'U1700018', N'colins', N'Rn5sE6W5YkI=', N'Colins', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'colins@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'No', 17, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x0751A5499D91BD3C0B AS DateTime2), CAST(0x0751A5499D91BD3C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (19, N'U1700019', N'elvin', N'Rn5sE6W5YkI=', N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'elvinong@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'Yes', 1, N'Super Admin', NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07B7F0F9B695C23C0B AS DateTime2), CAST(0x07B7F0F9B695C23C0B AS DateTime2), N'N', NULL)
SET IDENTITY_INSERT [dbo].[Users] OFF
