USE [NODC_OMS]
GO
SET IDENTITY_INSERT [dbo].[CustomerReminders] ON 

INSERT [dbo].[CustomerReminders] ([ID], [CustomerId], [Description], [Date], [Repeat]) VALUES (1, 2, N'Reminder 1', CAST(N'2017-04-27 00:00:00.0000000' AS DateTime2), N'Monthly')
SET IDENTITY_INSERT [dbo].[CustomerReminders] OFF
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (1, N'C1700001', N'tds', N'Rn5sE6W5YkI=', N'The Dott Solutions Pte Ltd', N'Era', N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'elvinong@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'User', 2, N'Referral Tier 1', 16, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-04-27 17:27:04.9166847' AS DateTime2), CAST(N'2017-09-07 15:56:54.0448000' AS DateTime2), N'Y', N'CSeBOj1cenu47VW1gYnZUw==')
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (2, N'C1700002', N'nodc', N'Rn5sE6W5YkI=', N'NODC Pte Ltd', N'Era', N'Number One Design + Consultancy', N'81 UBI AVENUE 4, #07-06, Singapore 408830', NULL, N'+658029579', N'+658029579', N'Bank of Singapore', N'123456789', NULL, N'User', 2, N'Referral Tier 1', 17, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-04-27 17:31:56.7660093' AS DateTime2), CAST(N'2017-09-07 15:56:44.6692000' AS DateTime2), N'Y', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (3, N'C1700003', N'tds1', N'Rn5sE6W5YkI=', N'The Art Haus', N'No Agency', N'Slone Lim', N'50 East Coast Road, #01-110 , #01-134 Roxy Square, Singapore 428769', NULL, N'9222 3605', NULL, NULL, NULL, NULL, N'User', 12, N'Professional', 12, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 14:59:27.5701961' AS DateTime2), CAST(N'2017-09-07 15:56:40.6912000' AS DateTime2), N'Y', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (4, N'C1700004', N'tds2', N'Rn5sE6W5YkI=', N'MENCK ', N'No Agency', N'MR CHRISTIAN FRANK', N'107 TUAS SOUTH AVE 8 OFFSHORE MARINE CENTRE SINGAPORE 637036', NULL, N'67055800', N'67948345', NULL, NULL, NULL, N'User', 8, N'Professional', 8, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 15:04:06.8413961' AS DateTime2), CAST(N'2017-09-07 15:49:28.0408000' AS DateTime2), N'Y', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (5, N'C1700005', N'tds3', N'Rn5sE6W5YkI=', N'AceCom Technologies Pte Ltd', N'No Agency', N'Ms Haze Tan', N'1 Ubi View #03-10, Focus One Singapore 408555', NULL, N'63537767', NULL, NULL, NULL, NULL, N'User', 10, N'Referral Tier 1', 10, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 15:20:07.4581961' AS DateTime2), CAST(N'2017-09-07 15:56:35.7772000' AS DateTime2), N'Y', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (10, N'C1700010', N'tds8', N'Rn5sE6W5YkI=', N'My Banh Mi Singapore Pte Ltd', N'No Agency', N'Attn: Miss Tay Ying Hui', N'81 Ubi Ave 4 #07-06 Singapore 408830', NULL, N'94554784', NULL, NULL, NULL, NULL, N'User', 12, N'Professional', 12, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 16:49:33.9205961' AS DateTime2), CAST(N'2017-09-07 15:56:12.9232000' AS DateTime2), N'Y', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (11, N'C1700011', N'tds9', N'Rn5sE6W5YkI=', N'Grains Pte Ltd', N'No Agency', N'Jake Chia', N'703 Ang Mo Kio Ave 5#04-40 Singapore 569880 Northstar@AMK', NULL, N'9144 6336', NULL, NULL, NULL, NULL, N'User', 15, N'Referral Tier 1', 15, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-21 15:39:59.6381136' AS DateTime2), CAST(N'2017-09-07 15:56:08.6176000' AS DateTime2), N'Y', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (12, N'C1700012', N'tds10', N'Rn5sE6W5YkI=', N'24 Pte Ltd', N'No Agency', N'Miss Swathi Mathur', N'21 Media Circle #05-06, Infinite Studios Singapore 138562', NULL, N'6589 8744', NULL, NULL, NULL, NULL, N'User', 11, N'Referral Tier 1', 11, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-21 15:40:36.4073136' AS DateTime2), CAST(N'2017-09-07 15:56:00.1000000' AS DateTime2), N'Y', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (13, N'C1700013', N'tds11', N'Rn5sE6W5YkI=', N'Orange Lantern', N'No Agency', N'Alvin', N'1006 Aljunied Ave 5', NULL, N'9675 9990', NULL, NULL, NULL, NULL, N'User', 15, N'Referral Tier 1', 15, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-21 16:43:23.4329136' AS DateTime2), CAST(N'2017-09-07 15:55:52.5652000' AS DateTime2), N'Y', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (14, N'C1700014', N'tds12', N'Rn5sE6W5YkI=', N'Pezzo Singapore Pte Ltd', N'No Agency', N'Chiang Zhan Yi', N'51 Imbiah Road, Singapore 099702', NULL, N'9640 7401', NULL, NULL, NULL, NULL, N'User', 15, N'Referral Tier 1', 15, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-21 17:09:15.0089136' AS DateTime2), CAST(N'2017-09-07 15:55:47.9788000' AS DateTime2), N'Y', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (15, N'C1700015', N'tds13', N'Rn5sE6W5YkI=', N'Hidden Chefs Pte Ltd', N'No Agency', N'Kelly Bok', N'216 Boon Lay Avenue	 #01-01 Singapore 640216	', NULL, N'9631 7106', NULL, NULL, NULL, NULL, N'User', 17, N'Professional', 17, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-21 17:25:33.0197136' AS DateTime2), CAST(N'2017-09-07 15:55:42.5812000' AS DateTime2), N'Y', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (16, N'C1700016', N'tds14', N'Rn5sE6W5YkI=', N'Sanfrance F&B Pte Ltd', N'No Agency', N'Michael', N'1 Jalan Anak Bukit #B1-52, Singapore 588996', NULL, N'9636 2377', NULL, NULL, NULL, NULL, N'User', 16, N'Referral Tier 1', 16, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-21 17:38:16.2497136' AS DateTime2), CAST(N'2017-09-07 15:55:29.9764000' AS DateTime2), N'Y', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (17, N'C1700001', N'tds1', N'JN8JsdftjiU=', N'Menck', N'No Agency', N'Mr Christian Frank ', NULL, NULL, N'67055800', NULL, NULL, NULL, NULL, N'User', 8, N'Professional', 8, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-09-07 16:09:24.9508000' AS DateTime2), CAST(N'2017-09-07 16:52:38.5132000' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (18, N'C1700018', N'tds2', N'Rn5sE6W5YkI=', N'AceCom Technologies Pte Ltd', N'No Agency', N'Haze Tan', NULL, NULL, N'63537767', NULL, NULL, NULL, NULL, N'User', 10, N'Professional', 10, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-09-07 17:34:24.6532000' AS DateTime2), CAST(N'2017-09-07 17:34:24.6532000' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (19, N'C1700019', N'td3', N'Rn5sE6W5YkI=', N'24 Pte Ltd', N'No Agency', N'Miss Swathi Mathur', N'21 Media Circle #05-06, Infinite Studios Singapore 138562', NULL, N'90046524', NULL, NULL, NULL, NULL, N'User', 11, N'Referral Tier 1', 11, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-09-14 16:47:40.5446000' AS DateTime2), CAST(N'2017-09-14 16:47:40.5446000' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (20, N'C1700020', N'tds3', N'Rn5sE6W5YkI=', N'Orange Lantern', N'No Agency', N'Mr Alvin', NULL, NULL, N'96759990', NULL, NULL, NULL, NULL, N'User', 15, N'Referral Tier 1', 15, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-09-14 17:08:20.5574000' AS DateTime2), CAST(N'2017-09-14 17:08:20.5574000' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (21, N'C1700021', N'tds5', N'Rn5sE6W5YkI=', N'My Banh Mi Sinapore Pte Ltd', N'No Agency', N'Tay Ying Hui', NULL, NULL, N'94554784', NULL, NULL, NULL, NULL, N'User', 12, N'Referral Tier 1', 12, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-09-14 17:44:40.0022000' AS DateTime2), CAST(N'2017-09-14 17:44:40.0022000' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (22, N'C1700022', N'tds6', N'Sf/8hf+jTHc=', N'JE Education Pte Ltd', N'No Agency', N'Jacky Lim', NULL, NULL, N'93835543', NULL, NULL, NULL, NULL, N'User', 13, N'Referral Tier 1', 13, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-09-14 17:55:16.9502000' AS DateTime2), CAST(N'2017-09-14 17:55:16.9502000' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (23, N'C1700023', N'elly', N'Rl7GfqCtGkQ=', NULL, N'No Agency', N'elly', N'123', NULL, N'12345678', NULL, NULL, NULL, NULL, N'User', 7, N'Referral Tier 1', 3, CAST(0.00 AS Decimal(18, 2)), N'Lead', CAST(N'2017-11-02 13:36:57.9406000' AS DateTime2), CAST(N'2017-11-07 14:35:44.1236000' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (24, N'C1700024', N'mavis', N'+jLsCN1tlu4=', NULL, N'No Agency', N'1234', NULL, NULL, N'12344', N'1', NULL, NULL, NULL, N'User', 7, N'Referral Tier 1', 22, CAST(0.00 AS Decimal(18, 2)), N'Lead', CAST(N'2017-11-02 13:44:45.7222000' AS DateTime2), CAST(N'2017-11-02 13:44:45.7222000' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (25, N'C1700025', N'peter', N'phWjm/FyrHc=', NULL, N'No Agency', N'323', NULL, NULL, N'234', NULL, NULL, NULL, NULL, N'User', 10, N'Referral Tier 1', 15, CAST(0.00 AS Decimal(18, 2)), N'Lead', CAST(N'2017-11-02 13:51:25.3318000' AS DateTime2), CAST(N'2017-11-02 13:51:25.3318000' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (26, N'C1700026', N'khoo', N'phWjm/FyrHc=', NULL, N'No Agency', N'khoo', NULL, NULL, N'123456', NULL, NULL, NULL, NULL, N'User', 10, N'Referral Tier 1', 22, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-11-02 13:59:18.4018000' AS DateTime2), CAST(N'2017-11-02 14:03:14.4298000' AS DateTime2), N'N', N'AZ2crKnf/xFUg2XZGYaffA==')
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (27, N'C1700027', N'sally', N'phWjm/FyrHc=', NULL, N'No Agency', N'sally', NULL, NULL, N'12344', NULL, NULL, NULL, NULL, N'Customer', 26, N'Referral Tier 1', 12, CAST(0.00 AS Decimal(18, 2)), N'Lead', CAST(N'2017-11-02 14:07:56.6182000' AS DateTime2), CAST(N'2017-11-02 14:07:56.6182000' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (28, N'C1700028', N'Cust1021', N'phWjm/FyrHc=', NULL, N'No Agency', N'cust1021', NULL, NULL, N'123456', NULL, NULL, NULL, NULL, N'User', 34, N'Referral Tier 1', 34, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-11-02 14:43:52.2574000' AS DateTime2), CAST(N'2017-11-02 14:45:53.9998000' AS DateTime2), N'N', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (6, N'C1700006', N'tds4', N'Rn5sE6W5YkI=', N'Outdoor Venture Pte Ltd', N'No Agency', N'Walter Tan', N'17 Kaki Bukit Crescent #02-02 Kaki Bukit Techpark 1, Singapore 416248', NULL, N'9819 6468', NULL, NULL, NULL, NULL, N'User', 12, N'Referral Tier 1', 12, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 15:46:03.1369961' AS DateTime2), CAST(N'2017-09-07 15:56:32.6260000' AS DateTime2), N'Y', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (7, N'C1700007', N'tds5', N'Rn5sE6W5YkI=', N'Now Communications Group Pte Ltd', N'No Agency', N'Mr Hendra Chong, Miss Trixy Tan', N'114 Lavender Street CT Hub 2, #08-53 Singapore 338729	', NULL, N'87820766', NULL, NULL, NULL, NULL, N'User', 9, N'Referral Tier 1', 9, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 16:01:07.7809961' AS DateTime2), CAST(N'2017-09-07 15:56:28.9132000' AS DateTime2), N'Y', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (8, N'C1700008', N'tds7', N'Rn5sE6W5YkI=', N'Outdoor Venture Pte Ltd', N'No Agency', N'Attn: Mr Walter Tan', N'17 Kaki Bukit Crescent #02-02 Kaki Bukit Techpark 1 Singapore 416248', NULL, N'98196468', NULL, NULL, NULL, NULL, N'User', 14, N'Professional', 14, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 16:15:55.1245961' AS DateTime2), CAST(N'2017-09-07 15:56:24.0460000' AS DateTime2), N'Y', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (9, N'C1700009', N'tds6', N'Rn5sE6W5YkI=', N'JE Education Pte Ltd', N'No Agency', N'Jacky Lim', N'224 Westwood Ave #09-15 Singapore 648366', NULL, N'9383 5543', NULL, NULL, NULL, NULL, N'User', 13, N'Referral Tier 1', 13, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 16:26:53.6317961' AS DateTime2), CAST(N'2017-09-07 15:56:20.2552000' AS DateTime2), N'Y', NULL)
INSERT [dbo].[Customers] ([ID], [CustomerID], [LoginID], [Password], [CompanyName], [Agency], [ContactPerson], [Address], [Email], [Tel], [Fax], [BankName], [BankAccNo], [Remarks], [UplineUserType], [UplineId], [Position], [AssignToId], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (29, N'C1700029', N'Cust1022', N'phWjm/FyrHc=', NULL, N'No Agency', N'cust1022', NULL, NULL, N'1234', NULL, NULL, NULL, NULL, N'Customer', 28, N'Referral Tier 1', 33, CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-11-02 14:48:05.0398000' AS DateTime2), CAST(N'2017-11-02 14:48:05.0398000' AS DateTime2), N'N', NULL)
SET IDENTITY_INSERT [dbo].[Customers] OFF
SET IDENTITY_INSERT [dbo].[InvoiceItemDescriptions] ON 

INSERT [dbo].[InvoiceItemDescriptions] ([ID], [InvoiceId], [Description], [PercentageOfQuotation], [Amount]) VALUES (1, 1, N'test', N'5', CAST(0.05 AS Decimal(20, 2)))
SET IDENTITY_INSERT [dbo].[InvoiceItemDescriptions] OFF
SET IDENTITY_INSERT [dbo].[Invoices] ON 

INSERT [dbo].[Invoices] ([ID], [InvoiceID], [Company], [QuotationId], [CustomerId], [ContactPerson], [PreparedById], [Date], [Remarks], [Notes], [GST], [PaymentMade], [SubtotalAmount], [GSTAmount], [GrandTotalAmount], [Status], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (1, N'INV1700001', N'NODC Pte Ltd', 44, 23, N'elly', 3, CAST(N'2017-11-07 00:00:00.0000000' AS DateTime2), NULL, NULL, N'Yes', NULL, CAST(0.05 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.05 AS Decimal(18, 2)), N'Pending Payment', CAST(N'2017-11-07 14:34:20.9600000' AS DateTime2), CAST(N'2017-11-07 14:34:20.9600000' AS DateTime2), N'N')
SET IDENTITY_INSERT [dbo].[Invoices] OFF
SET IDENTITY_INSERT [dbo].[ProjectProjectExpenses] ON 

INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (1, 1, N'Colin''s Expenses 1', N'Colins', N'Invoice1', CAST(N'2017-04-27 00:00:00.000' AS DateTime), N'Yes', CAST(200.00 AS Decimal(18, 2)), N'Paid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (2, 2, N'Bobby''s Expenses', N'Bobby', N'Invoice2', CAST(N'2017-04-27 00:00:00.000' AS DateTime), N'Yes', CAST(300.00 AS Decimal(18, 2)), N'Paid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (3, 3, N'Aaron''s Expenses', N'Aaron', N'Invoice3', CAST(N'2017-04-27 00:00:00.000' AS DateTime), N'Yes', CAST(500.00 AS Decimal(18, 2)), N'Paid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (4, 4, N'JIA', N'MSXK', N'123', CAST(N'2017-08-16 00:00:00.000' AS DateTime), N'Yes', CAST(2000.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (5, 4, N'jv', N'jv', N'122', CAST(N'2017-08-16 00:00:00.000' AS DateTime), N'Yes', CAST(3000.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (6, 4, N'a', N'ab', N'1205', CAST(N'2017-08-16 00:00:00.000' AS DateTime), N'Yes', CAST(1205.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (7, 5, N'TILE', N'MSXK', N'MSXK', CAST(N'2017-08-16 00:00:00.000' AS DateTime), N'Yes', CAST(6000.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (8, 5, N'LOA', N'DLLD', N'DLLD', CAST(N'2017-08-16 00:00:00.000' AS DateTime), N'Yes', CAST(5000.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (9, 5, N'JWF', N'DDDJJFK', N'DDDJJFK', CAST(N'2017-08-16 00:00:00.000' AS DateTime), N'Yes', CAST(1560.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (10, 5, N'SDJJK', N'THDN', N'THDN', CAST(N'2017-08-16 00:00:00.000' AS DateTime), N'No', CAST(5000.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (11, 5, N'WEEED', N'LAAA', N'LAAA', CAST(N'2017-08-16 00:00:00.000' AS DateTime), N'Yes', CAST(20000.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (12, 5, N'WWIE', N'EWWS', N'EWWS', CAST(N'2017-08-16 00:00:00.000' AS DateTime), N'Yes', CAST(20000.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (13, 5, N'EEE', N'EEEE', N'EEEE', CAST(N'2017-08-16 00:00:00.000' AS DateTime), N'Yes', CAST(3947.50 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (14, 6, N'dd', N'avc', N'avc', CAST(N'2017-08-17 00:00:00.000' AS DateTime), N'Yes', CAST(7180.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (15, 7, N'dd', N'avc', N'avc', CAST(N'2017-08-17 00:00:00.000' AS DateTime), N'Yes', CAST(7180.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (16, 8, N'hhhjj', N'vsedfg', N'1233', CAST(N'2017-08-17 00:00:00.000' AS DateTime), N'Yes', CAST(7180.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (17, 9, N'fgggg', N'gggg', N'gggg', CAST(N'2017-08-17 00:00:00.000' AS DateTime), N'Yes', CAST(6699.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (18, 9, N'ggg', N'ttt', N'ttt', CAST(N'2017-08-17 00:00:00.000' AS DateTime), N'Yes', CAST(10000.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (19, 10, N'install', N'YYY', N'1236', CAST(N'2017-08-17 00:00:00.000' AS DateTime), N'Yes', CAST(6205.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (20, 11, N'dd', N'gggg', N'1235', CAST(N'2017-08-17 00:00:00.000' AS DateTime), N'Yes', CAST(7180.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (21, 12, N'install', N'MSXK', N'1236', CAST(N'2017-08-17 00:00:00.000' AS DateTime), N'Yes', CAST(1507.50 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (22, 12, N'GLASS', N'LKL', N'655', CAST(N'2017-08-17 00:00:00.000' AS DateTime), N'Yes', CAST(10000.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (23, 12, N'demolis', N'DDDJJFK', N'98', CAST(N'2017-08-17 00:00:00.000' AS DateTime), N'Yes', CAST(50000.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (24, 13, N'glass', N'Ace', N'12334', CAST(N'2017-08-17 00:00:00.000' AS DateTime), N'Yes', CAST(6000.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (25, 13, N'painting', N'Paint', N'003', CAST(N'2017-08-17 00:00:00.000' AS DateTime), N'Yes', CAST(5507.50 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (26, 13, N'General', N'Madman', N'566', CAST(N'2017-08-17 00:00:00.000' AS DateTime), N'Yes', CAST(50000.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (27, 14, N'dd', N'Ace', N'788', CAST(N'2017-08-25 00:00:00.000' AS DateTime), N'Yes', CAST(6000.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (28, 14, N'GLASS', N'DLLD', N'003', CAST(N'2017-08-25 00:00:00.000' AS DateTime), N'Yes', CAST(1180.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (29, 15, N'Solid top', N'3s', N'321', CAST(N'2017-08-25 00:00:00.000' AS DateTime), N'Yes', CAST(7180.00 AS Decimal(18, 2)), N'Unpaid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (30, 16, N'Item A', N'TDS', N'TDS', CAST(N'2017-10-19 00:00:00.000' AS DateTime), N'Yes', CAST(100.00 AS Decimal(18, 2)), N'Paid')
INSERT [dbo].[ProjectProjectExpenses] ([ID], [ProjectId], [Description], [SupplierName], [InvoiceNo], [Date], [GST], [Cost], [Status]) VALUES (35, 18, N'test1', N'1', N'1', CAST(N'2017-10-19 00:00:00.000' AS DateTime), N'Yes', CAST(1.00 AS Decimal(18, 2)), N'Unpaid')
SET IDENTITY_INSERT [dbo].[ProjectProjectExpenses] OFF
SET IDENTITY_INSERT [dbo].[Projects] ON 

INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (1, N'P1700001', N'NODC Pte Ltd', 1, 1, 1, CAST(N'2017-04-27 00:00:00.0000000' AS DateTime2), 6, N'Colin''s Project $1,200', N'Total Quotation of $1,200', N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-04-27 18:07:40.9041829' AS DateTime2), CAST(N'2017-04-27 18:07:40.9041829' AS DateTime2), CAST(N'2017-09-07 15:32:47.0668000' AS DateTime2), N'Y')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (2, N'P1700002', N'NODC Pte Ltd', 2, 1, 1, CAST(N'2017-04-27 00:00:00.0000000' AS DateTime2), 5, N'Bobby''s Project $1,500', N'Bobby''s Quotation $1,500', N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-05-27 18:09:07.8661945' AS DateTime2), CAST(N'2017-04-27 18:09:07.8661945' AS DateTime2), CAST(N'2017-09-07 15:32:56.0524000' AS DateTime2), N'Y')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (3, N'P1700003', N'NODC Pte Ltd', 3, 2, 1, CAST(N'2017-04-27 00:00:00.0000000' AS DateTime2), 3, N'Aaron''s Project $2,000', N'Aaron''s Quotation $2,000', N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-06-27 18:10:39.3977041' AS DateTime2), CAST(N'2017-04-27 18:10:39.3977041' AS DateTime2), CAST(N'2017-09-07 15:33:06.2080000' AS DateTime2), N'Y')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (4, N'P1700004', N'NODC Pte Ltd', 25, 7, 9, CAST(N'2017-08-16 00:00:00.0000000' AS DateTime2), 9, N'Project 123', NULL, N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-08-17 12:10:36.8696000' AS DateTime2), CAST(N'2017-08-16 17:45:56.1564000' AS DateTime2), CAST(N'2017-09-07 15:33:14.7724000' AS DateTime2), N'Y')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (5, N'P1700005', N'NODC Pte Ltd', 14, 11, 15, CAST(N'2017-08-16 00:00:00.0000000' AS DateTime2), 15, N'Project 124', NULL, N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-08-16 17:56:06.9588000' AS DateTime2), CAST(N'2017-08-16 17:56:06.9588000' AS DateTime2), CAST(N'2017-09-07 15:33:20.6692000' AS DateTime2), N'Y')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (6, N'P1700006', N'NODC Pte Ltd', 23, 4, 8, CAST(N'2017-08-17 00:00:00.0000000' AS DateTime2), 8, N'Project 123', NULL, N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-08-17 10:18:10.1192000' AS DateTime2), CAST(N'2017-08-17 10:18:10.1192000' AS DateTime2), CAST(N'2017-09-07 15:33:26.4568000' AS DateTime2), N'Y')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (7, N'P1700007', N'NODC Pte Ltd', 23, 4, 8, CAST(N'2017-08-17 00:00:00.0000000' AS DateTime2), 8, N'jnk', NULL, N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-08-17 12:10:45.4964000' AS DateTime2), CAST(N'2017-08-17 10:25:50.2880000' AS DateTime2), CAST(N'2017-09-07 15:33:31.5268000' AS DateTime2), N'Y')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (8, N'P1700008', N'NODC Pte Ltd', 23, 4, 8, CAST(N'2017-08-17 00:00:00.0000000' AS DateTime2), 8, N'Project 124', NULL, N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-08-17 12:10:56.1980000' AS DateTime2), CAST(N'2017-08-17 10:29:17.5340000' AS DateTime2), CAST(N'2017-09-07 15:33:49.4980000' AS DateTime2), N'Y')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (9, N'P1700009', N'NODC Pte Ltd', 26, 12, 11, CAST(N'2017-08-17 00:00:00.0000000' AS DateTime2), 11, N'ytijjs', NULL, N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-08-17 10:49:31.4636000' AS DateTime2), CAST(N'2017-08-17 10:49:31.4636000' AS DateTime2), CAST(N'2017-09-07 15:33:44.1160000' AS DateTime2), N'Y')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (10, N'P1700010', N'NODC Pte Ltd', 25, 7, 9, CAST(N'2017-08-17 00:00:00.0000000' AS DateTime2), 9, N'Project 124', NULL, N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-08-17 10:51:15.4688000' AS DateTime2), CAST(N'2017-08-17 10:51:15.4688000' AS DateTime2), CAST(N'2017-09-07 15:33:37.8448000' AS DateTime2), N'Y')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (11, N'P1700011', N'NODC Pte Ltd', 9, 8, 14, CAST(N'2017-08-17 00:00:00.0000000' AS DateTime2), 14, N'Project 124', NULL, N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-08-17 11:18:41.2064000' AS DateTime2), CAST(N'2017-08-17 11:18:41.2064000' AS DateTime2), CAST(N'2017-09-07 15:32:28.1284000' AS DateTime2), N'Y')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (12, N'P1700012', N'NODC Pte Ltd', 14, 13, 15, CAST(N'2017-08-17 00:00:00.0000000' AS DateTime2), 15, N'Project 1243', NULL, N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-08-17 11:43:53.9852000' AS DateTime2), CAST(N'2017-08-17 11:43:53.9852000' AS DateTime2), CAST(N'2017-09-07 15:32:23.9944000' AS DateTime2), N'Y')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (13, N'P1700013', N'NODC Pte Ltd', 14, 14, 15, CAST(N'2017-08-17 00:00:00.0000000' AS DateTime2), 15, N'Project 12436', NULL, N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-08-17 11:46:52.2152000' AS DateTime2), CAST(N'2017-08-17 11:46:52.2152000' AS DateTime2), CAST(N'2017-09-07 15:32:18.2692000' AS DateTime2), N'Y')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (14, N'P1700014', N'NODC Pte Ltd', 9, 8, 14, CAST(N'2017-08-25 00:00:00.0000000' AS DateTime2), 14, N'Project : Outdoor', NULL, N'Commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-08-25 11:08:20.4618000' AS DateTime2), CAST(N'2017-08-25 11:07:36.7194000' AS DateTime2), CAST(N'2017-09-07 15:32:00.1888000' AS DateTime2), N'Y')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (15, N'P1700015', N'NODC Pte Ltd', 23, 4, 8, CAST(N'2017-08-25 00:00:00.0000000' AS DateTime2), 8, N'Project :11111', NULL, N'Residential', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Completed', CAST(N'2017-08-25 11:54:51.3330000' AS DateTime2), CAST(N'2017-08-25 11:54:51.3330000' AS DateTime2), CAST(N'2017-09-07 15:31:55.0252000' AS DateTime2), N'Y')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (16, N'P1700001', N'NODC Pte Ltd', 37, 21, 2, CAST(N'2017-10-19 00:00:00.0000000' AS DateTime2), 12, N'Project A', NULL, N'Commercial', N'Cool-Blue-Sea-Pokemon-Wallpapers-HD_171019154238.jpg,Cool-Pokemon-HD-Wallpapers_171019154238.jpg,Cool-Pokemon-Wallpapers_171019154238.jpg', N'Cool-Pokemon-HD-Wallpapers_171019154246.jpg,Cool-Pokemon-Wallpapers_171019154246.jpg,Cool-Pokemon-Wallpapers-HD_171019154246.jpg', N'Cool-Blue-Sea-Pokemon-Wallpapers-HD_171019154259.jpg,Cool-Pokemon-Wallpapers_171019154259.jpg,Cool-Pokemon-Wallpapers-HD_171019154259.jpg', N'Cool-Blue-Sea-Pokemon-Wallpapers-HD_171019154306.jpg,Cool-Pokemon-Wallpapers_171019154306.jpg,Cool-Pokemon-Wallpapers-HD_171019154306.jpg', N'Cool-Blue-Sea-Pokemon-Wallpapers-HD_171019154310.jpg,Cool-Pokemon-HD-Wallpapers_171019154310.jpg', N'Cool-Pokemon-HD-Wallpapers_171019154317.jpg,Cool-Pokemon-Wallpapers-HD_171019154317.jpg', N'Test for File Upload', N'Active', NULL, CAST(N'2017-10-19 15:44:30.7270000' AS DateTime2), CAST(N'2017-10-19 16:21:58.6090000' AS DateTime2), N'Y')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (17, N'P1700017', N'NODC Pte Ltd', 37, 21, 2, CAST(N'2017-10-19 00:00:00.0000000' AS DateTime2), 12, N'Test', NULL, N'Commercial', N'1_-_3_C1___C2_171019163020.pdf,C.O.C_171019163020.pdf,CS3_171019163020.pdf,Equitment_Layout_Plan_171019163020.pdf,Form_C_171019163020.pdf,Form_C2_171019163020.pdf', N'Equitment_Layout_Plan_171019161932.pdf,Form_C_171019161932.pdf,Form_C2_171019161932.pdf', N'Keppel_Electric_Mar_17_Bill__inv_164584_171019161938.pdf,Keppel_meter_report_171019161938.pdf,NEA_-_Grains_171019161938.pdf', N'Single_line_diagram_171019161950.pdf,SLD_temporary_supply_171019161950.pdf,statement_of_turn_on_171019161950.pdf', N'Water_and_Electrical_Layout_Plan1_171019161959.pdf,Water_Bill_Apr_17_171019161959.pdf', NULL, NULL, N'Active', NULL, CAST(N'2017-10-19 16:20:34.3846000' AS DateTime2), CAST(N'2017-10-19 16:30:23.6746000' AS DateTime2), N'N')
INSERT [dbo].[Projects] ([ID], [ProjectID], [CompanyName], [QuotationReferenceId], [CustomerId], [PreparedById], [Date], [CoordinatorId], [ProjectTitle], [ProjectDescription], [ProjectType], [PhotoUploads], [FloorPlanUploads], [ThreeDDrawingUploads], [AsBuildDrawingUploads], [SubmissionStageUploads], [OtherUploads], [Remarks], [Status], [CompletedDate], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (18, N'P1700018', N'NODC Pte Ltd', 40, 17, 7, CAST(N'2017-10-19 00:00:00.0000000' AS DateTime2), 7, N'test', NULL, N'Commercial', N'Chrysanthemum_171019172319.jpg,Desert_171019172319.jpg,Hydrangeas_171019172319.jpg,Jellyfish_171019172319.jpg,Koala_171019172319.jpg,Lighthouse_171019172319.jpg,Penguins_171019172319.jpg,Tulips_171019172319.jpg', N'Chrysanthemum_171019165625.jpg,Desert_171019165625.jpg,Hydrangeas_171019165625.jpg,Jellyfish_171019165625.jpg,Koala_171019165625.jpg,Lighthouse_171019165625.jpg,Penguins_171019165625.jpg,Tulips_171019165625.jpg', N'Chrysanthemum_171019165630.jpg,Desert_171019165630.jpg,Hydrangeas_171019165630.jpg,Jellyfish_171019165630.jpg,Koala_171019165630.jpg,Lighthouse_171019165630.jpg,Penguins_171019165630.jpg,Tulips_171019165630.jpg', N'Chrysanthemum_171019165634.jpg,Desert_171019165634.jpg,Hydrangeas_171019165634.jpg,Jellyfish_171019165634.jpg,Koala_171019165634.jpg,Lighthouse_171019165634.jpg,Penguins_171019165634.jpg,Tulips_171019165634.jpg', N'Chrysanthemum_171019165640.jpg,Desert_171019165640.jpg,Hydrangeas_171019165640.jpg,Jellyfish_171019165640.jpg,Koala_171019165640.jpg,Lighthouse_171019165640.jpg,Penguins_171019165640.jpg,Tulips_171019165640.jpg', N'Chrysanthemum_171019165645.jpg,Desert_171019165645.jpg,Hydrangeas_171019165645.jpg,Jellyfish_171019165645.jpg,Koala_171019165645.jpg,Lighthouse_171019165645.jpg,Penguins_171019165645.jpg,Tulips_171019165645.jpg', NULL, N'Active', NULL, CAST(N'2017-10-19 16:56:56.2786000' AS DateTime2), CAST(N'2017-10-19 17:23:56.6974000' AS DateTime2), N'N')
SET IDENTITY_INSERT [dbo].[Projects] OFF
SET IDENTITY_INSERT [dbo].[QuotationCoBrokes] ON 

INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (1, 1, NULL, 6, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (2, 2, NULL, 5, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (3, 3, NULL, 2, 45)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (4, 3, NULL, 3, 10)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (5, 3, NULL, 1, 45)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (6, 4, NULL, 8, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (7, 5, NULL, 12, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (8, 6, NULL, 10, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (11, 9, NULL, 14, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (16, 14, NULL, 15, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (17, 15, NULL, 11, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (32, 16, NULL, 15, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (19, 17, NULL, 17, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (23, 21, NULL, 12, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (24, 22, NULL, 10, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (25, 23, NULL, 12, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (26, 24, NULL, 9, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (27, 25, NULL, 8, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (28, 26, NULL, 12, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (29, 27, NULL, 11, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (30, 28, NULL, 10, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (31, 29, NULL, 9, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (33, 30, NULL, 9, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (34, 31, NULL, 7, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (35, 32, NULL, 8, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (36, 19, NULL, 7, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (37, 33, NULL, 10, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (38, 34, NULL, 11, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (39, 35, NULL, 15, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (40, 36, NULL, 15, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (41, 37, NULL, 12, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (42, 38, NULL, 13, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (43, 39, NULL, 7, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (44, 40, NULL, 7, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (47, 41, NULL, 7, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (49, 42, NULL, 7, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (50, 43, NULL, 33, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (51, 44, NULL, 3, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (9, 7, NULL, 12, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (10, 8, NULL, 9, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (12, 10, NULL, 13, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (13, 11, NULL, 12, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (14, 12, NULL, 7, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (15, 13, NULL, 15, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (21, 19, NULL, 7, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (22, 20, NULL, 8, 100)
INSERT [dbo].[QuotationCoBrokes] ([ID], [QuotationId], [QuotationCoBrokeUserType], [QuotationCoBrokeUserId], [QuotationCoBrokePercentOfProfit]) VALUES (20, 18, NULL, 16, 100)
SET IDENTITY_INSERT [dbo].[QuotationCoBrokes] OFF
SET IDENTITY_INSERT [dbo].[QuotationItemDescriptions] ON 

INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (1, 1, 0, N'Preliminaries', N'Preliminaries', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (2, 1, 0, N'Authority Submission', N'Authority Submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (3, 1, 0, N'Partitioning & Ceiling Work', N'Partitioning & Ceiling Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (4, 1, 0, N'Flooring Work', N'Flooring Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (5, 1, 0, N'Painting Work', N'Painting Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (6, 1, 0, N'Mill Work', N'Mill Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (7, 1, 0, N'Glazing Work', N'Glazing Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (8, 1, 0, N'Electrical', N'Electrical', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (9, 1, 0, N'Plumbing & Sanitory', N'Plumbing & Sanitory', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (10, 1, 0, N'ACMV Work', N'ACMV Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (11, 1, 0, N'Fire Protection Work', N'Fire Protection Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (12, 1, 0, N'Miscellaneous', N'Miscellaneous', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (13, 2, 0, N'Preliminaries', N'Preliminaries 1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (14, 2, 0, N'Preliminaries', N'Preliminaries 2', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (15, 2, 0, N'Authority Submission', N'Authority Submission 1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (16, 2, 0, N'Authority Submission', N'Authority Submission 2', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (17, 2, 0, N'Partitioning & Ceiling Work', N'Partitioning & Ceiling Work 1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (18, 2, 0, N'Partitioning & Ceiling Work', N'Partitioning & Ceiling Work 2', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (19, 2, 0, N'Flooring Work', N'Flooring Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (20, 2, 0, N'Painting Work', N'Painting Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (21, 2, 0, N'Mill Work', N'Mill Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (22, 2, 0, N'Glazing Work', N'Glazing Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (23, 2, 0, N'Electrical', N'Electrical', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (24, 2, 0, N'Plumbing & Sanitory', N'Plumbing & Sanitory', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (25, 2, 0, N'ACMV Work', N'ACMV Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (26, 2, 0, N'Fire Protection Work', N'Fire Protection Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (27, 2, 0, N'Miscellaneous', N'Miscellaneous', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (28, 3, 0, N'Preliminaries', N'Preliminaries', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (29, 3, 0, N'Authority Submission', N'Authority Submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (30, 3, 0, N'Partitioning & Ceiling Work', N'Partitioning & Ceiling Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (31, 3, 0, N'Flooring Work', N'Flooring Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (32, 3, 0, N'Painting Work', N'Painting Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (33, 3, 0, N'Mill Work', N'Mill Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (34, 3, 0, N'Glazing Work', N'Glazing Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (35, 3, 0, N'Electrical', N'Electrical', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (36, 3, 0, N'Plumbing & Sanitory', N'Plumbing & Sanitory', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (37, 3, 0, N'ACMV Work', N'ACMV Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (38, 3, 0, N'Fire Protection Work', N'Fire Protection Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (39, 3, 0, N'Miscellaneous', N'Miscellaneous', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(900.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (40, 4, 0, N'PARTITION / WALL WORK', N'"Supply and install 75mm thick gypsum partition c/w plastering work and paint finishes (Compactus to be removed temporary by Client for the installation work)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(950.00 AS Decimal(18, 2)), CAST(950.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(250.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (41, 4, 0, N'PARTITION / WALL WORK', N'Supply and install selected range wallpaper (Easy-washed range)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2800.00 AS Decimal(18, 2)), CAST(2800.00 AS Decimal(18, 2)), CAST(1320.00 AS Decimal(18, 2)), CAST(1480.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (43, 4, 0, N'GLASS  WORK', N'Supply and install framing to glass as per existing office glass detail', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(600.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (46, 5, 0, N'Preliminaries', N'Design Consultancy including 3D for landlord submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (101, 7, 0, N'Builders Work', N'"Supply labour to install 9 set of wire mesh onto ceiling (Wire mesh by other)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1350.00 AS Decimal(18, 2)), CAST(1350.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(950.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (104, 7, 0, N'Electrical', N'Supply and install S/S/O', CAST(5.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (106, 7, 0, N'Electrical', N'Supply and install lighting point', CAST(9.0 AS Decimal(18, 1)), N'lot', CAST(90.00 AS Decimal(18, 2)), CAST(810.00 AS Decimal(18, 2)), CAST(630.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (108, 7, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (113, 8, 0, N'WALL WORK', N'"Supply and install 20mm thick laminate writing panel mounted onto existing framework. Dimension 1550 x 1300 Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(550.00 AS Decimal(18, 2)), CAST(550.00 AS Decimal(18, 2)), CAST(440.00 AS Decimal(18, 2)), CAST(110.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (115, 9, 0, N'Preliminaries', N'Design submission to landlord for approval including 3D', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (42, 4, 0, N'GLASS  WORK', N'Supply and install 10mm thick glass c/w u-channel onto existing wall and floor including frosted film (2pieces) "PS: Create swing door c/w necessary iron mongery.  (additional $350)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2200.00 AS Decimal(18, 2)), CAST(2200.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (44, 4, 0, N'FLOOR WORK', N'Supply and install 5mm thick vinyl clip system flooring including removal of existing carpet tiles', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2870.00 AS Decimal(18, 2)), CAST(2870.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(1870.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (45, 5, 0, N'Preliminaries', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(800.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (47, 5, 0, N'Preliminaries', N'Installation of 12mm thick gypsum hoarding c/w 1 no of swing door and white paint finishes up to 3000mm height', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2700.00 AS Decimal(18, 2)), CAST(2700.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (48, 5, 0, N'Preliminaries', N'Final Cleaning, Removal of hoarding and Clearing of Debris', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (49, 5, 0, N'Authority Submission', N'Building Plan Submission ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3750.00 AS Decimal(18, 2)), CAST(3750.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (50, 5, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel, and plastering work up to slab at 4200mm Ht Area: Leaseline and inter-tenancy wall"', CAST(700.0 AS Decimal(18, 1)), N'lot', CAST(4.50 AS Decimal(18, 2)), CAST(3150.00 AS Decimal(18, 2)), CAST(3100.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (52, 5, 0, N'Glazing Work', N'Supply and install 12mm thick tempered glass c/w u-channel support top and bottom, 2400mm Ht', CAST(125.0 AS Decimal(18, 1)), N'lot', CAST(16.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (54, 5, 0, N'Joinery Work', N'Supply and install 1500mm ht reception counter cabinet in laminate finishes an solid surface top', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1875.00 AS Decimal(18, 2)), CAST(1875.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(375.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (55, 5, 0, N'Joinery Work', N'"Supply and install sink cabinet in selected laminate finishes c/w solid surface finishing, 900mm ht (Sink and Faucet by other)"', CAST(16.0 AS Decimal(18, 1)), N'lot', CAST(250.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(3200.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (57, 5, 0, N'Wall and Floor Finishes', N'Supply labour and material to repaint entire area in emulsion white finishes c/w sealant', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (60, 5, 0, N'Air Conditioning Work', N'"To supply and install FCU ducts c/w 25mm thick 32kg insulation inclusive of night work charges PS: Landlord to approved on the tee-off point from FCU outside the unit"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5460.00 AS Decimal(18, 2)), CAST(5460.00 AS Decimal(18, 2)), CAST(4200.00 AS Decimal(18, 2)), CAST(1260.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (61, 5, 0, N'Air Conditioning Work', N'To supply and install 300mm dia flexible ducts c/w 4 ways diffuser in white colour finishing', CAST(9.0 AS Decimal(18, 1)), N'lot', CAST(300.00 AS Decimal(18, 2)), CAST(2700.00 AS Decimal(18, 2)), CAST(2250.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (62, 5, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On, Inclusive of sub main cable', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2550.00 AS Decimal(18, 2)), CAST(2550.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(550.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (63, 5, 0, N'Electrical', N'Supply and install T/S/O', CAST(8.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(880.00 AS Decimal(18, 2)), CAST(720.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (64, 5, 0, N'Electrical', N'Supply and install 15amp S/S/O', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(150.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (65, 5, 0, N'Electrical', N'Supply and install lighting point', CAST(16.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(1280.00 AS Decimal(18, 2)), CAST(960.00 AS Decimal(18, 2)), CAST(320.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (66, 5, 0, N'Electrical', N'Supply and install timer for shop front signage', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(60.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), CAST(45.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (67, 5, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (68, 5, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (71, 5, 0, N'Electrical', N'Supply labour and material to install light fitting', CAST(16.0 AS Decimal(18, 1)), N'lot', CAST(15.00 AS Decimal(18, 2)), CAST(240.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (72, 5, 0, N'Fire Protection Work and Plumbing Work', N'Supply and install 3 nos PVC outlet pipe and PPR inlet pipe as per SP requirement', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3500.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(2800.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (73, 6, 0, N'Preliminaries and Demolition Work', N'To supply labour for general cleaning work upon completion of renovation work including dismantling of partition', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (75, 6, 0, N'Preliminaries and Demolition Work', N'Design Consultancy including 3D for submission to landlord for permit to work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2300.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (77, 6, 0, N'Authority Submission (provision sum)', N'MAA Submission (Pending SCDF approval)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (79, 6, 0, N'Builders Work-Ceiling and Partition', N'Supply and install 19mm thick gypsum ceiling c/w cove light design at 3000Ht', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(7770.00 AS Decimal(18, 2)), CAST(7770.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(770.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (81, 6, 0, N'Builders Work-Foor Finishing', N'"Supply and install slected range carpet tiles PC Rate: $2/sqft range"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5895.00 AS Decimal(18, 2)), CAST(5895.00 AS Decimal(18, 2)), CAST(5171.00 AS Decimal(18, 2)), CAST(724.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (83, 6, 0, N'Builders Work-Carpentry', N'Supply labour and install 1200Ht x 1200ht reception counter in laminate finishing c/w tempered glass top', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1488.00 AS Decimal(18, 2)), CAST(512.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (51, 5, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel, and plastering work up to slab at 3000mm Ht Area: Internal partitioning wall"', CAST(510.0 AS Decimal(18, 1)), N'lot', CAST(4.00 AS Decimal(18, 2)), CAST(2040.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (53, 5, 0, N'Glazing Work', N'Supply and install 2100ht, 10mm thick tempered glass door c/w selected range floor spring, 600mm ht handle bar and lockset.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1500.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1360.00 AS Decimal(18, 2)), CAST(140.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (56, 5, 0, N'Joinery Work', N'Supply and install LED signage at shop front,', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3200.00 AS Decimal(18, 2)), CAST(3200.00 AS Decimal(18, 2)), CAST(2800.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (58, 5, 0, N'Wall and Floor Finishes', N'"Supply labour and material to screed up existing flooring (night delivery of material)"', CAST(1060.0 AS Decimal(18, 1)), N'lot', CAST(5.00 AS Decimal(18, 2)), CAST(5300.00 AS Decimal(18, 2)), CAST(4240.00 AS Decimal(18, 2)), CAST(1060.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (59, 5, 0, N'Wall and Floor Finishes', N'Supply labour and install selected range 3mm thick vinyl flooring', CAST(1060.0 AS Decimal(18, 1)), N'lot', CAST(4.00 AS Decimal(18, 2)), CAST(4240.00 AS Decimal(18, 2)), CAST(3180.00 AS Decimal(18, 2)), CAST(1060.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (69, 5, 0, N'Electrical', N'Lighting point include supply & install of E-light fitting', CAST(6.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(480.00 AS Decimal(18, 2)), CAST(360.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (70, 5, 0, N'Electrical', N'Lighting point include supply & install of Exit light fitting', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(240.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (74, 6, 0, N'Preliminaries and Demolition Work', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (76, 6, 0, N'Preliminaries and Demolition Work', N'Supply labour and material to demolished existing fixture and disposed off-site and terminate all electricity to DB', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4500.00 AS Decimal(18, 2)), CAST(4500.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (78, 6, 0, N'Authority Submission (provision sum)', N'Plan fees (Reimbursement)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(90.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (80, 6, 0, N'Builders Work-Ceiling and Partition', N'Supply and install 75mm thick gypsum partition using 50mm u-channel c/w 40kg rock wools up to 3000Ht', CAST(1430.0 AS Decimal(18, 1)), N'sqft', CAST(5.00 AS Decimal(18, 2)), CAST(7150.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (82, 6, 0, N'Builders Work-Foor Finishing', N'"Supply labour and material to overlay outdoor flooring inclusive tiles wastage PC Rate: $3/sqft range"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(6080.00 AS Decimal(18, 2)), CAST(6080.00 AS Decimal(18, 2)), CAST(6000.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (84, 6, 0, N'Builders Work-Carpentry', N'"Supply and install Top and bottom cabinet in laminate finishes and internal white polykem c/w  glass back splash and solid surface top PC Rate: solid surface $80/ftrun"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4700.00 AS Decimal(18, 2)), CAST(4700.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (85, 6, 0, N'Builders Work-Carpentry', N'Supply and install full height cabinet in selected laminate finishes and internal colour pvc c/w individual lockers and compartment to detail (up to 2400ht)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(8640.00 AS Decimal(18, 2)), CAST(8640.00 AS Decimal(18, 2)), CAST(8000.00 AS Decimal(18, 2)), CAST(640.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (86, 6, 0, N'Builders Work-Carpentry', N'Supply and install full height cabinet in selected laminate finishes and internal colour pvc c/w individual pigeon hole lockers and casement cabinet to detail (up to 2400ht)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (87, 6, 0, N'Builders Work-Carpentry', N'"Supply and install white board c/w marker holder to detail at meeting room 2000L x 1200Ht"', CAST(3.0 AS Decimal(18, 1)), N'nos', CAST(950.00 AS Decimal(18, 2)), CAST(2850.00 AS Decimal(18, 2)), CAST(2166.00 AS Decimal(18, 2)), CAST(684.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (88, 6, 0, N'Painting Work', N'Supply labour and materail to paint existing office using nippon paint', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4000.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (89, 6, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (90, 6, 0, N'Electrical', N'Supply and install S/S/O', CAST(3.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), CAST(230.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (91, 6, 0, N'Electrical', N'Supply and install T/S/O', CAST(35.0 AS Decimal(18, 1)), N'nos', CAST(110.00 AS Decimal(18, 2)), CAST(3850.00 AS Decimal(18, 2)), CAST(3150.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (92, 6, 0, N'Electrical', N'Supply and install lighting point for signage and LED Strip and Kebab Machine', CAST(22.0 AS Decimal(18, 1)), N'nos', CAST(80.00 AS Decimal(18, 2)), CAST(1760.00 AS Decimal(18, 2)), CAST(1600.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (93, 6, 0, N'Electrical', N'Supply and install Telephone Line (Non PABx System)', CAST(35.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(3100.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
GO
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (94, 6, 0, N'Electrical', N'Supply and install Cat 5E Data Line', CAST(35.0 AS Decimal(18, 1)), N'nos', CAST(110.00 AS Decimal(18, 2)), CAST(3850.00 AS Decimal(18, 2)), CAST(3700.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (95, 7, 0, N'Preliminaries', N'Design submission to landlord for approval including 3D', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (96, 7, 0, N'Preliminaries', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(800.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (97, 7, 0, N'Preliminaries', N'Supply labour and material to demolish hoarding and general cleaning upon completion', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(2700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (98, 7, 0, N'Authority Submission (provision sum)', N'Building Plan Submission ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2880.00 AS Decimal(18, 2)), CAST(2880.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), CAST(2230.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (99, 7, 0, N'Builders Work', N'"Supply labour and material to install racking and boltless shelves PS: assuming partition reinforcement still intact"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(750.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (100, 7, 0, N'Builders Work', N'Hanging 2 set of fitting room curtain', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(350.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), CAST(250.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (102, 7, 0, N'Builders Work', N'Supply and install grey exhibition carpet onto existing floor', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3120.00 AS Decimal(18, 2)), CAST(3120.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1120.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (103, 7, 0, N'Electrical', N'To alter existing DB c/w necessary MCB and MCCB including temp supply turn on by NODC appointed LEW, PE endorsement on Singleline drawing and perm turn on testing', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (105, 7, 0, N'Electrical', N'Supply and install T/S/O', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(220.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (107, 7, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (109, 8, 0, N'WALL WORK', N'"Supply and install 65mm thick acoustic wall panels c/w acoustic fabrics and fibre glass mounted onto existing framework. Dimension 2950 x 2600. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3240.00 AS Decimal(18, 2)), CAST(3240.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(2240.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (110, 8, 0, N'WALL WORK', N'"Supply and install 65mm thick acoustic wall panels c/w acoustic fabrics and fibre glass mounted onto existing framework. Dimension 1550 x 1300 Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(860.00 AS Decimal(18, 2)), CAST(860.00 AS Decimal(18, 2)), CAST(660.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (111, 8, 0, N'WALL WORK', N'"Supply and install 65mm thick acoustic wall panels c/w acoustic fabrics and fibre glass mounted onto existing framework. Dimension 1880 x 2080. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1640.00 AS Decimal(18, 2)), CAST(1640.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(1040.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (112, 8, 0, N'WALL WORK', N'"Supply and install 20mm thick laminate writing panel mounted onto existing framework. Dimension 2950 x 2600. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2075.00 AS Decimal(18, 2)), CAST(2075.00 AS Decimal(18, 2)), CAST(875.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (114, 8, 0, N'WALL WORK', N'"Supply and install 20mm thick laminate writing panel mounted onto existing framework. Dimension 1880 x 2080. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1050.00 AS Decimal(18, 2)), CAST(1050.00 AS Decimal(18, 2)), CAST(840.00 AS Decimal(18, 2)), CAST(210.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (116, 9, 0, N'Preliminaries', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(800.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (117, 9, 0, N'Preliminaries', N'Supply labour and material to demolish hoarding and general cleaning upon completion', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(2700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (120, 9, 0, N'Builders Work', N'Hanging 2 set of fitting room curtain', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(350.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), CAST(250.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (121, 9, 0, N'Builders Work', N'"Supply labour to install 9 set of wire mesh onto ceiling (Wire mesh by other)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1350.00 AS Decimal(18, 2)), CAST(1350.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (122, 9, 0, N'Builders Work', N'Supply and install grey exhibition carpet onto existing floor', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3120.00 AS Decimal(18, 2)), CAST(3120.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), CAST(720.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (124, 9, 0, N'Electrical', N'Supply and install S/S/O', CAST(5.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (125, 9, 0, N'Electrical', N'Supply and install T/S/O', CAST(2.0 AS Decimal(18, 1)), N'nos', CAST(110.00 AS Decimal(18, 2)), CAST(220.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (127, 9, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'no', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (128, 9, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (130, 10, 0, N'Preliminaries', N'Final Cleaning, Removal of hoarding and Clearing of Debris', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2200.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (133, 10, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel, single side double layer claded to existing wall, 80kg/m3 rockwool and plastering work up to 3650mm Ht Area: Non Mirror Side"', CAST(485.0 AS Decimal(18, 1)), N'lot', CAST(5.00 AS Decimal(18, 2)), CAST(2425.00 AS Decimal(18, 2)), CAST(2300.00 AS Decimal(18, 2)), CAST(125.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (135, 10, 0, N'Partition and Ceiling Work', N'"Supply and install 100mm thick gypsum partition wall c/w 50mm aluminium u-channel, double side double layer claded to wall, 80kg/m3 rockwool and plastering work up to 3650mm Ht Area: Mirror Side"', CAST(170.0 AS Decimal(18, 1)), N'lot', CAST(6.50 AS Decimal(18, 2)), CAST(1105.00 AS Decimal(18, 2)), CAST(884.00 AS Decimal(18, 2)), CAST(221.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (138, 10, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum drop down partition wall c/w 50mm aluminium u-channel and plastering work up to 2700mm Ht to 3650Ht Area: Mirror Side"', CAST(92.0 AS Decimal(18, 1)), N'lot', CAST(4.00 AS Decimal(18, 2)), CAST(368.00 AS Decimal(18, 2)), CAST(276.00 AS Decimal(18, 2)), CAST(92.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (140, 10, 0, N'Glazing Work', N'Supply and instal 2700mm ht, 12mm thick tempered glass c/w top and bottom embeded aluminium u-channel.', CAST(256.0 AS Decimal(18, 1)), N'lot', CAST(15.00 AS Decimal(18, 2)), CAST(3840.00 AS Decimal(18, 2)), CAST(3072.00 AS Decimal(18, 2)), CAST(768.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (141, 10, 0, N'Glazing Work', N'Supply and install 2100ht, 10mm thick tempered glass door c/w selected range floor spring, 600mm ht handle bar and lockset.', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(1250.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (143, 10, 0, N'Joinery Work', N'Supply and install 1200mm ht reception counter cabinet in laminate finishes an solid surface top', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1400.00 AS Decimal(18, 2)), CAST(1400.00 AS Decimal(18, 2)), CAST(1125.00 AS Decimal(18, 2)), CAST(275.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (145, 10, 0, N'Joinery Work', N'"Supply and install laminated door in laminate and mirror finishes Area: Reception Counter"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(750.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (118, 9, 0, N'Authority Submission (provision sum)Preparation of drawings for Authority Submission', N'Building Plan Submission ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2880.00 AS Decimal(18, 2)), CAST(2880.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(1880.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (119, 9, 0, N'Builders Work', N'"Supply labour and material to install racking and boltless shelves PS: assuming partition reinforcement still intact"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(750.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (123, 9, 0, N'Electrical', N'To alter existing DB c/w necessary MCB and MCCB including temp supply turn on by NODC appointed LEW, PE endorsement on Singleline drawing and perm turn on testing', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), CAST(1250.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (126, 9, 0, N'Electrical', N'Supply and install lighting point', CAST(9.0 AS Decimal(18, 1)), N'nos', CAST(90.00 AS Decimal(18, 2)), CAST(810.00 AS Decimal(18, 2)), CAST(530.00 AS Decimal(18, 2)), CAST(280.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (129, 10, 0, N'Preliminaries', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(800.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (131, 10, 0, N'Authority Submission', N'Building Plan Submission ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3600.00 AS Decimal(18, 2)), CAST(3600.00 AS Decimal(18, 2)), CAST(3286.00 AS Decimal(18, 2)), CAST(314.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (132, 10, 0, N'Authority Submission', N'QP endorsement and calculation on heat load calculation.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (134, 10, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel, single side double layer claded to existing wall, 80kg/m3 rockwool and plastering work up to 3650mm Ht Area: Mirror Side"', CAST(265.0 AS Decimal(18, 1)), N'lot', CAST(5.00 AS Decimal(18, 2)), CAST(1325.00 AS Decimal(18, 2)), CAST(1180.00 AS Decimal(18, 2)), CAST(145.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (136, 10, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel,  and plastering work up to 3650mm Ht Area: TV Wall Column"', CAST(40.0 AS Decimal(18, 1)), N'lot', CAST(4.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (137, 10, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel,  and plastering work up to 3650mm Ht Area: Counter Door Area"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(550.00 AS Decimal(18, 2)), CAST(550.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (139, 10, 0, N'Partition and Ceiling Work', N'"Supply and install 9mm thick gypsum ceiling c/w 50mm aluminium u-channel and plastering work to entire ceiling Area: General"', CAST(745.0 AS Decimal(18, 1)), N'lot', CAST(4.00 AS Decimal(18, 2)), CAST(2980.00 AS Decimal(18, 2)), CAST(2235.00 AS Decimal(18, 2)), CAST(745.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (142, 10, 0, N'Glazing Work', N'Supply and install 2700mm ht, 6mm thick clear mirror onto new erected partition', CAST(315.0 AS Decimal(18, 1)), N'lot', CAST(9.00 AS Decimal(18, 2)), CAST(2835.00 AS Decimal(18, 2)), CAST(2405.00 AS Decimal(18, 2)), CAST(430.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (144, 10, 0, N'Joinery Work', N'"Supply and install 950mm height MDF Spray painted bulk head Area: Outside Conference room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(6150.00 AS Decimal(18, 2)), CAST(6150.00 AS Decimal(18, 2)), CAST(5620.00 AS Decimal(18, 2)), CAST(530.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (146, 10, 0, N'Wall and Floor Finishes', N'Supply labour and material to repaint entire area in emulsion white finishes c/w sealant', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (147, 10, 0, N'Wall and Floor Finishes', N'"Supply labour and material to screed up existing flooring (night delivery of material)"', CAST(745.0 AS Decimal(18, 1)), N'lot', CAST(5.00 AS Decimal(18, 2)), CAST(3725.00 AS Decimal(18, 2)), CAST(2980.00 AS Decimal(18, 2)), CAST(745.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (148, 10, 0, N'Wall and Floor Finishes', N'Supply labour and material to apply self leveling screed to receive 5mm thick vinyl floor finishes', CAST(745.0 AS Decimal(18, 1)), N'lot', CAST(2.00 AS Decimal(18, 2)), CAST(1490.00 AS Decimal(18, 2)), CAST(1117.50 AS Decimal(18, 2)), CAST(372.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (149, 10, 0, N'Wall and Floor Finishes', N'Supply labour and install 5mm thick vinyl flooring', CAST(745.0 AS Decimal(18, 1)), N'lot', CAST(6.50 AS Decimal(18, 2)), CAST(4842.50 AS Decimal(18, 2)), CAST(3725.00 AS Decimal(18, 2)), CAST(1117.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (150, 10, 0, N'Air Conditioning Work', N'To supply and install 1 nos of FCU ducts c/w 25mm thick 32kg insulation c/w night work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2725.00 AS Decimal(18, 2)), CAST(2725.00 AS Decimal(18, 2)), CAST(2180.00 AS Decimal(18, 2)), CAST(545.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (151, 10, 0, N'Air Conditioning Work', N'To supply and install 300mm dia flexible ducts c/w 4 ways diffuser in white colour finishing', CAST(8.0 AS Decimal(18, 1)), N'lot', CAST(300.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (152, 10, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On, Inclusive of sub main cable', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2550.00 AS Decimal(18, 2)), CAST(2550.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(550.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (153, 10, 0, N'Electrical', N'Supply and install S/S/O', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), CAST(30.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (154, 10, 0, N'Electrical', N'Supply and install T/S/O', CAST(5.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(550.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (155, 10, 0, N'Electrical', N'Supply and install 15amp S/S/O', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(150.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (156, 10, 0, N'Electrical', N'Supply and install 20amp Single phase isolator', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(250.00 AS Decimal(18, 2)), CAST(250.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (157, 10, 0, N'Electrical', N'Supply and install lighting point', CAST(24.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(1920.00 AS Decimal(18, 2)), CAST(1440.00 AS Decimal(18, 2)), CAST(480.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (158, 10, 0, N'Electrical', N'Supply and install timer for 2 shop front signage', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(60.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(30.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (159, 10, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (160, 10, 0, N'Electrical', N'Supply and install Data Line', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(170.00 AS Decimal(18, 2)), CAST(30.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (161, 10, 0, N'Electrical', N'Lighting point include supply & install of E-light fitting', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (162, 10, 0, N'Electrical', N'Lighting point include supply & install of Exit light fitting', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(240.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (163, 10, 0, N'Electrical', N'Supply labour and material to install light fitting', CAST(24.0 AS Decimal(18, 1)), N'lot', CAST(15.00 AS Decimal(18, 2)), CAST(360.00 AS Decimal(18, 2)), CAST(240.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (164, 10, 0, N'Electrical', N'Supply labour and material to install signages', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(300.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (165, 11, 0, N'Preliminaries', N'"Supply labour and material to construct hoarding in 12mm thick gypsum board c/w swing door and emulsion paint  (By outgoing Tenant)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1326.00 AS Decimal(18, 2)), CAST(1326.00 AS Decimal(18, 2)), CAST(1020.00 AS Decimal(18, 2)), CAST(306.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (166, 11, 0, N'Preliminaries', N'"To supply and install graphics print to hoarding (Graphics by Owner)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(650.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), CAST(455.00 AS Decimal(18, 2)), CAST(195.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (167, 11, 0, N'Preliminaries', N'Public Liability Insurance ($2 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(600.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (168, 11, 0, N'Preliminaries', N'Design Consultancy including 3D for submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (169, 11, 0, N'Authority Submission (provision sum)', N'Building Plan Submission ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2880.00 AS Decimal(18, 2)), CAST(2880.00 AS Decimal(18, 2)), CAST(2880.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (170, 11, 0, N'Builders Work', N'Demolish existing partition and to to remove all unwanted fixtures and dispose off site', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(800.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (171, 11, 0, N'Builders Work', N'Touch up to damage ceiling work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(300.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (172, 11, 0, N'Builders Work', N'Supply and install new partition wall', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(767.00 AS Decimal(18, 2)), CAST(767.00 AS Decimal(18, 2)), CAST(590.00 AS Decimal(18, 2)), CAST(177.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (173, 11, 0, N'Builders Work', N'Supply and install shopfront counter in solid surface top and vinyl strip finishes c/w POS counter and flip top', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2899.00 AS Decimal(18, 2)), CAST(2899.00 AS Decimal(18, 2)), CAST(2230.00 AS Decimal(18, 2)), CAST(669.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (174, 11, 0, N'Builders Work', N'Supply and install back feature wall c/w conceal door in laminate finishes and nyatoh wood', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4797.00 AS Decimal(18, 2)), CAST(4797.00 AS Decimal(18, 2)), CAST(3690.00 AS Decimal(18, 2)), CAST(1107.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (175, 11, 0, N'Builders Work', N'Supply and install TV Brackets mounted onto wall', CAST(3.0 AS Decimal(18, 1)), N'nos', CAST(50.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (176, 11, 0, N'Builders Work', N'Supply and install My Banh Mi Logo', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (177, 11, 0, N'Builders Work', N'Supply and install stainless backing onto wall at BOH', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (178, 11, 0, N'Plumbing', N'Supply and install PPR water inlet and PVC outlet point including fees for license plumbing endorsement as per drawing', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (179, 11, 0, N'ACMV Work (optional)', N'Relocation of Exhaust hood', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(400.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (180, 11, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2400.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), CAST(1800.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (181, 11, 0, N'Electrical', N'Supply and install S/S/O', CAST(7.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(490.00 AS Decimal(18, 2)), CAST(210.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (182, 11, 0, N'Electrical', N'Supply and install T/S/O', CAST(4.0 AS Decimal(18, 1)), N'nos', CAST(110.00 AS Decimal(18, 2)), CAST(440.00 AS Decimal(18, 2)), CAST(360.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (183, 11, 0, N'Electrical', N'Supply and install 15amp S/S/O', CAST(4.0 AS Decimal(18, 1)), N'nos', CAST(150.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(360.00 AS Decimal(18, 2)), CAST(240.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (184, 11, 0, N'Electrical', N'Supply and install 20amp 3 phase isolator', CAST(1.0 AS Decimal(18, 1)), N'nos', CAST(320.00 AS Decimal(18, 2)), CAST(320.00 AS Decimal(18, 2)), CAST(250.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (185, 11, 0, N'Electrical', N'Supply and install lighting point for signage, carpentry, ceiling, and Kebab Machine', CAST(11.0 AS Decimal(18, 1)), N'nos', CAST(80.00 AS Decimal(18, 2)), CAST(880.00 AS Decimal(18, 2)), CAST(660.00 AS Decimal(18, 2)), CAST(220.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (186, 11, 0, N'Electrical', N'Supply and install timer for signage', CAST(1.0 AS Decimal(18, 1)), N'nos', CAST(80.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (187, 11, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'no', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (188, 11, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'no', CAST(110.00 AS Decimal(18, 2)), CAST(110.00 AS Decimal(18, 2)), CAST(105.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (189, 12, 0, N'Builders Work', N'"Supply and install Exhaust hood Cladding as per detail including mosiac tiles  Tiles PC rate @ $6.30 per sqft (Pending availability) (Exhaust hood by other)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (190, 12, 0, N'Builders Work', N'Supply and install 10mm thick tempered glass partition at 3 side of exhaust area', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1100.00 AS Decimal(18, 2)), CAST(1100.00 AS Decimal(18, 2)), CAST(742.50 AS Decimal(18, 2)), CAST(357.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (191, 12, 0, N'Builders Work', N'Supply and install front counter c/w tiles finishing, black stainless steel base inclusive of lavastone and mosiac tiles', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(6100.00 AS Decimal(18, 2)), CAST(6100.00 AS Decimal(18, 2)), CAST(4002.50 AS Decimal(18, 2)), CAST(2097.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (192, 12, 0, N'Builders Work', N'Supply and install 1 set of 10mm acrylic signages (logo and name)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (193, 12, 0, N'Builders Work', N'Supply and install 1 set of acrylic back lit signage (logo and name) at exhaust hood area', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(1700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (194, 12, 0, N'Builders Work', N'"Supply labour and material to construct subframe to suspend 2 nos of TV onto ceiling including TV bracket (TV by others)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (195, 12, 0, N'Builders Work', N'Supply and install POS counter c/w bottom cabinet in laminate finishes and internal white polykem', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1300.00 AS Decimal(18, 2)), CAST(1300.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (196, 12, 0, N'Plumbing', N'Copper water supply  & copper drainage pipe installation work including fees for license plumbing endorsement.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3600.00 AS Decimal(18, 2)), CAST(3600.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), N'"Note: 
Building Consultant fee, if any, to be borned by Client
PE endorsement will be billed directly to client with a 10% P&A
"
', NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (197, 12, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (198, 12, 0, N'Electrical', N'Supply and install S/S/O', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (199, 12, 0, N'Electrical', N'Supply and install T/S/O', CAST(6.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(660.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(460.00 AS Decimal(18, 2)), NULL, NULL)
GO
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (200, 12, 0, N'Electrical', N'Supply and install 32amp 3 phase isolator', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(475.00 AS Decimal(18, 2)), CAST(1425.00 AS Decimal(18, 2)), CAST(1140.00 AS Decimal(18, 2)), CAST(285.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (201, 12, 0, N'Electrical', N'Supply and install lighting point for signage and LED Strip and Kebab Machine', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (202, 12, 0, N'Electrical', N'Supply and install timer for signage', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (203, 12, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (204, 12, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(110.00 AS Decimal(18, 2)), CAST(105.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (205, 13, 0, N'Preliminaries', N'To supply labour for general cleaning work upon completion of renovation work including dismantling of partition', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(431.30 AS Decimal(18, 2)), CAST(568.70 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (206, 13, 0, N'Preliminaries', N'"To supply and install graphics print to hoarding (Design by Client)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1550.00 AS Decimal(18, 2)), CAST(1550.00 AS Decimal(18, 2)), CAST(690.00 AS Decimal(18, 2)), CAST(860.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (207, 13, 0, N'Preliminaries', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(700.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (208, 13, 0, N'Builders Work', N'"Supply and install Exhaust hood Cladding as per detail including mosiac tiles  Tiles PC rate @ $6.30 per sqft (Pending availability) (Exhaust hood by other)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(400.80 AS Decimal(18, 2)), CAST(2099.20 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (209, 13, 0, N'Builders Work', N'Supply and install 10mm thick tempered glass partition at 3 side of exhaust area', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1100.00 AS Decimal(18, 2)), CAST(1100.00 AS Decimal(18, 2)), CAST(840.00 AS Decimal(18, 2)), CAST(260.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (210, 13, 0, N'Builders Work', N'Supply and install front counter c/w tiles finishing, black stainless steel base inclusive of lavastone and mosiac tiles', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(6100.00 AS Decimal(18, 2)), CAST(6100.00 AS Decimal(18, 2)), CAST(4682.50 AS Decimal(18, 2)), CAST(1417.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (211, 13, 0, N'Builders Work', N'Supply and install 1 set of 10mm acrylic signages (logo and name)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (212, 13, 0, N'Builders Work', N'Supply and install 1 set of acrylic back lit signage (logo and name) at exhaust hood area', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(1700.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (213, 13, 0, N'Builders Work', N'"Supply labour and material to construct subframe to suspend 2 nos of TV onto ceiling including TV bracket (TV by others)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (214, 13, 0, N'Builders Work', N'Supply and install POS counter c/w bottom cabinet in laminate finishes and internal white polykem', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1300.00 AS Decimal(18, 2)), CAST(1300.00 AS Decimal(18, 2)), CAST(1100.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (215, 13, 0, N'Plumbing', N'Copper water supply  & copper drainage pipe installation work including fees for license plumbing endorsement.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3600.00 AS Decimal(18, 2)), CAST(3600.00 AS Decimal(18, 2)), CAST(2600.40 AS Decimal(18, 2)), CAST(999.60 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (216, 13, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (217, 13, 0, N'Electrical', N'Supply and install S/S/O', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(210.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (218, 13, 0, N'Electrical', N'Supply and install T/S/O', CAST(6.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(660.00 AS Decimal(18, 2)), CAST(540.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (219, 13, 0, N'Electrical', N'Supply and install 32amp 3 phase isolator', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(475.00 AS Decimal(18, 2)), CAST(1425.00 AS Decimal(18, 2)), CAST(1140.00 AS Decimal(18, 2)), CAST(285.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (220, 13, 0, N'Electrical', N'Supply and install lighting point for signage and LED Strip and Kebab Machine', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (221, 13, 0, N'Electrical', N'Supply and install timer for signage', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (222, 13, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (223, 13, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(110.00 AS Decimal(18, 2)), CAST(105.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (226, 14, 0, N'Preliminaries', N'Supply labour to do general cleaning during renovation and upon completion', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (228, 14, 0, N'Preliminaries', N'Public liability of 1 million coverage and Contractor All Risk', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (229, 14, 0, N'Preliminaries', N'Supply and installation of scaffold, 4000Ht', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (234, 14, 0, N'Demolition and Wet Work', N'Supply labour and material to install wall tiles including chipping of wall to screed up from 1800 to 2700 height', CAST(880.0 AS Decimal(18, 1)), N'lot', CAST(5.50 AS Decimal(18, 2)), CAST(4840.00 AS Decimal(18, 2)), CAST(3802.00 AS Decimal(18, 2)), CAST(1038.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (239, 15, 0, N'Preliminaries', N'Design Consultancy including 3D for MCST submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3500.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(3023.00 AS Decimal(18, 2)), CAST(477.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (224, 14, 0, N'Preliminaries', N'To design and and produce full drawing for construction including M&E planning for HDB approval', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(1980.50 AS Decimal(18, 2)), CAST(519.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (231, 14, 0, N'Demolition and Wet Work', N'Supply and install hollow block wall up to 2700Ht', CAST(885.0 AS Decimal(18, 1)), N'lot', CAST(8.00 AS Decimal(18, 2)), CAST(7080.00 AS Decimal(18, 2)), CAST(6817.00 AS Decimal(18, 2)), CAST(263.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (232, 14, 0, N'Demolition and Wet Work', N'"Supply labour and material to install floor tiles including chipping of floor to receive finishing (Tiles PC Sum: $3.00/sqft)"', CAST(1225.0 AS Decimal(18, 1)), N'lot', CAST(11.00 AS Decimal(18, 2)), CAST(13475.00 AS Decimal(18, 2)), CAST(10657.50 AS Decimal(18, 2)), CAST(2817.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (233, 14, 0, N'Demolition and Wet Work', N'"Supply labour and material to install wall tiles including chipping of wall to receive finishing up to 1800ht (Tiles PC Sum: $3.00/sqft)"', CAST(1775.0 AS Decimal(18, 1)), N'lot', CAST(11.00 AS Decimal(18, 2)), CAST(19525.00 AS Decimal(18, 2)), CAST(18000.50 AS Decimal(18, 2)), CAST(1524.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (236, 14, 0, N'False ceiling Works', N'Supply and install 600 x 1200 x 6mm thick calcium silicate ceiling board', CAST(1225.0 AS Decimal(18, 1)), N'lot', CAST(5.50 AS Decimal(18, 2)), CAST(6737.50 AS Decimal(18, 2)), CAST(6000.00 AS Decimal(18, 2)), CAST(737.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (237, 14, 0, N'Painting Works', N'To supply and apply 2coats of white matex paint including sealant to all wall', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4000.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(3200.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (240, 15, 0, N'Preliminaries', N'Site Supervision and project management', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (241, 15, 0, N'Preliminaries', N'Final Cleaning and Clearing of Debris', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (243, 15, 0, N'Demolition Work and Structural Work', N'Demolish existing parapet wall', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2285.00 AS Decimal(18, 2)), CAST(2285.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(285.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (244, 15, 0, N'Demolition Work and Structural Work', N'"Supply and install new mezzanine work c/w PE calculation and endorsement with 25mm plywood decking. Approximately 1,000 sqft BCA Submission Excluded due to government regulation on mezzanine"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(39000.00 AS Decimal(18, 2)), CAST(39000.00 AS Decimal(18, 2)), CAST(38000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (247, 15, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel, 40kg/m3 rockwool and plastering work to 3000mm Ht Area: Conference Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1725.00 AS Decimal(18, 2)), CAST(1725.00 AS Decimal(18, 2)), CAST(1380.00 AS Decimal(18, 2)), CAST(345.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (249, 15, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel and plastering work to 3000mm Ht Area: Pantry"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(470.00 AS Decimal(18, 2)), CAST(470.00 AS Decimal(18, 2)), CAST(375.00 AS Decimal(18, 2)), CAST(95.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (251, 15, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel and plastering work to 3000mm Ht Area: Stair Case Area"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1575.00 AS Decimal(18, 2)), CAST(1575.00 AS Decimal(18, 2)), CAST(1260.00 AS Decimal(18, 2)), CAST(315.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (252, 15, 0, N'Glazing Work', N'Supply and install 12mm thick tempered glass, 1200mm Ht along glass mezzanine edges c/w necessary structure support', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2080.00 AS Decimal(18, 2)), CAST(2080.00 AS Decimal(18, 2)), CAST(1900.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (253, 15, 0, N'Glazing Work', N'"Supply and install 12mm thick tempered glass along glass 1200mm Ht along mezzanine edges c/w subframe support from ceiling slab 3700L x 2400Ht Area: Conference Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3840.00 AS Decimal(18, 2)), CAST(3840.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(340.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (259, 15, 0, N'Builders Work', N'Supply and install low height standalone cabinet in laminate finishes c/w ABS edging and 4-sides exposed laminate', CAST(6.0 AS Decimal(18, 1)), N'ftrun', CAST(145.00 AS Decimal(18, 2)), CAST(870.00 AS Decimal(18, 2)), CAST(110.00 AS Decimal(18, 2)), CAST(760.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (261, 15, 0, N'Builders Work', N'"Supply and install 2400Ht store cabinet in laminate finishes and ABS edging Area: Stairs Area"', CAST(14.0 AS Decimal(18, 1)), N'ftrun', CAST(320.00 AS Decimal(18, 2)), CAST(4480.00 AS Decimal(18, 2)), CAST(4400.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (262, 15, 0, N'Wall and Floor Finishes', N'Supply labour and material to repaint entire area in emulsion white finishes c/w sealant', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3750.00 AS Decimal(18, 2)), CAST(3750.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (263, 15, 0, N'Wall and Floor Finishes', N'Supply and install selected range wallpaper finishing at selected mezzanine area', CAST(550.0 AS Decimal(18, 1)), N'sqft', CAST(3.50 AS Decimal(18, 2)), CAST(1925.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(425.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (265, 15, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On, Inclusive of sub main cable', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3300.00 AS Decimal(18, 2)), CAST(3300.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (266, 15, 0, N'Electrical', N'Supply and install T/S/O', CAST(18.0 AS Decimal(18, 1)), N'nos', CAST(110.00 AS Decimal(18, 2)), CAST(1980.00 AS Decimal(18, 2)), CAST(1620.00 AS Decimal(18, 2)), CAST(360.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (267, 15, 0, N'Electrical', N'Supply and install lighting point', CAST(32.0 AS Decimal(18, 1)), N'nos', CAST(80.00 AS Decimal(18, 2)), CAST(2560.00 AS Decimal(18, 2)), CAST(2300.00 AS Decimal(18, 2)), CAST(260.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (268, 15, 0, N'Electrical', N'Supply and install Telephone Line', CAST(15.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1475.00 AS Decimal(18, 2)), CAST(25.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (270, 15, 0, N'Electrical', N'Supply labour and material to install light fitting', CAST(32.0 AS Decimal(18, 1)), N'nos', CAST(15.00 AS Decimal(18, 2)), CAST(480.00 AS Decimal(18, 2)), CAST(320.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (271, 15, 0, N'Fire Protection Work', N'Design and build fire sprinkler work (Water discharge fees will be borne by client)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5900.00 AS Decimal(18, 2)), CAST(5900.00 AS Decimal(18, 2)), CAST(5700.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (538, 16, 0, N'Builders Work', N'Supply and install back wall, 1000mm height c/w black solid surface', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2950.00 AS Decimal(18, 2)), CAST(2950.00 AS Decimal(18, 2)), CAST(2357.50 AS Decimal(18, 2)), CAST(592.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (225, 14, 0, N'Preliminaries', N'Supply labour and material to protect existing mall flooring and common properties', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (227, 14, 0, N'Preliminaries', N'Project Management Fees', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3500.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (230, 14, 0, N'Demolition and Wet Work', N'Demolition of existing tiles in toilet area including removal of toilet fitting', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1550.00 AS Decimal(18, 2)), CAST(1550.00 AS Decimal(18, 2)), CAST(1300.00 AS Decimal(18, 2)), CAST(250.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (235, 14, 0, N'Carpentry', N'To supply and install single leaf laminate door including door frame and iron mongery', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(3600.00 AS Decimal(18, 2)), CAST(3050.00 AS Decimal(18, 2)), CAST(550.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (238, 15, 0, N'Preliminaries', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (242, 15, 0, N'Authority Submission', N'Building Plan Submission ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3600.00 AS Decimal(18, 2)), CAST(3600.00 AS Decimal(18, 2)), CAST(3300.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (245, 15, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel, 40kg/m3 rockwool and plastering work up to 3000mm Ht Area: CEO Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1525.00 AS Decimal(18, 2)), CAST(1525.00 AS Decimal(18, 2)), CAST(1420.00 AS Decimal(18, 2)), CAST(105.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (246, 15, 0, N'Partition and Ceiling Work', N'"Supply and install 9mm thick gypsum ceiling c/w 40kg rockwool and plastering work Area: CEO Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(440.00 AS Decimal(18, 2)), CAST(440.00 AS Decimal(18, 2)), CAST(352.00 AS Decimal(18, 2)), CAST(88.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (248, 15, 0, N'Partition and Ceiling Work', N'"Supply and install 9mm thick gypsum ceiling c/w 40kg rockwool and plastering work Area: Conference Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1150.00 AS Decimal(18, 2)), CAST(1150.00 AS Decimal(18, 2)), CAST(920.00 AS Decimal(18, 2)), CAST(230.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (250, 15, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel and plastering work to 3000mm Ht Area: Reception Area"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(470.00 AS Decimal(18, 2)), CAST(470.00 AS Decimal(18, 2)), CAST(375.00 AS Decimal(18, 2)), CAST(95.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (254, 15, 0, N'Glazing Work', N'"Supply and install 10mm thick tempered glass screen  1200Ht x 1000L Area: CEO Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(180.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(130.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (255, 15, 0, N'Builders Work', N'Supply and install top hung and bottom pantry cabinet in laminate finishes c/w soft close hinge and solid surface top', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4200.00 AS Decimal(18, 2)), CAST(4200.00 AS Decimal(18, 2)), CAST(4060.00 AS Decimal(18, 2)), CAST(140.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (256, 15, 0, N'Builders Work', N'"Supply and install 40mm thick, 4 -seaters high bench in laminate finishes and ABS edging Area: Outside Conference room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(570.00 AS Decimal(18, 2)), CAST(570.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (257, 15, 0, N'Builders Work', N'"Supply and install glass writing wall c/w stainless marker tray 1500Ht x 1200L"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(875.00 AS Decimal(18, 2)), CAST(875.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(175.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (258, 15, 0, N'Builders Work', N'Supply and install low height standalone cabinet in laminate finishes c/w ABS edging and 5-sides exposed laminate', CAST(27.0 AS Decimal(18, 1)), N'ftrun', CAST(175.00 AS Decimal(18, 2)), CAST(4725.00 AS Decimal(18, 2)), CAST(4500.00 AS Decimal(18, 2)), CAST(225.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (260, 15, 0, N'Builders Work', N'"Supply and install 2400Ht filing cabinet in laminate finishes and ABS edging Area: Filing Room, Level 7 main office area"', CAST(35.0 AS Decimal(18, 1)), N'ftrun', CAST(320.00 AS Decimal(18, 2)), CAST(11200.00 AS Decimal(18, 2)), CAST(11000.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (264, 15, 0, N'Wall and Floor Finishes', N'Supply and install selected range 600 by 600 carpet tiles onto both mezzanine and Level 7', CAST(2280.0 AS Decimal(18, 1)), N'sqft', CAST(3.50 AS Decimal(18, 2)), CAST(7980.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(980.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (269, 15, 0, N'Electrical', N'Supply and install Data Line', CAST(15.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1275.00 AS Decimal(18, 2)), CAST(225.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (533, 16, 0, N'Builders Work', N'Supply and install Shop frontage and side wall in pure black solid surface top 100mm drop 40mm, graphics diamond print and pure black solid surface skirting 100mm turn in 20mm including cowboy/flip door and LED Strip', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(7450.00 AS Decimal(18, 2)), CAST(7450.00 AS Decimal(18, 2)), CAST(2430.00 AS Decimal(18, 2)), CAST(5020.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (535, 16, 0, N'Builders Work', N'Supply and install double side light box with internal leD, front and back printed rainbow strips direct paste on and 3D mini acrylic lighted texts install at both front and back', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3800.00 AS Decimal(18, 2)), CAST(3800.00 AS Decimal(18, 2)), CAST(2980.00 AS Decimal(18, 2)), CAST(820.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (539, 16, 0, N'Builders Work', N'Supply and install stainless steel back for back wall, 6150 L x 1000mm height including cutting of profile for M&E services.', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(300.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (540, 16, 0, N'Builders Work', N'Supply wall mount TV Bracket including installation of 3 x 43" TV onto existing framework', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(60.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (541, 16, 0, N'Electrical', N'Supply and install S/S/O', CAST(4.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(280.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (542, 16, 0, N'Electrical', N'Supply and install T/S/O', CAST(5.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(550.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (543, 16, 0, N'Electrical', N'Supply and install 15amp S/S/O', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(150.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (544, 16, 0, N'Electrical', N'Supply and install DP swtich for TV and Signages', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(65.00 AS Decimal(18, 2)), CAST(130.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(30.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (545, 16, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (546, 16, 0, N'Electrical', N'Supply and install 20amp 3 phase isolator', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(320.00 AS Decimal(18, 2)), CAST(640.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(140.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (547, 16, 0, N'Electrical', N'Supply and install lighting point for signage and carpentry', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(240.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (548, 16, 0, N'Electrical', N'Supply and install timer for signage', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (549, 16, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (550, 16, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(110.00 AS Decimal(18, 2)), CAST(105.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (551, 16, 0, N'Plumbing', N'Supply and install PPR water inlet and PVC outlet point including fees for license plumbing endorsement as per drawing', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1700.00 AS Decimal(18, 2)), CAST(1700.00 AS Decimal(18, 2)), CAST(1300.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (559, 31, 0, N'Builders Work', N'Supply and install 10mm thick tempered glass partition at 3 side of exhaust area', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1100.00 AS Decimal(18, 2)), CAST(1100.00 AS Decimal(18, 2)), CAST(742.50 AS Decimal(18, 2)), CAST(357.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (560, 31, 0, N'Builders Work', N'Supply and install front counter c/w tiles finishing, black stainless steel base inclusive of lavastone and mosiac tiles', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(6100.00 AS Decimal(18, 2)), CAST(6100.00 AS Decimal(18, 2)), CAST(4002.50 AS Decimal(18, 2)), CAST(2097.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (561, 31, 0, N'Builders Work', N'Supply and install 1 set of 10mm acrylic signages (logo and name)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (562, 31, 0, N'Builders Work', N'Supply and install 1 set of acrylic back lit signage (logo and name) at exhaust hood area', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(1700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (294, 17, 0, N'Preliminaries', N'To supply labour for general cleaning work upon completion of renovation work.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1500.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1300.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (296, 17, 0, N'Preliminaries', N'Design Fees for Submission to necessary authorities for permit to work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1800.00 AS Decimal(18, 2)), CAST(1800.00 AS Decimal(18, 2)), CAST(1500.18 AS Decimal(18, 2)), CAST(299.82 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (297, 17, 0, N'Authority Submission', N'Building Plan design and submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(7800.00 AS Decimal(18, 2)), CAST(7800.00 AS Decimal(18, 2)), CAST(6900.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (299, 17, 0, N'Wet Trade', N'Supply and install cement screed flooring for shop front.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4200.00 AS Decimal(18, 2)), CAST(4200.00 AS Decimal(18, 2)), CAST(3800.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (300, 17, 0, N'Wet Trade', N'"Supply and install floor tiles for new kitchen area including 10% wastage (PC rate: $3.50/sqft tiles)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2300.00 AS Decimal(18, 2)), CAST(2300.00 AS Decimal(18, 2)), CAST(2025.20 AS Decimal(18, 2)), CAST(274.80 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (301, 17, 0, N'Painting', N'Supply labour and material to paint exposed wall and ceiling in matex white finishing', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1600.00 AS Decimal(18, 2)), CAST(1600.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (303, 17, 0, N'Carpentry', N'Supply and install T.V bracket. (T.V to be provided by Owner)', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(200.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(375.00 AS Decimal(18, 2)), CAST(225.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (304, 17, 0, N'Carpentry', N'Supply and install shop counter low height wooden partition in selected laminated finishing c/w solid surface top and swing door.', CAST(34.0 AS Decimal(18, 1)), N'run', CAST(180.00 AS Decimal(18, 2)), CAST(6120.00 AS Decimal(18, 2)), CAST(4590.00 AS Decimal(18, 2)), CAST(1530.00 AS Decimal(18, 2)), NULL, NULL)
GO
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (306, 17, 0, N'Carpentry', N'Supply and install low ht. storage cabinet in laminated finishing.', CAST(7.0 AS Decimal(18, 1)), N'ftrun', CAST(182.14 AS Decimal(18, 2)), CAST(1274.98 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(274.98 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (308, 17, 0, N'Carpentry', N'Supply and install open display/storage cabinet in laminated finishing up to 2400mmHt', CAST(7.0 AS Decimal(18, 1)), N'ftrun', CAST(321.43 AS Decimal(18, 2)), CAST(2250.01 AS Decimal(18, 2)), CAST(2050.00 AS Decimal(18, 2)), CAST(200.01 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (310, 17, 0, N'Plumbing', N'Supply labour to dismantle existing sink and reconnect back', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(200.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (312, 17, 0, N'ACMV', N'To supply and install 3500cmh exhaust fan (Hood by Others)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4000.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(3200.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (313, 17, 0, N'ACMV', N'To supply and install 2500 cmh air cleaner', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3125.00 AS Decimal(18, 2)), CAST(3125.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(625.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (316, 18, 0, N'3D Rendering Work', N'Shop drawing for', CAST(18.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(1800.00 AS Decimal(18, 2)), CAST(1660.00 AS Decimal(18, 2)), CAST(140.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (318, 18, 0, N'Joinery Works', N'Item 1: Supply and install Reception Feature wall in laminate finishes', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(2800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (319, 18, 0, N'Joinery Works', N'Item 14: Supply and install PALACE signage logo c/w LED Strip', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3900.00 AS Decimal(18, 2)), CAST(3900.00 AS Decimal(18, 2)), CAST(3700.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (321, 18, 0, N'Joinery Works', N'Item 3: Supply and install dressing table c/w mirror in laminate finishes 7.5ft long', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1350.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (322, 18, 0, N'Joinery Works', N'Item 4: Supply and install Singer''s cabinet  in laminate finishes 11ft long', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3520.00 AS Decimal(18, 2)), CAST(3520.00 AS Decimal(18, 2)), CAST(2750.00 AS Decimal(18, 2)), CAST(770.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (323, 18, 0, N'Joinery Works', N'Item 5: Supply and install TV Console in laminate finishes 8ft long', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1440.00 AS Decimal(18, 2)), CAST(1440.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(640.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (324, 18, 0, N'Joinery Works', N'Item 6: Supply and install TV Console in laminate finishes', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(750.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1490.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (327, 18, 0, N'Joinery Works', N'Item 9: Supply and install box up to existing inlet and outlet pipe', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (328, 18, 0, N'Joinery Works', N'Item 10: Supply and install TV feature wall in selected laminate finishes', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2750.00 AS Decimal(18, 2)), CAST(2750.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (329, 18, 0, N'Joinery Works', N'Item 11: Supply and install mini bar counter in selected laminate finishes', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1050.00 AS Decimal(18, 2)), CAST(1050.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (330, 18, 0, N'Joinery Works', N'Item 12: Supply and install wooden platform in laminate finishes', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(700.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (298, 17, 0, N'Authority Submission', N'Plan fee reimbursement x 3 nos', CAST(3.0 AS Decimal(18, 1)), N'nos', CAST(90.00 AS Decimal(18, 2)), CAST(270.00 AS Decimal(18, 2)), CAST(272.79 AS Decimal(18, 2)), CAST(-2.79 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (302, 17, 0, N'Painting', N'Supply labour and material to paint matt/gloss lacquer over cement screed flooring', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(900.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (305, 17, 0, N'Carpentry', N'Supply and install POS counter in selected laminated finish c/w solid surface top.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1100.00 AS Decimal(18, 2)), CAST(1100.00 AS Decimal(18, 2)), CAST(910.00 AS Decimal(18, 2)), CAST(190.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (307, 17, 0, N'Carpentry', N'Supply and install top hung display/storage cabinet in laminated finishing up to 900mmHt', CAST(26.0 AS Decimal(18, 1)), N'ftrun', CAST(173.27 AS Decimal(18, 2)), CAST(4505.02 AS Decimal(18, 2)), CAST(4180.00 AS Decimal(18, 2)), CAST(325.02 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (311, 17, 0, N'ACMV', N'Supply labour and material to construct exhaust duct out of unit ending at the existing ducting point', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (314, 17, 0, N'Gas and Fire Supression Work', N'Supply and install 2.5gallon wet chemical system to exhaust hood est size (1200 x 750 x 500ht) up to max 8 nozzle complete with "PSB" and piping work accessories', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3850.00 AS Decimal(18, 2)), CAST(3850.00 AS Decimal(18, 2)), CAST(2950.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (315, 17, 0, N'Glass', N'Supply and install 12mm thickness clear tempered glass panel c/w 2 swing door with floor spring and selected range iron mongeries and lock set', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5050.00 AS Decimal(18, 2)), CAST(5050.00 AS Decimal(18, 2)), CAST(4040.00 AS Decimal(18, 2)), CAST(1010.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (317, 18, 0, N'3D Rendering Work', N'Design and 3D for the following carpentry', CAST(11.0 AS Decimal(18, 1)), N'lot', CAST(200.00 AS Decimal(18, 2)), CAST(2200.00 AS Decimal(18, 2)), CAST(1650.00 AS Decimal(18, 2)), CAST(550.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (320, 18, 0, N'Joinery Works', N'Item 2: Supply and install L-shape DJ console in laminate finishes 12ft long', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3240.00 AS Decimal(18, 2)), CAST(3240.00 AS Decimal(18, 2)), CAST(2300.00 AS Decimal(18, 2)), CAST(940.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (325, 18, 0, N'Joinery Works', N'Item 7: Supply and install Vanity Top in laminate finishes and solid surface finishes including mirror excluding sink and tap', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(2150.00 AS Decimal(18, 2)), CAST(850.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (331, 18, 0, N'Joinery Works', N'Item 13: Supply and install shopfront signage', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(7000.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(6700.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (334, 18, 0, N'Joinery Works', N'Double Leaf Door', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (337, 18, 0, N'Gas Work', N'Supply labour and material to alter gas pipe including necessary gas leak detection system and solenoid valve', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(15000.00 AS Decimal(18, 2)), CAST(15000.00 AS Decimal(18, 2)), CAST(13000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (338, 19, 0, N'Preliminaries', N'Waa', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (339, 19, 0, N'Authority Submission', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (340, 19, 0, N'Partitioning and Ceiling Work', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (341, 19, 0, N'Flooring Work', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (342, 19, 0, N'Painting Work', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (343, 19, 0, N'Mill Work', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (344, 19, 0, N'Glazing Work', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (345, 19, 0, N'Electrical', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (346, 19, 0, N'Plumbing & Sanitory', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (347, 19, 0, N'ACMV Work', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (348, 19, 0, N'Fire Protection Work', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (349, 19, 0, N'Miscellaneous', N'', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (350, 20, 0, N'Preliminaries', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (351, 20, 0, N'Authority Submission', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (352, 20, 0, N'Partitioning and Ceiling Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (353, 20, 0, N'Flooring Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (354, 20, 0, N'Painting Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (355, 20, 0, N'Mill Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (356, 20, 0, N'Glazing Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (357, 20, 0, N'Electrical', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (358, 20, 0, N'Plumbing & Sanitory', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (359, 20, 0, N'ACMV Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (360, 20, 0, N'Fire Protection Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (361, 20, 0, N'Miscellaneous', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (295, 17, 0, N'Preliminaries', N'Provision of Site Supervisor to supervise site progress and to liaise with direct supplier, i.e Kitchen Specialise, CCTV, Furniture etc', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1700.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (309, 17, 0, N'Plumbing', N'Supply and install PPR inlet and PVC drainage pipe. Installation work including fees for license plumbing endorsement and water turn on fees', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3500.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(3101.84 AS Decimal(18, 2)), CAST(398.16 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (326, 18, 0, N'Joinery Works', N'Item 8: Supply and install POS counter in selected laminate finishes', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(900.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (333, 18, 0, N'Joinery Works', N'Single Leaf Door', CAST(4.0 AS Decimal(18, 1)), N'lot', CAST(750.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(2700.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (335, 18, 0, N'Joinery Works', N'Supply and install Sliding Door', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (332, 18, 0, N'Joinery Works', N'Item 18: Supply and install Coat Hangar Cabinet 9 ft long in laminate finishes', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3500.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(3250.00 AS Decimal(18, 2)), CAST(250.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (336, 18, 0, N'Joinery Works', N'Supply and install Louver Door', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1600.00 AS Decimal(18, 2)), CAST(1600.00 AS Decimal(18, 2)), CAST(1300.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (362, 21, 0, N'Preliminaries', N'Design Consultancy including 3D for landlord submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (363, 21, 0, N'Preliminaries', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(800.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (364, 21, 0, N'Preliminaries', N'Installation of 12mm thick gypsum hoarding c/w 1 no of swing door and white paint finishes up to 3000mm height', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2700.00 AS Decimal(18, 2)), CAST(2700.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (365, 21, 0, N'Preliminaries', N'Final Cleaning, Removal of hoarding and Clearing of Debris', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (366, 21, 0, N'Authority Submission', N'Building Plan Submission ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3750.00 AS Decimal(18, 2)), CAST(3750.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (367, 21, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel, and plastering work up to slab at 4200mm Ht Area: Leaseline and inter-tenancy wall"', CAST(700.0 AS Decimal(18, 1)), N'lot', CAST(4.50 AS Decimal(18, 2)), CAST(3150.00 AS Decimal(18, 2)), CAST(3100.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (368, 21, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel, and plastering work up to slab at 3000mm Ht Area: Internal partitioning wall"', CAST(510.0 AS Decimal(18, 1)), N'lot', CAST(4.00 AS Decimal(18, 2)), CAST(2040.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (369, 21, 0, N'Glazing Work', N'Supply and install 12mm thick tempered glass c/w u-channel support top and bottom, 2400mm Ht', CAST(125.0 AS Decimal(18, 1)), N'lot', CAST(16.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (370, 21, 0, N'Glazing Work', N'Supply and install 2100ht, 10mm thick tempered glass door c/w selected range floor spring, 600mm ht handle bar and lockset.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1500.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1360.00 AS Decimal(18, 2)), CAST(140.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (371, 21, 0, N'Joinery Work', N'Supply and install 1500mm ht reception counter cabinet in laminate finishes an solid surface top', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1875.00 AS Decimal(18, 2)), CAST(1875.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(375.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (372, 21, 0, N'Joinery Work', N'"Supply and install sink cabinet in selected laminate finishes c/w solid surface finishing, 900mm ht (Sink and Faucet by other)"', CAST(16.0 AS Decimal(18, 1)), N'lot', CAST(250.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(3200.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (373, 21, 0, N'Joinery Work', N'Supply and install LED signage at shop front,', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3200.00 AS Decimal(18, 2)), CAST(3200.00 AS Decimal(18, 2)), CAST(2800.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (374, 21, 0, N'Wall and Floor Finishes', N'Supply labour and material to repaint entire area in emulsion white finishes c/w sealant', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (375, 21, 0, N'Wall and Floor Finishes', N'"Supply labour and material to screed up existing flooring (night delivery of material)"', CAST(1060.0 AS Decimal(18, 1)), N'lot', CAST(5.00 AS Decimal(18, 2)), CAST(5300.00 AS Decimal(18, 2)), CAST(4240.00 AS Decimal(18, 2)), CAST(1060.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (376, 21, 0, N'Wall and Floor Finishes', N'Supply labour and install selected range 3mm thick vinyl flooring', CAST(1060.0 AS Decimal(18, 1)), N'lot', CAST(4.00 AS Decimal(18, 2)), CAST(4240.00 AS Decimal(18, 2)), CAST(3180.00 AS Decimal(18, 2)), CAST(1060.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (377, 21, 0, N'Air Conditioning Work', N'"To supply and install FCU ducts c/w 25mm thick 32kg insulation inclusive of night work charges PS: Landlord to approved on the tee-off point from FCU outside the unit"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5460.00 AS Decimal(18, 2)), CAST(5460.00 AS Decimal(18, 2)), CAST(4200.00 AS Decimal(18, 2)), CAST(1260.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (378, 21, 0, N'Air Conditioning Work', N'To supply and install 300mm dia flexible ducts c/w 4 ways diffuser in white colour finishing', CAST(9.0 AS Decimal(18, 1)), N'lot', CAST(300.00 AS Decimal(18, 2)), CAST(2700.00 AS Decimal(18, 2)), CAST(2250.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (379, 21, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On, Inclusive of sub main cable', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2550.00 AS Decimal(18, 2)), CAST(2550.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(550.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (380, 21, 0, N'Electrical', N'Supply and install T/S/O', CAST(8.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(880.00 AS Decimal(18, 2)), CAST(720.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (381, 21, 0, N'Electrical', N'Supply and install 15amp S/S/O', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(150.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (382, 21, 0, N'Electrical', N'Supply and install lighting point', CAST(16.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(1280.00 AS Decimal(18, 2)), CAST(960.00 AS Decimal(18, 2)), CAST(320.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (383, 21, 0, N'Electrical', N'Supply and install timer for shop front signage', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(60.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), CAST(45.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (384, 21, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (385, 21, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (386, 21, 0, N'Electrical', N'Supply labour and material to install light fitting', CAST(16.0 AS Decimal(18, 1)), N'lot', CAST(15.00 AS Decimal(18, 2)), CAST(240.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (387, 21, 0, N'Electrical', N'Lighting point include supply & install of E-light fitting', CAST(6.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(480.00 AS Decimal(18, 2)), CAST(360.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (388, 21, 0, N'Electrical', N'Lighting point include supply & install of Exit light fitting', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(240.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (389, 21, 0, N'Fire Protection Work and Plumbing Work', N'Supply and install 3 nos PVC outlet pipe and PPR inlet pipe as per SP requirement', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3500.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(2800.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (412, 23, 0, N'Builders Work', N'"Supply labour to install 9 set of wire mesh onto ceiling (Wire mesh by other)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1350.00 AS Decimal(18, 2)), CAST(1350.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(950.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (390, 22, 0, N'Preliminaries and Demolition Work', N'To supply labour for general cleaning work upon completion of renovation work including dismantling of partition', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (391, 22, 0, N'Preliminaries and Demolition Work', N'Design Consultancy including 3D for submission to landlord for permit to work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2300.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (392, 22, 0, N'Preliminaries and Demolition Work', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (393, 22, 0, N'Preliminaries and Demolition Work', N'Supply labour and material to demolished existing fixture and disposed off-site and terminate all electricity to DB', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4500.00 AS Decimal(18, 2)), CAST(4500.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (394, 22, 0, N'Authority Submission (provision sum)', N'MAA Submission (Pending SCDF approval)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (395, 22, 0, N'Authority Submission (provision sum)', N'Plan fees (Reimbursement)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(90.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (396, 22, 0, N'Builders Work-Ceiling and Partition', N'Supply and install 19mm thick gypsum ceiling c/w cove light design at 3000Ht', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(7770.00 AS Decimal(18, 2)), CAST(7770.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(770.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (397, 22, 0, N'Builders Work-Ceiling and Partition', N'Supply and install 75mm thick gypsum partition using 50mm u-channel c/w 40kg rock wools up to 3000Ht', CAST(1430.0 AS Decimal(18, 1)), N'sqft', CAST(5.00 AS Decimal(18, 2)), CAST(7150.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (398, 22, 0, N'Builders Work-Foor Finishing', N'"Supply and install slected range carpet tiles PC Rate: $2/sqft range"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5895.00 AS Decimal(18, 2)), CAST(5895.00 AS Decimal(18, 2)), CAST(5171.00 AS Decimal(18, 2)), CAST(724.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (399, 22, 0, N'Builders Work-Foor Finishing', N'"Supply labour and material to overlay outdoor flooring inclusive tiles wastage PC Rate: $3/sqft range"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(6080.00 AS Decimal(18, 2)), CAST(6080.00 AS Decimal(18, 2)), CAST(6000.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (400, 22, 0, N'Builders Work-Carpentry', N'Supply labour and install 1200Ht x 1200ht reception counter in laminate finishing c/w tempered glass top', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1488.00 AS Decimal(18, 2)), CAST(512.00 AS Decimal(18, 2)), NULL, NULL)
GO
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (401, 22, 0, N'Builders Work-Carpentry', N'"Supply and install Top and bottom cabinet in laminate finishes and internal white polykem c/w  glass back splash and solid surface top PC Rate: solid surface $80/ftrun"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4700.00 AS Decimal(18, 2)), CAST(4700.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (402, 22, 0, N'Builders Work-Carpentry', N'Supply and install full height cabinet in selected laminate finishes and internal colour pvc c/w individual lockers and compartment to detail (up to 2400ht)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(8640.00 AS Decimal(18, 2)), CAST(8640.00 AS Decimal(18, 2)), CAST(8000.00 AS Decimal(18, 2)), CAST(640.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (403, 22, 0, N'Builders Work-Carpentry', N'Supply and install full height cabinet in selected laminate finishes and internal colour pvc c/w individual pigeon hole lockers and casement cabinet to detail (up to 2400ht)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (404, 22, 0, N'Builders Work-Carpentry', N'"Supply and install white board c/w marker holder to detail at meeting room 2000L x 1200Ht"', CAST(3.0 AS Decimal(18, 1)), N'nos', CAST(950.00 AS Decimal(18, 2)), CAST(2850.00 AS Decimal(18, 2)), CAST(2166.00 AS Decimal(18, 2)), CAST(684.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (405, 22, 0, N'Painting Work', N'Supply labour and materail to paint existing office using nippon paint', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4000.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (406, 22, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (407, 22, 0, N'Electrical', N'Supply and install S/S/O', CAST(3.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), CAST(230.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (408, 22, 0, N'Electrical', N'Supply and install T/S/O', CAST(35.0 AS Decimal(18, 1)), N'nos', CAST(110.00 AS Decimal(18, 2)), CAST(3850.00 AS Decimal(18, 2)), CAST(3150.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (409, 22, 0, N'Electrical', N'Supply and install lighting point for signage and LED Strip and Kebab Machine', CAST(22.0 AS Decimal(18, 1)), N'nos', CAST(80.00 AS Decimal(18, 2)), CAST(1760.00 AS Decimal(18, 2)), CAST(1600.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (410, 22, 0, N'Electrical', N'Supply and install Telephone Line (Non PABx System)', CAST(35.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(3100.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (411, 22, 0, N'Electrical', N'Supply and install Cat 5E Data Line', CAST(35.0 AS Decimal(18, 1)), N'nos', CAST(110.00 AS Decimal(18, 2)), CAST(3850.00 AS Decimal(18, 2)), CAST(3700.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (413, 23, 0, N'Builders Work', N'"Supply labour and material to install racking and boltless shelves PS: assuming partition reinforcement still intact"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(750.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (414, 23, 0, N'Builders Work', N'Hanging 2 set of fitting room curtain', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(350.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), CAST(250.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (415, 23, 0, N'Builders Work', N'Supply and install grey exhibition carpet onto existing floor', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3120.00 AS Decimal(18, 2)), CAST(3120.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1120.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (416, 23, 0, N'Electrical', N'Supply and install S/S/O', CAST(5.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (417, 23, 0, N'Electrical', N'Supply and install lighting point', CAST(9.0 AS Decimal(18, 1)), N'lot', CAST(90.00 AS Decimal(18, 2)), CAST(810.00 AS Decimal(18, 2)), CAST(630.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (418, 23, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (419, 23, 0, N'Electrical', N'To alter existing DB c/w necessary MCB and MCCB including temp supply turn on by NODC appointed LEW, PE endorsement on Singleline drawing and perm turn on testing', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (420, 23, 0, N'Electrical', N'Supply and install T/S/O', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(220.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (421, 23, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (422, 23, 0, N'Preliminaries', N'Design submission to landlord for approval including 3D', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (423, 23, 0, N'Preliminaries', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(800.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (424, 23, 0, N'Preliminaries', N'Supply labour and material to demolish hoarding and general cleaning upon completion', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(2700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (425, 23, 0, N'Authority Submission (provision sum)', N'Building Plan Submission ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2880.00 AS Decimal(18, 2)), CAST(2880.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), CAST(2230.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (426, 24, 0, N'WALL WORK', N'"Supply and install 20mm thick laminate writing panel mounted onto existing framework. Dimension 1550 x 1300 Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5550.00 AS Decimal(18, 2)), CAST(5550.00 AS Decimal(18, 2)), CAST(440.00 AS Decimal(18, 2)), CAST(5110.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (427, 24, 0, N'WALL WORK', N'"Supply and install 65mm thick acoustic wall panels c/w acoustic fabrics and fibre glass mounted onto existing framework. Dimension 2950 x 2600. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3240.00 AS Decimal(18, 2)), CAST(3240.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(2240.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (428, 24, 0, N'WALL WORK', N'"Supply and install 65mm thick acoustic wall panels c/w acoustic fabrics and fibre glass mounted onto existing framework. Dimension 1550 x 1300 Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(860.00 AS Decimal(18, 2)), CAST(860.00 AS Decimal(18, 2)), CAST(660.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (429, 24, 0, N'WALL WORK', N'"Supply and install 65mm thick acoustic wall panels c/w acoustic fabrics and fibre glass mounted onto existing framework. Dimension 1880 x 2080. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1640.00 AS Decimal(18, 2)), CAST(1640.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(1040.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (430, 24, 0, N'WALL WORK', N'"Supply and install 20mm thick laminate writing panel mounted onto existing framework. Dimension 2950 x 2600. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2075.00 AS Decimal(18, 2)), CAST(2075.00 AS Decimal(18, 2)), CAST(875.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (431, 24, 0, N'WALL WORK', N'"Supply and install 20mm thick laminate writing panel mounted onto existing framework. Dimension 1880 x 2080. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1050.00 AS Decimal(18, 2)), CAST(1050.00 AS Decimal(18, 2)), CAST(840.00 AS Decimal(18, 2)), CAST(210.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (432, 25, 0, N'Preliminaries', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(990.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (433, 25, 0, N'Authority Submission', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (434, 25, 0, N'Partitioning and Ceiling Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(2200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (435, 25, 0, N'Flooring Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(7000.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (436, 25, 0, N'Painting Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(700.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(280.00 AS Decimal(18, 2)), CAST(420.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (437, 25, 0, N'Mill Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(490.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (438, 25, 0, N'Glazing Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(1400.00 AS Decimal(18, 2)), CAST(1600.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (439, 25, 0, N'Electrical', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (440, 25, 0, N'Plumbing & Sanitory', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (441, 25, 0, N'ACMV Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (442, 25, 0, N'Fire Protection Work', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (443, 25, 0, N'Miscellaneous', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (444, 26, 0, N'Preliminaries', N'"Supply labour and material to construct hoarding in 12mm thick gypsum board c/w swing door and emulsion paint  (By outgoing Tenant)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1326.00 AS Decimal(18, 2)), CAST(1326.00 AS Decimal(18, 2)), CAST(210.00 AS Decimal(18, 2)), CAST(1116.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (445, 26, 0, N'Preliminaries', N'"To supply and install graphics print to hoarding (Graphics by Owner)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(650.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), CAST(455.00 AS Decimal(18, 2)), CAST(195.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (446, 26, 0, N'Preliminaries', N'Public Liability Insurance ($2 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(600.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (447, 26, 0, N'Preliminaries', N'Design Consultancy including 3D for submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (448, 26, 0, N'Authority Submission (provision sum)', N'Building Plan Submission ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2880.00 AS Decimal(18, 2)), CAST(2880.00 AS Decimal(18, 2)), CAST(1608.00 AS Decimal(18, 2)), CAST(1272.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (449, 26, 0, N'Builders Work', N'Demolish existing partition and to to remove all unwanted fixtures and dispose off site', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(800.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (450, 26, 0, N'Builders Work', N'Touch up to damage ceiling work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(300.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (451, 26, 0, N'Builders Work', N'Supply and install new partition wall', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(767.00 AS Decimal(18, 2)), CAST(767.00 AS Decimal(18, 2)), CAST(590.00 AS Decimal(18, 2)), CAST(177.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (452, 26, 0, N'Builders Work', N'Supply and install shopfront counter in solid surface top and vinyl strip finishes c/w POS counter and flip top', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2899.00 AS Decimal(18, 2)), CAST(2899.00 AS Decimal(18, 2)), CAST(2230.00 AS Decimal(18, 2)), CAST(669.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (453, 26, 0, N'Builders Work', N'Supply and install back feature wall c/w conceal door in laminate finishes and nyatoh wood', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4797.00 AS Decimal(18, 2)), CAST(4797.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), CAST(2397.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (454, 26, 0, N'Builders Work', N'Supply and install TV Brackets mounted onto wall', CAST(3.0 AS Decimal(18, 1)), N'nos', CAST(50.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (455, 26, 0, N'Builders Work', N'Supply and install My Banh Mi Logo', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (456, 26, 0, N'Builders Work', N'Supply and install stainless backing onto wall at BOH', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (457, 26, 0, N'Plumbing', N'Supply and install PPR water inlet and PVC outlet point including fees for license plumbing endorsement as per drawing', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (458, 26, 0, N'ACMV Work (optional)', N'Relocation of Exhaust hood', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(400.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (459, 26, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2400.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), CAST(1966.00 AS Decimal(18, 2)), CAST(434.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (460, 26, 0, N'Electrical', N'Supply and install S/S/O', CAST(7.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(490.00 AS Decimal(18, 2)), CAST(210.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (461, 26, 0, N'Electrical', N'Supply and install T/S/O', CAST(4.0 AS Decimal(18, 1)), N'nos', CAST(110.00 AS Decimal(18, 2)), CAST(440.00 AS Decimal(18, 2)), CAST(360.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (462, 26, 0, N'Electrical', N'Supply and install 15amp S/S/O', CAST(4.0 AS Decimal(18, 1)), N'nos', CAST(150.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(360.00 AS Decimal(18, 2)), CAST(240.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (463, 26, 0, N'Electrical', N'Supply and install 20amp 3 phase isolator', CAST(1.0 AS Decimal(18, 1)), N'nos', CAST(320.00 AS Decimal(18, 2)), CAST(320.00 AS Decimal(18, 2)), CAST(250.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (464, 26, 0, N'Electrical', N'Supply and install lighting point for signage, carpentry, ceiling, and Kebab Machine', CAST(11.0 AS Decimal(18, 1)), N'nos', CAST(80.00 AS Decimal(18, 2)), CAST(880.00 AS Decimal(18, 2)), CAST(660.00 AS Decimal(18, 2)), CAST(220.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (465, 26, 0, N'Electrical', N'Supply and install timer for signage', CAST(1.0 AS Decimal(18, 1)), N'nos', CAST(80.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (466, 26, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'no', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (467, 26, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'no', CAST(110.00 AS Decimal(18, 2)), CAST(110.00 AS Decimal(18, 2)), CAST(105.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (468, 27, 0, N'Preliminaries', N'Design Consultancy including 3D for MCST submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3500.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(3013.00 AS Decimal(18, 2)), CAST(487.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (469, 27, 0, N'Preliminaries', N'Site Supervision and project management', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2300.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (470, 27, 0, N'Preliminaries', N'Final Cleaning and Clearing of Debris', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1700.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (471, 27, 0, N'Preliminaries', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (472, 27, 0, N'Demolition Work and Structural Work', N'Demolish existing parapet wall', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2285.00 AS Decimal(18, 2)), CAST(2285.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(285.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (473, 27, 0, N'Demolition Work and Structural Work', N'"Supply and install new mezzanine work c/w PE calculation and endorsement with 25mm plywood decking. Approximately 1,000 sqft BCA Submission Excluded due to government regulation on mezzanine"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(39000.00 AS Decimal(18, 2)), CAST(39000.00 AS Decimal(18, 2)), CAST(38000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (474, 27, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel, 40kg/m3 rockwool and plastering work to 3000mm Ht Area: Conference Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1725.00 AS Decimal(18, 2)), CAST(1725.00 AS Decimal(18, 2)), CAST(1380.00 AS Decimal(18, 2)), CAST(345.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (475, 27, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel and plastering work to 3000mm Ht Area: Pantry"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(470.00 AS Decimal(18, 2)), CAST(470.00 AS Decimal(18, 2)), CAST(375.00 AS Decimal(18, 2)), CAST(95.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (476, 27, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel and plastering work to 3000mm Ht Area: Stair Case Area"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1575.00 AS Decimal(18, 2)), CAST(1575.00 AS Decimal(18, 2)), CAST(1260.00 AS Decimal(18, 2)), CAST(315.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (477, 27, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel, 40kg/m3 rockwool and plastering work up to 3000mm Ht Area: CEO Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1525.00 AS Decimal(18, 2)), CAST(1525.00 AS Decimal(18, 2)), CAST(1420.00 AS Decimal(18, 2)), CAST(105.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (478, 27, 0, N'Partition and Ceiling Work', N'"Supply and install 9mm thick gypsum ceiling c/w 40kg rockwool and plastering work Area: CEO Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(440.00 AS Decimal(18, 2)), CAST(440.00 AS Decimal(18, 2)), CAST(352.00 AS Decimal(18, 2)), CAST(88.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (479, 27, 0, N'Partition and Ceiling Work', N'"Supply and install 9mm thick gypsum ceiling c/w 40kg rockwool and plastering work Area: Conference Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1150.00 AS Decimal(18, 2)), CAST(1150.00 AS Decimal(18, 2)), CAST(920.00 AS Decimal(18, 2)), CAST(230.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (480, 27, 0, N'Partition and Ceiling Work', N'"Supply and install 75mm thick gypsum partition wall c/w 50mm aluminium u-channel and plastering work to 3000mm Ht Area: Reception Area"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(470.00 AS Decimal(18, 2)), CAST(470.00 AS Decimal(18, 2)), CAST(375.00 AS Decimal(18, 2)), CAST(95.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (481, 27, 0, N'Glazing Work', N'Supply and install 12mm thick tempered glass, 1200mm Ht along glass mezzanine edges c/w necessary structure support', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2080.00 AS Decimal(18, 2)), CAST(2080.00 AS Decimal(18, 2)), CAST(1900.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (483, 27, 0, N'Glazing Work', N'"Supply and install 10mm thick tempered glass screen  1200Ht x 1000L Area: CEO Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(180.00 AS Decimal(18, 2)), CAST(180.00 AS Decimal(18, 2)), CAST(130.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (482, 27, 0, N'Glazing Work', N'"Supply and install 12mm thick tempered glass along glass 1200mm Ht along mezzanine edges c/w subframe support from ceiling slab 3700L x 2400Ht Area: Conference Room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3840.00 AS Decimal(18, 2)), CAST(3840.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(340.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (484, 27, 0, N'Builders Work', N'Supply and install low height standalone cabinet in laminate finishes c/w ABS edging and 4-sides exposed laminate', CAST(6.0 AS Decimal(18, 1)), N'ftrun', CAST(145.00 AS Decimal(18, 2)), CAST(870.00 AS Decimal(18, 2)), CAST(110.00 AS Decimal(18, 2)), CAST(760.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (485, 27, 0, N'Builders Work', N'"Supply and install 2400Ht store cabinet in laminate finishes and ABS edging Area: Stairs Area"', CAST(14.0 AS Decimal(18, 1)), N'ftrun', CAST(320.00 AS Decimal(18, 2)), CAST(4480.00 AS Decimal(18, 2)), CAST(4400.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (486, 27, 0, N'Builders Work', N'Supply and install top hung and bottom pantry cabinet in laminate finishes c/w soft close hinge and solid surface top', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4200.00 AS Decimal(18, 2)), CAST(4200.00 AS Decimal(18, 2)), CAST(4060.00 AS Decimal(18, 2)), CAST(140.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (487, 27, 0, N'Builders Work', N'"Supply and install 40mm thick, 4 -seaters high bench in laminate finishes and ABS edging Area: Outside Conference room"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(570.00 AS Decimal(18, 2)), CAST(570.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (488, 27, 0, N'Builders Work', N'"Supply and install glass writing wall c/w stainless marker tray 1500Ht x 1200L"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(875.00 AS Decimal(18, 2)), CAST(875.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(175.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (489, 27, 0, N'Builders Work', N'Supply and install low height standalone cabinet in laminate finishes c/w ABS edging and 5-sides exposed laminate', CAST(27.0 AS Decimal(18, 1)), N'ftrun', CAST(175.00 AS Decimal(18, 2)), CAST(4725.00 AS Decimal(18, 2)), CAST(4500.00 AS Decimal(18, 2)), CAST(225.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (490, 27, 0, N'Builders Work', N'"Supply and install 2400Ht filing cabinet in laminate finishes and ABS edging Area: Filing Room, Level 7 main office area"', CAST(35.0 AS Decimal(18, 1)), N'ftrun', CAST(320.00 AS Decimal(18, 2)), CAST(11200.00 AS Decimal(18, 2)), CAST(11000.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (491, 27, 0, N'Wall and Floor Finishes', N'Supply labour and material to repaint entire area in emulsion white finishes c/w sealant', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3750.00 AS Decimal(18, 2)), CAST(3750.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (492, 27, 0, N'Wall and Floor Finishes', N'Supply and install selected range wallpaper finishing at selected mezzanine area', CAST(550.0 AS Decimal(18, 1)), N'sqft', CAST(3.50 AS Decimal(18, 2)), CAST(1925.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(425.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (493, 27, 0, N'Wall and Floor Finishes', N'Supply and install selected range 600 by 600 carpet tiles onto both mezzanine and Level 7', CAST(2280.0 AS Decimal(18, 1)), N'sqft', CAST(3.50 AS Decimal(18, 2)), CAST(7980.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(980.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (494, 27, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On, Inclusive of sub main cable', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3300.00 AS Decimal(18, 2)), CAST(3300.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (495, 27, 0, N'Electrical', N'Supply and install T/S/O', CAST(18.0 AS Decimal(18, 1)), N'nos', CAST(110.00 AS Decimal(18, 2)), CAST(1980.00 AS Decimal(18, 2)), CAST(1620.00 AS Decimal(18, 2)), CAST(360.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (496, 27, 0, N'Electrical', N'Supply and install lighting point', CAST(32.0 AS Decimal(18, 1)), N'nos', CAST(80.00 AS Decimal(18, 2)), CAST(2560.00 AS Decimal(18, 2)), CAST(2300.00 AS Decimal(18, 2)), CAST(260.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (497, 27, 0, N'Electrical', N'Supply and install Telephone Line', CAST(15.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1475.00 AS Decimal(18, 2)), CAST(25.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (498, 27, 0, N'Electrical', N'Supply labour and material to install light fitting', CAST(32.0 AS Decimal(18, 1)), N'nos', CAST(15.00 AS Decimal(18, 2)), CAST(480.00 AS Decimal(18, 2)), CAST(320.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (499, 27, 0, N'Electrical', N'Supply and install Data Line', CAST(15.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1275.00 AS Decimal(18, 2)), CAST(225.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (500, 27, 0, N'Fire Protection Work', N'Design and build fire sprinkler work (Water discharge fees will be borne by client)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5900.00 AS Decimal(18, 2)), CAST(5900.00 AS Decimal(18, 2)), CAST(5700.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (501, 27, 0, N'Authority Submission', N'Building Plan Submission ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3600.00 AS Decimal(18, 2)), CAST(3600.00 AS Decimal(18, 2)), CAST(3300.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), NULL, NULL)
GO
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (502, 28, 0, N'Preliminaries and Demolition Work', N'To supply labour for general cleaning work upon completion of renovation work including dismantling of partition', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (503, 28, 0, N'Preliminaries and Demolition Work', N'Design Consultancy including 3D for submission to landlord for permit to work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(2300.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (504, 28, 0, N'Preliminaries and Demolition Work', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (505, 28, 0, N'Preliminaries and Demolition Work', N'Supply labour and material to demolished existing fixture and disposed off-site and terminate all electricity to DB', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4500.00 AS Decimal(18, 2)), CAST(4500.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (506, 28, 0, N'Authority Submission (provision sum)', N'MAA Submission (Pending SCDF approval)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (507, 28, 0, N'Authority Submission (provision sum)', N'Plan fees (Reimbursement)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(90.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (508, 28, 0, N'Builders Work-Ceiling and Partition', N'Supply and install 19mm thick gypsum ceiling c/w cove light design at 3000Ht', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(7770.00 AS Decimal(18, 2)), CAST(7770.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(770.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (509, 28, 0, N'Builders Work-Ceiling and Partition', N'Supply and install 75mm thick gypsum partition using 50mm u-channel c/w 40kg rock wools up to 3000Ht', CAST(1430.0 AS Decimal(18, 1)), N'sqft', CAST(5.00 AS Decimal(18, 2)), CAST(7150.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (510, 28, 0, N'Builders Work-Foor Finishing', N'"Supply and install slected range carpet tiles PC Rate: $2/sqft range"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5895.00 AS Decimal(18, 2)), CAST(5895.00 AS Decimal(18, 2)), CAST(5171.00 AS Decimal(18, 2)), CAST(724.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (511, 28, 0, N'Builders Work-Foor Finishing', N'"Supply labour and material to overlay outdoor flooring inclusive tiles wastage PC Rate: $3/sqft range"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(6080.00 AS Decimal(18, 2)), CAST(6080.00 AS Decimal(18, 2)), CAST(6000.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (512, 28, 0, N'Builders Work-Carpentry', N'Supply labour and install 1200Ht x 1200ht reception counter in laminate finishing c/w tempered glass top', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1488.00 AS Decimal(18, 2)), CAST(512.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (513, 28, 0, N'Builders Work-Carpentry', N'"Supply and install Top and bottom cabinet in laminate finishes and internal white polykem c/w  glass back splash and solid surface top PC Rate: solid surface $80/ftrun"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4700.00 AS Decimal(18, 2)), CAST(4700.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (514, 28, 0, N'Builders Work-Carpentry', N'Supply and install full height cabinet in selected laminate finishes and internal colour pvc c/w individual lockers and compartment to detail (up to 2400ht)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(8640.00 AS Decimal(18, 2)), CAST(8640.00 AS Decimal(18, 2)), CAST(8000.00 AS Decimal(18, 2)), CAST(640.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (515, 28, 0, N'Builders Work-Carpentry', N'Supply and install full height cabinet in selected laminate finishes and internal colour pvc c/w individual pigeon hole lockers and casement cabinet to detail (up to 2400ht)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (516, 28, 0, N'Builders Work-Carpentry', N'"Supply and install white board c/w marker holder to detail at meeting room 2000L x 1200Ht"', CAST(3.0 AS Decimal(18, 1)), N'nos', CAST(950.00 AS Decimal(18, 2)), CAST(2850.00 AS Decimal(18, 2)), CAST(2166.00 AS Decimal(18, 2)), CAST(684.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (517, 28, 0, N'Painting Work', N'Supply labour and materail to paint existing office using nippon paint', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4000.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (518, 28, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (519, 28, 0, N'Electrical', N'Supply and install S/S/O', CAST(3.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), CAST(230.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (520, 28, 0, N'Electrical', N'Supply and install T/S/O', CAST(35.0 AS Decimal(18, 1)), N'nos', CAST(110.00 AS Decimal(18, 2)), CAST(3850.00 AS Decimal(18, 2)), CAST(3150.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (521, 28, 0, N'Electrical', N'Supply and install lighting point for signage and LED Strip and Kebab Machine', CAST(22.0 AS Decimal(18, 1)), N'nos', CAST(80.00 AS Decimal(18, 2)), CAST(1760.00 AS Decimal(18, 2)), CAST(1600.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (522, 28, 0, N'Electrical', N'Supply and install Telephone Line (Non PABx System)', CAST(35.0 AS Decimal(18, 1)), N'nos', CAST(100.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(3100.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (523, 28, 0, N'Electrical', N'Supply and install Cat 5E Data Line', CAST(35.0 AS Decimal(18, 1)), N'nos', CAST(110.00 AS Decimal(18, 2)), CAST(3850.00 AS Decimal(18, 2)), CAST(3700.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (524, 29, 0, N'WALL WORK', N'"Supply and install 20mm thick laminate writing panel mounted onto existing framework. Dimension 1550 x 1300 Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5550.00 AS Decimal(18, 2)), CAST(5550.00 AS Decimal(18, 2)), CAST(440.00 AS Decimal(18, 2)), CAST(5110.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (525, 29, 0, N'WALL WORK', N'"Supply and install 65mm thick acoustic wall panels c/w acoustic fabrics and fibre glass mounted onto existing framework. Dimension 2950 x 2600. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3240.00 AS Decimal(18, 2)), CAST(3240.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(2240.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (526, 29, 0, N'WALL WORK', N'"Supply and install 65mm thick acoustic wall panels c/w acoustic fabrics and fibre glass mounted onto existing framework. Dimension 1550 x 1300 Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(860.00 AS Decimal(18, 2)), CAST(860.00 AS Decimal(18, 2)), CAST(660.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (527, 29, 0, N'WALL WORK', N'"Supply and install 65mm thick acoustic wall panels c/w acoustic fabrics and fibre glass mounted onto existing framework. Dimension 1880 x 2080. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1640.00 AS Decimal(18, 2)), CAST(1640.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(1040.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (528, 29, 0, N'WALL WORK', N'"Supply and install 20mm thick laminate writing panel mounted onto existing framework. Dimension 2950 x 2600. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2075.00 AS Decimal(18, 2)), CAST(2075.00 AS Decimal(18, 2)), CAST(875.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (529, 29, 0, N'WALL WORK', N'"Supply and install 20mm thick laminate writing panel mounted onto existing framework. Dimension 1880 x 2080. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1050.00 AS Decimal(18, 2)), CAST(1050.00 AS Decimal(18, 2)), CAST(840.00 AS Decimal(18, 2)), CAST(210.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (530, 16, 0, N'Preliminaries', N'Design Adapation including 3D for submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (531, 16, 0, N'Preliminaries', N'Final cleaning of outlet', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(400.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (532, 16, 0, N'Preliminaries', N'Public Liability Insurance and Contractor All Risk ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(800.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(700.50 AS Decimal(18, 2)), CAST(99.50 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (534, 16, 0, N'Builders Work', N'Supply and install 1500mm wide POS counter for Pezzo c/w based for drink display', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1800.00 AS Decimal(18, 2)), CAST(1800.00 AS Decimal(18, 2)), CAST(1300.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (536, 16, 0, N'Builders Work', N'"Supply and install 12mm thick, 1550 ht tempered glass at escalator side c/w 2 fins to the existing metal frame. 600L,6150L,600L x 1550Ht"', CAST(136.0 AS Decimal(18, 1)), N'lot', CAST(15.00 AS Decimal(18, 2)), CAST(2040.00 AS Decimal(18, 2)), CAST(1632.00 AS Decimal(18, 2)), CAST(408.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (537, 16, 0, N'Builders Work', N'Supply and install matt black laminate frame work onto back glass', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(450.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (552, 30, 0, N'WALL WORK', N'"Supply and install 20mm thick laminate writing panel mounted onto existing framework. Dimension 1550 x 1300 Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5550.00 AS Decimal(18, 2)), CAST(5550.00 AS Decimal(18, 2)), CAST(440.00 AS Decimal(18, 2)), CAST(5110.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (553, 30, 0, N'WALL WORK', N'"Supply and install 65mm thick acoustic wall panels c/w acoustic fabrics and fibre glass mounted onto existing framework. Dimension 2950 x 2600. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3240.00 AS Decimal(18, 2)), CAST(3240.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(2240.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (554, 30, 0, N'WALL WORK', N'"Supply and install 65mm thick acoustic wall panels c/w acoustic fabrics and fibre glass mounted onto existing framework. Dimension 1550 x 1300 Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(860.00 AS Decimal(18, 2)), CAST(860.00 AS Decimal(18, 2)), CAST(660.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (555, 30, 0, N'WALL WORK', N'"Supply and install 65mm thick acoustic wall panels c/w acoustic fabrics and fibre glass mounted onto existing framework. Dimension 1880 x 2080. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1640.00 AS Decimal(18, 2)), CAST(1640.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(1040.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (556, 30, 0, N'WALL WORK', N'"Supply and install 20mm thick laminate writing panel mounted onto existing framework. Dimension 2950 x 2600. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2075.00 AS Decimal(18, 2)), CAST(2075.00 AS Decimal(18, 2)), CAST(875.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (557, 30, 0, N'WALL WORK', N'"Supply and install 20mm thick laminate writing panel mounted onto existing framework. Dimension 1880 x 2080. Acoustic Fabrics at $40/mr"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1050.00 AS Decimal(18, 2)), CAST(1050.00 AS Decimal(18, 2)), CAST(840.00 AS Decimal(18, 2)), CAST(210.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (558, 31, 0, N'Builders Work', N'"Supply and install Exhaust hood Cladding as per detail including mosiac tiles  Tiles PC rate @ $6.30 per sqft (Pending availability) (Exhaust hood by other)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (563, 31, 0, N'Builders Work', N'"Supply labour and material to construct subframe to suspend 2 nos of TV onto ceiling including TV bracket (TV by others)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1200.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (564, 31, 0, N'Builders Work', N'Supply and install POS counter c/w bottom cabinet in laminate finishes and internal white polykem', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1300.00 AS Decimal(18, 2)), CAST(1300.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (565, 31, 0, N'Plumbing', N'Copper water supply  & copper drainage pipe installation work including fees for license plumbing endorsement.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3600.00 AS Decimal(18, 2)), CAST(3600.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), N'"Note: 
Building Consultant fee, if any, to be borned by Client
PE endorsement will be billed directly to client with a 10% P&A
"
', NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (566, 31, 0, N'Electrical', N'Supply and install DB including fee for LEW endorsement Fees including testing and commissioning and Electrical Turn On.', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (567, 31, 0, N'Electrical', N'Supply and install S/S/O', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (568, 31, 0, N'Electrical', N'Supply and install T/S/O', CAST(6.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(660.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(460.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (569, 31, 0, N'Electrical', N'Supply and install 32amp 3 phase isolator', CAST(3.0 AS Decimal(18, 1)), N'lot', CAST(475.00 AS Decimal(18, 2)), CAST(1425.00 AS Decimal(18, 2)), CAST(1140.00 AS Decimal(18, 2)), CAST(285.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (570, 31, 0, N'Electrical', N'Supply and install lighting point for signage and LED Strip and Kebab Machine', CAST(2.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(160.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (571, 31, 0, N'Electrical', N'Supply and install timer for signage', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(80.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (572, 31, 0, N'Electrical', N'Supply and install Telephone Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(100.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (573, 31, 0, N'Electrical', N'Supply and install Data Line', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(110.00 AS Decimal(18, 2)), CAST(110.00 AS Decimal(18, 2)), CAST(105.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (574, 32, 0, N'Glass Work', N'"Supply and install 75mm thick gypsum partition c/w plastering work and paint finishes (Compactus to be removed temporary by Client for the installation work)"', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(950.00 AS Decimal(18, 2)), CAST(950.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), CAST(900.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (575, 32, 0, N'Glass Work', N'Supply and install 10mm thick glass c/w u-channel onto existing wall and floor including frosted film (2pieces)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2200.00 AS Decimal(18, 2)), CAST(2200.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (576, 32, 0, N'Glass Work', N'Supply and install framing to glass as per existing office glass detail', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(600.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (577, 32, 0, N'Glass Work', N'Supply and install 5mm thick vinyl clip system flooring including removal of existing carpet tiles', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2800.00 AS Decimal(18, 2)), CAST(2800.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (578, 32, 0, N'Glass Work', N'Supply and install selected range wallpaper (Easy-washed range)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), CAST(1700.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (579, 32, 0, N'Glass Work', N'Install glass door', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (580, 19, 0, N'Preliminaries', N'Waa', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (581, 33, 0, N'Preliminaries', N'Preliminaries and Demolition Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(9000.00 AS Decimal(18, 2)), CAST(9000.00 AS Decimal(18, 2)), CAST(8745.00 AS Decimal(18, 2)), CAST(255.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (582, 33, 0, N'Authority Submission', N'Authority Submission (provision sum)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1600.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (583, 33, 0, N'Builders Work', N'Builder Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(51085.00 AS Decimal(18, 2)), CAST(51085.00 AS Decimal(18, 2)), CAST(50000.00 AS Decimal(18, 2)), CAST(1085.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (584, 33, 0, N'Electrical', N'Supply and install power point', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(16260.00 AS Decimal(18, 2)), CAST(16260.00 AS Decimal(18, 2)), CAST(8000.00 AS Decimal(18, 2)), CAST(8260.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (585, 34, 0, N'Preliminaries', N'Public Liability Insurance ($1 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(6000.00 AS Decimal(18, 2)), CAST(6000.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (586, 34, 0, N'Authority Submission', N'Building Plan Submission ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3600.00 AS Decimal(18, 2)), CAST(3600.00 AS Decimal(18, 2)), CAST(1400.00 AS Decimal(18, 2)), CAST(2200.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (587, 34, 0, N'Partitioning and Ceiling Work', N'Demolish existing parapet wall', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5000.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), CAST(3600.00 AS Decimal(18, 2)), CAST(1400.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (588, 35, 0, N'Preliminaries', N'To design and and produce full drawing for construction including M&E planning for HDB approval', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (589, 35, 0, N'Plumbing & Sanitory', N'Supply and install PVC outlet pipe and PPR inlet pipe as per SP requirement', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3600.00 AS Decimal(18, 2)), CAST(3600.00 AS Decimal(18, 2)), CAST(2600.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (590, 35, 0, N'Painting Work', N'Paint whole unit ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(15000.00 AS Decimal(18, 2)), CAST(15000.00 AS Decimal(18, 2)), CAST(8000.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (591, 36, 0, N'Preliminaries', N'To design and and produce full drawing for construction including M&E planning for HDB approval', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2500.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (592, 36, 0, N'Plumbing & Sanitory', N'Supply and install PVC outlet pipe and PPR inlet pipe as per SP requirement', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3600.00 AS Decimal(18, 2)), CAST(3600.00 AS Decimal(18, 2)), CAST(2600.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (593, 36, 0, N'Painting Work', N'Paint whole unit ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(15000.00 AS Decimal(18, 2)), CAST(15000.00 AS Decimal(18, 2)), CAST(8000.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (594, 37, 0, N'Preliminaries', N'Public Liability Insurance ($2 mil)', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(1020.00 AS Decimal(18, 2)), CAST(1980.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (595, 37, 0, N'Preliminaries', N'Design Consultancy including 3D for submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (596, 37, 0, N'Authority Submission', N'Building Plan Submission', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2880.00 AS Decimal(18, 2)), CAST(2880.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(2080.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (597, 37, 0, N'Builders Work', N'Demolish existing partition and to to remove all unwanted fixtures and dispose off site', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(7000.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(2560.00 AS Decimal(18, 2)), CAST(4440.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (598, 38, 0, N'Preliminaries', N'Final cleaning', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5000.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (599, 38, 0, N'Preliminaries', N'Insurance ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5000.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (600, 38, 0, N'Preliminaries', N'Project Management ', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (601, 39, 0, N' 123', N'123', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (602, 40, 0, N' 123', N'123', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (613, 41, 3, N'tt', N'tt', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (614, 41, 1, N'aa', N'aa', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (615, 41, 2, N'aa', N'aa', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(2.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (616, 41, 2, N'aa', N'bb', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(3.00 AS Decimal(18, 2)), CAST(3.00 AS Decimal(18, 2)), CAST(3.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (617, 41, 2, N'aa', N'cc', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(6.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (629, 42, 1, N'Preliminaries', N'Preliminaries', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(6056.00 AS Decimal(18, 2)), CAST(6056.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(3056.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (630, 42, 2, N'Wet Trade', N'Wet Trade', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(41265.00 AS Decimal(18, 2)), CAST(41265.00 AS Decimal(18, 2)), CAST(30000.00 AS Decimal(18, 2)), CAST(11265.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (631, 42, 3, N'Tiles Procurement', N'Tiles Procurement', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(16500.50 AS Decimal(18, 2)), CAST(16500.50 AS Decimal(18, 2)), CAST(8500.50 AS Decimal(18, 2)), CAST(8000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (632, 42, 4, N'Partition and Glazing Work', N'Partition and Glazing Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(10940.00 AS Decimal(18, 2)), CAST(10940.00 AS Decimal(18, 2)), CAST(5940.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (633, 42, 5, N'Painting Works', N'Painting Works', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(4750.00 AS Decimal(18, 2)), CAST(4750.00 AS Decimal(18, 2)), CAST(2750.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (634, 42, 6, N'Builders Work', N'Builders Work', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(28230.00 AS Decimal(18, 2)), CAST(28230.00 AS Decimal(18, 2)), CAST(20000.00 AS Decimal(18, 2)), CAST(8230.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (635, 42, 7, N'Loose Furniture', N'Loose Furniture', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(5760.00 AS Decimal(18, 2)), CAST(5760.00 AS Decimal(18, 2)), CAST(3760.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (636, 42, 8, N'Plumbing', N'Plumbing', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(11000.00 AS Decimal(18, 2)), CAST(11000.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (637, 42, 9, N'ACMV Work', N'ACMV', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(45300.00 AS Decimal(18, 2)), CAST(45300.00 AS Decimal(18, 2)), CAST(35000.00 AS Decimal(18, 2)), CAST(10300.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (638, 42, 10, N'Electrical', N'Electrical', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(26850.00 AS Decimal(18, 2)), CAST(26850.00 AS Decimal(18, 2)), CAST(20000.00 AS Decimal(18, 2)), CAST(6850.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (639, 42, 11, N'Miscellaneous', N'Miscellaneous', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(10850.00 AS Decimal(18, 2)), CAST(10850.00 AS Decimal(18, 2)), CAST(8000.00 AS Decimal(18, 2)), CAST(2850.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (640, 43, 1, N'Preliminary', N'Sales', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(15000.00 AS Decimal(18, 2)), CAST(15000.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), CAST(10000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[QuotationItemDescriptions] ([ID], [QuotationId], [RowId], [CategoryName], [QuotationItem_Description], [QuotationItemQty], [QuotationItemUnit], [QuotationItemRate], [QuotationItemAmount], [QuotationItemEstCost], [QuotationItemEstProfit], [QuotationItemRemarks], [QuotationItemMgrComments]) VALUES (641, 44, 1, N'test', N'1', CAST(1.0 AS Decimal(18, 1)), N'lot', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, NULL)
SET IDENTITY_INSERT [dbo].[QuotationItemDescriptions] OFF
GO
SET IDENTITY_INSERT [dbo].[Quotations] ON 

INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (1, N'Q1700001', N'NODC Pte Ltd', N'Yes', 1, N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'65 9192 9394', N'65 9192 9393', 2, NULL, 6, CAST(N'2017-04-27 00:00:00.0000000' AS DateTime2), N'UserGuide_170427174953.pdf', N'Colins''s Quotation on 27/04/2017', NULL, N'Confirmed', 0, 0, 1, CAST(N'2017-04-27 17:40:01.3117595' AS DateTime2), CAST(N'2017-09-07 15:35:15.0952000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (2, N'Q1700002', N'NODC Pte Ltd', N'Yes', 2, N'Elvin Ong', N'81 UBI AVENUE 4, #07-06, Singapore 408830', N'+658029579', N'+658029579', 1, NULL, 5, CAST(N'2017-04-27 00:00:00.0000000' AS DateTime2), N'UserGuide_170427175744.pdf', N'Bobby''s Quotation on 27/04/2017', NULL, N'Pending Customer Confirmation', 0, 0, 2, CAST(N'2017-04-27 17:56:38.2103939' AS DateTime2), CAST(N'2017-09-07 15:35:08.4652000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (3, N'Q1700003', N'NODC Pte Ltd', N'Yes', 2, N'Elvin Ong', N'81 UBI AVENUE 4, #07-06, Singapore 408830', N'+658029579', N'+658029579', 2, NULL, 3, CAST(N'2017-04-27 00:00:00.0000000' AS DateTime2), N'UserGuide_170427175854.pdf', N'Aaron''s Quotation on 27/04/2017', NULL, N'Confirmed', 0, 0, 3, CAST(N'2017-04-27 18:01:10.9952190' AS DateTime2), CAST(N'2017-09-07 15:35:02.6464000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (4, N'Q1700004', N'NODC Pte Ltd', N'Yes', 4, N'MR CHRISTIAN FRANK', N'107 TUAS SOUTH AVE 8 OFFSHORE MARINE CENTRE SINGAPORE 637036', N'67055800', N'67948345', 8, NULL, 8, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_16-0005_-_Menck_(Dir_Head)_170720150622.pdf', N'Testing', N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Cancelled by Customer', 0, 0, 4, CAST(N'2017-07-20 15:12:43.9189961' AS DateTime2), CAST(N'2017-09-07 15:34:57.0772000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (5, N'Q1700005', N'NODC Pte Ltd', N'Yes', 3, N'Slone Lim', N'50 East Coast Road, #01-110 , #01-134 Roxy Square, Singapore 428769', N'9222 3605', NULL, 12, NULL, 12, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0054_-_The_Art_Haus_(Snr_Mgr_1.1)_170720151528.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Customer Confirmation ( SNR MGR 1.1 )', 0, 0, 5, CAST(N'2017-07-20 15:36:23.1913961' AS DateTime2), CAST(N'2017-09-07 15:31:03.1084000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (6, N'Q1700006', N'NODC Pte Ltd', N'Yes', 5, N'Ms Haze Tan', N'1 Ubi View #03-10, Focus One Singapore 408555', N'63537767', NULL, 10, NULL, 10, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_16-0008_-_AceCom_Technologies_(Director_1)_170720152158.pdf', N'testing', N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Sales Revision ( Director1 )', 0, 0, 6, CAST(N'2017-07-20 15:41:12.1189961' AS DateTime2), CAST(N'2017-09-07 15:28:56.4208000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (7, N'Q1700007', N'NODC Pte Ltd', N'Yes', 6, N'Walter Tan', N'17 Kaki Bukit Crescent #02-02 Kaki Bukit Techpark 1, Singapore 416248', N'9819 6468', NULL, 12, NULL, 12, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0055_-_Carpenter_and_Cook_Lor_Kilat_(Snr_Mgr_1.1)_170720154634.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Customer Confirmation ( SNR MGR 1.1 )', 0, 0, 7, CAST(N'2017-07-20 15:54:17.5165961' AS DateTime2), CAST(N'2017-09-07 15:34:13.1476000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (8, N'Q1700008', N'NODC Pte Ltd', N'Yes', 7, N' Mr Hendra Chong, Miss Trixy Tan', N'114 Lavender Street CT Hub 2, #08-53 Singapore 338729	', N'87820766', NULL, 9, NULL, 9, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0015_-_Now_Comms_(Snr_Mgr_3)_170720160126.pdf', N'testing', N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Sales Revision ( Loh Chiat Min )', 0, 0, 8, CAST(N'2017-07-20 16:06:02.2465961' AS DateTime2), CAST(N'2017-09-07 15:28:49.9468000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (9, N'Q1700009', N'NODC Pte Ltd', N'Yes', 8, N'Attn: Mr Walter Tan', N'17 Kaki Bukit Crescent #02-02 Kaki Bukit Techpark 1 Singapore 416248', N'98196468', NULL, 14, NULL, 14, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0052_-_Liv_Activ_@_CCP_(Snr_Mgr_1.3)_170720161627.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Confirmed', 0, 0, 9, CAST(N'2017-07-20 16:30:52.2181961' AS DateTime2), CAST(N'2017-09-07 15:34:51.4300000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (11, N'Q1700011', N'NODC Pte Ltd', N'Yes', 10, N'Attn: Miss Tay Ying Hui', N'81 Ubi Ave 4 #07-06 Singapore 408830', N'94554784', NULL, 12, NULL, 12, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0038_-_My_Banh_Mi_at_TPC_(Snr_Mgr_1.1)_170720165129.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Customer Confirmation ( SNR MGR 1.1 )', 0, 0, 11, CAST(N'2017-07-20 17:07:27.2473961' AS DateTime2), CAST(N'2017-09-07 15:34:00.4804000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (14, N'Q1700014', N'NODC Pte Ltd', N'Yes', 13, N'Alvin', N'1006 Aljunied Ave 5', N'9675 9990', NULL, 15, NULL, 15, CAST(N'2017-07-21 00:00:00.0000000' AS DateTime2), N'NODC_17-0014_-_Orange_Lantern_Central_Kitchen_(Snr_Mgr_2.1)_170721164953.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Confirmed', 0, 0, 14, CAST(N'2017-07-21 16:58:29.6057136' AS DateTime2), CAST(N'2017-09-07 15:34:32.9128000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (15, N'Q1700015', N'NODC Pte Ltd', N'Yes', 12, N'Miss Swathi Mathur', N'21 Media Circle #05-06, Infinite Studios Singapore 138562', N'6589 8744', NULL, 11, NULL, 11, CAST(N'2017-07-21 00:00:00.0000000' AS DateTime2), N'NODC_17-004_-_24_@_Henderson_-_(Director_2)_170721165745.pdf', N'testing', N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Sales Revision ( Director2 )', 0, 0, 15, CAST(N'2017-07-21 17:18:13.5209136' AS DateTime2), CAST(N'2017-09-07 15:29:02.5984000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (16, N'Q1700016', N'NODC Pte Ltd', N'Yes', 14, N'Chiang Zhan Yi', N'51 Imbiah Road, Singapore 099702', N'9640 7401', NULL, 15, NULL, 15, CAST(N'2017-07-21 00:00:00.0000000' AS DateTime2), N'NODC_17-0059_-_Pezzo_@_Sembawang_(Snr_Mgr_2.1)_170721170946.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'', 0, 0, 16, CAST(N'2017-07-21 17:20:47.1653136' AS DateTime2), CAST(N'2017-09-07 15:34:25.5340000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (17, N'Q1700017', N'NODC Pte Ltd', N'Yes', 15, N'Kelly Bok', N'216 Boon Lay Avenue	 #01-01 Singapore 640216	', N'9631 7106', NULL, 17, NULL, 17, CAST(N'2017-07-21 00:00:00.0000000' AS DateTime2), N'NODC17-0058__-_Box_Kitchen_(Snr_Mgr_2.3)_170721172622.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Lim Hong Guan )', 0, 0, 17, CAST(N'2017-07-21 17:39:20.9585136' AS DateTime2), CAST(N'2017-09-07 15:34:18.9664000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (10, N'Q1700010', N'NODC Pte Ltd', N'Yes', 9, N'Jacky Lim', N'224 Westwood Ave #09-15 Singapore 648366', N'9383 5543', NULL, 13, NULL, 13, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0044_-_DF_Academy_(Snr_Mgr_1.2)_170720162735.pdf', N'testing', N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Sales Revision ( SNR MGR 1.2 )', 0, 0, 10, CAST(N'2017-07-20 17:05:06.8629961' AS DateTime2), CAST(N'2017-09-07 15:34:45.7516000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (12, N'Q1700004', N'NODC Pte Ltd', N'Yes', 4, N'Mr Jake Chia ', NULL, NULL, NULL, 7, NULL, 21, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_16-0007_-_Grains_Pte_Ltd_(Snr_Mgr_2.1)_170720104126.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Customer Confirmation ( CUST 1.1.2.1.1 )', 0, 0, 12, CAST(N'2017-07-20 17:35:32.7961961' AS DateTime2), CAST(N'2017-09-07 15:28:25.1896000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (19, N'Q1700021', N'No 1 Design Consultancy Pte Ltd', NULL, 17, N'Mr Christian Frank ', NULL, N'67055800', NULL, 7, NULL, 7, CAST(N'2017-09-11 00:00:00.0000000' AS DateTime2), NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Customer Confirmation ( Elvin Ong )', 0, 0, 19, CAST(N'2017-07-25 17:08:09.6479132' AS DateTime2), CAST(N'2017-09-07 16:54:27.9940000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (13, N'Q1700013', N'NODC Pte Ltd', N'Yes', 11, N'Jake Chia', N'703 Ang Mo Kio Ave 5#04-40 Singapore 569880 Northstar@AMK', N'9144 6336', NULL, 15, NULL, 15, CAST(N'2017-07-21 00:00:00.0000000' AS DateTime2), N'NODC_16-0007_-_Grains_Pte_Ltd_(Snr_Mgr_2.1)_170721154037.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'', 0, 0, 13, CAST(N'2017-07-21 15:50:50.5793136' AS DateTime2), CAST(N'2017-09-07 15:34:39.5272000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (18, N'Q1700018', N'NODC Pte Ltd', N'Yes', 16, N'Michael', N'1 Jalan Anak Bukit #B1-52, Singapore 588996', N'9636 2377', NULL, 16, NULL, 16, CAST(N'2017-07-21 00:00:00.0000000' AS DateTime2), N'NODC_17-0061R1_-_212_Desert_Loft_(Snr_Mgr_2.2)_170721173850.pdf', N'TEST', N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Cancelled by Customer', 0, 0, 18, CAST(N'2017-07-21 17:48:19.7825136' AS DateTime2), CAST(N'2017-09-07 15:29:55.5760000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (20, N'Q1700019', N'NODC Pte Ltd', N'Yes', 4, N'MR CHRISTIAN FRANK', N'107 TUAS SOUTH AVE 8 OFFSHORE MARINE CENTRE SINGAPORE 637036', N'67055800', N'67948345', 8, NULL, 9, CAST(N'2017-07-26 00:00:00.0000000' AS DateTime2), NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Customer Confirmation ( Loh Chiat Min )', 0, 0, 20, CAST(N'2017-07-26 10:07:45.0241849' AS DateTime2), CAST(N'2017-09-07 15:34:06.5176000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (21, N'Q1700005-R1', N'NODC Pte Ltd', N'Yes', 3, N'Slone Lim', N'50 East Coast Road, #01-110 , #01-134 Roxy Square, Singapore 428769', N'9222 3605', NULL, 8, NULL, 8, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0054_-_The_Art_Haus_(Snr_Mgr_1.1)_170720151528.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'', 1, 0, 5, CAST(N'2017-08-11 16:45:35.2166000' AS DateTime2), CAST(N'2017-09-07 15:31:03.1084000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (22, N'Q1700006-V1', N'NODC Pte Ltd', N'Yes', 5, N'Ms Haze Tan', N'1 Ubi View #03-10, Focus One Singapore 408555', N'63537767', NULL, 8, NULL, 10, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_16-0008_-_AceCom_Technologies_(Director_1)_170720152158.pdf', N'testing', N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Customer Confirmation ( Director1 )', 0, 1, 6, CAST(N'2017-08-11 16:53:43.9802000' AS DateTime2), CAST(N'2017-09-07 15:28:56.4364000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (23, N'Q1700007-R1', N'NODC Pte Ltd', N'Yes', 6, N'Walter Tan', N'17 Kaki Bukit Crescent #02-02 Kaki Bukit Techpark 1, Singapore 416248', N'9819 6468', NULL, 8, NULL, 8, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0055_-_Carpenter_and_Cook_Lor_Kilat_(Snr_Mgr_1.1)_170720154634.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Confirmed', 1, 0, 7, CAST(N'2017-08-14 17:42:12.7998000' AS DateTime2), CAST(N'2017-09-07 15:34:13.1476000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (24, N'Q1700008-V1', N'NODC Pte Ltd', N'Yes', 7, N' Mr Hendra Chong, Miss Trixy Tan', N'114 Lavender Street CT Hub 2, #08-53 Singapore 338729	', N'87820766', NULL, 8, NULL, 9, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0015_-_Now_Comms_(Snr_Mgr_3)_170720160126.pdf', N'testing', N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Sales Revision ( Loh Chiat Min )', 0, 1, 8, CAST(N'2017-08-14 17:43:59.0202000' AS DateTime2), CAST(N'2017-09-07 15:28:49.9468000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (25, N'Q1700019-R1', N'NODC Pte Ltd', N'Yes', 4, N'MR CHRISTIAN FRANK', N'107 TUAS SOUTH AVE 8 OFFSHORE MARINE CENTRE SINGAPORE 637036', N'67055800', N'67948345', 8, NULL, 9, CAST(N'2017-07-26 00:00:00.0000000' AS DateTime2), N'NODC_17-0038_-_My_Banh_Mi_at_TPC_(Snr_Mgr_1.1)_170816173602.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Confirmed', 1, 0, 20, CAST(N'2017-08-14 17:46:52.6794000' AS DateTime2), CAST(N'2017-09-07 15:34:06.5332000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (26, N'Q1700011-R1', N'NODC Pte Ltd', N'Yes', 10, N'Attn: Miss Tay Ying Hui', N'81 Ubi Ave 4 #07-06 Singapore 408830', N'94554784', NULL, 8, NULL, 11, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0038_-_My_Banh_Mi_at_TPC_(Snr_Mgr_1.1)_170720165129.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Confirmed', 1, 0, 11, CAST(N'2017-08-15 17:05:04.0076000' AS DateTime2), CAST(N'2017-09-07 15:34:00.4960000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (27, N'Q1700015-V1', N'NODC Pte Ltd', N'Yes', 12, N'Miss Swathi Mathur', N'21 Media Circle #05-06, Infinite Studios Singapore 138562', N'6589 8744', NULL, 11, NULL, 11, CAST(N'2017-07-21 00:00:00.0000000' AS DateTime2), N'NODC_17-004_-_24_@_Henderson_-_(Director_2)_170721165745.pdf', N'testing', N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Cancelled by Customer', 0, 1, 15, CAST(N'2017-08-16 17:20:25.3284000' AS DateTime2), CAST(N'2017-09-07 15:29:02.5984000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (28, N'Q1700006-R1-V1', N'NODC Pte Ltd', N'Yes', 5, N'Ms Haze Tan', N'1 Ubi View #03-10, Focus One Singapore 408555', N'63537767', NULL, 8, NULL, 10, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_16-0008_-_AceCom_Technologies_(Director_1)_170720152158.pdf', N'testing', N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Customer Confirmation ( Director1 )', 1, 1, 6, CAST(N'2017-08-17 10:11:37.9196000' AS DateTime2), CAST(N'2017-09-07 15:28:56.4364000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (29, N'Q1700008-V2', N'NODC Pte Ltd', N'Yes', 7, N' Mr Hendra Chong, Miss Trixy Tan', N'114 Lavender Street CT Hub 2, #08-53 Singapore 338729	', N'87820766', NULL, 8, NULL, 9, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0015_-_Now_Comms_(Snr_Mgr_3)_170720160126.pdf', N'testing', N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Customer Confirmation ( Loh Chiat Min )', 0, 2, 8, CAST(N'2017-08-17 10:13:17.0576000' AS DateTime2), CAST(N'2017-09-07 15:28:49.9468000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (31, N'Q1700004-R1', N'NODC Pte Ltd', N'Yes', 4, N'Mr Jake Chia ', N'107 TUAS SOUTH AVE 8 OFFSHORE MARINE CENTRE SINGAPORE 637036', N'67055800', N'67948345', 7, NULL, 22, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_16-0007_-_Grains_Pte_Ltd_(Snr_Mgr_2.1)_170720104126.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Customer Confirmation ( CUST 1.1.2.1.1 )', 1, 0, 12, CAST(N'2017-08-25 11:56:44.3862000' AS DateTime2), CAST(N'2017-09-07 15:28:25.2364000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (32, N'Q1700020', N'NODC Pte Ltd', N'Yes', 17, N'Mr Christian Frank ', NULL, N'67055800', NULL, 8, NULL, 10, CAST(N'2017-09-07 00:00:00.0000000' AS DateTime2), N'NODC_16-0005_-_Menck_(Dir_Head)_170907161133.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'', 0, 0, 32, CAST(N'2017-09-07 16:15:59.0848000' AS DateTime2), CAST(N'2017-09-27 17:37:05.0332000' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (33, N'Q1700022', N'NODC Pte Ltd', N'Yes', 18, N'Haze Tan', NULL, N'63537767', NULL, 10, NULL, 10, CAST(N'2017-09-07 00:00:00.0000000' AS DateTime2), N'NODC_16-0008_-_AceCom_Technologies_(Director_1)_170907173545.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'', 0, 0, 33, CAST(N'2017-09-07 17:41:25.4164000' AS DateTime2), CAST(N'2017-09-27 17:40:51.7012000' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (35, N'Q1700024', N'NODC Pte Ltd', N'Yes', 20, N'Mr Alvin', NULL, N'96759990', NULL, 15, NULL, 15, CAST(N'2017-09-14 00:00:00.0000000' AS DateTime2), N'NODC_16-0007_-_Grains_Pte_Ltd_(Snr_Mgr_2.1)_170927175053.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Confirmed', 0, 0, 35, CAST(N'2017-09-14 17:23:21.2234000' AS DateTime2), CAST(N'2017-09-27 17:51:01.8016000' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (36, N'Q1700025', N'NODC Pte Ltd', N'Yes', 20, N'Mr Alvin', NULL, N'96759990', NULL, 15, NULL, 15, CAST(N'2017-09-14 00:00:00.0000000' AS DateTime2), NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Director2 )', 0, 0, 36, CAST(N'2017-09-14 17:23:26.0594000' AS DateTime2), CAST(N'2017-09-14 17:34:28.2638000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (30, N'Q1700008-R1-V2', N'NODC Pte Ltd', N'Yes', 7, N' Mr Hendra Chong, Miss Trixy Tan', N'114 Lavender Street CT Hub 2, #08-53 Singapore 338729	', N'87820766', NULL, 8, NULL, 9, CAST(N'2017-07-20 00:00:00.0000000' AS DateTime2), N'NODC_17-0015_-_Now_Comms_(Snr_Mgr_3)_170720160126.pdf', N'testing', N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Customer Confirmation ( Loh Chiat Min )', 1, 2, 8, CAST(N'2017-08-17 12:05:51.0932000' AS DateTime2), CAST(N'2017-09-07 15:28:49.9468000' AS DateTime2), N'Y')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (34, N'Q1700023', N'NODC Pte Ltd', N'Yes', 19, N'Miss Swathi Mathur', N'21 Media Circle #05-06, Infinite Studios Singapore 138562', N'90046524', NULL, 11, NULL, 15, CAST(N'2017-09-14 00:00:00.0000000' AS DateTime2), NULL, N'test', N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Cancelled by Customer', 0, 0, 34, CAST(N'2017-09-14 16:53:07.7390000' AS DateTime2), CAST(N'2017-09-27 17:44:23.5336000' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (37, N'Q1700026', N'NODC Pte Ltd', N'Yes', 21, N'Tay Ying Hui', NULL, N'94554784', NULL, 12, NULL, 12, CAST(N'2017-09-14 00:00:00.0000000' AS DateTime2), N'NODC_17-0038_-_My_Banh_Mi_at_TPC_(Snr_Mgr_1.1)_170927174539.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Confirmed', 0, 0, 37, CAST(N'2017-09-14 17:50:36.1190000' AS DateTime2), CAST(N'2017-09-27 17:45:55.2460000' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (38, N'Q1700027', N'NODC Pte Ltd', N'Yes', 22, N'Jacky Lim', NULL, N'93835543', NULL, 13, NULL, 13, CAST(N'2017-09-14 00:00:00.0000000' AS DateTime2), N'NODC_17-0052_-_Liv_Activ_@_CCP_(Snr_Mgr_1.3)_170927174755.pdf', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'', 0, 0, 38, CAST(N'2017-09-14 17:57:17.3510000' AS DateTime2), CAST(N'2017-09-27 17:48:22.0420000' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (39, N'Q1700028', N'NODC Pte Ltd', N'Yes', 17, N'Mr Christian Frank ', NULL, N'67055800', NULL, 7, NULL, 7, CAST(N'2017-10-19 00:00:00.0000000' AS DateTime2), N'Chrysanthemum_171019165531.jpg,Desert_171019165531.jpg,Hydrangeas_171019165531.jpg,Jellyfish_171019165531.jpg,Koala_171019165531.jpg,Lighthouse_171019165531.jpg,Penguins_171019165531.jpg,Tulips_171019165531.jpg', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Confirmed', 0, 0, 39, CAST(N'2017-10-19 16:55:05.1754000' AS DateTime2), CAST(N'2017-10-19 17:23:56.6974000' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (40, N'Q1700028-V1', N'NODC Pte Ltd', N'Yes', 17, N'Mr Christian Frank ', NULL, N'67055800', NULL, 7, NULL, 7, CAST(N'2017-10-19 00:00:00.0000000' AS DateTime2), N'Chrysanthemum_171019172353.jpg,Desert_171019172353.jpg,Hydrangeas_171019172353.jpg,Jellyfish_171019172353.jpg,Koala_171019172353.jpg,Lighthouse_171019172353.jpg,Penguins_171019172353.jpg,Tulips_171019172353.jpg', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Confirmed', 0, 1, 39, CAST(N'2017-10-19 17:23:56.6662000' AS DateTime2), CAST(N'2017-10-19 17:23:56.6662000' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, 7, CAST(N'2017-10-24 00:00:00.0000000' AS DateTime2), NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Draft', 0, 0, 41, CAST(N'2017-10-24 16:53:03.0302000' AS DateTime2), CAST(N'2017-10-24 16:53:45.7430000' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (42, N'Q1700029', N'NODC Pte Ltd', N'Yes', 17, N'Mr Christian Frank ', NULL, N'67055800', NULL, 7, NULL, 7, CAST(N'2017-10-25 00:00:00.0000000' AS DateTime2), NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Customer Confirmation ( Elvin Ong )', 0, 0, 42, CAST(N'2017-10-25 10:36:58.7482000' AS DateTime2), CAST(N'2017-10-25 10:37:12.5230000' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (43, N'Q1700030', N'NODC Pte Ltd', N'Yes', 29, N'cust1022', NULL, N'1234', NULL, 33, NULL, 33, CAST(N'2017-11-02 00:00:00.0000000' AS DateTime2), NULL, NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Pending Manager Approval ( Tay Ying Yi )', 0, 0, 43, CAST(N'2017-11-02 14:55:54.7246000' AS DateTime2), CAST(N'2017-11-02 14:55:54.7402000' AS DateTime2), N'N')
INSERT [dbo].[Quotations] ([ID], [QuotationID], [CompanyName], [HasGST], [CustomerId], [ContactPerson], [Address], [Tel], [Fax], [PreparedById], [SalesPersonType], [SalesPersonId], [QuotationDate], [SalesDocumentUploads], [Remarks], [TermsAndConditions], [Status], [ReviseQuotationTimes], [VariationOrderTimes], [PreviousQuotationId], [CreatedOn], [UpdatedOn], [IsDeleted]) VALUES (44, N'Q1700031', N'NODC Pte Ltd', N'Yes', 17, N'Mr Christian Frank ', NULL, N'67055800', NULL, 3, NULL, 3, CAST(N'2017-11-07 00:00:00.0000000' AS DateTime2), N'Chrysanthemum_171107143331.jpg', NULL, N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'Confirmed', 0, 0, 44, CAST(N'2017-11-07 14:30:32.6218000' AS DateTime2), CAST(N'2017-11-07 14:33:36.3586000' AS DateTime2), N'N')
SET IDENTITY_INSERT [dbo].[Quotations] OFF
SET IDENTITY_INSERT [dbo].[SystemSettings] ON 

INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (1, N'COMPANY_NAME', N'NODC Pte Ltd', N'string', N'Y', CAST(N'2017-11-07 14:38:32.7284000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (2, N'COMPANY_ADDRESS', N'123', N'string', N'Y', CAST(N'2017-11-07 14:38:32.7440000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (3, N'COMPANY_POSTAL_CODE', N'12345', N'string', N'Y', CAST(N'2017-11-07 14:38:32.7440000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (4, N'DEFAULT_EMAIL', N'email@gmail.com', N'string', N'Y', CAST(N'2017-11-07 14:38:32.7440000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (5, N'MAIN_CONTACT_PERSON', N'lim', N'string', N'N', CAST(N'2017-11-07 14:38:32.7440000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (6, N'MAIN_CONTACT_NO', N'123', N'string', N'N', CAST(N'2017-11-07 14:38:32.7440000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (7, N'PREFIX_INVOICE_ID', N'INV', N'string', N'N', CAST(N'2017-11-07 14:38:32.7596000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (8, N'PREFIX_QUOTATION_ID', N'Q', N'string', N'N', CAST(N'2017-11-07 14:38:32.7596000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (9, N'PREFIX_PROJECT_ID', N'P', N'string', N'N', CAST(N'2017-11-07 14:38:32.7596000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (10, N'PREFIX_CUSTOMER_ID', N'C', N'string', N'N', CAST(N'2017-11-07 14:38:32.7596000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (11, N'PREFIX_USER_ID', N'U', N'string', N'N', CAST(N'2017-11-07 14:38:32.7596000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (12, N'PREFIX_PAYOUT_ID', N'P', N'string', N'N', CAST(N'2017-11-07 14:38:32.7596000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (13, N'PREFIX_EARLY_PAYOUT_ID', N'E', N'string', N'N', CAST(N'2017-11-07 14:38:32.7596000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (14, N'TERM_AND_CONDITION', N'<ol>
	<li style="font-weight:bold">STANDARD TERMS AND CONDITIONS
	<ol style="font-weight:normal">
		<li>The following are the standard terms and conditions for our Services and shall apply to work undertaken by KS Project Management Pte Ltd (Company Registration No. 2011500075E) for all its clients under our quotation.</li>
		<li>By signing on the acceptance portion of our quotation, you agree to be bound by all the terms and conditions contained herein.</li>
		<li>123</li>
	</ol>
	</li>
	<li style="font-weight:bold">OUR FEES AND DEPOSIT
	<ol style="font-weight:normal">
		<li>A deposit of the total fee payable under our quotation as stated in our quotation (the &ldquo;Deposit&rdquo;) is due and payable to us immediately upon the signing of the acceptance portion of our quotation by you. Further payments shall become due to us upon the completion of each stage of work as stated in our quotation. We reserve the right not to commence any work until the Deposit has been paid to us in full and/or to continue any work until the payment required for each stage of work has been paid to us</li>
		<li>The Deposit once paid is strictly non-refundable if the contract is terminated by you through no fault of ours after we have commenced work for you. If work has not been commenced and the contract is terminated by you through no fault of ours and provided that we have not commenced any work for you, we shall be entitled to retain 50% of the Deposit as liquidated damages.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PAYMENT AND LATE INTEREST
	<ol style="font-weight:normal">
		<li>Subject to the &ldquo;Approval of Work&rdquo; clause and upon expiration of the 7-day Review Period, we will invoice you for each stage of work completed.</li>
		<li>All payments must be received by us within seven (7) days from the date of our invoice. Any payment(s) received by us after seven (7) days from the date of our invoice shall be subjected to a late payment interest charge of [5%] on the invoiced sum calculated on a daily basis until full payment is received by us.</li>
		<li>All payments shall be by way of crossed cheques made in favour of &ldquo;KS Project Management Pte Ltd&rdquo;. We do not accept any cash payments and we shall not be held liable for any cash payments given directly to and/or accepted by any of our employees, agents, servants and/or sub-contractors for and on behalf of KS Project Management Pte Ltd.</li>
		<li>You agree to fully indemnify us for all costs and expenses including but not limited to legal fees incurred by us to recover any outstanding payments due from you to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUPPLY OF MATERIALS/ITEMS
	<ol style="font-weight:normal">
		<li>We will supply all material/items as listed in our quotation to complete the work in accordance with any agreed specification(s).</li>
		<li>Any material(s)/items not specifically stated in our quotation is/are excluded from the total fee payable under our quotation.</li>
		<li>If you require us to source for any materials and/or undertake work for items not specifically stated in our quotation, we reserve the right to issue you a further quotation and charge additional fees to procure materials and/or undertake such work.</li>
		<li>All materials supplied by us are likely to contain slight imperfections and likely vary in terms of colour, tone and veins. You are deemed to have accepted the condition of such materials as supplied by us if the imperfection does not exceed 15% of the total visible surface area of the materials.</li>
	</ol>
	</li>
	<li style="font-weight:bold">VARIATIONS
	<ol style="font-weight:normal">
		<li>Any request(s) for a variation of our Services by you shall be made in writing to us.</li>
		<li>We reserve the right to render a further quotation to you upon receipt of your written instruction(s) for a variation of our Services.</li>
		<li>For the avoidance of doubt, we are not obliged to commence any work requested by you under your written instruction(s) for a variation of our Services until our further quotation for such variation has been accepted by you.</li>
	</ol>
	</li>
	<li style="font-weight:bold">PROJECT DELAYS AND CLIENT LIABILITY
	<ol style="font-weight:normal">
		<li>Any time frames or estimates that we give to provide our Services are contingent upon your full co-operation to provide us with timely instructions and/or the relevant access to your premises. It is required that you provide us with a single point of contact to be made available on a daily basis in order for us to receive timely instructions and/or confirmation.</li>
		<li>We shall not be held liable for any delays and/or failure to provide our Services caused by factors outside of our control, including but not limited to, war, riots, fire, flood, hurricane, typhoon, earthquake, lightning, explosion, strikes, lockouts, slowdown, prolonged shortage of energy supplies and acts of state or governmental action prohibiting and/or impeding us from performing our Services. If such circumstances continue for a continuous period of more than six (6) months, we shall be entitled to cease all Services yet to be rendered to you and make a full refund to you (if any) for such Services yet to be rendered. In such event, no party shall make any further claims against the other party save for antecedent breaches under these terms and conditions.</li>
	</ol>
	</li>
	<li style="font-weight:bold">APPROVAL OF WORK
	<ol style="font-weight:normal">
		<li>On completion of each stage of the work you will be notified and have the opportunity to review it. You must notify us in writing of any unsatisfactory points within seven (7) days of such notification (the &ldquo;7-day Review Period&rdquo;). Any of the work which has not been reported in writing to us as unsatisfactory within the 7-day Review Period will be deemed to have been approved. Once approved, or deemed approved, the work cannot be subsequently rejected by you and the work will be deemed to have been completed by us and the payment for that portion of work completed or deemed completed will become due to us.</li>
	</ol>
	</li>
	<li style="font-weight:bold">REJECTED WORK
	<ol style="font-weight:normal">
		<li>If you reject any of our work within the 7-day Review Period, or not approve any subsequent work performed by us to remedy/rectify any points being noted as unsatisfactory, and we, acting reasonably, consider that you have been unreasonably in the rejection of any of the work, can elect to treat this contract at an end and take steps to recover any payment due for completed work.</li>
	</ol>
	</li>
	<li style="font-weight:bold">SUBCONTRACTING
	<ol style="font-weight:normal">
		<li>We reserve the right to subcontract any Services that we have agreed to perform for you as we deem fit.</li>
	</ol>
	</li>
	<li style="font-weight:bold">NON-DISCLOSURE &amp; PERSONAL DATA PROTECTION ACT 2012
	<ol style="font-weight:normal">
		<li>Subject to Clause 10.2, we agree that we will not at any time disclose any of your confidential information to any third party.</li>
		<li>From time to time, we may collect, use and disclose your personal data for the purpose of completing any work undertaken by us for you and you consent to us collecting, using and disclosing your personal information for such purpose in accordance with the Personal Data Protection Act 2012.</li>
	</ol>
	</li>
	<li style="font-weight:bold">CONSEQUENTIAL LOSS
	<ol style="font-weight:normal">
		<li>We shall not be liable for any losses, damages and/or delays caused directly or indirectly by third parties whom we have not contracted with to provide the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">OWNERSHIP OF MATERIALS AND FIXTURES &amp; INTELLECTUAL PROPERTY
	<ol style="font-weight:normal">
		<li>Materials and/or items supplied by us for each stage of the Services remain the sole property of KS Project Management Pte Ltd until full payment is received by us in accordance to the quotation. We reserve the right to recover any or all materials and/or items supplied by us if payment is not received in accordance to the quotation.</li>
		<li>All intellectual property rights relating to designs, plans and/or drawings provided and/or created by us for the provision of the Services remain the sole property of KS Project Management Pte Ltd and you recognise and agree that all such intellectual property rights remain vested with us both during and after the provision of the Services.</li>
		<li>You are responsible and must obtain all necessary permissions and authorisations in respect of the use of all copies, graphics, images, registered company logos, names, trademarks and any other materials that you supply to us to include in the provision of the Services.</li>
		<li>In this respect, you agree to fully indemnify and hold us harmless against any claims or legal actions brought by the owner of such intellectual property rights relating to the use of any such contents as supplied by you for the provision of the Services.</li>
	</ol>
	</li>
	<li style="font-weight:bold">MISCELLANEOUS
	<ol style="font-weight:normal">
		<li>In these terms and conditions, &quot;Services&quot; means all the following services provided by us as listed under our quotation.</li>
		<li>Save as provided in our quotation and under these terms and conditions, no other warranties whether express or implied, statutory or otherwise are given by us in relation to the provision of the Services.</li>
		<li>In the event that any provision(s) in these terms and conditions is/are found to be invalid or unenforceable by a Court of competent jurisdiction, the affected provision(s) shall be severable and shall not affect the validity or enforceability of any other provisions contained herein.</li>
	</ol>
	</li>
	<li style="font-weight:bold">GOVERNING LAW
	<ol style="font-weight:normal">
		<li>These terms and conditions shall be governed and construed in accordance with the laws of the Republic of Singapore and any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the Singapore Courts in all matters regarding it.</li>
	</ol>
	</li>
</ol>
', N'string', N'N', CAST(N'2017-11-07 14:38:32.7752000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (15, N'PERCENTAGE_HIGHLIGH_RED', N'17', N'string', N'Y', CAST(N'2017-11-07 14:38:32.7752000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (16, N'GST', N'7', N'string', N'Y', CAST(N'2017-11-07 14:38:32.7752000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (17, N'AGENCY_LIST', N'Propnex|ERA', N'string', N'Y', CAST(N'2017-11-07 14:38:32.7752000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (18, N'QUOTATION_CATEGORIES', N'', N'string', N'Y', CAST(N'2017-11-07 14:38:32.7908000' AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (19, N'LIST_OF_COMPANY', N'NODC Pte Ltd|Yes|80 Ubi Avenue 4 UB.One #07-06 Singapore 408830
Tel: +65 6802 9579 Email: admin@kspm.com.sg
Co & GST Reg No: 201500075E;No 1 Design Consultancy Pte Ltd|Yes|82 Ubi Avenue 4 UB.One #07-06 Singapore 408830
Tel: +65 6802 9579 Email: admin@kspm.com.sg
Co & GST Reg No: 201500075E', N'string', N'Y', CAST(N'2017-11-07 14:38:32.7908000' AS DateTime2))
SET IDENTITY_INSERT [dbo].[SystemSettings] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (1, N'U1700001', N'hongguan', N'Rn5sE6W5YkI=', N'Lim Hong Guan', N'46 Tagore Lane, 787900 Singapore', N'hongguan@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'Yes', 1, N'Super Admin', NULL, CAST(1000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-04-27 00:00:00.0000000' AS DateTime2), CAST(N'2017-05-02 17:54:08.0213220' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (2, N'U1700002', N'erntay2', N'Rn5sE6W5YkI=', N'Stephen Tay Chan Ern', N'46 Tagore Lane, 787900 Singapore', N'chanern1@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'Yes', 1, N'Super Admin', NULL, CAST(1000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-04-27 16:52:45.6498767' AS DateTime2), CAST(N'2017-08-17 10:12:24.5792000' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (3, N'U1700003', N'aaron', N'Rn5sE6W5YkI=', N'Aaron', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'aaron@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'No', 1, N'Sales', N'Director', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-04-27 17:21:03.7094309' AS DateTime2), CAST(N'2017-04-27 17:21:03.7094309' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (4, N'U1700004', N'accounts', N'Rn5sE6W5YkI=', N'accounts', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'accounts@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'No', 1, N'Accounts', NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-04-27 17:17:21.3312901' AS DateTime2), CAST(N'2017-04-27 17:17:21.3312901' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (5, N'U1700005', N'bobby', N'Rn5sE6W5YkI=', N'Bobby', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'bobby@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'No', 3, N'Sales', N'Senior Manager', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-04-27 17:21:50.1600752' AS DateTime2), CAST(N'2017-04-27 17:21:50.1600752' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (6, N'U1700006', N'colins', N'Rn5sE6W5YkI=', N'Colins', N'81 UBI AVENUE 4, #07-06,Singapore 408830', N'colins@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'No', N'No', 5, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-04-27 17:22:20.9107281' AS DateTime2), CAST(N'2017-04-27 17:22:20.9107281' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (7, N'U1700007', N'elvin', N'Rn5sE6W5YkI=', N'Elvin Ong', N'46 Tagore Lane, 787900 Singapore', N'elvinong@thedottsolutions.com', N'65 9192 9394', N'65 9192 9393', N'Bank of Singapore', N'123456789', NULL, N'Yes', N'Yes', 1, N'Super Admin', NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-05-02 17:51:41.9960503' AS DateTime2), CAST(N'2017-05-02 17:51:41.9960503' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (8, N'U1700008', N'DirectorH', N'Rn5sE6W5YkI=', N'Tay Ying Yi', NULL, N'davis.tay@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'Yes', 1, N'Sales', N'Director', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 14:19:49.0069961' AS DateTime2), CAST(N'2017-11-02 14:59:57.5542000' AS DateTime2), N'N', N'Ob1geX+NbmDq+6OvLt99QQ==')
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (9, N'U1700009', N'SM3', N'phWjm/FyrHc=', N'Loh Chiat Min', NULL, N'chiatmn.loh@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 8, N'Sales', N'Senior Manager', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 14:22:55.1773961' AS DateTime2), CAST(N'2017-07-20 14:23:11.3233961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (10, N'U1700010', N'Director1', N'phWjm/FyrHc=', N'Director1', NULL, N'000@nodc.com', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 8, N'Sales', N'Director', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 14:24:30.7897961' AS DateTime2), CAST(N'2017-09-14 17:33:24.2570000' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (11, N'U1700011', N'Director2', N'phWjm/FyrHc=', N'Director2', NULL, N'Director2@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 8, N'Sales', N'Director', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 14:25:37.6201961' AS DateTime2), CAST(N'2017-09-14 17:32:37.3478000' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (12, N'U1700012', N'SM11', N'phWjm/FyrHc=', N'SNR MGR 1.1', NULL, N'SM1.1@nosc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 10, N'Sales', N'Senior Manager', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 14:27:25.7905961' AS DateTime2), CAST(N'2017-07-20 14:28:01.3273961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (13, N'U1700013', N'SM12', N'phWjm/FyrHc=', N'SNR MGR 1.2', NULL, N'SNRMGR@NODC.COM', N'0000', NULL, NULL, NULL, NULL, N'No', N'No', 10, N'Sales', N'Senior Manager', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 14:29:17.4241961' AS DateTime2), CAST(N'2017-07-20 14:29:17.4241961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (14, N'U1700014', N'SM13', N'phWjm/FyrHc=', N'SNR MGR 1.3', NULL, N'SM13@NODC.COM.DG', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 10, N'Sales', N'Senior Manager', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 14:42:03.1033961' AS DateTime2), CAST(N'2017-07-20 14:42:03.1033961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (16, N'U1700016', N'SM22', N'phWjm/FyrHc=', N'SNR MGR 2.2', NULL, N'SNRMGR3@NODC.COM', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 11, N'Sales', N'Senior Manager', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:28:39.7549961' AS DateTime2), CAST(N'2017-07-20 17:28:39.7549961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (17, N'U1700017', N'SM23', N'phWjm/FyrHc=', N'SNR MGR 2.3', NULL, N'SNRMGR4@NODC.COM', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 11, N'Sales', N'Senior Manager', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:29:50.9377961' AS DateTime2), CAST(N'2017-07-20 17:29:50.9377961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (18, N'U1700018', N'PROF111', N'phWjm/FyrHc=', N'PropNex 1.1.1', NULL, N'propnex@nods.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 12, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:31:50.6053961' AS DateTime2), CAST(N'2017-07-20 17:31:50.6053961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (19, N'U1700019', N'PROF112', N'phWjm/FyrHc=', N'PropNex 1.1.2', NULL, N'propnex1@nods.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 12, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:32:55.4233961' AS DateTime2), CAST(N'2017-07-20 17:32:55.4233961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (31, N'U1700031', N'chiatmin', N'Vi9Z4MJx09NchrlZMEltRw==', N'Loh Chiat Min', NULL, N'chiatmin.loh@nodc.com.sg', N'91681610', NULL, NULL, NULL, NULL, N'Yes', N'Yes', 8, N'Sales', N'Senior Manager', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-09-10 11:28:56.5980000' AS DateTime2), CAST(N'2017-09-10 11:29:32.6184000' AS DateTime2), N'Y', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (32, N'U1700031', N'Mgr10', N'phWjm/FyrHc=', N'Manager 10', NULL, N'asr@gmail.com', N'1234', NULL, NULL, NULL, NULL, N'No', N'No', 10, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-11-02 14:40:14.9182000' AS DateTime2), CAST(N'2017-11-02 14:40:14.9182000' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (33, N'U1700033', N'Con101', N'phWjm/FyrHc=', N'Consultatn 101', NULL, N'con101@gmail.com', N'124', NULL, NULL, NULL, NULL, N'No', N'No', 32, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-11-02 14:41:12.7474000' AS DateTime2), CAST(N'2017-11-02 14:42:20.4358000' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (34, N'U1700034', N'Con102', N'phWjm/FyrHc=', N'Consultant 102', NULL, N'con102@gmail.com', N'1243', NULL, NULL, NULL, NULL, N'No', N'No', 32, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-11-02 14:42:03.7906000' AS DateTime2), CAST(N'2017-11-02 14:42:03.7906000' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (15, N'U1700015', N'SM21', N'phWjm/FyrHc=', N'SNR MGR 2.1', NULL, N'SNRMGR1@NODC.COM', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 11, N'Sales', N'Senior Manager', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:27:14.3449961' AS DateTime2), CAST(N'2017-07-20 17:27:14.3449961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (20, N'U1700020', N'CUST1121', N'phWjm/FyrHc=', N'CUST 1.1.2.1', NULL, N'cust@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 19, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:34:27.1825961' AS DateTime2), CAST(N'2017-07-20 17:34:27.1825961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (21, N'U1700021', N'CUST11211', N'phWjm/FyrHc=', N'CUST 1.1.2.1.1', NULL, N'cust1@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 20, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:35:32.6401961' AS DateTime2), CAST(N'2017-07-20 17:35:32.6401961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (22, N'U1700022', N'CON1.3.1', N'phWjm/FyrHc=', N'CONS 1.3.1', NULL, N'cust5@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 14, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:38:59.0593961' AS DateTime2), CAST(N'2017-07-20 17:38:59.0593961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (23, N'U1700023', N'PROF1311', N'phWjm/FyrHc=', N'PropNex 1.3.1.1', NULL, N'propnex11@nods.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 22, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:40:30.0541961' AS DateTime2), CAST(N'2017-07-20 17:40:30.0541961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (24, N'U1700024', N'PROF211', N'phWjm/FyrHc=', N'ERA 2.1.1', NULL, N'ERA1@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 15, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:43:21.8881961' AS DateTime2), CAST(N'2017-07-20 17:43:21.8881961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (25, N'U1700025', N'PROF212', N'phWjm/FyrHc=', N'ERA 2.1.2', NULL, N'ERA2@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 15, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:44:31.0897961' AS DateTime2), CAST(N'2017-07-20 17:44:31.0897961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (26, N'U1700026', N'CUST2121', N'phWjm/FyrHc=', N'CUST 2.1.2.1', NULL, N'custi@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 25, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:45:58.8709961' AS DateTime2), CAST(N'2017-07-20 17:45:58.8709961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (27, N'U1700027', N'CUST21211', N'phWjm/FyrHc=', N'CUST 2.1.2.1.1', NULL, N'cust6@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 26, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:47:37.2289961' AS DateTime2), CAST(N'2017-07-20 17:47:37.2289961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (28, N'U1700028', N'CON231', N'phWjm/FyrHc=', N'CONS 2.3.1', NULL, N'const@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 17, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:49:42.1537961' AS DateTime2), CAST(N'2017-07-20 17:49:42.1537961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (29, N'U1700029', N'PROF2311', N'phWjm/FyrHc=', N'ERA 2.3.1.1', NULL, N'ERA4@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 28, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-20 17:50:54.8497961' AS DateTime2), CAST(N'2017-07-20 17:50:54.8497961' AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfit], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (30, N'U1700030', N'CUST13111', N'phWjm/FyrHc=', N'CUST1.3.1.1', NULL, N'nodc@nodc.com.sg', N'0000000', NULL, NULL, NULL, NULL, N'No', N'No', 23, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(N'2017-07-25 17:03:23.6999132' AS DateTime2), CAST(N'2017-07-25 17:03:23.6999132' AS DateTime2), N'N', NULL)
SET IDENTITY_INSERT [dbo].[Users] OFF
