/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_ProjectEstimators
	(
	ID int NOT NULL IDENTITY (1, 1),
	ProjectId int NOT NULL,
	RowId int NOT NULL,
	ProjectExpensesCategoryName nvarchar(255) NOT NULL,
	ProjectExpensesAmount decimal(18, 2) NULL,
	ProjectExpensesProjectedCost decimal(18, 2) NULL,
	ProjectExpensesAdjCost decimal(18, 2) NOT NULL,
	ProjectExpensesProfit decimal(18, 2) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_ProjectEstimators SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_ProjectEstimators ON
GO
IF EXISTS(SELECT * FROM dbo.ProjectEstimators)
	 EXEC('INSERT INTO dbo.Tmp_ProjectEstimators (ID, ProjectId, RowId, ProjectExpensesCategoryName, ProjectExpensesAdjCost, ProjectExpensesProfit)
		SELECT ID, ProjectId, RowId, ProjectExpensesCategoryName, ProjectExpensesAdjCost, ProjectExpensesProfit FROM dbo.ProjectEstimators WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_ProjectEstimators OFF
GO
DROP TABLE dbo.ProjectEstimators
GO
EXECUTE sp_rename N'dbo.Tmp_ProjectEstimators', N'ProjectEstimators', 'OBJECT' 
GO
COMMIT
