USE [NODC_OMS]
GO
/****** Object:  Table [dbo].[AuditLogs]    Script Date: 3/4/2017 10:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditLogs](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IPAddress] [nvarchar](255) NOT NULL,
	[Timestamp] [datetime2](7) NOT NULL,
	[UserTriggeringId] [int] NOT NULL,
	[UserRole] [nvarchar](255) NOT NULL,
	[TableAffected] [nvarchar](255) NOT NULL,
	[Description] [ntext] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerReminders]    Script Date: 3/4/2017 10:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerReminders](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[Repeat] [nvarchar](255) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customers]    Script Date: 3/4/2017 10:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [nvarchar](255) NOT NULL,
	[LoginID] [nvarchar](255) NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[CompanyName] [nvarchar](255) NULL,
	[Agency] [nvarchar](255) NOT NULL,
	[ContactPerson] [nvarchar](255) NOT NULL,
	[Address] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[Tel] [nvarchar](255) NOT NULL,
	[Fax] [nvarchar](255) NULL,
	[BankName] [nvarchar](255) NULL,
	[BankAccNo] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[UplineId] [int] NOT NULL,
	[Position] [nvarchar](255) NOT NULL,
	[AssignToId] [int] NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL,
	[ResetPasswordToken] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoiceItemDescriptions]    Script Date: 3/4/2017 10:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceItemDescriptions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceId] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[PercentageQuotation] [int] NOT NULL,
	[Amount] [decimal](20, 2) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Invoices]    Script Date: 3/4/2017 10:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoices](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceID] [nvarchar](255) NOT NULL,
	[QuotationId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[ContactPerson] [nvarchar](255) NOT NULL,
	[PreparedById] [int] NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[Remarks] [ntext] NULL,
	[Notes] [ntext] NULL,
	[HaveGST] [nvarchar](255) NOT NULL,
	[PaymentMade] [decimal](18, 2) NOT NULL,
	[SubTotal] [decimal](18, 2) NOT NULL,
	[GSTTotal] [decimal](18, 2) NOT NULL,
	[GrantTotal] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Payouts]    Script Date: 3/4/2017 10:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payouts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PayoutID] [nvarchar](255) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[UserType] [nvarchar](255) NOT NULL,
	[PayToId] [int] NOT NULL,
	[PayoutDate] [datetime2](7) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[Remarks] [ntext] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProjectEstimators]    Script Date: 3/4/2017 10:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProjectEstimators](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[RowId] [int] NOT NULL,
	[ProjectExpensesCategoryName] [nvarchar](255) NOT NULL,
	[ProjectExpensesAdjCost] [decimal](18, 2) NOT NULL,
	[ProjectExpensesProfit] [decimal](18, 2) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProjectProjectExpenses]    Script Date: 3/4/2017 10:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProjectProjectExpenses](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[SupplierName] [nvarchar](255) NOT NULL,
	[Date] [datetime] NOT NULL,
	[GST] [nvarchar](255) NOT NULL,
	[Cost] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Projects]    Script Date: 3/4/2017 10:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Projects](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [nvarchar](255) NOT NULL,
	[CompanyName] [nvarchar](255) NOT NULL,
	[QuotationReferenceId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[PreparedById] [int] NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[CoordinatorType] [nvarchar](255) NULL,
	[CoordinatorId] [int] NOT NULL,
	[ProjectTitle] [nvarchar](255) NOT NULL,
	[ProjectDescription] [ntext] NULL,
	[ProjectType] [nvarchar](255) NOT NULL,
	[PhotoUploads] [nvarchar](255) NULL,
	[FloorPlanUploads] [nvarchar](255) NULL,
	[ThreeDDrawingUploads] [nvarchar](255) NULL,
	[AsBuildDrawingUploads] [nvarchar](255) NULL,
	[SubmissionStageUploads] [nvarchar](255) NULL,
	[OtherUploads] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuotationCoBrokes]    Script Date: 3/4/2017 10:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuotationCoBrokes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationId] [int] NOT NULL,
	[QuotationCoBrokeUserType] [nvarchar](255) NOT NULL,
	[QuotationCoBrokeUserId] [int] NOT NULL,
	[QuotationCoBrokePercentOfProfit] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuotationItemDescriptions]    Script Date: 3/4/2017 10:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuotationItemDescriptions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationId] [int] NOT NULL,
	[RowId] [int] NOT NULL,
	[CategoryName] [nvarchar](255) NOT NULL,
	[QuotationItem_Description] [nvarchar](255) NOT NULL,
	[QuotationItemQty] [int] NULL,
	[QuotationItemUnit] [nvarchar](255) NULL,
	[QuotationItemRate] [decimal](18, 2) NULL,
	[QuotationItemAmount] [decimal](18, 2) NULL,
	[QuotationItemEstCost] [decimal](18, 2) NULL,
	[QuotationItemEstProfit] [decimal](18, 2) NULL,
	[QuotationItemRemarks] [ntext] NULL,
	[QuotationItemMgrComments] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Quotations]    Script Date: 3/4/2017 10:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quotations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationID] [nvarchar](255) NOT NULL,
	[CompanyName] [nvarchar](255) NULL,
	[HasGST] [nvarchar](255) NULL,
	[CustomerId] [int] NULL,
	[ContactPerson] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Tel] [nvarchar](255) NULL,
	[Fax] [nvarchar](255) NULL,
	[PreparedById] [int] NOT NULL,
	[SalesPersonType] [nvarchar](255) NULL,
	[SalesPersonId] [int] NULL,
	[QuotationDate] [datetime2](7) NULL,
	[SalesDocumentUploads] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[TermsAndConditions] [ntext] NULL,
	[Status] [nvarchar](255) NOT NULL,
	[ReviseQuotationTimes] [int] NOT NULL,
	[VariationOrderTimes] [int] NOT NULL,
	[PreviousQuotationId] [int] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SystemSettings]    Script Date: 3/4/2017 10:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemSettings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](255) NOT NULL,
	[Value] [nvarchar](255) NULL,
	[DataType] [nvarchar](255) NOT NULL,
	[IsRequired] [nvarchar](255) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 3/4/2017 10:46:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [nvarchar](255) NOT NULL,
	[LoginID] [nvarchar](255) NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Address] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[Tel] [nvarchar](255) NOT NULL,
	[Mobile] [nvarchar](255) NULL,
	[BankName] [nvarchar](255) NULL,
	[BankAccNo] [nvarchar](255) NULL,
	[Remarks] [ntext] NULL,
	[Reminder] [nvarchar](255) NOT NULL,
	[IsAgencyLeader] [nvarchar](255) NOT NULL,
	[UplineId] [int] NOT NULL,
	[Role] [nvarchar](255) NOT NULL,
	[Position] [nvarchar](255) NULL,
	[PresetValue] [decimal](18, 2) NOT NULL,
	[TotalCompletedProfile] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[UpdatedOn] [datetime2](7) NOT NULL,
	[IsDeleted] [nvarchar](255) NOT NULL,
	[ResetPasswordToken] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[AuditLogs] ON 

INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (1, N'::1', CAST(0x078885785F58A53C0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (2, N'::1', CAST(0x07D30F944659A53C0B AS DateTime2), 1, N'User', N'Login Transactions', N'Super Admin [hongguan] Logged In')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (3, N'::1', CAST(0x07FC523B6259A53C0B AS DateTime2), 1, N'User', N'Users', N'User [hongguan] Created New User')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (4, N'::1', CAST(0x07846DCB9359A53C0B AS DateTime2), 1, N'User', N'Users', N'User [hongguan] Created New User')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (5, N'::1', CAST(0x07EDB419B459A53C0B AS DateTime2), 1, N'User', N'Users', N'User [hongguan] Created New User')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (6, N'::1', CAST(0x078D16A9CB59A53C0B AS DateTime2), 1, N'User', N'Users', N'User [hongguan] Created New User')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (7, N'::1', CAST(0x0726951CE159A53C0B AS DateTime2), 1, N'User', N'Users', N'User [hongguan] Created New User')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (8, N'::1', CAST(0x0778D467F459A53C0B AS DateTime2), 1, N'User', N'Users', N'User [hongguan] Created New User')
INSERT [dbo].[AuditLogs] ([ID], [IPAddress], [Timestamp], [UserTriggeringId], [UserRole], [TableAffected], [Description]) VALUES (9, N'::1', CAST(0x07A3323F165AA53C0B AS DateTime2), 1, N'User', N'Users', N'User [hongguan] Updated [] User')
SET IDENTITY_INSERT [dbo].[AuditLogs] OFF
SET IDENTITY_INSERT [dbo].[SystemSettings] ON 

INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (1, N'COMPANY_NAME', N'NODC Pte Ltd', N'string', N'Y', CAST(0x07B91421CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (2, N'COMPANY_ADDRESS', N'123', N'string', N'Y', CAST(0x071B7A32CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (3, N'COMPANY_POSTAL_CODE', N'12345', N'string', N'Y', CAST(0x07608C3CCD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (4, N'DEFAULT_EMAIL', N'email@gmail.com', N'string', N'Y', CAST(0x07001845CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (5, N'MAIN_CONTACT_PERSON', N'lim', N'string', N'N', CAST(0x072D924CCD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (6, N'MAIN_CONTACT_NO', N'123', N'string', N'N', CAST(0x077A5A54CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (7, N'PREFIX_INVOICE_ID', N'INV', N'string', N'N', CAST(0x07F9975CCD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (8, N'PREFIX_QUOTATION_ID', N'Q', N'string', N'N', CAST(0x07568764CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (9, N'PREFIX_PROJECT_ID', N'P', N'string', N'N', CAST(0x0739AF6DCD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (10, N'PREFIX_CUSTOMER_ID', N'C', N'string', N'N', CAST(0x07E96176CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (11, N'PREFIX_USER_ID', N'U', N'string', N'N', CAST(0x0747517ECD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (12, N'PREFIX_PAYOUT_ID', N'P', N'string', N'N', CAST(0x07B56786CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (13, N'TERM_AND_CONDITION', N'123', N'string', N'N', CAST(0x0734A58ECD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (14, N'PERCENTAGE_HIGHLIGH_RED', N'17', N'string', N'Y', CAST(0x07B3E296CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (15, N'GST', N'7', N'string', N'Y', CAST(0x0721F99ECD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (16, N'AGENCY_LIST', N'Propnex|ERA', N'string', N'Y', CAST(0x07D2ABA7CD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (17, N'QUOTATION_CATEGORIES', N'Preliminaries|Authority Submission|Partitioning & Ceiling Work|Flooring Work|Painting Work|Mill Work|Glazing Work|Electrical|Plumbing & Sanitory|ACMV Work|Fire Protection Work|Miscellaneous', N'string', N'Y', CAST(0x07FE25AFCD56993C0B AS DateTime2))
INSERT [dbo].[SystemSettings] ([ID], [Code], [Value], [DataType], [IsRequired], [UpdatedOn]) VALUES (18, N'LIST_OF_COMPANY', N'NODC Pte Ltd|Yes;No 1 Design Consultancy Pte Ltd|Yes', N'string', N'Y', CAST(0x077D63B7CD56993C0B AS DateTime2))
SET IDENTITY_INSERT [dbo].[SystemSettings] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfile], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (1, N'U1700001', N'hongguan', N'Rn5sE6W5YkI=', N'Hong Guan', N'123', N'hongguan@thedottsolutions.com', N'123', N'123', N'Public Bank', N'1234', NULL, N'No', N'Yes', 1, N'Super Admin', NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x070000000000A53C0B AS DateTime2), CAST(0x070000000000A53C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfile], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (2, N'U1700002', N'chanern', N'Rn5sE6W5YkI=', N'Chan Ern', N'123', N'chanern@thedottsolutions.com', N'123', N'123', N'Public Bank', N'1234', NULL, N'No', N'Yes', 2, N'Super Admin', NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x070000000000A53C0B AS DateTime2), CAST(0x070000000000A53C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfile], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (3, N'U1700003', N'AccountsAL', N'Rn5sE6W5YkI=', N'AccountsAL', N'123', N'accountsal@gmail.com', N'123', N'123', N'Public Bank', N'1234', NULL, N'No', N'Yes', 2, N'Accounts', NULL, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07FED72A6259A53C0B AS DateTime2), CAST(0x07E15836165AA53C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfile], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (4, N'U1700004', N'SalesConsultantAL', N'Rn5sE6W5YkI=', N'SalesConsultantAL', N'123', N'salesconsultantal@gmail.com', N'123', N'123', N'Public Bank', N'1234', NULL, N'No', N'Yes', 2, N'Sales', N'Consultant', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x074456BA9359A53C0B AS DateTime2), CAST(0x074456BA9359A53C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfile], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (5, N'U1700005', N'SalesAssistantManagerAL', N'Rn5sE6W5YkI=', N'SalesAssistantManagerAL', N'123', N'salesassistantmanageral@gmail.com', N'123', N'123', N'Public Bank', N'1234', NULL, N'No', N'Yes', 2, N'Sales', N'Assistant Manager', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x078FC511B459A53C0B AS DateTime2), CAST(0x078FC511B459A53C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfile], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (6, N'U1700006', N'SalesManagerAL', N'Rn5sE6W5YkI=', N'SalesManagerAL', N'123', N'salesmanageral@gmail.com', N'123', N'123', N'Public Bank', N'1234', NULL, N'No', N'Yes', 2, N'Sales', N'Manager', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x072871A3CB59A53C0B AS DateTime2), CAST(0x072871A3CB59A53C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfile], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (7, N'U1700007', N'SalesSeniorManagerAL', N'Rn5sE6W5YkI=', N'SalesSeniorManagerAL', N'123', N'salesseniormanageral@gmail.com', N'123', N'123', N'Public Bank', N'1234', NULL, N'No', N'Yes', 2, N'Sales', N'Senior Manger', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07A75714E159A53C0B AS DateTime2), CAST(0x07A75714E159A53C0B AS DateTime2), N'N', NULL)
INSERT [dbo].[Users] ([ID], [UserID], [LoginID], [Password], [Name], [Address], [Email], [Tel], [Mobile], [BankName], [BankAccNo], [Remarks], [Reminder], [IsAgencyLeader], [UplineId], [Role], [Position], [PresetValue], [TotalCompletedProfile], [Status], [CreatedOn], [UpdatedOn], [IsDeleted], [ResetPasswordToken]) VALUES (8, N'U1700008', N'SalesDirectorAL', N'Rn5sE6W5YkI=', N'SalesDirectorAL', N'123', N'salesdirectoral@gmail.com', N'123', N'123', N'Public Bank', N'1234', NULL, N'No', N'Yes', 2, N'Sales', N'Director', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Active', CAST(0x07030862F459A53C0B AS DateTime2), CAST(0x07030862F459A53C0B AS DateTime2), N'N', NULL)
SET IDENTITY_INSERT [dbo].[Users] OFF
