/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Projects
	(
	ID int NOT NULL IDENTITY (1, 1),
	ProjectID nvarchar(255) NOT NULL,
	CompanyName nvarchar(255) NOT NULL,
	QuotationReferenceId int NOT NULL,
	CustomerId int NOT NULL,
	PreparedById int NOT NULL,
	Date datetime2(7) NOT NULL,
	CoordinatorId int NOT NULL,
	ProjectTitle nvarchar(255) NOT NULL,
	ProjectDescription ntext NULL,
	ProjectType nvarchar(255) NOT NULL,
	PhotoUploads nvarchar(255) NULL,
	FloorPlanUploads nvarchar(255) NULL,
	ThreeDDrawingUploads nvarchar(255) NULL,
	AsBuildDrawingUploads nvarchar(255) NULL,
	SubmissionStageUploads nvarchar(255) NULL,
	OtherUploads nvarchar(255) NULL,
	Remarks ntext NULL,
	Status nvarchar(255) NOT NULL,
	CompletedDate datetime2(7) NULL,
	CreatedOn datetime2(7) NOT NULL,
	UpdatedOn datetime2(7) NOT NULL,
	IsDeleted nvarchar(255) NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Projects SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Projects ON
GO
IF EXISTS(SELECT * FROM dbo.Projects)
	 EXEC('INSERT INTO dbo.Tmp_Projects (ID, ProjectID, CompanyName, QuotationReferenceId, CustomerId, PreparedById, Date, CoordinatorId, ProjectTitle, ProjectDescription, ProjectType, PhotoUploads, FloorPlanUploads, ThreeDDrawingUploads, AsBuildDrawingUploads, SubmissionStageUploads, OtherUploads, Remarks, Status, CreatedOn, UpdatedOn, IsDeleted)
		SELECT ID, ProjectID, CompanyName, QuotationReferenceId, CustomerId, PreparedById, Date, CoordinatorId, ProjectTitle, ProjectDescription, ProjectType, PhotoUploads, FloorPlanUploads, ThreeDDrawingUploads, AsBuildDrawingUploads, SubmissionStageUploads, OtherUploads, Remarks, Status, CreatedOn, UpdatedOn, IsDeleted FROM dbo.Projects WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Projects OFF
GO
DROP TABLE dbo.Projects
GO
EXECUTE sp_rename N'dbo.Tmp_Projects', N'Projects', 'OBJECT' 
GO
COMMIT
